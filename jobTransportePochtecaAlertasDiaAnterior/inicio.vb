﻿Imports CapaNegocio
Imports System.Data
Imports System.Text
Imports System.Configuration
Imports Syncfusion.XlsIO

Module inicio

#Region "Configuracion Job"
  Const NOMBRE_JOB As String = "jobTransportePochtecaAlertasDiaAnterior"
  Const KEY As String = "AlertasDiaAnterior"
#End Region

#Region "Enum"
  Enum eTablaConfiguracion As Integer
    EmailPara = 0
    EmailCC = 1
    AsuntoEmail = 2
    CuerpoEmail = 3
  End Enum

  Enum eTablaDatos As Integer
    T00_Ejecutivos = 0
  End Enum

#End Region

#Region "Privado"
  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GenerarExcelEjecutivos(ByVal ds As DataSet, ByVal nombreArchivo As String, ByVal dsConfiguracion As DataSet) As Boolean
    Dim fueGenerado As Boolean = False
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim workbook As IWorkbook

    Try
      application.UseNativeStorage = False

      Dim nombreHoja As String() = {"Listado"}
      workbook = application.Workbooks.Create(nombreHoja)

      Dim sheet0 As IWorksheet = workbook.Worksheets(nombreHoja(0))
      GenerarHojaExcel(workbook, sheet0, ds.Tables(eTablaDatos.T00_Ejecutivos))

      workbook.SaveAs(Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls")
      workbook.Close()
      fueGenerado = True

      Return fueGenerado
    Catch ex As Exception
      Throw New Exception(ex.Message)
    Finally
      excelEngine.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GenerarHojaExcel(ByRef workbook As IWorkbook, ByRef sheet As IWorksheet, ByVal dt As DataTable)
    Dim fueGenerado As Boolean = False

    Try
      sheet.ImportDataTable(dt, True, 1, 1)
      With sheet.Range(1, 1, 1, dt.Columns.Count).CellStyle
        .Font.Bold = True
      End With

      sheet.UsedRange.AutofitColumns()
      sheet.IsGridLinesVisible = True
      fueGenerado = True
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' envia excel con las alertas que estan en cola de atencion
  ''' </summary>
  ''' <remarks></remarks>
  Private Function EnviarExcelPorEmail(ByVal dsConfiguracionXML As DataSet, ByVal ds As DataSet) As String
    Dim msgRetorno, nombreArchivo, bodyMensaje, body, contenidoLog, asunto, msgProceso As String
    Dim totalRegistros As Integer
    Dim fueGenerado As Boolean
    Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
    Dim contadorErrores As Integer = 0
    Dim listadoMailPara As String = ""

    Try
      'crea el archivo en disco
      nombreArchivo = Archivo.CrearNombreUnicoArchivo(KEY)
      totalRegistros = ds.Tables(eTablaDatos.T00_Ejecutivos).Rows.Count

      If (totalRegistros = 0) Then
        msgRetorno = ""
      Else
        fueGenerado = GenerarExcelEjecutivos(ds, nombreArchivo, dsConfiguracionXML)

        If fueGenerado Then
          'construye cuerpo del mail
          bodyMensaje = dsConfiguracionXML.Tables(eTablaConfiguracion.CuerpoEmail).Rows(0).Item("texto")
          bodyMensaje = bodyMensaje.Replace("{BR}", vbCrLf)
          'constuye contenido del excel y del log
          body = Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, eTablaDatos.T00_Ejecutivos)

          'construye listado de email de destinos
          If Herramientas.MailModoDebug Then
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas())
            ColeccionEmailsCC = Nothing
          Else
            'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
            listadoMailPara = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailPara, "email")
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
            ColeccionEmailsCC = Mail.ConstruirCollectionMail(Herramientas.MailAdministrador())
          End If
          'crea contenido para crear el log
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(body & "<br/><div>Enviado a: " & listadoMailPara & "</div>", NOMBRE_JOB)
          Dim path As String = Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls"
          'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
          asunto = dsConfiguracionXML.Tables(eTablaConfiguracion.AsuntoEmail).Rows(0).Item("texto")

          msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, False, ColeccionEmailsCC, True, path)
          If msgProceso <> "" Then
            contadorErrores = contadorErrores + 1
            contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & msgProceso, NOMBRE_JOB)
          End If
          'limpia variable para los nuevos email
          ColeccionEmailsPara = Nothing
          ColeccionEmailsCC = Nothing
          msgRetorno = "Se envía listado de alertas día anterior<br />"
        Else
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay alertas para informar", NOMBRE_JOB)
          msgRetorno = "No se pudo generar excel con el listado de alertas<br />"
        End If

        'crea log del proceso
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
        nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
      End If
    Catch ex As Exception
      msgRetorno = ex.Message & "<br />"
    End Try

    Return msgRetorno
  End Function

#End Region

  Sub Main()
    Dim msgProceso As String = ""
    'construye fecha inicio
    Dim ds As New DataSet
    Dim dsConfiguracionXml As New DataSet
    Dim nombreArchivo As String
    Dim contadorErrores As Integer = 0
    Dim contenidoLog As String = ""

    'inicia la ejecucion
    Console.WriteLine("Iniciando tarea: " & NOMBRE_JOB)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug.ToString)

    Try
      'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
      dsConfiguracionXml = Herramientas.LeerXML("configuracion.xml")

      'obtiene listado de planifcaciones
      ds = Sistema.ConsultaGenerica("spu_Job_AlertasDiaAnterior")
      contenidoLog &= EnviarExcelPorEmail(dsConfiguracionXml, ds)
    Catch ex As Exception
      contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & ex.Message, NOMBRE_JOB)
      contadorErrores = 1
    End Try
    Console.WriteLine("Fin tarea: " & NOMBRE_JOB)

    'crea log del proceso
    If (contenidoLog <> "") Then
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
      nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
    End If
  End Sub

End Module
