﻿Imports CapaNegocio
Imports System.Data

Public Class Supervisor_ReporteListado
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    ExportarDatos
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim idReporte As String = Utilidades.IsNull(Request("idReporte"), "-1")
    'verifica los permisos sobre los controles
    VerificarPermisos()

    If (idReporte <> "-1") Then
      Me.ddlReporte.SelectedValue = idReporte
      Utilidades.RegistrarScript(Me.Page, "Reporte.mostrarControles();", Utilidades.eRegistrar.FINAL, "inicializaControles", False)
    End If
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnExportar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.ExportarDatos.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Reporte.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger, True)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Warning, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Reporte.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    MostrarMensajeUsuario()
    oUtilidades.GrabarObjectSession(Reporte.KEY_SESION_REPORTE_GENERADO, "0")
    oUtilidades.GrabarObjectSession(Reporte.KEY_SESION_MENSAJE, Nothing)
    oUtilidades.GrabarObjectSession(Reporte.KEY_SESION_DATASET, Nothing)
  End Sub


End Class