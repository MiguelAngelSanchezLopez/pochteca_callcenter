﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mantenedor.CambiarClave.aspx.vb" Inherits="WebTransportePochteca.Mantenedor_CambiarClave" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Cambiar clave</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/sha1.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtClaveEncriptada" runat="server" value="" />
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Cambiar clave"></asp:Label>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <asp:Literal ID="ltlMensajeCaducidadPassword" runat="server"></asp:Literal>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">INGRESE CLAVE ACTUAL</h3></div>
            <div class="panel-body">
              <div>
                <div class="row">
                  <div class="col-xs-12">
                    <label class="control-label">Clave actual<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtClaveActual" runat="server" MaxLength="255" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    <div id="hErrorClaveActual"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">INGRESE NUEVA CLAVE</h3></div>
            <div class="panel-body">
              <div>
                <div class="row form-group">
                  <div class="col-xs-12">
                    <label class="control-label">Nueva clave<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtNuevaClave" runat="server" MaxLength="255" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    <div id="hErrorNuevaClave"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <label class="control-label">Confirmar nueva clave<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtConfirmarNuevaClave" runat="server" MaxLength="255" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    <div id="hErrorConfirmarNuevaClave"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrarSesion" runat="server" class="btn btn-lg btn-default" visible="false"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar Sesi&oacute;n</div>
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Sistema.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Sistema.validarCambioClave())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
        </div>
      </div>
    </div>

  </form>
</body>
</html>
