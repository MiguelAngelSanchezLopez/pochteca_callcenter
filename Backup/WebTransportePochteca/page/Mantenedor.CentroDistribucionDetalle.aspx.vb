﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_CentroDistribucionDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idCentroDistribucion As String = Me.ViewState.Item("idCentroDistribucion")

    Try
      ds = CentroDistribucion.ObtenerDetalle(idCentroDistribucion)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idCentroDistribucion As String = Me.ViewState.Item("idCentroDistribucion")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim nombre, codigo, activo As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    nombre = ""
    codigo = ""
    activo = "1"

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
          nombre = dr.Item("NombreCentroDistribucion")
          codigo = dr.Item("CodigoInterno")
          activo = dr.Item("Activo")
        End If
      End If
    Catch ex As Exception
      nombre = ""
      codigo = ""
      activo = "1"
    End Try

    'asigna valores
    Me.txtCategoria.Value = CentroDistribucion.NombreEntidad
    Me.txtIdCentroDistribucion.Value = idCentroDistribucion
    Me.txtNombre.Text = nombre
    Me.txtCodigo.Text = codigo
    Me.chkActivo.Checked = IIf(idCentroDistribucion = "-1", True, IIf(activo = "1", True, False))
    Me.lblTituloFormulario.Text = IIf(idCentroDistribucion = "-1", "Nuevo ", "Modificar ") & "Centro de Distribuci&oacute;n"
    Me.lblIdRegistro.Text = IIf(idCentroDistribucion = "-1", "", "[ID: " & idCentroDistribucion & "]")

    'Dibuja los cargos asociados al centro distribucion
    Utilidades.RegistrarScript(Me.Page, "CentroDistribucion.dibujarComboCargos();", Utilidades.eRegistrar.FINAL, False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    Dim idCentroDistribucion As String = Me.ViewState.Item("idCentroDistribucion")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idCentroDistribucion = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idCentroDistribucion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idCentroDistribucion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idCentroDistribucion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nombre, codigo, activo, listadoUsuariosCargos As String

    Try
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "")
      codigo = Utilidades.IsNull(Me.txtCodigo.Text, "")
      activo = IIf(Me.chkActivo.Checked, "1", "0")
      listadoUsuariosCargos = Utilidades.IsNull(Me.txtListadoUsuariosCargos.Value, "")

      If idCentroDistribucion = "-1" Then Sistema.GrabarLogSesion(oUsuario.Id, "Crea nuevo centro de distribución: " & nombre & "- " & codigo) Else Sistema.GrabarLogSesion(oUsuario.Id, "Modifica centro de distribución: " & nombre & "- " & codigo)
      status = CentroDistribucion.GrabarDatos(idCentroDistribucion, nombre, codigo, activo, listadoUsuariosCargos)
      If status = "duplicado" Then idCentroDistribucion = "-1"
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar el centro de distribuci&oacute;n.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idCentroDistribucion As String = Me.ViewState.Item("idCentroDistribucion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosRegistro(status, idCentroDistribucion)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}El CENTRO DE DISTRIBUCION fue ingresado satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}El CENTRO DE DISTRIBUCION fue actualizado satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El Nombre del CENTRO DE DISTRIBUCION ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el CENTRO DE DISTRIBUCION. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el CENTRO DE DISTRIBUCION. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idCentroDistribucion = "-1", "", "[ID: " & idCentroDistribucion & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "CentroDistribucion.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idCentroDistribucion
      Response.Redirect("./Mantenedor.CentroDistribucionDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' elimina los datos del registro
  ''' </summary>
  ''' <returns></returns>
  Private Function EliminarRegistro(ByRef status As String, ByVal idCentroDistribucion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = CentroDistribucion.EliminarDatos(idCentroDistribucion)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina centro de distribución: IdCentroDistribucion " & idCentroDistribucion)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo eliminar el centro de distribuci&oacute;n<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idCentroDistribucion As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idCentroDistribucion", idCentroDistribucion)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idCentroDistribucion As String = Me.ViewState.Item("idCentroDistribucion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= EliminarRegistro(status, idCentroDistribucion)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}El CENTRO DE DISTRIBUCION fue eliminado satisfactoriamente. Complete el formulario para ingresar uno nuevo"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar el CENTRO DE DISTRIBUCION. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar el CENTRO DE DISTRIBUCION. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    queryStringPagina = "id=" & IIf(status = "eliminado", "-1", idCentroDistribucion)
    Response.Redirect("./Mantenedor.CentroDistribucionDetalle.aspx?" & queryStringPagina)

  End Sub

End Class