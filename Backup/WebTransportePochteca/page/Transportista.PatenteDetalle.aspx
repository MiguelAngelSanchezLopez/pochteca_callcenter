﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Transportista.PatenteDetalle.aspx.vb" Inherits="WebTransportePochteca.Transportista_PatenteDetalle" %>

<%@ Register Src="../wuc/wucHora.ascx" TagName="wucHora" TagPrefix="uc2" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Detalle Operador</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/combo.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
  <script type="text/javascript" src="../js/transportista.js"></script>  
</head>
<body>
  <form id="form1" runat="server">
  <input type="hidden" id="txtIdConductor" runat="server" value="-1" />
  <input type="hidden" id="txtJSONDatosPerfil" runat="server" value="[]" />
  <div class="container sist-margin-top-10">
    <div class="form-group text-center">
      <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
      <div>
        <asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
      <asp:Panel ID="pnlMensajeUsuario" runat="server">
      </asp:Panel>
    </div>
    <div class="form-group">
      <asp:Panel ID="pnlMensajeAcceso" runat="server">
      </asp:Panel>
      <asp:Panel ID="pnlContenido" runat="server">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">
              DATOS</h3>
          </div>
           <div class="panel-body">
            <div class="form-group">
              <div class="row">
                <div class="col-xs-2">
                  <label class="control-label">Tipo</label>
                    <select name="select"  id="ddlTipo" runat="server" class="form-control chosen-select">
                      <option value="Chasis">Chasis</option>
                      <option value="Tracto">Tracto</option>
                      <option value="Trailer">Remolque</option>
                    </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-2">
                    <div><label class="control-label">Placa<strong class="text-danger">&nbsp;*</strong></label></div>
                    <div class="col-sm-3 sist-padding-cero">
                      <asp:TextBox ID="txtPlacaPatente1" runat="server" MaxLength="2" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-3 sist-padding-cero">
                      <asp:TextBox ID="txtPlacaPatente2" runat="server" MaxLength="2" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-3 sist-padding-cero">
                      <asp:TextBox ID="txtPlacaPatente3" runat="server" MaxLength="2" CssClass="form-control"></asp:TextBox>                                   
                    </div>
                </div>
              </div>
              <div class="row">
               <div class="col-xs-6">
                  <input id="hPatente" type="hidden" value="" runat="server"/>
                  <div id="divErrorPatente"></div>
                </div>
              </div>
            </div>
             <div class="form-group">
                <div class="row">
                   <div class="col-xs-4">
                     <label class="control-label">Marca<strong class="text-danger">&nbsp;*</strong></label>
                      <asp:TextBox ID="txtMarca" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                    </div>
                  </div>
             </div>
          </div>

        </div>
      </asp:Panel>
    </div>
    <div class="form-group text-center">
      <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Transportista.cerrarPopUpFichaConductor('1')">
        <span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
      <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Transportista.validarFormularioPatente())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
      <asp:LinkButton ID="btnModificar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Transportista.validarFormularioPatente())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</asp:LinkButton>
    </div>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });
    });

  </script>
  </form>
</body>
</html>
