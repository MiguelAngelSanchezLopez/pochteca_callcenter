﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_PaginaDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks>Por VSR, 23/01/2009</remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idPagina As String = Me.ViewState.Item("idPagina")

    Try
      'obtiene detalle del usuario
      ds = Pagina.ObtenerDetalle(idPagina)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPagina As String = Me.ViewState.Item("idPagina")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim titulo, nombreArchivo, nombreCategoria, estado, mostrarEnMenu As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    titulo = ""
    nombreArchivo = ""
    nombreCategoria = ""
    estado = ""
    mostrarEnMenu = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(Pagina.eTabla.Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(Pagina.eTabla.Detalle).Rows(0)
          titulo = dr.Item("Titulo")
          nombreArchivo = dr.Item("NombreArchivo")
          nombreCategoria = dr.Item("NombreCategoria")
          estado = dr.Item("Estado")
          mostrarEnMenu = dr.Item("MostrarEnMenu")
        End If
      End If
    Catch ex As Exception
      titulo = ""
      nombreArchivo = ""
      nombreCategoria = ""
      estado = ""
      mostrarEnMenu = ""
    End Try

    'asigna valores
    Me.txtIdPagina.Value = idPagina
    Me.txtTitulo.Text = titulo
    Me.txtNombreArchivo.Text = nombreArchivo
    Me.txtCategoria.Text = nombreCategoria
    Me.chkEstado.Checked = IIf(idPagina = "-1", True, IIf(estado = "1", True, False))
    Me.chkMostrarEnMenu.Checked = IIf(idPagina = "-1", True, IIf(mostrarEnMenu = "1", True, False))
    Me.lblTituloFormulario.Text = IIf(idPagina = "-1", "Nueva ", "Modificar ") & "P&aacute;gina"
    Me.lblIdRegistro.Text = IIf(idPagina = "-1", "", "[ID: " & idPagina & "]")
    'dibuja una tabla con las funciones de la pagina
    Me.hListadoFunciones.Text = DibujarListadoFunciones(idPagina)

    'actualiza el multiselect
    If Not ds Is Nothing Then
      With Me.msPerfiles
        .DataSourcePadre = ds.Tables(Pagina.eTabla.ListadoPerfilesNoPertenece)
        .DataTextFieldPadre = "Texto"
        .DataValueFieldPadre = "Valor"
        .DataBindPadre()
        .DataSourceHija = ds.Tables(Pagina.eTabla.ListadoPerfilesSiPertenece)
        .DataTextFieldHija = "Texto"
        .DataValueFieldHija = "Valor"
        .DataBindHija()
      End With
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idPagina As String = Me.ViewState.Item("idPagina")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idPagina = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idPagina <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idPagina <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza la pagina en base de datos
  ''' </summary>
  ''' <param name="status"></param>
  ''' <param name="idPagina"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function GrabarDatosPagina(ByRef status As String, ByRef idPagina As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim titulo, nombreArchivo, nombreCategoria, estado, mostrarEnMenu As String
    Dim listadoIdPerfiles As String

    Try
      titulo = Utilidades.IsNull(Me.txtTitulo.Text, "")
      nombreArchivo = Utilidades.IsNull(Me.txtNombreArchivo.Text, "")
      nombreCategoria = Utilidades.IsNull(Me.txtCategoria.Text, "")
      estado = IIf(Me.chkEstado.Checked, "1", "0")
      mostrarEnMenu = IIf(Me.chkMostrarEnMenu.Checked, "1", "0")
      listadoIdPerfiles = Me.msPerfiles.ValueListadoId

      If idPagina = "-1" Then
        status = Pagina.GrabarDatos(idPagina, titulo, nombreArchivo, nombreCategoria, estado, mostrarEnMenu, listadoIdPerfiles)
        If status = "duplicado" Then idPagina = "-1" Else Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva página: " & nombreArchivo)
      Else
        status = Pagina.ModificarDatos(idPagina, titulo, nombreArchivo, nombreCategoria, estado, mostrarEnMenu, listadoIdPerfiles)
        Sistema.GrabarLogSesion(oUsuario.Id, "Modifica página: " & nombreArchivo)
      End If
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la p&aacute;gina.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' elimina los datos de una pagina
  ''' </summary>
  ''' <param name="status"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function EliminarPagina(ByRef status As String, ByVal idPagina As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = Pagina.EliminarDatos(idPagina)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina página: IdPagina " & idPagina)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo eliminar la p&aacute;gina.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba las funciones asociadas a la pagina
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarFunciones(ByVal idPagina As String) As String
    Dim msgError As String = ""
    Dim listadoIdFuncion As String = Request("txtIdFuncionAdd")
    Dim listadoNombre As String = Request("txtNombreAdd")
    Dim listadoLlave As String = Request("txtLlaveAdd")
    Dim listadoOrden As String = Request("txtOrdenAdd")
    Dim totalRegistros, i As Integer
    Dim idFuncion, nombre, llave, orden As String
    Dim status As String

    Try
      If Not listadoIdFuncion Is Nothing Then
        Dim aIdFuncion As String() = listadoIdFuncion.Split(",")
        Dim aNombre As String() = listadoNombre.Split(",")
        Dim aLlave As String() = listadoLlave.Split(",")
        Dim aOrden As String() = listadoOrden.Split(",")
        totalRegistros = aIdFuncion.Length - 1

        '-------------------------------------------------
        'elimina las funciones que no existen. enviar "listadoIdFuncion"
        status = Pagina.EliminarFuncionesNoExistentes(idPagina, listadoIdFuncion)
        If status = Sistema.eCodigoSql.Error Then
          Throw New Exception("No se pudo eliminar las funciones de la pagina")
        End If

        '-------------------------------------------------
        'guarda o actualiza las funciones existentes
        status = ""
        For i = 0 To totalRegistros
          idFuncion = Server.UrlDecode(aIdFuncion(i))
          nombre = Server.UrlDecode(aNombre(i))
          llave = Server.UrlDecode(aLlave(i))
          orden = Server.UrlDecode(aOrden(i))
          'graba las funciones
          If Not String.IsNullOrEmpty(nombre) Then
            status = Pagina.GrabarFuncion(idPagina, idFuncion, nombre, llave, orden)
            If status = Sistema.eCodigoSql.Error Then
              Throw New Exception("No se pudo grabar la función de la página")
            End If
          End If
        Next
      End If
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar las funciones.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos de la pagina
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarPagina()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPagina As String = Me.ViewState.Item("idPagina")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosPagina(status, idPagina)
    textoMensaje &= GrabarFunciones(idPagina)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La PAGINA fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La PAGINA fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El Nombre del Archivo ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la PAGINA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la PAGINA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idPagina = "-1", "", "[ID: " & idPagina & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Pagina.cerrarPopUpPagina('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idPagina
      Response.Redirect("./Mantenedor.PaginaDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' dibuja las funciones de la pagina en una tabla
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarListadoFunciones(ByVal idPagina As String) As String
    Dim html As String = ""
    Dim totalOpcionesDefecto As Integer = 2
    Dim totalRegistros, i, indiceFila As Integer
    Dim dr As DataRow
    Dim dt As DataTable
    Dim dv As DataView
    Dim sb As New StringBuilder
    Dim idFuncion, nombre, llave, orden, disabled, linkEliminar, classInput, idUnicoElemento As String
    Dim usarOpcionDefecto As Boolean
    Dim ds As DataSet = CType(Session("ds"), DataSet)

    Try
      '---------------------------------------------------------------------------------
      'si la pregunta es nueva entonces muestra la tabla con valores iniciales
      '---------------------------------------------------------------------------------
      If idPagina = "-1" Then
        usarOpcionDefecto = True
        totalRegistros = totalOpcionesDefecto
      Else
        If ds Is Nothing Then
          usarOpcionDefecto = True
          totalRegistros = totalOpcionesDefecto
        Else
          'filtra las opciones relacionadas con la pregunta consultada
          dt = ds.Tables(Pagina.eTabla.ListadoFunciones)
          dv = dt.DefaultView
          ds = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          totalRegistros = ds.Tables(0).Rows.Count
          'si no encuentra entonces usa la configuracion por defecto
          If (totalRegistros = 0) Then
            usarOpcionDefecto = True
            totalRegistros = totalOpcionesDefecto
          Else
            usarOpcionDefecto = False
          End If
        End If
      End If

      '---------------------------------------------------------------------------------
      'dibuja la tabla con los controles
      '---------------------------------------------------------------------------------
      sb.AppendLine("<input type=""hidden"" name=""txtTotalFunciones"" id=""txtTotalFunciones"" value=""" & totalRegistros & """ />")
      sb.AppendLine("<table id=""tablaFunciones"" border=""0"" cellpadding=""2"" cellspacing=""0"" width=""100%"" class=""table table-striped table-bordered"">")
      sb.AppendLine("  <tr>")
      sb.AppendLine("    <th>Nombre</th>")
      sb.AppendLine("    <th>Llave</th>")
      sb.AppendLine("    <th style=""width:200px;"">Orden</th>")
      sb.AppendLine("    <th style=""width:80px;"">Eliminar</th>")
      sb.AppendLine("  </tr>")

      'recorre las opciones para dibujarlas
      For i = 0 To totalRegistros - 1
        If usarOpcionDefecto Then
          idFuncion = "-1"
          nombre = IIf(i = 0, "Ver", "")
          llave = IIf(i = 0, "Ver", "")
          orden = IIf(i = 0, "1", "")
        Else
          dr = ds.Tables(0).Rows(i)
          idFuncion = dr.Item("IdFuncion")
          nombre = dr.Item("Nombre")
          llave = dr.Item("LlaveIdentificador")
          orden = dr.Item("Orden")
        End If
        indiceFila = i + 1
        disabled = IIf(nombre.ToUpper = "VER", "disabled", "")
        linkEliminar = IIf(Not String.IsNullOrEmpty(disabled), "&nbsp;", "<button type=""button"" class=""btn btn-default"" onclick=""Pagina.eliminarFuncion('" & idFuncion & "','tablaFunciones',this.parentNode.parentNode.rowIndex,'txtTotalFunciones');""><span class=""glyphicon glyphicon-trash""></span></button>&nbsp;")
        classInput = "form-control" & IIf(Not String.IsNullOrEmpty(disabled), " css-funcion-ver", "")
        idUnicoElemento = IIf(idFuncion = "-1", Archivo.CrearNombreUnicoArchivo("", False) & i, idFuncion)

        sb.AppendLine("<tr>")
        sb.AppendLine("  <td>")
        sb.AppendLine("    <input type=""hidden"" id=""txtIdFuncionAdd_" & idUnicoElemento & """ name=""txtIdFuncionAdd"" value=""" & idFuncion & """ />")
        sb.AppendLine("    <input type=""text"" id=""txtNombreAdd_" & idUnicoElemento & """ name=""txtNombreAdd"" value=""" & Server.HtmlEncode(nombre) & """ class=""" & classInput & """ maxlength=""100"" " & disabled & " />")
        sb.AppendLine("    <span id=""hErrorNombreAdd_" & idUnicoElemento & """></span>")
        sb.AppendLine("  </td>")
        sb.AppendLine("  <td><input type=""text"" id=""txtLlaveAdd_" & idUnicoElemento & """ name=""txtLlaveAdd"" value=""" & Server.HtmlEncode(llave) & """ class=""" & classInput & """ maxlength=""100"" " & disabled & " /><span id=""hErrorLlaveAdd_" & idUnicoElemento & """></span></td>")
        sb.AppendLine("  <td><input type=""text"" id=""txtOrdenAdd_" & idUnicoElemento & """ name=""txtOrdenAdd"" value=""" & Server.HtmlEncode(orden) & """ class=""" & classInput & """ maxlength=""3"" " & disabled & " /><span id=""hErrorOrdenAdd_" & idUnicoElemento & """></span></td>")
        sb.AppendLine("  <td>" & linkEliminar & "</td>")
        sb.AppendLine("</tr>")
      Next

      sb.AppendLine("</table>")
      sb.AppendLine("<div id=""hBotonAgregarFuncion"" class=""form-group text-right"">")
      sb.AppendLine("  <button type=""button"" class=""btn btn-success"" onclick=""Pagina.addFilaFuncion()""><span class=""glyphicon glyphicon-plus""></span>&nbsp;Nueva Funci&oacute;n</button>")
      sb.AppendLine("</div>")

      'retorna el texto
      html = sb.ToString
    Catch ex As Exception
      html = Sistema.eCodigoSql.Error
    End Try
    Return html
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPagina As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idPagina", idPagina)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      ActualizaInterfaz()
      InicializaControles(Page.IsPostBack)
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarPagina()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarPagina()
  End Sub

  Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPagina As String = Request("id")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= EliminarPagina(status, idPagina)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}La PAGINA fue eliminada satisfactoriamente. Complete el formulario para ingresar una nueva"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la PAGINA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la PAGINA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    queryStringPagina = "id=" & IIf(status = "eliminado", "-1", idPagina)
    Response.Redirect("./Mantenedor.PaginaDetalle.aspx?" & queryStringPagina)

  End Sub

End Class