﻿Imports CapaNegocio
Imports System.Data

Public Class Common_AsociarGrupo
  Inherits System.Web.UI.Page

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim prefijoControl As String = Me.ViewState.Item("prefijoControl")
    Dim idEscalamientoPorAlerta As String = Me.ViewState.Item("idEscalamientoPorAlerta")
    Dim idEscalamientoPorAlertaGrupoContacto As String = Me.ViewState.Item("idEscalamientoPorAlertaGrupoContacto")
    Dim idGrupoContactoEscalamientoAnterior As String = Me.ViewState.Item("idGrupoContactoEscalamientoAnterior")
    Dim invocadoDesde As String = Me.ViewState.Item("invocadoDesde")
    Dim nroEscalamiento As String = Me.ViewState.Item("nroEscalamiento")
    Dim idGrupoContactoPadre As String = Me.ViewState.Item("idGrupoContactoPadre")
    Dim sbScript As New StringBuilder

    Select Case invocadoDesde
      Case "AsociarTieneDependencia"
        sbScript.Append("Alerta.dibujarGruposEscalamientoAnterior({ " & _
                        "prefijoControl: '" & prefijoControl & "'" & _
                        ", idEscalamientoPorAlerta: '" & idEscalamientoPorAlerta & "'" & _
                        ", idEscalamientoPorAlertaGrupoContacto: '" & idEscalamientoPorAlertaGrupoContacto & "'" & _
                        ", idGrupoContactoEscalamientoAnterior: '" & idGrupoContactoEscalamientoAnterior & "'" & _
                        "})")
      Case "AsociarGrupoPadre"
        sbScript.Append("Alerta.dibujarGruposPadre({ " & _
                        "prefijoControl: '" & prefijoControl & "'" & _
                        ", idEscalamientoPorAlerta: '" & idEscalamientoPorAlerta & "'" & _
                        ", idEscalamientoPorAlertaGrupoContacto: '" & idEscalamientoPorAlertaGrupoContacto & "'" & _
                        ", nroEscalamiento: '" & nroEscalamiento & "'" & _
                        ", idGrupoContactoPadre: '" & idGrupoContactoPadre & "'" & _
                        "})")
    End Select

    If (Not String.IsNullOrEmpty(sbScript.ToString())) Then
      Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "scriptActualizaInterfaz", False)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim prefijoControl As String = Utilidades.IsNull(Request("prefijoControl"), "")
    Dim idEscalamientoPorAlerta As String = Utilidades.IsNull(Request("idEscalamientoPorAlerta"), "-1")
    Dim idEscalamientoPorAlertaGrupoContacto As String = Utilidades.IsNull(Request("idEscalamientoPorAlertaGrupoContacto"), "-1")
    Dim idGrupoContactoEscalamientoAnterior As String = Utilidades.IsNull(Request("idGrupoContactoEscalamientoAnterior"), "-1")
    Dim invocadoDesde As String = Utilidades.IsNull(Request("invocadoDesde"), "")
    Dim nroEscalamiento As String = Utilidades.IsNull(Request("nroEscalamiento"), "")
    Dim idGrupoContactoPadre As String = Utilidades.IsNull(Request("idGrupoContactoPadre"), "-1")

    Me.ViewState.Add("prefijoControl", prefijoControl)
    Me.ViewState.Add("idEscalamientoPorAlerta", idEscalamientoPorAlerta)
    Me.ViewState.Add("idEscalamientoPorAlertaGrupoContacto", idEscalamientoPorAlertaGrupoContacto)
    Me.ViewState.Add("idGrupoContactoEscalamientoAnterior", idGrupoContactoEscalamientoAnterior)
    Me.ViewState.Add("invocadoDesde", invocadoDesde)
    Me.ViewState.Add("nroEscalamiento", nroEscalamiento)
    Me.ViewState.Add("idGrupoContactoPadre", idGrupoContactoPadre)

    If Not Page.IsPostBack Then
      ActualizaInterfaz()
    End If
  End Sub

End Class