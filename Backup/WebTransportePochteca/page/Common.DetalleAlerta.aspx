﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Common.DetalleAlerta.aspx.vb" Inherits="WebTransportePochteca.Common_DetalleAlerta" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Alerta</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <asp:Panel ID="pnlMensajeAlertaSinAtender" runat="server">
        <div class="navbar-fixed-top sist-height-mensaje-alerta-sin-atender">
          <em>
            <div class="alert alert-danger sist-padding-5">
              <div class="row">
                <div class="col-xs-4">
                  <div class="sist-font-size-24"><img src="../img/warning.png" alt="" /><strong><asp:Label ID="lblSinAtenderClasificacion" runat="server"></asp:Label></strong></div>
                </div>
                <div class="col-xs-8">
                  <div class="row">
                    <div class="col-xs-1">&nbsp;</div>
                    <div class="col-xs-10">
                      <div class="row sist-padding-top-8">
                        <div class="col-xs-4">
                          <label class="control-label">Asignado a</label>
                          <div><asp:Label ID="lblSinAtenderAsignado" runat="server"></asp:Label></div>
                        </div>
                        <div class="col-xs-3">
                          <label class="control-label">Fecha recepci&oacute;n</label>
                          <div><asp:Label ID="lblSinAtenderFecha" runat="server"></asp:Label></div>
                        </div>
                        <div class="col-xs-3">
                          <label class="control-label">Tiempo sin atender</label>
                          <div id="hCronometro" runat="server">00:00:00</div>
                        </div>
                        <!-- *** LLAMADA: DESCOMENTAR CUANDO SE INTEGRE EL SISTEMA CON UNA APLICACION DE LLAMADAS DE CALLCENTER -->
                        <div class="col-xs-2 sist-display-none">
                          <div id="btnVerHistorialLlamadas" runat="server" class="btn btn-lg btn-danger show-tooltip" data-toggle="tooltip" data-placement="bottom" title="muestra el historial de llamadas"><span class="glyphicon glyphicon-phone-alt"></span>&nbsp;Historial</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-1">&nbsp;</div>
                  </div>
                </div>
              </div>
            </div>
          </em>
        </div>
        <div class="sist-height-mensaje-alerta-sin-atender">&nbsp;</div>
      </asp:Panel>

      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Font-Bold="true"></asp:Label>
        <div><asp:Label ID="lblTituloNroTransporte" runat="server" CssClass="h4" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><strong>DETALLE ALERTA</strong></h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="form-group row">
                  <div class="col-xs-2">
                    <label class="control-label">Formato</label>
                    <div><asp:Label ID="lblFormato" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">IdMaster</label>
                    <div><asp:Label ID="lblNroTransporte" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Prioridad</label>
                    <div><asp:Label ID="lblPrioridad" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Tienda Destino</label>
                    <div><asp:Label ID="lblLocalDestino" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Operador</label>
                    <div><asp:Label ID="lblConductor" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">L&iacute;nea de Transporte</label>
                    <div><asp:Label ID="lblTransportista" runat="server"></asp:Label></div>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-xs-2">
                    <label class="control-label">Origen</label>
                    <div><asp:Label ID="lblOrigen" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Placa Tracto</label>
                    <div><asp:Label ID="lblPatenteTracto" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Placa Remolque</label>
                    <div><asp:Label ID="lblPatenteTrailer" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Tipo Viaje</label>
                    <div><asp:Label ID="lblTipoViaje" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Lat Tracto</label>
                    <div><asp:Label ID="lblLatTracto" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Lon Tracto</label>
                    <div><asp:Label ID="lblLonTracto" runat="server"></asp:Label></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-2">
                    <label class="control-label">Fecha Creaci&oacute;n</label>
                    <div><asp:Label ID="lblFechaCreacion" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Tipo Alerta</label>
                    <div><asp:Label ID="lblTipoAlerta" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Autorizado</label>
                    <div><asp:Label ID="lblPermiso" runat="server"></asp:Label></div>
                  </div>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><strong>HISTORIAL ESCALAMIENTOS</strong></h3></div>
            <div class="panel-body">
              <asp:Literal ID="hHistorialEscalamientos" runat="server"></asp:Literal>
            </div>
          </div> 

        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".show-tooltip").tooltip();
      });
    </script>

  </form>
</body>
</html>
