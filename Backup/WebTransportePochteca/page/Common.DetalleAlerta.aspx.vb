﻿Imports CapaNegocio
Imports System.Data

Public Class Common_DetalleAlerta
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks>Por VSR, 23/01/2009</remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idAlerta As String = Me.ViewState.Item("idAlerta")

    Try
      'obtiene detalle del usuario
      ds = Alerta.ObtenerDetalle(idAlerta)
    Catch ex As Exception
      ds = Nothing
    End Try
    Me.ViewState.Add("ds", ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim ds As DataSet = CType(Me.ViewState.Item("ds"), DataSet)
    Dim nombreAlerta, nroTransporte, prioridad, localOrigenDescripcion, nombreConductor, nombreTransportista, tipoAlerta, nombreFormato As String
    Dim localDestinoDescripcion, localDestinoCodigo, patenteTracto, patenteTrailer, velocidad, tipoViaje, latTracto, lonTracto, fechaHoraCreacion, permiso As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder
    Dim idHistorialEscalamientoConsultado As String = Me.ViewState.Item("idHistorialEscalamiento")
    Dim sinAtender As String = Me.ViewState.Item("sinAtender")
    Dim asignadoA As String = Me.ViewState.Item("asignadoA")
    Dim fechaRecepcion As String = Me.ViewState.Item("fechaRecepcion")
    Dim clasificacion As String = Me.ViewState.Item("clasificacion")
    Dim idAlerta As String = Me.ViewState.Item("idAlerta")

    'inicializa controles en vacio
    nombreAlerta = ""
    nroTransporte = ""
    prioridad = ""
    localOrigenDescripcion = ""
    nombreConductor = ""
    nombreTransportista = ""
    localDestinoDescripcion = ""
    localDestinoCodigo = ""
    patenteTracto = ""
    patenteTrailer = ""
    velocidad = ""
    tipoViaje = ""
    latTracto = ""
    lonTracto = ""
    fechaHoraCreacion = ""
    permiso = ""
    tipoAlerta = ""
    nombreFormato = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(Alerta.eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(Alerta.eTabla.T00_Detalle).Rows(0)
          nombreAlerta = dr.Item("NombreAlerta")
          nroTransporte = dr.Item("NroTransporte")
          prioridad = dr.Item("Prioridad")
          localOrigenDescripcion = dr.Item("LocalOrigenDescripcion")
          nombreConductor = dr.Item("NombreConductor")
          nombreTransportista = dr.Item("NombreTransportista")
          localDestinoDescripcion = dr.Item("LocalDestinoDescripcion")
          localDestinoCodigo = dr.Item("LocalDestinoCodigo")
          patenteTracto = dr.Item("PatenteTracto")
          patenteTrailer = dr.Item("PatenteTrailer")
          velocidad = dr.Item("Velocidad")
          tipoViaje = dr.Item("TipoViaje")
          latTracto = dr.Item("LatTracto")
          lonTracto = dr.Item("LonTracto")
          fechaHoraCreacion = dr.Item("FechaHoraCreacion")
          permiso = dr.Item("Permiso")
          tipoAlerta = dr.Item("TipoAlerta")
          nombreFormato = dr.Item("NombreFormato")
        End If
      End If
    Catch ex As Exception
      nombreAlerta = ""
      nroTransporte = ""
      prioridad = ""
      localOrigenDescripcion = ""
      nombreConductor = ""
      nombreTransportista = ""
      localDestinoDescripcion = ""
      localDestinoCodigo = ""
      patenteTracto = ""
      patenteTrailer = ""
      velocidad = ""
      tipoViaje = ""
      latTracto = ""
      lonTracto = ""
      fechaHoraCreacion = ""
      permiso = ""
      tipoAlerta = ""
      nombreFormato = ""
    End Try

    'asigna valores
    prioridad = Utilidades.ObtenerLabelPrioridad(prioridad)
    Me.lblTituloFormulario.Text = nombreFormato & " - " & nombreAlerta
    Me.lblTituloNroTransporte.Text = "[IdMaster: " & nroTransporte & "]"
    Me.lblFormato.Text = nombreFormato
    Me.lblNroTransporte.Text = nroTransporte
    Me.lblPrioridad.Text = prioridad
    Me.lblOrigen.Text = localOrigenDescripcion
    Me.lblConductor.Text = nombreConductor
    Me.lblTransportista.Text = nombreTransportista
    Me.lblSinAtenderAsignado.Text = asignadoA
    Me.lblSinAtenderFecha.Text = fechaRecepcion
    Me.pnlMensajeAlertaSinAtender.Visible = IIf(sinAtender = "1", True, False)
    Me.lblSinAtenderClasificacion.Text = clasificacion.ToUpper()
    Me.btnVerHistorialLlamadas.Visible = IIf(clasificacion.ToUpper() = "SIENDO ATENDIDA", True, False)
    Me.lblLocalDestino.Text = localDestinoDescripcion & " (Cód. " & localDestinoCodigo & ")"
    Me.lblPatenteTracto.Text = patenteTracto
    Me.lblPatenteTrailer.Text = patenteTrailer
    Me.lblTipoViaje.Text = tipoViaje
    Me.lblLatTracto.Text = latTracto
    Me.lblLonTracto.Text = lonTracto
    Me.lblFechaCreacion.Text = fechaHoraCreacion
    Me.lblTipoAlerta.Text = tipoAlerta
    Me.lblPermiso.Text = Utilidades.ObtenerLabelPermisoZona(permiso)

    'dibuja una tabla con las funciones de la pagina
    Me.hHistorialEscalamientos.Text = DibujarHistorialEscalamiento(ds)

    If (idHistorialEscalamientoConsultado <> "-1") Then
      sbScript.Append("Sistema.moverScrollHastaControl('hIdHistorialEscalamiento_" & idHistorialEscalamientoConsultado & "');")
    End If

    If (sinAtender = "1") Then
      sbScript.Append("Alerta.mostrarCronometroEnDetalleAlerta({ fechaRecepcion: '" & fechaRecepcion & "', elemContenedor:'" & hCronometro.ClientID & "'});")
    End If

    If (Not String.IsNullOrEmpty(sbScript.ToString())) Then
      Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "script_ActualizaInterfaz", False)
    End If

    Sistema.GrabarLogSesion(oUsuario.Id, "Visualiza detalle alerta: IdAlerta " & idAlerta)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim idAlerta As String = Me.ViewState.Item("idAlerta")

    Me.btnVerHistorialLlamadas.Attributes.Add("onclick", "Alerta.verHistorialLlamadas({ idAlerta: '" & idAlerta & "', nroEscalamiento: '-1', fancy_width: '90%', fancy_height: '80%', fancy_overlayColor: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL })")

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = True 'Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' dibuja las funciones de la pagina en una tabla
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarHistorialEscalamiento(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim sbEscalamiento As New StringBuilder
    Dim template, templateAux, idHistorialEscalamiento, nroEscalamiento, fecha, nombreUsuario, cargo, telefono, explicacion As String
    Dim explicacionContacto, observacion, observacionContacto, styleFila, agrupadoEn, tituloNroEscalamiento, atendidoPor, botonVerHistorialLlamadas As String
    Dim eliminado, claveNroEscalamiento As String
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim htmlSinRegistro As String = "<em class=""help-block"">No hay escalamientos asociados</em>"
    Dim idHistorialEscalamientoConsultado As String = Me.ViewState.Item("idHistorialEscalamiento")
    Dim idAlerta As String = Me.ViewState.Item("idAlerta")

    Try
      template = "<tr {STYLE_FILA}>" & _
                 "  <td>{FECHA}</td>" & _
                 "  <td>{AGRUPADO_EN}</td>" & _
                 "  <td>{NOMBRE_USUARIO}</td>" & _
                 "  <td>{ATENDIDO_POR}</td>" & _
                 "  <td>{EXPLICACION}</td>" & _
                 "  <td>{OBSERVACION}</td>" & _
                 "</tr>"

      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        sb.Append(htmlSinRegistro)
      Else
        For Each dr In dt.Rows
          sbEscalamiento = New StringBuilder
          idHistorialEscalamiento = dr.Item("IdHistorialEscalamiento")
          nroEscalamiento = dr.Item("NroEscalamiento")
          fecha = dr.Item("Fecha")
          explicacion = dr.Item("Explicacion")
          observacion = dr.Item("Observacion")
          atendidoPor = dr.Item("AtendidoPor")
          eliminado = dr.Item("Eliminado")
          claveNroEscalamiento = dr.Item("claveNroEscalamiento")

          tituloNroEscalamiento = nroEscalamiento & IIf(eliminado = "1", "&nbsp;<span class=""text-danger"">(eliminado)</span>", "")
          tituloNroEscalamiento = "Escalamiento " & IIf(nroEscalamiento.ToUpper = Alerta.SUPERVISOR, nroEscalamiento, "NRO. " & tituloNroEscalamiento) & ":"
          styleFila = IIf(idHistorialEscalamiento = idHistorialEscalamientoConsultado, "class=""success""", "")

          '*** LLAMADA: DESCOMENTAR CUANDO SE INTEGRE EL SISTEMA CON UNA APLICACION DE LLAMADAS DE CALLCENTER
          'botonVerHistorialLlamadas = IIf(nroEscalamiento.ToUpper = Alerta.SUPERVISOR, "", "&nbsp;<span class=""btn btn-sm btn-success"" onclick=""Alerta.verHistorialLlamadas({ idAlerta: '" & idAlerta & "', nroEscalamiento: '" & claveNroEscalamiento & "', fancy_width: '90%', fancy_height: '80%', fancy_overlayColor: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL })""><span class=""glyphicon glyphicon-phone-alt""></span>&nbsp;Ver historial de llamadas</span>")
          botonVerHistorialLlamadas = ""

          sbEscalamiento.Append("<div id=""hIdHistorialEscalamiento_" & idHistorialEscalamiento & """>")
          sbEscalamiento.Append("<p class=""h4""><strong>" & tituloNroEscalamiento & "</strong>" & botonVerHistorialLlamadas & "</p>")
          sbEscalamiento.Append("<div class=""table-responsive"">")
          sbEscalamiento.Append("  <table class=""table table-striped table-condensed sist-width-100-porciento"">")
          sbEscalamiento.Append("    <thead>")
          sbEscalamiento.Append("      <tr>")
          sbEscalamiento.Append("        <th style=""width:180px;"">Fecha</th>")
          sbEscalamiento.Append("        <th style=""width:180px;"">Grupo</th>")
          sbEscalamiento.Append("        <th style=""width:250px;"">Contacto</th>")
          sbEscalamiento.Append("        <th style=""width:150px;"">Atendido Por</th>")
          sbEscalamiento.Append("        <th style=""width:200px;"">Explicaci&oacute;n</th>")
          sbEscalamiento.Append("        <th>Observaci&oacute;n</th>")
          sbEscalamiento.Append("      </tr>")
          sbEscalamiento.Append("    </thead>")
          sbEscalamiento.Append("    <tbody>")

          '---------------------------------------------------
          'obtiene los contactos por escalamiento
          dv = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos).DefaultView
          dv.RowFilter = "IdHistorialEscalamiento = '" & idHistorialEscalamiento & "'"
          dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          For Each drContacto As DataRow In dsFiltrado.Tables(0).Rows
            templateAux = template
            nombreUsuario = drContacto.Item("NombreUsuario")
            cargo = drContacto.Item("Cargo")
            telefono = drContacto.Item("Telefono")
            explicacionContacto = drContacto.Item("Explicacion")
            observacionContacto = drContacto.Item("Observacion")
            agrupadoEn = drContacto.Item("agrupadoEn")

            explicacionContacto = IIf(String.IsNullOrEmpty(explicacionContacto), explicacion, explicacionContacto)
            explicacionContacto = IIf(String.IsNullOrEmpty(explicacionContacto), "&nbsp;", Server.HtmlEncode(explicacionContacto))

            nombreUsuario = "<strong>" & nombreUsuario & "</strong>"
            nombreUsuario &= IIf(String.IsNullOrEmpty(cargo), "", "<div class=""sist-font-size-11""><em>" & Server.HtmlEncode(cargo) & "</em></div>")
            nombreUsuario &= IIf(String.IsNullOrEmpty(telefono), "", "<div class=""sist-font-size-11""><em>" & Server.HtmlEncode(telefono) & "</em></div>")

            observacionContacto = IIf(String.IsNullOrEmpty(observacionContacto), observacion, observacionContacto)
            observacionContacto = IIf(String.IsNullOrEmpty(observacionContacto), "&nbsp;", Server.HtmlEncode(observacionContacto))

            'remplaza marcas
            templateAux = templateAux.Replace("{FECHA}", fecha)
            templateAux = templateAux.Replace("{NOMBRE_USUARIO}", nombreUsuario)
            templateAux = templateAux.Replace("{EXPLICACION}", explicacionContacto)
            templateAux = templateAux.Replace("{OBSERVACION}", observacionContacto)
            templateAux = templateAux.Replace("{STYLE_FILA}", styleFila)
            templateAux = templateAux.Replace("{AGRUPADO_EN}", agrupadoEn.ToUpper())
            templateAux = templateAux.Replace("{ATENDIDO_POR}", atendidoPor)
            sbEscalamiento.Append(templateAux)
          Next
          '---------------------------------------------------

          sbEscalamiento.Append("    </tbody>")
          sbEscalamiento.Append("  </table>")
          sbEscalamiento.Append("</div>")
          sbEscalamiento.Append("</div>")
          sbEscalamiento.Append("<hr />")

          sb.Append(sbEscalamiento.ToString())
        Next
      End If

      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlerta As String = Utilidades.IsNull(Request("idAlerta"), "-1")
    Dim idHistorialEscalamiento As String = Utilidades.IsNull(Request("idHistorialEscalamiento"), "-1")
    Dim sinAtender As String = Utilidades.IsNull(Request("sinAtender"), "0")
    Dim asignadoA As String = Utilidades.IsNull(Server.UrlDecode(Request("asignadoA")), "")
    Dim fechaRecepcion As String = Utilidades.IsNull(Server.UrlDecode(Request("fechaRecepcion")), "")
    Dim clasificacion As String = Utilidades.IsNull(Server.UrlDecode(Request("clasificacion")), "")

    Me.ViewState.Add("idAlerta", idAlerta)
    Me.ViewState.Add("idHistorialEscalamiento", idHistorialEscalamiento)
    Me.ViewState.Add("sinAtender", sinAtender)
    Me.ViewState.Add("asignadoA", asignadoA)
    Me.ViewState.Add("fechaRecepcion", fechaRecepcion)
    Me.ViewState.Add("clasificacion", clasificacion)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      ActualizaInterfaz()
      InicializaControles(Page.IsPostBack)
    End If
  End Sub

End Class