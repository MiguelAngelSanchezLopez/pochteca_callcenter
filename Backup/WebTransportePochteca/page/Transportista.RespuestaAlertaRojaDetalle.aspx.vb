﻿Imports CapaNegocio
Imports System.Data

Public Class Transportista_RespuestaAlertaRojaDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Public Const MAXIMO_ARCHIVOS_ADJUNTOS As Integer = 5

  Private Enum eFunciones
    Ver
    Grabar
  End Enum

  Enum eTabla As Integer
    T00_ListadoAlertas = 0
    T01_ListadoCategoriasPorAlerta = 1
    T02_RespuestaTransportista = 2
    T03_Discrepancia = 3
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks>Por VSR, 23/01/2009</remarks>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim ds As New DataSet
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim rut As String = oUsuario.RutCompleto

    Try
      ds = Transportista.ObtenerDetalleAlertaRojaTransportista(rut, nroTransporte)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim html = ""
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim totalRegistros As Integer = 0
    Dim dt As DataTable
    Dim dr As DataRow
    Dim htmlSinRegistro As String = "<span class=""cssTextoNormal""><i><center>No tiene alertas asociadas</center></i></span>"
    Dim montoDiscrepancia As String

    montoDiscrepancia = 0

    Try
      If ds Is Nothing Then
        totalRegistros = 0
      Else
        dt = ds.Tables(eTabla.T00_ListadoAlertas)
        totalRegistros = dt.Rows.Count

        dr = ds.Tables(eTabla.T03_Discrepancia).Rows(0)
        montoDiscrepancia = dr.Item("MontoDiscrepancia")
      End If

      If (totalRegistros = 0) Then
        html = htmlSinRegistro
      Else
        html = Me.ConstruirListadoAlertas(ds)
      End If
    Catch ex As Exception
      html = ""
    End Try

    'asigna valores
    Me.lblTituloFormulario.Text = "Comentar Alertas Rojas"
    Me.lblIdRegistro.Text = IIf(nroTransporte = "-1", "", "[IdMaster: " & nroTransporte & "&nbsp;&nbsp;/&nbsp;&nbsp;Monto Discrepancia: $" & montoDiscrepancia & "]")
    Me.ltlListado.Text = html
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub


  ''' <summary>
  ''' graba contenido del archivo en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabaContenidoFileUpload(ByVal postedFile As HttpPostedFile, ByVal idAlertaRojaRespuesta As String, ByVal idUsuarioTransportista As String) As String
    Dim msgError As String = ""
    Dim idArchivo As Integer
    Dim nombreArchivo, ruta, extension, ContentType As String
    Dim oUtilidades As New Utilidades
    Dim tamano, orden As Integer
    Dim maxTamanoArchivoEnKb As Integer = Archivo.TamanoMaximoUploadEnKb()
    Dim tamanoEnKb As Integer
    Dim oRedimension As New Archivo.Redimension
    Dim anchoMax, altoMax As Integer
    Dim dicArchivo As New Dictionary(Of String, Integer)

    Try
      tamano = postedFile.ContentLength
      tamanoEnKb = (tamano / 1024)
      anchoMax = CType(Utilidades.AnchoMaximoImagen(), Integer)
      altoMax = CType(Utilidades.AltoMaximoImagen(), Integer)

      If (tamano > 0) And (tamanoEnKb <= maxTamanoArchivoEnKb) Then
        ruta = postedFile.FileName
        nombreArchivo = Archivo.ObtenerNombreArchivoDesdeRuta(postedFile.FileName)
        extension = Archivo.ObtenerExtension(nombreArchivo)
        ContentType = postedFile.ContentType
        orden = 1

        Dim Contenido(tamano) As Byte
        'lee el archivo en el arreglo de arreglo de Byte Contenido
        postedFile.InputStream.Read(Contenido, 0, tamano)

        If Archivo.EsTipoArchivo(ContentType) = Archivo.eTipoArchivo.image Then
          'asigna valores originales
          With oRedimension
            .Contenido = Contenido
            .ContentType = ContentType
            .Extension = extension
            .NombreArchivo = nombreArchivo
            .Tamano = tamano
          End With
          Contenido = Archivo.RedimensionarImagen2Bytes(Contenido, anchoMax, altoMax, False, oRedimension)
          'asigna valores despues de la redimension
          With oRedimension
            tamano = .Tamano
            nombreArchivo = .NombreArchivo
            extension = .Extension
            ContentType = .ContentType
          End With
        End If

        'guarda archivo
        idArchivo = Archivo.GrabarArchivoEnBD(idUsuarioTransportista, nombreArchivo, ruta, tamano, extension, ContentType, Contenido, orden)

        If idArchivo <> -200 Then
          Archivo.AsociarIdArchivoConIdRegistro(Alerta.ENTIDAD_ALERTA_ROJA_RESPUESTA, idAlertaRojaRespuesta, idArchivo)
        End If
      End If
    Catch ex As Exception
      idArchivo = -200
    End Try

    'si hay error entonces retorna string
    If idArchivo = -200 Then
      msgError = "Error al grabar el contenido del archivo"
    End If

    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim idAlertaRojaEstadoActual As String = Me.ViewState.Item("idAlertaRojaEstadoActual")
    Dim objJSON, row As Object
    Dim json, idAlerta, observacion, idAlertaRojaRespuesta, statusGrabar, estadoAlertaRoja As String
    Dim postedfile As HttpPostedFile
    Dim ds As New DataSet
    Dim totalCertificaciones As Integer

    Try
      json = Me.txtJSON.Value
      objJSON = MyJSON.ConvertJSONToObject(json)

      For Each row In objJSON
        idAlerta = MyJSON.ItemObject(row, "IdAlerta")
        observacion = MyJSON.ItemObject(row, "Observacion")
        idAlertaRojaRespuesta = Alerta.GrabarRespuestaAlertaRoja(oUsuario.Id, nroTransporte, idAlerta, observacion, False)

        If (idAlertaRojaRespuesta = "-200") Then
          status = "error"
          Exit For
        Else
          status = "ingresado"

          'graba los archivos adjuntos
          For i = 1 To MAXIMO_ARCHIVOS_ADJUNTOS
            postedfile = Request.Files(idAlerta & "_flArchivo_" & i)
            statusGrabar = GrabaContenidoFileUpload(postedfile, idAlertaRojaRespuesta, oUsuario.Id)
          Next

        End If
      Next

      'buscar si el viaje tiene certificacion
      ds = Alerta.TieneCertificacion(nroTransporte)
      If (ds Is Nothing) Then
        totalCertificaciones = 0
      Else
        totalCertificaciones = ds.Tables(0).Rows.Count
      End If

      'graba el estado de la alerta roja
      If (status <> "error") Then
        'si el viaje tiene certificaciones entonces el viaje queda en estado "POR APROBAR PLAN ACCION"
        estadoAlertaRoja = IIf(totalCertificaciones > 0, Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION, Alerta.ESTADO_ALERTA_ROJA_ENVIADO_OPERACION_TRANSPORTE)

        statusGrabar = Alerta.GrabarEstadoAlertaRoja(idAlertaRojaEstadoActual, oUsuario.Id, nroTransporte, estadoAlertaRoja)
        If (statusGrabar = Sistema.eCodigoSql.Error) Then Throw New Exception("Error al grabar estado alerta roja")

        If (estadoAlertaRoja = Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION) Then
          statusGrabar = Alerta.GrabarEstadoTransportista(idAlertaRojaEstadoActual, oUsuario.RutCompleto, "", "", -1, -1, estadoAlertaRoja)
          If (statusGrabar = Sistema.eCodigoSql.Error) Then Throw New Exception("Error al grabar estado de la Línea de Transporte")
        End If
      End If
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la respuesta de la alerta roja.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim idAlertaRojaEstadoActual As String = Me.ViewState.Item("idAlertaRojaEstadoActual")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosRegistro(status)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}Los registros fueron ingresados satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}Los registros fueron actualizados satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo grabar los comentarios de las alertas. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo grabar los comentarios de las alertas. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Transportista.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "nroTransporte=" & nroTransporte
      queryStringPagina = "&idAlertaRojaEstadoActual=" & idAlertaRojaEstadoActual
      Response.Redirect("./Transportista.RespuestaAlertaRojaDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' Construye tabla HTML con las alertas
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <remarks></remarks>
  Private Function ConstruirListadoAlertas(ByVal ds As DataSet) As String
    Dim oUtilidades As New Utilidades
    Dim html As String = ""
    Dim dt As DataTable
    Dim dr As DataRow
    Dim totalRegistros, i As Integer
    Dim idAlerta, nombreAlerta, nombreConductor, patenteTracto, patenteTrailer, localOrigenCodigo, localOrigenDescripcion, localDestinoCodigo As String
    Dim localDestinoDescripcion, tipoViaje, fechaHoraCreacion, botonResponder, patente, alertaMapa As String
    Dim sb As New StringBuilder
    Try
      dt = ds.Tables(eTabla.T00_ListadoAlertas)
      totalRegistros = dt.Rows.Count

      sb.AppendLine("<div>")
      sb.AppendLine("  <table class=""table table-condensed"">")
      sb.AppendLine("    <thead>")
      sb.AppendLine("      <tr>")
      sb.AppendLine("     <th style=""width:90px"">IdAlerta</th>")
      sb.AppendLine("     <th>Alerta</th>")
      sb.AppendLine("     <th>Fecha Alerta</th>")
      sb.AppendLine("     <th>Operador</th>")
      sb.AppendLine("     <th>Placa</th>")
      sb.AppendLine("     <th>Tienda Origen</th>")
      sb.AppendLine("     <th>Tienda Destino</th>")
      sb.AppendLine("     <th>Tipo Viaje</th>")
      sb.AppendLine("     <th style=""width:120px"">Opciones</th>")
      sb.AppendLine("      </tr>")
      sb.AppendLine("    </thead>")
      sb.AppendLine("    <tbody>")

      For i = 0 To totalRegistros - 1
        dr = dt.Rows(i)
        idAlerta = dr.Item("idAlerta")
        nombreAlerta = dr.Item("nombreAlerta")
        nombreConductor = dr.Item("nombreConductor")
        patenteTracto = dr.Item("patenteTracto")
        patenteTrailer = dr.Item("patenteTrailer")
        localOrigenCodigo = dr.Item("localOrigenCodigo")
        localOrigenDescripcion = dr.Item("localOrigenDescripcion")
        localDestinoCodigo = dr.Item("localDestinoCodigo")
        localDestinoDescripcion = dr.Item("localDestinoDescripcion")
        tipoViaje = dr.Item("tipoViaje")
        fechaHoraCreacion = dr.Item("fechaHoraCreacion")
        alertaMapa = dr.Item("alertaMapa")

        botonResponder = "<div id=""" & idAlerta & "_btnComentar"" class=""btn btn-success btn-sm sist-width-100-porciento""  onclick=""Transportista.expandirContraer({ elem_holderComentario:'" & idAlerta & "_holderComentario', idAlerta:'" & idAlerta & "' });"" /><span class=""glyphicon glyphicon-eye-open""></span>&nbsp; Comentar</div>"
        patente = IIf(String.IsNullOrEmpty(patenteTracto), "", "<strong>Tracto</strong>: " & patenteTracto & "&nbsp;&nbsp;") & IIf(String.IsNullOrEmpty(patenteTrailer), "", "<strong>Remolque</strong>: " & patenteTrailer & "&nbsp;&nbsp;")
        localOrigenDescripcion = localOrigenDescripcion & IIf(String.IsNullOrEmpty(localOrigenCodigo), "", " - " & localOrigenCodigo)
        localDestinoDescripcion = localDestinoDescripcion & IIf(String.IsNullOrEmpty(localDestinoCodigo), "", " - " & localDestinoCodigo)
        alertaMapa = IIf(String.IsNullOrEmpty(alertaMapa), "", "<br /><a href=""" & alertaMapa & """ target=""_blank"" class=""cssTextoNormal"" style=""color: #0e0aff"">[Ver mapa]</a>")

        sb.AppendLine("   <tr>")
        sb.AppendLine("     <td>" & idAlerta & alertaMapa & "</td>")
        sb.AppendLine("     <td>" & nombreAlerta & "</td>")
        sb.AppendLine("     <td>" & fechaHoraCreacion & "</td>")
        sb.AppendLine("     <td>" & nombreConductor & "</td>")
        sb.AppendLine("     <td>" & patente & "</td>")
        sb.AppendLine("     <td>" & localOrigenDescripcion & "</td>")
        sb.AppendLine("     <td>" & localDestinoDescripcion & "</td>")
        sb.AppendLine("     <td>" & tipoViaje & "</td>")
        sb.AppendLine("     <td style=""text-align:center"">" & botonResponder & "</td>")
        sb.AppendLine("   </tr>")
        sb.AppendLine("   <tr>")
        sb.AppendLine("     <td colspan=""9"" class=""sist-padding-cero"">" & ConstruirTablaCategoriasPorAlerta(ds, idAlerta) & "</td>")
        sb.AppendLine("   </tr>")
      Next
      sb.AppendLine("   </tbody>")
      sb.AppendLine(" </table>")
      sb.AppendLine("</div>")

      'asigna texto
      html = sb.ToString()
    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try
    Return html
  End Function

  ''' <summary>
  ''' construye tabla con las categorias de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirTablaCategoriasPorAlerta(ByVal ds As DataSet, ByVal idAlerta As String) As String
    Dim oUtilidades As New Utilidades
    Dim tpl As String = "<div class=""sist-padding-expandir-contraer"" style=""display:none;"" id=""" & idAlerta & "_holderComentario"">{CONTENIDO}</div>"
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<div class=""help-block text-center""><em>No tiene categor&iacute;as asociadas</em></div>"
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRowView
    Dim totalRegistros, totalCategorias, i As Integer
    Dim fechaGestion, categoriaAlerta, tipoObservacion, observacion As String
    Dim sb As New StringBuilder

    Try
      If ds Is Nothing Then
        html = htmlSinRegistro
      Else
        dt = ds.Tables(eTabla.T01_ListadoCategoriasPorAlerta)
        totalRegistros = dt.Rows.Count
        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
          dv = dt.DefaultView
          'filtra el contenido por el idAlerta
          dv.RowFilter = "IdAlerta='" & idAlerta & "'"
          totalCategorias = dv.Count

          If totalCategorias = 0 Then
            html = htmlSinRegistro
          Else
            sb.AppendLine("<div>")
            sb.AppendLine(" <table class=""table table-condensed table-bordered table-striped"">")
            sb.AppendLine("   <thead>")
            sb.AppendLine("   <tr>")
            sb.AppendLine("     <th>Fecha Gesti&oacute;n</th>")
            sb.AppendLine("     <th>Categor&iacute;a</th>")
            sb.AppendLine("     <th>Tipo Observaci&oacute;n</th>")
            sb.AppendLine("     <th>Observaci&oacute;n</th>")
            sb.AppendLine("   </tr>")
            sb.AppendLine("   </thead>")
            sb.AppendLine("   <tbody>")

            For i = 0 To totalCategorias - 1
              dr = dv.Item(i)
              fechaGestion = Utilidades.IsNull(dr.Item("fechaGestion"), "&nbsp;")
              categoriaAlerta = Utilidades.IsNull(dr.Item("categoriaAlerta"), "&nbsp;")
              tipoObservacion = Utilidades.IsNull(dr.Item("tipoObservacion"), "&nbsp;")
              observacion = Utilidades.IsNull(dr.Item("observacion"), "&nbsp;")

              sb.AppendLine("   <tr>")
              sb.AppendLine("     <td>" & fechaGestion & "</td>")
              sb.AppendLine("     <td>" & categoriaAlerta & "</td>")
              sb.AppendLine("     <td>" & tipoObservacion & "</td>")
              sb.AppendLine("     <td>" & observacion & "</td>")
              sb.AppendLine("   </tr>")
            Next
            sb.AppendLine("   </tbody>")
            sb.AppendLine(" </table>")
            sb.AppendLine("</div>")

            'Respuestas
            sb.AppendLine(ConstruirTablaRespuesta(ds, idAlerta))

            'observacion
            sb.AppendLine("<div class=""panel panel-default"">")
            sb.AppendLine("  <div class=""panel-heading""><strong>Observaci&oacute;n</strong></div>")
            sb.AppendLine("  <div class=""panel-body"">")
            sb.AppendLine("     <input type=""hidden"" id=""" & idAlerta & "_txtIdAlerta"" value=""" & idAlerta & """ class=""id-alerta"" />")
            sb.AppendLine("     <textarea type=""text"" id=""" & idAlerta & "_txtObservacion"" class=""form-control"" rows=""3"" style=""width:100%"" ></textarea>")
            sb.AppendLine("  </div>")
            sb.AppendLine("</div>")

            'adjuntar archivos
            sb.AppendLine("<div class=""panel panel-default"">")
            sb.AppendLine("  <div class=""panel-heading""><strong>Adjuntar archivos</strong></div>")
            sb.AppendLine("  <div class=""panel-body"">")
            For i = 1 To MAXIMO_ARCHIVOS_ADJUNTOS
              sb.Append("<div class=""sist-margin-bottom-2""><input type=""file""  id=""" & idAlerta & "_flArchivo_" & i & """ name=""" & idAlerta & "_flArchivo_" & i & """ size=""60"" class=""form-control input-sm"" /></div>")
            Next
            sb.AppendLine("  </div>")
            sb.AppendLine("</div>")

            'asigna texto
            html = sb.ToString
          End If
        End If
      End If

    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try
    Return tpl.Replace("{CONTENIDO}", html)
  End Function

  Private Function ConstruirTablaRespuesta(ByVal ds As DataSet, ByVal idAlerta As String) As String
    Dim oUtilidades As New Utilidades
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<div class=""help-block text-center""><em>No tiene respuestas asociadas</em></div>"
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRowView
    Dim totalRegistros, totalCategorias, i As Integer
    Dim idAlertaRojaRespuesta, rechazo, respuesta, fecha As String
    Dim sb As New StringBuilder
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")

    Try
      If ds Is Nothing Then
        html = htmlSinRegistro
      Else
        dt = ds.Tables(eTabla.T02_RespuestaTransportista)
        totalRegistros = dt.Rows.Count
        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
          dv = dt.DefaultView
          'filtra el contenido por el idAlerta
          dv.RowFilter = "IdAlerta='" & idAlerta & "'"
          totalCategorias = dv.Count

          If totalCategorias = 0 Then
            html = htmlSinRegistro
          Else

            'Listado de respuesta
            '----------------------------------------------------
            sb.AppendLine("<div>")
            sb.AppendLine(" <table class=""table table-condensed table-bordered table-striped"">")
            sb.AppendLine("   <thead>")
            sb.AppendLine("     <tr>")
            sb.AppendLine("       <th>Respuesta</th>")
            sb.AppendLine("       <th>Fecha</th>")
            sb.AppendLine("     </tr>")
            sb.AppendLine("   </thead>")
            sb.AppendLine("   <tbody>")

            For i = 0 To totalCategorias - 1
              dr = dv.Item(i)

              idAlertaRojaRespuesta = Utilidades.IsNull(dr.Item("id"), 0)
              respuesta = Utilidades.IsNull(dr.Item("Observacion"), "&nbsp;")
              fecha = Utilidades.IsNull(dr.Item("FechaCreacion"), "&nbsp;")
              rechazo = Utilidades.IsNull(dr.Item("Rechazo"), False)

              If (rechazo) Then
                rechazo = "<span class=""text-danger"" style=""font-weight:bold;"">(Rechazo)</span>"
              Else
                rechazo = ""
              End If

              sb.AppendLine("   <tr>")
              sb.AppendLine("     <td>" & respuesta & " " & rechazo & "</td>")
              sb.AppendLine("     <td>" & fecha & "</td>")
              sb.AppendLine("   </tr>")
            Next
            sb.AppendLine("   </tbody>")
            sb.AppendLine(" </table>")
            sb.AppendLine("</div>")

            'Rechazo de respuesta
            '----------------------------------------------------
            'sb.AppendLine("<div>")
            'sb.AppendLine("   <label class=""control-label"">Rechazo Respuesta</label>")
            'sb.AppendLine("   <div class=""text-center"">")
            'sb.AppendLine("      <textarea id=""txtObservacionR_" & idAlerta & """ rows=""2"" cols=""20"" class=""form-control""></textarea>")
            'sb.AppendLine("      </br>")
            'sb.AppendLine("      <div id=""btnRechazar_" & idAlerta & """ align=""center"" class=""btn btn-danger btn-sm sist-width-30-porciento"" onclick=""Alerta.grabarRespuestaRechazo({control:'txtObservacionR_" & idAlerta & "', idAlerta:" & idAlerta & ",nroTransporte:" & nroTransporte & " })"" /><span class=""glyphicon glyphicon-ban-circle""></span>&nbsp; Rechazar</div>")
            'sb.AppendLine("   </div>")
            'sb.AppendLine("</div>")

            'asigna texto
            html = sb.ToString()
          End If
        End If
      End If
    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try
    Return html
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
    Dim idAlertaRojaEstadoActual As String = Utilidades.IsNull(Request("idAlertaRojaEstadoActual"), "-1")

    Me.ViewState.Add("nroTransporte", nroTransporte)
    Me.ViewState.Add("idAlertaRojaEstadoActual", idAlertaRojaEstadoActual)

    If Not Page.IsPostBack Then
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    GrabarRegistro()
  End Sub

End Class