﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Mantenedor.LocalListado.aspx.vb" Inherits="WebTransportePochteca.Mantenedor_LocalListado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/local.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Tiendas</p>
      </div>
      <div class="col-xs-4 text-right">
        <div id="btnNuevo" runat="server" class="btn btn-success" onclick="Local.abrirFormulario({ idLocal:'-1' })"><span class="glyphicon glyphicon-plus"></span>&nbsp;Crear Nuevo Local</div>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">Formato</label>
              <uc1:wucCombo id="ddlFiltroFormato" runat="server" FuenteDatos="Tabla" TipoCombo="Formato" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nombre</label>
              <asp:TextBox ID="txtFiltroNombre" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">C&oacute;digo</label>
              <asp:TextBox ID="txtFiltroCodigo" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">Etapa</label>
              <asp:TextBox ID="txtEtapa" runat="server" MaxLength="6" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">Foco Alto</label>
              <asp:CheckBox ID="chkFocoAlto" runat="server" CssClass="control-label" />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Local.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="IdLocal" HeaderText="ID" SortExpression="IdLocal" HeaderStyle-Width="60px" />
              <asp:BoundField DataField="NombreLocal" HeaderText="Nombre" SortExpression="NombreLocal" />
              <asp:BoundField DataField="CodigoInterno" HeaderText="C&oacute;digo" SortExpression="CodigoInterno" />
              <asp:BoundField DataField="NombreFormato" HeaderText="Formato" SortExpression="NombreFormato" />
              <asp:BoundField DataField="FocoAlto" HeaderText="Foco Alto" SortExpression="FocoAlto" />
              <asp:BoundField DataField="Etapa" HeaderText="Etapa Ingreso" SortExpression="Etapa" />
              <asp:BoundField DataField="Activo" HeaderText="Activo" SortExpression="Activo" HeaderStyle-Width="80px" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdLocal" runat="server" value='<%# Bind("IdLocal") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>

</asp:Content>
