﻿Imports CapaNegocio
Imports CapaNegocio.Herramientas
Imports System.Data
Imports System.Data.OleDb

Public Class Mantenedor_CargadorPoolPlaca
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Const NOMBRE_CARPETA_CARGA As String = "PoolPlaca"
  Private Const SESION_LISTADO_ERRORES As String = "SESION_LISTADO_ERRORES"
  Private Const SESION_FECHA_CARGA As String = "SESION_FECHA_CARGA"
  Private Const SESION_TABLA_DESTINO As String = "SESION_TABLA_DESTINO"
  Private Const SESION_DATATABLE_CARGA_XLS As String = "SESION_DATATABLE_CARGA_XLS"
  Private Const SESION_DATASET_TABLAS_BASICAS As String = "SESION_DATASET_TABLAS_BASICAS"
  Private COLUMNA_00_PLACA As String = "PLACA"
  Private COLUMNA_01_TIPO_VEHICULO As String = "TIPO VEHICULO"
  Private COLUMNA_02_TRANSPORTE As String = "TRANSPORTE"
  Private COLUMNA_03_CEDIS As String = "CEDIS"
  Private COLUMNA_04_DETERMINANTE As String = "DETERMINANTE"
  Private COLUMNA_05_NUMERO_ECONOMICO As String = "NUMERO ECONOMICO"
  Private COLUMNA_REPORTABILIDAD_ESTADO As String = "REPORTABILIDAD_ESTADO"
  Private COLUMNA_REPORTABILIDAD As String = "REPORTABILIDAD"
  Private COLUMNA_REPORTABILIDAD_ORDEN As String = "REPORTABILIDAD_ORDEN"

  Dim DATASET_TABLAS_BASICAS As New DataSet
  Dim DATATABLE_CARGA_XLS As New DataTable

  Private Enum eFunciones
    Ver
  End Enum

#End Region

#Region "Metodo Controles ASP"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    ActualizaInterfaz()
    MostrarMensajeUsuario()
  End Sub

  Protected Sub btnValidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    Sistema.GrabarLogSesion(oUsuario.Id, "Valida carga de pool placas")
    ValidarArchivo()
    Response.Redirect("./Mantenedor.CargadorPoolPlaca.aspx")
  End Sub

  Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    Sistema.GrabarLogSesion(oUsuario.Id, "Cancela la carga de pool placas")

    Session(Utilidades.KEY_SESION_MENSAJE) = "{INGRESADO}La cancelaci&oacute;n de la carga fue realizada satisfactoriamente"
    Session(SESION_LISTADO_ERRORES) = Nothing
    Session(SESION_DATATABLE_CARGA_XLS) = Nothing
    Session(SESION_DATASET_TABLAS_BASICAS) = Nothing
    Response.Redirect("./Mantenedor.CargadorPoolPlaca.aspx")
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim listadoErrores As New List(Of String)

    listadoErrores = GrabarDatosCarga()
    If (listadoErrores.Count > 0) Then
      Session(SESION_LISTADO_ERRORES) = listadoErrores
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ERROR}Se produjo un error y algunos registros no se pudieron grabar correctamente"
    Else
      Session(SESION_LISTADO_ERRORES) = Nothing
      Session(Utilidades.KEY_SESION_MENSAJE) = "{INGRESADO}Los datos fueron grabados satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Graba la carga de pool placas")
    End If

    Session(SESION_DATATABLE_CARGA_XLS) = Nothing
    Session(SESION_DATASET_TABLAS_BASICAS) = Nothing
    Response.Redirect("./Mantenedor.CargadorPoolPlaca.aspx")
  End Sub

#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim listadoErrores As New List(Of String)
    Dim dibujaErrores As Boolean = False
    Dim dt As New DataTable

    'revisa si hay errores para mostrarlos
    listadoErrores = CType(Session(SESION_LISTADO_ERRORES), List(Of String))
    If (Not listadoErrores Is Nothing) Then
      If (listadoErrores.Count > 0) Then
        dibujaErrores = True
        MostrarListadoErrores()
      End If
    End If

    'si no tiene errores, dibuja los datos para ser cargados
    If (Not dibujaErrores) Then
      dt = CType(Session(SESION_DATATABLE_CARGA_XLS), DataTable)
      If (Not dt Is Nothing) Then
        MostrarDatosPorGuardar(dt)
      End If
    End If

  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensaje.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' valida el archivo y si esta bien entonces graba los datos
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub ValidarArchivo()
    Dim msgError As String = ""
    Dim esValido As Boolean = True
    Dim nombreArchivo As String = ""
    Dim extension As String = ""
    Dim pathFile As String = ""
    Dim pathFolder As String = ""
    Dim oUtilidades As New Utilidades()
    Dim ListaIds As String = ""

    Try
      nombreArchivo = Archivo.ObtenerNombreArchivoDesdeRuta(Me.fileCarga.FileName)
      nombreArchivo = Archivo.CrearNombreUnicoArchivo() & "_" & nombreArchivo
      pathFolder = Utilidades.RutaLogsSitio & "\" & NOMBRE_CARPETA_CARGA

      'crea carpeta si es que no existe
      Archivo.CrearDirectorioEnDisco(pathFolder)

      'ruta archivo
      pathFile = pathFolder & "\" & nombreArchivo

      'envie al servidor
      Me.fileCarga.PostedFile.SaveAs(pathFile)

      'verifica que el archivo sea valido
      esValido = ValidarFormatoArchivo(pathFile)

      'graba el resultado de la validacion
      If Not esValido Then
        msgError = "{ERROR}El archivo tiene errores, favor revisar y volver a cargar"
        Session(SESION_DATATABLE_CARGA_XLS) = Nothing
        Session(SESION_DATASET_TABLAS_BASICAS) = Nothing
      Else
        msgError = "{INGRESADO}El archivo está correcto para ser cargado"
        Session(SESION_DATATABLE_CARGA_XLS) = DATATABLE_CARGA_XLS
      End If
      Session(Utilidades.KEY_SESION_MENSAJE) = msgError
    Catch ex As Exception
      Session(Utilidades.KEY_SESION_MENSAJE) = ex.Message
    End Try
  End Sub

  ''' <summary>
  ''' valida el formato para el tipo de encuesta Evaluacion de desempeño
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ValidarFormatoArchivo(ByVal pathFile As String) As Boolean
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim retorno As Boolean
    Dim extension As String
    Dim NUMERO_COLUMNAS As Integer = 6
    Dim totalRegistros, i As Integer
    Dim nombreColumna, placa, tipoVehiculo, transporte, cedis, determinante, numeroEconomico As String
    Dim columnasQueFaltan As String = ""
    Dim validacionDatosFila As String = ""
    Dim dr As DataRow
    Dim listadoErrores As New List(Of String)

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      DATASET_TABLAS_BASICAS = Sistema.ObtenerTablasBasicasSistema(oUsuario.Id)
      Session(SESION_DATASET_TABLAS_BASICAS) = DATASET_TABLAS_BASICAS

      '-----------------------------------------------
      'valida la extension
      extension = Archivo.ObtenerExtension(pathFile)
      If (extension.ToLower() <> "xlsx" And extension.ToLower() <> "xls") Then
        listadoErrores.Add("El tipo de archivo no es v&aacute;lido, debe ser un archivo *.XLSX o *.XLS")
      Else
        '-----------------------------------------------
        'valida si existe el archivo en disco
        If Not Archivo.ExisteArchivoEnDisco(pathFile) Then
          listadoErrores.Add("El archivo no existe en el repositorio")
        Else
          DATATABLE_CARGA_XLS = CargarDataTable(pathFile, extension)

          '---------------------------------------
          'valida total de registros
          totalRegistros = DATATABLE_CARGA_XLS.Rows.Count
          If totalRegistros = 0 Then listadoErrores.Add("El archivo no contiene registros")

          '---------------------------------------
          'valida el numero de columnas
          If DATATABLE_CARGA_XLS.Columns.Count <> NUMERO_COLUMNAS Then listadoErrores.Add("El n&uacute;mero de columnas no es válido, deben ser " & NUMERO_COLUMNAS)

          '---------------------------------------
          'valida nombre de columnas
          If DATATABLE_CARGA_XLS.Columns.Count = NUMERO_COLUMNAS Then
            For i = 0 To NUMERO_COLUMNAS - 1
              nombreColumna = DATATABLE_CARGA_XLS.Columns(i).ColumnName.ToUpper()
              Select Case i
                Case 0 : If nombreColumna <> COLUMNA_00_PLACA Then columnasQueFaltan &= "- " & COLUMNA_00_PLACA & "<br />"
                Case 1 : If nombreColumna <> COLUMNA_01_TIPO_VEHICULO Then columnasQueFaltan &= "- " & COLUMNA_01_TIPO_VEHICULO & "<br />"
                Case 2 : If nombreColumna <> COLUMNA_02_TRANSPORTE Then columnasQueFaltan &= "- " & COLUMNA_02_TRANSPORTE & "<br />"
                Case 3 : If nombreColumna <> COLUMNA_03_CEDIS Then columnasQueFaltan &= "- " & COLUMNA_03_CEDIS & "<br />"
                Case 4 : If nombreColumna <> COLUMNA_04_DETERMINANTE Then columnasQueFaltan &= "- " & COLUMNA_04_DETERMINANTE & "<br />"
                Case 5 : If nombreColumna <> COLUMNA_05_NUMERO_ECONOMICO Then columnasQueFaltan &= "- " & COLUMNA_05_NUMERO_ECONOMICO & "<br />"
              End Select
            Next
            If Not String.IsNullOrEmpty(columnasQueFaltan) Then listadoErrores.Add("Las siguientes columnas faltan:<br />" & columnasQueFaltan)

            '---------------------------------------
            'valida los registros ingresado
            If totalRegistros > 0 And String.IsNullOrEmpty(columnasQueFaltan) Then
              For i = 0 To totalRegistros - 1
                dr = DATATABLE_CARGA_XLS.Rows(i)
                placa = Herramientas.IsNull(dr.Item(COLUMNA_00_PLACA), "")
                tipoVehiculo = Herramientas.IsNull(dr.Item(COLUMNA_01_TIPO_VEHICULO), "")
                transporte = Herramientas.IsNull(dr.Item(COLUMNA_02_TRANSPORTE), "")
                cedis = Herramientas.IsNull(dr.Item(COLUMNA_03_CEDIS), "")
                determinante = Herramientas.IsNull(dr.Item(COLUMNA_04_DETERMINANTE), "")
                numeroEconomico = Herramientas.IsNull(dr.Item(COLUMNA_05_NUMERO_ECONOMICO), "")
                validacionDatosFila = ""

                'formatea valores
                placa = placa.Trim()
                tipoVehiculo = tipoVehiculo.Trim()
                transporte = transporte.Trim()
                cedis = cedis.Trim()
                determinante = determinante.Trim()
                numeroEconomico = numeroEconomico.Trim()

                '-------------------------------------------------
                'COLUMNA_00_PLACA obligatorio
                If String.IsNullOrEmpty(placa) Then validacionDatosFila &= COLUMNA_00_PLACA & " es requerido / "

                '-------------------------------------------------
                'COLUMNA_01_TIPO_VEHICULO obligatorio
                If String.IsNullOrEmpty(tipoVehiculo) Then validacionDatosFila &= COLUMNA_01_TIPO_VEHICULO & " es requerido / "

                '-------------------------------------------------
                'COLUMNA_02_TRANSPORTE obligatorio
                If String.IsNullOrEmpty(transporte) Then
                  validacionDatosFila &= COLUMNA_02_TRANSPORTE & " es requerido / "
                Else
                  If Not ExisteRegistroEnTablaBasica(DATASET_TABLAS_BASICAS, Sistema.eTablasBasicas.T01_Transportista, "Nomenclatura", transporte) Then validacionDatosFila &= COLUMNA_02_TRANSPORTE & " <em>&quot;" & transporte & "&quot;</em> no está registrado en el sistema / "
                End If

                '-------------------------------------------------
                'COLUMNA_03_CEDIS obligatorio
                If String.IsNullOrEmpty(cedis) Then validacionDatosFila &= COLUMNA_03_CEDIS & " es requerido / "

                '-------------------------------------------------
                'COLUMNA_04_DETERMINANTE obligatorio
                If String.IsNullOrEmpty(determinante) Then
                  validacionDatosFila &= COLUMNA_04_DETERMINANTE & " es requerido / "
                Else
                  '-- recorre el listado de CD --
                  Dim arrDeterminante As String() = determinante.Split(",")
                  For Each codigo As String In arrDeterminante
                    codigo = codigo.Trim()
                    '-- valida que sea numerico --
                    If Not Utilidades.EsNumerico(codigo) Then
                      validacionDatosFila &= COLUMNA_04_DETERMINANTE & " <em>&quot;" & codigo & "&quot;</em> no es v&aacute;lido / "
                    Else
                      '-- valida que el codigo este registrado en tabla CentroDistribucion
                      If Not ExisteRegistroEnTablaBasica(DATASET_TABLAS_BASICAS, Sistema.eTablasBasicas.T00_CentroDistribucion, "CodigoInterno", codigo) Then
                        validacionDatosFila &= COLUMNA_04_DETERMINANTE & " <em>&quot;" & codigo & "&quot;</em> no está registrado en el sistema / "
                      Else
                        '-- valida que el CD este asociado al usuario
                        If Not ExisteRegistroEnTablaBasica(DATASET_TABLAS_BASICAS, Sistema.eTablasBasicas.T02_CentroDistribucionAsociados, "CodigoInterno", codigo) Then validacionDatosFila &= COLUMNA_04_DETERMINANTE & " <em>&quot;" & codigo & "&quot;</em> no está asociado a su perfil / "
                      End If
                    End If
                  Next
                End If

                '-------------------------------------------------
                'COLUMNA_05_NUMERO_ECONOMICO obligatorio
                If String.IsNullOrEmpty(numeroEconomico) Then validacionDatosFila &= COLUMNA_05_NUMERO_ECONOMICO & " es requerido / "

                '---------------------------------------------
                'devuelve los errores encontrados
                If Not String.IsNullOrEmpty(validacionDatosFila) Then listadoErrores.Add("<strong>Fila " & (i + 2) & "</strong>: " & Utilidades.EliminaUltimoCaracterTexto(validacionDatosFila, 2))
              Next

            End If '<-- If totalRegistros > 0 And String.IsNullOrEmpty(columnasQueFaltan)
          End If '<-- If DATATABLE_CARGA_CSV.Columns.Count = NUMERO_COLUMNAS
        End If '<-- If Not Archivo.ExisteArchivoEnDisco(pathFile)
      End If '<-- If (extension.ToLower() <> "csv")

      If listadoErrores.Count = 0 Then
        retorno = True
      Else
        retorno = False
      End If
    Catch ex As Exception
      listadoErrores.Add(ex.Message)
      retorno = False
    End Try

    'graba el listado de errores en sesion
    Session(SESION_LISTADO_ERRORES) = listadoErrores
    Return retorno
  End Function

  ''' <summary>
  ''' verifica si el valor existe en los datos extraidos de la base
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExisteRegistroEnTablaBasica(ByVal ds As DataSet, ByVal indiceTabla As Planificacion.eTablasBasicas, ByVal nombreColumna As String, ByVal valorComparar As String) As Boolean
    Dim existe As Boolean

    Try
      'se iniciliza el valor como que no existe el registro
      existe = False

      'formatea valores
      valorComparar = IIf(String.IsNullOrEmpty(valorComparar), "-1", valorComparar)
      Dim table As DataTable = ds.Tables(indiceTabla)
      Dim query = From row As DataRow In table.Rows Where row.Item(nombreColumna) = valorComparar

      For Each item In query
        existe = True
        Exit For
      Next
    Catch ex As Exception
      existe = False
    End Try
    Return existe
  End Function

  ''' <summary>
  ''' carga archivo excel en datatable
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CargarDataTable(ByVal filePath As String, ByVal extension As String) As DataTable
    Dim conStr As String = ""
    Dim cmdExcel As New OleDbCommand()
    Dim oda As New OleDbDataAdapter()
    Dim dtExcel As New DataTable()

    Try
      conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source= {0};Extended Properties=""Excel 12.0;hdr=YES;imex=1"""
      conStr = String.Format(conStr, filePath)

      'Read Data from First Sheet
      Dim nameSheet As String
      Dim connExcel As New OleDbConnection(conStr)
      cmdExcel.Connection = connExcel
      connExcel.Open()
      'obtengo nombre de la hoja
      nameSheet = connExcel.GetSchema("Tables").Rows(0)("TABLE_NAME")
      cmdExcel.CommandText = "SELECT * From [" + nameSheet + "]"
      oda.SelectCommand = cmdExcel
      oda.Fill(dtExcel)
      connExcel.Close()

      Return dtExcel
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' graba los datos de la carga
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosCarga() As List(Of String)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim msgError As String = ""
    Dim oPoolPlaca As New Sistema.PoolPlaca
    Dim placa, tipoVehiculo, transporte, cedis, determinante, numeroEconomico As String
    Dim sb As New StringBuilder
    Dim indice As Integer = 0
    Dim listadoErrores As New List(Of String)
    Dim listadoDeterminantes As New List(Of String)
    Dim fechaCarga As New DateTime
    Dim dt As DataTable

    Try
      fechaCarga = Herramientas.MyNow()
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      dt = CType(Session(SESION_DATATABLE_CARGA_XLS), DataTable)

      'graba los registros
      For Each dr As DataRow In dt.Rows
        oPoolPlaca = New Sistema.PoolPlaca

        indice += 1
        placa = Herramientas.IsNull(dr.Item(COLUMNA_00_PLACA), "")
        tipoVehiculo = Herramientas.IsNull(dr.Item(COLUMNA_01_TIPO_VEHICULO), "")
        transporte = Herramientas.IsNull(dr.Item(COLUMNA_02_TRANSPORTE), "")
        cedis = Herramientas.IsNull(dr.Item(COLUMNA_03_CEDIS), "")
        determinante = Herramientas.IsNull(dr.Item(COLUMNA_04_DETERMINANTE), "")
        numeroEconomico = Herramientas.IsNull(dr.Item(COLUMNA_05_NUMERO_ECONOMICO), "")

        '-- recorre el listado de CD --
        Dim arrDeterminante As String() = determinante.Split(",")
        For Each codigo As String In arrDeterminante
          codigo = codigo.Trim()

          'graba el registro
          With oPoolPlaca
            .Placa = placa.Trim().Replace(".", "").Replace(" ", "").Replace("-", "").ToUpper()
            .TipoVehiculo = tipoVehiculo.Trim()
            .Transporte = transporte.Trim()
            .Cedis = cedis.Trim()
            .Determinante = codigo
            .IdUsuarioCreacion = oUsuario.Id
            .FechaCarga = fechaCarga
            .NumeroEconomico = numeroEconomico.Trim()
          End With

          Try
            'graba registro
            Sistema.GuardarPoolPlaca(oPoolPlaca)

            'almacena el codigo de los cedis para traspasar la informacion a tabla historia
            If (Not listadoDeterminantes.Contains(codigo)) Then
              listadoDeterminantes.Add(codigo)
            End If
          Catch ex As Exception
            listadoErrores.Add("<strong>Fila " & indice & "</strong>: No se pudo grabar los datos debido a - " & ex.Message)
          End Try

        Next '<-- For Each codigo As String In arrDeterminante
      Next ' <-- For Each dr As DataRow In dt.Rows

      'si no hay errores entonces envia informacion de la carga por email
      If (listadoErrores.Count = 0) Then
        'graba pool patentes historia
        Sistema.GrabarPoolPlacaHistoria(String.Join(",", listadoDeterminantes.ToArray()), fechaCarga)

        'envia carga de placas por email
        InformeMail.CargaPoolPatente(oUsuario.Id, fechaCarga)
      End If
    Catch ex As Exception
      listadoErrores.Add(ex.Message)
    End Try

    Return listadoErrores
  End Function

  ''' <summary>
  ''' muestra el listado de errores
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarListadoErrores()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim listadoErrores As New List(Of String)
    Dim sb As New StringBuilder

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      listadoErrores = CType(Session(SESION_LISTADO_ERRORES), List(Of String))

      If (Not listadoErrores Is Nothing) Then
        If (listadoErrores.Count > 0) Then
          Sistema.GrabarLogSesion(oUsuario.Id, "Muestra errores carga pool placas")

          sb.Append("<div class=""form-group""><strong>Se encontraron los siguientes errores:</strong></div>")
          sb.Append("<div>")
          sb.Append("  <ul>")
          For Each texto As String In listadoErrores
            sb.Append("<li>" & texto & "</li>")
          Next
          sb.Append("  </ul>")
          sb.Append("</div>")

          Me.ltlTablaDatos.Visible = True
          Me.ltlTablaDatos.Text = Utilidades.MostrarMensajeUsuario(sb.ToString(), Utilidades.eTipoMensajeAlert.Danger, False, "text-left")
        End If
      End If
    Catch ex As Exception
      Me.ltlTablaDatos.Visible = False
    End Try

    Session(SESION_LISTADO_ERRORES) = Nothing
  End Sub

  ''' <summary>
  ''' muestra la carga que sera grabada
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarDatosPorGuardar(ByVal dt As DataTable)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim template As String = ""
    Dim totalRegistros As Integer
    Dim templateAux, placa, tipoVehiculo, transporte, cedis, determinante, reportabilidad, reportabilidadEstado, classFila As String
    Dim numeroEconomico As String
    Dim indice As Integer = 0

    Try
      'agrega columnas de reportabilidad de patentes en datatable
      dt = AgregarColumnaReportabilidad(dt)
      oUsuario = oUtilidades.ObtenerUsuarioSession()

      template &= "<tr class=""{CLASS_FILA}"">"
      template &= "  <td>{INDICE}</td>"
      template &= "  <td>{PLACA}</td>"
      template &= "  <td>{REPORTABILIDAD}</td>"
      template &= "  <td>{TIPO_VEHICULO}</td>"
      template &= "  <td>{TRANSPORTE}</td>"
      template &= "  <td>{CEDIS}</td>"
      template &= "  <td>{DETERMINANTE}</td>"
      template &= "  <td>{NUMERO_ECONOMICO}</td>"
      template &= "</tr>"

      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        Sistema.GrabarLogSesion(oUsuario.Id, "Muestra datos por guardar en pool de placas")

        sb.Append("<div class=""small"">")
        sb.Append("<table class=""table table-condensed table-bordered"">")
        sb.Append("  <thead>")
        sb.Append("    <tr>")
        sb.Append("      <th style=""width: 40px;"">#</th>")
        sb.Append("      <th>Placa</th>")
        sb.Append("      <th>Reportabilidad</th>")
        sb.Append("      <th>Tipo Veh&iacute;culo</th>")
        sb.Append("      <th>Transporte</th>")
        sb.Append("      <th>Cedis</th>")
        sb.Append("      <th>Determinante</th>")
        sb.Append("      <th>N&uacute;mero Econ&oacute;mico</th>")
        sb.Append("    </tr>")
        sb.Append("  </thead>")
        sb.Append("  <tbody>")

        For Each dr As DataRow In dt.Rows
          templateAux = template
          indice += 1
          placa = dr.Item(COLUMNA_00_PLACA)
          tipoVehiculo = dr.Item(COLUMNA_01_TIPO_VEHICULO)
          transporte = dr.Item(COLUMNA_02_TRANSPORTE)
          cedis = dr.Item(COLUMNA_03_CEDIS)
          determinante = dr.Item(COLUMNA_04_DETERMINANTE)
          numeroEconomico = dr.Item(COLUMNA_05_NUMERO_ECONOMICO)
          reportabilidad = dr.Item(COLUMNA_REPORTABILIDAD)
          reportabilidadEstado = dr.Item(COLUMNA_REPORTABILIDAD_ESTADO)

          Select Case reportabilidadEstado.ToLower()
            Case "online"
              classFila = ""
            Case Else
              classFila = "danger"
          End Select

          templateAux = templateAux.Replace("{INDICE}", indice)
          templateAux = templateAux.Replace("{CLASS_FILA}", classFila)
          templateAux = templateAux.Replace("{PLACA}", Server.HtmlEncode(placa))
          templateAux = templateAux.Replace("{REPORTABILIDAD}", Server.HtmlEncode(reportabilidad))
          templateAux = templateAux.Replace("{TIPO_VEHICULO}", Server.HtmlEncode(tipoVehiculo))
          templateAux = templateAux.Replace("{TRANSPORTE}", Server.HtmlEncode(transporte))
          templateAux = templateAux.Replace("{CEDIS}", Server.HtmlEncode(cedis))
          templateAux = templateAux.Replace("{DETERMINANTE}", Server.HtmlEncode(determinante))
          templateAux = templateAux.Replace("{NUMERO_ECONOMICO}", Server.HtmlEncode(numeroEconomico))
          sb.Append(templateAux)
        Next

        sb.Append("  </tbody>")
        sb.Append("</table>")
        sb.Append("</div>")
        Me.ltlTablaDatos.Text = sb.ToString()

        Me.pnlBotones.Visible = True
      End If
    Catch ex As Exception
      Me.ltlTablaDatos.Visible = False
    End Try
  End Sub

  ''' <summary>
  ''' agrega columna de reportabilidad
  ''' </summary>
  ''' <param name="dt"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function AgregarColumnaReportabilidad(ByVal dt As DataTable) As DataTable
    Dim nuevoDT As New DataTable
    Dim placa, reportabilidad, reportabilidadOrden, estado, fecha As String
    Dim dv As DataView
    Dim dsOrdenado As New DataSet

    Try
      DATASET_TABLAS_BASICAS = CType(Session(SESION_DATASET_TABLAS_BASICAS), DataSet)

      dt.Columns.Add(COLUMNA_REPORTABILIDAD_ESTADO, Type.GetType("System.String"))
      dt.Columns.Add(COLUMNA_REPORTABILIDAD, Type.GetType("System.String"))
      dt.Columns.Add(COLUMNA_REPORTABILIDAD_ORDEN, Type.GetType("System.Int32"))

      For Each dr As DataRow In dt.Rows
        placa = dr.Item(COLUMNA_00_PLACA)
        estado = ""
        reportabilidad = ""
        reportabilidadOrden = 1

        'busca reportabilidad de la placa
        placa = placa.Trim()
        Dim table As DataTable = DATASET_TABLAS_BASICAS.Tables(Sistema.eTablasBasicas.T03_ReportabilidadPatentes)
        Dim query = From row As DataRow In table.Rows Where row.Item("Patente") = placa

        If query.Count = 0 Then
          estado = "NoIntegrada"
          reportabilidad = "Placa no integrada en el sistema"
          reportabilidadOrden = 0
        Else
          For Each row In query
            estado = row.Item("Estado")
            fecha = row.Item("Fecha")

            Select Case estado.ToLower()
              Case "online"
                reportabilidad = "Reportando Online"
                reportabilidadOrden = 1
              Case "offline"
                reportabilidad = "No reporta desde " & fecha
                reportabilidadOrden = 0
              Case Else
                reportabilidad = "Placa no integrada en el sistema"
                reportabilidadOrden = 0
            End Select
            Exit For
          Next
        End If

        dr.Item(COLUMNA_REPORTABILIDAD_ESTADO) = estado
        dr.Item(COLUMNA_REPORTABILIDAD) = reportabilidad
        dr.Item(COLUMNA_REPORTABILIDAD_ORDEN) = reportabilidadOrden
      Next

      dv = dt.DefaultView
      dv.Sort = COLUMNA_REPORTABILIDAD_ORDEN & " ASC"
      dsOrdenado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      nuevoDT = dsOrdenado.Tables(0)
    Catch ex As Exception
      nuevoDT = dt
    End Try

    Return nuevoDT
  End Function

#End Region

End Class