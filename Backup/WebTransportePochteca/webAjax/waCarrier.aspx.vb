﻿Imports System.Web.Script.Services
Imports CapaNegocio
Imports System.Web.Script.Serialization

Public Class waCarrier
  Inherits System.Web.UI.Page

  <System.Web.Services.WebMethod()> _
  Public Shared Function getTractosDisponibles(ByVal idTransportista As Integer) As List(Of Object)
    Dim dictionary As Dictionary(Of String, String)
    Dim listOject As New List(Of Object)()
    Dim codigoCedis, nombreCedis, placaTracto, fechaLlegada, tiempoEspera, nombreCompletoCedis As String

    'Obtengo datatable con datos
    Dim dt As DataTable = Carrier.getTractosDisponibles(idTransportista)
    If (Not dt Is Nothing) Then
      For Each dr In dt.Rows
        codigoCedis = dr.Item("CodigoCedis")
        nombreCedis = dr.Item("NombreCedis")
        placaTracto = dr.Item("PlacaTracto")
        fechaLlegada = dr.Item("FechaLlegada")
        tiempoEspera = dr.Item("TiempoEspera")
        nombreCompletoCedis = nombreCedis & " - " & codigoCedis

        'formatea valores
        nombreCompletoCedis = IIf(codigoCedis = "-1", "Sin registro", nombreCompletoCedis)
        fechaLlegada = IIf(fechaLlegada = "", "Sin registro", fechaLlegada)
        tiempoEspera = IIf(tiempoEspera = "", "Sin registro", tiempoEspera)

        dictionary = New Dictionary(Of String, String)
        dictionary.Add("CodigoCedis", codigoCedis)
        dictionary.Add("NombreCedis", nombreCedis)
        dictionary.Add("NombreCompletoCedis", nombreCompletoCedis)
        dictionary.Add("PlacaTracto", placaTracto)
        dictionary.Add("FechaLlegada", fechaLlegada)
        dictionary.Add("TiempoEspera", tiempoEspera)
        listOject.Add(dictionary)
      Next
    End If

    Return listOject
  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function getRemolquesEnPatio(ByVal idTransportista As Integer) As List(Of Object)
    Dim dictionary As Dictionary(Of String, String)
    Dim listOjectResumen As New List(Of Object)()
    Dim listOjectDetalle As New List(Of Object)()
    Dim listOject As New List(Of Object)()
    Dim idEmbarque, codigoCedis, nombreCedis, placaRemolque, determinante, tiempoEspera As String
    Dim semaforo, nombreCompletoCedis As String

    'Obtengo datatable con datos
    Dim ds As DataSet = Carrier.getRemolquesEnPatio(idTransportista)
    If (Not ds Is Nothing) Then

      For Each row As DataRow In ds.Tables(0).Rows
        dictionary = New Dictionary(Of String, String)
        For Each column As DataColumn In ds.Tables(0).Columns
          Dim valor As String = IIf(IsDBNull(row(column)), String.Empty, row(column))
          dictionary.Add(column.ColumnName, valor)
        Next

        listOjectResumen.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectResumen)

      For Each dr In ds.Tables(1).Rows
        idEmbarque = dr.Item("IdEmbarque")
        codigoCedis = dr.Item("CodigoCedis")
        nombreCedis = dr.Item("NombreCedis")
        placaRemolque = dr.Item("PlacaRemolque")
        determinante = dr.Item("Determinante")
        tiempoEspera = dr.Item("TiempoEspera")
        semaforo = dr.Item("Semaforo")
        nombreCompletoCedis = nombreCedis & " - " & codigoCedis

        dictionary = New Dictionary(Of String, String)
        dictionary.Add("IdEmbarque", idEmbarque)
        dictionary.Add("CodigoCedis", codigoCedis)
        dictionary.Add("NombreCedis", nombreCedis)
        dictionary.Add("NombreCompletoCedis", nombreCompletoCedis)
        dictionary.Add("PlacaRemolque", placaRemolque)
        dictionary.Add("Determinante", determinante)
        dictionary.Add("TiempoEspera", tiempoEspera)
        dictionary.Add("Semaforo", semaforo)
        listOjectDetalle.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectDetalle)

    End If

    Return listOject

  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function getRemolquesEnCortina(ByVal idTransportista As Integer) As List(Of Object)
    Dim dictionary As Dictionary(Of String, String)
    Dim listOjectResumen As New List(Of Object)()
    Dim listOjectDetalle As New List(Of Object)()
    Dim listOject As New List(Of Object)()
    Dim idEmbarque, codigoCedis, nombreCedis, placaRemolque, determinante, fechaPosicionCortina, cortina, tiempoCargando As String
    Dim semaforo, nombreCompletoCedis As String

    'Obtengo datatable con datos
    Dim ds As DataSet = Carrier.getRemolquesEnCortina(idTransportista)
    If (Not ds Is Nothing) Then

      For Each row As DataRow In ds.Tables(0).Rows
        dictionary = New Dictionary(Of String, String)
        For Each column As DataColumn In ds.Tables(0).Columns
          Dim valor As String = IIf(IsDBNull(row(column)), String.Empty, row(column))
          dictionary.Add(column.ColumnName, valor)
        Next

        listOjectResumen.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectResumen)

      For Each dr In ds.Tables(1).Rows
        idEmbarque = dr.Item("IdEmbarque")
        codigoCedis = dr.Item("CodigoCedis")
        nombreCedis = dr.Item("NombreCedis")
        placaRemolque = dr.Item("PlacaRemolque")
        determinante = dr.Item("Determinante")
        fechaPosicionCortina = dr.Item("FechaPosicionCortina")
        cortina = dr.Item("Cortina")
        tiempoCargando = dr.Item("TiempoCargando")
        semaforo = dr.Item("Semaforo")
        nombreCompletoCedis = nombreCedis & " - " & codigoCedis

        dictionary = New Dictionary(Of String, String)
        dictionary.Add("IdEmbarque", idEmbarque)
        dictionary.Add("CodigoCedis", codigoCedis)
        dictionary.Add("NombreCedis", nombreCedis)
        dictionary.Add("NombreCompletoCedis", nombreCompletoCedis)
        dictionary.Add("PlacaRemolque", placaRemolque)
        dictionary.Add("Determinante", determinante)
        dictionary.Add("FechaPosicionCortina", fechaPosicionCortina)
        dictionary.Add("Cortina", cortina)
        dictionary.Add("TiempoCargando", tiempoCargando)
        dictionary.Add("Semaforo", semaforo)
        listOjectDetalle.Add(dictionary)
      Next
      'Se agrega al listado final
      listOject.Add(listOjectDetalle)

    End If

    Return listOject

  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function getEmbarqueEnRuta(ByVal idTransportista As Integer) As List(Of Object)
    Dim dictionary As Dictionary(Of String, String)
    Dim listOjectResumen As New List(Of Object)()
    Dim listOjectDetalle As New List(Of Object)()
    Dim listOject As New List(Of Object)()
    Dim idEmbarque, idMaster, codigoCedis, nombreCedis, placaRemolque, placaTracto, determinante, tiempoViaje, tiempoEstimadoArribo, kilometrosRecorridos, totalAlertas As String
    Dim semaforo, nombreCompletoCedis As String

    'Obtengo datatable con datos
    Dim ds As DataSet = Carrier.getEmbarqueEnRuta(idTransportista)
    If (Not ds Is Nothing) Then

      For Each row As DataRow In ds.Tables(0).Rows
        dictionary = New Dictionary(Of String, String)
        For Each column As DataColumn In ds.Tables(0).Columns
          Dim valor As String = IIf(IsDBNull(row(column)), String.Empty, row(column))
          dictionary.Add(column.ColumnName, valor)
        Next

        listOjectResumen.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectResumen)

      For Each dr In ds.Tables(1).Rows
        idEmbarque = dr.Item("IdEmbarque")
        idMaster = dr.Item("IdMaster")
        codigoCedis = dr.Item("CodigoCedis")
        nombreCedis = dr.Item("NombreCedis")
        placaRemolque = dr.Item("PlacaRemolque")
        placaTracto = dr.Item("PlacaTracto")
        determinante = dr.Item("Determinante")
        tiempoViaje = dr.Item("TiempoViaje")
        tiempoEstimadoArribo = dr.Item("TiempoEstimadoArribo")
        kilometrosRecorridos = dr.Item("KilometrosRecorridos")
        totalAlertas = dr.Item("TotalAlertas")
        semaforo = dr.Item("Semaforo")
        nombreCompletoCedis = nombreCedis & " - " & codigoCedis

        dictionary = New Dictionary(Of String, String)
        dictionary.Add("IdEmbarque", idEmbarque)
        dictionary.Add("IdMaster", idMaster)
        dictionary.Add("CodigoCedis", codigoCedis)
        dictionary.Add("NombreCedis", nombreCedis)
        dictionary.Add("NombreCompletoCedis", nombreCompletoCedis)
        dictionary.Add("PlacaRemolque", placaRemolque)
        dictionary.Add("PlacaTracto", placaTracto)
        dictionary.Add("Determinante", determinante)
        dictionary.Add("TiempoViaje", tiempoViaje)
        dictionary.Add("TiempoEstimadoArribo", tiempoEstimadoArribo)
        dictionary.Add("KilometrosRecorridos", kilometrosRecorridos)
        dictionary.Add("TotalAlertas", totalAlertas)
        dictionary.Add("Semaforo", semaforo)
        listOjectDetalle.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectDetalle)

    End If

    Return listOject

  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function getEmbarqueEnTienda(ByVal idTransportista As Integer) As List(Of Object)
    Dim dictionary As Dictionary(Of String, String)
    Dim listOjectResumen As New List(Of Object)()
    Dim listOjectDetalle As New List(Of Object)()
    Dim listOject As New List(Of Object)()
    Dim idEmbarque, idMaster, codigoCedis, nombreCedis, placaRemolque, placaTracto, determinante, fechaLlegada, fechaCita, tipoTienda, tiempoSobreestadia As String
    Dim estadoEnTienda, semaforo, nombreCompletoCedis As String

    'Obtengo datatable con datos
    Dim ds As DataSet = Carrier.getEmbarqueEnTienda(idTransportista)
    If (Not ds Is Nothing) Then

      For Each row As DataRow In ds.Tables(0).Rows
        dictionary = New Dictionary(Of String, String)
        For Each column As DataColumn In ds.Tables(0).Columns
          Dim valor As String = IIf(IsDBNull(row(column)), String.Empty, row(column))
          dictionary.Add(column.ColumnName, valor)
        Next

        listOjectResumen.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectResumen)

      For Each dr In ds.Tables(1).Rows
        idEmbarque = dr.Item("IdEmbarque")
        idMaster = dr.Item("IdMaster")
        codigoCedis = dr.Item("CodigoCedis")
        nombreCedis = dr.Item("NombreCedis")
        placaRemolque = dr.Item("PlacaRemolque")
        placaTracto = dr.Item("PlacaTracto")
        determinante = dr.Item("Determinante")
        fechaLlegada = dr.Item("FechaLlegada")
        fechaCita = dr.Item("FechaCita")
        tipoTienda = dr.Item("TipoTienda")
        tiempoSobreestadia = dr.Item("TiempoSobreestadia")
        estadoEnTienda = dr.Item("EstadoEnTienda")
        semaforo = dr.Item("Semaforo")
        nombreCompletoCedis = nombreCedis & " - " & codigoCedis

        dictionary = New Dictionary(Of String, String)
        dictionary.Add("IdEmbarque", idEmbarque)
        dictionary.Add("IdMaster", idMaster)
        dictionary.Add("CodigoCedis", codigoCedis)
        dictionary.Add("NombreCedis", nombreCedis)
        dictionary.Add("NombreCompletoCedis", nombreCompletoCedis)
        dictionary.Add("PlacaRemolque", placaRemolque)
        dictionary.Add("PlacaTracto", placaTracto)
        dictionary.Add("Determinante", determinante)
        dictionary.Add("FechaLlegada", fechaLlegada)
        dictionary.Add("FechaCita", fechaCita)
        dictionary.Add("TipoTienda", tipoTienda)
        dictionary.Add("TiempoSobreestadia", tiempoSobreestadia)
        dictionary.Add("EstadoEnTienda", estadoEnTienda)
        dictionary.Add("Semaforo", semaforo)
        listOjectDetalle.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectDetalle)

    End If

    Return listOject

  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function getTractoRegresando(ByVal idTransportista As Integer) As List(Of Object)
    Dim dictionary As Dictionary(Of String, String)
    Dim listOjectResumen As New List(Of Object)()
    Dim listOjectDetalle As New List(Of Object)()
    Dim listOject As New List(Of Object)()
    Dim placaTracto, codigoCedis, nombreCedis, tiempoLlegadaDestino, kilometrosDestino, tipoCarga, tipoRegreso, semaforo, nombreCompletoCedis As String

    'Obtengo datatable con datos
    Dim ds As DataSet = Carrier.getTractoRegresando(idTransportista)
    If (Not ds Is Nothing) Then

      For Each row As DataRow In ds.Tables(0).Rows
        dictionary = New Dictionary(Of String, String)
        For Each column As DataColumn In ds.Tables(0).Columns
          Dim valor As String = IIf(IsDBNull(row(column)), String.Empty, row(column))
          dictionary.Add(column.ColumnName, valor)
        Next

        listOjectResumen.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectResumen)

      For Each dr In ds.Tables(1).Rows
        placaTracto = dr.Item("PlacaTracto")
        codigoCedis = dr.Item("CodigoCedis")
        nombreCedis = dr.Item("NombreCedis")
        tiempoLlegadaDestino = dr.Item("TiempoLlegadaDestino")
        kilometrosDestino = dr.Item("KilometrosDestino")
        tipoCarga = dr.Item("TipoCarga")
        tipoRegreso = dr.Item("TipoRegreso")
        semaforo = dr.Item("Semaforo")
        nombreCompletoCedis = nombreCedis & " - " & codigoCedis

        dictionary = New Dictionary(Of String, String)
        dictionary.Add("PlacaTracto", placaTracto)
        dictionary.Add("CodigoCedis", codigoCedis)
        dictionary.Add("NombreCedis", nombreCedis)
        dictionary.Add("NombreCompletoCedis", nombreCompletoCedis)
        dictionary.Add("TiempoLlegadaDestino", tiempoLlegadaDestino)
        dictionary.Add("KilometrosDestino", kilometrosDestino)
        dictionary.Add("TipoCarga", tipoCarga)
        dictionary.Add("TipoRegreso", tipoRegreso)
        dictionary.Add("Semaforo", semaforo)
        listOjectDetalle.Add(dictionary)
      Next

      'Se agrega al listado final
      listOject.Add(listOjectDetalle)

    End If

    Return listOject

  End Function

End Class