﻿Imports CapaNegocio
Imports System.Data

Public Class waAlerta
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim opcion As String = Server.UrlDecode(Request("op"))
    Dim respuesta As String
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      Select Case opcion
        Case "ObtenerAlertasPorAtender"
          respuesta = ObtenerAlertasPorAtender()
        Case "ObtenerAlertasSinAtender"
          respuesta = ObtenerAlertasSinAtender()
        Case "ObtenerAlertasAtendidasHoy"
          respuesta = ObtenerAlertasAtendidasHoy()
        Case "ObtenerDetalleAlerta"
          respuesta = ObtenerDetalleAlerta()
        Case "GrabarGestionAlerta_TeleOperador"
          respuesta = GrabarGestionAlerta_TeleOperador()
        Case "ObtenerComboExplicacion"
          respuesta = ObtenerComboExplicacion()
        Case "DesvincularAlerta"
          respuesta = DesvincularAlerta()
        Case "ObtenerAlertasPorGestionar"
          respuesta = ObtenerAlertasPorGestionar()
        Case "GrabarGestionAlerta_Supervisor"
          respuesta = GrabarGestionAlerta_Supervisor()
        Case "ObtenerAlertasAtendidasHoyEnTabla"
          respuesta = ObtenerAlertasAtendidasHoyEnTabla()
        Case "EliminarAlertaConfiguracion"
          respuesta = EliminarAlertaConfiguracion()
        Case "ObtenerAlertasSinAtenderEnTabla"
          respuesta = ObtenerAlertasSinAtenderEnTabla()
        Case "GrabarEstadoLlamada"
          respuesta = GrabarEstadoLlamada()
        Case "ValidarClaveUsuarioConectado"
          respuesta = ValidarClaveUsuarioConectado()
        Case "EliminarDefinicionAlerta"
          respuesta = EliminarDefinicionAlerta()
        Case "ObtenerPerfilesPorFormato"
          respuesta = ObtenerPerfilesPorFormato()
        Case Else
          respuesta = Sistema.eCodigoSql.Error
      End Select
      Response.Write(respuesta)
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub

#Region "ObtenerAlertasPorAtender"
  ''' <summary>
  ''' obtiene listado de alertas por atender
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAlertasPorAtender() As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim totalRegistros, contador As Integer
    Dim templateTotal, templateTotalAux, templateColaAtencion, templateColaAtencionAux As String
    Dim idAlerta, nroTransporte, nombreAlerta, prioridad, total, tipoAlerta, fecha As String
    Dim sb As New StringBuilder
    Dim sl As New SortedList
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim htmlSinRegistro As String = "<em class=""help-block text-center"">No hay alertas por atender</em>"
    Dim arrPrioridad As String() = {"Alta", "Media", "Baja"}
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()

      templateTotal = "<div class=""alert alert-{TIPO_ALERTA} text-center sist-padding-cero sist-margin-bottom-5"">" & _
                      "  {PRIORIDAD}: <strong class=""sist-font-size-20"">{TOTAL}</strong>" & _
                      "</div>"

      templateColaAtencion = "<div class=""alert alert-{TIPO_ALERTA} sist-padding-5 sist-margin-bottom-2 sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta:'{ID_ALERTA}', idHistorialEscalamiento:'-1' })"">" & _
                             "  <small>" & _
                             "    <strong>#{INDICE}</strong><br />" & _
                             "    <strong>{NOMBRE_ALERTA}</strong><br />" & _
                             "    <strong>IdMaster: </strong>{NRO_TRANSPORTE}<br />" & _
                             "    <strong>Fecha: </strong>{FECHA}<br />" & _
                             "  </small>" & _
                             "</div>"

      ds = Alerta.ObtenerAlertasPorAtender(oUsuario.Id)
      If (ds Is Nothing) Then
        For Each prioridad In arrPrioridad
          templateTotalAux = templateTotal
          tipoAlerta = Utilidades.ObtenerStyleAlertaPorPrioridad(prioridad)

          'reemplaza las marcas
          templateTotalAux = templateTotalAux.Replace("{TIPO_ALERTA}", tipoAlerta)
          templateTotalAux = templateTotalAux.Replace("{PRIORIDAD}", prioridad)
          templateTotalAux = templateTotalAux.Replace("{TOTAL}", "0")
          sb.Append(templateTotalAux)
        Next

        'agrega listado a json final
        sl.Add("listadoTotalAlertas", sb.ToString())
        sl.Add("listadoColaAtencion", htmlSinRegistro)
      Else
        For Each prioridad In arrPrioridad
          templateTotalAux = templateTotal
          dt = ds.Tables(0)
          dv = dt.DefaultView
          dv.RowFilter = "Prioridad = '" & prioridad & "'"
          total = dv.Count
          dv.RowFilter = ""
          tipoAlerta = Utilidades.ObtenerStyleAlertaPorPrioridad(prioridad)

          'reemplaza las marcas
          templateTotalAux = templateTotalAux.Replace("{TIPO_ALERTA}", tipoAlerta)
          templateTotalAux = templateTotalAux.Replace("{PRIORIDAD}", prioridad)
          templateTotalAux = templateTotalAux.Replace("{TOTAL}", Utilidades.FormatearConSeparadorMiles(total))
          sb.Append(templateTotalAux)
        Next
        'agrega listado a json final
        sl.Add("listadoTotalAlertas", sb.ToString())

        '----------------------------------------------------------
        'obtiene el listado de las alertas
        '----------------------------------------------------------
        sb = New StringBuilder
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros = 0) Then
          sb.Append(htmlSinRegistro)
        Else
          contador = 0

          For Each dr In ds.Tables(0).Rows
            templateColaAtencionAux = templateColaAtencion
            idAlerta = dr.Item("IdAlerta")
            nroTransporte = dr.Item("NroTransporte")
            nombreAlerta = dr.Item("NombreAlerta")
            prioridad = dr.Item("Prioridad")
            fecha = dr.Item("FechaHoraCreacionDMA")
            tipoAlerta = Utilidades.ObtenerStyleAlertaPorPrioridad(prioridad)

            'reemplaza las marcas
            templateColaAtencionAux = templateColaAtencionAux.Replace("{INDICE}", totalRegistros - contador)
            templateColaAtencionAux = templateColaAtencionAux.Replace("{ID_ALERTA}", idAlerta)
            templateColaAtencionAux = templateColaAtencionAux.Replace("{TIPO_ALERTA}", tipoAlerta)
            templateColaAtencionAux = templateColaAtencionAux.Replace("{NOMBRE_ALERTA}", Server.HtmlEncode(nombreAlerta))
            templateColaAtencionAux = templateColaAtencionAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateColaAtencionAux = templateColaAtencionAux.Replace("{FECHA}", fecha)
            sb.Append(templateColaAtencionAux)

            contador += 1
          Next
        End If
        'agrega listado a json final
        sl.Add("listadoColaAtencion", "<div class=""sist-width-99-porciento"">" & sb.ToString() & "</div>")

      End If

      retorno = MyJSON.ConvertSortedListToJSON(sl)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region

#Region "ObtenerAlertasSinAtender"
  ''' <summary>
  ''' obtiene listado de alertas sin atender
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAlertasSinAtender() As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim totalRegistros, contador As Integer
    Dim templateColaSinAtencion, templateColaSinAtencionAux As String
    Dim nombreAlerta, nroTransporte, fechaAlerta, atendidoPor, prioridad, tipoAlerta, idAlerta, asignadoA, clasificacion As String
    Dim sb As New StringBuilder
    Dim sl As New SortedList
    Dim dr As DataRow
    Dim htmlSinRegistro As String = "<em class=""help-block text-center"">No hay alertas sin atender</em>"

    Try
      templateColaSinAtencion = "<div class=""alert alert-{TIPO_ALERTA} sist-padding-5 sist-margin-bottom-5 sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta:'{ID_ALERTA}', idHistorialEscalamiento:'-1', sinAtender:true, asignadoA:'{ATENDIDO_POR}', fechaRecepcion:'{FECHA_ALERTA}', clasificacion: '{CLASIFICACION}' })"">" & _
                                "  <small>" & _
                                "  <strong>#{INDICE}</strong><br />" & _
                                "    <div class=""sist-padding-top-3""><strong>{NOMBRE_ALERTA}</strong></div>" & _
                                "    <div class=""sist-padding-top-3""><strong>IdMaster: </strong>{NRO_TRANSPORTE}</div>" & _
                                "    <div class=""sist-padding-top-3""><strong>Fecha: </strong><br /><span id=""{ID_ALERTA}_lblFecha"">{FECHA_ALERTA}</span></div>" & _
                                "    <div class=""sist-padding-top-3""><strong>Asignado: </strong><br />{ASIGNADO_A}</div>" & _
                                "    <div class=""sist-padding-top-3""><strong>Tiempo espera: </strong><br /><span id=""{ID_ALERTA}_lblCronometro"">00:00:00</span></div>" & _
                                "    <input type=""hidden"" id=""{ID_ALERTA}_txtIdAlerta"" value=""{ID_ALERTA}"" class=""alertas-sin-atender-id"">" & _
                                "  </small>" & _
                                "</div>"

      ds = Alerta.ObtenerAlertasSinAtender()
      If (ds Is Nothing) Then
        sb.Append(htmlSinRegistro)
      Else
        totalRegistros = ds.Tables(Alerta.eTablaAlertasSinAtender.T00_Listado).Rows.Count

        If (totalRegistros = 0) Then
          sb.Append(htmlSinRegistro)
        Else
          contador = 0

          sb.Append("<ul id=""ulAlertasSinAtender"" class=""list-unstyled"">")
          For Each dr In ds.Tables(Alerta.eTablaAlertasSinAtender.T00_Listado).Rows
            templateColaSinAtencionAux = templateColaSinAtencion
            idAlerta = dr.Item("IdAlerta")
            nombreAlerta = dr.Item("NombreAlerta")
            nroTransporte = dr.Item("NroTransporte")
            fechaAlerta = dr.Item("Fecha")
            atendidoPor = dr.Item("AtendidoPor")
            prioridad = dr.Item("Prioridad")
            clasificacion = dr.Item("Clasificacion")
            tipoAlerta = Utilidades.ObtenerStyleAlertaPorPrioridad(prioridad)

            'reemplaza las marcas
            asignadoA = IIf(atendidoPor.ToUpper() = "SIN ASIGNAR", "<em>" & atendidoPor & "</em>", atendidoPor)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{INDICE}", totalRegistros - contador)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{ID_ALERTA}", idAlerta)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{TIPO_ALERTA}", tipoAlerta)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{NOMBRE_ALERTA}", nombreAlerta)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{FECHA_ALERTA}", fechaAlerta)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{ATENDIDO_POR}", atendidoPor)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{ASIGNADO_A}", asignadoA)
            templateColaSinAtencionAux = templateColaSinAtencionAux.Replace("{CLASIFICACION}", clasificacion)
            sb.Append("<li>" & templateColaSinAtencionAux & "</li>")

            contador += 1
          Next
          sb.Append("</ul>")
        End If
        'agrega listado a json final
        sl.Add("listadoColaSinAtencion", "<div class=""sist-width-99-porciento"">" & sb.ToString() & "</div>")

      End If

      retorno = MyJSON.ConvertSortedListToJSON(sl)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region

#Region "ObtenerAlertasAtendidasHoy"
  ''' <summary>
  ''' obtiene listado de alertas atendidas el día de hoy
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAlertasAtendidasHoy() As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim templateDatosAlerta, templateDestacado, templateNormal, templateAux As String
    Dim idAlerta, nroTransporte, nroEscalamiento, nombreAlerta, prioridad, cerrado As String
    Dim sb As New StringBuilder
    Dim sl As New SortedList
    Dim dr As DataRow
    Dim totalRegistros, contador As Integer
    Dim htmlSinRegistro As String = "<em class=""help-block text-center"">No hay alertas atendidas el d&iacute;a de hoy</em>"

    Try
      templateDatosAlerta = "<small>" & _
                            "  <strong>#{INDICE}</strong><br />" & _
                            "  <strong>{NOMBRE_ALERTA}</strong><br />" & _
                            "  <strong>IdMaster: </strong>{NRO_TRANSPORTE}<br />" & _
                            "  <strong>Prioridad: </strong>{PRIORIDAD}<br />" & _
                            "  <strong>Nro. Escalamiento: </strong>{NRO_ESCALAMIENTO}<br />" & _
                            "  <strong>Cerrado: </strong>{CERRADO}<br />" & _
                            "</small>"

      templateDestacado = "<div class=""alert alert-info sist-padding-5 sist-margin-bottom-5 sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta: '{ID_ALERTA}', idHistorialEscalamiento:'-1' })"">" & _
                          templateDatosAlerta & _
                          "</div>"

      templateNormal = "<div class=""panel panel-default sist-margin-bottom-5 sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta: '{ID_ALERTA}', idHistorialEscalamiento:'-1' })"">" & _
                       "   <div class=""panel-body sist-padding-5"">" & _
                       templateDatosAlerta & _
                       "   </div>" & _
                       " </div>"

      ds = Alerta.ObtenerAlertasAtendidasHoy()
      If (ds Is Nothing) Then
        sb.Append(htmlSinRegistro)
      Else
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros = 0) Then
          sb.Append(htmlSinRegistro)
        Else
          contador = 0

          sb.Append("<ul id=""ulAlertasAtendidasHoy"" class=""list-unstyled"">")
          For Each dr In ds.Tables(0).Rows
            idAlerta = dr.Item("IdAlerta")
            nombreAlerta = dr.Item("NombreAlerta")
            nroTransporte = dr.Item("NroTransporte")
            prioridad = dr.Item("Prioridad")
            nroEscalamiento = dr.Item("NroEscalamiento")
            cerrado = dr.Item("Cerrado")
            templateAux = IIf(nroEscalamiento >= 2, templateDestacado, templateNormal)

            'reemplaza las marcas
            prioridad = Utilidades.ObtenerLabelPrioridad(prioridad)
            cerrado = IIf(cerrado = "1", "SI", "NO")
            templateAux = templateAux.Replace("{INDICE}", totalRegistros - contador)
            templateAux = templateAux.Replace("{ID_ALERTA}", idAlerta)
            templateAux = templateAux.Replace("{NOMBRE_ALERTA}", Server.HtmlEncode(nombreAlerta))
            templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateAux = templateAux.Replace("{PRIORIDAD}", prioridad)
            templateAux = templateAux.Replace("{NRO_ESCALAMIENTO}", nroEscalamiento)
            templateAux = templateAux.Replace("{CERRADO}", cerrado)
            sb.Append("<li>" & templateAux & "</li>")

            contador += 1
          Next
          sb.Append("</ul>")
        End If
      End If
      'agrega listado a json final
      sl.Add("listado", "<div class=""sist-width-99-porciento"">" & sb.ToString() & "</div>")

      retorno = MyJSON.ConvertSortedListToJSON(sl)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region

#Region "ObtenerDetalleAlerta"
  ''' <summary>
  ''' obtiene detalle de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim idAlerta As String = Utilidades.IsNull(Request("idAlerta"), "-1")
    Dim sb As New StringBuilder
    Dim totalRegistros, status As Integer
    Dim dt As DataTable
    Dim slAlertaGestionAcumulada As New SortedList
    Dim idAlertaHija As String = "-1"
    Dim idAlertaGestionAcumulada As String = "-1"
    Dim cerrado As String = "0"
    Dim cerradoSatisfactorio As String = "0"
    Dim fechaAsignacion As String = ""
    Dim revisarAlertaAsignadaPreviamente As Boolean = False
    Dim ultimaGestionUsuarioConectado, ultimaGestionAlerta, ultimoLogSesion As String
    Dim dsAuxiliarTeleoperador As New DataSet

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      dsAuxiliarTeleoperador = Alerta.ObtenerDatosAuxiliarPorAccion("", "-1") 'inicialia el dataset

      'si idAlerta = -1 y el usuario es perefil Teleoperador entonces se le asigna una alerta
      If (idAlerta = "-1" And oUsuario.IdPerfil = Perfil.eTipo.TeleOperador) Then

        'verifica si el usuario conectado esta en algun estado auxiliar y aun no ha cerrado la ventana con su contraseña
        ultimaGestionUsuarioConectado = Utilidades.IsNull(Alerta.ObtenerUltimaGestion("-1", oUsuario.Id), "")
        ultimoLogSesion = Utilidades.IsNull(Sistema.ObtenerUltimoLogSesion(oUsuario.Id), "")

        If (ultimaGestionUsuarioConectado.StartsWith("TELEOPERADOR_AUXILIAR_") And Not ultimoLogSesion.StartsWith("Valida clave cierre estado auxiliar")) Then
          dsAuxiliarTeleoperador = Alerta.ObtenerDatosAuxiliarPorAccion(ultimaGestionUsuarioConectado, oUsuario.Id)
          idAlertaGestionAcumulada = "-1"
          idAlerta = "-1"
          idAlertaHija = "-1"
          Sistema.GrabarLogSesion(oUsuario.Id, "Estado auxiliar: " & ultimaGestionUsuarioConectado)

          '--------------------------------------
          'verifica que si su ultimo estado es auxiliar y si no hay alertas en el sistema, entonces elimina este ultimo estado para que no se quede pegado
          'en el ultimo estado auxiliar (esto ocurre cuando colocan estado auxiliar y despues cierran el navegador sin cerrar sesion)
          ds = Alerta.ObtenerAlertasPorAtender(oUsuario.Id)
          If (Not ds Is Nothing) Then
            dt = ds.Tables(0)
            totalRegistros = dt.Rows.Count
          Else
            totalRegistros = 0
          End If

          If (ultimaGestionUsuarioConectado.StartsWith("TELEOPERADOR_AUXILIAR_") And totalRegistros = 0) Then
            status = Alerta.EliminarUltimoHistorialGestionPorUsuario(oUsuario.Id)
          End If
          '--------------------------------------
        Else
          'verifica si ya tiene una alerta asignada entonces la vuelve a cargar
          ds = Alerta.ObtenerAsignadaActualmente(oUsuario.Id)
          If (ds Is Nothing) Then
            totalRegistros = 0
          Else
            dt = ds.Tables(0)
            totalRegistros = dt.Rows.Count
            If (totalRegistros > 0) Then
              idAlertaGestionAcumulada = dt.Rows(0).Item("IdAlertaGestionAcumulada")
              idAlerta = dt.Rows(0).Item("IdAlerta")
              idAlertaHija = dt.Rows(0).Item("IdAlertaHija")
              fechaAsignacion = dt.Rows(0).Item("FechaAsignacion")
              revisarAlertaAsignadaPreviamente = True
            End If
          End If

          'si no tiene alerta asignada previamente, obtiene la primera alerta que esta en la cola de atencion y lo asigna
          If (totalRegistros = 0) Then
            ds = Alerta.ObtenerAlertasPorAtender(oUsuario.Id)
            If (Not ds Is Nothing) Then
              dt = ds.Tables(0)
              totalRegistros = dt.Rows.Count

              If (totalRegistros > 0) Then
                For Each dr As DataRow In dt.Rows
                  idAlertaGestionAcumulada = "-1"
                  idAlerta = dr.Item("IdAlerta")
                  idAlertaHija = dr.Item("IdAlertaHija")

                  'revisa si la alerta ya fue asignada previamente, y no quede asociada a mas de un usuario a la vez
                  Utilidades.Delay(2000)
                  ultimaGestionAlerta = Utilidades.IsNull(Alerta.ObtenerUltimaGestion(idAlerta, "-1"), "")

                  If (ultimaGestionAlerta <> Alerta.eAccionHistorial.TELEOPERADOR_ALERTA_ASIGNADA.ToString()) Then
                    'graba registro en las alertas acumuladas
                    idAlertaGestionAcumulada = Alerta.GrabarAlertaGestionAcumulada(idAlertaGestionAcumulada, fechaAsignacion, idAlerta, idAlertaHija, oUsuario.Id, cerrado, cerradoSatisfactorio, "-1")
                    If (idAlertaGestionAcumulada = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar la alerta acumulada")

                    'graba registro en historial alerta gestion
                    status = Alerta.GrabarHistorialAlertaGestion(idAlerta, idAlertaHija, oUsuario.Id, Alerta.eAccionHistorial.TELEOPERADOR_ALERTA_ASIGNADA.ToString())
                    If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar el historial de la gestión de la alerta")
                    Sistema.GrabarLogSesion(oUsuario.Id, "Se asigna alerta: IdAlerta " & idAlerta)

                    'se actualiza la asignacion de la alerta
                    status = Alerta.ActualizarAsignacionAlertaEnCola(idAlerta, idAlertaHija, oUsuario.Id)

                    'una vez asignada la alerta se sale del ciclo
                    Exit For
                  Else
                    'si ya esta asignada entonces no muestra los datos de la id consultada
                    idAlertaGestionAcumulada = "-1"
                    idAlerta = "-1"
                    idAlertaHija = "-1"
                  End If
                Next '<-- For Each dr As DataRow In dt.Rows

              End If '<-- If (totalRegistros > 0) Then
            End If '<-- If (Not ds Is Nothing) Then
          End If '<-- If (totalRegistros = 0) Then
        End If '<-- If (ultimaGestionUsuarioConectado.StartsWith("TELEOPERADOR_AUXILIAR_") And Not ultimoLogSesion.StartsWith("Valida clave cierre estado auxiliar")) Then
      End If '<-- If (idAlerta = "-1" And oUsuario.IdPerfil = Perfil.eTipo.TeleOperador) Then

      'asigna los datos de la gestion acumulada para grabarlo en el json de retorno
      slAlertaGestionAcumulada.Add("idAlertaGestionAcumulada", idAlertaGestionAcumulada)
      slAlertaGestionAcumulada.Add("idAlertaHija", idAlertaHija)
      slAlertaGestionAcumulada.Add("fechaAsignacion", fechaAsignacion)
      slAlertaGestionAcumulada.Add("revisarAlertaAsignadaPreviamente", revisarAlertaAsignadaPreviamente)

      'obtiene los datos de la alerta consultada
      ds = Alerta.ObtenerDetalle(idAlerta)

      'agrega listado a json final
      sb.Append("{")
      sb.Append(" ""detalle"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(Alerta.eTabla.T00_Detalle)))
      sb.Append(",""historialEscalamientos"":" & ObtenerDetalleAlerta_HistorialGestionAlertas(ds))
      sb.Append(",""contactosEscalamiento"":" & ObtenerDetalleAlerta_ContactosEscalamiento(ds))
      sb.Append(",""alertaGestionAcumulada"":" & MyJSON.ConvertSortedListToJSON(slAlertaGestionAcumulada))
      sb.Append(",""comboContactadoEscalamientoAnterior"":" & MyJSON.ConvertObjectToJSON(ObtenerComboContactadoEscalamientoAnterior(ds)))
      sb.Append(",""historialLlamadas"":" & ObtenerDetalleAlerta_HistorialLlamadas(ds))
      sb.Append(",""listadoAuxiliares"":" & ObtenerAuxiliaresTeleoperador())
      sb.Append(",""estadoAuxiliarActual"":" & MyJSON.ConvertDataTableToJSON(dsAuxiliarTeleoperador.Tables(0)))
      sb.Append("}")

      retorno = sb.ToString()
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function

  ''' <summary>
  ''' dibuja el historial de escalamientos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta_HistorialEscalamientos(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim template, templateAux, idHistorialEscalamiento, nroEscalamiento, fecha, listadoContactos, nombreUsuario, cargo, explicacion As String
    Dim explicacionContacto, listadoExplicacion, idAlerta, atendidoPor As String
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim htmlSinRegistro As String = "<em class=""help-block"">No hay escalamientos asociados</em>"

    Try
      template = "<tr class=""sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta: '{ID_ALERTA}', idHistorialEscalamiento:'{ID_HISTORIAL_ESCALAMIENTO}' })"">" & _
                 "  <td>{NRO_ESCALAMIENTO}</td>" & _
                 "  <td>{FECHA}</td>" & _
                 "  <td>{LISTADO_CONTACTOS}</td>" & _
                 "  <td>{EXPLICACION}</td>" & _
                 "  <td>{ATENDIDO_POR}</td>" & _
                 "</tr>"

      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        sb.Append(htmlSinRegistro)
      Else
        sb.Append("<table class=""table table-hover table-condensed sist-width-100-porciento"">")
        sb.Append("  <thead>")
        sb.Append("    <tr>")
        sb.Append("      <th>Escalamiento</th>")
        sb.Append("      <th>Fecha</th>")
        sb.Append("      <th>Contacto</th>")
        sb.Append("      <th>Explicaci&oacute;n</th>")
        sb.Append("      <th>Atendido Por</th>")
        sb.Append("    </tr>")
        sb.Append("  </thead>")
        sb.Append("  <tbody>")

        For Each dr In dt.Rows
          templateAux = template
          listadoContactos = ""
          listadoExplicacion = ""
          idHistorialEscalamiento = dr.Item("IdHistorialEscalamiento")
          nroEscalamiento = dr.Item("NroEscalamiento")
          fecha = dr.Item("Fecha")
          explicacion = dr.Item("Explicacion")
          idAlerta = dr.Item("IdAlerta")
          atendidoPor = dr.Item("AtendidoPor")

          '---------------------------------------------------
          'obtiene los contactos por escalamiento
          dv = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos).DefaultView
          dv.RowFilter = "IdHistorialEscalamiento = '" & idHistorialEscalamiento & "'"
          dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          For Each drContacto As DataRow In dsFiltrado.Tables(0).Rows
            nombreUsuario = drContacto.Item("NombreUsuario")
            cargo = drContacto.Item("Cargo")
            explicacionContacto = drContacto.Item("Explicacion")

            nombreUsuario = "<div>" & Server.HtmlEncode(nombreUsuario) & IIf(String.IsNullOrEmpty(cargo), "", " (" & Server.HtmlEncode(cargo) & ")") & "</div>"
            listadoContactos &= nombreUsuario

            explicacionContacto = "<div>" & Server.HtmlEncode(explicacionContacto) & "</div>"
            listadoExplicacion &= explicacionContacto
          Next

          explicacion = IIf(String.IsNullOrEmpty(explicacion), "&nbsp;", Server.HtmlEncode(explicacion))
          listadoContactos = IIf(String.IsNullOrEmpty(listadoContactos), "&nbsp;", listadoContactos)
          listadoExplicacion = IIf(String.IsNullOrEmpty(listadoExplicacion), explicacion, listadoExplicacion)
          '---------------------------------------------------

          'remplaza marcas
          templateAux = templateAux.Replace("{ID_ALERTA}", idAlerta)
          templateAux = templateAux.Replace("{ID_HISTORIAL_ESCALAMIENTO}", idHistorialEscalamiento)
          templateAux = templateAux.Replace("{NRO_ESCALAMIENTO}", nroEscalamiento)
          templateAux = templateAux.Replace("{FECHA}", fecha)
          templateAux = templateAux.Replace("{LISTADO_CONTACTOS}", listadoContactos)
          templateAux = templateAux.Replace("{EXPLICACION}", listadoExplicacion)
          templateAux = templateAux.Replace("{ATENDIDO_POR}", atendidoPor)
          sb.Append(templateAux)
        Next

        sb.Append("  </tbody>")
        sb.Append("</table>")
      End If

      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    html = MyJSON.ConvertObjectToJSON(html)
    Return html
  End Function

  ''' <summary>
  ''' dibuja los contactos del escalamiento que debe registrar
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta_ContactosEscalamiento(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim sbContactos As New StringBuilder
    Dim totalRegistros As Integer
    Dim template, templateAux, cargo, telefono, selectExplicacion, scriptGrupo, displayBotonScript, nombreConductor, localDestinoCodigo, localOrigenNombre, nombreTransportista As String
    Dim dt As DataTable
    Dim dr As DataRow
    Dim dsFiltrado As New DataSet
    Dim arrayGroupBy As New ArrayList
    Dim mostrarComboItemSeleccione, noContestaEscalamientoAnterior As Boolean
    Dim dv As DataView
    Dim idEscalamientoPorAlertaGrupoContacto, mostrarCuandoEscalamientoAnteriorNoContesta, tieneDependenciaGrupoEscalamientoAnterior, idGrupoContactoEscalamientoAnterior As String
    Dim mostrarSiempre, idGrupoContactoPadre, displayGrupo, idAlerta, nroEscalamientoActual, idEscalamientoPorAlertaGrupoContactoUltimoEscalamiento, selectContactos As String
    Dim nombreAlerta, colSMSelectContactos, localDestinoNombre, displayTelefono, agrupadoEn, rutTransportista, rutConductor, llavePerfil, tipoGrupo, localOrigenCodigo, idFormato As String
    Dim slInformacionUltimoContactoEscalamientoAnterior As New SortedList
    Dim slInformacionAlerta As New SortedList
    Dim jsonContactos As String = "[]"

    Try
      template = "<div id=""{ID_UNICO}_hGrupoContacto"" class=""sist-margin-bottom-5 {DISPLAY_GRUPO}"" rel=""relGrupoHijo_GrupoPadre{ID_GRUPO_CONTACTO_PADRE}"">" & _
                 "  <div class=""row"">" & _
                 "		<div class=""col-xs-4"">" & _
                 "		  <label class=""control-label"">Llamar a {AGRUPADO_EN}</label>" & _
                 "      <div>" & _
                 "        <div class=""col-sm-2 sist-padding-cero show-tooltip {DISPLAY_BOTON_SCRIPT}"" data-toggle=""tooltip"" data-placement=""bottom"" title=""muestra y oculta script""><button type=""button"" class=""btn btn-default"" onclick=""Alerta.mostrarScriptGrupoContacto({ prefijoControl: '{ID_UNICO}' })""><span class=""glyphicon glyphicon-bullhorn""></span></button></div>" & _
                 "	  	  <div class=""{COL_SM_SELECT_CONTACTOS}"">{SELECT_CONTACTOS}</div>" & _
                 "      </div>" & _
                 "		</div>" & _
                 "		<div class=""col-xs-2"">" & _
                 "		  <label class=""control-label"">&nbsp;</label>" & _
                 "		  <div id=""{ID_UNICO}_hTelefono"" class=""sist-cursor-pointer {DISPLAY_TELEFONO}"">" & _
                 "		    <span id=""{ID_UNICO}_lblLlamarTelefono""><label class=""control-label""><span class=""glyphicon glyphicon-phone-alt""></span></label>&nbsp;<span id=""{ID_UNICO}_lblTelefono"" class=""show-tooltip"">{TELEFONO}</span></span>" & _
                 "		    <span id=""{ID_UNICO}_lblFinalizarLlamada"" class=""sist-display-none"">&nbsp;<button type=""button"" class=""btn btn-sm btn-danger show-tooltip"" data-toggle=""tooltip"" data-placement=""bottom"" title=""finalizar la llamada"" onclick=""Alerta.finalizarLlamado()""><span class=""glyphicon glyphicon-earphone""></span></button></span>" & _
                 "		  </div>" & _
                 "		  <div class=""small""><em><span id=""{ID_UNICO}_lblCargo"">{CARGO}</span></em></div>" & _
                 "			" & _
                 "		</div>" & _
                 "		<div class=""col-xs-3"">" & _
                 "		  <label class=""control-label"">Explicaci&oacute;n</label>" & _
                 "		  {SELECT_EXPLICACION}" & _
                 "		</div>" & _
                 "		<div class=""col-xs-3"">" & _
                 "	  	<label class=""control-label"">Observaci&oacute;n</label>" & _
                 "	  	<div><input type=""text"" id=""{ID_UNICO}_txtObservacion"" class=""form-control input-sm observacion-contacto-escalamiento"" value="""" maxlength=""4000"" /></div>" & _
                 "		</div>" & _
                 "	</div>" & _
                 "	<div id=""{ID_UNICO}_lblMensajeLlamada""></div>" & _
                 "  <div>{SCRIPT_GRUPO}</div>" & _
                 "  <input type=""hidden"" id=""{ID_UNICO}_txtJSONContacto"" value=""{JSON_CONTACTOS}"" />" & _
                 "  <input type=""hidden"" id=""{ID_UNICO}_txtAgrupadoEn"" value=""{AGRUPADO_EN}"" />" & _
                 "  <input type=""hidden"" id=""{ID_UNICO}_txtJSONExplicacion"" value="""" />" & _
                 "</div>"

      '---------------------------------------------
      'obtiene informacion de la alerta
      slInformacionAlerta = Utilidades.ObtenerInformacionAlerta(ds)
      nombreConductor = slInformacionAlerta.Item("NombreConductor")
      localDestinoCodigo = slInformacionAlerta.Item("LocalDestinoCodigo")
      localDestinoNombre = slInformacionAlerta.Item("LocalDestinoDescripcion")
      nombreTransportista = slInformacionAlerta.Item("NombreTransportista")
      idAlerta = slInformacionAlerta.Item("IdAlerta")
      nroEscalamientoActual = slInformacionAlerta.Item("NroEscalamientoActual")
      nombreAlerta = slInformacionAlerta.Item("NombreAlerta")
      rutTransportista = slInformacionAlerta.Item("RutTransportista")
      rutConductor = slInformacionAlerta.Item("RutConductor")
      localOrigenCodigo = slInformacionAlerta.Item("LocalOrigenCodigo")
      localOrigenNombre = slInformacionAlerta.Item("LocalOrigenDescripcion")
      idFormato = slInformacionAlerta.Item("IdFormato")

      '*** NO CONTESTA: DESCOMENTAR LAS LINEAS POR SI SE QUIERE UTILIZAR LOS GRUPOS QUE NO CONTESTAN DEL ESCALAMIENTO ANTERIOR, COMO FUNCIONA EN WALMART (VSR, 08/07/2015)
      ''---------------------------------------------
      ''verifica si en el escalamiento anterior hubo contactabilidad
      'noContestaEscalamientoAnterior = Utilidades.VerificaEscalamientoAnteriorNoContesta(ds, nroEscalamientoActual, nombreAlerta)

      'se asigna variable en FALSE para que siempre muestre los grupos que estan marcados "mostrarCuandoEscalamientoAnteriorNoContesta = true"
      noContestaEscalamientoAnterior = False

      '---------------------------------------------
      'obtiene informacion del ultimo contacto registrado en el escalamiento anterior
      slInformacionUltimoContactoEscalamientoAnterior = Utilidades.ObtenerInformacionUltimoContactoEscalamientoAnterior(ds, nroEscalamientoActual)
      idEscalamientoPorAlertaGrupoContactoUltimoEscalamiento = slInformacionUltimoContactoEscalamientoAnterior.Item("IdEscalamientoPorAlertaGrupoContacto")

      '---------------------------------------------
      'obtiene los contactos del proximo escalamiento
      dt = ds.Tables(Alerta.eTabla.T03_ContactosProximoEscalamiento)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        arrayGroupBy = Utilidades.ObtenerArrayGroupByDeDataTable(dt, "IdEscalamientoPorAlertaGrupoContacto")

        For Each idEscalamientoPorAlertaGrupoContacto In arrayGroupBy
          sbContactos = New StringBuilder
          templateAux = template

          'obtiene datos del grupo
          dv = dt.DefaultView
          dv.RowFilter = "idEscalamientoPorAlertaGrupoContacto = " & idEscalamientoPorAlertaGrupoContacto
          agrupadoEn = dv.Item(0).Item("AgrupadoEn")
          mostrarCuandoEscalamientoAnteriorNoContesta = dv.Item(0).Item("MostrarCuandoEscalamientoAnteriorNoContesta")
          tieneDependenciaGrupoEscalamientoAnterior = dv.Item(0).Item("TieneDependenciaGrupoEscalamientoAnterior")
          idGrupoContactoEscalamientoAnterior = dv.Item(0).Item("IdGrupoContactoEscalamientoAnterior")
          mostrarSiempre = dv.Item(0).Item("MostrarSiempre")
          idGrupoContactoPadre = dv.Item(0).Item("IdGrupoContactoPadre")
          llavePerfil = dv.Item(0).Item("LlavePerfil")
          tipoGrupo = dv.Item(0).Item("TipoGrupo")
          dv.RowFilter = ""

          'muestra los grupos dependiendo si tienen relacion cuando en el escalamiento anterior no hubo contacto, o si lo hubo
          If (CBool(mostrarCuandoEscalamientoAnteriorNoContesta) = noContestaEscalamientoAnterior) Then

            'muestra los grupos que no tienen ninguna dependencia de grupos anteriores, y los que tienen dependencia del escalamiento anterior
            If (tieneDependenciaGrupoEscalamientoAnterior = 0 Or (tieneDependenciaGrupoEscalamientoAnterior = 1 And idGrupoContactoEscalamientoAnterior = idEscalamientoPorAlertaGrupoContactoUltimoEscalamiento)) Then

              'si el grupo que tiene que dibujar es de un grupo especial, entonces lo obtiene de un spu especifico, sino lo obtiene del procedimiento
              'del detalle de la alerta
              dsFiltrado = Utilidades.ObtenerDatasetComboGrupoContactoEspecial(mostrarComboItemSeleccione, dt, agrupadoEn, rutConductor, localDestinoCodigo, rutTransportista, _
                                                                               idEscalamientoPorAlertaGrupoContacto, nombreConductor, nombreTransportista, llavePerfil, _
                                                                               tipoGrupo, localOrigenCodigo)
              scriptGrupo = Me.ObtenerDetalleAlerta_ContactosEscalamiento_ScriptGrupo(ds, agrupadoEn, idEscalamientoPorAlertaGrupoContacto, noContestaEscalamientoAnterior, idAlerta, nroEscalamientoActual, slInformacionAlerta, slInformacionUltimoContactoEscalamientoAnterior)
              displayBotonScript = IIf(String.IsNullOrEmpty(scriptGrupo), "sist-display-none", "")
              colSMSelectContactos = IIf(String.IsNullOrEmpty(scriptGrupo), "col-sm-12 sist-padding-cero", "col-sm-10 sist-padding-cero sist-padding-left-5")
              displayGrupo = IIf(mostrarSiempre = "1", "", "sist-display-none")
              selectContactos = Me.DibujarComboContacto(dsFiltrado, idEscalamientoPorAlertaGrupoContacto, mostrarComboItemSeleccione)

              'si la alerta es de un Multipunto, entonces trae explicaciones especiales para mostrar en el combo
              If (nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO) And agrupadoEn.ToUpper() = Alerta.GRUPO_CONTACTO_CONDUCTOR) Then
                selectExplicacion = Me.DibujarComboExplicacionMultipunto(idEscalamientoPorAlertaGrupoContacto)
              Else
                selectExplicacion = Me.DibujarComboExplicacionGrupoContacto(idEscalamientoPorAlertaGrupoContacto)
              End If

              'si hay contactos entonces lo dibuja
              If (Not String.IsNullOrEmpty(selectContactos)) Then
                'obtiene informacion del primer contacto
                If (mostrarComboItemSeleccione) Then
                  cargo = "-"
                  telefono = "-"
                Else
                  dr = dsFiltrado.Tables(0).Rows(0)
                  cargo = dr.Item("Cargo")
                  telefono = dr.Item("Telefono").ToString().Replace(" ", "").Replace(",", ", ")
                End If
                displayTelefono = IIf(String.IsNullOrEmpty(telefono), "sist-display-none", "")

                If (Not dsFiltrado Is Nothing) Then jsonContactos = MyJSON.ConvertDataTableToJSON(dsFiltrado.Tables(0))

                templateAux = templateAux.Replace("{SELECT_CONTACTOS}", selectContactos)
                templateAux = templateAux.Replace("{CARGO}", Server.HtmlEncode(cargo))
                templateAux = templateAux.Replace("{TELEFONO}", telefono)
                templateAux = templateAux.Replace("{ID_UNICO}", idEscalamientoPorAlertaGrupoContacto)
                templateAux = templateAux.Replace("{JSON_CONTACTOS}", Server.HtmlEncode(jsonContactos))
                templateAux = templateAux.Replace("{SELECT_EXPLICACION}", selectExplicacion)
                templateAux = templateAux.Replace("{SCRIPT_GRUPO}", scriptGrupo)
                templateAux = templateAux.Replace("{DISPLAY_BOTON_SCRIPT}", displayBotonScript)
                templateAux = templateAux.Replace("{AGRUPADO_EN}", agrupadoEn.ToUpper())
                templateAux = templateAux.Replace("{LOCAL_DESTINO_CODIGO}", Server.HtmlEncode(localDestinoCodigo))
                templateAux = templateAux.Replace("{LOCAL_DESTINO_NOMBRE}", Server.HtmlEncode(localDestinoNombre))
                templateAux = templateAux.Replace("{LOCAL_ORIGEN_CODIGO}", Server.HtmlEncode(localOrigenCodigo))
                templateAux = templateAux.Replace("{LOCAL_ORIGEN_NOMBRE}", Server.HtmlEncode(localOrigenNombre))
                templateAux = templateAux.Replace("{ID_ESCALAMIENTO_GRUPO_CONTACTO}", idEscalamientoPorAlertaGrupoContacto)
                templateAux = templateAux.Replace("{DISPLAY_GRUPO}", displayGrupo)
                templateAux = templateAux.Replace("{ID_GRUPO_CONTACTO_PADRE}", idGrupoContactoPadre)
                templateAux = templateAux.Replace("{COL_SM_SELECT_CONTACTOS}", colSMSelectContactos)
                templateAux = templateAux.Replace("{NOMBRE_CONDUCTOR}", Server.HtmlEncode(nombreConductor))
                templateAux = templateAux.Replace("{NOMBRE_TRANSPORTISTA}", Server.HtmlEncode(nombreTransportista))
                templateAux = templateAux.Replace("{DISPLAY_TELEFONO}", displayTelefono)
                templateAux = templateAux.Replace("{RUT_CONDUCTOR}", Server.HtmlEncode(rutConductor))
                templateAux = templateAux.Replace("{RUT_TRANSPORTISTA}", Server.HtmlEncode(rutTransportista))
                templateAux = templateAux.Replace("{LLAVE_PERFIL}", Server.HtmlEncode(llavePerfil))
                templateAux = templateAux.Replace("{TIPO_GRUPO}", Server.HtmlEncode(tipoGrupo))
                templateAux = templateAux.Replace("{ID_FORMATO}", Server.HtmlEncode(idFormato))
                sb.Append(templateAux)

              End If '<- If (Not String.IsNullOrEmpty(selectContactos))
            End If '<- If (tieneDependenciaGrupoEscalamientoAnterior = 0 Or (tieneDependenciaGrupoEscalamientoAnterior = 1 And idGrupoContactoEscalamientoAnterior = idEscalamientoPorAlertaGrupoContactoUltimoEscalamiento))
          End If '<- If (mostrarCuandoEscalamientoAnteriorNoContesta = CInt(noContestaEscalamientoAnterior)) Then

        Next '<- For Each agrupadoEn As String In arrayGroupBy

        html = sb.ToString()
      End If
    Catch ex As Exception
      html = ""
    End Try

    html = MyJSON.ConvertObjectToJSON(html)
    Return html
  End Function

  ''' <summary>
  ''' dibuja el script asociado al grupo de contacto
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta_ContactosEscalamiento_ScriptGrupo(ByVal ds As DataSet, ByVal agrupadoEn As String, ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal noContestaEscalamientoAnterior As Boolean, _
                                                                          ByVal idAlerta As String, ByVal nroEscalamientoActual As String, ByVal slInformacionAlerta As SortedList, ByVal slInformacionUltimoContactoEscalamientoAnterior As SortedList) As String
    Dim html As String = ""
    Dim oUtilidades As New Utilidades
    Dim template, templateAux, nombreScript, descripcion As String
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim sb As New StringBuilder
    Dim dsFiltrado As New DataSet
    Dim listadoContactosNoContesta As String = ""

    Try
      template = "<div>" & _
                 "  <strong class=""sist-font-size-16"">- {NOMBRE_SCRIPT}</strong>" & _
                 "  <p id=""{PREFIJO_CONTROL}_hScriptDescripcion"" class=""sist-font-size-14 script-descripcion"">{DESCRIPCION}</p>" & _
                 "</div>"

      dt = ds.Tables(Alerta.eTabla.T04_ScriptGrupoContacto)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        '-------------------------------------------------------------
        'verifica si en el escalamiento anterior (que es el actual que tiene asignada la alerta) hubo una NO CONTACTABILIDAD para mostrar los contactos y el script asociado
        If (noContestaEscalamientoAnterior) Then
          listadoContactosNoContesta = ObtenerDetalleAlerta_ContactosEscalamiento_ScriptGrupo_NoContesta(ds, idAlerta, nroEscalamientoActual)
        End If

        '-------------------------------------------------------------
        'busca los script asociados al grupo de contactos
        dv = dt.DefaultView
        dv.RowFilter = "IdEscalamientoPorAlertaGrupoContacto = " & idEscalamientoPorAlertaGrupoContacto
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        'vuelve a calcular cuantos registros encontro despues de hacer el filtro
        dt = dsFiltrado.Tables(0)
        totalRegistros = dt.Rows.Count

        If (totalRegistros > 0) Then
          sb.Append("<div id=""" & idEscalamientoPorAlertaGrupoContacto & "_hScript"" class=""sist-display-none sist-padding-script"">")
          sb.Append("<div class=""alert alert-success"">")
          sb.Append("  <p class=""sist-font-size-20 text-center""><strong>SCRIPT</strong></p>")

          'agrega listado de no contactados del escalamiento anterior si existen
          If (Not String.IsNullOrEmpty(listadoContactosNoContesta)) Then
            sb.Append(listadoContactosNoContesta)
          End If

          For Each dr In dt.Rows
            templateAux = template
            nombreScript = dr.Item("NombreScript")
            descripcion = dr.Item("Descripcion")

            'formatea valores
            descripcion = oUtilidades.ReemplazarMarcasScript(descripcion, slInformacionAlerta, slInformacionUltimoContactoEscalamientoAnterior)

            templateAux = templateAux.Replace("{NOMBRE_SCRIPT}", Server.HtmlEncode(nombreScript))
            templateAux = templateAux.Replace("{DESCRIPCION}", descripcion)
            templateAux = templateAux.Replace("{PREFIJO_CONTROL}", idEscalamientoPorAlertaGrupoContacto)
            sb.Append(templateAux)
          Next

          sb.Append("</div>")
          sb.Append("</div>")
          html = sb.ToString()
        End If
      End If
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' verifica si en el escalamiento anterior (que es el actual que tiene asignada la alerta) hubo una NO CONTACTABILIDAD para mostrar los contactos y el script asociado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta_ContactosEscalamiento_ScriptGrupo_NoContesta(ByVal ds As DataSet, ByVal idAlerta As String, ByVal nroEscalamientoActual As String) As String
    Dim html As String = ""
    Dim template, templateAux, idHistorialEscalamiento, nombreUsuario, cargo, telefono, agrupadoEn, explicacion As String
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim sb As New StringBuilder
    Dim dsFiltrado As New DataSet

    Try
      template = "<small class=""sist-font-size-12"">" & _
                 "  {AGRUPADO_EN}: <span>{NOMBRE_USUARIO}</span>" & _
                 "</small>"

      '----------------------------------------------------------------------
      'busca el historial asociado al escalamiento actual
      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      dv = dt.DefaultView
      dv.RowFilter = "NroEscalamiento = " & nroEscalamientoActual
      If (dv.Count = 0) Then
        idHistorialEscalamiento = "-1"
      Else
        idHistorialEscalamiento = dv.Item(0).Item("IdHistorialEscalamiento")
      End If
      dv.RowFilter = ""

      '----------------------------------------------------------------------
      'busca los contactos registrados en el historial encontrado
      dt = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos)
      dv = dt.DefaultView
      dv.RowFilter = "IdHistorialEscalamiento = " & idHistorialEscalamiento
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""

      sb.Append("<blockquote class=""sist-cursor-pointer show-tooltip"" data-toggle=""tooltip"" data-placement=""bottom"" title=""haz click para mostrar m&aacute;s detalles del escalamiento"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta: '" & idAlerta & "', idHistorialEscalamiento:'" & idHistorialEscalamiento & "' })"">")
      sb.Append("  <em>")
      sb.Append("    <p class=""sist-font-size-12""><strong>NO CONTACTADOS EN ESCALAMIENTO NRO. " & nroEscalamientoActual & "</strong></p>")

      For Each dr In dsFiltrado.Tables(0).Rows
        templateAux = template
        agrupadoEn = dr.Item("AgrupadoEn")
        nombreUsuario = dr.Item("NombreUsuario")
        cargo = dr.Item("Cargo")
        telefono = dr.Item("Telefono")
        explicacion = dr.Item("Explicacion")

        nombreUsuario = "<strong>" & Server.HtmlEncode(nombreUsuario) & "</strong>"
        nombreUsuario &= IIf(String.IsNullOrEmpty(cargo), "", " (" & Server.HtmlEncode(cargo) & ")")
        nombreUsuario &= IIf(String.IsNullOrEmpty(telefono), "", " / <strong>Tel&eacute;fono:</strong> " & Server.HtmlEncode(telefono))

        templateAux = templateAux.Replace("{AGRUPADO_EN}", agrupadoEn.ToUpper())
        templateAux = templateAux.Replace("{NOMBRE_USUARIO}", nombreUsuario)
        sb.Append(templateAux)
      Next
      sb.Append("  </em>")
      sb.Append("</blockquote>")

      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' dibuja combo con los contactos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarComboContacto(ByVal ds As DataSet, ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal mostrarComboItemSeleccione As Boolean) As String
    Dim html As String = ""
    Dim contadorContacto, totalRegistros As Integer
    Dim cargo, telefono, idEscalamientoPorAlertaContacto, nombreUsuario, colSM, jsonTemplate As String
    Dim sbContactos As New StringBuilder

    Try
      contadorContacto = 1
      cargo = "-"
      telefono = "-"
      colSM = IIf(mostrarComboItemSeleccione, "col-sm-10", "")

      'verifica que si encuentra un grupo, debe contener al menos un contacto para dibujar
      If (ds Is Nothing) Then
        totalRegistros = 0
      Else
        totalRegistros = ds.Tables(0).Rows.Count
        If (totalRegistros = 1) Then
          idEscalamientoPorAlertaContacto = ds.Tables(0).Rows(0).Item("IdEscalamientoPorAlertaContacto")
          If (idEscalamientoPorAlertaContacto = "-1") Then totalRegistros = 0
        End If
      End If

      'si hay contactos entonces lo dibuja
      If (totalRegistros > 0) Then
        sbContactos.Append("<div>")
        sbContactos.Append("  <select id=""" & idEscalamientoPorAlertaGrupoContacto & "_ddlContacto"" class=""form-control input-sm chosen-select"" onchange=""Alerta.mostrarDatosContacto({ prefijoControl:'" & idEscalamientoPorAlertaGrupoContacto & "' })"">")
        If (mostrarComboItemSeleccione) Then sbContactos.Append("<option value="""">--Seleccione--</option>")

        For Each dr In ds.Tables(0).Rows
          idEscalamientoPorAlertaContacto = dr.Item("IdEscalamientoPorAlertaContacto")
          nombreUsuario = dr.Item("NombreUsuario")
          sbContactos.Append("<option value=""" & idEscalamientoPorAlertaContacto & """>" & Server.HtmlEncode(nombreUsuario) & "</option>")

          'si es el primero en dibujarse entonces obtiene los datos para mostrar en los controles
          If (Not mostrarComboItemSeleccione And contadorContacto = 1) Then
            cargo = dr.Item("Cargo")
            telefono = dr.Item("Telefono")
          End If

          contadorContacto += 1
        Next
        sbContactos.Append("  </select>")
        sbContactos.Append("</div>")

        'si no encuentra el contacto en el listado, muestra boton para que sea ingresado a mano
        If (mostrarComboItemSeleccione) Then
          jsonTemplate = "{"
          jsonTemplate &= "prefijoControl:  '{ID_UNICO}'"
          jsonTemplate &= ", agrupadoEn: '{AGRUPADO_EN}'"
          jsonTemplate &= ", localDestinoCodigo: '{LOCAL_DESTINO_CODIGO}'"
          jsonTemplate &= ", localDestinoNombre: '{LOCAL_DESTINO_NOMBRE}'"
          jsonTemplate &= ", localOrigenCodigo: '{LOCAL_ORIGEN_CODIGO}'"
          jsonTemplate &= ", localOrigenNombre: '{LOCAL_ORIGEN_NOMBRE}'"
          jsonTemplate &= ", nombreConductor: '{NOMBRE_CONDUCTOR}'"
          jsonTemplate &= ", rutConductor: '{RUT_CONDUCTOR}'"
          jsonTemplate &= ", nombreTransportista: '{NOMBRE_TRANSPORTISTA}'"
          jsonTemplate &= ", rutTransportista: '{RUT_TRANSPORTISTA}'"
          jsonTemplate &= ", llavePerfil: '{LLAVE_PERFIL}'"
          jsonTemplate &= ", tipoGrupo: '{TIPO_GRUPO}'"
          jsonTemplate &= ", idFormato: '{ID_FORMATO}'"
          jsonTemplate &= "}"

          '-- descomentar si es que se va a usar el boton Agregar Explicacion
          'sbContactos.Append("<div class=""col-sm-2 sist-padding-left-5 show-tooltip"" data-toggle=""tooltip"" data-placement=""bottom"" title=""agregar contacto"">")
          'sbContactos.Append("  <button type=""button"" class=""btn btn-sm btn-default"" onclick=""Alerta.agregarContactoModoManual(" & jsonTemplate & ")""><span class=""glyphicon glyphicon-plus""></span></button>")
          'sbContactos.Append("</div>")
        End If

        html = sbContactos.ToString()
      End If
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' dibuja el historial de llamadas realizadas en cada escalamiento
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta_HistorialLlamadas(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim sbEscalamiento As New StringBuilder
    Dim template, templateAux, tituloNroEscalamiento, fechaCreacion, agrupadoEn, nombreContacto, telefono, accion, atendidoPor, nroEscalamiento As String
    Dim eliminado As String
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim dsFiltrado As New DataSet
    Dim arrayGroupBy As New ArrayList
    Dim htmlSinRegistro As String = "<div class=""text-center sist-font-size-24""><em class=""help-block"">No hay llamadas realizadas</em></div>"

    Try
      template = "<tr>" & _
                 "  <td>{AGRUPADO_EN}</td>" & _
                 "  <td>{NOMBRE_CONTACTO}</td>" & _
                 "  <td>{TELEFONO}</td>" & _
                 "  <td>{ACCION}</td>" & _
                 "  <td>{FECHA_CREACION}</td>" & _
                 "  <td>{ATENDIDO_POR}</td>" & _
                 "</tr>"

      dt = ds.Tables(Alerta.eTabla.T05_HistorialLlamadas)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        sb.Append(htmlSinRegistro)
      Else
        arrayGroupBy = Utilidades.ObtenerArrayGroupByDeDataTable(dt, "ClaveNroEscalamiento")
        For Each claveNroEscalamiento As String In arrayGroupBy
          sbEscalamiento = New StringBuilder

          '---------------------------------------------------
          'obtiene las llamadas por escalamiento
          dv = dt.DefaultView
          dv.RowFilter = "ClaveNroEscalamiento = '" & claveNroEscalamiento & "'"
          dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          'obtiene informacion del primer registro
          dr = dsFiltrado.Tables(0).Rows(0)
          nroEscalamiento = dr.Item("NroEscalamiento")
          eliminado = dr.Item("Eliminado")

          eliminado = IIf(eliminado = "1", "&nbsp;<span class=""text-danger"">(eliminado)</span>", "")
          tituloNroEscalamiento = "Escalamiento NRO. " & nroEscalamiento & eliminado

          sbEscalamiento.Append("<div id=""hNroEscalamiento_" & claveNroEscalamiento & """>")
          sbEscalamiento.Append("<p class=""h4""><strong>" & tituloNroEscalamiento & "</strong></p>")
          sbEscalamiento.Append("<div class=""table-responsive"">")
          sbEscalamiento.Append("  <table class=""table table-striped table-condensed sist-width-100-porciento"">")
          sbEscalamiento.Append("    <thead>")
          sbEscalamiento.Append("      <tr>")
          sbEscalamiento.Append("        <th>Grupo</th>")
          sbEscalamiento.Append("        <th>Contacto</th>")
          sbEscalamiento.Append("        <th style=""width:120px;"">Tel&eacute;fono</th>")
          sbEscalamiento.Append("        <th style=""width:130px;"">Acci&oacute;n realizada</th>")
          sbEscalamiento.Append("        <th style=""width:150px;"">Fecha</th>")
          sbEscalamiento.Append("        <th style=""width:200px;"">Atendido Por</th>")
          sbEscalamiento.Append("      </tr>")
          sbEscalamiento.Append("    </thead>")
          sbEscalamiento.Append("    <tbody>")

          For Each dr In dsFiltrado.Tables(0).Rows
            templateAux = template
            agrupadoEn = dr.Item("agrupadoEn")
            nombreContacto = dr.Item("nombreContacto")
            telefono = dr.Item("telefono")
            accion = dr.Item("accion")
            atendidoPor = dr.Item("atendidoPor")
            fechaCreacion = dr.Item("fechaCreacion")

            'remplaza marcas
            accion = Utilidades.MensajeLlamadaPorAccion(accion)
            templateAux = templateAux.Replace("{FECHA_CREACION}", fechaCreacion)
            templateAux = templateAux.Replace("{AGRUPADO_EN}", agrupadoEn.ToUpper())
            templateAux = templateAux.Replace("{NOMBRE_CONTACTO}", nombreContacto)
            templateAux = templateAux.Replace("{TELEFONO}", telefono)
            templateAux = templateAux.Replace("{ACCION}", accion)
            templateAux = templateAux.Replace("{ATENDIDO_POR}", atendidoPor)
            sbEscalamiento.Append(templateAux)
          Next
          '---------------------------------------------------

          sbEscalamiento.Append("    </tbody>")
          sbEscalamiento.Append("  </table>")
          sbEscalamiento.Append("</div>")
          sbEscalamiento.Append("</div>")
          sbEscalamiento.Append("<hr />")

          sb.Append(sbEscalamiento.ToString())
        Next
      End If

      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    html = MyJSON.ConvertObjectToJSON(html)
    Return html
  End Function

  ''' <summary>
  ''' Obtiene los auxiliares utilizados por el Teleoperador
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAuxiliaresTeleoperador() As String
    Dim json As String = "[]"
    Dim ds As New DataSet

    Try
      ds = Alerta.ObtenerAuxiliaresTeleoperador()
      json = MyJSON.ConvertDataTableToJSON(ds.Tables(0))
    Catch ex As Exception
      json = "[]"
    End Try

    Return json
  End Function

  ''' <summary>
  ''' dibuja el historial de gestiones de alertas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDetalleAlerta_HistorialGestionAlertas(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim templatePanel, templatePanelAux, nombreAlerta, fechaHoraCreacion, tablaDatos, idAlerta As String
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim htmlSinRegistro As String = "<em class=""help-block"">No hay gesti&oacute;n de alertas asociados</em>"
    Dim arrayGroupBy As New ArrayList

    Try
      templatePanel = "<div class=""panel panel-default"">" & _
                      "  <div class=""panel-heading sist-font-color-black"">" & _
                      "    <strong>{NOMBRE_ALERTA} ({FECHA_HORA_CREACION}) [IdAlerta: {ID_ALERTA}]</strong>" & _
                      "  </div>" & _
                      "{TABLA_DATOS}" & _
                      "</div>"

      dt = ds.Tables(Alerta.eTabla.T06_HistorialGestionAlertas)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        sb.Append(htmlSinRegistro)
      Else
        'obtiene id de alertas
        arrayGroupBy = Utilidades.ObtenerArrayGroupByDeDataTable(dt, "IdAlerta")

        'recorre las alertas para dibujar la gestion de cada una de ellas
        For Each idAlerta In arrayGroupBy
          dv = dt.DefaultView
          dv.RowFilter = "IdAlerta = " & idAlerta
          dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          templatePanelAux = templatePanel
          dr = dsFiltrado.Tables(0).Rows(0)
          nombreAlerta = dr.Item("NombreAlerta")
          fechaHoraCreacion = dr.Item("FechaHoraCreacion")
          tablaDatos = ObtenerDetalleAlerta_HistorialGestionAlertas_TablaDatos(ds, idAlerta)

          templatePanelAux = templatePanelAux.Replace("{ID_ALERTA}", idAlerta)
          templatePanelAux = templatePanelAux.Replace("{NOMBRE_ALERTA}", nombreAlerta)
          templatePanelAux = templatePanelAux.Replace("{FECHA_HORA_CREACION}", fechaHoraCreacion)
          templatePanelAux = templatePanelAux.Replace("{TABLA_DATOS}", tablaDatos)
          sb.Append(templatePanelAux)
        Next
      End If

      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    html = MyJSON.ConvertObjectToJSON(html)
    Return html
  End Function

  Private Function ObtenerDetalleAlerta_HistorialGestionAlertas_TablaDatos(ByVal ds As DataSet, ByVal idAlerta As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim templateFila, templateFilaAux, fechaGestion, atendidoPor, nombreContacto, cargo, explicacion, observacion As String
    Dim dt As DataTable
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet

    Try
      templateFila = "<tr>" & _
                     "  <td>{FECHA_GESTION}</td>" & _
                     "  <td>{ATENDIDO_POR}</td>" & _
                     "  <td>{NOMBRE_CONTACTO}<br /><small>({CARGO})</small></td>" & _
                     "  <td>{EXPLICACION}</td>" & _
                     "  <td>{OBSERVACION}</td>" & _
                     "</tr>"

      dt = ds.Tables(Alerta.eTabla.T06_HistorialGestionAlertas)
      dv = dt.DefaultView
      dv.RowFilter = "IdAlerta = " & idAlerta
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""

      sb.Append("<table class=""table table-condensed"">")
      sb.Append("  <thead>")
      sb.Append("    <tr>")
      sb.Append("      <th>Fecha gesti&oacute;n</th>")
      sb.Append("      <th>Atendido por</th>")
      sb.Append("      <th>Contacto</th>")
      sb.Append("      <th>Explicaci&oacute;n</th>")
      sb.Append("      <th>Observaci&oacute;n</th>")
      sb.Append("    </tr>")
      sb.Append("  </thead>")
      sb.Append("  <tbody>")

      For Each dr In dsFiltrado.Tables(0).Rows
        templateFilaAux = templateFila
        fechaGestion = dr.Item("FechaGestion")
        atendidoPor = dr.Item("AtendidoPor")
        nombreContacto = dr.Item("NombreContacto")
        cargo = dr.Item("Cargo")
        explicacion = dr.Item("Explicacion")
        Observacion = dr.Item("Observacion")

        'remplaza marcas
        templateFilaAux = templateFilaAux.Replace("{FECHA_GESTION}", fechaGestion)
        templateFilaAux = templateFilaAux.Replace("{ATENDIDO_POR}", atendidoPor)
        templateFilaAux = templateFilaAux.Replace("{NOMBRE_CONTACTO}", nombreContacto)
        templateFilaAux = templateFilaAux.Replace("{CARGO}", cargo)
        templateFilaAux = templateFilaAux.Replace("{EXPLICACION}", explicacion)
        templateFilaAux = templateFilaAux.Replace("{OBSERVACION}", observacion)
        sb.Append(templateFilaAux)
      Next

      sb.Append("  </tbody>")
      sb.Append("</table>")

      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function


#End Region

#Region "GrabarGestionAlerta_TeleOperador"
  ''' <summary>
  ''' graba la gestion de la alerta realizada por el teleoperador
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarGestionAlerta_TeleOperador() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim json As String = Utilidades.IsNull(Request("json"), "")
    Dim sb As New StringBuilder
    Dim objJSON, row, objListadoContactos As Object
    Dim status, idHistorialEscalamiento, idAlerta, proximoEscalamiento, nombreAlerta, explicacion, explicacionOtro, observacion As String
    Dim idEscalamientoPorAlertaContacto, idAlertaGestionAcumulada, idAlertaHija, cerrado, fechaAsignacion, totalEscalamientos, agrupadoEn, tipoGrupo As String
    Dim cerradoSatisfactorio, idUsuarioSinGrupoContacto, tipoExplicacion, idEscalamientoPorAlertaGrupoContacto, telefono, nombreUsuario, idFormato As String
    Dim sl As New SortedList
    Dim revisarAlertaAsignadaPreviamente As Boolean = False
    Dim totalContactos As Integer
    Dim totalNoContesta As Integer = 0
    Dim totalEnLocal As Integer = 0
    Dim explicacionConductor As String = ""
    Dim tipoExplicacionConductor As String = ""
    Dim totalCierreViaje As Integer = 0
    Dim totalReprogramar As Integer = 0
    Dim explicacionReprogramar As String = ""
    Dim tipoExplicacionReprogramar As String = ""

    Try
      If (String.IsNullOrEmpty(json)) Then
        Throw New Exception("JSON Vacio")
      Else
        oUsuario = oUtilidades.ObtenerUsuarioSession()
        objJSON = MyJSON.ConvertJSONToObject(json)
        row = objJSON(0)
        cerrado = "0"
        cerradoSatisfactorio = "0"
        fechaAsignacion = ""
        tipoExplicacion = "Normal"
        idAlerta = MyJSON.ItemObject(row, "idAlerta")
        proximoEscalamiento = MyJSON.ItemObject(row, "proximoEscalamiento")
        totalEscalamientos = MyJSON.ItemObject(row, "totalEscalamientos")
        nombreAlerta = MyJSON.ItemObject(row, "nombreAlerta")
        explicacion = MyJSON.ItemObject(row, "explicacion")
        explicacionOtro = MyJSON.ItemObject(row, "explicacionOtro")
        observacion = MyJSON.ItemObject(row, "observacion")
        idAlertaGestionAcumulada = MyJSON.ItemObject(row, "idAlertaGestionAcumulada")
        idAlertaHija = MyJSON.ItemObject(row, "idAlertaHija")
        objListadoContactos = MyJSON.ItemObjectJSON(row, "listadoContactos")
        idFormato = MyJSON.ItemObject(row, "idFormato")

        'graba el registro
        idHistorialEscalamiento = Alerta.GrabarGestionTeleOperador(idAlerta, oUsuario.Id, proximoEscalamiento, nombreAlerta, explicacion, explicacionOtro, observacion, idFormato)

        If (idHistorialEscalamiento = Sistema.eCodigoSql.Error) Then
          Throw New Exception("No se pudo grabar la gestión de la alerta")
        Else
          totalContactos = MyJSON.LengthObject(objListadoContactos)
          For Each row In objListadoContactos
            idEscalamientoPorAlertaContacto = MyJSON.ItemObject(row, "idEscalamientoPorAlertaContacto")
            explicacion = MyJSON.ItemObject(row, "explicacion")
            observacion = MyJSON.ItemObject(row, "observacion")
            agrupadoEn = MyJSON.ItemObject(row, "agrupadoEn")
            tipoGrupo = MyJSON.ItemObject(row, "tipoGrupo")
            idUsuarioSinGrupoContacto = IIf(Utilidades.EsGrupoContactoEspecial(tipoGrupo), MyJSON.ItemObject(row, "idUsuarioSinGrupoContacto"), "-1")
            tipoExplicacion = MyJSON.ItemObject(row, "tipoExplicacion")
            idEscalamientoPorAlertaGrupoContacto = MyJSON.ItemObject(row, "idEscalamientoPorAlertaGrupoContacto")
            telefono = MyJSON.ItemObject(row, "telefono")
            nombreUsuario = MyJSON.ItemObject(row, "nombreUsuario")
            status = Alerta.GrabarGestionContactos(idHistorialEscalamiento, idEscalamientoPorAlertaContacto, explicacion, "", observacion, idUsuarioSinGrupoContacto, idEscalamientoPorAlertaGrupoContacto, "", "", "", "")

            '*** NO CONTESTA: DESCOMENTAR LAS LINEAS POR SI SE QUIERE UTILIZAR EL PASO AUTOMATICO AL SIGUIENTE ESCALAMIENTO COMO FUNCIONA EN WALMART (VSR, 08/07/2015)
            ''si el contacto pertenece al grupo "CONDUCTOR" o "TRANSPORTISTA" entonces lo excluye de los NO CONTESTA para avanzar al siguiente escalamiento cuando la alerta sea diferente a MULTIPUNTO
            'If (Not nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO) And (agrupadoEn.ToUpper() = Alerta.GRUPO_CONTACTO_CONDUCTOR Or agrupadoEn.ToUpper() = Alerta.GRUPO_CONTACTO_TRANSPORTISTA)) Then
            '  totalContactos -= 1
            'End If

            'If (tipoExplicacion.ToUpper() = Alerta.NO_CONTESTA) Then
            '  'cuenta los NO CONTESTA cuando la alerta es MULTIPUNTO o cuando es el grupo es distinto a CONDUCTO Y TRANSPORTISTA
            '  If (nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO)) Or (agrupadoEn.ToUpper() <> Alerta.GRUPO_CONTACTO_CONDUCTOR And agrupadoEn.ToUpper() <> Alerta.GRUPO_CONTACTO_TRANSPORTISTA) Then
            '    totalNoContesta += 1
            '  End If
            'End If

            'si la alerta es MULTIPUNTO entonces obtiene la explicacion del CONDUCTOR cuando es distinto a "NO CONTESTA" para generar una nueva alerta
            If (nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO) And agrupadoEn.ToUpper() = Alerta.GRUPO_CONTACTO_CONDUCTOR And tipoExplicacion.ToUpper() <> Alerta.NO_CONTESTA) Then
              explicacionConductor = explicacion
              tipoExplicacionConductor = tipoExplicacion
            End If

            'cuenta cuantas explicaciones son EN LOCAL
            If (tipoExplicacion.ToUpper() = Alerta.EN_LOCAL) Then
              totalEnLocal += 1
            End If

            'cuenta cuantas explicaciones son CIERRE VIAJE
            If (tipoExplicacion.ToUpper() = Alerta.CIERRE_VIAJE) Then
              totalCierreViaje += 1
            End If

            'cuenta cuantas explicaciones son REPROGRAMAR
            If (tipoExplicacion.ToUpper() = Alerta.REPROGRAMAR) Then
              totalReprogramar += 1
              explicacionReprogramar = explicacion
              tipoExplicacionReprogramar = tipoExplicacion
            End If
          Next

          'si la alerta es MULTIPUNTO y no se obtuvo la explicacion del CONDUCTOR, entonces obtiene la ultima explicacion ingresada en el listado de grupo de contactos
          If (nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO) And String.IsNullOrEmpty(explicacionConductor)) Then
            explicacionConductor = explicacion
            tipoExplicacionConductor = tipoExplicacion
          End If

          'graba registro en las alertas acumuladas
          idAlertaGestionAcumulada = Alerta.GrabarAlertaGestionAcumulada(idAlertaGestionAcumulada, fechaAsignacion, idAlerta, idAlertaHija, oUsuario.Id, cerrado, cerradoSatisfactorio, idHistorialEscalamiento)
          If (idAlertaGestionAcumulada = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar la alerta acumulada")

          'graba registro en historial alerta gestion
          status = Alerta.GrabarHistorialAlertaGestion(idAlerta, idAlertaHija, oUsuario.Id, Alerta.eAccionHistorial.TELEOPERADOR_ALERTA_GRABADA.ToString())
          If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar el historial de la gestión de la alerta")
          Sistema.GrabarLogSesion(oUsuario.Id, "Graba gestión alerta: IdAlerta " & idAlerta)

          '*** NO CONTESTA: DESCOMENTAR LAS LINEAS POR SI SE QUIERE UTILIZAR EL PASO AUTOMATICO AL SIGUIENTE ESCALAMIENTO COMO FUNCIONA EN WALMART (VSR, 08/07/2015)
          ''---------------------------------------------------------
          ''revisa que si ningun contacto responde (excluyendo a los del grupo conductor), entonces realiza el siguiente escalamiento de la alerta automaticamente
          'If (proximoEscalamiento < totalEscalamientos) And (totalContactos = totalNoContesta And totalContactos > 0) Then
          '  Utilidades.Delay(200) 'delay: detiene el sistema 200 milisegundos para que no queden las asignaciones con la misma fecha
          '  idAlertaGestionAcumulada = "-1"
          '  fechaAsignacion = ""
          '  cerrado = "0"
          '  cerradoSatisfactorio = "0"
          '  revisarAlertaAsignadaPreviamente = True

          '  'graba registro en las alertas acumuladas
          '  idAlertaGestionAcumulada = Alerta.GrabarAlertaGestionAcumulada(idAlertaGestionAcumulada, fechaAsignacion, idAlerta, idAlertaHija, oUsuario.Id, cerrado, cerradoSatisfactorio, "-1")
          '  If (idAlertaGestionAcumulada = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar la alerta acumulada")

          '  'graba registro en historial alerta gestion
          '  status = Alerta.GrabarHistorialAlertaGestion(idAlerta, idAlertaHija, oUsuario.Id, Alerta.eAccionHistorial.TELEOPERADOR_ALERTA_ASIGNADA.ToString())
          '  If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar el historial de la gestión de la alerta")
          '  Sistema.GrabarLogSesion(oUsuario.Id, "Avanza al siguiente escalamiento: IdAlerta " & idAlerta)
          'End If
          ''---------------------------------------------------------

          'si la alerta no pasa al siguiente escalamiento automaticamente, entonces envia email con los datos ingresados
          If (Not revisarAlertaAsignadaPreviamente) Then
            status = Alerta.EliminarAlertaEnColaAtencion(idAlerta, idAlertaHija)

            Dim dsAlerta As New DataSet
            dsAlerta = Alerta.ObtenerDetalle(idAlerta)
            InformeMail.GestionAlerta(idAlerta, dsAlerta)

            If (nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO)) Then
              If (tipoExplicacionConductor.ToUpper() <> Alerta.NO_CONTESTA) Then
                status = Alerta.GrabarNuevaAlertaFuturaMultipunto(idAlerta, explicacionConductor, tipoExplicacionConductor)
              End If
            End If

            'si se ingreso alguna explicacion EN LOCAL entonces desactiva todas las alertas asociadas al NroTransporte - LocalDestino y marca el viaje que esta en local
            'ademas envia email con el detalle de las alertas de ese viaje. Tambien se envia el informe cuando se gestiona la alerta "LOCAL"
            If (totalEnLocal > 0 Or nombreAlerta.ToUpper() = Alerta.LOCAL) Then
              If (totalEnLocal > 0) Then DesactivarAlertaEnLocal(idAlerta)
              InformeMail.ResumenViaje(dsAlerta)
            End If

            'si se ingreso alguna explicacion de CIERRE VIAJE, envia email con la informacion del viaje
            If (totalCierreViaje > 0) Then
              InformeMail.CierreViaje(dsAlerta)
            End If

            'si se ingreso alguna explicacion de REPROGRAMAR, entonces se creará una nueva alerta futura en el tiempo seleccionado
            If (totalReprogramar > 0) Then
              status = Alerta.GrabarNuevaAlertaFuturaMultipunto(idAlerta, explicacionReprogramar, tipoExplicacionReprogramar)
            End If
          End If

        End If

        sl.Add("revisarAlertaAsignadaPreviamente", revisarAlertaAsignadaPreviamente)
        retorno = MyJSON.ConvertSortedListToJSON(sl)
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function

  ''' <summary>
  ''' envia SMS con la informacion de alerta
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnviarSMSGestionAlerta(ByVal idAlerta As String, ByVal lstTelefonos As List(Of SortedList))
    Dim mensaje, telefono, nroTransporte, patenteTracto, patenteTrailer, localDestinoCodigo, nombreAlerta, tipoAlerta, latTracto, lonTracto As String
    Dim enviadoA, cargo, smsFolio, smsCodigo, smsMensaje As String
    Dim ds As New DataSet
    Dim dr As DataRow
    Dim patente As String = ""
    Dim sl As New SortedList

    Try
      mensaje = "ALTOTRACK: Le recordamos realizar doble control en IdMaster {NRO_TRANSPORTE}{PATENTE}, Tienda {LOCAL_DESTINO_CODIGO}, {NOMBRE_ALERTA} ({TIPO_ALERTA})"

      ds = Alerta.ObtenerDetalle(idAlerta)
      If (Not ds Is Nothing) Then
        dr = ds.Tables(Alerta.eTabla.T00_Detalle).Rows(0)
        nroTransporte = dr.Item("NroTransporte")
        patenteTracto = dr.Item("PatenteTracto")
        patenteTrailer = dr.Item("PatenteTrailer")
        localDestinoCodigo = dr.Item("LocalDestinoCodigo")
        nombreAlerta = dr.Item("NombreAlerta")
        tipoAlerta = dr.Item("TipoAlerta")
        latTracto = dr.Item("LatTracto")
        lonTracto = dr.Item("LonTracto")

        patente &= IIf(String.IsNullOrEmpty(patenteTracto), "", "tracto " & patenteTracto & " ")
        patente &= IIf(String.IsNullOrEmpty(patenteTrailer), "", "remolque " & patenteTrailer)
        patente = IIf(String.IsNullOrEmpty(patente.Trim()), "", ", " & patente)

        'reemplaza marcas
        mensaje = mensaje.Replace("{NRO_TRANSPORTE}", nroTransporte)
        mensaje = mensaje.Replace("{PATENTE}", patente.Trim())
        mensaje = mensaje.Replace("{LOCAL_DESTINO_CODIGO}", localDestinoCodigo)
        mensaje = mensaje.Replace("{NOMBRE_ALERTA}", nombreAlerta)
        mensaje = mensaje.Replace("{TIPO_ALERTA}", tipoAlerta)

        If Not Utilidades.MailModoDebug Then
          For Each slContacto As SortedList In lstTelefonos
            enviadoA = slContacto.Item("NombreUsuario")
            telefono = slContacto.Item("Telefono")
            cargo = slContacto.Item("Cargo")

            'If (Utilidades.EstaDentroHorario(cargo)) Then
            If (Utilidades.EsNumerico(telefono) AndAlso telefono.Length = 8 AndAlso Not telefono.StartsWith("2")) Then
              sl = Herramientas.EnviarSMS(telefono, mensaje)

              smsFolio = sl.Item("Folio")
              smsCodigo = sl.Item("Codigo")
              smsMensaje = sl.Item("Mensaje")
              Alerta.GrabarHistorialAlertaSMS(nombreAlerta, nroTransporte, latTracto, lonTracto, localDestinoCodigo, telefono, enviadoA, cargo, smsFolio.Trim(), smsCodigo.Trim(), smsMensaje.Trim(), "APLICACION WEB")
            End If
            'End If
          Next
        End If

      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  ''' <summary>
  ''' desactiva las alertas que estan en cola para el viaje que ya llego al local
  ''' </summary>
  ''' <param name="idAlerta"></param>
  ''' <remarks></remarks>
  Private Sub DesactivarAlertaEnLocal(ByVal idAlerta As Integer)
    Dim oUtilidades As New Utilidades
    Dim oAlertaWebservice As AlertaWebservice
    Dim oUsuario As Usuario
    Dim status As String

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      oAlertaWebservice = New AlertaWebservice(idAlerta)
      oAlertaWebservice.GrabarAlertaEnLocal()
      oAlertaWebservice.TraspasarTodoAlertaHistoria()

      'desactiva las alertas que estan en cola
      status = Alerta.DesactivarAlertasEnCola(oUsuario.Id, oAlertaWebservice.NroTransporte, oAlertaWebservice.LocalDestino)
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

#Region "ObtenerComboExplicacion"
  ''' <summary>
  ''' obtiene combo Explicacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerComboExplicacion() As String
    Dim html As String = ""
    Dim prefijoControl As String = Utilidades.IsNull(Request("prefijoControl"), "")

    Try
      html = DibujarComboExplicacionAjax(prefijoControl)
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' dibuja combo Explicacion invocado desde ajax
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarComboExplicacionAjax(ByVal prefijoControl As String) As String
    Dim html As String = ""
    Dim ds As New DataSet
    Dim sb As New StringBuilder
    Dim texto, valor, prefijoControlCombo As String
    Dim dr As DataRow

    prefijoControlCombo = prefijoControl & IIf(String.IsNullOrEmpty(prefijoControl), "", "_")

    'dibuja combo
    sb.Append("<div class=""sist-padding-cero"">")
    sb.Append("  <select id=""" & prefijoControlCombo & "ddlExplicacion"" class=""form-control input-sm chosen-select"" rel=""combo-explicacion"" onchange=""Alerta.habilitarCampoOtro({ prefijoControl: '" & prefijoControl & "',ddlControl:'ddlExplicacion',txtControl:'txtExplicacionOtro'})"">")
    sb.Append("    <option value="""" data-frecuencia=""-1"" data-tipoExplicacion=""Normal"">--Seleccione--</option>")

    Try
      ds = Sistema.ObtenerComboDinamico("Explicacion", "-1")

      For Each dr In ds.Tables(0).Rows
        texto = dr.Item("Texto")
        valor = dr.Item("Valor")
        sb.Append("<option value=""" & valor & """ data-frecuencia=""-1"" data-tipoExplicacion=""Normal"">" & Server.HtmlEncode(texto) & "</option>")
      Next
    Catch ex As Exception
      'no hace nada
    End Try

    sb.Append("  </select>")
    sb.Append("</div>")

    html = sb.ToString()
    Return html
  End Function

  ''' <summary>
  ''' dibuja combo Explicacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarComboExplicacionGrupoContacto(ByVal idEscalamientoPorAlertaGrupoContacto As String) As String
    Dim html As String = ""
    Dim ds As New DataSet
    Dim sb As New StringBuilder
    Dim texto, valor, prefijoControlCombo, tipoExplicacion As String
    Dim dr As DataRow

    prefijoControlCombo = idEscalamientoPorAlertaGrupoContacto & IIf(String.IsNullOrEmpty(idEscalamientoPorAlertaGrupoContacto), "", "_")

    'dibuja combo
    'sb.Append("<div class=""col-sm-10 sist-padding-cero"">") '-- descomentar si es que se va a usar el boton Agregar Explicacion

    sb.Append("  <select id=""" & prefijoControlCombo & "ddlExplicacion"" class=""form-control input-sm chosen-select"" rel=""combo-explicacion"" onchange=""Alerta.mostrarGrupoContactoHijo_TeleOperador({ idEscalamientoPorAlertaGrupoContacto: '" & idEscalamientoPorAlertaGrupoContacto & "', invocadoDesde: 'Combo' })"">")
    sb.Append("    <option value="""" data-frecuencia=""-1"" data-tipoExplicacion=""Normal"">--Seleccione--</option>")

    Try
      ds = Sistema.ObtenerComboDinamico("Explicacion", "-1")

      For Each dr In ds.Tables(0).Rows
        texto = dr.Item("Texto")
        valor = dr.Item("Valor")
        tipoExplicacion = dr.Item("TipoExplicacion")

        If (valor <> "MostrarCampoOtro") Then
          sb.Append("<option value=""" & valor & """ data-frecuencia=""-1"" data-tipoExplicacion=""" & tipoExplicacion & """>" & Server.HtmlEncode(texto) & "</option>")
        End If
      Next
    Catch ex As Exception
      'no hace nada
    End Try

    sb.Append("  </select>")

    '-- descomentar si es que se va a usar el boton Agregar Explicacion
    'sb.Append("</div>")
    'sb.Append("<div class=""col-sm-2 sist-padding-left-5 show-tooltip"" data-toggle=""tooltip"" data-placement=""bottom"" title=""agregar explicaci&oacute;n"">")
    'sb.Append("  <button type=""button"" class=""btn btn-sm btn-default"" onclick=""Alerta.agregarExplicacionModoManual({ prefijoControl: '" & idEscalamientoPorAlertaGrupoContacto & "' })""><span class=""glyphicon glyphicon-plus""></span></button>")
    'sb.Append("</div>")

    html = sb.ToString()
    Return html
  End Function

  ''' <summary>
  ''' dibuja combo Explicacion Multipunto
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarComboExplicacionMultipunto(ByVal idEscalamientoPorAlertaGrupoContacto As String) As String
    Dim html As String = ""
    Dim ds As New DataSet
    Dim sb As New StringBuilder
    Dim texto, valor, prefijoControlCombo, frecuenciaRepeticion, tipoExplicacion As String
    Dim dr As DataRow

    prefijoControlCombo = idEscalamientoPorAlertaGrupoContacto & IIf(String.IsNullOrEmpty(idEscalamientoPorAlertaGrupoContacto), "", "_")

    'dibuja combo
    sb.Append("<div class=""sist-padding-cero"">")
    sb.Append("  <select id=""" & prefijoControlCombo & "ddlExplicacion"" class=""form-control input-sm chosen-select"" rel=""combo-explicacion"" onchange=""Alerta.mostrarGrupoContactoHijo_TeleOperador({ idEscalamientoPorAlertaGrupoContacto: '" & idEscalamientoPorAlertaGrupoContacto & "', invocadoDesde: 'Combo' }); Alerta.reemplazarMarcasEspecialesScript({ prefijoControl: '" & idEscalamientoPorAlertaGrupoContacto & "'})"">")
    sb.Append("    <option value="""" data-frecuencia=""-1"" data-tipoExplicacion=""Normal"">--Seleccione--</option>")

    Try
      ds = Sistema.ObtenerComboDinamico("ExplicacionMultipunto", "-1")

      For Each dr In ds.Tables(0).Rows
        texto = dr.Item("Texto")
        valor = dr.Item("Valor")
        frecuenciaRepeticion = dr.Item("FrecuenciaRepeticion")
        tipoExplicacion = dr.Item("TipoExplicacion")
        sb.Append("<option value=""" & valor & """ data-frecuencia=""" & frecuenciaRepeticion & """ data-tipoExplicacion=""" & tipoExplicacion & """>" & Server.HtmlEncode(texto) & "</option>")
      Next
    Catch ex As Exception
      'no hace nada
    End Try

    sb.Append("  </select>")
    sb.Append("</div>")

    html = sb.ToString()
    Return html
  End Function

#End Region

#Region "DesvincularAlerta"
  ''' <summary>
  ''' desasigna alerta del usuario
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DesvincularAlerta() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim retorno As String = ""
    Dim idAlerta As String = Utilidades.IsNull(Request("idAlerta"), "-1")
    Dim idAlertaHija As String = Utilidades.IsNull(Request("idAlertaHija"), "-1")
    Dim idAlertaGestionAcumulada As String = Utilidades.IsNull(Request("idAlertaGestionAcumulada"), "-1")
    Dim accion As String = Utilidades.IsNull(Request("accion"), Alerta.eAccionHistorial.TELEOPERADOR_OCUPADO.ToString())
    Dim accionHistorial, accionAuxiliar As String
    Dim status As Integer

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      accionAuxiliar = accion
      accion = IIf(accion.StartsWith("TELEOPERADOR_AUXILIAR_"), "TELEOPERADOR_AUXILIAR", accion)

      Select Case accion
        Case Alerta.eAccionHistorial.TELEOPERADOR_OCUPADO.ToString()
          accionHistorial = Alerta.eAccionHistorial.TELEOPERADOR_OCUPADO.ToString()
          Sistema.GrabarLogSesion(oUsuario.Id, "Ocupado y desvincula alerta: IdAlerta " & idAlerta)
        Case Alerta.eAccionHistorial.TELEOPERADOR_CERRAR_SESION.ToString()
          accionHistorial = Alerta.eAccionHistorial.TELEOPERADOR_CERRAR_SESION.ToString()
          Sistema.GrabarLogSesion(oUsuario.Id, "Cierra sesión y desvincula alerta: IdAlerta " & idAlerta)
        Case "TELEOPERADOR_AUXILIAR"
          accionHistorial = accionAuxiliar
          Sistema.GrabarLogSesion(oUsuario.Id, "Estado auxiliar: " & accionAuxiliar)
        Case Else
          accionHistorial = Alerta.eAccionHistorial.TELEOPERADOR_OCUPADO.ToString()
          Sistema.GrabarLogSesion(oUsuario.Id, "Ocupado y desvincula alerta: IdAlerta " & idAlerta)
      End Select

      'graba registro en las alertas acumuladas
      status = Alerta.DesvincularDeUsuario(idAlertaGestionAcumulada)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo desvincular la alerta")

      'desasigna la alerta que esta en cola de atencion
      status = Alerta.ActualizarAsignacionAlertaEnCola(idAlerta, idAlertaHija, "-1")

      'graba registro en historial alerta gestion
      If (idAlerta <> "-1") Then
        status = Alerta.GrabarHistorialAlertaGestion(idAlerta, idAlertaHija, oUsuario.Id, accionHistorial)
        If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar el historial de la gestión de la alerta")
      End If

      Return Sistema.eCodigoSql.Exito
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function

#End Region

#Region "ObtenerAlertasPorGestionar"
  ''' <summary>
  ''' obtiene listado de alertas sin atender
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAlertasPorGestionar() As String
    Dim retorno As String = ""
    Dim sb As New StringBuilder
    Dim template, templateAux As String
    Dim nroTransporte, nombreAlerta, atendidoPor, ultimaGestionFecha, idAlerta, clasificacion, ultimaGestionAccion, clasificacionAlertaSist, montoDiscrepancia As String
    Dim totalRegistros, contador As Integer
    Dim sl As New SortedList
    Dim ds As DataSet
    Dim dr As DataRow
    Dim dsFiltrado As New DataSet
    Dim htmlSinRegistro As String = "<em class=""help-block"">No Hay Alertas Por Gestionar</em>"

    Try
      template = "<tr id=""tr{INDICE}"" class=""sist-cursor-pointer destacar-fila"" onclick=""Alerta.obtenerDetalle({ idAlerta: '{ID_ALERTA}', desdeFormulario: Alerta.FORMULARIO_SUPERVISOR_GESTION_ALERTA }); Sistema.destacarFilaTabla({ indice: '{INDICE}' })"">" & _
                 "  <td>{INDICE}</td>" & _
                 "  <td>{ID_ALERTA}</td>" & _
                 "  <td>{NRO_TRANSPORTE}</td>" & _
                 "  <td>{MONTO_DISCREPANCIA}</td>" & _
                 "  <td>{NOMBRE_ALERTA}{CLASIFICACION_ALERTASIST}</td>" & _
                 "  <td>{ULTIMA_GESTION_FECHA}</td>" & _
                 "</tr>"

      ds = Alerta.ObtenerAlertasPorGestionar()
      If (ds Is Nothing) Then
        totalRegistros = 0
        sb.Append(htmlSinRegistro)
      Else
        totalRegistros = ds.Tables(Alerta.eTablaAlertasPorGestionar.T00_Listado).Rows.Count

        If (totalRegistros = 0) Then
          sb.Append(htmlSinRegistro)
        Else
          contador = 1

          sb.Append("<table id=""TablaAlertasPorGestionar"" class=""table table-hover table-condensed"">")
          sb.Append("  <thead>")
          sb.Append("    <tr>")
          sb.Append("      <th>#</th>")
          sb.Append("      <th>ID Alerta</th>")
          sb.Append("      <th>IdMaster</th>")
          sb.Append("      <th>Monto Discrepancia</th>")
          sb.Append("      <th>Alerta</th>")
          sb.Append("      <th>Fecha Creaci&oacute;n</th>")
          'sb.Append("      <th>Clasificaci&oacute;n</th>")
          sb.Append("    </tr>")
          sb.Append("  </thead>")
          sb.Append("  <tbody>")

          For Each dr In ds.Tables(Alerta.eTablaAlertasPorGestionar.T00_Listado).Rows
            templateAux = template
            idAlerta = dr.Item("IdAlerta")
            nroTransporte = dr.Item("NroTransporte")
            nombreAlerta = dr.Item("NombreAlerta")
            atendidoPor = dr.Item("AtendidoPor")
            ultimaGestionFecha = dr.Item("UltimaGestionFecha")
            clasificacion = dr.Item("Clasificacion")
            ultimaGestionAccion = dr.Item("UltimaGestionAccion")
            clasificacionAlertaSist = dr.Item("ClasificacionAlertaSist")
            montoDiscrepancia = dr.Item("MontoDiscrepancia")

            'remplaza marcas
            'prioridad = Utilidades.ObtenerLabelPrioridad(prioridad)
            clasificacion = IIf(clasificacion = "SIN CONTACTABILIDAD", "<span class=""alert alert-danger sist-padding-label-prioridad sist-margin-cero""><strong>" & clasificacion & "</strong></span>", clasificacion)
            clasificacion &= IIf(ultimaGestionAccion = Alerta.eAccionHistorial.SUPERVISOR_ALERTA_CERRADO_PENDIENTE.ToString(), "<div class=""small""><strong><em>CERRADO PENDIENTE</em></strong></div>", "")
            clasificacionAlertaSist = IIf(Not String.IsNullOrEmpty(clasificacionAlertaSist), "&nbsp;<span class=""alert alert-danger sist-padding-label-prioridad sist-margin-cero""><strong>" & clasificacionAlertaSist & "</strong></span>", clasificacionAlertaSist)

            templateAux = templateAux.Replace("{INDICE}", contador)
            templateAux = templateAux.Replace("{ID_ALERTA}", idAlerta)
            templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateAux = templateAux.Replace("{NOMBRE_ALERTA}", Server.HtmlEncode(nombreAlerta))
            'templateAux = templateAux.Replace("{PRIORIDAD}", prioridad)
            templateAux = templateAux.Replace("{ULTIMA_GESTION_FECHA}", ultimaGestionFecha)
            'templateAux = templateAux.Replace("{CLASIFICACION}", clasificacion)
            templateAux = templateAux.Replace("{CLASIFICACION_ALERTASIST}", clasificacionAlertaSist)
            templateAux = templateAux.Replace("{MONTO_DISCREPANCIA}", montoDiscrepancia)

            sb.Append(templateAux)

            contador += 1
          Next

          sb.Append("  </tbody>")
          sb.Append("</table>")
        End If

        Dim sbRetorno As New StringBuilder
        sbRetorno.Append("{")
        sbRetorno.Append(" ""totalRegistros"":""" & totalRegistros & """")
        sbRetorno.Append(",""listado"":" & MyJSON.ConvertObjectToJSON(sb.ToString()))
        sbRetorno.Append("}")
        retorno = sbRetorno.ToString()
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region

#Region "GrabarGestionAlerta_Supervisor"
  ''' <summary>
  ''' graba la gestion de la alerta realizada por el supervisor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarGestionAlerta_Supervisor() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim json As String = Utilidades.IsNull(Request("json"), "")
    Dim sb As New StringBuilder
    Dim objJSON, row As Object
    Dim status, idHistorialEscalamiento, idAlerta, explicacion, explicacionOtro, observacion, categoriaAlerta, tipoObservacion, clasificacionAlerta, clasificacionAlertaSist As String
    Dim idEscalamientoPorAlertaContacto, idAlertaGestionAcumulada, idAlertaHija, cerrado, fechaAsignacion, cerradoSatisfactorio As String
    Dim idUsuarioSinGrupoContacto, agrupadoEn, accionLog, idEscalamientoPorAlertaGrupoContacto, accion, tipoGrupo As String

    Try
      If (String.IsNullOrEmpty(json)) Then
        Throw New Exception("JSON Vacio")
      Else
        oUsuario = oUtilidades.ObtenerUsuarioSession()
        objJSON = MyJSON.ConvertJSONToObject(json)
        row = objJSON(0)

        fechaAsignacion = ""
        idAlerta = MyJSON.ItemObject(row, "idAlerta")
        idAlertaGestionAcumulada = MyJSON.ItemObject(row, "idAlertaGestionAcumulada")
        idEscalamientoPorAlertaContacto = MyJSON.ItemObject(row, "idEscalamientoPorAlertaContacto")
        explicacion = MyJSON.ItemObject(row, "explicacion")
        explicacionOtro = MyJSON.ItemObject(row, "explicacionOtro")
        observacion = MyJSON.ItemObject(row, "observacion")
        cerrado = MyJSON.ItemObject(row, "cerrado")
        cerradoSatisfactorio = MyJSON.ItemObject(row, "cerradoSatisfactorio")
        agrupadoEn = MyJSON.ItemObject(row, "agrupadoEn")
        tipoGrupo = MyJSON.ItemObject(row, "tipoGrupo")
        idEscalamientoPorAlertaGrupoContacto = MyJSON.ItemObject(row, "idEscalamientoPorAlertaGrupoContacto")
        idUsuarioSinGrupoContacto = IIf(Utilidades.EsGrupoContactoEspecial(tipoGrupo), MyJSON.ItemObject(row, "idUsuarioSinGrupoContacto"), "-1")
        idAlertaHija = idAlerta
        categoriaAlerta = MyJSON.ItemObject(row, "categoriaAlerta")
        tipoObservacion = MyJSON.ItemObject(row, "tipoObservacion")
        clasificacionAlerta = MyJSON.ItemObject(row, "clasificacionAlerta")
        clasificacionAlertaSist = MyJSON.ItemObject(row, "clasificacionAlertaSist")

        If (cerrado = "0") Then
          accion = Alerta.eAccionHistorial.SUPERVISOR_ALERTA_CERRADO_PENDIENTE.ToString()
          Sistema.GrabarLogSesion(oUsuario.Id, "Cerrado Pendiente alerta: IdAlerta " & idAlerta)
        Else
          accion = IIf(cerradoSatisfactorio = "1", Alerta.eAccionHistorial.SUPERVISOR_ALERTA_CERRADO_SATISFACTORIO.ToString(), Alerta.eAccionHistorial.SUPERVISOR_ALERTA_CERRADO_NO_SATISFACTORIO.ToString())
          accionLog = IIf(cerradoSatisfactorio = "1", "Cerrado Satisfactorio", "Cerrado No Satisfactorio")
          Sistema.GrabarLogSesion(oUsuario.Id, accionLog & " alerta: IdAlerta " & idAlerta)
        End If

        'graba el registro
        idHistorialEscalamiento = Alerta.GrabarGestionSupervisor(idAlerta, oUsuario.Id, explicacion, explicacionOtro, observacion, categoriaAlerta, tipoObservacion, clasificacionAlerta, clasificacionAlertaSist)

        If (idHistorialEscalamiento = Sistema.eCodigoSql.Error) Then
          Throw New Exception("No se pudo grabar la gestión de la alerta")
        Else
          'graba al usuario contactado de algun escalamiento anterior
          status = Alerta.GrabarGestionContactos(idHistorialEscalamiento, idEscalamientoPorAlertaContacto, explicacion, explicacionOtro, observacion, idUsuarioSinGrupoContacto, idEscalamientoPorAlertaGrupoContacto, categoriaAlerta, tipoObservacion, clasificacionAlerta, clasificacionAlertaSist)

          'graba registro en las alertas acumuladas
          idAlertaGestionAcumulada = Alerta.GrabarAlertaGestionAcumulada(idAlertaGestionAcumulada, fechaAsignacion, idAlerta, idAlertaHija, oUsuario.Id, cerrado, cerradoSatisfactorio, "-1")
          If (idAlertaGestionAcumulada = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar la alerta acumulada")

          'graba registro en historial alerta gestion
          status = Alerta.GrabarHistorialAlertaGestion(idAlerta, idAlertaHija, oUsuario.Id, accion)
          If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar el historial de la gestión de la alerta")
        End If

        Return Sistema.eCodigoSql.Exito
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function

#End Region

#Region "ObtenerComboContactadoEscalamientoAnterior"
  ''' <summary>
  ''' obtiene combo Contactado Escalamiento Anterior
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerComboContactadoEscalamientoAnterior(ByVal ds As DataSet) As String
    Dim html As String = ""
    Try
      html = DibujarComboContactadoEscalamientoAnterior(ds)
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' dibuja combo Explicacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarComboContactadoEscalamientoAnterior(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim dsContactadoEscalamientoAnterior As New DataSet
    Dim sb As New StringBuilder
    Dim textoEscalamiento, textoNombre, idEscalamientoPorAlertaContacto, nombreUsuario, cargo, agrupadoEn, idUsuarioSinGrupoContacto As String
    Dim idEscalamientoPorAlertaGrupoContacto, ordenEscalamiento, eliminado, tipoGrupo As String
    Dim dr As DataRow
    Dim arrayGroupBy As New ArrayList
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim dt As DataTable

    sb.Append("<select id=""ddlContactadoEscalamientoAnterior"" class=""form-control input-sm chosen-select"">")
    sb.Append("<option value="""" data-agrupadoEn="""" data-idEscalamientoPorAlertaGrupoContacto=""-1"" data-tipoGrupo="""">--Seleccione--</option>")

    Try
      dt = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos)
      arrayGroupBy = Utilidades.ObtenerArrayGroupByDeDataTable(dt, "ClaveOrdenEscalamiento", True)

      For Each claveOrdenEscalamiento As String In arrayGroupBy
        If (claveOrdenEscalamiento.ToUpper() <> Alerta.SUPERVISOR) Then
          dv = dt.DefaultView
          dv.RowFilter = "ClaveOrdenEscalamiento = '" & claveOrdenEscalamiento & "'"
          dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          'obtiene informacion del primer registro
          dr = dsFiltrado.Tables(0).Rows(0)
          ordenEscalamiento = dr.Item("OrdenEscalamiento")
          eliminado = IIf(claveOrdenEscalamiento.Contains("-eliminado"), " (eliminado)", "")

          textoEscalamiento = "Escalamiento Nro. " & ordenEscalamiento & eliminado & ":"
          sb.Append("<optgroup label=""" & textoEscalamiento & """>")

          For Each dr In dsFiltrado.Tables(0).Rows
            idEscalamientoPorAlertaContacto = dr.Item("IdEscalamientoPorAlertaContacto")
            idUsuarioSinGrupoContacto = dr.Item("IdUsuarioSinGrupoContacto")
            nombreUsuario = dr.Item("NombreUsuario")
            cargo = dr.Item("Cargo")
            agrupadoEn = dr.Item("AgrupadoEn")
            idEscalamientoPorAlertaGrupoContacto = dr.Item("IdEscalamientoPorAlertaGrupoContacto")
            tipoGrupo = dr.Item("TipoGrupo")

            textoNombre = agrupadoEn.ToUpper & " - " & nombreUsuario & " (" & cargo & ")"
            idEscalamientoPorAlertaContacto = IIf(Utilidades.EsGrupoContactoEspecial(tipoGrupo), idUsuarioSinGrupoContacto, idEscalamientoPorAlertaContacto)
            sb.Append("<option value=""" & idEscalamientoPorAlertaContacto & """ data-agrupadoEn=""" & Server.HtmlEncode(agrupadoEn) & """ data-idEscalamientoPorAlertaGrupoContacto=""" & idEscalamientoPorAlertaGrupoContacto & """ data-tipoGrupo=""" & Server.HtmlEncode(tipoGrupo) & """>" & Server.HtmlEncode(textoNombre) & "</option>")
          Next

          sb.Append("</optgroup>")
        End If
      Next
    Catch ex As Exception
      'no hace nada
    End Try

    sb.Append("</select>")
    html = sb.ToString()
    Return html
  End Function

#End Region

#Region "ObtenerAlertasAtendidasHoyEnTabla"
  ''' <summary>
  ''' obtiene listado de alertas atendidas el día de hoy
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAlertasAtendidasHoyEnTabla() As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim template, templateAux As String
    Dim idAlerta, nroTransporte, nroEscalamiento, nombreAlerta, prioridad, cerrado, fecha As String
    Dim sb As New StringBuilder
    Dim sl As New SortedList
    Dim dr As DataRow
    Dim totalRegistros, contador As Integer
    Dim htmlSinRegistro As String = "<em class=""help-block text-center"">No hay alertas atendidas el d&iacute;a de hoy</em>"

    Try
      template = "<tr class=""sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta: '{ID_ALERTA}', idHistorialEscalamiento:'-1', fancy_width:'90%', fancy_height:'90%', fancy_overlayColor:Sistema.FANCYBOX_OVERLAY_COLOR_DEFECTO })"">" & _
                 "  <td>{INDICE}</td>" & _
                 "  <td>{FECHA}</td>" & _
                 "  <td>{NRO_TRANSPORTE}</td>" & _
                 "  <td>{NOMBRE_ALERTA}</td>" & _
                 "  <td>{PRIORIDAD}</td>" & _
                 "  <td>{NRO_ESCALAMIENTO}</td>" & _
                 "  <td>{CERRADO}</td>" & _
                 "</tr>"

      ds = Alerta.ObtenerAlertasAtendidasHoy()
      If (ds Is Nothing) Then
        sb.Append(htmlSinRegistro)
      Else
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros = 0) Then
          sb.Append(htmlSinRegistro)
        Else
          contador = 1

          sb.Append("<table id=""tblAlertasAtendidasHoy"" class=""table table-hover sist-width-100-porciento"">")
          sb.Append("  <thead>")
          sb.Append("    <tr>")
          sb.Append("      <th>#</th>")
          sb.Append("      <th>Fecha</th>")
          sb.Append("      <th>IdMaster</th>")
          sb.Append("      <th>Alerta</th>")
          sb.Append("      <th>Prioridad</th>")
          sb.Append("      <th>Escalamientos realizados</th>")
          sb.Append("      <th>Cerrado</th>")
          sb.Append("    </tr>")
          sb.Append("  </thead>")
          sb.Append("  <tbody>")

          For Each dr In ds.Tables(0).Rows
            templateAux = template
            idAlerta = dr.Item("IdAlerta")
            fecha = dr.Item("Fecha")
            nombreAlerta = dr.Item("NombreAlerta")
            nroTransporte = dr.Item("NroTransporte")
            prioridad = dr.Item("Prioridad")
            nroEscalamiento = dr.Item("NroEscalamiento")
            cerrado = dr.Item("Cerrado")

            'reemplaza las marcas
            prioridad = Utilidades.ObtenerLabelPrioridad(prioridad)
            cerrado = IIf(cerrado = "1", "SI", "NO")
            templateAux = templateAux.Replace("{INDICE}", contador)
            templateAux = templateAux.Replace("{FECHA}", fecha)
            templateAux = templateAux.Replace("{ID_ALERTA}", idAlerta)
            templateAux = templateAux.Replace("{NOMBRE_ALERTA}", Server.HtmlEncode(nombreAlerta))
            templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateAux = templateAux.Replace("{PRIORIDAD}", prioridad)
            templateAux = templateAux.Replace("{NRO_ESCALAMIENTO}", nroEscalamiento)
            templateAux = templateAux.Replace("{CERRADO}", cerrado)
            sb.Append(templateAux)

            contador += 1
          Next
          sb.Append("  </tbody>")
          sb.Append("</table>")
        End If
      End If
      'agrega listado a json final
      sl.Add("listado", sb.ToString())

      retorno = MyJSON.ConvertSortedListToJSON(sl)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function

#End Region

#Region "EliminarAlertaConfiguracion"
  ''' <summary>
  ''' elimina la configuracion de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EliminarAlertaConfiguracion() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""
    Dim idAlertaConfiguracion As String = Utilidades.IsNull(Request("idAlertaConfiguracion"), "-1")

    Try
      retorno = Alerta.EliminarDatosConfiguracion(idAlertaConfiguracion)
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}La ALERTA fue eliminada satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina configuración alerta: IdAlertaConfiguracion " & idAlertaConfiguracion)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

#End Region

#Region "ObtenerAlertasSinAtenderEnTabla"
  ''' <summary>
  ''' obtiene listado de alertas atendidas el día de hoy
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerAlertasSinAtenderEnTabla() As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim template, templateAux As String
    Dim idAlerta, nroTransporte, nombreAlerta, prioridad, fecha, atendidoPor, asignadoA, clasificacion, nombreFormato As String
    Dim sb As New StringBuilder
    Dim sl As New SortedList
    Dim dr As DataRow
    Dim totalRegistros, contador As Integer
    Dim htmlSinRegistro As String = "<em class=""help-block text-center"">No hay alertas sin atender</em>"

    Try
      template = "<tr class=""sist-cursor-pointer"" onclick=""Alerta.mostrarDetalleAlerta({ idAlerta:'{ID_ALERTA}', idHistorialEscalamiento:'-1', sinAtender:true, asignadoA:'{ATENDIDO_POR}', fechaRecepcion:'{FECHA}', clasificacion: '{CLASIFICACION}' })"">" & _
                 "  <td>{INDICE}</td>" & _
                 "  <td>" & _
                 "    <span id=""{ID_ALERTA}_lblFecha"">{FECHA}</span>" & _
                 "    <input type=""hidden"" id=""{ID_ALERTA}_txtIdAlerta"" value=""{ID_ALERTA}"" class=""alertas-sin-atender-id"">" & _
                 "  </td>" & _
                 "  <td>{NOMBRE_FORMATO}</td>" & _
                 "  <td>{NOMBRE_ALERTA}</td>" & _
                 "  <td>{NRO_TRANSPORTE}</td>" & _
                 "  <td>{PRIORIDAD}</td>" & _
                 "  <td>{ASIGNADO_A}</td>" & _
                 "  <td><strong><span id=""{ID_ALERTA}_lblCronometro"">00:00:00</span></strong></td>" & _
                 "  <td>{CLASIFICACION}</td>" & _
                 "</tr>"

      ds = Alerta.ObtenerAlertasSinAtender()
      If (ds Is Nothing) Then
        totalRegistros = 0
        sb.Append(htmlSinRegistro)
      Else
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros = 0) Then
          sb.Append(htmlSinRegistro)
        Else
          contador = 1

          sb.Append("<table id=""tblAlertasSinAtender"" class=""table table-hover"">")
          sb.Append("  <thead>")
          sb.Append("    <tr>")
          sb.Append("      <th>#</th>")
          sb.Append("      <th>Fecha</th>")
          sb.Append("      <th>Formato</th>")
          sb.Append("      <th>Alerta</th>")
          sb.Append("      <th>IdMaster</th>")
          sb.Append("      <th>Prioridad</th>")
          sb.Append("      <th>Asignado A</th>")
          sb.Append("      <th>Tiempo espera</th>")
          sb.Append("      <th>Clasificaci&oacute;n</th>")
          sb.Append("    </tr>")
          sb.Append("  </thead>")
          sb.Append("  <tbody>")

          For Each dr In ds.Tables(0).Rows
            templateAux = template
            idAlerta = dr.Item("IdAlerta")
            nombreFormato = dr.Item("NombreFormato")
            nombreAlerta = dr.Item("NombreAlerta")
            nroTransporte = dr.Item("NroTransporte")
            fecha = dr.Item("Fecha")
            atendidoPor = dr.Item("AtendidoPor")
            prioridad = dr.Item("prioridad")
            clasificacion = dr.Item("Clasificacion")

            'reemplaza las marcas
            prioridad = Utilidades.ObtenerLabelPrioridad(prioridad)
            asignadoA = IIf(atendidoPor.ToUpper() = "SIN ASIGNAR", "<em>" & atendidoPor & "</em>", atendidoPor)
            templateAux = templateAux.Replace("{INDICE}", contador)
            templateAux = templateAux.Replace("{ID_ALERTA}", idAlerta)
            templateAux = templateAux.Replace("{PRIORIDAD}", prioridad)
            templateAux = templateAux.Replace("{NOMBRE_FORMATO}", nombreFormato)
            templateAux = templateAux.Replace("{NOMBRE_ALERTA}", nombreAlerta)
            templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateAux = templateAux.Replace("{FECHA}", fecha)
            templateAux = templateAux.Replace("{ATENDIDO_POR}", atendidoPor)
            templateAux = templateAux.Replace("{ASIGNADO_A}", asignadoA)
            templateAux = templateAux.Replace("{CLASIFICACION}", clasificacion)
            sb.Append(templateAux)

            contador += 1
          Next
          sb.Append("  </tbody>")
          sb.Append("</table>")
        End If
      End If
      'agrega listado a json final
      sl.Add("listado", sb.ToString())
      sl.Add("totalRegistros", totalRegistros)

      retorno = MyJSON.ConvertSortedListToJSON(sl)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function


#End Region

#Region "GrabarEstadoLlamada"
  ''' <summary>
  ''' graba el estado de la llamada
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarEstadoLlamada() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim retorno As String = ""
    Dim idLlamada = Utilidades.IsNull(Request("idLlamada"), "")
    Dim idAlertaPadre = Utilidades.IsNull(Request("idAlertaPadre"), "-1")
    Dim idAlertaHija = Utilidades.IsNull(Request("idAlertaHija"), "-1")
    Dim idEscalamientoPorAlertaGrupoContacto = Utilidades.IsNull(Request("idEscalamientoPorAlertaGrupoContacto"), "-1")
    Dim nombreContacto = Server.UrlDecode(Utilidades.IsNull(Request("nombreContacto"), ""))
    Dim telefono = Server.UrlDecode(Utilidades.IsNull(Request("telefono"), ""))
    Dim destinoLocal = Server.UrlDecode(Utilidades.IsNull(Request("destinoLocal"), ""))
    Dim destinoLocalCodigo = Server.UrlDecode(Utilidades.IsNull(Request("destinoLocalCodigo"), ""))
    Dim accion = Server.UrlDecode(Utilidades.IsNull(Request("accion"), ""))
    Dim status As Integer

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      status = Alerta.GrabarHistorialLlamadaCallCenter(idLlamada, idAlertaPadre, idAlertaHija, idEscalamientoPorAlertaGrupoContacto, nombreContacto, telefono, _
                                                       destinoLocal, destinoLocalCodigo, oUsuario.Id, accion)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar el historial de la gestión de la alerta")
      Sistema.GrabarLogSesion(oUsuario.Id, accion & " " & nombreContacto & " (teléfono " & telefono & "): IdAlerta " & idAlertaPadre)
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

#End Region

#Region "ValidarClaveUsuarioConectado"
  ''' <summary>
  ''' valida la clave del usuario
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ValidarClaveUsuarioConectado() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim retorno As String = ""
    Dim clave As String = Server.UrlDecode(Utilidades.IsNull(Request("clave"), "-1"))

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      If (clave = "-1") Then
        retorno = "-1"
      Else
        clave = Criptografia.obtenerSHA1(clave)
        If (oUsuario.Password = clave) Then
          retorno = "1"
          Sistema.GrabarLogSesion(oUsuario.Id, "Valida clave cierre estado auxiliar: IdUsuario " & oUsuario.Id)
        Else
          retorno = "-1"
        End If
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region

#Region "EliminarDefinicionAlerta"
  ''' <summary>
  ''' elimina la definicion de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EliminarDefinicionAlerta() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""
    Dim idAlertaDefinicion As String = Utilidades.IsNull(Request("idAlertaDefinicion"), "-1")

    Try
      retorno = Alerta.EliminarDatosDefinicionAlerta(idAlertaDefinicion)
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}La DEFINICION DE ALERTA fue eliminada satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina definición de alerta: IdAlertaDefinicion " & idAlertaDefinicion)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

#End Region

#Region "ObtenerPerfilesPorFormato"
  ''' <summary>
  ''' obtiene perfiles asociados al formato consultado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerPerfilesPorFormato() As String
    Dim retorno As String = ""
    Dim idFormato As String = Server.UrlDecode(Utilidades.IsNull(Request("idFormato"), "-1"))
    Dim ds As New DataSet
    Dim totalRegistros As Integer

    Try
      ds = Formato.ObtenerPerfilesAsociados(idFormato)
      If (idFormato = "-1" Or ds Is Nothing) Then
        retorno = "[]"
      Else
        totalRegistros = ds.Tables(0).Rows.Count
        If (totalRegistros = 0) Then
          retorno = "[]"
        Else
          retorno = MyJSON.ConvertDataTableToJSON(ds.Tables(0))
        End If
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region

End Class
