﻿var ClasificacionConductor = {

  validarFormularioBusqueda: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: ClasificacionConductor.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var idClasificacionConductor = opciones.idClasificacionConductor;

      queryString += "?id=" + idClasificacionConductor;

      jQuery.fancybox({
        width: "80%",
        height: "80%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.ClasificacionConductorDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception ClasificacionConductor.abrirFormulario:\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.ClasificacionConductor.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception ClasificacionConductor.cerrarPopUp:\n" + e);
    }

  },

  cargarDesdePopUp: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception ClasificacionConductor.cargarDesdePopUp:\n" + e);
    }
  },

  validarFormularioDetalle: function () {
    var item;

    try {
      var existeNombre = ClasificacionConductor.existeNombreClasificacionConductor();

      var jsonValidarCampos = [
        { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que el nombre sea unico
      if (jQuery("#txtNombre").val() != "") {
        item = {
          valor_a_validar: existeNombre,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombre",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: ClasificacionConductor.validarFormularioDetalle\n" + e);
      return false;
    }

  },

  existeNombreClasificacionConductor: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var nombreClasificacionConductor = jQuery("#txtNombre").val();
      var idClasificacionConductor = jQuery("#txtIdClasificacionConductor").val();
      queryString += "op=ClasificacionConductor.Nombre";
      queryString += "&id=" + idClasificacionConductor;
      queryString += "&valor=" + Sistema.urlEncode(nombreClasificacionConductor);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único de la clasificación Operador");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único de la clasificación Operador");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception ClasificacionConductor.existeNombreClasificacionConductor:\n" + e);
    }

    return existe;
  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarClasificacionConductor";
      queryString += "&idClasificacionConductor=" + Sistema.urlEncode(opciones.idClasificacionConductor);

      if (confirm("Si elimina clasificación de operador se eliminarán todas las referencias asociados a él y no se podrá volver a recuperar.\n¿Desea eliminar la clasificación de conductor?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waSistema.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: ClasificacionConductor.eliminarRegistro\n" + e);
    }
  },

  validarEliminar: function () {
    try {
      if (confirm("Si elimina clasificación de operador se eliminarán todas las referencias asociados a él y no se podrá volver a recuperar.\n¿Desea eliminar la clasificación de conductor?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: ClasificacionConductor.validarEliminar\n" + e);
      return false;
    }
  }

};