﻿var Pagina = {

  validarFormularioBusqueda: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroCategoria", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Pagina.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  validarFormularioPagina: function () {
    var item;

    try {
      var existeNombreArchivo = Pagina.existeNombreArchivo();

      var jsonValidarCampos = [
          { elemento_a_validar: "txtTitulo", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtNombreArchivo", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtCategoria", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que el nombre del archivo sea unico
      if (jQuery("#txtNombreArchivo").val() != "") {
        item = {
          valor_a_validar: existeNombreArchivo,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombreArchivo",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      //valida los valores ingresados en el listado de funciones
      jsonValidarCampos = Pagina.validarValoresFunciones(jsonValidarCampos);

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        jQuery(".css-funcion-ver").attr("disabled", false);
        Pagina.encodearValorFunciones();
        return true;
      }

    } catch (e) {
      alert("Exception: Pagina.validarFormularioPagina\n" + e);
      return false;
    }

  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var idPagina = opciones.idPagina;

      queryString += "?id=" + idPagina;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.PaginaDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Pagina.abrirFormulario:\n" + e);
    }
  },

  addFilaFuncion: function () {
    var txtTotalFunciones = document.getElementById("txtTotalFunciones");
    var tablaFunciones = document.getElementById("tablaFunciones");
    var totalFunciones = 0;

    try {
      //incrementa el total de opciones
      totalFunciones = parseInt(txtTotalFunciones.value);
      totalFunciones++;
      txtTotalFunciones.value = totalFunciones;

      //crea una nueva fila con controles
      var tbody = tablaFunciones.getElementsByTagName("TBODY")[0];
      var row = document.createElement("TR");
      var idUnico = Sistema.crearIdUnico();

      //columna: NOMBRE
      var tdNombre = document.createElement("TD");
      txtIdFuncion = document.createElement("input");
      txtIdFuncion.type = "hidden";
      txtIdFuncion.name = "txtIdFuncionAdd";
      txtIdFuncion.id = "txtIdFuncionAdd_" + idUnico;
      txtIdFuncion.value = "-1";
      txtNombre = document.createElement("input");
      txtNombre.type = "text";
      txtNombre.name = "txtNombreAdd";
      txtNombre.id = "txtNombreAdd_" + idUnico;
      txtNombre.value = "";
      txtNombre.maxLength = 100;
      txtNombre.className = "form-control";
      spanErrorNombre = document.createElement("span");
      spanErrorNombre.id = "hErrorNombreAdd_" + idUnico;
      tdNombre.appendChild(txtIdFuncion);
      tdNombre.appendChild(txtNombre);
      tdNombre.appendChild(spanErrorNombre);

      //columna: VALOR
      var tdLlave = document.createElement("TD");
      txtLlave = document.createElement("input");
      txtLlave.type = "text";
      txtLlave.name = "txtLlaveAdd";
      txtLlave.id = "txtLlaveAdd_" + idUnico;
      txtLlave.value = "";
      txtLlave.maxLength = 100;
      txtLlave.className = "form-control";
      spanErrorLlave = document.createElement("span");
      spanErrorLlave.id = "hErrorLlaveAdd_" + idUnico;
      tdLlave.appendChild(txtLlave);
      tdLlave.appendChild(spanErrorLlave);

      //columna: VALOR
      var tdOrden = document.createElement("TD");
      txtOrden = document.createElement("input");
      txtOrden.type = "text";
      txtOrden.name = "txtOrdenAdd";
      txtOrden.id = "txtOrdenAdd_" + idUnico;
      txtOrden.value = "";
      txtOrden.maxLength = 100;
      txtOrden.className = "form-control";
      spanErrorOrden = document.createElement("span");
      spanErrorOrden.id = "hErrorOrdenAdd_" + idUnico;
      tdOrden.appendChild(txtOrden);
      tdOrden.appendChild(spanErrorOrden);

      //columna: ELIMINAR
      var tdEliminar = document.createElement("td");
      btnEliminar = document.createElement("button");
      btnEliminar.type = "button";
      btnEliminar.className = "btn btn-default";
      btnEliminar.onclick = function () { Sistema.deleteFila({ elem_tabla: "tablaFunciones", rowIndex: this.parentNode.parentNode.rowIndex, elem_total_registros: "txtTotalFunciones" }) };
      spanEliminar = document.createElement("span");
      spanEliminar.className = "glyphicon glyphicon-trash";
      btnEliminar.appendChild(spanEliminar);
      tdEliminar.appendChild(btnEliminar);

      //ingresa las nuevas columnas a la fila de la tabla    
      row.appendChild(tdNombre);
      row.appendChild(tdLlave);
      row.appendChild(tdOrden);
      row.appendChild(tdEliminar);
      tbody.appendChild(row);
    } catch (e) {
      alert("Exception Pagina.addFilaFuncion:\n" + e);
    }

  },

  cerrarPopUpPagina: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Pagina.cargarDesdePopUpPagina(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Pagina.cerrarPopUpPagina:\n" + e);
    }

  },

  cargarDesdePopUpPagina: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Pagina.cargarDesdePopUpPagina:\n" + e);
    }
  },

  eliminarFuncion: function (idFuncion, idTabla, indiceFila, objTotalRegistros) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=VerificarEliminacionFuncion";
      queryString += "&idFuncion=" + Sistema.urlEncode(idFuncion);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo verificar la eliminación de la función");
        } else {
          //elimina funcion que no tiene registros asociados
          if (respuesta == 0) {
            Sistema.deleteFila({ elem_tabla: idTabla, rowIndex: indiceFila, elem_total_registros: objTotalRegistros });
          } else {
            //confirma si desea eliminar la funcion que tiene registros asociados
            if (confirm("Al eliminar la función se eliminarán todas sus referencias, ¿desea continuar?")) {
              Sistema.deleteFilaSinConfirmacion({ elem_tabla: idTabla, rowIndex: indiceFila, elem_total_registros: objTotalRegistros });
            }
          }

        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo verificar la eliminación de la función");
      }
      jQuery.ajax({
        url: "../webAjax/waPagina.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Pagina.cargarDesdePopUpPagina:\n" + e);
      return false;
    }

  },

  existeNombreArchivo: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var nombreArchivo = jQuery("#txtNombreArchivo").val();
      var idPagina = jQuery("#txtIdPagina").val();
      queryString += "op=Pagina.NombreArchivo";
      queryString += "&id=" + idPagina;
      queryString += "&valor=" + Sistema.urlEncode(nombreArchivo);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único del archivo");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único del archivo");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Pagina.existeNombreArchivo:\n" + e);
    }

    return existe;
  },

  validarValoresFunciones: function (jsonValidarCampos) {
    var item, txtNombre, txtLlave, txtOrden, valorValidar, nombre, llave, orden, esRequerido;

    try {
      //------------------------------------------------------------
      //incluye las validaciones tradicionales por campo
      jQuery("input[name='txtNombreAdd']").each(function (indice, obj) {
        esRequerido = false;
        txtNombre = jQuery(this).attr("id");
        txtLlave = txtNombre.replace("txtNombre", "txtLlave");
        txtOrden = txtNombre.replace("txtNombre", "txtOrden");

        nombre = jQuery("#" + txtNombre).val();
        llave = jQuery("#" + txtLlave).val();
        orden = jQuery("#" + txtOrden).val();

        //si escribio algun valor, entonces todos los demas son obligatorios, sino queda opcional
        if (!Validacion.tipoVacio(nombre) || !Validacion.tipoVacio(llave) || !Validacion.tipoVacio(orden)) { esRequerido = true; }

        //valida Nombre
        item = { elemento_a_validar: txtNombre, requerido: esRequerido, tipo_validacion: "caracteres-prohibidos" };
        jsonValidarCampos.push(item);

        //valida Llave
        item = { elemento_a_validar: txtLlave, requerido: esRequerido, tipo_validacion: "alfanumerico" };
        jsonValidarCampos.push(item);

        //valida Orden
        item = { elemento_a_validar: txtOrden, requerido: esRequerido, tipo_validacion: "numero-entero-positivo-mayor-cero" };
        jsonValidarCampos.push(item);
      });

      //------------------------------------------------------------
      //verifica que por columna no existan valores repetidos
      var arrCamposPorRevisar = ["Nombre", "Llave", "Orden"];

      //recorre por columna
      jQuery.each(arrCamposPorRevisar, function (indice, campo) {
        //recorre por fila verifican si ese valor existe en las demas filas
        jQuery("input[name='txt" + campo + "Add']").each(function (indice, obj) {
          txtNombre = jQuery(this).attr("id");
          var valor = jQuery(this).val();
          var hError = txtNombre.replace("txt" + campo, "hError" + campo);
          var totalEncontrados = 0;
          valorValidar = 1; // se inicializa que el valor no esta repetido (1: No repetido | -1: repetido)

          //verifica por cada fila si el valor existe (se incluye la misma fila que esta verificando, por eso el totalEncontrados debe ser mayor a 1)
          jQuery("input[name='txt" + campo + "Add']").each(function (indice, obj) {
            var valorComparar = jQuery(this).val();
            if (Sistema.toUpper(valor) == Sistema.toUpper(valorComparar)) {
              totalEncontrados++;
              if (totalEncontrados > 1) {
                valorValidar = -1;
                return false;
              }
            }
          });

          //asigna la valdiacion por control
          var item = {
            valor_a_validar: valorValidar,
            requerido: false,
            tipo_validacion: "numero-entero-positivo",
            contenedor_mensaje_validacion: hError,
            mensaje_validacion: "El valor est&aacute; repetido"
          };
          jsonValidarCampos.push(item);

        }); // <-- jQuery("input[name='txtNombreAdd']").each(function (indice, obj)

      }); // <-- jQuery.each(arrCamposPorRevisar, function (indice, campo)

    } catch (e) {
      alert("Exception: Pagina.validarValoresFunciones\n" + e);
    }

    return jsonValidarCampos;
  },

  encodearValorFunciones: function () {
    var txtIdFuncion, txtNombre, txtLlave, txtOrden, idFuncion, nombre, llave, orden;

    try {
      //------------------------------------------------------------
      //incluye las validaciones tradicionales por campo
      jQuery("input[name='txtNombreAdd']").each(function (indice, obj) {
        txtNombre = jQuery(this).attr("id");
        txtIdFuncion = txtNombre.replace("txtNombre", "txtIdFuncion");
        txtLlave = txtNombre.replace("txtNombre", "txtLlave");
        txtOrden = txtNombre.replace("txtNombre", "txtOrden");

        idFuncion = jQuery("#" + txtIdFuncion).val();
        nombre = jQuery("#" + txtNombre).val();
        llave = jQuery("#" + txtLlave).val();
        orden = jQuery("#" + txtOrden).val();

        jQuery("#" + txtIdFuncion).val(Sistema.urlEncode(idFuncion));
        jQuery("#" + txtNombre).val(Sistema.urlEncode(nombre));
        jQuery("#" + txtLlave).val(Sistema.urlEncode(llave));
        jQuery("#" + txtOrden).val(Sistema.urlEncode(orden));
      });

    } catch (e) {
      alert("Exception: Pagina.encodearValorFunciones\n" + e);
    }

  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarPagina";
      queryString += "&idPagina=" + Sistema.urlEncode(opciones.idPagina);

      if (confirm("¿Está seguro de eliminar el registro?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waPagina.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Pagina.eliminarRegistro\n" + e);
    }
  },

  validarEliminar: function () {
    try {
      if (confirm("Si elimina la página se eliminarán todas las referencias asociadas a ellas y no se podrá recuperar\n¿Desea eliminar la página?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: Pagina.validarEliminar\n" + e);
      return false;
    }
  }

};