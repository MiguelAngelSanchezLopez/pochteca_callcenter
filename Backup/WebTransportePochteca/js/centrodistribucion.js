﻿var CentroDistribucion = {

  validarFormularioBusqueda: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroCodigo", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: CentroDistribucion.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var idCentroDistribucion = opciones.idCentroDistribucion;

      queryString += "?id=" + idCentroDistribucion;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.CentroDistribucionDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception CentroDistribucion.abrirFormulario:\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.CentroDistribucion.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception CentroDistribucion.cerrarPopUp:\n" + e);
    }

  },

  cargarDesdePopUp: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception CentroDistribucion.cargarDesdePopUp:\n" + e);
    }
  },

  validarFormularioCentroDistribucion: function () {
    var item;

    try {
      var existeNombreCentroDistribucion = CentroDistribucion.existeNombreCentroDistribucion();
      var existeCodigoCentroDistribucion = CentroDistribucion.existeCodigoCentroDistribucion();

      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtCodigo", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      //valida que el nombre del centro distribucion sea unico
      if (jQuery("#txtNombre").val() != "") {
        item = {
          valor_a_validar: existeNombreCentroDistribucion,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombre",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      //valida que el codigo del centro distribucion sea unico
      if (jQuery("#txtCodigo").val() != "") {
        item = {
          valor_a_validar: existeCodigoCentroDistribucion,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorCodigo",
          mensaje_validacion: "El c&oacute;digo ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        CentroDistribucion.obtenerUsuariosPorCargo();
        return true;
      }

    } catch (e) {
      alert("Exception: CentroDistribucion.validarFormularioCentroDistribucion\n" + e);
      return false;
    }

  },

  existeNombreCentroDistribucion: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var nombreCentroDistribucion = jQuery("#txtNombre").val();
      var idCentroDistribucion = jQuery("#txtIdCentroDistribucion").val();
      queryString += "op=CentroDistribucion.Nombre";
      queryString += "&id=" + idCentroDistribucion;
      queryString += "&valor=" + Sistema.urlEncode(nombreCentroDistribucion);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único del centro de distribución");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único del centro de distribución");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception CentroDistribucion.existeNombreCentroDistribucion:\n" + e);
    }

    return existe;
  },

  existeCodigoCentroDistribucion: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var codigoCentroDistribucion = jQuery("#txtCodigo").val();
      var idCentroDistribucion = jQuery("#txtIdCentroDistribucion").val();
      queryString += "op=CentroDistribucion.Codigo";
      queryString += "&id=" + idCentroDistribucion;
      queryString += "&valor=" + Sistema.urlEncode(codigoCentroDistribucion);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el código único del centro de distribución");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el código único del centro de distribución");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception CentroDistribucion.existeCodigoCentroDistribucion:\n" + e);
    }

    return existe;
  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarCentroDistribucion";
      queryString += "&idCentroDistribucion=" + Sistema.urlEncode(opciones.idCentroDistribucion);

      if (confirm("Si elimina el centro de distribución se eliminarán todas las referencias asociadas a él y no se podrá volver a recuperar.\n¿Desea eliminar el centro de distribución?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waSistema.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: CentroDistribucion.eliminarRegistro\n" + e);
    }
  },

  validarEliminar: function () {
    try {
      if (confirm("Si elimina el centro de distribución se eliminarán todas las referencias asociados a él y no se podrá volver a recuperar.\n¿Desea eliminar el centro de distribución?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: CentroDistribucion.validarEliminar\n" + e);
      return false;
    }
  },

  dibujarComboCargos: function () {
    var queryString = "";
    var idCentroDistribucion, categoria;

    try {
      idCentroDistribucion = jQuery("#txtIdCentroDistribucion").val();
      categoria = jQuery("#txtCategoria").val();

      //construye queryString
      queryString += "op=DibujarComboCargosCentroDistribucion";
      queryString += "&idCentroDistribucion=" + Sistema.urlEncode(idCentroDistribucion);
      queryString += "&categoria=" + Sistema.urlEncode(categoria);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener los cargos");
        } else {
          jQuery("#hListadoCargos").html(respuesta);
          jQuery("select[data-categoria=combo-cargo]").select2();
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener los cargos");
      }

      jQuery.ajax({
        url: "../webAjax/waSistema.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception: CentroDistribucion.dibujarComboCargos\n" + e);
    }
  },

  obtenerUsuariosPorCargo: function () {
    var arrUsuarios = [];
    var listado = "";
    var idUsuario;

    try {
      jQuery("select[data-categoria=combo-cargo]").each(function (indice, obj) {
        idUsuario = jQuery(obj).select2("val");
        if (!Validacion.tipoVacio(idUsuario)) {
          arrUsuarios.push(idUsuario);
        }
      });

      listado = arrUsuarios.join(",");
    } catch (e) {
      alert("Exception: CentroDistribucion.obtenerUsuariosPorCargo\n" + e);
    }

    //asigna valor al contol
    jQuery("#txtListadoUsuariosCargos").val(listado);
  }

};