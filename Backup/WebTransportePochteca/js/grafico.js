﻿var Grafico = {
  roundNumber: function (num, dec) {
    var result;

    try {
      result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    } catch (e) {
      alert("Exception Grafico.roundNumber:\n" + e);
      result = num;
    }

    return result;
  },

  torta: function (opciones) {
    var data, elem_contenedor, datosJSON, titulo, nombreSerie, datos, chart, animacion, width, height;

    try {
      data = opciones.data;
      elem_contenedor = opciones.elem_contenedor;

      if (data) {
        if (data.length > 0) {
          datosJSON = data[0];
          titulo = datosJSON.titulo;
          nombreSerie = datosJSON.nombreSerie;
          datos = eval(datosJSON.datos);
          animacion = datosJSON.animacion;
          width = datosJSON.width;
          height = datosJSON.height;

          chart = new Highcharts.Chart({
            colors: ["#7cb5ec", "#c71c22", "#7030a0", "#7798BF", "#55BF3B", "#f7a35c", "#DF5353", "#90ee7e", "#ff0066", "#aaeeee"],
            chart: {
              renderTo: elem_contenedor,
              type: "pie",
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              width: width, // 650; 400
              height: height, // 250; 150
              borderWidth: 2,
              borderColor: "#999"
            },
            title: {
              text: titulo
            },
            tooltip: {
              formatter: function () {
                return "<b>" + this.point.name + "</b>: " + Grafico.roundNumber(this.percentage, 2) + " %";
              }
            },
            plotOptions: {
              pie: {
                animation: animacion,
                allowPointSelect: true,
                cursor: "pointer",
                dataLabels: {
                  enabled: true,
                  color: '#000000',
                  connectorColor: '#000000',
                  formatter: function () {
                    return "<b>" + this.point.name + "</b>: " + Grafico.roundNumber(this.percentage, 2) + " %";
                  },
                  style: {
                    font: "bold 10px Arial, sans-serif"
                  }
                }
              }
            },
            series: [{
              name: nombreSerie,
              data: datos
            }]
          });

        } else {
          jQuery("#" + elem_contenedor).html("");
        }
      } else {
        jQuery("#" + elem_contenedor).html("");
      }
    } catch (e) {
      alert("Exception Grafico.torta:\n" + e);
    }

  },

  bar: function (opciones) {
    var data, elem_contenedor, datosJSON, titulo, nombreSerie, datos, chart, animacion, width, height;

    try {
      data = opciones.data;
      elem_contenedor = opciones.elem_contenedor;

      if (data) {
        if (data.length > 0) {
          datosJSON = data[0];
          titulo = datosJSON.titulo;
          nombreSerie = datosJSON.nombreSerie;
          datos = eval(datosJSON.datos);
          animacion = datosJSON.animacion;
          width = datosJSON.width;
          height = datosJSON.height;

          chart = new Highcharts.Chart({
            colors: ["#7cb5ec", "#c71c22", "#7030a0", "#7798BF", "#55BF3B", "#f7a35c", "#DF5353", "#90ee7e", "#ff0066", "#aaeeee"],
            chart: {
              renderTo: elem_contenedor,
              type: "bar",
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              width: width, // 650; 400
              height: height, // 250; 150
              borderWidth: 2,
              borderColor: "#999"
            },
            title: {
              text: titulo
            },
            xAxis: {
              categories: [datos.NombreCompleto]
            },
            yAxis: {
              min: 0,
              title: {
                text: '',
                align: 'high'
              },
              labels: {
                overflow: 'justify'
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
              }
            },
            plotOptions: {
              series: {
                stacking: 'normal'
              }
            },
            series: [{
              name: nombreSerie,
              data: datos
            }]
          });

        } else {
          jQuery("#" + elem_contenedor).html("");
        }
      } else {
        jQuery("#" + elem_contenedor).html("");
      }
    } catch (e) {
      alert("Exception Grafico.torta:\n" + e);
    }

  }



};