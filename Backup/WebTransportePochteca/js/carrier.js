﻿var Carrier = {
  /*
  templateTracto: "<div class=\"text-center pull-left sist-carrier-camion {SEMAFORO} show-popover\" data-toggle=\"popover\" data-trigger=\"hover\" data-placement=\"top\" data-content=\"{POPOVER}\">" +
  "  <img src=\"../img/truck.png\" alt=\"\" />" +
  "  {PLACA_TRACTO}" +
  "</div>",
  */
  templateTracto: "<div class=\"text-center pull-left sist-carrier-camion show-popover\" data-toggle=\"popover\" data-trigger=\"hover\" data-placement=\"top\" data-content=\"{POPOVER}\" onclick=\"Carrier.abrirVentana('{PAGINA}')\">" +
                  "  <img src=\"../img/{IMAGEN_CAMION}\" alt=\"\" />" +
                  "  {PLACA_TRACTO}" +
                  "</div>",


  templatePopoverTractoIzquierda: "<div class='small'>" +
                                  "<div><strong>Placa tracto: </strong>{PLACA_TRACTO}</div>" +
                                  "<div><strong>Cedis: </strong>{CEDIS}</div>" +
                                  "<div><strong>Fecha llegada: </strong>{FECHA_LLEGADA}</div>" +
                                  "<div><strong>Tiempo de espera: </strong>{TIEMPO_ESPERA}</div>" +
                                  "</div>",

  templatePopoverRemolquesEnPatio: "<div class='small'>" +
                                   "<div><strong>Placa remolque: </strong>{PLACA_REMOLQUE}</div>" +
                                   "<div><strong>IdEmbarque: </strong>{ID_EMBARQUE}</div>" +
                                   "<div><strong>Cedis: </strong>{CEDIS}</div>" +
                                   "<div><strong>Determinante: </strong>{DETERMINANTE}</div>" +
                                   "<div><strong>Tiempo de espera: </strong>{TIEMPO_ESPERA}</div>" +
                                   "</div>",

  templatePopoverRemolquesEnCortina: "<div class='small'>" +
                                     "<div><strong>Placa remolque: </strong>{PLACA_REMOLQUE}</div>" +
                                     "<div><strong>IdEmbarque: </strong>{ID_EMBARQUE}</div>" +
                                     "<div><strong>Cedis: </strong>{CEDIS}</div>" +
                                     "<div><strong>Determinante: </strong>{DETERMINANTE}</div>" +
                                     "<div><strong>Cortina: </strong>{CORTINA}</div>" +
                                     "<div><strong>Tiempo cargando: </strong>{TIEMPO_CARGANDO}</div>" +
                                     "</div>",

  templatePopoverEmbarquesEnRuta: "<div class='small'>" +
                                  "<div><strong>IdEmbarque: </strong>{ID_EMBARQUE}</div>" +
                                  "<div><strong>IdMaster: </strong>{ID_MASTER}</div>" +
                                  "<div><strong>Cedis: </strong>{CEDIS}</div>" +
                                  "<div><strong>Placa remolque: </strong>{PLACA_REMOLQUE}</div>" +
                                  "<div><strong>Placa tracto: </strong>{PLACA_TRACTO}</div>" +
                                  "<div><strong>Determinante: </strong>{DETERMINANTE}</div>" +
                                  "<div><strong>Tiempo estimado arribo: </strong>{TIEMPO_ESTIMADO_ARRIBO}</div>" +
                                  "<div><strong>Kilómetros recorridos: </strong>{KILOMETROS_RECORRIDOS}</div>" +
                                  "<div><strong>Total alertas: </strong>{TOTAL_ALERTAS}</div>" +
                                  "<div><strong>Tiempo del viaje: </strong>{TIEMPO_VIAJE}</div>" +
                                  "</div>",

  templatePopoverEmbarquesEnTienda: "<div class='small'>" +
                                    "<div><strong>IdEmbarque: </strong>{ID_EMBARQUE}</div>" +
                                    "<div><strong>IdMaster: </strong>{ID_MASTER}</div>" +
                                    "<div><strong>Cedis: </strong>{CEDIS}</div>" +
                                    "<div><strong>Placa remolque: </strong>{PLACA_REMOLQUE}</div>" +
                                    "<div><strong>Placa tracto: </strong>{PLACA_TRACTO}</div>" +
                                    "<div><strong>Determinante: </strong>{DETERMINANTE}</div>" +
                                    "<div><strong>Fecha Llegada: </strong>{FECHA_LLEGADA}</div>" +
                                    "<div><strong>Fecha Cita: </strong>{FECHA_CITA}</div>" +
                                    "<div><strong>Tipo Tienda: </strong>{TIPO_TIENDA}</div>" +
                                    "<div><strong>Tiempo sobrestadía: </strong>{TIEMPO_SOBRESTADIA}</div>" +
                                    "</div>",

  templatePopoverTractosRegresando: "<div class='small'>" +
                                    "<div><strong>Placa tracto: </strong>{PLACA_TRACTO}</div>" +
                                    "<div><strong>Cedis Destino: </strong>{CEDIS}</div>" +
                                    "<div><strong>Kilómetros recorridos: </strong>{KILOMETROS_RECORRIDOS}</div>" +
                                    "<div><strong>Tiempo llegada destino: </strong>{TIEMPO_LLEGADA_DESTINO}</div>" +
                                    "</div>",


  obtenerClassSemaforo: function (semaforo) {
    var clase = "";

    try {
      switch (semaforo) {
        case "rojo":
          clase = "alert-danger";
          break;
        case "amarillo":
          clase = "alert-warning";
          break;
        case "verde":
          clase = "alert-success";
          break;
      }
    } catch (e) {
      alert("Exception: Carrier.obtenerClassSemaforo\n" + e);
    }
    return clase;
  },


  //Métodos para carga de datos
  //---------------------------------------------------------------------------------------
  cargaTractosDisponibles: function () {
    try {

      //Obtiene el json con los datos
      var listObject = Carrier.obtenerTractosDisponibles();
      var jsonString = JSON.stringify(listObject);

      //Guarda el json en un hidden
      jQuery("#hJsonTractosDisponibles").val(jsonString);

      //Dibuja el json en pantalla
      Carrier.dibujarTractoDisponibles(listObject);

    } catch (e) {
      alert("Exception: Carrier.cargaTractosDisponibles\n" + e);
    }
  },

  cargaRemolquesEnPatio: function () {

    try {

      //Obtiene el json con los datos
      var listObject = Carrier.obtenerRemolquesEnPatio();
      var jsonString = JSON.stringify(listObject);

      //Guarda el json en un hidden
      jQuery("#hJsonRemolquesEnPatio").val(jsonString);

      //Dibuja el json en pantalla
      Carrier.dibujarRemolquesEnPatio(listObject);

    } catch (e) {
      alert("Exception: Carrier.cargaRemolquesEnPatio\n" + e);
    }

  },

  cargaRemolquesEnCortina: function () {
    try {

      //Obtiene el json con los datos
      var listObject = Carrier.obtenerRemolquesEnCortina();
      var jsonString = JSON.stringify(listObject);

      //Guarda el json en un hidden
      jQuery("#hJsonRemolquesEnCortina").val(jsonString);

      //Dibuja el json en pantalla
      Carrier.dibujarRemolquesEnCortina(listObject);

    } catch (e) {
      alert("Exception: Carrier.cargaRemolquesEnCortina\n" + e);
    }
  },

  cargaEmbarquesEnRuta: function () {
    try {

      //Obtiene el json con los datos
      var listObject = Carrier.obtenerEmbarqueEnRuta();
      var jsonString = JSON.stringify(listObject);

      //Guarda el json en un hidden
      jQuery("#hJsonEmbarqueEnRuta").val(jsonString);

      //Dibuja el json en pantalla
      Carrier.dibujarEmbarqueEnRuta(listObject);

    } catch (e) {
      alert("Exception: Carrier.cargaEmbarquesEnRuta\n" + e);
    }
  },

  cargaEmbarquesEnTienda: function () {
    try {

      //Obtiene el json con los datos
      var listObject = Carrier.obtenerEmbarqueEnTienda();
      var jsonString = JSON.stringify(listObject);

      //Guarda el json en un hidden
      jQuery("#hJsonEmbarqueEnTienda").val(jsonString);

      //Dibuja el json en pantalla
      Carrier.dibujarEmbarqueEnTienda(listObject);

    } catch (e) {
      alert("Exception: Carrier.cargaEmbarquesEnRuta\n" + e);
    }
  },

  cargaTractosRegresando: function () {
    try {

      //Obtiene el json con los datos
      var listObject = Carrier.obtenerTractoRegresando();
      var jsonString = JSON.stringify(listObject);

      //Guarda el json en un hidden
      jQuery("#hJsonTractoRegresando").val(jsonString);

      //Dibuja el json en pantalla
      Carrier.dibujarTractoRegresando(listObject);

    } catch (e) {
      alert("Exception: Carrier.cargaTractosRegresando\n" + e);
    }
  },

  abrirVentana: function (pagina) {
    try {
      jQuery.fancybox({
        width: "90%",
        height: "90%",
        modal: false,
        type: "iframe",
        href: "../page/" + pagina
      });
    } catch (e) {
      alert("Exception: Carrier.abrirVentana\n" + e);
    }
  },

  abrirAlertaMapa: function (lat, lon) {
    try {
      jQuery.fancybox({
        width: "90%",
        height: "90%",
        modal: false,
        type: "iframe",
        href: "../page/Carrier.EmbarqueEnRutaAlertaMapa.aspx?lat=" + lat + "&lon=" + lon
      });
    } catch (e) {
      alert("Exception: Carrier.abrirAlertaMapa\n" + e);
    }

    return false;
  },

  listarInformes: function () {
    var templateFilaAux, nombre, semana, archivo;
    var html = "";
    var templateFila = "";

    try {
      var jsonInformePdf = [
          { nombre: "Lorem ipsum dolor sit amet", semana: "Semana 1", archivo: "InformePrueba.pdf" }
        , { nombre: "Donec tristique nulla iaculis ante consectetur ornare", semana: "Semana 1", archivo: "InformePrueba.pdf" }
        , { nombre: "Maecenas pretium ex nunc", semana: "Semana 1", archivo: "InformePrueba.pdf" }
        , { nombre: "Donec bibendum cursus erat", semana: "Semana 2", archivo: "InformePrueba.pdf" }
        , { nombre: "Interdum et malesuada fames ac ante ipsum", semana: "Semana 2", archivo: "InformePrueba.pdf" }
        , { nombre: "Aliquam lacus mi, pulvinar et molestie eget", semana: "Semana 2", archivo: "InformePrueba.pdf" }
        , { nombre: "Quisque eros quam, dapibus ac congue id", semana: "Semana 2", archivo: "InformePrueba.pdf" }
        , { nombre: "Cras ut fermentum massa, blandit aliquam elit", semana: "Semana 3", archivo: "InformePrueba.pdf" }
        , { nombre: "Suspendisse accumsan nulla in dictum feugiat", semana: "Semana 3", archivo: "InformePrueba.pdf" }
        , { nombre: "Aenean id vestibulum ex. Quisque venenatis", semana: "Semana 3", archivo: "InformePrueba.pdf" }
      ];

      templateFila += "<tr>";
      templateFila += "  <td>{INDICE}</td>";
      templateFila += "  <td>{NOMBRE}</td>";
      templateFila += "  <td>{SEMANA}</td>";
      templateFila += "  <td>{ARCHIVO}</td>";
      templateFila += "</tr>";

      html += "<table class=\"table\">";
      html += "  <thead>";
      html += "    <tr>";
      html += "      <th>#</th>";
      html += "      <th>Informe</th>";
      html += "      <th>Semana</th>";
      html += "      <th>PDF</th>";
      html += "    </tr>";
      html += "  </thead>";
      html += "  <tbody>";

      jQuery.each(jsonInformePdf, function (indice, obj) {
        templateFilaAux = templateFila;
        nombre = obj.nombre;
        semana = obj.semana;
        archivo = obj.archivo;
        archivo = '<a href="' + archivo + '" target="_blank"><img src="../img/ico-pdf.png" alt="descargar pdf" /></a>';

        templateFilaAux = templateFilaAux.replace(/{INDICE}/ig, indice + 1);
        templateFilaAux = templateFilaAux.replace(/{NOMBRE}/ig, nombre);
        templateFilaAux = templateFilaAux.replace(/{SEMANA}/ig, semana);
        templateFilaAux = templateFilaAux.replace(/{ARCHIVO}/ig, archivo);
        html += templateFilaAux;
      });

      html += "  </tbody>";
      html += "</table>";

      jQuery("#hTablaDatos").html(html);
    } catch (e) {
      alert("Exception: Carrier.listarInformes\n" + e);
    }
  },

  //Métodos para obtener datos de webservices
  //---------------------------------------------------------------------------------------
  obtenerTractosDisponibles: function () {
    var listOject;

    try {

      var okFunc = function (response) {
        listOject = response.d;
      }

      var errFunc = function (response) {
        Sistema.dibujarMensajeUsuario(response.d);
      }

      jQuery.ajax({
        type: "POST",
        url: "../webAjax/waCarrier.aspx/getTractosDisponibles",
        data: "{idTransportista:'" + jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: okFunc,
        failure: errFunc
      });

    }
    catch (e) {
      alert("Exception Carrier.obtenerTractosDisponibles:\n" + e);
    }

    return listOject;
  },

  obtenerRemolquesEnPatio: function () {
    var listObject;

    try {

      var okFunc = function (response) {
        listObject = response.d;
      }

      var errFunc = function (response) {
        Sistema.dibujarMensajeUsuario(response.d);
      }

      jQuery.ajax({
        type: "POST",
        url: "../webAjax/waCarrier.aspx/getRemolquesEnPatio",
        data: "{idTransportista:'" + jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: okFunc,
        failure: errFunc
      });

    }
    catch (e) {
      alert("Exception Carrier.obtenerRemolquesEnPatio:\n" + e);
    }

    return listObject;
  },

  obtenerRemolquesEnCortina: function () {
    var listObject;

    try {

      var okFunc = function (response) {
        listObject = response.d;
      }

      var errFunc = function (response) {
        Sistema.dibujarMensajeUsuario(response.d);
      }

      jQuery.ajax({
        type: "POST",
        url: "../webAjax/waCarrier.aspx/getRemolquesEnCortina",
        data: "{idTransportista:'" + jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: okFunc,
        failure: errFunc
      });

    }
    catch (e) {
      alert("Exception Carrier.obtenerRemolquesEnPatio:\n" + e);
    }

    return listObject;
  },

  obtenerEmbarqueEnRuta: function () {
    var listObject;

    try {

      var okFunc = function (response) {
        listObject = response.d;
      }

      var errFunc = function (response) {
        Sistema.dibujarMensajeUsuario(response.d);
      }

      jQuery.ajax({
        type: "POST",
        url: "../webAjax/waCarrier.aspx/getEmbarqueEnRuta",
        data: "{idTransportista:'" + jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: okFunc,
        failure: errFunc
      });

    }
    catch (e) {
      alert("Exception Carrier.obtenerEmbarqueEnRuta:\n" + e);
    }

    return listObject;
  },

  obtenerEmbarqueEnTienda: function () {
    var listObject;

    try {

      var okFunc = function (response) {
        listObject = response.d;
      }

      var errFunc = function (response) {
        Sistema.dibujarMensajeUsuario(response.d);
      }

      jQuery.ajax({
        type: "POST",
        url: "../webAjax/waCarrier.aspx/getEmbarqueEnTienda",
        data: "{idTransportista:'" + jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: okFunc,
        failure: errFunc
      });

    }
    catch (e) {
      alert("Exception Carrier.obtenerEmbarqueEnRuta:\n" + e);
    }

    return listObject;
  },

  obtenerTractoRegresando: function () {
    var listObject;

    try {

      var okFunc = function (response) {
        listObject = response.d;
      }

      var errFunc = function (response) {
        Sistema.dibujarMensajeUsuario(response.d);
      }

      jQuery.ajax({
        type: "POST",
        url: "../webAjax/waCarrier.aspx/getTractoRegresando",
        data: "{idTransportista:'" + jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: okFunc,
        failure: errFunc
      });

    }
    catch (e) {
      alert("Exception Carrier.obtenerEmbarqueEnRuta:\n" + e);
    }

    return listObject;
  },

  //Métodos para dibujar
  //---------------------------------------------------------------------------------------
  dibujarTractoDisponibles: function (listObject) {
    var templateAux, templatePopoverAux, placaTracto, nombreCompletoCedis, fechaLlegada, tiempoEspera, semaforo;
    try {

      if (listObject              //Que exista el objeto
      && listObject.length != 0) {//Que tenga registros

        //Limpia control
        jQuery("#hTractosDisponibles").html("");

        //Se recorre el json 
        jQuery.each(listObject, function (indice, obj) {
          templateAux = Carrier.templateTracto;
          templatePopoverAux = Carrier.templatePopoverTractoIzquierda;
          placaTracto = obj.PlacaTracto;
          nombreCompletoCedis = obj.NombreCompletoCedis;
          fechaLlegada = obj.FechaLlegada;
          tiempoEspera = obj.TiempoEspera;
          semaforo = obj.Semaforo;
          semaforo = Carrier.obtenerClassSemaforo(semaforo);

          templatePopoverAux = templatePopoverAux.replace(/{PLACA_TRACTO}/ig, placaTracto);
          templatePopoverAux = templatePopoverAux.replace(/{CEDIS}/ig, nombreCompletoCedis);
          templatePopoverAux = templatePopoverAux.replace(/{FECHA_LLEGADA}/ig, fechaLlegada);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_ESPERA}/ig, tiempoEspera);

          templateAux = templateAux.replace(/{PLACA_TRACTO}/ig, placaTracto);
          templateAux = templateAux.replace(/{POPOVER}/ig, templatePopoverAux);
          templateAux = templateAux.replace(/{SEMAFORO}/ig, semaforo);
          templateAux = templateAux.replace(/{IMAGEN_CAMION}/ig, "truck.png");
          templateAux = templateAux.replace(/{PAGINA}/ig, "Carrier.TractoDisponible.aspx?json=" + encodeURIComponent(JSON.stringify(obj)));
          jQuery("#hTractosDisponibles").append(templateAux);
        });

        jQuery("#hTotalTractosDisponibles").html(listObject.length);
        jQuery("#dvFiltroTracto").show();
      }
      else {

        if (jQuery("#txtFiltroTracto").val().length === 0) {
          jQuery("#dvFiltroTracto").hide();
        }

        jQuery("#hTractosDisponibles").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hTotalTractosDisponibles").html(0);
      }
    } catch (e) {
      alert("Exception: Carrier.dibujarTractoDisponibles\n" + e);
    }
  },

  dibujarRemolquesEnPatio: function (listObject) {
    var templateAux, templatePopoverAux, placaRemolque, placaRemolqueHtml, nombreCompletoCedis, idEmbarque, idMaster, tiempoEspera, semaforo, semaforoAux, imagenCamion, determinante;

    try {

      //Valida:       
      if (listObject                    //Que exista el objeto
          && listObject.length == 2     //Que tenga resumen y el detalle
          && listObject[1].length != 0) {//Que el detalle tenga registros

        //Resumen
        //-----------------------------------------
        var listObjectResumen = listObject[0];
        jQuery.each(listObjectResumen, function (indice, obj) {

          if (obj.Semaforo == "ROJO") {
            jQuery("#dvTiempoExcedidoRemolquesEnPatio").html("<p><strong>Tiempo excedido: " + obj.Total + "</strong> (Tiempo promedio:" + obj.TiempoPromedio + ")</p>");
          } else {
            jQuery("#dvTiempoRemolquesEnPatio").html("<p><strong>En tiempo: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
          }

        });

        //Detalle
        //-----------------------------------------
        jQuery("#hRemolquesEnPatio").html("");

        var listObjectDetalle = listObject[1];
        jQuery.each(listObjectDetalle, function (indice, obj) {
          templateAux = Carrier.templateTracto;
          templatePopoverAux = Carrier.templatePopoverRemolquesEnPatio;
          placaRemolque = obj.PlacaRemolque;
          nombreCompletoCedis = obj.NombreCompletoCedis;
          idEmbarque = obj.IdEmbarque;
          idMaster = obj.IdMaster;
          tiempoEspera = obj.TiempoEspera;
          determinante = obj.Determinante;
          semaforo = obj.Semaforo;
          semaforoAux = obj.Semaforo;

          tiempoEspera = (semaforo === "ROJO") ? "<strong class='text-danger'>" + tiempoEspera + "</strong>" : "<strong class='text-success'>" + tiempoEspera + "</strong>";
          placaRemolqueHtml = (semaforo === "ROJO") ? "<strong class='text-danger'>" + placaRemolque + "</strong>" : placaRemolque;
          imagenCamion = (semaforo === "ROJO") ? "trailer_error.png" : "trailer_ok.png";
          semaforo = Carrier.obtenerClassSemaforo(semaforo);

          templatePopoverAux = templatePopoverAux.replace(/{PLACA_REMOLQUE}/ig, placaRemolque);
          templatePopoverAux = templatePopoverAux.replace(/{CEDIS}/ig, nombreCompletoCedis);
          templatePopoverAux = templatePopoverAux.replace(/{ID_EMBARQUE}/ig, idEmbarque);
          templatePopoverAux = templatePopoverAux.replace(/{ID_MASTER}/ig, idMaster);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_ESPERA}/ig, tiempoEspera);
          templatePopoverAux = templatePopoverAux.replace(/{DETERMINANTE}/ig, determinante);

          templateAux = templateAux.replace(/{PLACA_TRACTO}/ig, placaRemolqueHtml);
          templateAux = templateAux.replace(/{POPOVER}/ig, templatePopoverAux);
          templateAux = templateAux.replace(/{SEMAFORO}/ig, semaforo);
          templateAux = templateAux.replace(/{IMAGEN_CAMION}/ig, imagenCamion);
          templateAux = templateAux.replace(/{PAGINA}/ig, "Carrier.RemolqueEnPatio.aspx?semaforo=" + semaforoAux + "&json=" + encodeURIComponent(JSON.stringify(obj)));

          jQuery("#hRemolquesEnPatio").append(templateAux);
        });

        jQuery("#hTotalRemolquesEnPatio").html(listObjectDetalle.length);
        jQuery("#dvFiltroRemolquesEnPatio").show();
      }
      else {

        if (jQuery("#txtFiltroRemolquesEnPatio").val().length === 0) {
          jQuery("#dvFiltroRemolquesEnPatio").hide();
        }
        jQuery("#dvTiempoExcedidoRemolquesEnPatio").html("");
        jQuery("#dvTiempoRemolquesEnPatio").html("");
        jQuery("#hRemolquesEnPatio").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hTotalRemolquesEnPatio").html(0);
      }
    } catch (e) {
      alert("Exception: Carrier.dibujarRemolquesEnPatio\n" + e);
    }
  },

  dibujarRemolquesEnCortina: function (listObject) {
    var templateAux, templatePopoverAux, placaRemolque, placaRemolqueHtml, nombreCompletoCedis, idEmbarque, idMaster, tiempoCargando, semaforo, semaforoAux, imagenCamion, determinante, cortina;

    try {

      //Valida:       
      if (listObject                    //Que exista el objeto
          && listObject.length == 2      //Que tenga resumen y el detalle
          && listObject[1].length != 0) {//Que el detalle tenga registros

        //Resumen
        //-----------------------------------------
        var listObjectResumen = listObject[0];
        jQuery.each(listObjectResumen, function (indice, obj) {

          if (obj.Semaforo == "ROJO") {
            jQuery("#dvTiempoExcedidoRemolquesEnCortina").html("<p><strong>Tiempo excedido: " + obj.Total + "</strong> (Tiempo promedio:" + obj.TiempoPromedio + ")</p>");
          } else {
            jQuery("#dvTiempoRemolquesEnCortina").html("<p><strong>En tiempo: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
          }

        });

        //Detalle
        //-----------------------------------------

        jQuery("#hRemolquesEnCortina").html("");

        var listObjectDetalle = listObject[1];
        jQuery.each(listObjectDetalle, function (indice, obj) {
          templateAux = Carrier.templateTracto;
          templatePopoverAux = Carrier.templatePopoverRemolquesEnCortina;
          placaRemolque = obj.PlacaRemolque;
          nombreCompletoCedis = obj.NombreCompletoCedis;
          idEmbarque = obj.IdEmbarque;
          idMaster = obj.idMaster;
          tiempoCargando = obj.TiempoCargando;
          determinante = obj.Determinante;
          cortina = obj.Cortina;
          semaforo = obj.Semaforo;
          semaforoAux = obj.Semaforo;

          tiempoCargando = (semaforo === "ROJO") ? "<strong class='text-danger'>" + tiempoCargando + "</strong>" : "<strong class='text-success'>" + tiempoCargando + "</strong>";
          placaRemolqueHtml = (semaforo === "ROJO") ? "<strong class='text-danger'>" + placaRemolque + "</strong>" : placaRemolque;
          imagenCamion = (semaforo === "ROJO") ? "trailer_error.png" : "trailer_ok.png";
          semaforo = Carrier.obtenerClassSemaforo(semaforo);

          templatePopoverAux = templatePopoverAux.replace(/{PLACA_REMOLQUE}/ig, placaRemolque);
          templatePopoverAux = templatePopoverAux.replace(/{CEDIS}/ig, nombreCompletoCedis);
          templatePopoverAux = templatePopoverAux.replace(/{ID_EMBARQUE}/ig, idEmbarque);
          templatePopoverAux = templatePopoverAux.replace(/{ID_MASTER}/ig, idMaster);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_CARGANDO}/ig, tiempoCargando);
          templatePopoverAux = templatePopoverAux.replace(/{DETERMINANTE}/ig, determinante);
          templatePopoverAux = templatePopoverAux.replace(/{CORTINA}/ig, cortina);

          templateAux = templateAux.replace(/{PLACA_TRACTO}/ig, placaRemolqueHtml);
          templateAux = templateAux.replace(/{POPOVER}/ig, templatePopoverAux);
          templateAux = templateAux.replace(/{SEMAFORO}/ig, semaforo);
          templateAux = templateAux.replace(/{IMAGEN_CAMION}/ig, imagenCamion);
          templateAux = templateAux.replace(/{PAGINA}/ig, "Carrier.RemolqueEnCortina.aspx?semaforo=" + semaforoAux + "&json=" + encodeURIComponent(JSON.stringify(obj)));
          jQuery("#hRemolquesEnCortina").append(templateAux);
        });

        jQuery("#hTotalRemolquesEnCortina").html(listObjectDetalle.length);
        jQuery("#dvFiltroRemolquesEnCortina").show();
      }
      else {
        if (jQuery("#txtFiltroRemolquesEnCortina").val().length === 0) {
          jQuery("#dvFiltroRemolquesEnCortina").hide();
        }
        jQuery("#dvTiempoExcedidoRemolquesEnCortina").html("");
        jQuery("#dvTiempoRemolquesEnCortina").html("");
        jQuery("#hRemolquesEnCortina").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hTotalRemolquesEnCortina").html(0);
      }
    } catch (e) {
      alert("Exception: Carrier.dibujarRemolquesEnCortina\n" + e);
    }
  },

  dibujarEmbarqueEnRuta: function (listObject) {
    var templateAux, templatePopoverAux, idEmbarque, idMaster, nombreCompletoCedis, placaRemolque, placaTracto, determinante, tiempoViaje, tiempoEstimadoArribo, kilometrosRecorridos, totalAlertas;
    var placaRemolqueHtml, semaforo, semaforoAux, imagenCamion;

    try {

      //Valida:       
      if (listObject //Que exista el objeto
        && listObject.length == 2 //Que tenga resumen y el detalle
        && listObject[1].length != 0) { //Que el detalle tenga registros

        //Resumen
        //-----------------------------------------
        var listObjectResumen = listObject[0];
        jQuery.each(listObjectResumen, function (indice, obj) {

          if (obj.Semaforo == "ROJO") {
            jQuery("#dvAtrasadosEmbarqueEnRuta").html("<p><strong>Atrasados: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
          } else {
            jQuery("#dvATiempoEmbarqueEnRuta").html("<p><strong>A tiempo: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
          }

        });

        //Detalle
        //-----------------------------------------

        jQuery("#hEmbarquesEnRuta").html("");

        var listObjectDetalle = listObject[1];
        jQuery.each(listObjectDetalle, function (indice, obj) {
          templateAux = Carrier.templateTracto;
          templatePopoverAux = Carrier.templatePopoverEmbarquesEnRuta;
          idEmbarque = obj.IdEmbarque;
          idMaster = obj.IdMaster;
          nombreCompletoCedis = obj.NombreCompletoCedis;
          placaRemolque = obj.PlacaRemolque;
          placaTracto = obj.PlacaTracto;
          determinante = obj.Determinante;
          tiempoViaje = obj.TiempoViaje;
          tiempoEstimadoArribo = obj.TiempoEstimadoArribo;
          kilometrosRecorridos = obj.KilometrosRecorridos;
          totalAlertas = obj.TotalAlertas;
          semaforo = obj.Semaforo;
          semaforoAux = obj.Semaforo;

          tiempoViaje = (semaforo === "ROJO") ? "<strong class='text-danger'>" + tiempoViaje + "</strong>" : "<strong class='text-success'>" + tiempoViaje + "</strong>";
          placaRemolqueHtml = (semaforo === "ROJO") ? "<strong class='text-danger'>" + placaTracto + "</strong>" : placaTracto;
          imagenCamion = (semaforo === "ROJO") ? "truck_azul_error.png" : "truck_azul_ok.png";
          semaforo = Carrier.obtenerClassSemaforo(semaforo);

          templatePopoverAux = templatePopoverAux.replace(/{ID_EMBARQUE}/ig, idEmbarque);
          templatePopoverAux = templatePopoverAux.replace(/{ID_MASTER}/ig, idMaster);
          templatePopoverAux = templatePopoverAux.replace(/{CEDIS}/ig, nombreCompletoCedis);
          templatePopoverAux = templatePopoverAux.replace(/{PLACA_REMOLQUE}/ig, placaRemolque);
          templatePopoverAux = templatePopoverAux.replace(/{PLACA_TRACTO}/ig, placaTracto);
          templatePopoverAux = templatePopoverAux.replace(/{DETERMINANTE}/ig, determinante);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_VIAJE}/ig, tiempoViaje);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_ESTIMADO_ARRIBO}/ig, tiempoEstimadoArribo);
          templatePopoverAux = templatePopoverAux.replace(/{KILOMETROS_RECORRIDOS}/ig, kilometrosRecorridos);
          templatePopoverAux = templatePopoverAux.replace(/{TOTAL_ALERTAS}/ig, totalAlertas);

          templateAux = templateAux.replace(/{PLACA_TRACTO}/ig, placaRemolqueHtml);
          templateAux = templateAux.replace(/{POPOVER}/ig, templatePopoverAux);
          templateAux = templateAux.replace(/{SEMAFORO}/ig, semaforo);
          templateAux = templateAux.replace(/{IMAGEN_CAMION}/ig, imagenCamion);
          templateAux = templateAux.replace(/{PAGINA}/ig, "Carrier.EmbarqueEnRuta.aspx?semaforo=" + semaforoAux + "&json=" + encodeURIComponent(JSON.stringify(obj)));
          jQuery("#hEmbarquesEnRuta").append(templateAux);
        });

        jQuery("#hTotalEmbarquesEnRuta").html(listObjectDetalle.length);
        jQuery("#dvFiltroEmbarqueEnRuta").show();
      }
      else {
        if (jQuery("#txtFiltroEmbarqueEnRuta").val().length === 0) {
          jQuery("#dvFiltroEmbarqueEnRuta").hide();
        }
        jQuery("#dvAtrasadosEmbarqueEnRuta").html("");
        jQuery("#dvATiempoEmbarqueEnRuta").html("");
        jQuery("#hEmbarquesEnRuta").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hTotalEmbarquesEnRuta").html(0);
      }
    } catch (e) {
      alert("Exception: Carrier.dibujarEmbarqueEnRuta\n" + e);
    }
  },

  dibujarEmbarqueEnTienda: function (listObject) {
    var templateAux, templatePopoverAux, idEmbarque, idMaster, nombreCompletoCedis, placaRemolque, placaTracto, determinante, fechaLlegada, fechaCita, tipoTienda, tiempoSobrestadia, estadoEnTienda;
    var placaRemolqueHtml, semaforo, semaforoAux, imagenCamion;
    var countDescargando = 0;
    var countEsperando = 0;

    try {

      //Valida:       
      if (listObject //Que exista el objeto
        && listObject.length == 2 //Que tenga resumen y el detalle
        && listObject[1].length != 0) { //Que el detalle tenga registros

        //Resumen
        //-----------------------------------------
        var listObjectResumen = listObject[0];
        jQuery.each(listObjectResumen, function (indice, obj) {

          if (obj.Semaforo == "ROJO") {
            if (obj.Estado === "DESCARGANDO") {
              jQuery("#dvAtrasadosDescargando").html("<p><strong>Atrasados: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
            } else {
              jQuery("#dvAtrasadosEsperando").html("<p><strong>Atrasados: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
            }
          } else {
            if (obj.Estado === "DESCARGANDO") {
              jQuery("#dvATiempoDescargando").html("<p><strong>A tiempo: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
            } else {
              jQuery("#dvATiempoEsperando").html("<p><strong>A tiempo: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
            }
          }

        });

        //Detalle
        //-----------------------------------------
        jQuery("#hEmbarquesEnTiendaDescargando").html("");
        jQuery("#hEmbarquesEnTiendaEsperando").html("");

        var listObjectDetalle = listObject[1];
        jQuery.each(listObjectDetalle, function (indice, obj) {
          templateAux = Carrier.templateTracto;
          templatePopoverAux = Carrier.templatePopoverEmbarquesEnTienda;
          idEmbarque = obj.IdEmbarque;
          idMaster = obj.IdMaster;
          nombreCompletoCedis = obj.NombreCompletoCedis;
          placaRemolque = obj.PlacaRemolque;
          placaTracto = obj.PlacaTracto;
          determinante = obj.Determinante;
          fechaLlegada = obj.FechaLlegada;
          fechaCita = obj.FechaCita;
          tipoTienda = obj.TipoTienda;
          tiempoSobrestadia = obj.TiempoSobreestadia;
          semaforo = obj.Semaforo;
          semaforoAux = obj.Semaforo;
          estadoEnTienda = obj.EstadoEnTienda;

          tiempoSobrestadia = (semaforo === "ROJO") ? "<strong class='text-danger'>" + tiempoSobrestadia + "</strong>" : "<strong class='text-success'>" + tiempoSobrestadia + "</strong>";
          placaRemolqueHtml = (semaforo === "ROJO") ? "<strong class='text-danger'>" + placaTracto + "</strong>" : placaTracto;
          imagenCamion = (semaforo === "ROJO") ? "truck_azul_error.png" : "truck_azul_ok.png";
          semaforo = Carrier.obtenerClassSemaforo(semaforo);

          templatePopoverAux = templatePopoverAux.replace(/{ID_EMBARQUE}/ig, idEmbarque);
          templatePopoverAux = templatePopoverAux.replace(/{ID_MASTER}/ig, idMaster);
          templatePopoverAux = templatePopoverAux.replace(/{CEDIS}/ig, nombreCompletoCedis);
          templatePopoverAux = templatePopoverAux.replace(/{PLACA_REMOLQUE}/ig, placaRemolque);
          templatePopoverAux = templatePopoverAux.replace(/{PLACA_TRACTO}/ig, placaTracto);
          templatePopoverAux = templatePopoverAux.replace(/{DETERMINANTE}/ig, determinante);
          templatePopoverAux = templatePopoverAux.replace(/{FECHA_LLEGADA}/ig, fechaLlegada);
          templatePopoverAux = templatePopoverAux.replace(/{FECHA_CITA}/ig, fechaCita);
          templatePopoverAux = templatePopoverAux.replace(/{TIPO_TIENDA}/ig, tipoTienda);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_SOBRESTADIA}/ig, tiempoSobrestadia);

          templateAux = templateAux.replace(/{PLACA_TRACTO}/ig, placaRemolqueHtml);
          templateAux = templateAux.replace(/{POPOVER}/ig, templatePopoverAux);
          templateAux = templateAux.replace(/{SEMAFORO}/ig, semaforo);
          templateAux = templateAux.replace(/{IMAGEN_CAMION}/ig, imagenCamion);
          templateAux = templateAux.replace(/{PAGINA}/ig, "Carrier.EmbarqueEnTienda.aspx?semaforo=" + semaforoAux + "&json=" + encodeURIComponent(JSON.stringify(obj)));

          if (estadoEnTienda === "DESCARGANDO") {
            jQuery("#hEmbarquesEnTiendaDescargando").append(templateAux);
            countDescargando = countDescargando + 1;
          } else {
            jQuery("#hEmbarquesEnTiendaEsperando").append(templateAux);
            countEsperando = countEsperando + 1;
          }

        });

        jQuery("#hTotalEmbarquesEnTienda").html(listObjectDetalle.length);
        jQuery("#hTotalEmbarquesEnTiendaDescargando").html(countDescargando);
        jQuery("#hTotalEmbarquesEnTiendaEsperando").html(countEsperando);
        jQuery("#dvFiltroEmbarqueEnTienda").show();
      }
      else {
        if (jQuery("#txtFiltroEmbarqueEnTienda").val().length === 0) {
          jQuery("#dvFiltroEmbarqueEnTienda").hide();
        }
        jQuery("#dvAtrasadosDescargando").html("");
        jQuery("#dvATiempoDescargando").html("");
        jQuery("#dvAtrasadosEsperando").html("");
        jQuery("#dvATiempoEsperando").html("");
        jQuery("#hEmbarquesEnTiendaDescargando").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hEmbarquesEnTiendaEsperando").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hTotalEmbarquesEnTienda").html(0);
        jQuery("#hTotalEmbarquesEnTiendaDescargando").html(0);
        jQuery("#hTotalEmbarquesEnTiendaEsperando").html(0);
      }
    } catch (e) {
      alert("Exception: Carrier.dibujarEmbarqueEnTienda\n" + e);
    }
  },

  dibujarTractoRegresando: function (listObject) {
    var templateAux, templatePopoverAux, placaTracto, nombreCompletoCedis, tiempoLlegadaDestino, kilometrosRecorridos, tipoRegreso, placaRemolqueHtml, semaforo, semaforoAux, imagenCamion;

    try {

      //Valida:       
      if (listObject //Que exista el objeto
        && listObject.length == 2 //Que tenga resumen y el detalle
        && listObject[1].length != 0) { //Que el detalle tenga registros

        //Resumen
        //-----------------------------------------
        var listObjectResumen = listObject[0];
        jQuery.each(listObjectResumen, function (indice, obj) {

          if (obj.Semaforo == "ROJO") {
            jQuery("#dvAtrasadosTractoRegresando").html("<p><strong>Atrasados: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
          } else {
            jQuery("#dvATiempoTractoRegresando").html("<p><strong>A tiempo: " + obj.Total + "</strong> (Tiempo promedio: " + obj.TiempoPromedio + ")</p>");
          }

        });

        //Detalle
        //-----------------------------------------

        jQuery("#hTractosRegresando").html("");

        var listObjectDetalle = listObject[1];
        jQuery.each(listObjectDetalle, function (indice, obj) {
          templateAux = Carrier.templateTracto;
          templatePopoverAux = Carrier.templatePopoverTractosRegresando;
          placaTracto = obj.PlacaTracto;
          nombreCompletoCedis = obj.NombreCompletoCedis;
          tiempoLlegadaDestino = obj.TiempoLlegadaDestino;
          kilometrosRecorridos = obj.KilometrosDestino;
          tipoRegreso = obj.TipoRegreso;
          semaforo = obj.Semaforo;
          semaforoAux = obj.Semaforo;

          tiempoLlegadaDestino = (semaforo === "ROJO") ? "<strong class='text-danger'>" + tiempoLlegadaDestino + "</strong>" : "<strong class='text-success'>" + tiempoLlegadaDestino + "</strong>";
          placaRemolqueHtml = (semaforo === "ROJO") ? "<strong class='text-danger'>" + placaTracto + "</strong>" : placaTracto;
          imagenCamion = (semaforo === "ROJO") ? "truck_azul_error.png" : "truck_azul_ok.png";
          semaforo = Carrier.obtenerClassSemaforo(semaforo);

          templatePopoverAux = templatePopoverAux.replace(/{PLACA_TRACTO}/ig, placaTracto);
          templatePopoverAux = templatePopoverAux.replace(/{CEDIS}/ig, nombreCompletoCedis);
          templatePopoverAux = templatePopoverAux.replace(/{TIEMPO_LLEGADA_DESTINO}/ig, tiempoLlegadaDestino);
          templatePopoverAux = templatePopoverAux.replace(/{KILOMETROS_RECORRIDOS}/ig, kilometrosRecorridos);
          templatePopoverAux = templatePopoverAux.replace(/{TIPO_REGRESO}/ig, tipoRegreso);

          templateAux = templateAux.replace(/{PLACA_TRACTO}/ig, placaRemolqueHtml);
          templateAux = templateAux.replace(/{POPOVER}/ig, templatePopoverAux);
          templateAux = templateAux.replace(/{SEMAFORO}/ig, semaforo);
          templateAux = templateAux.replace(/{IMAGEN_CAMION}/ig, imagenCamion);
          templateAux = templateAux.replace(/{PAGINA}/ig, "Carrier.TractoRegresando.aspx?semaforo=" + semaforoAux + "&json=" + encodeURIComponent(JSON.stringify(obj)));
          jQuery("#hTractosRegresando").append(templateAux);
        });

        jQuery("#hTotalTractosRegresando").html(listObjectDetalle.length);
        jQuery("#dvFiltroTractoRegresando").show();
      }
      else {
        if (jQuery("#txtFiltroTractoRegresando").val().length === 0) {
          jQuery("#dvFiltroTractoRegresando").hide();
        }
        jQuery("#dvAtrasadosTractoRegresando").html("");
        jQuery("#dvATiempoTractoRegresando").html("");
        jQuery("#hTractosRegresando").html("<div><strong>No se encontraron registros.</strong></div>");
        jQuery("#hTotalTractosRegresando").html(0);
      }
    } catch (e) {
      alert("Exception: Carrier.dibujarTractoRegresando\n" + e);
    }
  },

  //Métodos para buscar 
  //---------------------------------------------------------------------------------------
  busquedaInstantanea: function (crtlFilter) {

    jQuery("#" + crtlFilter).keyup(function (e) {
      var jsonFilter, json;
      //obtenemos el texto introducido en el campo de búsqueda
      var filtro = jQuery("#" + crtlFilter).val();

      switch (crtlFilter) {
        case "txtFiltroTracto":
          json = JSON.parse(jQuery("#hJsonTractosDisponibles").val());
          jsonFilter = jQuery.grep(json, function (element, index) {
            return element.PlacaTracto.indexOf(filtro) == 0;
          });

          Carrier.dibujarTractoDisponibles(jsonFilter);
          break;

        case "txtFiltroRemolquesEnPatio":
          json = JSON.parse(jQuery("#hJsonRemolquesEnPatio").val());
          jsonFilter = jQuery.grep(json[1], function (element, index) {
            return element.PlacaRemolque.indexOf(filtro) == 0;
          });
          json[1] = jsonFilter;

          Carrier.dibujarRemolquesEnPatio(json);
          break;

        case "txtFiltroRemolquesEnCortina":
          json = JSON.parse(jQuery("#hJsonRemolquesEnCortina").val());
          jsonFilter = jQuery.grep(json[1], function (element, index) {
            return element.PlacaRemolque.indexOf(filtro) == 0;
          });
          json[1] = jsonFilter;

          Carrier.dibujarRemolquesEnCortina(json);
          break;

        case "txtFiltroEmbarqueEnRuta":
          json = JSON.parse(jQuery("#hJsonEmbarqueEnRuta").val());
          jsonFilter = jQuery.grep(json[1], function (element, index) {
            return element.PlacaTracto.indexOf(filtro) == 0;
          });
          json[1] = jsonFilter;

          Carrier.dibujarEmbarqueEnRuta(json);
          break;

        case "txtFiltroEmbarqueEnTienda":
          json = JSON.parse(jQuery("#hJsonEmbarqueEnTienda").val());
          jsonFilter = jQuery.grep(json[1], function (element, index) {
            return element.PlacaTracto.indexOf(filtro) == 0;
          });
          json[1] = jsonFilter;

          Carrier.dibujarEmbarqueEnTienda(json);
          break;

        case "txtFiltroTractoRegresando":
          json = JSON.parse(jQuery("#hJsonTractoRegresando").val());
          jsonFilter = jQuery.grep(json[1], function (element, index) {
            return element.PlacaTracto.indexOf(filtro) == 0;
          });
          json[1] = jsonFilter;

          Carrier.dibujarTractoRegresando(json);
          break;
      }

      setTimeout(function () { jQuery(".show-popover").popover({ html: true }); }, 100);
    });
  },

  validarControlesGenerico: function () {
    var item;

    try {
      var fechaDesde = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaDesde").val();
      var fechaHasta = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaHasta").val();

      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "ddlTransportista_ddlCombo", requerido: true }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaDesde", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaHasta", requerido: false, tipo_validacion: "fecha" }
        , { valor_a_validar: fechaDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaDesde, horaInicio: "00:00", fechaTermino: fechaHasta, horaTermino: "23:59"} }
        ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception Reporte.validarControlesGenerico:\n" + e);
    }

    return jsonValidarCampos;
  },

  cargarLineaTransporte: function () {

    //Establece id transportista
    var idTransportista = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlTransportista_ddlCombo").val();
    jQuery("#" + Sistema.PREFIJO_CONTROL + "hIdTransportista").val(idTransportista);

    jQuery("#hTractosDisponibles").html(Sistema.mensajeCargandoDatos());
    jQuery("#hRemolquesEnPatio").html(Sistema.mensajeCargandoDatos());
    jQuery("#hRemolquesEnCortina").html(Sistema.mensajeCargandoDatos());
    jQuery("#hEmbarquesEnRuta").html(Sistema.mensajeCargandoDatos());
    jQuery("#hEmbarquesEnTiendaDescargando").html(Sistema.mensajeCargandoDatos());
    jQuery("#hEmbarquesEnTiendaEsperando").html(Sistema.mensajeCargandoDatos());
    jQuery("#hTractosRegresando").html(Sistema.mensajeCargandoDatos());

    setTimeout(function () {
      Carrier.cargaTractosDisponibles();
      Carrier.cargaRemolquesEnPatio();
      Carrier.cargaRemolquesEnCortina();
      Carrier.cargaEmbarquesEnRuta();
      Carrier.cargaEmbarquesEnTienda();
      Carrier.cargaTractosRegresando();
    }, 500);
  }
};