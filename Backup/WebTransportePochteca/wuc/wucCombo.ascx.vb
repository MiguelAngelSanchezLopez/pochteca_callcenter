﻿Imports System.Data
Imports CapaNegocio

Public Class wucCombo
  Inherits System.Web.UI.UserControl

#Region "Enum"
  Enum eFuenteDatos As Integer
    Tabla = 1
    TipoGeneral = 2
  End Enum
#End Region

#Region "Propiedades del control"
  'Variables privadas
  Private _itemTodos As Boolean
  Private _itemSeleccione As Boolean
  Private _itemBlanco As Boolean
  Private _SelectedValue As String
  Private _SelectedIndex As String
  Private _SelectedItem As String
  Private _filtradoPorId As String = "-1"
  Private _tipoCombo As String
  Private _FuenteDatos As eFuenteDatos
  Private _deshabilitarConUnItem As Boolean

  Public Property TipoCombo() As String
    Get
      Return _tipoCombo
    End Get
    Set(ByVal Value As String)
      _tipoCombo = Value
    End Set
  End Property

  Public Property ItemTodos() As Boolean
    Get
      Return _itemTodos
    End Get
    Set(ByVal Value As Boolean)
      _itemTodos = Value
    End Set
  End Property

  Public Property ItemSeleccione() As Boolean
    Get
      Return _itemSeleccione
    End Get
    Set(ByVal Value As Boolean)
      _itemSeleccione = Value
    End Set
  End Property

  Public Property ItemBlanco() As Boolean
    Get
      Return _itemBlanco
    End Get
    Set(ByVal Value As Boolean)
      _itemBlanco = Value
    End Set
  End Property

  Public Property SelectedValue() As String
    Get
      'Si el combo esta vacio, lo llena
      If ddlCombo.Items.Count = 0 Then
        CargarCombo()
      End If
      Return ddlCombo.SelectedValue
    End Get
    Set(ByVal Value As String)
      _SelectedValue = Value
      'Si el combo esta vacio, lo llena
      If ddlCombo.Items.Count > 0 Then
        'Solo si existe el valor en la lista, lo deja seleccionado
        If Not ddlCombo.Items.FindByValue(_SelectedValue) Is Nothing Then
          ddlCombo.SelectedValue = _SelectedValue
        End If
      Else
        CargarCombo()
      End If
    End Set
  End Property

  Public Property SelectedItem() As String
    Get
      Return ddlCombo.SelectedItem.Text
    End Get
    Set(ByVal Value As String)
      ddlCombo.SelectedItem.Text = Value
    End Set
  End Property

  Public Property SelectedIndex() As Integer
    Get
      Return ddlCombo.SelectedIndex
    End Get
    Set(ByVal Value As Integer)
      If Value > ddlCombo.Items.Count Then Value = 0
      ddlCombo.SelectedIndex = Value
    End Set
  End Property

  Public ReadOnly Property ControlValueProperty() As String
    Get
      'Si el combo esta vacio, lo llena
      If ddlCombo.Items.Count = 0 Then
        CargarCombo()
      End If
      Return ddlCombo.SelectedValue
    End Get
  End Property

  Public Property funcionOnChange() As String
    Get
      Return ""
    End Get
    Set(ByVal Value As String)
      ddlCombo.Attributes.Add("onchange", Value)
    End Set
  End Property

  Public Property funcionOnKeyDown() As String
    Get
      Return ""
    End Get
    Set(ByVal Value As String)
      ddlCombo.Attributes.Add("onkeydown", Value)
    End Set
  End Property

  Public Property funcionOnKeyPress() As String
    Get
      Return ""
    End Get
    Set(ByVal Value As String)
      ddlCombo.Attributes.Add("onkeypress", Value)
    End Set
  End Property

  Public Property funcionOnFocus() As String
    Get
      Return ""
    End Get
    Set(ByVal Value As String)
      ddlCombo.Attributes.Add("onfocus", Value)
    End Set
  End Property

  Public Property funcionOnBlur() As String
    Get
      Return ""
    End Get
    Set(ByVal Value As String)
      ddlCombo.Attributes.Add("onblur", Value)
    End Set
  End Property

  Public Property FiltradoPorId() As String
    Get
      If ViewState("_filtradoPorId") Is Nothing Then
        Return "-1"
      Else
        Return ViewState("_filtradoPorId")
      End If
    End Get
    Set(ByVal Value As String)
      ViewState("_filtradoPorId") = Value
      CargarCombo()
    End Set
  End Property

  Public Property Enabled() As Boolean
    Get
      Return ddlCombo.Enabled
    End Get
    Set(ByVal Value As Boolean)
      ddlCombo.Enabled = Value
    End Set
  End Property

  Public Property CssClass() As String
    Get
      Return ddlCombo.CssClass
    End Get
    Set(ByVal Value As String)
      ddlCombo.CssClass = Value
    End Set
  End Property

  Public Property Width() As Unit
    Get
      Return ddlCombo.Width
    End Get
    Set(ByVal value As Unit)
      ddlCombo.Width = value
    End Set
  End Property

  Public Property FuenteDatos() As eFuenteDatos
    Get
      Return _FuenteDatos
    End Get
    Set(ByVal value As eFuenteDatos)
      _FuenteDatos = value
    End Set
  End Property

  Public ReadOnly Property Count() As Integer
    Get
      Return ddlCombo.Items.Count
    End Get
  End Property

  Public Property DeshabilitarConUnItem() As Boolean
    Get
      Return _deshabilitarConUnItem
    End Get
    Set(ByVal value As Boolean)
      _deshabilitarConUnItem = value
    End Set
  End Property



#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' carga el combo con los datos requeridos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CargarCombo()
    Dim ds As New DataSet
    'Recupera la informacion para llenar el combo
    ds = ObtenerDatos()
    ddlCombo.DataSource = ds

    'Establece propiedades del combobox que son la mismas para todos los tipos
    ddlCombo.DataTextField = "Texto"
    ddlCombo.DataValueField = "Valor"
    ddlCombo.DataBind()

    'Si la propiedad esta indicada, se agrega un elemento "--Todos--" al principio del combo
    If itemTodos Then
      Dim item As ListItem = New ListItem("--Todos--", "-1")
      'Se deja seleccionado el item Todos
      item.Selected = True
      ddlCombo.Items.Insert(0, item)
    End If

    'Si la propiedad esta indicada, se agrega un elemento "--Seleccione--" al principio del combo
    If itemSeleccione Then
      Dim item As ListItem = New ListItem("--Seleccione--", "")
      ddlCombo.Items.Insert(0, item)
    End If

    'Si la propiedad esta indicada, se agrega un elemento vacio al principio del combo
    If ItemBlanco Then
      Dim item As ListItem = New ListItem("", "")
      ddlCombo.Items.Insert(0, item)
    End If

    'se asigna el valor indicado por el usuario
    If _SelectedValue <> "" Then
      'Solo si existe el valor en la lista, lo deja seleccionado
      If Not ddlCombo.Items.FindByValue(_SelectedValue) Is Nothing Then
        ddlCombo.SelectedValue = _SelectedValue
      End If
    End If

    'si el combo tiene un solo item, entonces deja seleccionado el primero y deshabilita el combo
    If Me.DeshabilitarConUnItem() Then
      If Me.Count() = 2 Then
        Me.SelectedIndex = 1
        Me.Enabled = False
      End If
    End If

  End Sub

  ''' <summary>
  ''' Recupera la informacion desde la BD
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDatos() As DataSet
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim tipoCombo As String = Me.TipoCombo
    Dim filtroId As String = Utilidades.IsNull(Me.FiltradoPorId, "-1")
    If Me.FuenteDatos = eFuenteDatos.TipoGeneral Then
      ds = Sistema.ObtenerComboEstatico(tipoCombo)
    Else
      ds = Sistema.ObtenerComboDinamico(tipoCombo, filtroId)
    End If

    Return ds
  End Function
#End Region

#Region "Publicos"
  ''' <summary>
  ''' Selecciona item del DropDownList
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub SeleccionarValor(ByVal valor As String)
    Dim valorItem As String = String.Empty
    Dim valorSeleccionado As String = String.Empty

    Try
      If String.IsNullOrEmpty(valor) Then
        Me.ddlCombo.SelectedIndex = 0
        Exit Sub
      End If

      'Si el combo esta vacio, lo llena
      If ddlCombo.Items.Count = 0 Then
        CargarCombo()
      End If

      valor = "," & valor & ","  'formate el valor para buscarlo correctamente
      For Each item As ListItem In Me.ddlCombo.Items
        valorItem = "," & item.Value & ","
        If (InStr(valorItem, valor) > 0) Then
          valorSeleccionado = item.Value
          Exit For
        End If
      Next

      'deja seleccionado el item
      If String.IsNullOrEmpty(valorSeleccionado) Then
        Me.ddlCombo.SelectedIndex = 0
      Else
        Me.ddlCombo.SelectedValue = valorSeleccionado
      End If

    Catch ex As Exception
      Me.ddlCombo.SelectedIndex = 0
    End Try
  End Sub

  ''' <summary>
  ''' devuelve el total de items cargados
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function TotalItem() As Integer
    Dim total As Integer
    Try
      total = Me.ddlCombo.Items.Count
    Catch ex As Exception
      total = 0
    End Try
    Return total
  End Function
#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'Solo la primera vez se carga el combo con los datos
    If Not Page.IsPostBack Then
      CargarCombo()
    End If
  End Sub

End Class