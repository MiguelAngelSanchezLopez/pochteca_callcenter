﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_AlertasAtendidas')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_AlertasAtendidas'
  DROP  Procedure  dbo.spu_Reporte_AlertasAtendidas
END

GO

PRINT  'Creating Procedure spu_Reporte_AlertasAtendidas'
GO
CREATE Procedure dbo.spu_Reporte_AlertasAtendidas
/******************************************************************************
**    Descripcion  : obtiene total de alertas que se han gestionado
**    Por          : VSR, 25/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  --------------------------------------------------------------
  -- Alertas atendidas por el callcenter
  --------------------------------------------------------------
  SELECT DISTINCT
    [ID ALERTA] = AA.IdAlerta,
    [FORMATO] = AA.NombreFormato,
    [ALERTA] = AA.NombreAlerta,
    [PLACA TRACTO] = AA.PatenteTracto,
    [NOMBRE OPERADOR] = UPPER(AA.NombreConductor),
    [RUT OPERADOR] = AA.RutConductor + '-' + AA.DvConductor,
    [IDMASTER] = AA.NroTransporte,
    [IDEMBARQUE] = AA.IdEmbarque,
    [PRIORIDAD] = AA.Prioridad,
    [LINEA DE TRANSPORTE] = AA.NombreTransportista,
    [TIENDA DESTINO] = AA.LocalDestino,
    [DETERMINANTE DESTINO] = AA.LocalDestinoCodigo,
    [ZONA] = AA.Zona,
    [TIPO VIAJE] = AA.TipoViaje,
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(AA.FechaCreacion, 'FECHA_COMPLETA'),
    [ESCALAMIENTO] = CONVERT(VARCHAR,AA.NroEscalamiento) + CASE WHEN AA.Eliminado = 1 THEN ' (eliminado)' ELSE '' END,
    [ATENDIDO POR] = AA.AtendidoPor,
    [OBSERVACION] = AA.Observacion,
    [CATEGORIA ALERTA] = AA.CategoriaAlerta,
    [TIPO OBSERVACION] = AA.TipoObservacion,
    [MAPA ALERTA] = AA.AlertaMapa,
    [RESPUESTA LINEA DE TRANSPORTE] = AA.RespuestaCategoria,
    [OBSERVACION LINEA DE TRANSPORTE] = AA.RespuestaObservacion,
    [CLASIFICACION ALERTA] = AA.ClasificacionAlerta
  FROM
    vwu_AlertaAtendida AS AA
  WHERE
      ( AA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  ORDER BY
    [IDMASTER], [ID ALERTA], [ESCALAMIENTO]

END
