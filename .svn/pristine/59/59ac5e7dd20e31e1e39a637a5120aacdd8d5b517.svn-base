﻿<?xml version="1.0" encoding="utf-8" ?>
<reglas>
  <!--  
  **
  * =========================================================================
  * ESPECIFICACION ARCHIVO XML
  * =========================================================================
  * en este bloque se estructuran los tipos de alerta, que pueden ser: Apertura Puerta, Detención, Pérdida Señal, Temperatura
  *
  <detencion>
    **
    * en este bloque se estructuran las configuraciones de las alertas según el tipo
    * la definición de los atributos son:
    * nombre: Es el nombre de la alerta especificada en la base de datos del sistema
    *
    <alerta nombre="Combustible">
      **
      * la definición de los atributos son:
      * ocurrencias     : indica el número de ocurrencia (pertenezca a la tupla NroTransporte - DescripcionAlerta - LatTracto - LonTracto - LocalDestino) que se ejecutará el criterio
      * condicion       : determina la condición de la ocurrencia si debe ser mayor, igual o menor que el número indicado. Sus valores son: [Menor | MenorIgual | Igual | MayorIgual | Mayor]
      * canal           : Indica el canal por el cual se enviará la información de la gestión de la alerta. Los valores pueden ser: [CallCenter | Email | SMS]
      * criticidad      : indica la prioridad del criterio. Puede ser [Alta | Media | Baja]
      * horarioAtencion : indica el horario en que se gestionara la alerta. El formato es el siguiente <DIA_SEMANA>:<HORA_INICIO>-<HORA_TERMINO>|<DIA_SEMANA>:<HORA_INICIO>-<HORA_TERMINO>|...
      *                   donde los valores son los siguientes:
      *                   - DIA_SEMANA  : Es el dia de la semana en formato corto [ LU | MA | MI | JU | VI | SA | DO ]
      *                   - HORA_INICIO : Hora que comienza la gestion. El formato es <HHmm> (hora minutos sin puntos) [ Ejm: 0845 ]
      *                   - HORA_TERMINO: Hora que termina la gestion. El formato es <HHmm> (hora minutos sin puntos) [ Ejm: 2359 ]
      *
      <criterio ocurrencias="1" condicion="Igual" canal="SMS" criticidad="Baja" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359">
        **
        * en este bloque se definen los contactos que se contactarán como exclusivos o que se incluirán como copia en los envíos.
        * El funcionamiento es el siguiente:
        *     a) Si no se definió ningún contacto entonces envía la notificación de la alerta a todos los contactos que estén marcados con la opción de Canal de Comunicación Email|SMS|u otro...
        *     b) Si se definieron contactos estos se pueden separar en dos grupos:
        *        b.1) Exclusivos: Indica que solamente a ellos se les enviará la notificación, que puede ser a un perfil determinado o a uno personalizado.
        *        b.2) ConCopia  : Indica que ellos se incluirán como copia en los envíos de sms o email según corresponda. También puede estar definido un perfil o uno personalizado.
        *
        * Los atributos definidos son los siguientes:
        *   tipoEnvio           : Define si al contacto se le notifica en forma exclusiva o se agrega como copia. Los valores pueden ser [Exclusivo | ConCopia]
        *   formato             : Identifica a que empresa pertenece el contacto. Se usará el nombre definido en la base de datos del sistema.
        *   llaveFormato        : Clave para identificar la empresa sin usar el nombre.
        *   perfil              : Identifica el perfil del contacto.
        *   llavePerfil         : Clave para identificar el perfil.
        *   canalComunicacion   : Especifica por que canal se va a notificar al contacto. Los valores pueden ser [(SMS) | (Email) | (SMS)(Email)]
        *   horaAtencionInicio  : Indica el horario de inicio en que se le puede contactar. El formato de la hora es HH:mm (Ejm: "00:00")
        *   horaAtencionTermino : Indica el horario de término en que se le puede contactar. El formato de la hora es HH:mm (Ejm: "23:59")
        *   cargo               : Nombre del cargo del contacto personalizado.
        *   nombre              : Nombre del contacto personalizado.
        *   telefono            : Teléfono del contacto personalizado.
        *   email               : Email del contacto personalizado.
        
        ** listado de contactos exclusivos
        <contacto tipoEnvio="Exclusivo" formato="Unimarc" llaveFormato="Unimarc" perfil="AP Local" llavePerfil="APL" canalComunicacion="(SMS)" horaAtencionInicio="08:00" horaAtencionTermino="19:00"></contacto>
        <contacto tipoEnvio="Exclusivo" formato="Unimarc" llaveFormato="Unimarc" perfil="Gerente de Venta" llavePerfil="GTV" canalComunicacion="(SMS)" horaAtencionInicio="00:00" horaAtencionTermino="23:59"></contacto>
        <contacto tipoEnvio="Exclusivo" formato="Mayorista 10" llaveFormato="Mayorista10" perfil="Jefe AP" llavePerfil="JFAP" canalComunicacion="(SMS)" horaAtencionInicio="20:00" horaAtencionTermino="07:59"></contacto>
        <contacto tipoEnvio="Exclusivo" formato="Mayorista 10" llaveFormato="Mayorista10" perfil="Personalizado" llavePerfil="" canalComunicacion="(SMS)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="SubGerente Region" nombre="SubGerente de Prueba" telefono="12345678" email="prueba@prueba.cl" ></contacto>

        ** listado de contactos con copia
        <contacto tipoEnvio="ConCopia" formato="Unimarc" llaveFormato="Unimarc" perfil="Gerente de Mercado" ></contacto>
        <contacto tipoEnvio="ConCopia" formato="Alvi" llaveFormato="Alvi" perfil="Personalizado" llavePerfil="" canalComunicacion="(SMS)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Jefe Distribución" nombre="Nombre del Usuario" telefono="99887766" email="contacto@prueba.cl" ></contacto>

      </criterio>
      <criterio ocurrencias="3" condicion="Igual" canal="CallCenter" criticidad="Baja" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359">
        <contacto tipoEnvio="ConCopia" formato="Unimarc" llaveFormato="Unimarc" perfil="Personalizado" llavePerfil="" canalComunicacion="(Email)(SMS)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Desarrollador" nombre="Contacto Tres" telefono="7788994" email="prueba3@prueba.cl" ></contacto>
        <contacto tipoEnvio="ConCopia" formato="Unimarc" llaveFormato="Unimarc" perfil="Personalizado" llavePerfil="" canalComunicacion="(Email)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Jefe Proyecto" nombre="Contacto Cuatro" telefono="6655448" email="prueba4@prueba.cl" ></contacto>
      </criterio>
      <criterio ocurrencias="5" condicion="Igual" canal="Email" criticidad="Media" horarioAtencion="LU:0000-0059|SA:2045-2359|DO:0000-2359">
        <contacto tipoEnvio="ConCopia" formato="Unimarc" llaveFormato="Unimarc" perfil="Personalizado" llavePerfil="" canalComunicacion="(Email)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Desarrollador" nombre="Contacto Tres" telefono="7788994" email="prueba3@prueba.cl" ></contacto>
        <contacto tipoEnvio="ConCopia" formato="Unimarc" llaveFormato="Unimarc" perfil="Personalizado" llavePerfil="" canalComunicacion="(Email)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Jefe Proyecto" nombre="Contacto Cuatro" telefono="6655448" email="prueba4@prueba.cl" ></contacto>
        <contacto tipoEnvio="ConCopia" formato="Mayorista 10" llaveFormato="Mayorista10" perfil="Personalizado" llavePerfil="" canalComunicacion="(Email)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Gerente Comercial" nombre="Contacto Cinco" telefono="8855226" email="prueba5@prueba.cl" ></contacto>
      </criterio>
      <criterio ocurrencias="7" condicion="MayorIgual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-0059|SA:2045-2359|DO:0000-2359">
        <contacto tipoEnvio="ConCopia" formato="Alvi" llaveFormato="Alvi" perfil="Personalizado" llavePerfil="" canalComunicacion="(Email)(SMS)" horaAtencionInicio="00:00" horaAtencionTermino="23:59" cargo="Gerente Operaciones" nombre="Contacto Seis" telefono="4455669" email="prueba6@prueba.cl" ></contacto>
      </criterio>
    </alerta>

  </detencion>
-->

  <!--
  **
  * RUTA
  *
  -->
  <ruta>
    <alerta nombre="PERDIDA SEÑAL">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="PERDIDA SEÑAL ZONA GRIS">
  <!--
      [OCURRENCIA 1]
  -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="DETENCION NO AUTORIZADA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="DETENCION PATIO LINEA TRANSPORTE">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="DETENCION PROLONGADA ZONA AUTORIZADA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="DETENCION PROLONGADA ZONA NO AUTORIZADA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="EXCESO DE VELOCIDAD">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRETIEMPO EN RUTA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="DETENCION EN ZONA DE RIESGO">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

  </ruta>

  <!--
  **
  * TIENDAS
  *
  -->
  <tiendas>

    <alerta nombre="MOVIMIENTO FUERA DE HORARIO">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="MOVIMIENTO FUERA DE HORARIO">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN DESCARGA APP MOBILE">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRETIEMPO EN DESCARGA DROP">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRETIMEPO EN DESCARGA GEOCERCA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="LOCAL">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="CRUCE GEOCERCA PARA INGRESAR A TIENDA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

  </tiendas>

  <!--
  **
  * OPERACIONAL
  *
  -->
  <operacional>
    <alerta nombre="REMOLQUE QUE NO CORRESPONDE AL CEDIS">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS REMOLQUE LLENO 4H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS REMOLQUE LLENO 8H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS REMOLQUE LLENO 12H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS REMOLQUE VACIO 48H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS REMOLQUE VACIO 72H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS REMOLQUE VACIO 100H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS TRACTO 4H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS TRACTO 6H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA EN CEDIS TRACTO 8H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA CORTINA REMOLQUE FLOTA DEDICADA 4H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA CORTINA REMOLQUE FLOTA DEDICADA 6H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA CORTINA REMOLQUE FLOTA DEDICADA 24H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="SOBRESTADIA CORTINA REMOLQUE FLOTA DEDICADA 48H">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

    <alerta nombre="CAMION DESPACHADO SIN SALIR A RUTA">
      <!--
      [OCURRENCIA 1]
      -->
      <criterio ocurrencias="1" condicion="Igual" canal="CallCenter" criticidad="Alta" horarioAtencion="LU:0000-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2359|DO:0000-2359"></criterio>
    </alerta>

  </operacional>


</reglas>
