﻿Imports CapaNegocio
Imports System.Data
Imports System.Text
Imports System.Configuration
Imports Syncfusion.XlsIO

Module inicio

#Region "Configuracion Job"
  Const NOMBRE_JOB As String = "jobTransportePochtecaAlertasEnColaAtencion"
  Const KEY As String = "AlertasEnColaAtencion"
#End Region

#Region "Enum"
  Enum eTablaConfiguracion As Integer
    EmailPara = 0
    EmailCC = 1
    DiasEjecucion = 2
    AsuntoEmailAlertaEnColaAtencion = 3
    CuerpoEmailAlertaEnColaAtencion = 4
    EmailParaAlertaPorDesactivar = 5
    EmailCCAlertaPorDesactivar = 6
    AsuntoEmailAlertaPorDesactivar = 7
    CuerpoEmailAlertaPorDesactivar = 8
  End Enum

  Enum eTablaDatos As Integer
    T00_AlertasEnColaAtencion = 0
    T01_AlertasPorDesactivar = 1
    T03_ListadoContactos = 2
  End Enum

#End Region

#Region "Privado"
  ''' <summary>
  ''' determina si se puede ejecutar la consola en el dia actual
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EjecutarDiaMes() As Boolean
    Dim sePuedeEjecutar As Boolean = False
    Dim dsConfiguracionXML As New DataSet
    Dim diasEjecucion, incluirFinMes As String
    Dim diaMesActual As String = Herramientas.MyNow().Day
    Dim ultimoDiaMes As Date

    Try
      dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")
      diasEjecucion = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("diasEjecucion")
      incluirFinMes = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("incluirFinMes")

      If (InStr(diasEjecucion, "{-1}") > 0) Or (InStr(diasEjecucion, "{" & diaMesActual & "}") > 0) Then
        sePuedeEjecutar = True
      Else
        If incluirFinMes.Trim.ToUpper = "TRUE" Then
          ultimoDiaMes = Herramientas.ObtenerUltimoDiaMes(Herramientas.MyNow())
          If Day(ultimoDiaMes) = diaMesActual Then
            sePuedeEjecutar = True
          End If
        End If
      End If
    Catch ex As Exception
      sePuedeEjecutar = False
    End Try
    Return sePuedeEjecutar
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GenerarExcelAlertasEnCola(ByVal ds As DataSet, ByVal nombreArchivo As String, ByVal dsConfiguracion As DataSet) As Boolean
    Dim fueGenerado As Boolean = False
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim workbook As IWorkbook

    Try
      application.UseNativeStorage = False

      Dim nombreHoja As String() = {"ALERTA SIN GESTION"}
      workbook = application.Workbooks.Create(nombreHoja)

      Dim sheet0 As IWorksheet = workbook.Worksheets(nombreHoja(0))
      GenerarHojaExcel(workbook, sheet0, ds.Tables(eTablaDatos.T00_AlertasEnColaAtencion))

      workbook.SaveAs(Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls")
      workbook.Close()
      fueGenerado = True

      Return fueGenerado
    Catch ex As Exception
      Throw New Exception(ex.Message)
    Finally
      excelEngine.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GenerarExcelAlertasPorDesactivar(ByVal dt As DataTable, ByVal nombreArchivo As String, ByVal dsConfiguracion As DataSet) As Boolean
    Dim fueGenerado As Boolean = False
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim workbook As IWorkbook

    Try
      application.UseNativeStorage = False

      Dim nombreHoja As String() = {"ALERTAS DESACTIVADAS"}
      workbook = application.Workbooks.Create(nombreHoja)

      Dim sheet0 As IWorksheet = workbook.Worksheets(nombreHoja(0))
      GenerarHojaExcel(workbook, sheet0, dt)

      workbook.SaveAs(Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls")
      workbook.Close()
      fueGenerado = True

      Return fueGenerado
    Catch ex As Exception
      Throw New Exception(ex.Message)
    Finally
      excelEngine.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GenerarHojaExcel(ByRef workbook As IWorkbook, ByRef sheet As IWorksheet, ByVal dt As DataTable)
    Dim fueGenerado As Boolean = False

    Try
      sheet.ImportDataTable(dt, True, 1, 1)
      With sheet.Range(1, 1, 1, dt.Columns.Count).CellStyle
        .Font.Bold = True
      End With

      sheet.UsedRange.AutofitColumns()
      sheet.IsGridLinesVisible = True
      fueGenerado = True
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' envia excel con las alertas que estan en cola de atencion
  ''' </summary>
  ''' <remarks></remarks>
  Private Function EnviarExcelAlertasEnColaAtencion(ByVal dsConfiguracionXML As DataSet, ByVal ds As DataSet) As String
    Dim msgRetorno, nombreArchivo, bodyMensaje, body, listadoMailCC, contenidoLog, asunto, msgProceso As String
    Dim totalRegistros As Integer
    Dim fueGenerado As Boolean
    Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
    Dim contadorErrores As Integer = 0
    Dim listadoMailPara As String = ""

    Try
      'crea el archivo en disco
      nombreArchivo = Archivo.CrearNombreUnicoArchivo(KEY)
      totalRegistros = ds.Tables(eTablaDatos.T00_AlertasEnColaAtencion).Rows.Count

      If (totalRegistros = 0) Then
        msgRetorno = ""
      Else
        fueGenerado = GenerarExcelAlertasEnCola(ds, nombreArchivo, dsConfiguracionXML)

        If fueGenerado Then
          'construye cuerpo del mail
          bodyMensaje = dsConfiguracionXML.Tables(eTablaConfiguracion.CuerpoEmailAlertaEnColaAtencion).Rows(0).Item("texto")
          bodyMensaje = bodyMensaje.Replace("{BR}", vbCrLf)
          'constuye contenido del excel y del log
          body = Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, eTablaDatos.T00_AlertasEnColaAtencion)

          'construye listado de email de destinos
          If Herramientas.MailModoDebug Then
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas())
            ColeccionEmailsCC = Nothing
          Else
            'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
            listadoMailPara = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailPara, "email")
            listadoMailCC = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailCC, "email")
            listadoMailCC &= IIf(String.IsNullOrEmpty(listadoMailCC), Herramientas.MailAdministrador(), ";" & Herramientas.MailAdministrador())
            'construye las colecciones
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
            ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailCC)
          End If
          'crea contenido para crear el log
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(body & "<br/><div>Enviado a: " & listadoMailPara & "</div>", NOMBRE_JOB)
          Dim path As String = Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls"
          'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
          asunto = dsConfiguracionXML.Tables(eTablaConfiguracion.AsuntoEmailAlertaEnColaAtencion).Rows(0).Item("texto")

          msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, False, ColeccionEmailsCC, True, path)
          If msgProceso <> "" Then
            contadorErrores = contadorErrores + 1
            contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & msgProceso, NOMBRE_JOB)
          End If
          'limpia variable para los nuevos email
          ColeccionEmailsPara = Nothing
          ColeccionEmailsCC = Nothing
          msgRetorno = "Se envían las alerta en cola de atención<br />"
        Else
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay alertas por informar", NOMBRE_JOB)
          msgRetorno = "No se pudo generar excel con las alertas en cola de atención<br />"
        End If

        'crea log del proceso
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
        nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
      End If
    Catch ex As Exception
      msgRetorno = ex.Message & "<br />"
    End Try

    Return msgRetorno
  End Function

  ''' <summary>
  ''' envia sms con las alertas que se van a desactivar
  ''' </summary>
  ''' <remarks></remarks>
  Private Function EnviarSMSAlertasPorDesactivar(ByVal dsConfiguracionXML As DataSet, ByVal ds As DataSet) As String
    Dim msgRetorno, localDestino, mensaje, mensajeAux, nroTransporte, nombreLocal, nombreAlerta, enviadoA, telefono, cargo As String
    Dim smsFolio, smsCodigo, smsMensaje As String
    Dim totalRegistros As Integer
    Dim arrayGroupBy As New ArrayList
    Dim dt, dtContactosLocal As DataTable
    Dim dr, drContacto As DataRow
    Dim sl As New SortedList
    Dim HORA_ATENCION_INICIO As String = "08:00"
    Dim HORA_ATENCION_TERMINO As String = "21:00"

    Try
      mensaje = "ALTOTRACK: Le recordamos realizar doble control en IdMaster {NRO_TRANSPORTE}, Tienda {LOCAL_DESTINO_CODIGO}, Alerta {NOMBRE_ALERTA}"
      dt = ds.Tables(eTablaDatos.T01_AlertasPorDesactivar)
      dtContactosLocal = ds.Tables(eTablaDatos.T03_ListadoContactos)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        msgRetorno = ""
      Else
        'agrupa los grupos padres por cada escalamiento
        arrayGroupBy = Herramientas.ObtenerArrayGroupByDeDataTable(dt, "DETERMINANTE")

        For Each localDestino In arrayGroupBy
          '------------------------------------------------
          'envia SMS por cada alerta
          Dim filtroListadoAlertasEnCola = From row As DataRow In dt.Rows Where
                                           row.Item("DETERMINANTE") = localDestino

          For Each dr In filtroListadoAlertasEnCola
            mensajeAux = mensaje
            nroTransporte = dr.Item("IDMASTER")
            nombreLocal = dr.Item("LOCAL")
            nombreAlerta = dr.Item("NOMBRE ALERTA")

            'reemplaza marcas
            mensajeAux = mensajeAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            mensajeAux = mensajeAux.Replace("{LOCAL_DESTINO_CODIGO}", nombreLocal & "-" & localDestino)
            mensajeAux = mensajeAux.Replace("{NOMBRE_ALERTA}", nombreAlerta)

            Dim filtroContactosLocal = From row As DataRow In dtContactosLocal.Rows Where
                                       row.Item("LocalDestino") = localDestino

            For Each drContacto In filtroContactosLocal
              '------------------------
              'ENCARGADO 1
              enviadoA = drContacto.Item("NombreEncargado1")
              telefono = drContacto.Item("TelefonoEncargado1")
              cargo = drContacto.Item("CargoEncargado1")

              If Herramientas.MailModoDebug Then
                smsFolio = ""
                smsCodigo = ""
                smsMensaje = "Sistema en modo debug"
              Else
                If (Herramientas.EstaDentroHorarioAtencion(HORA_ATENCION_INICIO, HORA_ATENCION_TERMINO)) Then
                  sl = Herramientas.EnviarSMS(telefono, mensajeAux)
                  smsFolio = sl.Item("Folio")
                  smsCodigo = sl.Item("Codigo")
                  smsMensaje = sl.Item("Mensaje")
                Else
                  smsFolio = ""
                  smsCodigo = ""
                  smsMensaje = "SMS desactivado horario nocturno"
                End If
              End If
              Alerta.GrabarHistorialAlertaSMS(nombreAlerta, nroTransporte, localDestino, telefono, enviadoA, cargo, "", "1", smsFolio.Trim(), smsCodigo.Trim(), smsMensaje.Trim(), "CONSOLA " & NOMBRE_JOB)

              '------------------------
              'ENCARGADO 2
              enviadoA = drContacto.Item("NombreEncargado2")
              telefono = drContacto.Item("TelefonoEncargado2")
              cargo = drContacto.Item("CargoEncargado2")

              If Herramientas.MailModoDebug Then
                smsFolio = ""
                smsCodigo = ""
                smsMensaje = "Sistema en modo debug"
              Else
                If (Herramientas.EstaDentroHorarioAtencion(HORA_ATENCION_INICIO, HORA_ATENCION_TERMINO)) Then
                  sl = Herramientas.EnviarSMS(telefono, mensajeAux)
                  smsFolio = sl.Item("Folio")
                  smsCodigo = sl.Item("Codigo")
                  smsMensaje = sl.Item("Mensaje")
                Else
                  smsFolio = ""
                  smsCodigo = ""
                  smsMensaje = "SMS desactivado horario nocturno"
                End If
              End If
              Alerta.GrabarHistorialAlertaSMS(nombreAlerta, nroTransporte, localDestino, telefono, enviadoA, cargo, "", "1", smsFolio.Trim(), smsCodigo.Trim(), smsMensaje.Trim(), "CONSOLA " & NOMBRE_JOB)
            Next
          Next
        Next

        msgRetorno = "Se envían sms con las alerta que se van a desactivar<br />"
      End If
    Catch ex As Exception
      msgRetorno = ex.Message & "<br />"
    End Try

    Return msgRetorno
  End Function

  ''' <summary>
  ''' envia email con las alertas que se van a desactivar
  ''' </summary>
  ''' <remarks></remarks>
  Private Function EnviarEmailAlertasPorDesactivar(ByVal dsConfiguracionXML As DataSet, ByVal ds As DataSet) As String
    Dim msgRetorno, localDestino, email, listadoMailPara, listadoMailCC, nombreArchivo, bodyMensaje, body, contenidoLog, asunto As String
    Dim msgProceso As String
    Dim totalRegistros, contadorErrores As Integer
    Dim arrayGroupBy As New ArrayList
    Dim dt, dtContactosLocal As DataTable
    Dim drContacto As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim listContactos As New List(Of String)
    Dim fueGenerado As Boolean
    Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection

    Try
      dt = ds.Tables(eTablaDatos.T01_AlertasPorDesactivar)
      dtContactosLocal = ds.Tables(eTablaDatos.T03_ListadoContactos)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        msgRetorno = ""
      Else
        'agrupa los grupos padres por cada escalamiento
        arrayGroupBy = Herramientas.ObtenerArrayGroupByDeDataTable(dt, "DETERMINANTE")

        For Each localDestino In arrayGroupBy
          listContactos = New List(Of String)
          contadorErrores = 0

          '------------------------------------------------
          'obtiene email de los contactos del local
          Dim filtroContactosLocal = From row As DataRow In dtContactosLocal.Rows Where
                                     row.Item("LocalDestino") = localDestino

          For Each drContacto In filtroContactosLocal
            '------------------------
            'ENCARGADO 1
            email = drContacto.Item("EmailEncargado1")
            listContactos.Add(email)

            '------------------------
            'ENCARGADO 2
            email = drContacto.Item("EmailEncargado2")
            listContactos.Add(email)
          Next
          listadoMailPara = String.Join(";", listContactos.ToArray())
          '------------------------------------------------


          '------------------------------------------------
          'envia email con las alertas
          dv = dt.DefaultView
          dv.RowFilter = "[DETERMINANTE] = " & localDestino
          dsFiltrado = Herramientas.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          'crea el archivo en disco
          nombreArchivo = Archivo.CrearNombreUnicoArchivo("AlertasDesactivadas_Local" & localDestino)
          totalRegistros = dsFiltrado.Tables(0).Rows.Count

          If (totalRegistros = 0) Then
            msgRetorno = ""
          Else
            fueGenerado = GenerarExcelAlertasPorDesactivar(dsFiltrado.Tables(0), nombreArchivo, dsConfiguracionXML)

            If fueGenerado Then
              'construye cuerpo del mail
              bodyMensaje = dsConfiguracionXML.Tables(eTablaConfiguracion.CuerpoEmailAlertaPorDesactivar).Rows(0).Item("texto")
              bodyMensaje = bodyMensaje.Replace("{BR}", vbCrLf)
              'constuye contenido del excel y del log
              body = Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, eTablaDatos.T00_AlertasEnColaAtencion)

              'construye listado de email de destinos
              If Herramientas.MailModoDebug Then
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas())
                ColeccionEmailsCC = Nothing
              Else
                'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
                listadoMailPara &= IIf(String.IsNullOrEmpty(listadoMailPara), Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailParaAlertaPorDesactivar, "email"), ";" & Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailParaAlertaPorDesactivar, "email"))
                listadoMailCC = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailCCAlertaPorDesactivar, "email")
                listadoMailCC &= IIf(String.IsNullOrEmpty(listadoMailCC), Herramientas.MailAdministrador(), ";" & Herramientas.MailAdministrador())
                'construye las colecciones
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
                ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailCC)
              End If
              'crea contenido para crear el log
              contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(body & "<br/><div>Enviado a: " & listadoMailPara & "</div>", NOMBRE_JOB)
              Dim path As String = Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls"
              'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
              asunto = dsConfiguracionXML.Tables(eTablaConfiguracion.AsuntoEmailAlertaPorDesactivar).Rows(0).Item("texto")

              msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, False, ColeccionEmailsCC, True, path)
              If msgProceso <> "" Then
                contadorErrores = contadorErrores + 1
                contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & msgProceso, NOMBRE_JOB)
              End If
              'limpia variable para los nuevos email
              ColeccionEmailsPara = Nothing
              ColeccionEmailsCC = Nothing
              msgRetorno = "Se envían las alerta en cola de atención<br />"
            Else
              contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay alertas por informar", NOMBRE_JOB)
              msgRetorno = "No se pudo generar excel con las alertas en cola de atención<br />"
            End If

            'crea log del proceso
            Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
            nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo & "_Local" & localDestino)
            Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
          End If
        Next

        msgRetorno = "Se envían email con las alerta que se van a desactivar<br />"
      End If
    Catch ex As Exception
      msgRetorno = ex.Message & "<br />"
    End Try

    Return msgRetorno
  End Function

  ''' <summary>
  ''' desactiva las alertas en base de datos y las elimina de la cola de atencion
  ''' </summary>
  ''' <remarks></remarks>
  Private Function DesactivarAlertas(ByVal dsConfiguracionXML As DataSet, ByVal ds As DataSet) As String
    Dim msgRetorno, idAlerta, nombreAlerta, nroTransporte, idEmbarque, localDestino As String
    Dim totalRegistros, status As Integer
    Dim dt As DataTable

    Try
      dt = ds.Tables(eTablaDatos.T01_AlertasPorDesactivar)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        msgRetorno = ""
      Else
        For Each dr As DataRow In dt.Rows
          idAlerta = dr.Item("IDALERTA")
          nombreAlerta = dr.Item("NOMBRE ALERTA")
          nroTransporte = dr.Item("ID MASTER")
          idEmbarque = dr.Item("ID EMBARQUE")
          localDestino = dr.Item("DETERMINANTE")

          status = Alerta.DesactivarAlertaEnColaAtencionLocalesRMyOtros(idAlerta, nombreAlerta, nroTransporte, idEmbarque, localDestino)
        Next

        msgRetorno = "Se desactivan las alertas que están en cola de atención<br />"
      End If
    Catch ex As Exception
      msgRetorno = ex.Message & "<br />"
    End Try

    Return msgRetorno
  End Function

#End Region

  Sub Main()
    Dim msgProceso As String = ""
    'construye fecha inicio
    Dim ds As New DataSet
    Dim dsConfiguracionXML As New DataSet
    Dim nombreArchivo, fechaIniISO, fechaFinISO As String
    Dim contadorErrores As Integer = 0
    Dim contenidoLog As String = ""

    'inicia la ejecucion
    Console.WriteLine("Iniciando tarea: " & NOMBRE_JOB)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug.ToString)

    Try
      If EjecutarDiaMes() Then
        'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
        dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")

        'construye las fechas para el filtro
        fechaIniISO = "-1"
        fechaFinISO = "-1"

        'obtiene listado de planifcaciones
        ds = Sistema.ConsultaGenerica(fechaIniISO, fechaFinISO, "spu_Job_AlertasEnColaAtencion")

        'contenidoLog &= EnviarExcelAlertasEnColaAtencion(dsConfiguracionXML, ds)
        'contenidoLog &= EnviarSMSAlertasPorDesactivar(dsConfiguracionXML, ds)
        'contenidoLog &= EnviarEmailAlertasPorDesactivar(dsConfiguracionXML, ds)
        contenidoLog &= DesactivarAlertas(dsConfiguracionXML, ds)
      Else
        contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Tarea no realizada porque no estaba programada para ejecutarse el día de hoy", NOMBRE_JOB)
      End If
    Catch ex As Exception
      contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & ex.Message, NOMBRE_JOB)
      contadorErrores = 1
    End Try
    Console.WriteLine("Fin tarea: " & NOMBRE_JOB)

    'crea log del proceso
    If (contenidoLog <> "") Then
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
      nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
    End If
  End Sub

End Module
