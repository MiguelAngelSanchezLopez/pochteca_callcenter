﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Regla_ObtieneListadoContactosPorPerfil')
BEGIN
  PRINT  'Dropping Procedure spu_Regla_ObtieneListadoContactosPorPerfil'
  DROP  Procedure  dbo.spu_Regla_ObtieneListadoContactosPorPerfil
END

GO

PRINT  'Creating Procedure spu_Regla_ObtieneListadoContactosPorPerfil'
GO
CREATE Procedure dbo.spu_Regla_ObtieneListadoContactosPorPerfil
/******************************************************************************
**    Descripcion  : obtiene los contactos asociados al perfil
**    Por          : VSR, 09/07/2015
*******************************************************************************/
@LlavePerfil AS VARCHAR(255),
@RutTransportista AS VARCHAR(255),
@RutConductor AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------
  -- obtiene contactos relacionados al perfil
  --------------------------------------------------------
  SELECT
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') +  + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL(U.Telefono,''),
    Email = ISNULL(U.Email,''),
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,'')
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      U.Estado = 1
  AND (
           -- si el perfil es CONDUCTOR, busca los datos por el rut
           ( (@LlavePerfil = 'CON') AND (P.Llave = @LlavePerfil AND RTRIM(LTRIM(REPLACE(REPLACE(@RutConductor,' ',''),'.',''))) = U.Rut + '-' + U.DV) )
           -- si el perfil es TRANSPORTISTA, busca los datos por el rut
        OR ( (@LlavePerfil = 'TRA') AND (P.Llave = @LlavePerfil AND RTRIM(LTRIM(REPLACE(REPLACE(@RutTransportista,' ',''),'.',''))) = U.Rut + '-' + U.DV) )
           -- si es otro perfil los trae todos
        OR ( (@LlavePerfil <> 'CON') AND (@LlavePerfil <> 'TRA') AND (P.Llave = @LlavePerfil) )
      )

END



  