﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Archivo_AsociarIdArchivoConIdRegistro')
BEGIN
  PRINT  'Dropping Procedure spu_Archivo_AsociarIdArchivoConIdRegistro'
  DROP  Procedure  dbo.spu_Archivo_AsociarIdArchivoConIdRegistro
END

GO

PRINT  'Creating Procedure spu_Archivo_AsociarIdArchivoConIdRegistro'
GO
CREATE Procedure dbo.spu_Archivo_AsociarIdArchivoConIdRegistro
/******************************************************************************
**    Descripcion  : asocia el id del registro con los archivos relacionados con el
**    Autor        : VSR
**    Fecha        : 15/14/2014
*******************************************************************************/
@Status INT OUTPUT,
@Entidad VARCHAR(128),
@IdRegistro INT,
@IdArchivo INT,
@FechaVinculo VARCHAR(8),
@HoraVinculo VARCHAR(4)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @FechaActual AS VARCHAR(8)
    DECLARE @HoraActual AS VARCHAR(5)
    DECLARE @newHoraActual AS VARCHAR(4)

    SET @FechaActual = CONVERT(VARCHAR(8),dbo.fnu_GETDATE(),112)
    SET @HoraActual = REPLACE(CONVERT(VARCHAR(5),dbo.fnu_GETDATE(),108),':','')

    -- si no se ingreso una fecha entonces coloca la actual
    IF(@FechaVinculo='-1') SET @FechaVinculo = @FechaActual
    IF(@HoraVinculo='-1')  SET @HoraVinculo = @HoraActual

    -- si existe el registro entonces no hace nada, sino lo guarda
    IF(EXISTS(SELECT IdRegistro FROM ArchivosXRegistro WHERE IdRegistro=@IdRegistro AND IdArchivo=@IdArchivo AND RTRIM(LTRIM(Entidad))=RTRIM(LTRIM(@Entidad)) )) BEGIN
      SET @Status = 1
    END ELSE BEGIN
      -- inserta registro
      INSERT INTO ArchivosXRegistro(
        Entidad,
        IdRegistro,
        IdArchivo,
        FechaVinculo,
        HoraVinculo
      )
      VALUES(
        @Entidad,
        @IdRegistro,
        @IdArchivo,
        @FechaVinculo,
        @HoraVinculo
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status=-200
        RETURN
      END

      SET @Status = SCOPE_IDENTITY()

    END

  COMMIT
END
GO