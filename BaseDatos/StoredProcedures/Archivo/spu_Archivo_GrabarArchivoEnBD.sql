﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Archivo_GrabarArchivoEnBD')
BEGIN
  PRINT  'Dropping Procedure spu_Archivo_GrabarArchivoEnBD'
  DROP  Procedure  dbo.spu_Archivo_GrabarArchivoEnBD
END

GO

PRINT  'Creating Procedure spu_Archivo_GrabarArchivoEnBD'
GO
CREATE Procedure dbo.spu_Archivo_GrabarArchivoEnBD
/******************************************************************************
**    Descripcion  : graba un archivo en base de datos
**    Autor        : VSR
**    Fecha        : 15/12/2008
*******************************************************************************/
@Status AS INT OUTPUT,
@IdArchivo INT,
@NombreArchivo VARCHAR(128),
@Ruta VARCHAR(1024),
@Tamano INT,
@Extension VARCHAR(20),
@ContentType VARCHAR(512),
@Contenido IMAGE,
@Orden INT,
@IdUsuario INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @FechaActual AS VARCHAR(8)
    DECLARE @HoraActual AS VARCHAR(5)
    DECLARE @newHoraActual AS VARCHAR(4)

    SET @FechaActual = CONVERT(VARCHAR(8),dbo.fnu_GETDATE(),112)
    SET @HoraActual = REPLACE(CONVERT(VARCHAR(5),dbo.fnu_GETDATE(),108),':','')

    IF (@IdArchivo = -1) BEGIN
          INSERT INTO Archivo(
            Nombre,
            Ruta,
            Tamano,
            Extension,
            ContentType,
            Contenido,
            Orden,
            FechaCreacion,
            HoraCreacion,
            IdUsuario
          )
          VALUES(
            @NombreArchivo,
            @Ruta,
            @Tamano,
            @Extension,
            @ContentType,
            @Contenido,
            @Orden,
            @FechaActual,
            @HoraActual,
            @IdUsuario
          )

          IF @@ERROR<>0 BEGIN
            ROLLBACK
            SET @Status=-200
            RETURN
          END

          -- asigna valor
          SET @Status = SCOPE_IDENTITY()
    END ELSE BEGIN
        UPDATE Archivo
        SET
	        Nombre = @NombreArchivo,
	        Ruta = @Ruta,
	        Tamano = @Tamano,
	        Extension = @Extension,
	        ContentType = @ContentType,
	        Contenido = @Contenido
        WHERE
          Id = @IdArchivo

        IF @@ERROR<>0 BEGIN
          ROLLBACK
          SET @Status=-200
          RETURN
        END

        -- asigna valor
        SET @Status = @IdArchivo

    END


  COMMIT

END
GO