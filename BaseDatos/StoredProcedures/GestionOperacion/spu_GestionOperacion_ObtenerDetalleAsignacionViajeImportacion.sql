﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerDetalleAsignacionViajeImportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerDetalleAsignacionViajeImportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerDetalleAsignacionViajeImportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerDetalleAsignacionViajeImportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerDetalleAsignacionViajeImportacion
/******************************************************************************
**    Descripcion  : obtiene detalle asignacion viaje importacion
**    Por          : VSR, 18/04/2016
******************************************************************************/
@IdImportacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------------------
  -- TABLA 0: Detalle del viaje importacion
  --------------------------------------------------------------------
  SELECT
    IdImportacion = I.Id,
	  NumeroOrdenServicio = I.NumeroOrdenServicio,
	  FechaHoraPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(I.FechaHoraPresentacion, 'FECHA_COMPLETA'),
    NombreCliente = ISNULL(I.NombreCliente,''),
    --DescripcionTipoCarga = ISNULL(I.DescripcionTipoCarga,''),
    MedidaContenedor = I.MedidaContenedor,
    DirectoIndirecto = ISNULL(I.DirectoIndirecto,''),
    NombrePuertoDescarga = ISNULL(I.NombrePuertoDescarga,''),
    DiasDemurrage = I.DiasDemurrage,
    NombreNave = ISNULL(I.NombreNave,''),
    NumeroContenedor = ISNULL(I.MarcaContenedor,'') + ISNULL(I.NumeroContenedor,'') + ISNULL(I.DigitoContenedor,''),
    DireccionEntregaContenedor = ISNULL(I.DireccionEntregaContenedor,''),
    NombreConductor = ISNULL(IAC.NombreConductor,''),
    PatenteTracto = ISNULL(IAC.PatenteTracto,''),
    PatenteTrailer = ISNULL(IAC.PatenteTrailer,''),
    TelefonoConductor = ISNULL(IAC.Telefono,''),
    EstadoViaje = CASE ISNULL(IAC.EstadoViaje,'')
                    WHEN 'ENLOCAL-R' THEN 'EN TIENDA'
                    WHEN 'ENLOCAL-P' THEN 'FINALIZADO'
                    ELSE UPPER(ISNULL(IAC.EstadoViaje,''))
                  END
  FROM
	  OS_Importacion AS I
    LEFT JOIN OS_Importacion_AsignacionConductor AS IAC ON (I.Id = IAC.IdImportacion)
  WHERE
    Id = @IdImportacion


  --------------------------------------------------------------------
  -- TABLA 1: Listado conductores de SCAT
  --------------------------------------------------------------------
  -- obtiene el conductor asociado
  SELECT
	  IdEntrada = -1,
	  NombreConductor,
	  RutConductor,
	  PatenteTracto,
	  PatenteTrailer,
	  TipoCamion,
	  Telefono,
	  FechaEntrada = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaEntrada,'FECHA_COMPLETA'),
    IdEntradaSalida = -1,
    FechaEntradaOrden = FechaEntrada,
    OrigenDatoTabla = ISNULL(OrigenDatoTabla, ''),
    OrigenDatoIdRegistro = ISNULL(OrigenDatoIdRegistro, '-1'),
    FechaLicenciaConducir = ISNULL(T.FechaLicConducir,''),
    FechaRevisionTecnica = ISNULL(T.FechaRevTecnica,''),
    ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
    ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
    ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
    ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,''),
    Indice = 0
  FROM
	  OS_Importacion_AsignacionConductor
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (PatenteTracto = TLPTracto.Patente)
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (PatenteTrailer = TLPTrailer.Patente)
    OUTER APPLY(
      SELECT
        UDE.FechaLicConducir,
        UDE.FechaRevTecnica
      FROM
        Usuario AS U
        INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
        INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
      WHERE
          ISNULL(U.Rut,'') + ISNULL('-' + U.Dv,'') = UPPER(ISNULL(RutConductor,''))
      AND P.Llave = 'CON'
    ) AS T
  WHERE
    IdImportacion = @IdImportacion
  
  ------------------------------------------
  -- obtiene los conductor de SCAT
  UNION
  SELECT
    T.*,
    Indice = ROW_NUMBER() OVER( ORDER BY T.FechaEntradaOrden )
  FROM (
        SELECT
          IdEntrada = CAVEM.Id,
          NombreConductor = UPPER(ISNULL(CAVEM.NombreConductor,'')),
          RutConductor = UPPER(ISNULL(CAVEM.RutConductor,'')),
          PatenteTracto = UPPER(ISNULL(CAVEM.PatenteTracto,'')),
          PatenteTrailer = UPPER(ISNULL(CAVEM.PatenteTrailer,'')),
          TipoCamion = ISNULL(CAVEM.TipoCamion,''),
          Telefono = ISNULL(CAVEM.Celular,''),
          FechaEntrada = dbo.fnu_ConvertirDatetimeToDDMMYYYY(CAVEM.FechaEntrada,'FECHA_COMPLETA'),
          IdEntradaSalida = ISNULL(CAVSM.IdEntrada,-1),
          FechaEntradaOrden = CAVEM.FechaEntrada,
          OrigenDatoTabla = 'ControlAccesoValidacionEntradaManual',
          OrigenDatoIdRegistro = CAVEM.Id,
          FechaLicenciaConducir = ISNULL(T.FechaLicConducir,''),
          FechaRevisionTecnica = ISNULL(T.FechaRevTecnica,''),
          ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
          ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
          ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
          ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,'')
        FROM 
          ControlAccesoValidacionEntradaManual AS CAVEM
          LEFT JOIN ControlAccesoValidacionSalidaManual AS CAVSM ON (CAVEM.Id = CAVSM.IdEntrada)
          LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (CAVEM.PatenteTracto = TLPTracto.Patente)
          LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (CAVEM.PatenteTrailer = TLPTrailer.Patente)
          OUTER APPLY(
            SELECT
              UDE.FechaLicConducir,
              UDE.FechaRevTecnica
            FROM
              Usuario AS U
              INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
              INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
            WHERE
                ISNULL(U.Rut,'') + ISNULL('-' + U.Dv,'') = UPPER(ISNULL(CAVEM.RutConductor,''))
            AND P.Llave = 'CON'
          ) AS T
        WHERE 
            CAVEM.TipoPersona = 'Transportista'
        AND DATEDIFF(dd, CAVEM.FechaEntrada, dbo.fnu_GETDATE()) <= 1
        AND CAVEM.RutConductor NOT IN (
          ------------------------------------------
        	-- saca el conductor asignado al viaje
          SELECT RutConductor FROM OS_Importacion_AsignacionConductor WHERE IdImportacion = @IdImportacion
          
          ------------------------------------------
          -- saca los conductores que estan asignado en otros viajes o que esten en ruta en importacion
          /*
          UNION ALL
          SELECT
            IAC.RutConductor
          FROM
            OS_Importacion AS I
            INNER JOIN OS_Importacion_AsignacionConductor AS IAC ON (I.Id = IAC.IdImportacion)
          WHERE
            ( ISNULL(IAC.EstadoViaje,'') IN ('','ASIGNADO','RUTA') )

          ------------------------------------------
          -- saca los conductores que estan asignado en otros viajes o que esten en ruta en exportacion
          UNION ALL
          SELECT
            EAC.RutConductor
          FROM
            OS_Exportacion AS E
            INNER JOIN OS_Exportacion_AsignacionConductor AS EAC ON (E.Id = EAC.IdExportacion)
          WHERE
            ( ISNULL(EAC.EstadoViaje,'') IN ('','ASIGNADO','RUTA') )
          */
        )
  ) AS T
  WHERE
    IdEntradaSalida = -1
  ORDER BY
    Indice

END
	    