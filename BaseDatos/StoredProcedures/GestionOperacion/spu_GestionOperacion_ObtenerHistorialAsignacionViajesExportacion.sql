﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerHistorialAsignacionViajesExportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerHistorialAsignacionViajesExportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerHistorialAsignacionViajesExportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerHistorialAsignacionViajesExportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerHistorialAsignacionViajesExportacion
/******************************************************************************
**    Descripcion  : obtiene historial asignación viajes exportación
**    Por          : VSR, 16/06/2016
*******************************************************************************/
@NumeroOrdenServicio AS INT,
@NumeroLineaMO AS INT,
@VersionMO AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdExportacion = E.Id,
	  E.NumeroOrdenServicio,
    E.VersionMO,
    E.NumeroLineaMO,
    EstadoLinea = ISNULL(E.EstadoLinea,''),
    NombreCliente = ISNULL(E.NombreCliente,''),
    LugarPresentacion = CASE WHEN OROrigen.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                        ELSE OROrigen.DescripcionDestino + CASE WHEN OROrigen.CodDestino = '' THEN '' ELSE ' - ' + OROrigen.CodDestino END
                        END,
    SegundoDestino = CASE WHEN ORDestino.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                     ELSE ORDestino.DescripcionDestino + CASE WHEN ORDestino.CodDestino = '' THEN '' ELSE ' - ' + ORDestino.CodDestino END
                     END,
	  FechaHoraPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(E.FechaHoraStacking, 'FECHA_COMPLETA'),
	  MedidaContenedor = E.MedidaContenedor,
    --DescripcionTipoCarga = ISNULL(E.DescripcionTipoCarga,''),
    E.PesoNetoCarga,
    NumeroContenedor = ISNULL(E.MarcaContenedor,'') + ISNULL(E.NumeroContenedor,'') + ISNULL(E.DigitoContenedor,''),
    DepositoRetiro = 'FALTA DATO',
    Reserva = ISNULL(E.Reserva,''),
    NombreNave = ISNULL(E.NombreNave,''),
    FechaHoraProgramacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(E.FechaCreacion, 'FECHA_COMPLETA'),
    Observaciones = ISNULL(E.Observaciones,'')
  FROM
	  OS_ExportacionHistoria AS E
    OUTER APPLY (
      SELECT TOP 1
        DescripcionOrigen = ISNULL(aux.DescripcionOrigen,''),
        DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
        CodDestino = ISNULL(aux.CodDestino,'')
      FROM
        OS_Rutas AS aux
      WHERE
          aux.NumeroOrdenServicio = E.NumeroOrdenServicio
      AND aux.NumeroLineaMO = E.NumeroLineaMO
      AND aux.VersionMO = E.VersionMO
    ) AS OROrigen
    OUTER APPLY (
      SELECT TOP 1
        DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
        CodDestino = ISNULL(aux.CodDestino,'')
      FROM
        OS_Rutas AS aux
      WHERE
          aux.NumeroOrdenServicio = E.NumeroOrdenServicio
      AND aux.NumeroLineaMO = E.NumeroLineaMO
      AND aux.VersionMO = E.VersionMO
      ORDER BY
        aux.Id DESC
    ) AS ORDestino
  WHERE
      (E.NumeroOrdenServicio = @NumeroOrdenServicio)
  AND (E.NumeroLineaMO = @NumeroLineaMO)
  AND (E.VersionMO <> @VersionMO)
  ORDER BY
    E.VersionMO, E.NumeroLineaMO

END
	    
