﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_ObtenerPorId'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_PlanificacionTransportista_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    PlanificacionTransportista
  WHERE   
    Id = @Id  
  
END  