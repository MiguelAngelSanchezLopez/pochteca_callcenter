﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_GrabarHistorialReasignacionCamion')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_GrabarHistorialReasignacionCamion'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_GrabarHistorialReasignacionCamion
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_GrabarHistorialReasignacionCamion'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_GrabarHistorialReasignacionCamion
/******************************************************************************
**    Descripcion  : graba el historial de campos modificados en la reasignacion del camion
**    Autor        : VSR
**    Fecha        : 09/06/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPlanificacion AS INT,
@NroTransporte AS INT,
@CampoModificado AS TEXT,
@Observacion AS VARCHAR(4000),
@IdUsuarioModificacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    INSERT PlanificacionHistorialReasignacionCamion
    (
	    IdPlanificacion,
	    NroTransporte,
	    CampoModificado,
	    Observacion,
	    IdUsuarioModificacion,
	    FechaModificacion
    )
    VALUES
    (
	    @IdPlanificacion,
	    @NroTransporte,
	    @CampoModificado,
	    @Observacion,
	    @IdUsuarioModificacion,
	    dbo.fnu_GETDATE()
    )

    SET @Status = SCOPE_IDENTITY()
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 