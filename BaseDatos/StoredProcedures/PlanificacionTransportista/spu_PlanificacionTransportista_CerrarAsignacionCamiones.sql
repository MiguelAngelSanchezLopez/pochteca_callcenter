﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_CerrarAsignacionCamiones')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_CerrarAsignacionCamiones'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_CerrarAsignacionCamiones
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_CerrarAsignacionCamiones'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_CerrarAsignacionCamiones
/******************************************************************************
**    Descripcion  : finaliza el proceso de asignacion de camiones
**    Autor        : VSR
**    Fecha        : 10/06/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@Id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    UPDATE PlanificacionTransportista
    SET CerradoPorTransportista = 1
    WHERE
        Id = @Id

    SET @Status = 1
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 