﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_AutocompletarConductoresPorTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_AutocompletarConductoresPorTransportista'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_AutocompletarConductoresPorTransportista
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_AutocompletarConductoresPorTransportista'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_AutocompletarConductoresPorTransportista
/******************************************************************************
**    Descripcion  : obtiene listado de conductores asociados al transportista para autocompletar
**    Autor        : VSR
**    Fecha        : 31/07/2015
*******************************************************************************/
@Texto AS VARCHAR(1024),
@IdTransportista AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT TOP 5
    NombreConductor = ISNULL(FC.Nombre,'') + ISNULL(' ' + FC.Paterno,'') + ISNULL(' ' + FC.Materno,''),
    RutConductor = REPLACE(REPLACE(REPLACE(ISNULL(FC.Rut,'') + CASE WHEN ISNULL(FC.Rut,'') = '' THEN '' ELSE '-' + ISNULL(FC.DV,'') END,' ',''),'.',''),',',''),
    Telefono = ISNULL(FC.Telefono,'')
  FROM
    Usuario AS FC
  WHERE
     (
           ISNULL(FC.Nombre,'') + ISNULL(' ' + FC.Paterno,'') + ISNULL(' ' + FC.Materno,'') LIKE '%'+ @Texto +'%'
        OR REPLACE(REPLACE(REPLACE(ISNULL(FC.Rut,'') + '-' + ISNULL(FC.DV,''),' ',''),'.',''),',','') LIKE '%'+ @Texto +'%'
      )
  AND FC.IdPerfil = 2
  ORDER BY
    NombreConductor

END
GO   