﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_ObtenerInformacionPatio')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_ObtenerInformacionPatio'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_ObtenerInformacionPatio
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_ObtenerInformacionPatio'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_ObtenerInformacionPatio
/******************************************************************************
**    Descripcion  : obtiene informacion del patio
**    Por          : VSR, 15/07/2015
*******************************************************************************/
@IdPlanificacion AS INT,
@NroTransporte AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    ConfirmacionAnden = CASE WHEN PT.ConfirmacionAnden = 1 THEN 'SI' ELSE 'NO' END,
    Anden = ISNULL(PT.Anden,''),
    FechaConfirmacionAnden = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaConfirmacionAnden, 'FECHA_COMPLETA'),''),
	  ConfirmadoAndenPor = ISNULL(UCA.Nombre,'') + ISNULL(' ' + UCA.Materno,'') + ISNULL(' ' + UCA.Paterno,''),
    Sello = ISNULL(PT.Sello,-1),
    FechaLecturaSello = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaLecturaSello, 'FECHA_COMPLETA'),''),
	  ConfirmadoLecturaSelloPor = ISNULL(ULS.Nombre,'') + ISNULL(' ' + ULS.Materno,'') + ISNULL(' ' + ULS.Paterno,'')
  FROM
	  PlanificacionTransportista AS PT
    LEFT JOIN Usuario AS UCA ON (PT.IdUsuarioConfirmacionAnden = UCA.Id)
    LEFT JOIN Usuario AS ULS ON (PT.IdUsuarioLecturaSello = ULS.Id)
  WHERE
      PT.IdPlanificacion = @IdPlanificacion
	AND PT.NroTransporte = @NroTransporte

END



  