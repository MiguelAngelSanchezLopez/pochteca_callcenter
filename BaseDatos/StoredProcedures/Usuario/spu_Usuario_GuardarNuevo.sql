IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_GuardarNuevo'
  DROP  Procedure  dbo.spu_Usuario_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_Usuario_GuardarNuevo'
GO
CREATE Procedure dbo.spu_Usuario_GuardarNuevo
/******************************************************************************
**  Descripcion  : guarda un nuevo registro
**  Fecha        : 23/07/2009
*******************************************************************************/
@Id int output,  
@Username varchar(50),  
@Password varchar(50),  
@Rut varchar(20),
@Dv varchar(1),
@Nombre varchar(50),  
@Paterno varchar(50),  
@Materno varchar(50),  
@Email varchar(50),  
@Telefono varchar(50),  
@IdDireccion int,  
@FechaHoraUltimoAcceso varchar(12),
@IpUltimoAcceso varchar(15),
@FechaHoraCreacion varchar(12),
@FechaHoraModificacion varchar(12),
@LoginDias AS varchar(7),
@Super AS bit,  
@Estado AS int,
@IdPerfil AS int
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdPerfil = 0) SET @IdPerfil=NULL

  INSERT INTO Usuario
  (
	  Username,
	  Password,
	  Rut,
	  Dv,
	  Nombre,
	  Paterno,
	  Materno,
	  Email,
	  Telefono,
	  IdDireccion,
	  FechaHoraUltimoAcceso,
	  IpUltimoAcceso,
	  FechaHoraCreacion,
	  FechaHoraModificacion,
	  LoginDias,
	  Super,
	  Estado,
	  IdPerfil
  )
  VALUES
  (
	  @Username,
	  @Password,
	  @Rut,
	  @Dv,
	  @Nombre,
	  @Paterno,
	  @Materno,
	  @Email,
	  @Telefono,
	  @IdDireccion,
	  @FechaHoraUltimoAcceso,
	  @IpUltimoAcceso,
	  @FechaHoraCreacion,
	  @FechaHoraModificacion,
	  @LoginDias,
	  @Super,
	  @Estado,
	  @IdPerfil
  )
  
  SET @Id = @@IDENTITY  
END
GO  