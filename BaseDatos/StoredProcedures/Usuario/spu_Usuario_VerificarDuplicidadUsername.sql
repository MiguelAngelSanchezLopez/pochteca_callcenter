  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_VerificarDuplicidadUsername')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_VerificarDuplicidadUsername'
  DROP  Procedure  dbo.spu_Usuario_VerificarDuplicidadUsername
END

GO

PRINT  'Creating Procedure spu_Usuario_VerificarDuplicidadUsername'
GO
CREATE Procedure dbo.spu_Usuario_VerificarDuplicidadUsername
/******************************************************************************
**  Descripcion  : valida que el username sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 28/07/2009
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@Valor AS VARCHAR(20)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdUsuario IS NULL OR @IdUsuario=0) SET @IdUsuario = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdUsuario = -1) BEGIN
    IF( EXISTS(
                SELECT
                  U.Username
                FROM
                  Usuario AS U
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(U.Username,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  U.Username
                FROM
                  Usuario AS U
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(U.Username,'')))) = @Valor
                AND U.Id <> @IdUsuario
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO      