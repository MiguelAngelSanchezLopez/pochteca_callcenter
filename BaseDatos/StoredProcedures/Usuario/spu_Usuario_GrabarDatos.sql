IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_GrabarDatos'
  DROP  Procedure  dbo.spu_Usuario_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_Usuario_GrabarDatos'
GO
CREATE Procedure dbo.spu_Usuario_GrabarDatos
/******************************************************************************
**  Descripcion  : graba usuario
**  Fecha        : 27/07/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdUsuario AS INT OUTPUT,
@Rut AS VARCHAR(255),
@DV AS VARCHAR(1),
@Nombre AS VARCHAR(50),
@Paterno AS VARCHAR(50),
@Materno AS VARCHAR(50),
@Username AS VARCHAR(50),
@Password AS VARCHAR(50),
@Email AS VARCHAR(50),
@Telefono AS VARCHAR(4000),
@LoginDias AS VARCHAR(7),
@Super AS INT,
@Estado AS INT,
@IdPerfil AS INT,
@ListadoPermisos AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @existeRut AS INT
    DECLARE @existeUsername AS INT
    DECLARE @fechaHoraActual AS VARCHAR(20)
    DECLARE @MesesCaducaClave AS INT, @FechaCaducidadPassword DATETIME
    SET @fechaHoraActual = dbo.fnu_ObtenerFechaISO()
    SET @IdUsuario = -1
    SET @MesesCaducaClave = (SELECT TOP 1 CONVERT(INT, Valor) FROM Configuracion WHERE Clave = 'webMesesCaducaClave')
    SET @FechaCaducidadPassword = CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(mm, @MesesCaducaClave, dbo.fnu_GETDATE()), 112) + ' 23:59:59.997')

    -- verifica si el Rut existe
    EXEC dbo.spu_Usuario_VerificarDuplicidadRut @existeRut OUTPUT, -1, @RUT
    -- verifica si el Username existe
    EXEC dbo.spu_Usuario_VerificarDuplicidadUsername @existeUsername OUTPUT, -1, @Username

    -- formatea valores
    IF(@IdPerfil = 0) SET @IdPerfil=NULL

    -- graba Usuario si corresponde
    IF(@existeRut = 1 AND @existeUsername = 1) BEGIN
      INSERT INTO Usuario
      (
        Rut,
        Dv,
        Nombre,
        Paterno,
        Materno,
        Username,
        Password,
        Email,
        Telefono,
        LoginDias,
        Super,
        Estado,
        FechaHoraCreacion,
        FechaHoraModificacion,
        IdPerfil,
        FechaCaducidadPassword
      )
      VALUES
      (
        @Rut,
        @Dv,
        @Nombre,
        @Paterno,
        @Materno,
        @Username,
        @Password,
        @Email,
        @Telefono,
        @LoginDias,
        @Super,
        @Estado,
        @fechaHoraActual,
        @fechaHoraActual,
        @IdPerfil,
        @FechaCaducidadPassword
      )
      SET @Status = 'ingresado'
      SET @IdUsuario = SCOPE_IDENTITY()

      -- inserta la matriz de permisos
      INSERT INTO FuncionesPaginaxUsuario
      (
	      IdFuncionPagina,
	      IdUsuario
      )
      SELECT
        valor,
        @IdUsuario
      FROM
        dbo.fnu_InsertaListaTabla(@ListadoPermisos)

      ------------------------------------------------------
      -- grabar todas las funciones "VER" que no han sido asignadas en las paginas donde tiene permiso
      ------------------------------------------------------
      INSERT INTO FuncionesPaginaXUsuario
      (
	      IdFuncionPagina,
	      IdUsuario
      )
      SELECT
        T.IdFuncion,
        @IdUsuario
      FROM (
           -- obtiene todas las funciones "VER" de las paginas donde el usuario tiene permisos asignados y marca las que estan seleccionadas
            SELECT
              IdFuncion = FP.Id,
              NombreFuncion = FP.Nombre,
              Asignada = CASE WHEN (SELECT COUNT(aux.IdFuncionPagina) FROM FuncionesPaginaXUsuario AS aux WHERE aux.IdFuncionPagina = FP.Id) > 0 THEN 1 ELSE 0 END
            FROM
              FuncionPagina AS FP
              INNER JOIN (
        	                -- obtiene las paginas de las funciones asignadas del usuario
                          SELECT DISTINCT
                            IdPagina = FP.IdPagina
                          FROM
                            FuncionesPaginaXUsuario AS FPxU
                            INNER JOIN FuncionPagina AS FP ON (FPxU.IdFuncionPagina = FP.Id)
                          WHERE
                            FPxU.IdUsuario = @IdUsuario
              ) AS T ON (FP.IdPagina = T.IdPagina)
            WHERE
              FP.Nombre = 'VER'  	
      ) AS T
      WHERE
        T.Asignada = 0

    END ELSE IF (@existeRut = 1 Or @existeUsername = 1) BEGIN
      SET @Status = 'duplicado'
    END ELSE BEGIN
      SET @Status = 'error'
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO       