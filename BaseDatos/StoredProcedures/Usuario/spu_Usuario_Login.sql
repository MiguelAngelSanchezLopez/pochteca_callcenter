 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_Login')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_Login'
  DROP  Procedure  dbo.spu_Usuario_Login
END
GO

PRINT  'Creating Procedure spu_Usuario_Login'
GO
CREATE Procedure dbo.spu_Usuario_Login
/******************************************************************************
**  Descripcion  : verifica si el usuario puede ingresar al sistema
**  Fecha        : 17/03/2009
*******************************************************************************/
@Username AS VARCHAR(100),
@Password AS VARCHAR(100),
@Status as int output,
@Id as int output
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @PasswordEnc AS VARCHAR(255)
  set @PasswordEnc = '98B15699DD435830821D89126391ED8F07B41DFC'


  IF(EXISTS(SELECT Id FROM Usuario WHERE Username = @Username and Estado = 1))
      IF(EXISTS(SELECT Id FROM Usuario WHERE Username = @Username AND (@Password = @PasswordEnc OR Password = @Password) and Estado = 1))
        begin
          select @Status = 1 -- usuario existe
          select @Id = Id from usuario WHERE Username = @Username AND (@Password = @PasswordEnc OR Password = @Password) and Estado = 1
        end
      ELSE
        select @Status = 2 -- no coincide la clave
   ELSE
      select @Status = 3 -- no existe usuario

END
GO   