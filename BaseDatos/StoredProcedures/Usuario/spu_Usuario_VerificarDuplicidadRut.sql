 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_VerificarDuplicidadRut')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_VerificarDuplicidadRut'
  DROP  Procedure  dbo.spu_Usuario_VerificarDuplicidadRut
END

GO

PRINT  'Creating Procedure spu_Usuario_VerificarDuplicidadRut'
GO
CREATE Procedure dbo.spu_Usuario_VerificarDuplicidadRut
/******************************************************************************
**  Descripcion  : valida que el rut sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 27/07/2009
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@Valor AS VARCHAR(20)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdUsuario IS NULL OR @IdUsuario = 0) SET @IdUsuario = -1
  IF(@Valor IS NULL OR @Valor='') SET @Valor = '-1'

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdUsuario = -1) BEGIN
    IF( EXISTS(
                SELECT
                  U.Rut
                FROM
                  Usuario AS U
                WHERE
                  RTRIM(LTRIM(ISNULL(U.Rut,''))) + RTRIM(LTRIM(ISNULL(U.DV,''))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  U.Username
                FROM
                  Usuario AS U
                WHERE
                  RTRIM(LTRIM(ISNULL(U.Rut,''))) + RTRIM(LTRIM(ISNULL(U.DV,''))) = @Valor
                AND U.Id <> @IdUsuario
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO      