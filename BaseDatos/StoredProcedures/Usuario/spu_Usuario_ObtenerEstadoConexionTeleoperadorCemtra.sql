﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra'
  DROP  Procedure  dbo.spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra
END

GO

PRINT  'Creating Procedure spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra'
GO
CREATE Procedure [dbo].[spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra]
/******************************************************************************
**    Descripcion  : obtiene el estado de conexion de los teleoperadores
**    Por          : ME, 22/05/2017
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @ListadoTeleoperadorDescartados VARCHAR(8000)
  SET @ListadoTeleoperadorDescartados = ''

  -----------------------------------------------------------------
  -- obtiene la ultima accion realizada por el usuario
  -----------------------------------------------------------------
  DECLARE @TablaUltimaAccion AS TABLE(IdUsuario INT, Fecha DATETIME, Accion VARCHAR(255))
  INSERT INTO @TablaUltimaAccion(IdUsuario, Fecha, Accion)
  SELECT
    T.IdUsuario,
    T.Fecha,
    LS.Accion
  FROM (
        SELECT
          IdUsuario = LS.IdUsuario,
          Fecha = MAX(LS.Fecha)
        FROM
          LogSesion AS LS
          INNER JOIN Usuario AS U ON (LS.IdUsuario = U.Id)
          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
		      INNER JOIN UsuarioDatoExtra AS Ud ON (U.Id = Ud.IdUsuario)
        WHERE
            P.Llave = 'TO'
        AND U.Estado = 1
        AND U.Username NOT IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoTeleoperadorDescartados))
		    AND Ud.GestionarSoloCemtra <> 'ExcluirCemtra'
        AND LS.Accion NOT LIKE '%GPSTrack%'
        GROUP BY
          LS.IdUsuario
  ) AS T
  INNER JOIN LogSesion AS LS ON (T.IdUsuario = LS.IdUsuario AND T.Fecha = LS.Fecha)

  -----------------------------------------------------------------
  -- obtiene el ultimo inicio de sesion del usuario
  -----------------------------------------------------------------
  DECLARE @TablaUltimoInicioSesion AS TABLE(IdUsuario INT, Fecha DATETIME, Accion VARCHAR(255))
  INSERT INTO @TablaUltimoInicioSesion(IdUsuario, Fecha, Accion)
  SELECT
    T.IdUsuario,
    T.Fecha,
    LS.Accion
  FROM (
        SELECT
          IdUsuario = LS.IdUsuario,
          Fecha = MAX(LS.Fecha)
        FROM
          LogSesion AS LS
          INNER JOIN Usuario AS U ON (LS.IdUsuario = U.Id)
          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
		      INNER JOIN UsuarioDatoExtra AS Ud ON (U.Id = Ud.IdUsuario)
        WHERE
            P.Llave = 'TO'
        AND U.Estado = 1
        AND U.Username NOT IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoTeleoperadorDescartados))
        AND LS.Accion = 'Inicia sesión'
		    AND Ud.GestionarSoloCemtra <> 'ExcluirCemtra'
        AND LS.Accion NOT LIKE '%GPSTrack%'
        GROUP BY
          LS.IdUsuario
  ) AS T
  INNER JOIN LogSesion AS LS ON (T.IdUsuario = LS.IdUsuario AND T.Fecha = LS.Fecha)

  -----------------------------------------------------------------
  -- obtiene la informacion del log del usuario
  -----------------------------------------------------------------
  DECLARE @TablaLog AS TABLE (IdUsuario INT, Username VARCHAR(255), NombreUsuario VARCHAR(255), Estado VARCHAR(255), UltimaAccion VARCHAR(255), UltimaFechaAccion VARCHAR(255),
                              SegundosUltimaAccionFecha NUMERIC, FechaUltimoInicioSesion VARCHAR(255), SegundosFechaUltimoInicioSesion NUMERIC)
  INSERT @TablaLog
  (
	  IdUsuario,
	  Username,
	  NombreUsuario,
	  Estado,
	  UltimaAccion,
	  UltimaFechaAccion,
	  SegundosUltimaAccionFecha,
	  FechaUltimoInicioSesion,
	  SegundosFechaUltimoInicioSesion
  )
  SELECT
    T.IdUsuario,
    T.Username,
    T.NombreUsuario,
    Estado = CASE 
              WHEN PATINDEX('LLAMANDO %', T.UltimaAccion) > 0 THEN 'LLAMANDO'
              WHEN PATINDEX('HABLANDO %', T.UltimaAccion) > 0 THEN 'HABLANDO'
              WHEN PATINDEX('Cierra sesión', T.UltimaAccion) > 0 THEN 'DESCONECTADO'
              WHEN PATINDEX('%TELEOPERADOR_AUXILIAR%', T.UltimaAccion) > 0 THEN 'AUXILIAR'
              WHEN T.UltimaAccion = '' THEN 'DESCONECTADO'
              ELSE 'DISPONIBLE'
            END,
    UltimaAccion = CASE
                    WHEN PATINDEX('%TELEOPERADOR_AUXILIAR%', T.UltimaAccion) > 0 THEN REPLACE(REPLACE(REPLACE(T.UltimaAccion,'Estado auxiliar: ',''),'TELEOPERADOR_AUXILIAR_',''),'_',' ')
                    ELSE T.UltimaAccion
                  END,
    UltimaFechaAccion = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.UltimaFechaAccion,'FECHA_COMPLETA'),''),
    SegundosUltimaAccionFecha = CASE WHEN T.UltimaFechaAccion IS NULL THEN -1 ELSE DATEDIFF(ss,T.UltimaFechaAccion,dbo.fnu_GETDATE()) END,
    FechaUltimoInicioSesion = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaUltimoInicioSesion,'FECHA_COMPLETA'),''),
    SegundosFechaUltimoInicioSesion = CASE WHEN T.FechaUltimoInicioSesion IS NULL THEN -1 ELSE DATEDIFF(ss,T.FechaUltimoInicioSesion,dbo.fnu_GETDATE()) END
  FROM (
        SELECT
          IdUsuario = U.Id,
          Username = U.Username,
          NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
          UltimaAccion = ISNULL(TUA.Accion,''),
          UltimaFechaAccion = TUA.Fecha,
          FechaUltimoInicioSesion = TUIA.Fecha
        FROM
          Usuario AS U
          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
          LEFT JOIN @TablaUltimaAccion AS TUA ON (U.Id = TUA.IdUsuario)
          LEFT JOIN @TablaUltimoInicioSesion AS TUIA ON (U.Id = TUIA.IdUsuario)
		  INNER JOIN UsuarioDatoExtra AS Ud ON (U.Id = Ud.IdUsuario)
		  AND Ud.GestionarSoloCemtra <> 'ExcluirCemtra'
        WHERE
            P.Llave = 'TO'
        AND U.Estado = 1
        AND U.Username NOT IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoTeleoperadorDescartados))
  ) AS T
  ORDER BY
    T.UltimaFechaAccion DESC


  -----------------------------------------------------------------------
  -- TABLA 0: Listado usuarios
  -----------------------------------------------------------------------
  SELECT
    T.*,
    Orden = CASE WHEN (T.Estado = 'DESCONECTADO') THEN 1 ELSE 0 END
  FROM
    @TablaLog AS T
  WHERE
    T.Estado NOT IN ('DESCONECTADO')
  ORDER BY
    Orden, T.NombreUsuario

  -----------------------------------------------------------------------
  -- TABLA 1: Porcentajes
  -----------------------------------------------------------------------
  DECLARE @TotalRegistros AS INT
  SET @TotalRegistros = (SELECT COUNT(IdUsuario) FROM @TablaLog)

  SELECT
    Estado = T.Estado,
    Total = COUNT(T.Estado),
    Porcentaje = CASE WHEN @TotalRegistros = 0 THEN 0 ELSE
                  ROUND((CONVERT(FLOAT, COUNT(T.Estado)) / @TotalRegistros) * 100,2)
                 END   
  FROM (
        SELECT
          Estado = CASE WHEN T.Estado = 'AUXILIAR' THEN T.Estado + ' ' + T.UltimaAccion ELSE T.Estado END
        FROM
          @TablaLog AS T
  ) AS T
  GROUP BY
    T.Estado
  ORDER BY
    Estado

  -----------------------------------------------------------------------
  -- TABLA 2: Obtiene nivel contactabilidad de alertas en las ultimas 4 horas
  -----------------------------------------------------------------------
  SELECT
    TablaPaso2.TotalAlertasRecibidas,
    TablaPaso2.TotalAlertasGestionadas,
    Porcentaje = CASE WHEN TablaPaso2.TotalAlertasRecibidas = 0 THEN 0 ELSE
                   ROUND((CONVERT(FLOAT, TablaPaso2.TotalAlertasGestionadas) / TablaPaso2.TotalAlertasRecibidas) * 100, 2)
                 END
  FROM (
          -----------------------------------------------------------------------
          -- PASO 2: obtiene totales
          -----------------------------------------------------------------------
          SELECT
            TotalAlertasRecibidas = COUNT(TablaPaso1.IdAlerta),
            TotalAlertasGestionadas = ISNULL(SUM(TablaPaso1.Gestionada),0)
          FROM (
                -----------------------------------------------------------------------
                -- PASO 1: obtiene alertas recibidas y contactadas en las ultimas 4 horas
                -----------------------------------------------------------------------
                SELECT
                  IdAlerta = A.Id,
                  Gestionada = CASE WHEN HE.IdAlerta IS NULL THEN 0 ELSE 1 END,
                  A.DescripcionAlerta
                FROM
                  CallCenterAlerta AS CA
                  INNER JOIN Alerta AS A ON (CA.IdAlerta = A.Id)
                  INNER JOIN AlertaDefinicion AS AD ON (A.DescripcionAlerta = AD.Nombre)
                  LEFT JOIN CallCenterHistorialEscalamiento AS HE ON (A.Id = HE.IdAlerta)
                WHERE
                    A.Permiso <> 'REPROGRAMADA'
                AND DATEDIFF(hh, A.FechaHoraCreacion, dbo.fnu_GETDATE()) < 4
                AND AD.GestionarPorCemtra = 1
		      ) AS TablaPaso1  
  ) AS TablaPaso2

END