IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_Usuario_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_Usuario_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_Usuario_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de usuarios usando filtros de busqueda
**    Por          : VSR, 24/07/2009
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(150),
@Username AS VARCHAR(50),
@IdPerfil AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdPerfilUsuarioConectado INT, @ID_PERFIL_SUPERVISOR INT, @listadoPerfilesRestrigidos VARCHAR(8000)
  SET @ID_PERFIL_SUPERVISOR = 4
  SET @IdPerfilUsuarioConectado = (SELECT IdPerfil FROM Usuario WHERE Id = @IdUsuario)

  IF (@IdPerfilUsuarioConectado = @ID_PERFIL_SUPERVISOR) BEGIN
    SET @listadoPerfilesRestrigidos = 'TO,SPA,CLT'
  END ELSE BEGIN
    SET @listadoPerfilesRestrigidos = '-1'
  END

  SELECT DISTINCT
	  IdUsuario = U.Id,
	  Nombre = ISNULL(U.Nombre,''),
	  Paterno = ISNULL(U.Paterno,''),
	  Materno = ISNULL(U.Materno,''),
	  NombreCompleto = ISNULL(RTRIM(LTRIM(U.Nombre)),'') + ISNULL(' '+ RTRIM(LTRIM(U.Paterno)),'') + ISNULL(' '+ RTRIM(LTRIM(U.Materno)),''),
	  Username = ISNULL(U.Username,''),
	  Rut = ISNULL(U.Rut,''),
	  Dv = ISNULL(U.Dv,''),
	  RutCompleto = ISNULL(U.Rut,''),
	  NombrePerfil = ISNULL(P.Nombre,''),
	  FechaCreacion = ISNULL(U.FechaHoraCreacion,''),
    Activo = CASE WHEN U.Estado = 1 THEN 'SI' ELSE 'NO' END
  FROM
    Usuario AS U
    LEFT JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      (@Username = '-1' OR U.Username LIKE '%'+ @Username +'%')
  AND (@Nombre = '-1' OR RTRIM(LTRIM(ISNULL(U.Nombre,''))) +' '+ RTRIM(LTRIM(ISNULL(U.Paterno,''))) +' '+ RTRIM(LTRIM(ISNULL(U.Materno,''))) LIKE '%'+ REPLACE(@Nombre,' ','%') +'%')
  AND (@IdPerfil = -1 OR U.IdPerfil = @IdPerfil)
  AND (@listadoPerfilesRestrigidos = '-1' OR P.Llave IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@listadoPerfilesRestrigidos)))
  ORDER BY
    IdUsuario DESC

END

