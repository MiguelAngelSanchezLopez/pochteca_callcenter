﻿ IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ReportePulsoDiario')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ReportePulsoDiario'
  DROP  Procedure  dbo.spu_Usuario_ReportePulsoDiario
END

GO

PRINT  'Creating Procedure spu_Usuario_ReportePulsoDiario'
GO
/**********************************************************	
	Set @Jornada = 'Todos'  --'Nocturno (22:00 a 5:00)'
	Set @FechaIni = '20170301'
	Set @FechaFin = '20170728'

	exec spu_Usuario_ReportePulsoDiario '20170301', '20170728', 'Todos', 'Todos'
***********************************************************/
CREATE PROCEDURE spu_Usuario_ReportePulsoDiario (
	@FechaInicio Varchar(50),
	@FechaTermino Varchar(50),
	@Jornada  Varchar(50),
	@IdTeleOP Varchar(50)
)
As		
Begin
	Declare @i Int, @j Int	
	Declare @AuxFecIni  Datetime
	Declare @AuxFecFin  Datetime
	Declare @AuxFecIni2 Datetime
	Declare @AuxFecFin2 Datetime
	Declare @HrInicio   Varchar(5)
	Declare @HrFinal    Varchar(5)
  Declare	@FechaIni datetime, @FechaFin Datetime

  IF (@FechaInicio = '-1') BEGIN
    SET @FechaIni = CONVERT(DATETIME, '20170101 00:00')
  END ELSE BEGIN
    SET @FechaIni = CONVERT(DATETIME, @FechaInicio + ' 00:00')
  END

  IF (@FechaTermino = '-1') BEGIN
    SET @FechaFin = CONVERT(DATETIME, '21001231 23:59:59.997')
  END ELSE BEGIN
    SET @FechaFin = CONVERT(DATETIME, @FechaTermino + ' 23:59:59.997')
  END

	Declare @To as Table (
		Id       Int Identity,
		IdTO	 Int
	)

	Declare @Data As Table (
		Id       Int Identity,
		IdAlerta Int,
		IdTO	 Int,
		FechaAsi Datetime,
		FechaTer Datetime
	)

	Declare @Reporte As Table (
		Id			Int Identity,
		IdTo		Int,
		Alertas		float,
		NS			float,
		NS_Percent  float,
		Tmo			Int,
		TotalTime	Int
	)
	

	IF (@Jornada = '-1')
	Begin
		
		Insert Into @Data 
		Select
			AGA.IdAlerta,
			AGA.IdUsuarioAsignado,
			AGA.FechaAsignacion,
			AGA.FechaTerminoGestion 
		from 
			CallCenterAlertaGestionAcumulada AS AGA
      INNER JOIN Alerta AS A ON (A.Id = AGA.IdAlerta)
		Where 
			A.FechaHoraCreacion BetWeen @FechaIni And (@FechaFin)
			And (@IdTeleOP = '-1' Or convert(varchar(50),AGA.IdUsuarioAsignado) =  @IdTeleOP)

	End
	
	If (@Jornada <> 'Nocturno (22:00 a 5:00)')
	Begin
		Set @i = 0
		Set @j = DATEDIFF (DD, @FechaIni, @FechaFin)

		Select @HrInicio = Extra1, @HrFinal = Extra2 From TipoGeneral Where Tipo ='TurnoTeleOperador' And Nombre = @Jornada

		while (@i <= @j)
		begin
				
			Set @AuxFecIni = (select DATEADD(DD, @i, Convert(varchar(10),@FechaIni, 112) + ' ' + @HrInicio))
			Set @AuxFecFin = (select DATEADD(DD, @i, Convert(varchar(10),@FechaIni, 112) + ' ' + @HrFinal))

			Insert Into @Data 
			Select
				AGA.IdAlerta,
				AGA.IdUsuarioAsignado,
				AGA.FechaAsignacion,
				AGA.FechaTerminoGestion 
			from 
			  CallCenterAlertaGestionAcumulada AS AGA
        INNER JOIN Alerta AS A ON (A.Id = AGA.IdAlerta)
			Where 
				A.FechaHoraCreacion BetWeen @AuxFecIni And @AuxFecFin	
				And (@IdTeleOP = '-1' Or convert(varchar(50),AGA.IdUsuarioAsignado) =  @IdTeleOP)	

			set @i = @i + 1

		end
	End
	Else
	Begin
		Set @i = 0
		Set @j = DATEDIFF (DD, @FechaIni, @FechaFin)

		Select @HrInicio = Extra1, @HrFinal = Extra2 From TipoGeneral Where Tipo ='TurnoTeleOperador' And Nombre = @Jornada

		while (@i <= @j)
		begin
				
			Set @AuxFecIni  = (select DATEADD(DD, @i, Convert(varchar(10),@FechaIni, 112) + ' ' + @HrInicio))
			Set @AuxFecFin  = (select DATEADD(DD, @i, Convert(varchar(10),@FechaIni, 112) + ' ' + '23:59'))
			Set @AuxFecIni2 = (select DATEADD(DD, @i, Convert(varchar(10),@FechaIni, 112) + ' ' + '00:00'))
			Set @AuxFecFin2 = (select DATEADD(DD, @i, Convert(varchar(10),@FechaIni, 112) + ' ' + @HrFinal))

			Insert Into @Data 
			Select
				AGA.IdAlerta,
				AGA.IdUsuarioAsignado,
				AGA.FechaAsignacion,
				AGA.FechaTerminoGestion 
			from 
			  CallCenterAlertaGestionAcumulada AS AGA
        INNER JOIN Alerta AS A ON (A.Id = AGA.IdAlerta)
			Where 
				A.FechaHoraCreacion BetWeen @AuxFecIni And @AuxFecFin
				And (@IdTeleOP = '-1' Or convert(varchar(50),AGA.IdUsuarioAsignado) =  @IdTeleOP)
			Union
			Select
				AGA.IdAlerta,
				AGA.IdUsuarioAsignado,
				AGA.FechaAsignacion,
				AGA.FechaTerminoGestion 
			from 
			  CallCenterAlertaGestionAcumulada AS AGA
        INNER JOIN Alerta AS A ON (A.Id = AGA.IdAlerta)
			Where 
				A.FechaHoraCreacion BetWeen @AuxFecIni2 And @AuxFecFin2	
				And (@IdTeleOP = '-1' Or convert(varchar(50),AGA.IdUsuarioAsignado) =  @IdTeleOP)	

			set @i = @i + 1

		end
	End



		Insert Into @To
		select Distinct IdTO  from @Data

		Declare @idTo as Int
		set @i = 1
		Set @j = (Select MAX(Id) from @To)

		While (@i <= @j)
		Begin
			
			Select @idTo = IdTO from @To Where Id = @i 
			
			Begin Try
			Insert Into @Reporte (IdTo, Alertas, NS, Tmo)
				Select 
					@idTo,
					(Select count(1) from @Data Where IdTO = @idTo),
					(Select count(1) from @Data Where IdTO = @idTo And DATEDIFF (MINUTE, FechaAsi, FechaTer) <= 7),
					(Select SUM( DATEDIFF (SECOND , FechaAsi, FechaTer)) from @Data Where IdTO = @idTo)


				Update @Reporte Set 
					Tmo = (Tmo / Alertas),
					NS_Percent = ((NS / Alertas) * 100) ,
					TotalTime = Tmo 
				Where 
					IdTo = @idTo 
			End Try
			Begin Catch
				Set @idTo = NULL
			End Catch			
				
			
			Set @i = @i + 1
			Set @idTo = NULL

		End

	
	Select 		
		IdTo,
		NombreCompleto = u.Nombre + ' ' + u.Paterno + ' ' + u.Materno,
		Convert(Int,Alertas) As Alertas,
		Convert(Int,NS) As NS,
		Convert(int,NS_Percent) As NS_Percent ,
		[dbo].[fnu_ConvertirSegundosToDHMS] (Tmo) Tmo,
		[dbo].[fnu_ConvertirSegundosToDHMS] (TotalTime ) totalTime
	from 
		@Reporte r Inner Join
		Usuario u On r.IdTo = u.Id 
End
