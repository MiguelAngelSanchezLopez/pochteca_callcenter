﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getTractoDisponibles')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getTractoDisponibles'
  DROP  Procedure  dbo.spu_Carrier_getTractoDisponibles
END

GO

PRINT  'Creating Procedure spu_Carrier_getTractoDisponibles'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getTractoDisponibles]
/******************************************************************************  
**    Descripcion  : obtiene tractos disponibles
**    Por          : DG, 07/02/2017
*******************************************************************************/  
@IdUsuarioTransportista AS INT
AS  
BEGIN  

	-- Se obtienen los datos desde el SP de Mobile, almacenandolos 
	-- en una tabla temporal para utilizarlos localmente
	DECLARE @TablaTractosDisponibles AS TABLE(
		 PlacaTracto VARCHAR(255)
		,CodigoCedis INT
		,NombreCedis VARCHAR(255)
		,FechaLlegada VARCHAR(255)
		,TiempoEspera VARCHAR(255)
	)
  
	INSERT INTO @TablaTractosDisponibles
	EXEC spu_Mobile_MX_AltotrackWalmart_CarrierObtenerTractosDisponibles @IdUsuarioTransportista


	SELECT
		PlacaTracto
		,CodigoCedis
		,NombreCedis
		,FechaLlegada
    ,TiempoEspera
	FROM @TablaTractosDisponibles
  
END  