﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getTractoRegresando')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getTractoRegresando'
  DROP  Procedure  dbo.spu_Carrier_getTractoRegresando
END

GO

PRINT  'Creating Procedure spu_Carrier_getTractoRegresando'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getTractoRegresando] 
/******************************************************************************    
**    Descripcion  : obtiene remolques en patio  
**    Por          : DG, 07/02/2017  
*******************************************************************************/    
@IdUsuarioTransportista AS INT  
AS    
BEGIN    
  
	-- Se obtienen los datos desde el SP de Mobile, almacenandolos   
	-- en una tabla temporal para utilizarlos localmente  
	DECLARE @TablaTractosRegresando AS TABLE(
		PlacaTracto VARCHAR(255)
		,CodigoCedis INT
		,NombreCedis VARCHAR(255)
		,Determinante VARCHAR(8000)
		,TiempoLlegadaDestino VARCHAR(255)
		,KilometrosDestino VARCHAR(255)
		,TipoCarga VARCHAR(255)
		,TipoRegreso VARCHAR(255)
		,Semaforo VARCHAR(255)
		,TiempoAtrasoSegundos BIGINT)

  INSERT INTO @TablaTractosRegresando
  EXEC spu_Mobile_MX_AltotrackWalmart_CarrierObtenerTractosRegresando @IdUsuarioTransportista
  
  -----------------------------------------------------------------------
  -- TABLA 0: RESUMEN
  -----------------------------------------------------------------------
	
  SELECT
    Semaforo = T.Semaforo,
    TiempoPromedio = dbo.fnu_ConvertirSegundosToDHMS(AVG(T.TiempoAtrasoSegundos)),
    Total = COUNT(T.Semaforo)
  FROM
    @TablaTractosRegresando AS T
  GROUP BY
    T.Semaforo

  -----------------------------------------------------------------------
  -- TABLA 1: DETALLE
  -----------------------------------------------------------------------
	SELECT   
		 PlacaTracto 
		,CodigoCedis 
		,NombreCedis 
		,Determinante
		,TiempoLlegadaDestino 
		,KilometrosDestino 
		,TipoCarga 
		,TipoRegreso
		,Semaforo 
		,TiempoAtrasoSegundos
	FROM @TablaTractosRegresando     	

END 