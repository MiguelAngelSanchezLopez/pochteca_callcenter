﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getRemolquesEnCortina')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getRemolquesEnCortina'
  DROP  Procedure  dbo.spu_Carrier_getRemolquesEnCortina
END

GO

PRINT  'Creating Procedure spu_Carrier_getRemolquesEnCortina'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getRemolquesEnCortina] 
/******************************************************************************    
**    Descripcion  : obtiene remolques en patio  
**    Por          : DG, 07/02/2017  
*******************************************************************************/    
@IdUsuarioTransportista AS INT  
AS    
BEGIN    
  
 -- Se obtienen los datos desde el SP de Mobile, almacenandolos   
 -- en una tabla temporal para utilizarlos localmente  
 DECLARE @TablaRemolquesEnCortina AS TABLE(
	IdEmbarque BIGINT
	,CodigoCedis INT
	,NombreCedis VARCHAR(255)
	,PlacaRemolque VARCHAR(255)
	,Determinante VARCHAR(8000)
	,FechaPosicionCortina VARCHAR(255)
	,Cortina VARCHAR(255)
	,TiempoCargando VARCHAR(255)
	,Semaforo VARCHAR(255)
	,TiempoEsperaSegundos BIGINT)

  INSERT INTO @TablaRemolquesEnCortina
  EXEC spu_Mobile_MX_AltotrackWalmart_CarrierObtenerRemolquesEnCortina @IdUsuarioTransportista
  
  -----------------------------------------------------------------------
  -- TABLA 0: RESUMEN
  -----------------------------------------------------------------------
  SELECT
    Semaforo = T.Semaforo,
    TiempoPromedio = dbo.fnu_ConvertirSegundosToDHMS(AVG(T.TiempoEsperaSegundos)),
    Total = COUNT(T.Semaforo)
  FROM
    @TablaRemolquesEnCortina AS T
  GROUP BY
    T.Semaforo

  -----------------------------------------------------------------------
  -- TABLA 1: DETALLE
  -----------------------------------------------------------------------
	SELECT   
		IdEmbarque
		,CodigoCedis
		,NombreCedis
		,PlacaRemolque
		,Determinante
		,FechaPosicionCortina
		,Cortina
		,TiempoCargando
		,Semaforo
		,TiempoEsperaSegundos
	FROM @TablaRemolquesEnCortina     	

END 