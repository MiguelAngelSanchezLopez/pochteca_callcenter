﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ExplicacionMultipunto')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ExplicacionMultipunto'
  DROP  Procedure  dbo.spu_Combo_ExplicacionMultipunto
END

GO

PRINT  'Creating Procedure spu_Combo_ExplicacionMultipunto'
GO
CREATE Procedure dbo.spu_Combo_ExplicacionMultipunto
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> ExplicacionMultipunto"
**    Autor        : VSR
**    Fecha        : 27/08/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor)),
    FrecuenciaRepeticion = ISNULL(TG.Extra2,-1),
    TipoExplicacion = ISNULL(TG.Extra1,'Normal')
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'ExplicacionMultipunto'
  AND TG.Activo = 1
  ORDER BY
    Texto

END



