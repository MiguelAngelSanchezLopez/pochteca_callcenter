﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_CategoriaPagina')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_CategoriaPagina'
  DROP  Procedure  dbo.spu_Combo_CategoriaPagina
END

GO

PRINT  'Creating Procedure spu_Combo_CategoriaPagina'
GO
CREATE Procedure dbo.spu_Combo_CategoriaPagina
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "CategoriaPagina"
**    Autor        : VSR
**    Fecha        : 04/09/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = ISNULL(CP.Nombre,''),
    Valor = ISNULL(CP.Nombre,'')
  FROM
    CategoriaPagina AS CP
  ORDER BY
    Texto

END



