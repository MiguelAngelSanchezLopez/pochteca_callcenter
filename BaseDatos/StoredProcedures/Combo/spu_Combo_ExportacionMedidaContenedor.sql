﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ExportacionMedidaContenedor')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ExportacionMedidaContenedor'
  DROP  Procedure  dbo.spu_Combo_ExportacionMedidaContenedor
END

GO

PRINT  'Creating Procedure spu_Combo_ExportacionMedidaContenedor'
GO
CREATE Procedure dbo.spu_Combo_ExportacionMedidaContenedor
/******************************************************************************
**    Descripcion  : obtiene MedidaContenedor de tabla OS_Exportacion
**    Autor        : VSR
**    Fecha        : 26/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = E.MedidaContenedor,
    Valor = E.MedidaContenedor
  FROM
    OS_Exportacion AS E
  WHERE
    ISNULL(E.MedidaContenedor,'') <> ''
  ORDER BY
    Texto

END



