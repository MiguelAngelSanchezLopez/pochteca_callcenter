﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_TeleOperadores')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_TeleOperadores'
  DROP  Procedure  dbo.spu_Combo_TeleOperadores
END

GO

PRINT  'Creating Procedure spu_Combo_TeleOperadores'
GO

CREATE PROCEDURE spu_Combo_TeleOperadores (
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Tele Operadores"
**    Autor        : Mauricio Espinoza
**    Fecha        : 28/07/2017
*******************************************************************************/
    @Filtro AS VARCHAR(255)
)
As
Begin
	  SELECT
      Valor = U.Id,		
		  Texto = U.Nombre + ' ' + U.Paterno + ' ' + U.Materno
    FROM
      Usuario AS U
      INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)          
    WHERE
      P.Llave = 'TO'
		  And (@Filtro = '-1' Or U.Nombre Like '%' + @Filtro + '%')
	  ORDER BY
		  U.Nombre
End
		
