﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Region')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Region'
  DROP  Procedure  dbo.spu_Combo_Region
END

GO

PRINT  'Creating Procedure spu_Combo_Region'
GO
CREATE Procedure spu_Combo_Region
/*********************************************************************************************
**    Descripcion  : Obtiene el listado de regiones
**    Autor        : VSR
**    Fecha        : 02/06/2015
*********************************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.id))
  FROM
    region AS TG
  
  
END
