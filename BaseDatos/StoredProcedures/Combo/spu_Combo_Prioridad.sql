﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Prioridad')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Prioridad'
  DROP  Procedure  dbo.spu_Combo_Prioridad
END

GO

PRINT  'Creating Procedure spu_Combo_Prioridad'
GO
CREATE Procedure dbo.spu_Combo_Prioridad
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> Prioridad"
**    Autor        : VSR
**    Fecha        : 30/07/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor))
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'Prioridad'
  AND TG.Activo = 1
  ORDER BY
    CONVERT(INT, TG.Extra1)

END



