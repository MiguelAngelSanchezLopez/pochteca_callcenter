﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ExportacionNombrePuertoEmbarque')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ExportacionNombrePuertoEmbarque'
  DROP  Procedure  dbo.spu_Combo_ExportacionNombrePuertoEmbarque
END

GO

PRINT  'Creating Procedure spu_Combo_ExportacionNombrePuertoEmbarque'
GO
CREATE Procedure dbo.spu_Combo_ExportacionNombrePuertoEmbarque
/******************************************************************************
**    Descripcion  : obtiene NombrePuertoEmbarque de tabla OS_Exportacion
**    Autor        : VSR
**    Fecha        : 26/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = E.NombrePuertoEmbarque,
    Valor = E.NombrePuertoEmbarque
  FROM
    OS_Exportacion AS E
  WHERE
    ISNULL(E.NombrePuertoEmbarque,'') <> ''
  ORDER BY
    Texto

END



