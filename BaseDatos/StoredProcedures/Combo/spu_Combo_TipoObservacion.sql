﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_TipoObservacion')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_TipoObservacion'
  DROP  Procedure  dbo.spu_Combo_TipoObservacion
END

GO

PRINT  'Creating Procedure spu_Combo_TipoObservacion'
GO
CREATE Procedure dbo.spu_Combo_TipoObservacion
/*********************************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> TipoObservacion"
**    Autor        : DG
**    Fecha        : 13/11/2014
**********************************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(Nombre)),
    Valor = RTRIM(LTRIM(Valor)),
    Data_Clasificacion = ISNULL(Extra2,'')
  FROM TipoGeneral 
  WHERE Tipo = 'TipoObservacion'
		AND Activo = 1
		AND ((@Filtro = '-1') OR Extra1 = @Filtro)
  ORDER BY Texto

END