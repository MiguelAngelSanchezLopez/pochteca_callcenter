﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Explicacion')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Explicacion'
  DROP  Procedure  dbo.spu_Combo_Explicacion
END

GO

PRINT  'Creating Procedure spu_Combo_Explicacion'
GO
CREATE Procedure dbo.spu_Combo_Explicacion
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> Explicacion"
**    Autor        : VSR
**    Fecha        : 09/07/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor)),
    TipoExplicacion = ISNULL(TG.Extra1,'Normal'),
    Orden = 1
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'Explicacion'
  AND TG.Activo = 1

  -- agrega opcion OTRO al final del combo
  UNION ALL
  SELECT
    Texto = 'Otro ...',
    Valor = 'MostrarCampoOtro',
    TipoExplicacion = 'Normal',
    Orden = 2
  ORDER BY
    Orden, Texto

END



