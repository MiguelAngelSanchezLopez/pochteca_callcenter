﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Local')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Local'
  DROP  Procedure  dbo.spu_Combo_Local
END

GO

PRINT  'Creating Procedure spu_Combo_Local'
GO
CREATE Procedure dbo.spu_Combo_Local
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Local"
**    Autor        : VSR
**    Fecha        : 15/10/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @mostrarTodosLocales INT
  
  -- obtiene la llave del perfil del usuario conectado
  DECLARE @llavePerfilUsuarioConectado VARCHAR(255)
  SET @llavePerfilUsuarioConectado = (SELECT P.Llave FROM Usuario AS U INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id) WHERE U.Id = @Filtro)
  IF (@llavePerfilUsuarioConectado IS NULL) SET @llavePerfilUsuarioConectado = '-1'

  SET @mostrarTodosLocales = CASE WHEN EXISTS(
  	                                           SELECT
                                                 PPF.LlavePerfil
                                               FROM
                                                 vwu_PerfilesPorFormato AS PPF
                                               WHERE
                                                   PPF.Tipo = 'Local' 
                                               AND PPF.LlavePerfil = @llavePerfilUsuarioConectado
                                       ) THEN 0 ELSE 1
                             END

  --------------------------------------------------------------------------------
  -- obtiene locales filtrados segun el perfil del usuario conectado
  --------------------------------------------------------------------------------
  SELECT
    Texto = L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,L.CodigoInterno),''),
    Valor = L.CodigoInterno,
    FocoTablet = CONVERT(INT, ISNULL(L.FocoTablet,0))
  FROM
    Local AS L
  WHERE
      L.Activo = 1
  AND (
           @mostrarTodosLocales = 1
        OR ( L.Id IN (SELECT LPU.IdLocal FROM LocalesPorUsuario AS LPU WHERE LPU.IdUsuario = @Filtro ) )
      )
  ORDER BY
    Texto

END



