﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ExportacionNombrePuertoDescarga')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ExportacionNombrePuertoDescarga'
  DROP  Procedure  dbo.spu_Combo_ExportacionNombrePuertoDescarga
END

GO

PRINT  'Creating Procedure spu_Combo_ExportacionNombrePuertoDescarga'
GO
CREATE Procedure dbo.spu_Combo_ExportacionNombrePuertoDescarga
/******************************************************************************
**    Descripcion  : obtiene NombrePuertoDescarga de tabla OS_Exportacion
**    Autor        : VSR
**    Fecha        : 26/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = E.NombrePuertoDescarga,
    Valor = E.NombrePuertoDescarga
  FROM
    OS_Exportacion AS E
  WHERE
    ISNULL(E.NombrePuertoDescarga,'') <> ''
  ORDER BY
    Texto

END



