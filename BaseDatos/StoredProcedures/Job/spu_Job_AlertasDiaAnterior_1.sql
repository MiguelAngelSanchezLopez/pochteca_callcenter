﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_AlertasDiaAnterior')
BEGIN
  PRINT  'Dropping Procedure spu_Job_AlertasDiaAnterior'
  DROP  Procedure  dbo.spu_Job_AlertasDiaAnterior
END

GO

PRINT  'Creating Procedure spu_Job_AlertasDiaAnterior'
GO
CREATE Procedure dbo.spu_Job_AlertasDiaAnterior
/******************************************************************************
**    Descripcion  : obtiene listado de alertas del dia anterior
*******************************************************************************/
AS
BEGIN

	select 
		[Id Viaje]= a.id
		,[Tipo de Viaje] = (select top 1 tipoviaje from trazaviaje where nrotransporte = a.nroTransporte)
		,[Línea Transportista] = upper(a.NombreTransportista)
		,[Tracto] = a.PatenteTracto
		,[Económico] = isnull(tm.economico,'')
		,[Operador/Conductor] = upper(a.NombreConductor)
		,[Creación de Alerta]= case when haca.idalerta is null then isnull(convert(varchar(10),a.FechaHoraCreacion,103) + ' ' + convert(varchar(8),a.FechaHoraCreacion,108)  ,'')
									else
										isnull(convert(varchar(10),haca.FechaHoraCreacion,103) + ' ' + convert(varchar(8),haca.FechaHoraCreacion,108)  ,'')
								end
		,[Atención de Alerta]= isnull(convert(varchar(10),haca.FechaAsignacion,103) + ' ' + convert(varchar(8),haca.FechaAsignacion,108)  ,'')
		,[Nombre de Alerta] = a.DescripcionAlerta
		,[Atendido por] = isnull(upper(u.Nombre + ' ' + isnull(u.paterno,'')),'')
		,[Se Contactó] = isnull(upper(u1.Nombre + ' ' + isnull(u1.paterno,'')),'')
		,[Explicación] = isnull(upper(chec.explicacion),'')
		,[Observación] = isnull(upper(chec.observacion),'')
	from alerta a
	left join HistorialAlertaEnColaAtencion haca on a.id = haca.idalerta
	left join usuario u on haca.idusuarioasignada = u.id --Usuario creacion
	left join callcenterhistorialescalamiento che on a.id = che.idAlerta
	left join CallCenterHistorialEscalamientocontacto chec on che.id =  chec.idHistorialEscalamiento
	left join usuario u1 on chec.idUsuarioSinGrupoContacto = u1.id --Usuario contactado
	left join Track_Movil tm on a.PatenteTracto = tm.patente
	where convert(varchar(10),DATEADD(day,-1,dbo.fnu_getdate()),103) = convert(varchar(10),a.fechahoracreacion,103)
END

