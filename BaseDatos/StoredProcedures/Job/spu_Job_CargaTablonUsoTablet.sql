﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_CargaTablonUsoTablet')
BEGIN
  PRINT  'Dropping Procedure spu_Job_CargaTablonUsoTablet'
  DROP  Procedure  dbo.spu_Job_CargaTablonUsoTablet
END

GO

PRINT  'Creating Procedure spu_Job_CargaTablonUsoTablet'
GO
CREATE Procedure dbo.spu_Job_CargaTablonUsoTablet
/******************************************************************************
**    Descripcion  : carga los nuevos viajes y obtiene el listado de la carga
**    Por          : VSR, 27/01/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @UltimoIdTrazaViaje INT
  SELECT @UltimoIdTrazaViaje = MAX(IdTrazaViaje) FROM TablonUsoTablet

  INSERT TablonUsoTablet
  (
	  IdTrazaViaje,
	  NroTransporte,
	  FHSalidaOrigen,
	  CDOrigenCodigo,
	  CDOrigenDescripcion,
    CDFoco,
	  LocalDestinoCodigo,
	  LocalDestinoDescripcion,
	  Etapa,
	  Formato,
	  Region,
	  RegionNumero,
	  NumeroLocal,
	  AuditadoEnCD,
	  AuditadoEnLocal,
	  IdFormularioCarga,
	  NumeroDespacho,
	  Patente,
	  Sello,
	  FechaCarga,
	  IdFormularioRecepcion,
	  NumeroEnvio,
    CargaConReclamo,
    FechaRecepcion
  )
  SELECT
    IdTrazaViaje = TV.Id,
    NroTransporte = TV.NroTransporte,
    FHSalidaOrigen = TV.FHSalidaOrigen,
    CDOrigenCodigo = TV.OrigenCodigo,
    CDOrigenDescripcion = TV.OrigenDescripcion,
    CDFoco = CONVERT(INT,ISNULL(CD.FocoAlto,0)),
    LocalDestinoCodigo = TV.LocalDestino,
    LocalDestinoDescripcion = L.Nombre,
    Etapa = L.Etapa,
    Formato = L.DescripcionFormato,
    Region = L.RegionLocal,
    RegionNumero = L.NumeroRegionLocal,
    NumeroLocal = L.NumeroLocal,
    AuditadoEnCD = CASE WHEN (
  	                          SELECT
  	                            COUNT(aux.Id_Formulario)
  	                          FROM
  	                            FRMCargaWM AS aux
  	                          WHERE
  	                              aux.Fecha_Carga = LEFT(CONVERT(VARCHAR,CONVERT(DATETIME,LEFT(TV.FHSalidaOrigen,10)),120),10)
  	                          AND aux.Local_Destino = TV.LocalDestino
  	                          AND aux.Cd_Origen = TV.OrigenCodigo
  	                          AND TV.PatenteTracto LIKE '%' + UPPER(REPLACE(REPLACE(aux.Patente,'-',''),' ','')) + '%'
                    ) > 0 THEN 1 ELSE 0 END,
    AuditadoEnLocal = CASE WHEN (
  	                          SELECT
  	                            COUNT(aux.Id_Formulario)
  	                          FROM
  	                            FRMRecepcionWM AS aux
  	                          WHERE
  	                              aux.Hoja_Ruta = TV.NroTransporte
  	                          AND aux.Local_Destino = TV.LocalDestino
  	                          AND aux.Cd_Origen = TV.OrigenCodigo
                      ) > 0 THEN 1 ELSE 0 END,
    IdFormularioCarga = ISNULL(C.Id_Formulario,-1),
    NumeroDespacho = ISNULL(C.Num_Despacho,-1),
    Patente = ISNULL(C.Patente,''),
    Sello = ISNULL(C.Codigo_Sello_Fisico,''),
    FechaCarga = ISNULL(CONVERT(VARCHAR,CONVERT(DATETIME,REPLACE(C.Fecha_Carga,'-','')),103),''),
    IdFormularioRecepcion = ISNULL(R.Id_Formulario,-1),
    NumeroEnvio = ISNULL(R.Numero_Envio,-1),
    CargaConReclamo = ISNULL(R.Carga_Con_Problemas,''),
    FechaRecepcion = ISNULL(CONVERT(VARCHAR,CONVERT(DATETIME,REPLACE(R.Fecha_Recepcion,'-','')),103),'')
  FROM
    TrazaViaje AS TV
    INNER JOIN Local AS L ON (TV.LocalDestino = L.CodigoInterno)
    LEFT JOIN FRMCargaWM AS C ON (C.Fecha_Carga = LEFT(CONVERT(VARCHAR,CONVERT(DATETIME,LEFT(TV.FHSalidaOrigen,10)),120),10)
  	                              AND C.Local_Destino = TV.LocalDestino
  	                              AND C.Cd_Origen = TV.OrigenCodigo
                                  AND TV.PatenteTracto LIKE '%' + UPPER(REPLACE(REPLACE(C.Patente,'-',''),' ','')) + '%')
    LEFT JOIN FRMRecepcionWM AS R ON (R.Hoja_Ruta = TV.NroTransporte
  	                                  AND R.Local_Destino = TV.LocalDestino
  	                                  AND R.Cd_Origen = TV.OrigenCodigo)
  	LEFT JOIN CentroDistribucion AS CD ON (TV.OrigenCodigo = CD.CodigoInterno)  	
    LEFT JOIN EtapaLocalesFocoWM elfw ON elfw.Etapa = l.etapa
  WHERE
      CONVERT(VARCHAR,TV.FechaHoraCreacion,112) >= '20140916'
  AND TV.EstadoViaje = 'RUTA'
  AND L.FocoTablet = 1
  AND TV.Id > @UltimoIdTrazaViaje
  ORDER BY
    IdTrazaViaje    

  ---------------------------------------------------------
  -- TABLA 0: Devuelve los ultimos registros ingresados
  ---------------------------------------------------------
  SELECT * FROM TablonUsoTablet WHERE IdTrazaViaje > @UltimoIdTrazaViaje ORDER BY IdTrazaViaje


END
