﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_AlertasDiaAnterior')
BEGIN
  PRINT  'Dropping Procedure spu_Job_AlertasDiaAnterior'
  DROP  Procedure  dbo.spu_Job_AlertasDiaAnterior
END

GO

PRINT  'Creating Procedure spu_Job_AlertasDiaAnterior'
GO
CREATE Procedure dbo.spu_Job_AlertasDiaAnterior
/******************************************************************************
**    Descripcion  : obtiene listado de alertas gestionadas el dia anterior
**    Por          : VSR, 21/10/2014
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = '20140101'
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = '21001231'

  SELECT
    [ID ALERTA] = T.IdAlerta,
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaCreacion, 'FECHA_COMPLETA'),
    ALERTA = T.NombreAlerta,
    [IDMASTER] = T.NroTransporte, 
    PRIORIDAD = T.Prioridad, 
    [LINEA DE TRANSPORTE] = T.NombreTransportista,
    [TIENDA DESTINO] = T.LocalDestino + CASE WHEN T.LocalDestinoCodigo = '' THEN '' ELSE ' - ' + T.LocalDestinoCodigo END,
    ZONA = T.Zona, 
    [TIPO VIAJE] = T.TipoViaje, 
    [ESCALAMIENTO NRO] = CASE WHEN T.NroEscalamiento IS NULL THEN 'Supervisor' ELSE T.NroEscalamiento END,
    [CONTACTO EFECTIVO EN EL ESCALAMIENTO] = CASE WHEN T.NoContesta = 1 THEN 'NO' ELSE 'SI' END,
    [ESCALAMIENTO ATENDIDO POR] = T.AtendidoPor, 
    [ESCALAMIENTO FECHA INICIO GESTION] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaInicioGestion,'FECHA_COMPLETA'),''),
    [ESCALAMIENTO FECHA TERMINO GESTION] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaTerminoGestion,'FECHA_COMPLETA'),''),
    [ESCALAMIENTO MINUTOS GESTION] = ISNULL(T.MinutosGestion,-1),
    [CARGO CONTACTO] = T.GrupoContacto,
    [NOMBRE CONTACTO] = T.NombreContacto,
    EXPLICACION = T.Explicacion,
    Observacion = T.Observacion
  FROM (
        SELECT DISTINCT
          IdAlerta = A.Id,
          NombreAlerta = A.DescripcionAlerta,
          NroTransporte = A.NroTransporte,
          Prioridad = AC.Prioridad,
          NombreTransportista = ISNULL(A.NombreTransportista,''),
          LocalDestino = ISNULL(L.Nombre,''),
          LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
          Zona = ISNULL(A.Permiso,''),
          TipoViaje = ISNULL(TV.TipoViaje,''),
          AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
          FechaCreacion = A.FechaHoraCreacion,
          NroEscalamiento = CONVERT(VARCHAR,EPA.Orden),
          FechaInicioGestion = AGA.FechaAsignacion,
          FechaTerminoGestion = AGA.FechaTerminoGestion,
          MinutosGestion = DATEDIFF(mi,AGA.FechaAsignacion,AGA.FechaTerminoGestion),
          IdHistorialeEcalamientoContacto = HEC.Id,
          GrupoContacto = CASE WHEN HEC.IdEscalamientoPorAlertaContacto IS NULL THEN UPPER(P.Nombre) ELSE UPPER(EPAGC.Nombre) END,
          NombreContacto = CASE WHEN HEC.IdEscalamientoPorAlertaContacto IS NULL THEN
                             ISNULL(USinGrupo.Nombre,'') + ISNULL(' ' + USinGrupo.Paterno,'') + ISNULL(' ' + USinGrupo.Materno,'')
                           ELSE
       	                     ISNULL(UConGrupo.Nombre,'') + ISNULL(' ' + UConGrupo.Paterno,'') + ISNULL(' ' + UConGrupo.Materno,'')
                           END,
          Explicacion = ISNULL(HEC.Explicacion,''),
          Observacion = ISNULL(HEC.Observacion,''),
          NoContesta = NC.NoContesta                      
        FROM
          CallCenterHistorialEscalamiento AS HE
          INNER MERGE JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
          INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
          INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
          INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
          INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
          INNER JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
          INNER JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
          INNER JOIN CallCenterHistorialEscalamientoContacto AS HEC ON (HE.Id = HEC.IdHistorialEscalamiento)
          LEFT JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
          LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
          LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (HEC.IdEscalamientoPorAlertaContacto = EPAC.Id)
          LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPAC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
          LEFT JOIN Usuario AS UConGrupo ON (EPAC.IdUsuario = UConGrupo.Id)
          LEFT JOIN Usuario AS USinGrupo ON (HEC.IdUsuarioSinGrupoContacto = USinGrupo.Id)
          LEFT JOIN Perfil AS P ON (USinGrupo.IdPerfil = P.Id)
          LEFT MERGE JOIN vwu_NoContesta AS NC ON (A.Id = NC.IdAlerta)  
          LEFT JOIN CallCenterAlertaGestionAcumulada AS AGA ON (HE.Id = AGA.IdHistorialEscalamiento)
        WHERE
            CONVERT(VARCHAR,A.FechaHoraCreacion,112) BETWEEN @FechaISOInicio AND @FechaISOTermino
        AND CCA.Desactivada = 0
  ) AS T
  ORDER BY
    T.FechaCreacion, T.IdAlerta, T.IdHistorialeEcalamientoContacto

END