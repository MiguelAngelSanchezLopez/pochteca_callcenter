﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_GrabarViajesWT')
BEGIN
  PRINT  'Dropping Procedure spu_Job_GrabarViajesWT'
  DROP  Procedure  dbo.spu_Job_GrabarViajesWT
END

GO

PRINT  'Creating Procedure spu_Job_GrabarViajesWT'
GO
CREATE Procedure dbo.spu_Job_GrabarViajesWT    
/******************************************************************************    
**  Descripcion  : graba los datos del local    
**  Fecha        : 04/09/2014    
*******************************************************************************/    
@NUMTRANSPORTE AS VARCHAR(500),    
@PATENTE_TRACTO AS VARCHAR(7),    
@PATENTE_TRAILER AS VARCHAR(7),    
@PLOCALES AS VARCHAR(250),    
@SECUENCIA AS VARCHAR(250),    
@FECHALLEGADA AS VARCHAR(16),    
@FECHASALIDA AS VARCHAR(16),    
@MERCADERIA AS VARCHAR(250),    
@MINIMO AS VARCHAR(250),    
@MAXIMO AS VARCHAR(250),    
@MINUTOS AS VARCHAR(250),    
@FILENAME AS VARCHAR(250)    
AS    
BEGIN    
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
  SET NOCOUNT ON    
    
 DECLARE @RUTTRANSPORTISTA VARCHAR(10),    
   @NOMBRETRANSPORTISTA VARCHAR(50),    
   @RUTCONDUCTOR VARCHAR(10),    
   @NOMBRECONDUCTOR VARCHAR(50),    
   @TIPOVIAJE VARCHAR(255)    
     
  BEGIN TRANSACTION    
       
 SELECT TOP 1
   @RUTTRANSPORTISTA = U.Rut + '-' + U.DV    
  ,@NOMBRETRANSPORTISTA = u.Nombre  
  ,@RUTCONDUCTOR =  pt.RutConductor  
  ,@NOMBRECONDUCTOR = pt.NombreConductor    
  ,@TIPOVIAJE = pt.Carga    
 FROM usuario u  
 INNER JOIN PlanificacionTransportista pt ON pt.IdUsuarioTransportista = u.Id  
 WHERE pt.NroTransporte = @NUMTRANSPORTE    
  
 INSERT INTO ViajesWT    
 (    
  NUMTRANSPORTE,    
  PATENTE_TRACTO,    
  PATENTE_TRAILER,    
  PLOCALES,    
  SECUENCIA,    
  FECHALLEGADA,    
  FECHASALIDA,    
  MERCADERIA,    
  MINIMO,    
  MAXIMO,    
  MINUTOS,    
  RUTTRANSPORTISTA,    
  NOMBRETRANSPORTISTA,    
  RUTCONDUCTOR,    
  NOMBRECONDUCTOR,    
  TIPOVIAJE,    
  FILENAME    
 )    
 VALUES    
 (    
  @NUMTRANSPORTE,    
  @PATENTE_TRACTO,    
  @PATENTE_TRAILER,    
  @PLOCALES,    
  @SECUENCIA,    
  @FECHALLEGADA,    
  @FECHASALIDA,    
  @MERCADERIA,    
  @MINIMO,    
  @MAXIMO,    
  @MINUTOS,    
  @RUTTRANSPORTISTA,    
  @NOMBRETRANSPORTISTA,    
  @RUTCONDUCTOR,    
  @NOMBRECONDUCTOR,    
  @TIPOVIAJE,    
  @FILENAME    
 )    
       
      IF @@ERROR<>0 BEGIN    
        ROLLBACK    
        RETURN    
      END    
    
  COMMIT    
END 