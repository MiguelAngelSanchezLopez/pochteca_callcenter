﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_ControlPlanificacionDiaAnterior')
BEGIN
  PRINT  'Dropping Procedure spu_Job_ControlPlanificacionDiaAnterior'
  DROP  Procedure  dbo.spu_Job_ControlPlanificacionDiaAnterior
END

GO

PRINT  'Creating Procedure spu_Job_ControlPlanificacionDiaAnterior'
GO
CREATE Procedure dbo.spu_Job_ControlPlanificacionDiaAnterior
/******************************************************************************
**    Descripcion  : obtiene listado de planificacion del dia anterior con su respectiva validacion
**    Por          : VSR, 15/07/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = CONVERT(VARCHAR,dbo.fnu_GETDATE(),112)
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = CONVERT(VARCHAR,dbo.fnu_GETDATE(),112)

  SELECT
    [PLANIFICACION FECHA] = T.FechaPresentacion,
    [PLANIFICACION IDMASTER] = T.NroTransporte,
    [PLANIFICACION PLACA TRACTO] = T.PatenteTracto,
    [PLANIFICACION PLACA REMOLQUE] = T.PatenteTrailer,
    [FTP NUMERO TRANSPORTE] = T.FTPNumeroTransporte,
    [FTP PLACA TRACTO] = T.FTPPatenteTracto,
    [FTP PLACA REMOLQUE] = T.FTPPatenteTrailer,
    [VALIDACION] = CASE
                     WHEN (T.NroTransporte = T.FTPNumeroTransporte) AND (T.PatenteTracto = T.FTPPatenteTracto) AND (T.PatenteTrailer = T.FTPPatenteTrailer) THEN 'CORRECTO' 
                   ELSE
                 	    'INCORRECTO'
                   END
  FROM (
        SELECT DISTINCT
          FechaPresentacion = CONVERT(VARCHAR,PT.FechaPresentacion,103),
          NroTransporte = PT.NroTransporte,
          PatenteTracto = PT.PatenteTracto,
          PatenteTrailer = PT.PatenteTrailer,
          FTPNumeroTransporte = ISNULL(v.NUMTRANSPORTE,''),
          FTPPatenteTracto = REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(v.PATENTE_TRACTO,''),' ',''),'/',''),'-',''),'.',''),
          FTPPatenteTrailer = REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(v.PATENTE_TRAILER,''),' ',''),'/',''),'-',''),'.','')
        FROM
          PlanificacionTransportista AS PT
          LEFT JOIN ViajesWT AS V ON (PT.NroTransporte = V.NUMTRANSPORTE)
        WHERE
          CONVERT(VARCHAR,PT.FechaPresentacion,112) BETWEEN @FechaISOInicio AND @FechaISOTermino
  ) AS T
  ORDER BY 
    [PLANIFICACION IDMASTER]


END