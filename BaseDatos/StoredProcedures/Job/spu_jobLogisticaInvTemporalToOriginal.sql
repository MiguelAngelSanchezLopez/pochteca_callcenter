/**
**  Autor		:	Mauricio Espinoza A.
**	Fecha		:	10-02-2017
**	Descripción	:	Inserta los registros nuevos de tracto
**/
CREATE PROCEDURE [dbo].[spu_jobLogisticaInvTemporalToOriginal]
AS
Begin
	Begin try
		INSERT INTO InformacionLogisticaInv (
			FOLIO,
			FECHA_SOLICITADO,
			DETERMINANTE,
			PLACA_REMOLQUE,
			PLACA_TRACTOR,
			MARCHAMO_1,
			MARCHAMO_2,
			RECORRIDO,
			LT
			--,TARIMAS
			--,DONACION 
			--,RPC 
			--,CARTON 
			--,PLAYO 
			--,PAPEL 
			--,REJILLA 
			--,BIDONES 
			--,CUBETAS  
			--,METALES 
			--,LONA 
			--,CELOFAN 
			--,MADERA 
			--,GANCHO 
			--,USUARIO_TORRE  
		)
		SELECT  distinct
			  t.FOLIO,
			  convert(datetime,replace(Replace(t.[FECHA_SOLICITADO], 'a.m.', 'am'), 'p.m.', 'pm')),
			  t.DETERMINANTE,
			  UPPER(REPLACE(REPLACE(t.PLACA_REMOLQUE, '-',''), '/', '')),
			  UPPER(REPLACE(REPLACE(t.PLACA_TRACTOR, '-',''), '/', '')),
			  t.MARCHAMO_1,
		      t.MARCHAMO_2,
			  t.RECORRIDO,
			  t.LT
			  --,t.TARIMAS
			--,t.DONACION 
			--,t.RPC 
			--,t.CARTON 
			--,t.PLAYO 
			--,t.PAPEL 
			--,t.REJILLA 
			--,t.BIDONES 
			--,t.CUBETAS  
			--,t.METALES 
			--,t.LONA 
			--,t.CELOFAN 
			--,t.MADERA 
			--,t.GANCHO 
			--,t.USUARIO_TORRE
		FROM 
			InformacionLogisticaInvTemporal t LEFT JOIN 
			InformacionLogisticaInv r On
				t.FOLIO    = r.FOLIO AND				
				IsNull(t.MARCHAMO_1, -1) = IsNull(r.MARCHAMO_1, -1) AND
				Isnull(t.MARCHAMO_2, -1) = Isnull(r.MARCHAMO_2, -1) 
		Where
			r.FOLIO Is Null
	End try
	begin catch
		insert into Track_Error ( 
			Fecha,
			NombreObjeto,
			MensajeError
		)
		Values (
			dbo.fnu_GETDATE(),
			'spu_jobLogisticaInvTemporalToOriginal',
			'Error: ' + error_message()
		)
	end catch	
		
	truncate table InformacionLogisticaInvTemporal
	


End