IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_ObtenerPaginasAsociadas')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_ObtenerPaginasAsociadas'
  DROP  Procedure  dbo.spu_Perfil_ObtenerPaginasAsociadas
END

GO

PRINT  'Creating Procedure spu_Perfil_ObtenerPaginasAsociadas'
GO
CREATE Procedure dbo.spu_Perfil_ObtenerPaginasAsociadas
/******************************************************************************
**    Descripcion  : Obtiene listado de paginas asocidas al perfil
**    Por          : VSR, 28/07/2009
*******************************************************************************/
@IdPerfil AS INT,
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  /*****************************************************************************
  * TABLA 0 = LISTADO CATEGORIAS
  *****************************************************************************/
  SELECT DISTINCT
    IdPerfil = PE.Id,
    NombrePerfil = ISNULL(PE.Nombre,''),
    IdCategoria = CP.Id,
    NombreCategoria = ISNULL(CP.Nombre,''),
    OrdenCategoria = ISNULL(CP.Orden,0)
  FROM
    PaginasXPerfil AS PxP
    LEFT JOIN Perfil AS PE ON (PxP.IdPerfil = PE.Id)
    LEFT JOIN Pagina AS P ON (PxP.IdPagina = P.Id)
    LEFT JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)
  WHERE
    (@IdPerfil= -1 Or PxP.IdPerfil = @IdPerfil)
  ORDER BY
    NombrePerfil, OrdenCategoria

  /*****************************************************************************
  * TABLA 1 = LISTADO PAGINAS
  *****************************************************************************/
  SELECT DISTINCT
    IdPerfil = PE.Id,
    NombrePerfil = ISNULL(PE.Nombre,''),
    IdCategoria = ISNULL(CP.Id,0),
    NombreCategoria = ISNULL(CP.Nombre,''),
    OrdenCategoria = ISNULL(CP.Orden,0),
    IdPagina = ISNULL(P.Id,0),
    NombreArchivo = ISNULL(P.NombreArchivo,''),
    TituloPagina = ISNULL(P.Titulo,''),
    MostrarEnMenu = ISNULL(P.MostrarEnMenu,0),
    OrdenPagina = ISNULL(P.Orden,0)
  FROM
    PaginasXPerfil AS PxP
    LEFT JOIN Perfil AS PE ON (PxP.IdPerfil = PE.Id)
    LEFT JOIN Pagina AS P ON (PxP.IdPagina = P.Id)
    LEFT JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)
  WHERE
    (@IdPerfil= -1 Or PxP.IdPerfil = @IdPerfil)
  ORDER BY
    NombrePerfil, OrdenCategoria, OrdenPagina

  /*****************************************************************************
  * TABLA 2 = LISTADO ACCIONES PAGINA
  *****************************************************************************/
  SELECT DISTINCT
    IdPerfil = PE.Id,
    NombrePerfil = ISNULL(PE.Nombre,''),
    IdCategoria = ISNULL(CP.Id,0),
    NombreCategoria = ISNULL(CP.Nombre,''),
    OrdenCategoria = ISNULL(CP.Orden,0),
    IdPagina = ISNULL(P.Id,0),
    NombreArchivo = ISNULL(P.NombreArchivo,''),
    TituloPagina = ISNULL(P.Titulo,''),
    MostrarEnMenu = ISNULL(P.MostrarEnMenu,0),
    OrdenPagina = ISNULL(P.Orden,0),
    IdFuncion = ISNULL(FP.Id,0),
    NombreFuncion = ISNULL(FP.Nombre,''),
    LlaveIdentificador = ISNULL(FP.LlaveIdentificador,''),
    PerteneceFuncion = ISNULL((
                              SELECT
                                CASE WHEN (FPxU.IdFuncionPagina>0) THEN 1 ELSE 0 END
                              FROM
                                FuncionesPaginaxUsuario AS FPxU
                              WHERE
                                  (FPxU.IdFuncionPagina = ISNULL(FP.Id,0))
                              AND (FPxU.IdUsuario = @IdUsuario))
                              ,0),
    OrdenFuncion = ISNULL(FP.Orden,0)
  FROM
    PaginasXPerfil AS PxP
    LEFT JOIN Perfil AS PE ON (PxP.IdPerfil = PE.Id)
    LEFT JOIN Pagina AS P ON (PxP.IdPagina = P.Id)
    LEFT JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)
    LEFT JOIN FuncionPagina AS FP ON (P.Id = FP.IdPagina)
  WHERE
    (@IdPerfil= -1 Or PxP.IdPerfil = @IdPerfil)
  ORDER BY
    NombrePerfil, OrdenCategoria, OrdenPagina, OrdenFuncion
END



  