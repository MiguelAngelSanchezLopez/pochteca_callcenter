IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_ObtenerPorId'
  DROP  Procedure  dbo.spu_Perfil_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_Perfil_ObtenerPorId'
GO
CREATE Procedure dbo.spu_Perfil_ObtenerPorId
/******************************************************************************
**  Descripcion  : obtiene informacion por su id
**  Fecha        : 23/07/2009
*******************************************************************************/
@Id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    *
  FROM
    Perfil
  WHERE
    Id = @Id

END
GO    