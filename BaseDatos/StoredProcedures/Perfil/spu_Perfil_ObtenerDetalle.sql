﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Perfil_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Perfil_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Perfil_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle del perfil consultado
**    Por          : VSR, 06/04/2015
*******************************************************************************/
@IdPerfil AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  /*****************************************************************************
  * TABLA 0: DETALLE
  *****************************************************************************/
  SELECT
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    Llave = ISNULL(P.Llave,''),
	  PaginaInicio = ISNULL(P.PaginaInicio,'')
  FROM
    Perfil AS P
  WHERE
    P.Id = @IdPerfil


END



   