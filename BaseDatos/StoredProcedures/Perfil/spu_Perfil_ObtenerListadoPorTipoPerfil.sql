IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_ObtenerListadoPorTipoPerfil')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_ObtenerListadoPorTipoPerfil'
  DROP  Procedure  dbo.spu_Perfil_ObtenerListadoPorTipoPerfil
END

GO

PRINT  'Creating Procedure spu_Perfil_ObtenerListadoPorTipoPerfil'
GO
CREATE Procedure dbo.spu_Perfil_ObtenerListadoPorTipoPerfil
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo perfil
**    Fecha        : 28/07/2009
*******************************************************************************/
@LlavePerfil AS VARCHAR(50)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Valor = P.Id,
    Texto = RTRIM(LTRIM(P.Nombre))
  FROM
    Perfil AS P
  WHERE
      (1 = 1)
  AND (
        ( @LlavePerfil = '-1' OR @LlavePerfil = 'ADM' )
        OR
        ( @LlavePerfil = 'CON' AND P.Llave IN ('CON') )
        OR
        ( @LlavePerfil = 'TO' AND P.Llave IN ('TO') )
        OR
        ( @LlavePerfil = 'SUP' AND P.Llave IN ('TO','SPA','CLT') )
      )
  ORDER BY
    P.Nombre
END