IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Configuracion_Escribir')
BEGIN
  PRINT  'Dropping Procedure spu_Configuracion_Escribir'
  DROP  Procedure  dbo.spu_Configuracion_Escribir
END

GO

PRINT  'Creating Procedure spu_Configuracion_Escribir'
GO

CREATE Procedure [dbo].[spu_Configuracion_Escribir] (  
@Clave varchar(50),  
@Valor varchar(1000)  
) AS  
BEGIN  
   
 IF(EXISTS(SELECT Valor FROM Configuracion WHERE Clave = @Clave)) BEGIN
   UPDATE Configuracion SET  
   Valor = @Valor  
   WHERE Clave = @Clave  
 END
 ELSE BEGIN
  INSERT Configuracion
  (
	  Clave,
	  Valor
  )
  VALUES
  (
	  @Clave,
	  @Valor
  )
 END
   
END   
GO   