﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Formato_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Formato_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Formato_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle del formato consultado
**    Por          : VSR, 07/04/2015
*******************************************************************************/
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @PerfilesNoMostrar VARCHAR(8000)
  SET @PerfilesNoMostrar = 'ADM,CON,TO,SUP,GEN,TRA'

  ----------------------------------------------------------------
  -- TABLA 0: DETALLE
  ----------------------------------------------------------------
  SELECT
	  IdFormato = F.Id,
	  NombreFormato = ISNULL(F.Nombre,''),
	  Llave = ISNULL(F.Llave,''),
	  Activo = CONVERT(INT, F.Activo),
    TieneRegistrosAsociados = CASE WHEN (
                                (SELECT COUNT(aux.IdFormato) FROM PerfilesPorFormato AS aux WHERE aux.IdFormato = F.Id) +
                                (SELECT COUNT(aux.IdFormato) FROM Local AS aux WHERE aux.IdFormato = F.Id)
                              ) = 0 THEN '0' ELSE '1' END
  FROM
    Formato AS F
  WHERE
    F.Id = @IdFormato

  ----------------------------------------------------------------
  -- TABLA 1: Perfiles por Local
  ----------------------------------------------------------------
  SELECT
    Texto = ISNULl(P.Nombre,''),
    Valor = P.Id,
    Seleccionado = CASE WHEN PPF.IdPerfil IS NOT NULL THEN 1 ELSE 0 END
  FROM
    Perfil AS P
    LEFT JOIN PerfilesPorFormato AS PPF ON (P.Id = PPF.IdPerfil AND PPF.IdFormato = @IdFormato AND PPF.Categoria = 'Local')
  WHERE
    P.Llave NOT IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@PerfilesNoMostrar))
  ORDER BY
    Texto

END
