﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_CentroDistribucion_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_CentroDistribucion_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_CentroDistribucion_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_CentroDistribucion_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_CentroDistribucion_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de centro distribucion usando filtros de busqueda
**    Por          : VSR, 04/09/2014
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(255),
@CodigoInterno AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdCentroDistribucion = CD.Id,
    NombreCentroDistribucion = ISNULL(CD.Nombre,''),
    CodigoInterno = ISNULL(CD.CodigoInterno,''),
	  FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(CD.FechaCreacion,'FECHA_COMPLETA'),
    Activo = CASE WHEN ISNULL(CD.Activo,0) = 1 THEN 'SI' ELSE 'NO' END
  FROM
    CentroDistribucion AS CD
  WHERE
      (@Nombre = '-1' OR CD.Nombre LIKE '%'+ @Nombre +'%')
  AND (@CodigoInterno = -1 OR CD.CodigoInterno LIKE '%'+ CONVERT(VARCHAR,@CodigoInterno) +'%')
  ORDER BY
    IdCentroDistribucion DESC

END



  