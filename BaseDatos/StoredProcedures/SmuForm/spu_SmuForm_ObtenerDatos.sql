﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_SmuForm_ObtenerDatos')
BEGIN
  PRINT  'Dropping Procedure spu_SmuForm_ObtenerDatos'
  DROP  Procedure  dbo.spu_SmuForm_ObtenerDatos
END

GO

PRINT  'Creating Procedure spu_SmuForm_ObtenerDatos'
GO
CREATE Procedure dbo.spu_SmuForm_ObtenerDatos
/****************************************************************************************
**    Descripcion  : obtiene los registros de las tabla prontoform para carga y recepcion
**    Por          : DG, 18/06/2015
*****************************************************************************************/
AS
	DECLARE @SmuFormCarga AS TABLE(
		 IdFormulario INT
		,NumTransporte VARCHAR(250)
		,LocalDestino VARCHAR(250)
		,NombreLocalDestino VARCHAR(250)
		,FechaEnvioFormulario VARCHAR(10)
		,Auditado INT		
		,CargadorUsuario VARCHAR(250)
		,Tipo VARCHAR(250)
	)
	
	
	DECLARE @SmuFormRecepcion AS TABLE(
		 IdFormulario INT
		,NumTransporte VARCHAR(250)
		,LocalDestino VARCHAR(250)
		,NombreLocalDestino VARCHAR(250)
		,FechaEnvioFormulario VARCHAR(10)
		,Auditado INT
		,CargadorUsuario VARCHAR(250)		
		,Tipo VARCHAR(250)
	)
	
	-- =======================================================================
	-- CARGAR DATOS
	-- =======================================================================
	INSERT INTO @SmuFormCarga
		SELECT 
			 C.IDENTIFIER
			,CASE WHEN P2.NT IS NULL THEN P2.OPCIO ELSE P2.NT END
			,P2.LD
			,P2.NLD
			,CONVERT(VARCHAR(10),CONVERT(DATETIME,C.SHIFTEDDEVICESUBMITDATE,120),103)
			,1
			,P1.USERC
			,P1.TIPO
		FROM SMUFormCargaCabecera C
		INNER JOIN SMUFormCargaPagina1 P1 ON P1.IDENTIFIER = C.IDENTIFIER
		INNER JOIN SMUFormCargaPagina2 P2 ON P2.IDENTIFIER = C.IDENTIFIER
	
	INSERT INTO @SmuFormRecepcion
		SELECT 
			 C.IDENTIFIER
			,CASE WHEN P2.NT IS NULL THEN P2.OPCIO ELSE P2.NT END		
			,P2.LD
			,P2.NLD
			,CONVERT(VARCHAR(10),CONVERT(DATETIME,C.SHIFTEDDEVICESUBMITDATE,120),103)
			,1
			,P1.NOMR
			,''
		FROM SMUFormRecepcionCabecera C
		INNER JOIN SMUFormRecepcionPagina1 P1 ON P1.IDENTIFIER = C.IDENTIFIER
		INNER JOIN SMUFormRecepcionPagina2 P2 ON P2.IDENTIFIER = C.IDENTIFIER
		WHERE 
			--Formularios que quedan fuera del panel por presentar problemas
			C.IDENTIFIER NOT IN (1823873061,1823868937,1823680681,1823930113)
			
	-- =======================================================================
	-- LISTAR DATOS
	-- =======================================================================
	SELECT 
		 IdViajeCarga = SFC.NumTransporte
		,IdViajeRecepcion = SFR.NumTransporte
		,AuditadoCD = ISNULL(SFC.Auditado,'')
		,AuditadoLocal = ISNULL(SFR.Auditado,'')
		,FormularioCarga = SFC.IdFormulario
		,FormularioRecepcion = SFR.IdFormulario
		,CargaYRecepcion = CASE WHEN SFC.NumTransporte IS NOT NULL THEN SFC.NumTransporte + SFC.LocalDestino  ELSE SFR.NumTransporte + SFR.LocalDestino  END
		,FechaCargaYRecepcion = CASE WHEN SFC.FechaEnvioFormulario IS NOT NULL THEN SFC.FechaEnvioFormulario ELSE SFR.FechaEnvioFormulario END
		,LocalDestino = CASE WHEN SFC.LocalDestino IS NOT NULL THEN SFC.LocalDestino ELSE SFR.LocalDestino END
		,UsuarioCarga = UPPER(SFC.CargadorUsuario) 
		,UsuarioRecepcion = UPPER(SFR.CargadorUsuario) 
		,Tipo = SFC.Tipo
	FROM @SmuFormCarga SFC
	FULL JOIN @SmuFormRecepcion SFR ON (SFR.NumTransporte = SFC.NumTransporte AND SFR.LocalDestino = SFC.LocalDestino)