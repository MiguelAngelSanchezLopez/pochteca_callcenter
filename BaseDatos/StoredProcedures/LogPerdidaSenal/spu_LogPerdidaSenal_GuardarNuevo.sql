﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_LogPerdidaSenal_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_LogPerdidaSenal_GuardarNuevo'
  DROP  Procedure  dbo.spu_LogPerdidaSenal_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_LogPerdidaSenal_GuardarNuevo'
GO

CREATE PROCEDURE [dbo].[spu_LogPerdidaSenal_GuardarNuevo]
/******************************************************************************
**    Descripcion  : Agrega un nuevo registro a la tabla LogPerdidaSenal
**    Autor        : VSR
**    Fecha        : 23/04/2015
*******************************************************************************/
@Id INT OUTPUT,
@NroTransporte AS INT,
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@LocalDestino AS INT,
@FechaHoraInicio AS DATETIME,
@FechaHoraTermino AS DATETIME,
@FechaHoraCreacion AS DATETIME
AS
BEGIN

  IF (@NroTransporte = -1) SET @NroTransporte = NULL
  IF (@LocalDestino = -1) SET @LocalDestino = NULL

  INSERT INTO SMU_LogPerdidaSenal
  (
	  NroTransporte,
	  PatenteTracto,
	  PatenteTrailer,
	  LocalDestino,
	  FechaHoraInicio,
	  FechaHoraTermino,
	  FechaHoraCreacion
  )
  VALUES
  (
	  @NroTransporte,
	  @PatenteTracto,
	  @PatenteTrailer,
	  @LocalDestino,
	  @FechaHoraInicio,
	  @FechaHoraTermino,
	  @FechaHoraCreacion
  )

  SET @Id = SCOPE_IDENTITY()

  -- ACTUALIZA EL CAMPO CONLOG INDICANDO QUE SE RECUPERO LA TRAZA DE LAS ALERTAS CUANDO SE PERDIO LA SEÑAL
  UPDATE 
	  Alerta
  SET 
	  ConLog = 1 
  WHERE 
	    NroTransporte = @NroTransporte
  AND LocalDestino = @LocalDestino
  AND TipoAlertaDescripcion IN ('Perdida señal Tracto', 'Perdida señal viaje')
  AND FechaInicioAlerta BETWEEN @FechaHoraInicio AND @FechaHoraTermino

END