﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_LogPerdidaSenal_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_LogPerdidaSenal_ObtenerPorId'
  DROP  Procedure  dbo.spu_LogPerdidaSenal_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_LogPerdidaSenal_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_LogPerdidaSenal_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
 SET NOCOUNT ON  
  
  SELECT
    *
  FROM   
    SMU_LogPerdidaSenal  
  WHERE   
    Id = @Id
  
END  