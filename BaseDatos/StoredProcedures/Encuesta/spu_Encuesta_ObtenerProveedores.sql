﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Encuesta_ObtenerProveedores')
BEGIN
  PRINT  'Dropping Procedure spu_Encuesta_ObtenerProveedores'
  DROP  Procedure  dbo.spu_Encuesta_ObtenerProveedores
END

GO

PRINT  'Creating Procedure spu_Encuesta_ObtenerProveedores'
GO
CREATE Procedure dbo.spu_Encuesta_ObtenerProveedores
/******************************************************************************
**    Descripcion  : Obtiene el listado proveedores
**    Autor        : DG
**    Fecha        : 04/06/2015
*******************************************************************************/
@id AS INTEGER
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

----------------------------------------------------------
-- Tabla 0: Proveedores
----------------------------------------------------------
	SELECT 
		id
		,Nombre
	FROM EncuestaProveedor
	WHERE (@id = -1)
	OR (@id != -1 AND id = @id)
END
GO 