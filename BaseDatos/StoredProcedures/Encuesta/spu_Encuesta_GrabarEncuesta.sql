﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Encuesta_GrabarEncuesta')
BEGIN
  PRINT  'Dropping Procedure spu_Encuesta_GrabarEncuesta'
  DROP  Procedure  dbo.spu_Encuesta_GrabarEncuesta
END

GO

PRINT  'Creating Procedure spu_Encuesta_GrabarEncuesta'
GO
CREATE Procedure [dbo].[spu_Encuesta_GrabarEncuesta]
/******************************************************************************
**  Descripcion  : graba encuesta
**  Fecha        : 05/06/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@NumCategoria AS INT,
@NumRespuesta AS VARCHAR(5),
@Si AS BIT,
@No AS BIT,
@Na AS BIT,
@IdEncuestaProveedor AS INT,
@idTransportista AS INT

AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
  
	INSERT INTO EncuestaRespuesta
	(
		NumCategoria,
		NumRespuesta,
		Si,
		No,
		Na,
		IdEncuestaProveedor,
		IdTransportista
	)
	VALUES
	(
		@NumCategoria,
		@NumRespuesta,
		@Si,
		@No,
		@Na,
		@IdEncuestaProveedor,
		@idTransportista
	)
	
	SET @Status	= SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END