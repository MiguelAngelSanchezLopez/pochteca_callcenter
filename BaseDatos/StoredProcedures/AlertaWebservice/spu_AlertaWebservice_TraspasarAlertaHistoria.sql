﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_TraspasarAlertaHistoria')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_TraspasarAlertaHistoria'
  DROP  Procedure  dbo.spu_AlertaWebservice_TraspasarAlertaHistoria
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_TraspasarAlertaHistoria'
GO

CREATE PROCEDURE [dbo].[spu_AlertaWebservice_TraspasarAlertaHistoria]
/******************************************************************************
**    Descripcion  : traspasa los datos de la alerta a la tabla AlertaHistoria
**    Autor        : VSR
**    Fecha        : 18/05/2015
*******************************************************************************/
@NroTransporte AS INT,
@DescripcionAlerta AS VARCHAR(255),
@LocalDestino AS INT
AS
BEGIN

  BEGIN TRANSACTION

    -------------------------------------------------------    
    -- traspasa los datos a la tabla historia
    -------------------------------------------------------    
    INSERT INTO AlertaHistoria
    SELECT
      *
    FROM
      AlertaWebservice
    WHERE
        NroTransporte = @NroTransporte
    AND (@DescripcionAlerta = '-1' OR DescripcionAlerta = @DescripcionAlerta)
    AND LocalDestino = @LocalDestino

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

    -------------------------------------------------------    
    -- elimina los registros traspasados
    -------------------------------------------------------    
    DELETE FROM AlertaWebservice
    WHERE
        NroTransporte = @NroTransporte
    AND (@DescripcionAlerta = '-1' OR DescripcionAlerta = @DescripcionAlerta)
    AND LocalDestino = @LocalDestino

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END