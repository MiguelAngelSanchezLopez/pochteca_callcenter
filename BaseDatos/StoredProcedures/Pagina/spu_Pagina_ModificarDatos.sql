IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_ModificarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_ModificarDatos'
  DROP  Procedure  dbo.spu_Pagina_ModificarDatos
END

GO

PRINT  'Creating Procedure spu_Pagina_ModificarDatos'
GO
CREATE Procedure dbo.spu_Pagina_ModificarDatos
/******************************************************************************
**  Descripcion  : graba una pagina
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdPagina AS INT,
@Titulo AS VARCHAR(50),
@NombreArchivo AS VARCHAR(1028),
@NombreCategoria AS VARCHAR(50),
@Estado AS INT,
@MostrarEnMenu AS INT,
@ListadoIdPerfiles AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @idCategoria AS INT
    DECLARE @ultimoOrdenCategoria AS INT

    ------------------------------------------------------------------------------------
    -- VERIFICA LOS DATOS DE LA CATEGORIA PARA CREAR UNA U OBTENER SU ID SI EXISTE
    ------------------------------------------------------------------------------------
    -- verifica si existe
    IF(@NombreCategoria='') BEGIN
      SET @idCategoria = NULL
    END
    ELSE BEGIN
      -- verifica si la categoria existe, si existe obtiene su id, sino lo agrega
      IF(EXISTS(SELECT Id FROM CategoriaPagina WHERE Nombre = @NombreCategoria)) BEGIN
        SELECT @idCategoria = Id FROM CategoriaPagina WHERE Nombre = @NombreCategoria
      END
      ELSE BEGIN
        SELECT @ultimoOrdenCategoria = MAX(Orden) FROM CategoriaPagina
        IF(@ultimoOrdenCategoria IS NULL) SET @ultimoOrdenCategoria = 0
        
        INSERT INTO CategoriaPagina(Nombre, Orden) VALUES(@NombreCategoria,@ultimoOrdenCategoria+1)
        SET @idCategoria = SCOPE_IDENTITY()
      END
    END

    ------------------------------------------------------------------------------------
    -- ACTUALIZA LOS DATOS DE LA PAGINA
    ------------------------------------------------------------------------------------
    -- verifica que orden tiene la pagina
    IF(@idCategoria IS NOT NULL) BEGIN
      IF(NOT EXISTS(SELECT Orden FROM Pagina WHERE IdCategoria = @IdCategoria AND Id = @IdPagina)) BEGIN
        SELECT @ultimoOrdenCategoria = MAX(Orden) FROM Pagina WHERE IdCategoria = @IdCategoria
        IF(@ultimoOrdenCategoria IS NULL) SET @ultimoOrdenCategoria = 0
        SET @ultimoOrdenCategoria = @ultimoOrdenCategoria + 1
      END
      ELSE BEGIN
        SELECT @ultimoOrdenCategoria = Orden FROM Pagina WHERE Id = @IdPagina
      END
    END
    ELSE BEGIN
      SET @ultimoOrdenCategoria = 1
    END
    
    UPDATE Pagina
    SET
	    Titulo = @Titulo,
	    NombreArchivo = @NombreArchivo,
	    IdCategoria = @IdCategoria,
	    Orden = @ultimoOrdenCategoria,
	    Estado = @Estado,
	    MostrarEnMenu = @MostrarEnMenu
    WHERE
      Id = @IdPagina

    ------------------------------------------------------------------------------------
    -- INSERTA PERFILES ASOCIADOS A LA PAGINA
    ------------------------------------------------------------------------------------
    -- elimina las asociaciones previas para ingresar las nuevas
    DELETE FROM PaginasXPerfil WHERE Idpagina = @IdPagina
    IF(@ListadoIdPerfiles <> '') BEGIN
      INSERT INTO PaginasXPerfil
      (
        IdPagina,
        IdPerfil
      )
      SELECT
        @IdPagina,
        valor
      FROM
        dbo.fnu_InsertaListaTabla(@ListadoIdPerfiles)
    END

    SET @Status = 'modificado'
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO         