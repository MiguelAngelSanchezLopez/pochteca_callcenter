IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Pagina_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Pagina_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Pagina_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle de la pagina consultada
**    Por          : VSR, 15/09/2009
*******************************************************************************/
@IdPagina AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  /*****************************************************************************
  * TABLA 0 = DETALLE
  *****************************************************************************/
  SELECT
	  IdPagina = P.Id,
	  Titulo = ISNULL(P.Titulo,''),
	  NombreArchivo = ISNULL(P.NombreArchivo,''),
	  IdCategoria = CP.Id,
	  NombreCategoria = ISNULL(CP.Nombre,''),
	  Estado =  ISNULL(P.Estado,0),
	  MostrarEnMenu = ISNULL(CONVERT(INT,P.MostrarEnMenu),0)
  FROM
    Pagina AS P
    LEFT JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)
  WHERE
    P.Id = @IdPagina


  /*****************************************************************************
  * TABLA 1 = LISTADO FUNCIONES
  *****************************************************************************/
  SELECT
	  IdFuncion = FP.Id,
	  Nombre = ISNULL(FP.Nombre,''),
	  LlaveIdentificador = ISNULL(FP.LlaveIdentificador,''),
	  Orden = ISNULL(CONVERT(VARCHAR,FP.Orden),'')
  FROM
	  FuncionPagina AS FP
	WHERE
	  FP.IdPagina = @IdPagina
  ORDER BY
    FP.Orden


  /**************************************************************************
  * TABLA 2: LISTADO DE PERFILES QUE NO PERTENECEN
  ***************************************************************************/
  SELECT 
    Valor = P.Id,
    Texto = ISNULL(P.Nombre,'')
  FROM
    Perfil AS P
  WHERE
    P.Id NOT IN(
      SELECT
        P.Id
      FROM
        Perfil AS P
        INNER JOIN PaginasXPerfil AS PxP ON (P.Id = PxP.IdPerfil)
      WHERE
        PxP.IdPagina = @IdPagina
    )
  ORDER BY
    Texto


  /**************************************************************************
  * TABLA 3: LISTADO DE PERFILES QUE SI PERTENECEN
  ***************************************************************************/
  SELECT
    Valor = P.Id,
    Texto = ISNULL(P.Nombre,'')
  FROM
    Perfil AS P
    INNER JOIN PaginasXPerfil AS PxP ON (P.Id = PxP.IdPerfil)
  WHERE
    PxP.IdPagina = @IdPagina
  ORDER BY
    Texto
END



   