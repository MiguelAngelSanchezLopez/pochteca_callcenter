  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_VerificarDuplicidadNombreArchivo')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_VerificarDuplicidadNombreArchivo'
  DROP  Procedure  dbo.spu_Pagina_VerificarDuplicidadNombreArchivo
END

GO

PRINT  'Creating Procedure spu_Pagina_VerificarDuplicidadNombreArchivo'
GO
CREATE Procedure dbo.spu_Pagina_VerificarDuplicidadNombreArchivo
/******************************************************************************
**  Descripcion  : valida que el nombre del archivo sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 26/06/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPagina AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdPagina IS NULL OR @IdPagina=0) SET @IdPagina = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdPagina = -1) BEGIN
    IF( EXISTS(
                SELECT
                  P.NombreArchivo
                FROM
                  Pagina AS P
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(P.NombreArchivo,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  P.NombreArchivo
                FROM
                  Pagina AS P
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(P.NombreArchivo,'')))) = @Valor
                AND P.Id <> @IdPagina
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       