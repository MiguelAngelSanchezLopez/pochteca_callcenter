IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_AutocompletarCategoria')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_AutocompletarCategoria'
  DROP  Procedure  dbo.spu_Pagina_AutocompletarCategoria
END

GO

PRINT  'Creating Procedure spu_Pagina_AutocompletarCategoria'
GO
CREATE Procedure dbo.spu_Pagina_AutocompletarCategoria
/******************************************************************************
**    Descripcion  : Obtiene listado de categorias para autocompletar la busqueda
**    Autor        : VSR
**    Fecha        : 21/08/2009
*******************************************************************************/
@Texto AS VARCHAR(1024)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT TOP 50
    CP.Nombre
  FROM
    CategoriaPagina AS CP
  WHERE
      RTRIM(LTRIM(CP.Nombre)) LIKE '%'+ @Texto +'%'
  AND RTRIM(LTRIM(@Texto)) <> ''
  ORDER BY
    CP.Nombre

END
GO   