﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_ObtenerPorId'
  DROP  Procedure  dbo.spu_TrazaViaje_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_TrazaViaje_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
 SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    SMU_TrazaViaje  
  WHERE   
    Id = @Id  
  
END  