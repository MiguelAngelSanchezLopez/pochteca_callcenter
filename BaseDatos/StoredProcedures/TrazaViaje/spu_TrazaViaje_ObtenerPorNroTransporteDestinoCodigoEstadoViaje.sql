﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_ObtenerPorNroTransporteDestinoCodigoEstadoViaje')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_ObtenerPorNroTransporteDestinoCodigoEstadoViaje'
  DROP  Procedure  dbo.spu_TrazaViaje_ObtenerPorNroTransporteDestinoCodigoEstadoViaje
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_ObtenerPorNroTransporteDestinoCodigoEstadoViaje'
GO

CREATE Procedure [dbo].[spu_TrazaViaje_ObtenerPorNroTransporteDestinoCodigoEstadoViaje] (  
@NroTransporte INT,
@LocalDestino INT,
@EstadoViaje VARCHAR(255)
) AS  
BEGIN  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
 SET NOCOUNT ON  
  
  SELECT
    *
  FROM
    SMU_TrazaViaje
  WHERE   
      NroTransporte = @NroTransporte
  AND LocalDestino = @LocalDestino
  AND EstadoViaje = @EstadoViaje
  
END  