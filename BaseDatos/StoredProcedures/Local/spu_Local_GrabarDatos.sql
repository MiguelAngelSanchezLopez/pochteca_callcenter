﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Local_GrabarDatos'
  DROP  Procedure  dbo.spu_Local_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_Local_GrabarDatos'
GO
CREATE Procedure dbo.spu_Local_GrabarDatos
/******************************************************************************
**  Descripcion  : graba los datos del local
**  Fecha        : 04/09/2014
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdLocalOutput AS INT OUTPUT,
@IdLocal AS INT,
@Nombre AS VARCHAR(255),
@IdFormato AS INT,
@CodigoInterno AS INT,
@Activo AS INT,
@MaximoHorasAlertaActiva AS INT,
@ListadoUsuariosCargos VARCHAR(8000),
@Etapa AS INT,
@FocoAlto AS INT,
@FocoTablet AS INT,
@Telefono AS VARCHAR(4000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    IF (@MaximoHorasAlertaActiva = -1) SET @MaximoHorasAlertaActiva = NULL
    IF (@Etapa = -1) SET @Etapa = NULL

    -- ingresa nuevo registro
    IF (@IdLocal = -1) BEGIN
      INSERT INTO Local
      (
	      Nombre,
	      CodigoInterno,
	      IdFormato,
	      FechaCreacion,
	      Activo,
        MaximoHorasAlertaActiva,
        Etapa,
        FocoAlto,
        FocoTablet,
        Telefono
      )
      VALUES
      (
	      @Nombre,
	      @CodigoInterno,
	      @IdFormato,
	      dbo.fnu_GETDATE(),
	      @Activo,
        @MaximoHorasAlertaActiva,
        @Etapa,
        @FocoAlto,
        @FocoTablet,
        @Telefono
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdLocalOutput = @IdLocal
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdLocalOutput = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE Local
      SET
	      Nombre = @Nombre,
	      CodigoInterno = @CodigoInterno,
	      IdFormato = @IdFormato,
	      Activo = @Activo,
        MaximoHorasAlertaActiva = @MaximoHorasAlertaActiva,
        Etapa = @Etapa,
        FocoAlto = @FocoAlto,
        FocoTablet = @FocoTablet,
        Telefono = @Telefono
      WHERE
        Id = @IdLocal

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdLocalOutput = @IdLocal
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdLocalOutput = @IdLocal

    END

    ----------------------------------------------------------------
    -- elimina todas las referencias de usuarios y carga los nuevos
    ----------------------------------------------------------------
    DELETE FROM LocalesPorUsuario WHERE IdLocal = @IdLocalOutput 

    IF (@ListadoUsuariosCargos <> '') BEGIN
      INSERT INTO LocalesPorUsuario(IdLocal,IdUsuario)
      SELECT @IdLocalOutput, valor FROM dbo.fnu_InsertaListaTabla(@ListadoUsuariosCargos)
    END

  COMMIT
END
GO        