﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_EliminarPatente')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_EliminarPatente'
  DROP  Procedure  dbo.spu_Transportista_EliminarPatente
END

GO

PRINT  'Creating Procedure spu_Transportista_EliminarPatente'
GO
CREATE Procedure spu_Transportista_EliminarPatente
/******************************************************************************
**  Descripcion  : elimina una patente
**  Fecha        : 18/06/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

	BEGIN TRANSACTION
	
	DELETE FROM Patente WHERE Id = @id
	
	IF @@ERROR<>0 BEGIN
		ROLLBACK
		SET @Status='error'
		RETURN
	END
	
	SET @Status = 'eliminado'
	
	COMMIT
END