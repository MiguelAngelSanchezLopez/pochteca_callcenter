﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_ObtenerAlertasRojasTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_ObtenerAlertasRojasTransportista'
  DROP  Procedure  dbo.spu_Transportista_ObtenerAlertasRojasTransportista
END

GO

PRINT  'Creating Procedure spu_Transportista_ObtenerAlertasRojasTransportista'
GO
CREATE Procedure dbo.spu_Transportista_ObtenerAlertasRojasTransportista
/******************************************************************************    
**    Descripcion  : obtiene listado alertas rojas del transportista    
**    Por          : VSR, 04/08/2015
*******************************************************************************/    
@Rut AS VARCHAR(255),    
@NroTransporte AS INT    
AS    
BEGIN    
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
  SET NOCOUNT ON    
    
  SELECT DISTINCT    
    IdAlertaRojaEstadoActual = EA.Id,    
    NroTransporte = EA.NroTransporte,    
    Estado = EA.Estado,    
    Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(EA.FechaModificacion, 'FECHA_COMPLETA'),    
    ModificadoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),    
    FechaModificacion = EA.FechaModificacion    
  FROM    
    AlertaRojaEstadoActual AS EA    
    INNER JOIN Usuario AS U ON (EA.IdUsuarioModificacion = U.Id)    
    INNER JOIN Alerta AS A ON (EA.NroTransporte = A.NroTransporte)
    INNER JOIN CallCenterAlerta AS CCA ON (A.Id = CCA.IdAlerta)
    INNER JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
  WHERE    
      ( RTRIM(LTRIM(REPLACE(REPLACE(TV.RutTransportista,' ',''),'.',''))) = @Rut )    
  AND ( @NroTransporte = -1 OR EA.NroTransporte = @NroTransporte )    
  AND ( EA.Estado = 'ENVIADO TRANSPORTISTA' OR EA.Estado = 'RECHAZADO')     
  ORDER BY    
    FechaModificacion  

END  