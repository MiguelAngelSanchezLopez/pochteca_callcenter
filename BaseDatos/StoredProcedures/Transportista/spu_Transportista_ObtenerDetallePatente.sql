﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_ObtenerDetallePatente')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_ObtenerDetallePatente'
  DROP  Procedure  dbo.spu_Transportista_ObtenerDetallePatente
END

GO

PRINT  'Creating Procedure spu_Transportista_ObtenerDetallePatente'
GO
CREATE Procedure dbo.spu_Transportista_ObtenerDetallePatente
/******************************************************************************
**    Descripcion  : Obtiene listado de patentes asociadas al transportista
**    Por          : DG, 17/06/2015
*******************************************************************************/
 @Id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT 
	Id
	,PlacaPatente = ISNULL(PlacaPatente,'')
	,Marca = ISNULL(Marca,'')
	,Tipo
  FROM Patente
  WHERE Id = @Id
  
END