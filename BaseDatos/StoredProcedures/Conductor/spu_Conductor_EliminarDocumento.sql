﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_EliminarDocumento')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_EliminarDocumento'
  DROP  Procedure  dbo.spu_Conductor_EliminarDocumento
END

GO

PRINT  'Creating Procedure spu_Conductor_EliminarDocumento'
GO
CREATE Procedure [dbo].[spu_Conductor_EliminarDocumento]
/******************************************************************************
**  Descripcion  : elimina un Documento
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdTipoDocumentoXFichaConductor AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

	DECLARE @IdArchivo INT
	
	BEGIN TRANSACTION
	
	--Obtengo el id del achivo asociado al documento
	SELECT @IdArchivo = IdArchivo FROM ArchivosXRegistro WHERE Entidad = 'DocumentoConductor' AND IdRegistro = @IdTipoDocumentoXFichaConductor	

	DELETE FROM ArchivosXRegistro WHERE IdArchivo = @IdArchivo AND Entidad = 'DocumentoConductor'
	DELETE FROM Archivo WHERE Id = @IdArchivo
	DELETE FROM TipoDocumentoXFichaConductor WHERE Id = @IdTipoDocumentoXFichaConductor
	
	
	IF @@ERROR<>0 BEGIN
		ROLLBACK
		SET @Status='error'
		RETURN
	END
	
	SET @Status = 'modificado'
	
	COMMIT
END