﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_VerificarDuplicidadRut')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_VerificarDuplicidadRut'
  DROP  Procedure  dbo.spu_Conductor_VerificarDuplicidadRut
END

GO

PRINT  'Creating Procedure spu_Conductor_VerificarDuplicidadRut'
GO
CREATE Procedure [dbo].[spu_Conductor_VerificarDuplicidadRut]
/******************************************************************************
**  Descripcion  : valida que el rut sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 27/07/2009
*******************************************************************************/
@Status AS INT OUTPUT,
@IdConductor AS INT,
@Valor AS VARCHAR(20),
@IdTransportista AS INT

AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdConductor IS NULL OR @IdConductor = 0) SET @IdConductor = -1
  IF(@IdTransportista IS NULL OR @IdTransportista = 0) SET @IdTransportista = -1
  IF(@Valor IS NULL OR @Valor='') SET @Valor = '-1'

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdConductor = -1) BEGIN
    IF( EXISTS(
                SELECT
                  C.Rut
                FROM
                  FichaConductor AS C
                WHERE
                  RTRIM(LTRIM(ISNULL(C.Rut,''))) = @Valor
                  AND C.IdTransportista = @IdTransportista
                  AND C.Estado = 1
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  C.Rut
                FROM
                  FichaConductor AS C
                WHERE
                  RTRIM(LTRIM(ISNULL(C.Rut,''))) = @Valor
				  AND C.Id <> @IdConductor
				  AND C.IdTransportista = @IdTransportista
				  AND C.Estado = 1
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END