﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_ClasificacionConductor_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_ClasificacionConductor_EliminarDatos'
  DROP  Procedure  dbo.spu_ClasificacionConductor_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_ClasificacionConductor_EliminarDatos'
GO
CREATE Procedure dbo.spu_ClasificacionConductor_EliminarDatos
/******************************************************************************
**  Descripcion  : elimina una clasificacion Conductor
**  Fecha        : 25/04/2016
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdClasificacionConductor AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM ClasificacionConductor WHERE Id = @IdClasificacionConductor

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           