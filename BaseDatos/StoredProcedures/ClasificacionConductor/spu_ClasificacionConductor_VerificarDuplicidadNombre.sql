﻿  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_ClasificacionConductor_VerificarDuplicidadNombre')
BEGIN
  PRINT  'Dropping Procedure spu_ClasificacionConductor_VerificarDuplicidadNombre'
  DROP  Procedure  dbo.spu_ClasificacionConductor_VerificarDuplicidadNombre
END

GO

PRINT  'Creating Procedure spu_ClasificacionConductor_VerificarDuplicidadNombre'
GO
CREATE Procedure dbo.spu_ClasificacionConductor_VerificarDuplicidadNombre
/******************************************************************************
**  Descripcion  : valida que el nombre de la clasificacion transportista sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 25/04/2016
*******************************************************************************/
@Status AS INT OUTPUT,
@IdClasificacionConductor AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdClasificacionConductor IS NULL OR @IdClasificacionConductor = 0) SET @IdClasificacionConductor = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdClasificacionConductor = -1) BEGIN
    IF( EXISTS(
                SELECT
                  CC.Nombre
                FROM
                  ClasificacionConductor AS CC
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(CC.Nombre,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  CC.Nombre
                FROM
                  ClasificacionConductor AS CC
                WHERE
                    UPPER(RTRIM(LTRIM(ISNULL(CC.Nombre,'')))) = @Valor
                AND CC.Id <> @IdClasificacionConductor
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       