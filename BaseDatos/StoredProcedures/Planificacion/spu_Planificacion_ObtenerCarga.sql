﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Planificacion_ObtenerCarga')
BEGIN
  PRINT  'Dropping Procedure spu_Planificacion_ObtenerCarga'
  DROP  Procedure  dbo.spu_Planificacion_ObtenerCarga
END

GO

PRINT  'Creating Procedure spu_Planificacion_ObtenerCarga'
GO
CREATE Procedure dbo.spu_Planificacion_ObtenerCarga
/******************************************************************************
**    Descripcion  : obtiene los datos de la carga realizada
**    Por          : VSR, 05/06/2015
*******************************************************************************/
@IdPlanificacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @TablaPlanificacion AS TABLE(IdPlanificacionTransportista INT, IdLocal INT, NombreLocal VARCHAR(255), CodigoLocal INT, IdUsuarioTransportista INT, NombreTransportista VARCHAR(255), RutTransportista VARCHAR(255),
                                       NroTransporte INT, FechaPresentacion DATETIME, Carga VARCHAR(255), OrdenEntrega INT, TipoCamion VARCHAR(255), IdPlanificacion INT, EmailTransportista VARCHAR(255), EmailTransportistaConCopia TEXT)

  INSERT INTO @TablaPlanificacion(IdPlanificacionTransportista, IdLocal, NombreLocal, CodigoLocal, IdUsuarioTransportista, NombreTransportista, RutTransportista, NroTransporte, FechaPresentacion,
                                  Carga, OrdenEntrega, TipoCamion, IdPlanificacion, EmailTransportista, EmailTransportistaConCopia)
  SELECT
	  IdPlanificacionTransportista = PT.Id,
	  IdLocal = PT.IdLocal,
    NombreLocal = ISNULL(L.Nombre,''),
    CodigoLocal = ISNULL(L.CodigoInterno,''),
	  IdUsuarioTransportista = PT.IdUsuarioTransportista,
    NombreTransportista = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' ' + U.Paterno,''),
    RutTransportista = ISNULL(U.Rut,'') + CASE WHEN ISNULL(U.DV,'') = '' THEN '' ELSE '-' + ISNULL(U.DV,'') END,
	  NroTransporte = PT.NroTransporte,
	  FechaPresentacion = PT.FechaPresentacion,
	  Carga = ISNULL(PT.Carga,''),
	  OrdenEntrega = ISNULL(PT.OrdenEntrega,0),
	  TipoCamion = ISNULL(PT.TipoCamion,''),
    IdPlanificacion = PT.IdPlanificacion,
    EmailTransportista = ISNULL(U.Email,''),
    EmailTransportistaConCopia = ISNULL(UDE.EmailConCopia,'')
  FROM
	  PlanificacionTransportista AS PT
    INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
    INNER JOIN Usuario AS U ON (PT.IdUsuarioTransportista = U.Id)
    LEFT JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
  WHERE
      (PT.IdPlanificacion = @IdPlanificacion)

  ---------------------------------------------------------------
  -- TABLA 0: obtiene planificacion por NroTransporte
  ---------------------------------------------------------------
  SELECT DISTINCT
    P.IdPlanificacion,
	  P.NroTransporte,
	  FechaPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(P.FechaPresentacion, 'FECHA_COMPLETA'),
	  P.IdUsuarioTransportista,
    P.NombreTransportista,
    P.RutTransportista,
	  P.Carga,
	  P.TipoCamion,
    FechaOrden = P.FechaPresentacion
  FROM
	  @TablaPlanificacion AS P
  ORDER BY
    P.NombreTransportista, FechaOrden, P.NroTransporte

  ---------------------------------------------------------------
  -- TABLA 1: listado de locales por NroTransporte
  ---------------------------------------------------------------
  SELECT
    P.IdPlanificacion,
	  P.NroTransporte,
	  P.IdPlanificacionTransportista,
	  P.IdLocal,
    P.NombreLocal,
    P.CodigoLocal,
	  P.OrdenEntrega
  FROM
	  @TablaPlanificacion AS P
  ORDER BY
    NroTransporte, OrdenEntrega

  ---------------------------------------------------------------
  -- TABLA 2: listado de transportistas para notificar por email
  ---------------------------------------------------------------
  SELECT DISTINCT
    P.RutTransportista,
    P.EmailTransportista,
    EmailTransportistaConCopia = CONVERT(VARCHAR(8000),P.EmailTransportistaConCopia)
  FROM
	  @TablaPlanificacion AS P
  ORDER BY
    P.RutTransportista

  ---------------------------------------------------------------
  -- TABLA 3: planificacion transportistas para notificar por email
  ---------------------------------------------------------------
  SELECT
    [RUT LINEA DE TRANSPORTE] = P.RutTransportista,
    [LINEA DE TRANSPORTE] = P.NombreTransportista,
	  [FECHA HORA PRESENTACION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(P.FechaPresentacion, 'FECHA_COMPLETA'),
	  [IDMASTER] = P.NroTransporte,
    [TIENDA] = P.NombreLocal,
    [DETERMINANTE] = P.CodigoLocal,
	  [ORDEN ENTREGA] = P.OrdenEntrega,
	  [CARGA] = P.Carga,
	  [TIPO CAMION] = P.TipoCamion
  FROM
    @TablaPlanificacion AS P
  ORDER BY
    P.RutTransportista, P.FechaPresentacion, P.NroTransporte, P.OrdenEntrega

END



  