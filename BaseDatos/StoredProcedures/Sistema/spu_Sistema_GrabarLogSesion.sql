﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_GrabarLogSesion')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_GrabarLogSesion'
  DROP  Procedure  dbo.spu_Sistema_GrabarLogSesion
END

GO

PRINT  'Creating Procedure spu_Sistema_GrabarLogSesion'
GO
CREATE Procedure dbo.spu_Sistema_GrabarLogSesion
/******************************************************************************
**  Descripcion  : graba la accion realizada por el usuario
**  Fecha        : 09/10/2014
*******************************************************************************/
@IdUsuario AS INT,
@Accion AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    INSERT LogSesion
    (
	    IdUsuario,
	    Accion,
	    Fecha
    )
    VALUES
    (
	    @IdUsuario,
	    @Accion,
	    dbo.fnu_GETDATE()
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END
GO        