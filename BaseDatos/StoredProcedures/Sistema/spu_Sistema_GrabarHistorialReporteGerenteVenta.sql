﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_GrabarHistorialReporteGerenteVenta')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_GrabarHistorialReporteGerenteVenta'
  DROP  Procedure  dbo.spu_Sistema_GrabarHistorialReporteGerenteVenta
END

GO

PRINT  'Creating Procedure spu_Sistema_GrabarHistorialReporteGerenteVenta'
GO
CREATE Procedure dbo.spu_Sistema_GrabarHistorialReporteGerenteVenta
/******************************************************************************
**  Descripcion  : graba los nro transporte que han sido notificadas por email
**  Fecha        : 16/03/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@NroTransporte AS INT,
@EnviadoA AS VARCHAR(255),
@Cargo AS VARCHAR(255),
@NombreLocal AS VARCHAR(255),
@CodigoLocal AS VARCHAR(255),
@Formato AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO HistorialReporteGerenteVenta
    (
	    NroTransporte,
	    EnviadoA,
	    Cargo,
	    NombreLocal,
	    CodigoLocal,
	    Formato,
	    FechaEnvio
    )
    VALUES
    (
	    @NroTransporte,
	    @EnviadoA,
	    @Cargo,
	    @NombreLocal,
	    @CodigoLocal,
	    @Formato,
	    dbo.fnu_GETDATE()
    )
    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO
