﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_ObtenerCargaPoolPlaca')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_ObtenerCargaPoolPlaca'
  DROP  Procedure  dbo.spu_Sistema_ObtenerCargaPoolPlaca
END

GO

PRINT  'Creating Procedure spu_Sistema_ObtenerCargaPoolPlaca'
GO
CREATE Procedure dbo.spu_Sistema_ObtenerCargaPoolPlaca
/******************************************************************************
**    Descripcion  : envia carga de placas por email
**    Por          : VSR, 24/08/2017
*******************************************************************************/
@IdUsuario AS INT,
@FechaCarga AS DATETIME
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @TablaPoolPlaca AS TABLE (
    Placa VARCHAR(255),
    Reportabilidad VARCHAR(255),
    TipoVehiculo VARCHAR(255),
    TransporteHomoclave VARCHAR(255),
    TransporteNombre VARCHAR(255),
    Cedis VARCHAR(255),
    Determinante INT,
    FechaCarga VARCHAR(255),
    NumeroEconomico VARCHAR(255)
  )

  ---------------------------------------------------------
  -- obtiene informacion de la carga
  INSERT INTO @TablaPoolPlaca(
    Placa,
    Reportabilidad,
    TipoVehiculo,
    TransporteHomoclave,
    TransporteNombre,
    Cedis,
    Determinante,
    FechaCarga,
    NumeroEconomico
  )
  SELECT
    Placa = ISNULL(PP.Placa,''),
    Reportabilidad = CASE VRP.Estado
                       WHEN 'Online' THEN 'Reportando Online'
                       WHEN 'Offline' THEN 'No reporta desde ' + VRP.Fecha
                     ELSE
                       'Placa no integrada en el sistema'
                     END,
    TipoVehiculo = ISNULL(PP.TipoVehiculo,''),
    TransporteHomoclave = ISNULL(PP.Transporte, ''),
    TransporteNombre = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Cedis = ISNULL(PP.Cedis,''),
    Determinante = ISNULL(PP.Determinante,''),
    FechaCarga = dbo.fnu_ConvertirDatetimeToDDMMYYYY(PP.FechaCarga, 'FECHA_COMPLETA'),
    NumeroEconomico = ISNULL(PP.NumeroEconomico,'')
  FROM
    PoolPlaca AS PP
    INNER JOIN Usuario AS U ON (PP.IdUsuarioTransportista = U.Id)
    LEFT JOIN vwu_ReportabilidadPatente AS VRP ON (PP.Placa = VRP.Patente)
  WHERE
      PP.IdUsuarioCreacion = @IdUsuario
  AND PP.FechaCarga = @FechaCarga


  ------------------------------------------------------------------------------------------
  -- TABLA 0: Devuele la carga
  ------------------------------------------------------------------------------------------
  SELECT
    PLACA = PP.Placa,
    REPORTABILIDAD = PP.Reportabilidad,
    [TIPO VEHICULO] = PP.TipoVehiculo,
    [TRANSPORTE HOMOCLAVE] = PP.TransporteHomoclave,
    [TRANSPORTE NOMBRE] = PP.TransporteNombre,
    CEDIS = PP.Cedis,
    DETERMINANTE = PP.Determinante,
    [NUMERO ECONOMICO] = PP.NumeroEconomico,
    [FECHA CARGA] = PP.FechaCarga
  FROM
    @TablaPoolPlaca AS PP
  ORDER BY
    DETERMINANTE

  ------------------------------------------------------------------------------------------
  -- TABLA 1: Listad de encargados por Cedis
  ------------------------------------------------------------------------------------------
  SELECT DISTINCT
    IdUsuario = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Email = ISNULL(U.Email, ''),
    Telefono = ISNULL(U.Telefono, ''),
    IdCentroDistribucion = CD.Id,
    NombreCentroDistribucion = ISNULL(CD.Nombre,''),
    CodigoInterno = ISNULL(CD.CodigoInterno,-1)
  FROM
    CentroDistribucionPorUsuario AS CDPU
    INNER JOIN CentroDistribucion AS CD ON (CDPU.IdCentroDistribucion = CD.Id)
    INNER JOIN Usuario AS U ON (CDPU.IdUsuario = U.Id)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    INNER JOIN @TablaPoolPlaca AS PP ON (CD.CodigoInterno = PP.Determinante)
  WHERE
    P.Llave IN ('GT')
  ORDER BY
    IdUsuario

END



  