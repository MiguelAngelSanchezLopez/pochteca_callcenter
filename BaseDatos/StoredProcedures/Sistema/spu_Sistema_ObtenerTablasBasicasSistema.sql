﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_ObtenerTablasBasicasSistema')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_ObtenerTablasBasicasSistema'
  DROP  Procedure  dbo.spu_Sistema_ObtenerTablasBasicasSistema
END

GO

PRINT  'Creating Procedure spu_Sistema_ObtenerTablasBasicasSistema'
GO
CREATE Procedure dbo.spu_Sistema_ObtenerTablasBasicasSistema
/******************************************************************************
**    Descripcion  : obtiene datos de las tablas basicas para comparar si existen los datos a cargar
**    Autor        : VSR
**    Fecha        : 22/08/2017
*******************************************************************************/
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @EsSuper BIT
  SET @EsSuper = (SELECT Super FROM Usuario WHERE Id = @IdUsuario)
  IF (@EsSuper IS NULL) SET @EsSuper = 0

  --------------------------------------------------------------------------
  -- TABLA 0: Centro Distribucion
  --------------------------------------------------------------------------
  SELECT
    IdCentroDistribucion = CD.Id,
    Nombre = ISNULL(CD.Nombre,''),
    CodigoInterno = ISNULL(CD.CodigoInterno, -1)
  FROM 
    CentroDistribucion AS CD

  --------------------------------------------------------------------------
  -- TABLA 1: Homoclaves transportistas
  --------------------------------------------------------------------------
  SELECT
    IdHomoclave = THC.Id,
    Nomenclatura = THC.Nomenclatura,
    IdUsuario = THC.IdUsuario
  FROM
    Track_Homoclave AS THC
  WHERE
    THC.IdUsuario IS NOT NULL

  --------------------------------------------------------------------------
  -- TABLA 2: CD asociados al usuario
  --------------------------------------------------------------------------
  IF (@EsSuper = 1) BEGIN
    SELECT
      IdCentroDistribucion = CD.Id,
      Nombre = ISNULL(CD.Nombre,''),
      CodigoInterno = ISNULL(CD.CodigoInterno, -1)
    FROM 
      CentroDistribucion AS CD
  END ELSE BEGIN
    SELECT
      IdCentroDistribucion = CD.Id,
      Nombre = ISNULL(CD.Nombre,''),
      CodigoInterno = ISNULL(CD.CodigoInterno, -1)
    FROM
      CentroDistribucionPorUsuario AS CDPU
      INNER JOIN CentroDistribucion AS CD ON (CDPU.IdCentroDistribucion = CD.Id)
    WHERE
      CDPU.IdUsuario = @IdUsuario
  END

  --------------------------------------------------------------------------
  -- TABLA 3: Reportabilidad Patentes
  --------------------------------------------------------------------------
  SELECT
    Patente,
    Fecha,
    Estado
  FROM
    vwu_ReportabilidadPatente
  WHERE
    Patente <> ''


END
  