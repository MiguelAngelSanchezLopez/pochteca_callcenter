﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_ValidarConexion')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_ValidarConexion'
  DROP  Procedure  dbo.spu_Sistema_ValidarConexion
END

GO

PRINT  'Creating Procedure spu_Sistema_ValidarConexion'
GO
CREATE Procedure dbo.spu_Sistema_ValidarConexion
/******************************************************************************
**    Descripcion  : Obtiene listado de usuarios
**    Autor        : VSR
**    Fecha        : 15/04/2008
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT TOP 100
    id
  FROM
    Usuario AS U

END
GO

 