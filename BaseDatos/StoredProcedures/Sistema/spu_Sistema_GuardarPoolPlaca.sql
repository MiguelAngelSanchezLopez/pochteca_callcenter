﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_GuardarPoolPlaca')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_GuardarPoolPlaca'
  DROP  Procedure  dbo.spu_Sistema_GuardarPoolPlaca
END

GO

PRINT  'Creating Procedure spu_Sistema_GuardarPoolPlaca'
GO
CREATE Procedure dbo.spu_Sistema_GuardarPoolPlaca
/******************************************************************************
**  Descripcion  : graba los datos del pool de placas
**  Fecha        : 23/08/2017
*******************************************************************************/
@Placa AS VARCHAR(255),
@TipoVehiculo AS VARCHAR(255),
@Transporte AS VARCHAR(255),
@Cedis AS VARCHAR(255),
@Determinante AS INT,
@IdUsuarioCreacion AS INT,
@FechaCarga AS DATETIME,
@NumeroEconomico AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @IdUsuarioTransportista INT, @IdCentroDistribucion INT
    SET @IdUsuarioTransportista = (SELECT TOP 1 IdUsuario FROM Track_Homoclave WHERE Nomenclatura = @Transporte)
    SET @IdCentroDistribucion = (SELECT TOP 1 Id FROM CentroDistribucion WHERE CodigoInterno = @Determinante)

    INSERT INTO PoolPlaca (
        Placa
      , TipoVehiculo
      , Transporte
      , Cedis
      , Determinante
      , IdUsuarioTransportista
      , IdCentroDistribucion
      , IdUsuarioCreacion
      , FechaCarga
      , NumeroEconomico
    )
    VALUES(
        @Placa
      , @TipoVehiculo
      , @Transporte
      , @Cedis
      , @Determinante
      , @IdUsuarioTransportista
      , @IdCentroDistribucion
      , @IdUsuarioCreacion
      , @FechaCarga
      , @NumeroEconomico
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END
GO        