﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_AsociarUsuarioDatosExtras')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_AsociarUsuarioDatosExtras'
  DROP  Procedure  dbo.spu_Alerta_AsociarUsuarioDatosExtras
END

GO

PRINT  'Creating Procedure spu_Alerta_AsociarUsuarioDatosExtras'
GO
CREATE Procedure dbo.spu_Alerta_AsociarUsuarioDatosExtras
/******************************************************************************
**    Descripcion  : asocia datos extras al usuario
**    Autor        : VSR
**    Fecha        : 28/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@Nombre AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @IdUsuarioDatosExtra INT
    SET @IdUsuarioDatosExtra = (SELECT IdUsuario FROM UsuarioDatoExtra WHERE IdUsuario = @IdUsuario)
    IF (@IdUsuarioDatosExtra IS NULL) SET @IdUsuarioDatosExtra = -1

    -------------------------------------------------
    -- si el usuario no existe entonces se crea
    -------------------------------------------------
    IF (@IdUsuarioDatosExtra = -1) BEGIN
      INSERT INTO UsuarioDatoExtra
      (
	      IdUsuario,
	      NombreAsociadoEnAlerta
      )
      VALUES
      (
	      @IdUsuario,
	      @Nombre
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END
      
    ELSE
      UPDATE UsuarioDatoExtra
      SET
	      NombreAsociadoEnAlerta = @Nombre
      WHERE
        IdUsuario = @IdUsuario

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

    END

    SET @Status = @IdUsuario

  COMMIT
END
GO 