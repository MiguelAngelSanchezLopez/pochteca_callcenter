﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerListadoDefinicionAlertas')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerListadoDefinicionAlertas'
  DROP  Procedure  dbo.spu_Alerta_ObtenerListadoDefinicionAlertas
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerListadoDefinicionAlertas'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerListadoDefinicionAlertas
/******************************************************************************
**    Descripcion  : Obtiene listado de definicion de alertas
**    Por          : VSR, 10/04/2015
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(255),
@GestionarPorCemtra AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
	  IdAlertaDefinicion = AD.Id,
	  NombreAlerta = ISNULL(AD.Nombre,''),
    TieneRegistrosAsociados = CASE WHEN (
                                (SELECT COUNT(aux.IdAlertaDefinicion) FROM AlertaDefinicionPorFormato AS aux WHERE aux.IdAlertaDefinicion = AD.Id)
                              ) = 0 THEN '0' ELSE '1' END,
    GestionarPorCemtra = CASE WHEN ISNULL(AD.GestionarPorCemtra, 0) = 0 THEN 'NO' ELSE 'SI' END
  FROM
    AlertaDefinicion AS AD
  WHERE
      (@Nombre = '-1' OR AD.Nombre LIKE '%'+ @Nombre +'%')
  AND (@GestionarPorCemtra = 0 OR AD.GestionarPorCemtra = @GestionarPorCemtra)
  ORDER BY
    IdAlertaDefinicion DESC

END



  