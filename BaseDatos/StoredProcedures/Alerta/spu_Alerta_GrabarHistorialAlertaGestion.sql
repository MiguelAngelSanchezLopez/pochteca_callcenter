﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarHistorialAlertaGestion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarHistorialAlertaGestion'
  DROP  Procedure  dbo.spu_Alerta_GrabarHistorialAlertaGestion
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarHistorialAlertaGestion'
GO
CREATE Procedure dbo.spu_Alerta_GrabarHistorialAlertaGestion
/******************************************************************************
**    Descripcion  : graba el historial de acciones para la alerta
**    Autor        : VSR
**    Fecha        : 14/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaPadre AS INT,
@IdAlertaHija AS INT,
@IdUsuarioCreacion AS INT,
@Accion AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO CallCenterHistorialAlertaGestion
    (
	    IdAlertaPadre,
	    IdAlertaHija,
	    Accion,
	    IdUsuarioCreacion,
	    FechaCreacion
    )
    VALUES
    (
	    @IdAlertaPadre,
	    @IdAlertaHija,
	    @Accion,
	    @IdUsuarioCreacion,
	    dbo.fnu_GETDATE()
    )

    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 