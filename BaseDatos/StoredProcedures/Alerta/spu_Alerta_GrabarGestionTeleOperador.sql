﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarGestionTeleOperador')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarGestionTeleOperador'
  DROP  Procedure  dbo.spu_Alerta_GrabarGestionTeleOperador
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarGestionTeleOperador'
GO
CREATE Procedure dbo.spu_Alerta_GrabarGestionTeleOperador
/******************************************************************************
**    Descripcion  : graba la gestion de la alerta realizada por el teleoperador
**    Autor        : VSR
**    Fecha        : 09/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@IdUsuario AS INT,
@ProximoEscalamiento AS INT,
@NombreAlerta AS VARCHAR(255),
@Explicacion AS VARCHAR(255),
@ExplicacionOtro AS VARCHAR(255),
@Observacion AS VARCHAR(4000),
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    SET @ExplicacionOtro = ISNULL(@ExplicacionOtro,'')
    IF (@ExplicacionOtro <> '') SET @Explicacion = @ExplicacionOtro

    ----------------------------------------------------------
    -- obtiene id del escalamiento que debe registrar
    ----------------------------------------------------------
    DECLARE @IdEscalamientoPorAlerta INT
    SELECT
      @IdEscalamientoPorAlerta = EPA.Id
    FROM
      AlertaConfiguracion AS AC
      INNER JOIN EscalamientoPorAlerta AS EPA ON (AC.Id = EPA.IdAlertaConfiguracion)
      INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
      INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
    WHERE
        AD.Nombre = @NombreAlerta
    AND ADPF.IdFormato = @IdFormato
    AND AC.Activo = 1
    AND EPA.Orden = @ProximoEscalamiento

    ----------------------------------------------------------
    -- graba el registro
    ----------------------------------------------------------
    INSERT INTO CallCenterHistorialEscalamiento
    (
	    IdAlerta,
	    IdEscalamientoPorAlerta,
	    Fecha,
	    Explicacion,
	    Observacion,
      IdUsuarioCreacion
    )
    VALUES
    (
	    @IdAlerta,
	    @IdEscalamientoPorAlerta,
	    dbo.fnu_GETDATE(),
	    @Explicacion,
	    @Observacion,
      @IdUsuario
    )

    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- si la alerta se desactivo antes de terminar de ser gestionada, se vuelve a dejar activa
    UPDATE CallCenterAlerta
    SET
      Desactivada = 0,
      IdUsuarioDesactivacion = NULL,
      FechaDesactivacion = NULL
    WHERE
      IdAlerta = @IdAlerta

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    IF (@ExplicacionOtro <> '') BEGIN
      DECLARE @StatusTipoGeneral VARCHAR(255)
      EXEC spu_Sistema_GrabarTipoGeneral @StatusTipoGeneral OUTPUT, @ExplicacionOtro, 'Explicacion', 'Normal', '-1'
    END

  COMMIT
END
GO 