﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ActualizarAsignacionAlertaEnCola')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ActualizarAsignacionAlertaEnCola'
  DROP  Procedure  dbo.spu_Alerta_ActualizarAsignacionAlertaEnCola
END

GO

PRINT  'Creating Procedure spu_Alerta_ActualizarAsignacionAlertaEnCola'
GO
CREATE Procedure dbo.spu_Alerta_ActualizarAsignacionAlertaEnCola
/******************************************************************************
**    Descripcion  : actualiza la asignacion de la alerta en cola
**    Autor        : VSR
**    Fecha        : 06/10/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaPadre AS INT,
@IdAlertaHija AS INT,
@IdUsuarioAsignada AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @Asignada BIT, @FechaAsignacion DATETIME

    IF (@IdUsuarioAsignada = -1) BEGIN
      SET @Asignada = 0
      SET @IdUsuarioAsignada = NULL
      SET @FechaAsignacion = NULL
    END ELSE BEGIN
      SET @Asignada = 1
      SET @FechaAsignacion = dbo.fnu_GETDATE()
    END

    -- actualiza los registros
    UPDATE AlertaEnColaAtencion
    SET
      Asignada = @Asignada,
      IdUsuarioAsignada = @IdUsuarioAsignada,
      FechaAsignacion = @FechaAsignacion
    WHERE
        IdAlerta = @IdAlertaPadre
    AND IdAlertaHija = @IdAlertaHija

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1

  COMMIT
END
GO 