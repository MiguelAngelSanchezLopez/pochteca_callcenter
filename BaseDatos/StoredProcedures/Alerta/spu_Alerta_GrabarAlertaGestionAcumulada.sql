﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarAlertaGestionAcumulada')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarAlertaGestionAcumulada'
  DROP  Procedure  dbo.spu_Alerta_GrabarAlertaGestionAcumulada
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarAlertaGestionAcumulada'
GO
CREATE Procedure dbo.spu_Alerta_GrabarAlertaGestionAcumulada
/******************************************************************************
**    Descripcion  : graba alerta acumulada
**    Autor        : VSR
**    Fecha        : 14/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@FechaAsignacion AS VARCHAR(50) OUTPUT,
@IdAlertaGestionAcumulada AS INT,
@IdAlerta AS INT,
@IdAlertaHija AS INT,
@IdUsuarioAsignado AS INT,
@Cerrado AS INT,
@CerradoSatisfactorio AS INT,
@IdHistorialEscalamiento AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdPerfil INT, @PERFIL_TELEOPERADOR INT
  SET @PERFIL_TELEOPERADOR = 3
  SET @IdPerfil = (SELECT IdPerfil FROM Usuario WHERE Id = @IdUsuarioAsignado)

  IF (@IdHistorialEscalamiento = -1) SET @IdHistorialEscalamiento = NULL

  BEGIN TRANSACTION
    DECLARE @FechaCerrado DATETIME
    SET @FechaCerrado = NULL
    IF (@Cerrado = 1) SET @FechaCerrado = dbo.fnu_GETDATE()

    -- ingresa nuevo registro
    IF (@IdAlertaGestionAcumulada = -1) BEGIN
      SET @FechaAsignacion = CONVERT(VARCHAR, dbo.fnu_GETDATE(), 121)

      INSERT INTO CallCenterAlertaGestionAcumulada
      (
	      IdAlerta,
	      IdAlertaHija,
	      IdUsuarioAsignado,
	      FechaAsignacion,
        IdHistorialEscalamiento
      )
      VALUES
      (
	      @IdAlerta,
	      @IdAlertaHija,
	      @IdUsuarioAsignado,
	      CONVERT(DATETIME, REPLACE(@FechaAsignacion,'-','')),
        @IdHistorialEscalamiento
      )

      SET @Status = SCOPE_IDENTITY()

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

    END ELSE BEGIN

      UPDATE CallCenterAlertaGestionAcumulada
      SET
        @FechaAsignacion = CONVERT(VARCHAR, FechaAsignacion, 121),
	      FechaTerminoGestion = CASE WHEN (@IdPerfil = @PERFIL_TELEOPERADOR) THEN dbo.fnu_GETDATE() ELSE FechaTerminoGestion END,
        IdHistorialEscalamiento = CASE WHEN (@IdPerfil = @PERFIL_TELEOPERADOR) THEN @IdHistorialEscalamiento ELSE IdHistorialEscalamiento END
      WHERE
	      Id = @IdAlertaGestionAcumulada
        
      SET @Status = @IdAlertaGestionAcumulada

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

    END

    --------------------------------------------------------------------------------------
    -- actualiza el cierre de la alerta padre
    --------------------------------------------------------------------------------------
    UPDATE CallCenterAlerta
    SET
	      FechaCerrado = @FechaCerrado,
	      Cerrado = @Cerrado,
        CerradoSatisfactorio = @CerradoSatisfactorio
    WHERE
	      IdAlerta = @IdAlerta

    --------------------------------------------------------------------------------------
    -- cuando se cierra la alerta, marca todas las alertas hijas como cerradas
    --------------------------------------------------------------------------------------
    IF (@Cerrado = 1) BEGIN
      -- obtiene fecha cierre de la alerta padre
      SET @FechaCerrado = (SELECT FechaCerrado FROM CallCenterAlerta WHERE IdAlerta = @IdAlerta)

      UPDATE CallCenterAlerta
      SET
        FechaCerrado = @FechaCerrado,
	      Cerrado = @Cerrado,
        CerradoSatisfactorio = @CerradoSatisfactorio
      WHERE
	      IdAlertaPadre = @IdAlerta
        
      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END
    END

  COMMIT
END
GO 