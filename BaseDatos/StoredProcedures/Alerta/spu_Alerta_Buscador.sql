﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_Buscador')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_Buscador'
  DROP  Procedure  dbo.spu_Alerta_Buscador
END

GO

PRINT  'Creating Procedure spu_Alerta_Buscador'
GO
CREATE Procedure dbo.spu_Alerta_Buscador
/******************************************************************************
**    Descripcion  : obtiene total de alertas que ingresan al sistema para ser atendidas y las que ya se han gestionado
**    Por          : VSR, 29/09/2014
*******************************************************************************/
@NombreAlerta AS VARCHAR(255),
@NroTransporte AS INT,
@LocalDestino AS VARCHAR(255),
@Prioridad AS VARCHAR(255),
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255),
@NombreTransportista AS VARCHAR(255),
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  SELECT
    T.IdAlerta,
    T.Clasificacion,
    T.NombreAlerta,
    T.NroTransporte,
    T.Prioridad,
    T.NombreTransportista,
    LocalDestino = T.LocalDestino + CASE WHEN T.LocalDestinoCodigo = '' THEN '' ELSE ' - ' + T.LocalDestinoCodigo END,
    T.NombreFormato,
    T.Zona,
    T.TipoViaje,
    Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaCreacion, 'FECHA_COMPLETA'),
    EscalamientosRealizados = ISNULL((
                                SELECT COUNT(aux.Id)
                                FROM
                                  CallCenterHistorialEscalamiento AS aux
                                WHERE
                                    aux.IdAlerta = T.IdAlerta
                                AND aux.IdEscalamientoPorAlerta IS NOT NULL
                              ),0),
    T.Cerrado,
    T.CerradoSatisfactorio,
	  T.AlertaMapa                           
  FROM (
        --------------------------------------------------------------
        -- Alertas atendidas por el callcenter
        --------------------------------------------------------------
        SELECT DISTINCT
          Clasificacion = VW.Clasificacion,
          IdAlerta = VW.IdAlerta,
          NombreAlerta = VW.NombreAlerta,
          NroTransporte = VW.NroTransporte,
          Prioridad = VW.Prioridad,
          NombreTransportista = VW.NombreTransportista,
          LocalDestino = VW.LocalDestino,
          LocalDestinoCodigo = VW.LocalDestinoCodigo,
          NombreFormato = VW.NombreFormato,
          Zona = VW.Zona,
          TipoViaje = VW.TipoViaje,
          FechaCreacion = VW.FechaCreacion,
          AlertaMapa = vw.AlertaMapa,
          Cerrado = VW.Cerrado,
          CerradoSatisfactorio = VW.CerradoSatisfactorio
        FROM 
          vwu_AlertaAtendida AS VW
        WHERE
            (@NombreAlerta = '-1' OR VW.NombreAlerta LIKE '%'+ @NombreAlerta +'%' OR CONVERT(VARCHAR, VW.IdAlerta) = @NombreAlerta)
        AND (@NroTransporte = -1 OR VW.NroTransporte = @NroTransporte)
        AND (@LocalDestino = '-1' OR ( VW.LocalDestino + ' ' + VW.LocalDestinoCodigo LIKE '%'+ @LocalDestino +'%' ) )
        AND (@Prioridad = '-1' OR ( VW.Prioridad = @Prioridad ) )
        AND (VW.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta)
        AND (@NombreTransportista = '-1' OR VW.NombreTransportista LIKE '%'+ @NombreTransportista +'%')
        AND (@IdFormato = -1 OR VW.IdFormato = @IdFormato)

        --------------------------------------------------------------
        -- UNION: Alertas que estan en cola de atencion
        --------------------------------------------------------------
        UNION ALL
        SELECT DISTINCT
          Clasificacion = VW.Clasificacion,
          IdAlerta = VW.IdAlerta,
          NombreAlerta = VW.NombreAlerta,
          NroTransporte = VW.NroTransporte,
          Prioridad = VW.Prioridad,
          NombreTransportista = VW.NombreTransportista,
          LocalDestino = VW.LocalDestino,
          LocalDestinoCodigo = VW.LocalDestinoCodigo,
          NombreFormato = VW.NombreFormato,
          Zona = VW.Zona,
          TipoViaje = VW.TipoViaje,
          FechaCreacion = VW.FechaCreacion,
          AlertaMapa = vw.AlertaMapa,
          Cerrado = VW.Cerrado,
          CerradoSatisfactorio = VW.CerradoSatisfactorio
        FROM
          vwu_AlertaEnColaAtencion VW  
        WHERE
            (@NombreAlerta = '-1' OR VW.NombreAlerta LIKE '%'+ @NombreAlerta +'%' OR CONVERT(VARCHAR, VW.IdAlerta) = @NombreAlerta)
        AND (@NroTransporte = -1 OR VW.NroTransporte = @NroTransporte)
        AND (@LocalDestino = '-1' OR ( VW.LocalDestino + ' ' + VW.LocalDestinoCodigo LIKE '%'+ @LocalDestino +'%' ) )
        AND (@Prioridad = '-1' OR ( VW.Prioridad = @Prioridad ) )
        AND (VW.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta)
        AND (@NombreTransportista = '-1' OR VW.NombreTransportista LIKE '%'+ @NombreTransportista +'%')
        AND (@IdFormato = -1 OR VW.IdFormato = @IdFormato)
  ) AS T
  ORDER BY
    IdAlerta DESC

END   