﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarConfiguracionEscalamiento')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarConfiguracionEscalamiento'
  DROP  Procedure  dbo.spu_Alerta_GrabarConfiguracionEscalamiento
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarConfiguracionEscalamiento'
GO
CREATE Procedure dbo.spu_Alerta_GrabarConfiguracionEscalamiento
/******************************************************************************
**  Descripcion  : graba los datos de la configuracion del escalamiento
**  Fecha        : 06/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdEscalamientoPorAlerta AS INT,
@IdAlertaConfiguracion AS INT,
@Orden AS INT,
@EmailCopia AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    -- ingresa nuevo registro
    IF (@IdEscalamientoPorAlerta = -1) BEGIN
      INSERT INTO EscalamientoPorAlerta
      (
	      IdAlertaConfiguracion,
	      Orden,
	      EmailCopia,
        Eliminado
      )
      VALUES
      (
	      @IdAlertaConfiguracion,
	      @Orden,
	      @EmailCopia,
        0
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE EscalamientoPorAlerta
      SET
	      Orden = @Orden,
	      EmailCopia = @EmailCopia
      WHERE
        Id = @IdEscalamientoPorAlerta

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = @IdEscalamientoPorAlerta

    END

  COMMIT
END
GO        