﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarGestionContactos')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarGestionContactos'
  DROP  Procedure  dbo.spu_Alerta_GrabarGestionContactos
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarGestionContactos'
GO
CREATE Procedure dbo.spu_Alerta_GrabarGestionContactos
/******************************************************************************
**    Descripcion  : graba los contactos de la gestion realizada
**    Autor        : VSR
**    Fecha        : 09/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdHistorialEscalamiento AS INT,
@IdEscalamientoPorAlertaContacto AS INT,
@Explicacion AS VARCHAR(255),
@ExplicacionOtro AS VARCHAR(255),
@Observacion AS VARCHAR(4000),
@IdUsuarioSinGrupoContacto AS INT,
@IdEscalamientoPorAlertaGrupoContacto AS INT,
@categoriaAlerta AS VARCHAR(500),
@tipoObservacion AS VARCHAR(500),
@clasificacionAlerta AS VARCHAR(100),
@clasificacionAlertaSist AS VARCHAR(100)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    SET @ExplicacionOtro = ISNULL(@ExplicacionOtro,'')
    
    IF (@ExplicacionOtro <> '') SET @Explicacion = @ExplicacionOtro
    IF (@IdUsuarioSinGrupoContacto <> -1) SET @IdEscalamientoPorAlertaContacto = NULL
    IF (@IdUsuarioSinGrupoContacto = -1) SET @IdUsuarioSinGrupoContacto = NULL

    ----------------------------------------------------------
    -- graba el registro
    ----------------------------------------------------------
    INSERT INTO CallCenterHistorialEscalamientoContacto
    (
	    IdHistorialEscalamiento,
	    IdEscalamientoPorAlertaContacto,
	    Explicacion,
	    Observacion,
		IdUsuarioSinGrupoContacto,
		IdEscalamientoPorAlertaGrupoContacto,
		CategoriaAlerta,
		TipoObservacion,
		ClasificacionAlerta,
		ClasificacionAlertaSist
    )
    VALUES
    (
	    @IdHistorialEscalamiento,
	    @IdEscalamientoPorAlertaContacto,
	    @Explicacion,
	    @Observacion,
		@IdUsuarioSinGrupoContacto,
		@IdEscalamientoPorAlertaGrupoContacto,
		@categoriaAlerta,
		@tipoObservacion,
		@clasificacionAlerta,
		@ClasificacionAlertaSist
    )

    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

	DECLARE @StatusTipoGeneral VARCHAR(255)
    IF (@ExplicacionOtro <> '') BEGIN
      EXEC spu_Sistema_GrabarTipoGeneral @StatusTipoGeneral OUTPUT, @ExplicacionOtro, 'Explicacion', 'Normal', '-1'
    END

  COMMIT
END
GO 

