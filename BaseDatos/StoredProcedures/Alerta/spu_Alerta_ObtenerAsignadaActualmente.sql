﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAsignadaActualmente')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAsignadaActualmente'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAsignadaActualmente
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAsignadaActualmente'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerAsignadaActualmente
/******************************************************************************
**    Descripcion  : obtiene alerta que tiene asignada actualmente el usuario conectado
**    Por          : VSR, 14/07/2014
*******************************************************************************/
@IdUsuarioAsignado AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT TOP 1
    IdAlertaGestionAcumulada = AGA.Id,
    IdAlerta = AGA.IdAlerta,
    IdAlertaHija = AGA.IdAlertaHija,
    FechaAsignacion = CONVERT(VARCHAR, AGA.FechaAsignacion, 121)
  FROM
    CallCenterAlertaGestionAcumulada AS AGA
    INNER JOIN CallCenterAlerta AS CCA ON (AGA.IdAlerta = CCA.IdAlerta)
  WHERE
      AGA.IdUsuarioAsignado = @IdUsuarioAsignado
  AND AGA.FechaTerminoGestion IS NULL
  AND ISNULL(CCA.Cerrado,0) = 0
  ORDER BY
    AGA.Id

END



  