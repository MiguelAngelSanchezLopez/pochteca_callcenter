﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerDatosAuxiliarPorAccion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerDatosAuxiliarPorAccion'
  DROP  Procedure  dbo.spu_Alerta_ObtenerDatosAuxiliarPorAccion
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerDatosAuxiliarPorAccion'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerDatosAuxiliarPorAccion
/******************************************************************************
**    Descripcion  : obtiene datos de estado auxiliar segun su accion
**    Autor        : VSR
**    Fecha        : 03/11/2014
*******************************************************************************/
@Accion AS VARCHAR(255),
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT TOP 1
    Nombre = ISNULL(TG.Nombre,''),
    Valor =  ISNULL(TG.Valor,''),
    Accion =  ISNULL(TG.Extra1,''),
    FechaAccion = ISNULL((SELECT TOP 1 dbo.fnu_ConvertirDatetimeToDDMMYYYY(aux.FechaCreacion,'FECHA_COMPLETA') FROM CallCenterHistorialAlertaGestion AS aux WHERE aux.Accion = TG.Extra1 AND aux.IdUsuarioCreacion = @IdUsuario ORDER BY aux.FechaCreacion DESC),'')
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'AuxiliarTeleoperador'
  AND TG.Activo = 1
  AND TG.Extra1 = @Accion
  ORDER BY
    Nombre

END



