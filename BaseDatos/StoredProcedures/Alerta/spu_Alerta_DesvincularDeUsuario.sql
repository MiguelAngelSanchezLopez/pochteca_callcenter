﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_DesvincularDeUsuario')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_DesvincularDeUsuario'
  DROP  Procedure  dbo.spu_Alerta_DesvincularDeUsuario
END

GO

PRINT  'Creating Procedure spu_Alerta_DesvincularDeUsuario'
GO
CREATE Procedure dbo.spu_Alerta_DesvincularDeUsuario
/******************************************************************************
**    Descripcion  : desasigna alerta del usuario
**    Autor        : VSR
**    Fecha        : 15/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaGestionAcumulada AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM CallCenterAlertaGestionAcumulada WHERE Id = @IdAlertaGestionAcumulada
    SET @Status = 1

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 