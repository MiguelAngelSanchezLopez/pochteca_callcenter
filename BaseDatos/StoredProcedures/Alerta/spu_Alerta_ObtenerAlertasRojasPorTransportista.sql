﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAlertasRojasPorTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAlertasRojasPorTransportista'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAlertasRojasPorTransportista
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAlertasRojasPorTransportista'
GO
CREATE Procedure spu_Alerta_ObtenerAlertasRojasPorTransportista  
/***********************************************************************************  
**    Descripcion  : obtiene listado alertas rojas agrupados por transportista  
**    Por          : DG, 10/12/2014  
*************************************************************************************/  
@FechaDesde AS VARCHAR(255),  
@FechaHasta AS VARCHAR(255),  
@Gestionadas AS BIT  
AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'  
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'  
  
  SET @FechaDesde = @FechaDesde + ' 00:00'  
  SET @FechaHasta = @FechaHasta + ' 23:59'  
    
  SELECT    
    NombreTransportista = ISNULL(a.NombreTransportista,''),  
    RutTransportista = ISNULL(T.Rut,'-1'),
    FechaDesde = dbo.fnu_ConvertirDatetimeToDDMMYYYY(MIN(a.FechaHoraCreacion), 'FECHA_COMPLETA'),  
    FechaHasta = dbo.fnu_ConvertirDatetimeToDDMMYYYY(MAX(a.FechaHoraCreacion), 'FECHA_COMPLETA'),  
    CantidadAlerta = COUNT(DISTINCT A.id),  
    CorreoTransportista = ISNULL(T.Email,'')  
  FROM 
    CallCenterAlerta AS CCA
    INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN CallCenterHistorialEscalamiento he ON he.IdAlerta = a.Id  
    INNER JOIN TrazaViaje tv ON (TV.NroTransporte = A.NroTransporte AND TV.LocalDestino = a.LocalDestino)
    INNER JOIN (
                SELECT
                  T.Nombre,
                  T.Rut,
                  Email = CASE
                            WHEN T.Email <> '' AND T.EmailConCopia = '' THEN T.Email
                            WHEN T.Email = '' AND T.EmailConCopia <> '' THEN T.EmailConCopia
                            WHEN T.Email <> '' AND T.EmailConCopia <> '' THEN T.Email + ';' + T.EmailConCopia
                          ELSE
          	                 ''
                          END
                FROM (
                        SELECT
                          U.Email, 
                          U.Nombre, 
                          Rut = CASE WHEN ISNULL(U.Rut,'') = '' THEN '-1' ELSE ISNULL(U.Rut,'') + ISNULL('-' + U.Dv,'') END,
                          EmailConCopia = CONVERT(VARCHAR(8000),ISNULL(UDE.EmailConCopia,''))
                        FROM 
                          Usuario AS U
                          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
                          LEFT JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
                        WHERE
                          P.Llave = 'TRA'
                ) AS T
    ) AS T ON ( RTRIM(LTRIM(REPLACE(REPLACE(TV.RutTransportista,' ',''),'.',''))) = T.Rut )
    LEFT JOIN AlertaRojaHistorialEstado ARHE ON ARHE.NroTransporte = A.NroTransporte AND ARHE.Estado = 'ENVIADO TRANSPORTISTA'  
  WHERE   
      he.ClasificacionAlerta = 'Roja'
  AND a.NroTransporte NOT IN (  -- Numeros de viajes que poseen alertas rojas sin gestionar  
                              SELECT
                                arb.NroTransporte  
                              FROM
                                AlertaRojaBatch arb  
                                LEFT JOIN CallCenterHistorialEscalamiento he ON (HE.IdAlerta = arb.IdAlerta AND he.idEscalamientoPorAlerta IS NULL)  
                                WHERE he.Id IS null  
  )
  AND (tv.EstadoViaje = 'EN LOCAL-AP' OR tv.EstadoViaje = 'EnLocal-R' OR tv.EstadoViaje = 'EnLocal-P')  
  AND (  
        -- SIN GESTIONADAS  
        (@Gestionadas = 0 AND ARHE.NroTransporte IS NULL)  
        -- GESTIONADAS  
        OR (@Gestionadas = 1 AND ARHE.NroTransporte IS NOT NULL)  
  )  
  AND a.FechaHoraCreacion BETWEEN @FechaDesde AND @FechaHasta
  GROUP BY   
    A.NombreTransportista, T.Rut, T.Email  
  ORDER BY   
    A.NombreTransportista  
   
END
