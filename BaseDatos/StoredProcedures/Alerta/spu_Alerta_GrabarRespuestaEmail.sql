﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarRespuestaEmail')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarRespuestaEmail'
  DROP  Procedure  dbo.spu_Alerta_GrabarRespuestaEmail
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarRespuestaEmail'
GO
CREATE Procedure spu_Alerta_GrabarRespuestaEmail
/******************************************************************************
**    Descripcion  : graba la respuesta a la alerta enviada por email
**    Autor        : DG
**    Fecha        : 04/11/2014
*******************************************************************************/
@IdAlerta AS INT,
@IdRespuestaCategoria AS INT,
@Observacion AS VARCHAR(1000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    ----------------------------------------------------------
    -- graba el registro
    ----------------------------------------------------------
    INSERT INTO RespuestaAlerta
    (
	    IdAlerta,
	    Observacion,
	    FechaCreacion,
	    idRespuestaCategoria
    )
    VALUES
    (
	    @IdAlerta,
	    @Observacion,
	    dbo.fnu_GETDATE(),
		@IdRespuestaCategoria
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
    END

  COMMIT
END