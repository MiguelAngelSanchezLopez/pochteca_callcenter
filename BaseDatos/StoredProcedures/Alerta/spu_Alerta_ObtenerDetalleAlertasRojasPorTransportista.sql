﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerDetalleAlertasRojasPorTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerDetalleAlertasRojasPorTransportista'
  DROP  Procedure  dbo.spu_Alerta_ObtenerDetalleAlertasRojasPorTransportista
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerDetalleAlertasRojasPorTransportista'
GO
CREATE Procedure spu_Alerta_ObtenerDetalleAlertasRojasPorTransportista
/******************************************************************************
**    Descripcion  : obtiene el detalle de alertas rojas por transportistas
**    Por          : DG, 11/12/2014
*******************************************************************************/
@RutTransportista  AS VARCHAR(255),
@Gestionadas AS BIT
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	-----------------------------------------------------------------------------
	--TABLE 0: Lista numeros de viajes de alertas rojas
	-----------------------------------------------------------------------------
	SELECT DISTINCT 
		NroTransporte = A.NroTransporte,
		NombreTransportista = a.NombreTransportista,
		TiempoRespuestaTransportista = ISNULL((SELECT DATEDIFF(HOUR,ARHE.FechaCreacion,MAX(ARHE2.FechaCreacion)) FROM AlertaRojaHistorialEstado ARHE2 WHERE ARHE2.NroTransporte = A.NroTransporte AND ARHE2.Estado = 'ENVIADO OPERACION TRANSPORTE'),''),
		MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0)
	FROM 
  CallCenterAlerta AS CCA
  INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
	INNER JOIN CallCenterHistorialEscalamiento HE ON HE.IdAlerta = A.Id
	INNER JOIN TrazaViaje tv ON TV.NroTransporte = A.NroTransporte AND tv.LocalDestino = a.LocalDestino 
	LEFT JOIN AlertaRojaHistorialEstado ARHE ON (ARHE.NroTransporte = A.NroTransporte AND ARHE.Estado = 'ENVIADO TRANSPORTISTA')
	WHERE 
		(HE.ClasificacionAlerta = 'Roja')
		AND a.NroTransporte NOT IN (  -- Numeros de viajes que poseen alertas rojas sin gestionar  
										SELECT arb.NroTransporte  
										FROM AlertaRojaBatch arb  
										LEFT JOIN CallCenterHistorialEscalamiento he ON (HE.IdAlerta = arb.IdAlerta AND he.idEscalamientoPorAlerta IS NULL)  
										WHERE he.Id IS null  
								  )   
		AND (tv.EstadoViaje = 'EN LOCAL-AP' OR tv.EstadoViaje = 'EnLocal-R' OR tv.EstadoViaje = 'EnLocal-P')
		AND (
			-- SIN GESTIONAR
			(@Gestionadas = 0 AND ARHE.NroTransporte IS NULL)
			-- GESTIONADAS
			OR (@Gestionadas = 1 AND ARHE.NroTransporte IS NOT NULL)
		)
		AND RTRIM(LTRIM(REPLACE(REPLACE(A.RutTransportista,' ',''),'.',''))) = @RutTransportista
	
	-----------------------------------------------------------------------------
	--TABLE 1: Lista detalle de alertas rojas
	-----------------------------------------------------------------------------
	SELECT DISTINCT
		NroTransporte = a.NroTransporte,
		IdAlerta = a.Id,
		PatenteTracto = a.PatenteTracto,
		NombreTransportista = a.NombreTransportista,
		NombreConductor = a.NombreConductor,
		CategoriaAlerta = he.CategoriaAlerta,
		FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),
		Observacion = he.Observacion,
		CDOrigen = tv.OrigenDescripcion,
		LocalDestino = a.LocalDestino,
		UrlMapa = a.AlertaMapa
	FROM 
  CallCenterAlerta AS CCA
  INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
	INNER JOIN CallCenterHistorialEscalamiento he ON HE.IdAlerta = A.Id
	INNER JOIN TrazaViaje tv ON TV.NroTransporte = A.NroTransporte AND tv.LocalDestino = a.LocalDestino
	LEFT JOIN AlertaRojaHistorialEstado ARHE ON (ARHE.NroTransporte = A.NroTransporte AND ARHE.Estado = 'ENVIADO TRANSPORTISTA')
	WHERE 
		he.ClasificacionAlerta = 'Roja'
		AND a.NroTransporte NOT IN (  -- Numeros de viajes que poseen alertas rojas sin gestionar
								SELECT arb.NroTransporte
								FROM AlertaRojaBatch arb
								LEFT JOIN CallCenterHistorialEscalamiento he ON (HE.IdAlerta = arb.IdAlerta AND he.idEscalamientoPorAlerta IS NULL)
								WHERE he.Id IS null
		)	
		AND (tv.EstadoViaje = 'EN LOCAL-AP' OR tv.EstadoViaje = 'EnLocal-R' OR tv.EstadoViaje = 'EnLocal-P')
		AND (
			-- SIN GESTIONAR
			(@Gestionadas = 0 AND ARHE.NroTransporte IS NULL)
			-- GESTIONADAS
			OR (@Gestionadas = 1 AND ARHE.NroTransporte IS NOT NULL)
		)
		AND RTRIM(LTRIM(REPLACE(REPLACE(A.RutTransportista,' ',''),'.',''))) = @RutTransportista
END
