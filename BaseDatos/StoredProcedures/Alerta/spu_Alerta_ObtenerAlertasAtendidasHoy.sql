﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAlertasAtendidasHoy')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAlertasAtendidasHoy'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAlertasAtendidasHoy
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAlertasAtendidasHoy'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerAlertasAtendidasHoy
/******************************************************************************
**    Descripcion  : obtiene listado de alertas atendidas el dia de hoy
**    Por          : VSR, 07/07/2014
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @FechaHoy VARCHAR(255)
  SET @FechaHoy = dbo.fnu_ConvertirDatetimeToDDMMYYYY(dbo.fnu_GETDATE(), 'SOLO_FECHA')
  
  SELECT DISTINCT
    IdAlerta = A.Id,
    Fecha = (SELECT TOP 1 dbo.fnu_ConvertirDatetimeToDDMMYYYY(aux.Fecha, 'FECHA_COMPLETA') FROM CallCenterHistorialEscalamiento AS aux WHERE aux.IdAlerta = A.Id ORDER BY aux.Fecha DESC),
    NroTransporte = A.NroTransporte,
    NombreAlerta = A.DescripcionAlerta,
    Prioridad = Ac.Prioridad,
    NroEscalamiento = ISNULL((
    	                          SELECT TOP 1
    	                            auxEPA.Orden
    	                          FROM
    	                            CallCenterHistorialEscalamiento AS auxHE
    	                            INNER JOIN EscalamientoPorAlerta AS auxEPA ON (auxHE.IdEscalamientoPorAlerta = auxEPA.Id)
    	                          WHERE
    	                            auxHE.IdAlerta = A.Id 
    	                          ORDER BY
    	                            auxEPA.Orden DESC
                      ),0),
    Cerrado = CONVERT(INT,ISNULL(CCA.Cerrado,0))
  FROM
    CallCenterAlerta AS CCA
    INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
    INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN CallCenterHistorialEscalamiento AS HE ON (A.Id = HE.IdAlerta)
  WHERE
      AC.Activo = 1
  AND CCA.Desactivada = 0
  AND dbo.fnu_ConvertirDatetimeToDDMMYYYY(HE.Fecha, 'SOLO_FECHA') = @FechaHoy
  ORDER BY
    Fecha DESC

END



  