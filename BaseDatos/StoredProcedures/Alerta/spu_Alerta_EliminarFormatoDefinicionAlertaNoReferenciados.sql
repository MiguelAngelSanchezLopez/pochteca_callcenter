﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarFormatoDefinicionAlertaNoReferenciados')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarFormatoDefinicionAlertaNoReferenciados'
  DROP  Procedure  dbo.spu_Alerta_EliminarFormatoDefinicionAlertaNoReferenciados
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarFormatoDefinicionAlertaNoReferenciados'
GO
CREATE Procedure dbo.spu_Alerta_EliminarFormatoDefinicionAlertaNoReferenciados
/******************************************************************************
**  Descripcion  : elimina los formatos que no tengan nada relacionado con la definicion de la alerta
**  Fecha        : 13/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaDefinicion AS INT,
@Listado AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    --------------------------------------------------------------------------
    -- elimina todos los formatos que no tengan definiciones de alertas usadas para la configuracion de alertas
    --------------------------------------------------------------------------
    DELETE FROM AlertaDefinicionPorFormato
    FROM
      AlertaDefinicionPorFormato AS ADPF
      INNER JOIN (
                    ------------------------------------------------
                    -- busca si hay alertas configuradas con algunas de las definiciones de los formatos
                    ------------------------------------------------
                    SELECT
                      IdAlertaDefinicionPorFormato = ADPF.Id,
                      TieneRegistrosAsociados = CASE WHEN (
                                                  (SELECT COUNT(aux.Id) FROM AlertaConfiguracion AS aux WHERE aux.IdAlertaDefinicionPorFormato = ADPF.Id)
                                                ) = 0 THEN 0 ELSE 1 END
                    FROM
                      AlertaDefinicionPorFormato AS ADPF
                    WHERE
                        ADPF.IdAlertaDefinicion = @IdAlertaDefinicion 
                    AND ADPF.IdFormato NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
      ) AS T ON (ADPF.Id = T.IdAlertaDefinicionPorFormato)
    WHERE
      T.TieneRegistrosAsociados = 0  

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO