﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerCargoPorCentroDistribucionyPerfil')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerCargoPorCentroDistribucionyPerfil'
  DROP  Procedure  dbo.spu_Alerta_ObtenerCargoPorCentroDistribucionyPerfil
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerCargoPorCentroDistribucionyPerfil'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerCargoPorCentroDistribucionyPerfil
/******************************************************************************
**    Descripcion  : obtiene listado de cargo por centro distribucion y perfil seleccionado
**    Autor        : VSR
**    Fecha        : 16/04/2015
*******************************************************************************/
@CDOrigenCodigo AS VARCHAR(255),
@LlavePerfil AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdUsuario INT
  SET @CDOrigenCodigo = UPPER(RTRIM(LTRIM(@CDOrigenCodigo)))

  -----------------------------------------------------------------------------
  -- se mantiene la misma estructura de campos que tiene el select de la TABLA 3: Contactos Proximo Escalamiento
  -- del procedimiento spu_Alerta_ObtenerDetalle
  -----------------------------------------------------------------------------
  DECLARE @Tabla AS TABLE(IdEscalamientoPorAlertaContacto INT, NombreUsuario VARCHAR(1000), Telefono VARCHAR(255), Email VARCHAR(255), Cargo VARCHAR(255),
                          TipoGrupo VARCHAR(255))

  INSERT INTO @Tabla(IdEscalamientoPorAlertaContacto, NombreUsuario, Telefono, Email, Cargo, TipoGrupo)
  SELECT DISTINCT
    IdEscalamientoPorAlertaContacto = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL((SELECT aux.Telefono FROM TelefonosPorUsuario AS aux WHERE aux.IdUsuario = U.Id AND REPLACE(LEFT(CONVERT(VARCHAR,dbo.fnu_GETDATE(),108),5),':','') BETWEEN aux.HoraInicio AND aux.HoraTermino), ISNULL(U.Telefono,'')),
    Email = ISNULL(U.Email,''),
    Cargo = ISNULL(P.Nombre,''),
    TipoGrupo = CASE WHEN PPF.NombrePerfil IS NULL THEN 'Personalizado' ELSE PPF.Tipo END
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    INNER JOIN CentroDistribucionPorUsuario AS CDPU ON (U.Id = CDPU.IdUsuario)
    INNER JOIN CentroDistribucion AS CD ON (CDPU.IdCentroDistribucion = CD.Id)
    LEFT JOIN vwu_PerfilesPorFormato AS PPF ON (P.Nombre = PPF.NombrePerfil)
  WHERE
      U.Estado = 1
  AND P.Llave = @LlavePerfil
  AND CD.CodigoInterno = @CDOrigenCodigo
  ORDER BY
    NombreUsuario

  -- cuenta cuantos registos encontro, si no encontro ninguna devuelve todos los Gerentes de Mercado
  DECLARE @TotalRegistros AS INT
  SET @TotalRegistros = (SELECT COUNT(IdEscalamientoPorAlertaContacto) FROM @Tabla)
  
  IF (@TotalRegistros > 0) BEGIN
    SELECT * FROM @Tabla
  END ELSE BEGIN
    EXEC spu_Alerta_ObtenerUsuariosPorPerfil -1, @LlavePerfil
  END

END



