﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarAlertaEnColaAtencion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarAlertaEnColaAtencion'
  DROP  Procedure  dbo.spu_Alerta_EliminarAlertaEnColaAtencion
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarAlertaEnColaAtencion'
GO
CREATE Procedure dbo.spu_Alerta_EliminarAlertaEnColaAtencion
/******************************************************************************
**    Descripcion  : elimina la alerta gestionada de la cola de atencion
**    Autor        : VSR
**    Fecha        : 06/10/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaPadre AS INT,
@IdAlertaHija AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    -- traspasa registro al historial de alerta en cola de atencion
    INSERT INTO HistorialAlertaEnColaAtencion
    SELECT * FROM AlertaEnColaAtencion
    WHERE
        IdAlerta = @IdAlertaPadre
    AND IdAlertaHija = @IdAlertaHija
    
    -- elimina registro de la cola de atencion
    DELETE FROM AlertaEnColaAtencion
    WHERE
        IdAlerta = @IdAlertaPadre
    AND IdAlertaHija = @IdAlertaHija

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1

  COMMIT
END
GO 