﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarHistorialLlamadaCallCenter')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarHistorialLlamadaCallCenter'
  DROP  Procedure  dbo.spu_Alerta_GrabarHistorialLlamadaCallCenter
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarHistorialLlamadaCallCenter'
GO
CREATE Procedure dbo.spu_Alerta_GrabarHistorialLlamadaCallCenter
/******************************************************************************
**    Descripcion  : graba el historial de la llamada del callcenter
**    Autor        : VSR
**    Fecha        : 10/09/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdLlamada AS VARCHAR(255),
@IdAlertaPadre AS INT,
@IdAlertaHija AS INT,
@IdEscalamientoPorAlertaGrupoContacto AS INT,
@NombreContacto AS VARCHAR(255),
@Telefono AS VARCHAR(255),
@DestinoLocal AS VARCHAR(255),
@DestinoLocalCodigo AS VARCHAR(255),
@Accion AS VARCHAR(255),
@IdUsuarioCreacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    IF (@IdAlertaPadre = -1) SET @IdAlertaPadre = NULL
    IF (@IdAlertaHija = -1) SET @IdAlertaHija = NULL
    IF (@IdEscalamientoPorAlertaGrupoContacto = -1) SET @IdEscalamientoPorAlertaGrupoContacto = NULL
    IF (@IdUsuarioCreacion = -1) SET @IdUsuarioCreacion = NULL

    INSERT CallCenterHistorialLlamadaEjecutivo
    (
	    IdLlamada,
	    IdAlertaPadre,
	    IdAlertaHija,
	    IdEscalamientoPorAlertaGrupoContacto,
	    NombreContacto,
	    Telefono,
	    DestinoLocal,
	    DestinoLocalCodigo,
	    Accion,
	    IdUsuarioCreacion,
	    FechaCreacion
    )
    VALUES
    (
	    @IdLlamada,
	    @IdAlertaPadre,
	    @IdAlertaHija,
	    @IdEscalamientoPorAlertaGrupoContacto,
	    @NombreContacto,
	    @Telefono,
	    @DestinoLocal,
	    @DestinoLocalCodigo,
	    @Accion,
	    @IdUsuarioCreacion,
	    dbo.fnu_GETDATE()
    )

    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 