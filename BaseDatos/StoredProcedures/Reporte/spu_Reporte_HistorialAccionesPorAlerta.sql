﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_HistorialAccionesPorAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_HistorialAccionesPorAlerta'
  DROP  Procedure  dbo.spu_Reporte_HistorialAccionesPorAlerta
END

GO

PRINT  'Creating Procedure spu_Reporte_HistorialAccionesPorAlerta'
GO
CREATE Procedure dbo.spu_Reporte_HistorialAccionesPorAlerta
/******************************************************************************
**    Descripcion  : obtiene el historial de acciones realizadas en cada alerta
**    Por          : VSR, 26/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  SELECT
    [ID ALERTA] = T.IdAlerta,
    [FORMATO] = T.NombreFormato,
    ALERTA = T.NombreAlerta,
    IDMASTER = T.NroTransporte,
    IDEMBARQUE = T.IdEmbarque,
    PRIORIDAD = T.Prioridad,
    [LINEA DE TRANSPORTE] = T.NombreTransportista,
    [TIENDA DESTINO] = T.LocalDestino,
    [DETERMINANTE DESTINO] = T.LocalDestinoCodigo,
    [ZONA] = T.Zona,
    [TIPO VIAJE] = T.TipoViaje,
    [FECHA ALERTA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaCreacion, 'FECHA_COMPLETA'),
    [ACCION] = T.Accion,
    [CREADO POR] = T.CreadoPor,
    [FECHA ACCION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaAccion, 'FECHA_COMPLETA')
  FROM (
        SELECT DISTINCT
          IdAlerta = A.Id,
          NombreAlerta = A.DescripcionAlerta,
          NroTransporte = A.NroTransporte,
          IdEmbarque = A.IdEmbarque,
          Prioridad = AC.Prioridad,
          NombreTransportista = ISNULL(A.NombreTransportista,''),
          LocalDestino = ISNULL(L.Nombre,''),
          LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
          IdFormato = L.IdFormato,
          NombreFormato = ISNULL(F.Nombre,''),
          Zona = ISNULL(A.Permiso,''),
          TipoViaje = ISNULL(TV.TipoViaje,''),
          FechaCreacion = A.FechaHoraCreacion,
          CreadoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
          Accion = HAG.Accion,
          FechaAccion = HAG.FechaCreacion,
          GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
        FROM
          CallCenterHistorialAlertaGestion AS HAG
          INNER MERGE JOIN CallCenterAlerta AS CCA ON (HAG.IdAlertaPadre = CCA.IdAlerta)
          INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
          INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
          INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
          INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
          INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
          INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
          INNER JOIN Usuario AS U ON (HAG.IdUsuarioCreacion = U.Id)
          LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
        WHERE
          CCA.Desactivada = 0
  ) AS T
  WHERE
      ( T.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( T.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  ORDER BY
    [ID ALERTA], [IDMASTER], T.FechaAccion

END