﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_AlertasEntrantes')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_AlertasEntrantes'
  DROP  Procedure  dbo.spu_Reporte_AlertasEntrantes
END

GO

PRINT  'Creating Procedure spu_Reporte_AlertasEntrantes'
GO
CREATE Procedure dbo.spu_Reporte_AlertasEntrantes
/******************************************************************************
**    Descripcion  : obtiene total de alertas que ingresan al sistema para ser atendidas y las que ya se han gestionado
**    Por          : VSR, 25/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  --------------------------------------------------------------
  -- Alertas atendidas por el callcenter
  --------------------------------------------------------------
  SELECT DISTINCT
    CLASIFICACION = AA.Clasificacion,
    [ID ALERTA] = AA.IdAlerta,
    [FORMATO] = AA.NombreFormato,
    ALERTA = AA.NombreAlerta,
    IDMASTER = AA.NroTransporte,
    IDEMBARQUE = AA.IdEmbarque,
    PRIORIDAD = AA.Prioridad,
    [LINEA DE TRANSPORTE] = AA.NombreTransportista,
    [TIENDA DESTINO] = AA.LocalDestino,
    [DETERMINANTE DESTINO] = AA.LocalDestinoCodigo,
    [ZONA] = AA.Zona,
    [TIPO VIAJE] = AA.TipoViaje,
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(AA.FechaCreacion, 'FECHA_COMPLETA'),
    ESCALAMIENTO = CONVERT(VARCHAR,AA.NroEscalamiento) + CASE WHEN AA.Eliminado = 1 THEN ' (eliminado)' ELSE '' END,
    [ATENDIDO POR] = AA.AtendidoPor
  FROM
    vwu_AlertaAtendida AS AA
  WHERE
      ( AA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad

  --------------------------------------------------------------
  -- UNION: Alertas que estan en cola de atencion
  --------------------------------------------------------------
  UNION ALL
  SELECT DISTINCT
    CLASIFICACION = AECA.Clasificacion,
    [ID ALERTA] = AECA.IdAlerta,
    [FORMATO] = AECA.NombreFormato,
    ALERTA = AECA.NombreAlerta,
    IDMASTER = AECA.NroTransporte,
    IDEMBARQUE = AECA.IdEmbarque,
    PRIORIDAD = AECA.Prioridad,
    [LINEA DE TRANSPORTE] = AECA.NombreTransportista,
    [TIENDA DESTINO] = AECA.LocalDestino,
    [DETERMINANTE DESTINO] = AECA.LocalDestinoCodigo,
    [ZONA] = AECA.Zona,
    [TIPO VIAJE] = AECA.TipoViaje,
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(AECA.FechaCreacion, 'FECHA_COMPLETA'),
    ESCALAMIENTO = '',
    [ATENDIDO POR] = ''
  FROM
    vwu_AlertaEnColaAtencion AECA
  WHERE
      ( AECA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AECA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  ORDER BY
    CLASIFICACION, [IDMASTER], [ID ALERTA], [ESCALAMIENTO]

END
