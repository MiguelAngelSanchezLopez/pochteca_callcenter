﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_AlertasContactadas')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_AlertasContactadas'
  DROP  Procedure  dbo.spu_Reporte_AlertasContactadas
END

GO

PRINT  'Creating Procedure spu_Reporte_AlertasContactadas'
GO
CREATE Procedure dbo.spu_Reporte_AlertasContactadas
/******************************************************************************
**    Descripcion  : obtiene total de alertas que han sido contactadas
**    Por          : VSR, 25/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  --------------------------------------------------------------
  -- filtra solo las alertas que fueron contactadas (NoContesta=0)
  --------------------------------------------------------------
  SELECT
    [ID ALERTA] = ACE.IdAlerta, 
    [FORMATO] = ACE.NombreFormato,
    ALERTA = ACE.NombreAlerta, 
    IDMASTER = ACE.NroTransporte, 
    IDEMBARQUE = ACE.IdEmbarque,
    PRIORIDAD = ACE.Prioridad, 
    [LINEA DE TRANSPORTE] = ACE.NombreTransportista, 
    ZONA = ACE.Zona,
    [TIENDA DESTINO] = ACE.LocalDestino,
    [DETERMINANTE DESTINO] = ACE.LocalDestinoCodigo,
    [TIPO VIAJE] = ACE.TipoViaje, 
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(ACE.FechaCreacion, 'FECHA_COMPLETA'),
    ESCALAMIENTO = CONVERT(VARCHAR,ACE.NroEscalamiento) + CASE WHEN ACE.Eliminado = 1 THEN ' (eliminado)' ELSE '' END,
    CARGO = ACE.GrupoContacto,
    [NOMBRE CONTACTO] = ACE.NombreContacto,
    [ATENDIDO POR] = ACE.AtendidoPor,
    EXPLICACION = ACE.Explicacion,
    OBSERVACION = ACE.Observacion
  FROM
    vwu_AlertaContactoEfectivo AS ACE
  WHERE
      ( ACE.NoContesta = 0 )
  AND ( ACE.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( ACE.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  ORDER BY
    [IDMASTER], [ID ALERTA], [ESCALAMIENTO], ACE.IdHistorialeEcalamientoContacto


END
