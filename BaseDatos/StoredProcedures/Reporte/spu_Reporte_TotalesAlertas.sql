﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_TotalesAlertas')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_TotalesAlertas'
  DROP  Procedure  dbo.spu_Reporte_TotalesAlertas
END

GO

PRINT  'Creating Procedure spu_Reporte_TotalesAlertas'
GO
CREATE Procedure dbo.spu_Reporte_TotalesAlertas
/******************************************************************************
**    Descripcion  : obtiene resumen con el total de alertas por clasificacion
**    Por          : VSR, 26/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  ---------------------------------------------------------------------
  -- total alertas atendidas
  ---------------------------------------------------------------------
  DECLARE @totalAlertasAtendidas INT
  SELECT
    @totalAlertasAtendidas = COUNT(DISTINCT AA.IdAlerta)
  FROM (
        SELECT DISTINCT
        IdAlerta = A.Id,
        FechaCreacion = A.FechaHoraCreacion,
        GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
        FROM
          CallCenterHistorialEscalamiento AS HE
          INNER MERGE JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
          INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
          INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
          INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
          INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
          INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
          INNER JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
        WHERE
          CCA.Desactivada = 0
  ) AS AA
  WHERE
      ( AA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  IF (@totalAlertasAtendidas IS NULL) SET @totalAlertasAtendidas = 0


  ---------------------------------------------------------------------
  -- total alertas en cola atencion
  ---------------------------------------------------------------------
  DECLARE @totalAlertasEnColaAtencion INT
  SELECT
    @totalAlertasEnColaAtencion = COUNT(DISTINCT AECA.IdAlerta)
  FROM
    vwu_AlertaEnColaAtencion AECA
  WHERE
      ( AECA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AECA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  IF (@totalAlertasEnColaAtencion IS NULL) SET @totalAlertasEnColaAtencion = 0


  ---------------------------------------------------------------------
  -- TABLA 0: devuelve los totales
  ---------------------------------------------------------------------
  SELECT
    [ALERTAS] = 'ENTRANTES',
    [TOTAL] = @totalAlertasAtendidas + @totalAlertasEnColaAtencion

  UNION ALL
  SELECT
    [ALERTAS] = 'CONTACTADAS',
    [TOTAL] = @totalAlertasAtendidas

  UNION ALL
  SELECT
    [ALERTAS] = 'EN COLA DE ATENCION',
    [TOTAL] = @totalAlertasEnColaAtencion
   

END
