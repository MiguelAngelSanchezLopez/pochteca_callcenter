﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_AlertasRojasCerradas')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_AlertasRojasCerradas'
  DROP  Procedure  dbo.spu_Reporte_AlertasRojasCerradas
END
GO

PRINT  'Creating Procedure spu_Reporte_AlertasRojasCerradas'
GO
CREATE Procedure [dbo].[spu_Reporte_AlertasRojasCerradas]
/***********************************************************************************
**    Descripcion  : obtiene el reporte para las alertas rojas que han sido cerradas
**    Por          : DG, 23/01/2015
************************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON
  
  
  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'
  
  SELECT
	[FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),
	[IDMASTER] = A.NroTransporte,
	[LINEA DE TRANSPORTE] = A.NombreTransportista,
	[PLACA TRACTO] = A.PatenteTracto,
	[PLACA REMOLQUE] = A.PatenteTrailer,
	[NOMBRE OPERADOR] = UPPER(A.NombreConductor),
  [RUT OPERADOR] = CASE WHEN ISNULL(C.Rut,'') = '' THEN '' ELSE C.Rut + '-' + C.Dv END,
  [TIPO ALERTA] = A.TipoAlerta,
  [MONTO DISCREPANCIA] = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0),
  [OBSERVACION C.F] = (SELECT Observacion FROM CallCenterHistorialEscalamiento HE WHERE HE.IdAlerta = A.Id AND HE.IdEscalamientoPorAlerta IS NULL),
  [RESPUESTA LINEA DE TRANSPORTE] = ARR.Observacion,
  [RECHAZO] = CASE WHEN ISNULL(ARR.Rechazo, 0) = 1 THEN 'SI' ELSE 'NO' END
  FROM
    Alerta A
	  INNER JOIN AlertaRojaEstadoActual ARE ON ARE.NroTransporte =  A.NroTransporte
	  INNER JOIN AlertaRojaRespuesta ARR ON arr.IdAlerta = A.Id
	  LEFT JOIN Conductores c ON (c.Nombre = a.NombreConductor) 
  WHERE
	    ARE.Estado = 'FINALIZADO'
  AND A.FechaHoraCreacion BETWEEN @FechaDesde AND @FechaHasta
END