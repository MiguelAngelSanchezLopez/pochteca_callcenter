IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_Permisos')
	BEGIN
	  PRINT  'Dropping Procedure vwu_Permisos'
		DROP  View dbo.vwu_Permisos
	END
GO

PRINT  'Creating Procedure vwu_Permisos'
GO
CREATE VIEW dbo.vwu_Permisos
/******************************************************************************
**    Descripcion  : Obtiene matriz de permisos por entidades
**    Por          : VSR, 30/07/2009
*******************************************************************************/
AS
 
  -- Permisos de la entidad USUARIO
  SELECT
	  IdUsuario = FPxU.IdUsuario,
	  IdCategoria = CP.Id,
	  Categoria = CP.Nombre,
	  OrdenCategoria = CP.Orden,
    IdPagina = P.Id,
    TituloPagina = P.Titulo,
    NombreArchivo = P.NombreArchivo,
    MostrarEnMenu = P.MostrarEnMenu,
    OrdenPagina = P.Orden,
    EstadoPagina = P.Estado,
	  IdFuncionPagina = FP.Id,
	  NombreFuncion = FP.Nombre,
	  LlaveIdentificador = FP.LlaveIdentificador,
	  OrdenFuncion = FP.Orden
  FROM
	  FuncionesPaginaXUsuario AS FPxU
	  INNER MERGE JOIN FuncionPagina AS FP ON (FPxU.IdFuncionPagina = FP.Id)
	  INNER JOIN Pagina AS P ON (FP.IdPagina = P.Id)
	  INNER JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)

