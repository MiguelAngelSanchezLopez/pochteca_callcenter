﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_NoContesta')
	BEGIN
	  PRINT  'Dropping View vwu_NoContesta'
		DROP  View dbo.vwu_NoContesta
	END
GO

PRINT  'Creating View vwu_NoContesta'
GO
CREATE VIEW dbo.vwu_NoContesta
/******************************************************************************
**    Descripcion  : verifica si una alerta fue contactada o no
**    Por          : VSR, 27/01/2015
*******************************************************************************/ 
AS         
  SELECT  
    IdAlerta = T.IdAlerta,  
    -- si el total de historial tiene la misma cantidad de No Contesta, entonces indica que durante todo el proceso no se contacto a nadie  
    NoContesta = CASE WHEN COUNT(T.NoContesta) = SUM(T.NoContesta) THEN 1 ELSE 0 END  
  FROM (
          -- busca en el historal de escalamiento cuantas Explicacion = 'No Contesta' hay  
          SELECT  
            IdAlerta = HE.IdAlerta,    
            DescripcionAlerta = A.DescripcionAlerta,  
            IdHistorialEscalamientoContacto = HEC.Id,  
            IdHistorialEscalamiento = HEC.IdHistorialEscalamiento,  
            NoContesta = CASE WHEN ISNULL(TablaExplicacion.Extra1,'') = 'No contesta' THEN 1 ELSE 0 END,  
            AgrupadoEn = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN  
                          UPPER(ISNULL(EPAGC.Nombre,''))  
                        ELSE  
                          UPPER(ISNULL(P.Nombre,''))  
                        END,  
            OrdenEscalamiento = CASE WHEN (EPA.Orden IS NULL) THEN 'Supervisor' ELSE CONVERT(VARCHAR,EPA.Orden) END
          FROM  
            CallCenterHistorialEscalamientoContacto AS HEC  
            INNER MERGE JOIN CallCenterHistorialEscalamiento AS HE ON (HEC.IdHistorialEscalamiento = HE.Id)  
            INNER JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
            INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
            LEFT JOIN (  
              SELECT DISTINCT Valor, Extra1 FROM TipoGeneral AS TG WHERE TG.Tipo IN ('Explicacion','ExplicacionMultipunto')  
            ) AS TablaExplicacion ON (HEC.Explicacion = TablaExplicacion.Valor)
            LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (HEC.IdEscalamientoPorAlertaContacto = EPAC.Id)
            LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPAC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)  
            LEFT JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
            LEFT MERGE JOIN Usuario AS USGC ON (HEC.IdUsuarioSinGrupoContacto = USGC.Id)  
            LEFT MERGE JOIN Perfil AS P ON (USGC.IdPerfil = P.Id)
          WHERE
            CCA.Desactivada = 0
  ) AS T  
  WHERE  
      ( T.OrdenEscalamiento <> 'Supervisor' )  
  AND (  
        --si la alerta es MULTIPUNTO entonces incluye al CONDUCTOR y TRANSPORTISTA, sino los excluye  
        T.DescripcionAlerta = 'Multipunto' OR  
        (  
              T.DescripcionAlerta <> 'Multipunto'   
          AND T.AgrupadoEn <> 'CONDUCTOR'  
          AND T.AgrupadoEn <> 'TRANSPORTISTA'  
        )  
      )  
  GROUP BY  
    T.IdAlerta  
