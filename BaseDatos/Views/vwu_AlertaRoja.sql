﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaRoja')
	BEGIN
	  PRINT  'Dropping View vwu_AlertaRoja'
		DROP  View dbo.vwu_AlertaRoja
	END
GO

PRINT  'Creating View vwu_AlertaRoja'
GO
CREATE VIEW dbo.vwu_AlertaRoja
/******************************************************************************
**    Descripcion  : obtiene listado de alertas rojas segun definicion
**    Por          : DG, 05/01/2015
*******************************************************************************/ 
AS         
--------------------------------------------------------
-- TIPO ALERTA : 
-- PERDIDA SEÑAL
-- DETENCION NO AUTORIZADA        
-- APERTURA PUERTA
--------------------------------------------------------      
SELECT DISTINCT         
	IDAlerta = vaa.idAlerta,      
	NroTransporte = vaa.NroTransporte, 
  IdEmbarque = vaa.IdEmbarque,   
	LocalDestino = vaa.LocalDestinoCodigo,    
	LatTracto = vaa.LatTracto,
	LonTracto = vaa.LonTracto,
	TipoAlerta = vaa.TipoAlerta,        
	Ocurrencias = Rojas.Ocurrencias        
FROM vwu_AlertaAtendida AS vaa      
INNER JOIN (    
	SELECT      
		A.NroTransporte,        
    A.IdEmbarque,
		A.LocalDestino,        
		A.LatTracto,        
		A.LonTracto,    
		A.TipoAlerta,        
		Ocurrencias = COUNT(A.Id)    
	FROM 
    CallCenterAlerta AS CCA
    INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
	WHERE         
		A.Activo = 1        
		AND A.FueraDeHorario = 0
    AND CCA.Desactivada = 0
		AND    
		(     
			(A.TipoAlerta = 'PERDIDA SEÑAL')        
			OR (A.TipoAlerta = 'DETENCION' AND A.Permiso = 'NO AUTORIZADA')        
			OR (A.TipoAlerta = 'APERTURA PUERTA')      
		)  
		AND A.Id > (SELECT ISNULL(MAX(idAlerta),0) FROM AlertaRojaBatch)  
	GROUP BY         
		A.NroTransporte,
    A.IdEmbarque,
		A.LocalDestino,        
		A.LatTracto,        
		A.LonTracto,    
		A.TipoAlerta        
	HAVING 
		COUNT(A.Id) > 2        
   ) AS Rojas ON (Rojas.NroTransporte = vaa.NroTransporte AND Rojas.IdEmbarque = vaa.IdEmbarque AND Rojas.LocalDestino = vaa.LocalDestinoCodigo AND Rojas.LatTracto = vaa.LatTracto AND Rojas.LonTracto = vaa.LonTracto AND Rojas.TipoAlerta = vaa.TipoAlerta)    
 WHERE     
	vaa.IdAlerta > (SELECT ISNULL(MAX(idAlerta),0) FROM AlertaRojaBatch)     
    
UNION         
 
-----------------------------------------          
-- TIPO ALERTA : 
-- TEMPERATURA        
-----------------------------------------
SELECT DISTINCT         
	IDAlerta = vaa.idAlerta,        
	NroTransporte = vaa.NroTransporte,
	IdEmbarque = vaa.IdEmbarque,
	LocalDestino = vaa.LocalDestinoCodigo,    
	LatTracto = vaa.LatTracto,
	LonTracto = vaa.LonTracto,
	TipoAlerta = vaa.TipoAlerta,        
	Ocurrencias = '0'       
FROM 
	vwu_AlertaAtendida AS vaa    
WHERE     
	TipoAlerta = 'TEMPERATURA'     
	AND vaa.IdAlerta > (SELECT ISNULL(MAX(idAlerta),0) FROM AlertaRojaBatch) 