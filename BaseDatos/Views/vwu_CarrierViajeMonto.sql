﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_CarrierViajeMonto')
	BEGIN
	  PRINT  'Dropping Procedure vwu_CarrierViajeMonto'
		DROP  View dbo.vwu_CarrierViajeMonto
	END
GO

PRINT  'Creating Procedure vwu_CarrierViajeMonto'
GO
CREATE VIEW dbo.vwu_CarrierViajeMonto    
/*****************************************************************************************************    
** Descripcion  : Obtiene los numeros de viajes y montos de Discrepancia asociados al proceso de Carrier    
**  Por          : DG, 19/02/2015    
**    
** Definición de Tipos:    
**  1: Viajes por gestionar)    
**  2: Viajes sin responder por Transportista    
**  3: Viajes respondidos por Transportista    
**  4: Viajes finalizados (Plan Accion)    
**  5: Viajes restantes (No visualizados)    
******************************************************************************************************/    
AS    
 ----------------------------------------------------------------    
 -- 1: Viajes por enviar (Gestionar)    
 ----------------------------------------------------------------    
 SELECT DISTINCT     
  A.NroTransporte    
  ,MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0)
  ,FechaGestion = ISNULL((SELECT CONVERT(varchar(10),MAX(fecha),103) FROM alerta a1 INNER JOIN CallCenterHistorialEscalamiento he ON he.IdAlerta= a1.Id WHERE a1.NroTransporte = a.NroTransporte),'')    
  ,PlanAccion = ''
  ,DetallePlanAccion = ''
  ,Codigo = 1    
  ,Descripcion = 'POR GESTIONAR'    
 FROM 
  CallCenterAlerta AS CCA
  INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
  INNER JOIN CallCenterHistorialEscalamiento HE ON HE.IdAlerta = A.Id    
  INNER JOIN TrazaViaje tv ON TV.NroTransporte = A.NroTransporte AND TV.LocalDestino = a.LocalDestino     
  LEFT JOIN AlertaRojaHistorialEstado ARHE ON (ARHE.NroTransporte = A.NroTransporte AND ARHE.Estado = 'ENVIADO TRANSPORTISTA')    
 WHERE     
  (HE.ClasificacionAlerta = 'Roja')    
  -- alertas rojas sin gestionar    
  AND a.NroTransporte NOT IN (     
          SELECT arb.NroTransporte      
          FROM AlertaRojaBatch arb      
          LEFT JOIN CallCenterHistorialEscalamiento he ON (HE.IdAlerta = arb.IdAlerta AND he.idEscalamientoPorAlerta IS NULL)      
          WHERE he.Id IS null      
         )       
  --viaje cerrado    
  AND (tv.EstadoViaje = 'EN LOCAL-AP' OR tv.EstadoViaje = 'EnLocal-R' OR tv.EstadoViaje = 'EnLocal-P')    
  AND (ARHE.NroTransporte IS NULL)    
     
 ----------------------------------------------------------------    
 -- 2: Viajes sin responder (Transportista)    
 ----------------------------------------------------------------    
 UNION     
 SELECT     
  NroTransporte    
  ,MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = area.NroTransporte),0)
  ,FechaGestion = ISNULL((SELECT CONVERT(varchar(10),MAX(fecha),103) FROM alerta a INNER JOIN CallCenterHistorialEscalamiento he ON he.IdAlerta= a.Id WHERE a.NroTransporte = area.NroTransporte),'')    
  ,PlanAccion = ''  
  ,DetallePlanAccion = ''
  ,Codigo = 2    
  ,Descripcion = 'SIN RESPONDER TRANSPORTISTA'    
 FROM AlertaRojaEstadoActual area     
 WHERE area.Estado = 'ENVIADO TRANSPORTISTA'     
    
 ----------------------------------------------------------------    
 -- 3: Viajes respondidos (Transportista)    
 ----------------------------------------------------------------     
 UNION    
 SELECT     
  NroTransporte    
  ,MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = area.NroTransporte),0)
  ,FechaGestion = ISNULL((SELECT CONVERT(varchar(10),MAX(fecha),103) FROM alerta a INNER JOIN CallCenterHistorialEscalamiento he ON he.IdAlerta= a.Id WHERE a.NroTransporte = area.NroTransporte),'')    
  ,PlanAccion = ''    
  ,DetallePlanAccion = ''
  ,Codigo = 3    
  ,Descripcion = 'RESPONDIDO TRANSPORTISTA'    
 FROM AlertaRojaEstadoActual area     
 WHERE area.Estado = 'ENVIADO OPERACION TRANSPORTE'      
    
 ----------------------------------------------------------------    
 -- 4: Viajes finalizados (Con plan de acción)    
 ----------------------------------------------------------------    
 UNION     
 SELECT     
  NroTransporte    
  ,MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = area.NroTransporte),0)
  ,FechaGestion = area.FechaModificacion
  ,PlanAccion = ''
  ,DetallePlanAccion = area.ObservacionEstado   
  ,Codigo = 4    
  ,Descripcion = 'FINALIZADO'    
 FROM AlertaRojaEstadoActual area  
 WHERE area.Estado = 'FINALIZADO'     
    
 ----------------------------------------------------------------    
 -- 5: Viajes restantes (No visualizados)    
 ----------------------------------------------------------------    
 UNION    
 SELECT DISTINCT    
  NroTransporte = A.NroTransporte    
  ,MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0)
  ,FechaGestion = ISNULL((SELECT CONVERT(varchar(10),MAX(fecha),103) FROM alerta a1 INNER JOIN CallCenterHistorialEscalamiento he ON he.IdAlerta= a1.Id WHERE a1.NroTransporte = a.NroTransporte),'')    
  ,PlanAccion = ''
  ,DetallePlanAccion = ''  
  ,Codigo = 5    
  ,Descripcion = 'RESTANTE'    
 FROM CallCenterHistorialEscalamiento he     
 LEFT JOIN Usuario u ON u.Id = he.IdUsuarioCreacion    
 LEFT JOIN Alerta a ON a.Id = he.IdAlerta    
 WHERE     
  he.ClasificacionAlerta = 'Roja'    
  AND A.NroTransporte NOT IN(    
         SELECT  NroTransporte    
         FROM AlertaRojaEstadoActual area     
         WHERE area.Estado = 'ENVIADO TRANSPORTISTA'     
    
         UNION    
         SELECT  NroTransporte    
         FROM AlertaRojaEstadoActual area     
         WHERE area.Estado = 'ENVIADO OPERACION TRANSPORTE'      
    
         UNION     
         SELECT NroTransporte    
         FROM AlertaRojaEstadoActual area     
         WHERE area.Estado = 'FINALIZADO'     
    
         UNION      
         SELECT DISTINCT A.NroTransporte    
         FROM
          CallCenterAlerta AS CCA
          INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
          INNER JOIN CallCenterHistorialEscalamiento HE ON HE.IdAlerta = A.Id    
          INNER JOIN TrazaViaje tv ON TV.NroTransporte = A.NroTransporte AND TV.LocalDestino = a.LocalDestino     
          LEFT JOIN AlertaRojaHistorialEstado ARHE ON (ARHE.NroTransporte = A.NroTransporte AND ARHE.Estado = 'ENVIADO TRANSPORTISTA')    
         WHERE     
          (HE.ClasificacionAlerta = 'Roja')    
          -- alertas rojas sin gestionar    
          AND a.NroTransporte NOT IN (     
                  SELECT arb.NroTransporte      
                  FROM AlertaRojaBatch arb      
                  LEFT JOIN CallCenterHistorialEscalamiento he ON (HE.IdAlerta = arb.IdAlerta AND he.idEscalamientoPorAlerta IS NULL)      
                  WHERE he.Id IS null      
                 )       
          --viaje cerrado    
          AND (tv.EstadoViaje = 'EN LOCAL-AP' OR tv.EstadoViaje = 'EnLocal-R' OR tv.EstadoViaje = 'EnLocal-P')    
          AND (ARHE.NroTransporte IS NULL)    
  )
