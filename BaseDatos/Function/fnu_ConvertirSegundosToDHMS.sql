﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ConvertirSegundosToDHMS' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ConvertirSegundosToDHMS'
  DROP FUNCTION dbo.fnu_ConvertirSegundosToDHMS
END
GO

PRINT 'Creating Function fnu_ConvertirSegundosToDHMS'
GO
CREATE FUNCTION dbo.fnu_ConvertirSegundosToDHMS (
/*-----------------------------------------------------------
Descripcion: convierte segundos en formato Xd Xh Xm Xs
Por        : VSR, 03/02/2017
-------------------------------------------------------------*/
@TotalSegundos NUMERIC
)
RETURNS VARCHAR(255)
AS
BEGIN
  DECLARE @numDias INT, @numHoras INT, @numMinutos INT, @numSegundos INT, @tiempo VARCHAR(255)
  SET @tiempo = ''

  SET @numDias = @TotalSegundos / (24 * 60 * 60)
  SET @numHoras = (@TotalSegundos % (24 * 60 * 60)) / (60 * 60)
  SET @numMinutos = ((@TotalSegundos % (24 * 60 * 60)) % (60 * 60)) / 60
  SET @numSegundos = ((@TotalSegundos % (24 * 60 * 60)) % (60 * 60)) % 60

  IF (@numDias > 0) SET @tiempo = @tiempo + CONVERT(VARCHAR,@numDias) + 'd '
  IF (@numHoras > 0) SET @tiempo = @tiempo + CONVERT(VARCHAR,@numHoras) + 'h '
  IF (@numMinutos > 0) SET @tiempo = @tiempo + CONVERT(VARCHAR,@numMinutos) + 'm '
  --IF (@numSegundos > 0) SET @tiempo = @tiempo + CONVERT(VARCHAR,@numSegundos) + 's '

	-- retorna valor a la funcion
	RETURN LTRIM(RTRIM(@tiempo))
END
GO
