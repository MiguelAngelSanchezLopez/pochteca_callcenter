﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ObtenerListadoLocalesProgramacionTextoPlano' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ObtenerListadoLocalesProgramacionTextoPlano'
  DROP FUNCTION dbo.fnu_ObtenerListadoLocalesProgramacionTextoPlano
END
GO

PRINT 'Creating Function fnu_ObtenerListadoLocalesProgramacionTextoPlano'
GO
CREATE FUNCTION dbo.fnu_ObtenerListadoLocalesProgramacionTextoPlano (
/******************************************************************************
**  Descripcion: obtiene los perfiles asociados a la pagina
**  Fecha      : 15/09/2009
*******************************************************************************/
@IdPlanificacion INT,
@NroTransporte INT
)
RETURNS VARCHAR(8000)
AS
BEGIN
	-- se inicializa la variable
	DECLARE @listado AS VARCHAR(8000)
	SET @listado = ''

	SELECT
		@listado = @listado + RTRIM(LTRIM(ISNULL(l.Nombre,''))) + ' - ' + CAST(l.CodigoInterno AS VARCHAR(50)) + ' ; '
	FROM
		PlanificacionTransportista AS PT
		INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
		INNER JOIN Usuario AS U ON (PT.IdUsuarioTransportista = U.Id)
	WHERE 
		IdPlanificacion =  @IdPlanificacion
		AND NroTransporte = @NroTransporte 
	
	-- retorna listado
	RETURN SUBSTRING(@listado, 1, Len(@listado) - 1 )
END
GO     