﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ConvertirDatetimeToDDMMYYYY' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ConvertirDatetimeToDDMMYYYY'
  DROP FUNCTION dbo.fnu_ConvertirDatetimeToDDMMYYYY
END
GO

PRINT 'Creating Function fnu_ConvertirDatetimeToDDMMYYYY'
GO
CREATE FUNCTION dbo.fnu_ConvertirDatetimeToDDMMYYYY (
/*-----------------------------------------------------------
Descripcion: convierte fecha datetime en dd/mm/yyyy hh:mm hrs.
Por        : VSR, 10/07/2014
MostrarTipo: FECHA_COMPLETA | SOLO_FECHA | SOLO_HORA
-------------------------------------------------------------*/
@Fecha AS DATETIME,
@MostrarTipo VARCHAR(255)
)
RETURNS VARCHAR(255)
AS
BEGIN

  DECLARE @dma VARCHAR(20), @hh VARCHAR(20), @nuevaFecha VARCHAR(255)

  SET @dma = CONVERT(VARCHAR, @Fecha, 103)
  SET @hh = LEFT(CONVERT(VARCHAR, @Fecha, 108),5)
  
  IF (@MostrarTipo = 'FECHA_COMPLETA') BEGIN
    SET @nuevaFecha = @dma + ' ' + @hh + ' hrs.'
  END ELSE IF (@MostrarTipo = 'SOLO_HORA') BEGIN
    SET @nuevaFecha = @hh
  END ELSE BEGIN
    SET @nuevaFecha = @dma
  END

	-- retorna valor a la funcion
	RETURN @nuevaFecha
END
GO
     