﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_GETDATE' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_GETDATE'
  DROP FUNCTION dbo.fnu_GETDATE
END
GO

PRINT 'Creating Function fnu_GETDATE'
GO
CREATE FUNCTION dbo.fnu_GETDATE (
/*-----------------------------------------------------------
Descripcion: obtiene fecha actual dependiendo de la zona horaria
Por        : VSR, 16/09/2016
-------------------------------------------------------------*/
)
RETURNS DATETIME
AS
BEGIN
  DECLARE @FechaHoy AS DATETIME, @diferenciaHoraria AS VARCHAR(255)
  SET @diferenciaHoraria = ISNULL((SELECT Valor FROM Configuracion WHERE Clave='webDiferenciaHoraria'),'0')
  SET @FechaHoy = DATEADD(hh, CONVERT(INT,@diferenciaHoraria),GETDATE())

	-- retorna valor a la funcion
	RETURN @FechaHoy
END
GO
