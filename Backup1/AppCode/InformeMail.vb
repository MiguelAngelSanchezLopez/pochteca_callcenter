﻿Imports CapaNegocio
Imports System.Data
Imports Syncfusion.XlsIO

Public Class InformeMail
  Inherits System.Web.UI.Page

#Region "Generar Excel"
  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function GenerarExcel(ByVal ds As DataSet, ByVal nombreArchivo As String, ByVal nombreCarpetaLog As String, nombreHoja As String()) As Boolean
    Dim fueGenerado As Boolean = False
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim workbook As IWorkbook
    Dim dt As DataTable
    Dim sheet As IWorksheet

    Try
      application.UseNativeStorage = False
      workbook = application.Workbooks.Create(nombreHoja)

      '--------------------------------------
      dt = ds.Tables(0)
      sheet = workbook.Worksheets(nombreHoja(0))
      GenerarHojaExcel(workbook, sheet, dt)

      workbook.SaveAs(Utilidades.RutaLogsSitio() & "\" & nombreCarpetaLog & "\" & nombreArchivo & ".xls")
      workbook.Close()
      fueGenerado = True

      Return fueGenerado
    Catch ex As Exception
      Throw New Exception(ex.Message)
    Finally
      excelEngine.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <remarks></remarks>
  Private Shared Sub GenerarHojaExcel(ByRef workbook As IWorkbook, ByRef sheet As IWorksheet, ByVal dt As DataTable)
    Dim fueGenerado As Boolean = False

    Try
      sheet.ImportDataTable(dt, True, 1, 1)
      With sheet.Range(1, 1, 1, dt.Columns.Count).CellStyle
        .Font.Bold = True
      End With

      sheet.UsedRange.AutofitColumns()
      sheet.IsGridLinesVisible = True
      fueGenerado = True
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

#End Region

#Region "GestionAlerta"
  ''' <summary>
  ''' envia mail con el detalle de la operacion registrada
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub GestionAlerta(ByVal idAlerta As String, ByVal ds As DataSet)
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nroTransporte, nroEscalamientoActual, nombreAlerta As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim dt As DataTable
    Dim dr As DataRow
    Dim listadoMailConCopia As String = ""
    Dim arrGrupoContactoEmail As String() = {Alerta.GRUPO_CONTACTO_TRANSPORTISTA, "RESTO"}

    Try
      If Not ds Is Nothing Then
        dt = ds.Tables(Alerta.eTabla.T00_Detalle)
        dr = dt.Rows(0)
        nombreAlerta = dr.Item("NombreAlerta")
        nroTransporte = dr.Item("NroTransporte")
        nroEscalamientoActual = dr.Item("NroEscalamientoActual")

        'recorre los grupos de contactos a cuales les enviara el mail
        For Each agrupadoEn As String In arrGrupoContactoEmail
          'obtiene los correos a quienes debe enviar la informacion
          listadoMailPara = GestionAlerta_EmailContactosEscalamientoActual(ds, nroEscalamientoActual, listadoMailConCopia, agrupadoEn)
          listadoMailConCopiaOculta = Utilidades.MailAdministrador()

          If ( _
               agrupadoEn = "RESTO" _
               Or (agrupadoEn = Alerta.GRUPO_CONTACTO_TRANSPORTISTA And Not String.IsNullOrEmpty(listadoMailPara)) _
             ) Then
            'construye asunto del mail
            asuntoMail = "[ALTOTRACK POCHTECA] Gestión Alerta {NOMBRE_ALERTA}: IdMaster {NRO_TRANSPORTE} - Escalamiento Nro. {NRO_ESCALAMIENTO_ACTUAL}"
            asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", nombreAlerta)
            asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)
            asuntoMail = asuntoMail.Replace("{NRO_ESCALAMIENTO_ACTUAL}", nroEscalamientoActual)

            'construye cuerpo del mail
            bodyMensaje = GestionAlerta_Detalle(ds, "GestionAlerta")
            bodyMensaje &= GestionAlerta_HistorialEscalamientos(ds)
            bodyMensaje &= GestionAlerta_PiePagina(idAlerta, agrupadoEn)

            'si esta en modo debug entonces envia el mail al correo de pruebas
            'sino lo envia a quien corresponda
            If Utilidades.MailModoDebug Then
              ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
              ColeccionEmailsCC = Nothing
              ColeccionEmailsCCO = Nothing
            Else
              ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
              ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
              ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
            End If

            'envia el mail
            msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                     Utilidades.MailEmisor, _
                                     Utilidades.MailPasswordEmisor, _
                                     Utilidades.MailNombreEmisor, _
                                     ColeccionEmailsPara, _
                                     asuntoMail, _
                                     bodyMensaje, _
                                     True, _
                                     ColeccionEmailsCC, _
                                     False, _
                                     "", _
                                     "", _
                                     ColeccionEmailsCCO)
            'graba el mensaje enviado por mail en el disco
            If msgProceso <> String.Empty Then contadorErrores = 1
            Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
            Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
            Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), "GestionAlerta", nombreArchivo, "html")
          End If
        Next

      End If

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  Private Shared Function GestionAlerta_Detalle(ByVal ds As DataSet, ByVal invocadoDesde As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim templateTitulo, templateFila, templateFilaAux, templatePrioridad, key, label, texto, tipoAlerta As String
    Dim dt As DataTable
    Dim dr As DataRow
    Dim diccionario As New Dictionary(Of String, String)
    Dim slColorBackground As New SortedList
    Dim slColorFuente As New SortedList

    Try
      templateTitulo = "<div style=""font-family:Arial; font-size:14px; font-weight:bold; color:#fff; background-color:#333; padding:10px;"">{TITULO}</div>"

      templateFila = "<tr valign=""top"">" & _
                     "  <td style=""font-family:Arial; font-size:12px; font-weight: bold; width:180px"">{LABEL}:</td>" & _
                     "  <td style=""font-family:Arial; font-size:12px;"">{TEXTO}</td>" & _
                     "</tr>"

      'define colores de la prioridad
      slColorBackground.Add("success", "#73A839")
      slColorBackground.Add("warning", "#fcf8e3")
      slColorBackground.Add("danger", "#C71C22")
      slColorFuente.Add("success", "#fff")
      slColorFuente.Add("warning", "#c09853")
      slColorFuente.Add("danger", "#fff")

      dt = ds.Tables(Alerta.eTabla.T00_Detalle)
      dr = dt.Rows(0)

      'texto cabecera
      Select Case invocadoDesde
        Case "CierreViaje"
          sb.Append("<div>")
          sb.Append("El siguiente viaje fue notificado con un <b>Cierre de Viaje</b> en la gesti&oacute;n del CallCenter. Favor de revisar y aplicar las acciones respectivas.")
          sb.Append("</div>")
          sb.Append("<br/ ><br/ >")
      End Select

      'Titulo
      templateTitulo = templateTitulo.Replace("{TITULO}", "DATOS DE LA ALERTA")
      sb.Append(templateTitulo)

      'agrega las columnas a mostrar
      diccionario.Add("prioridad", "Prioridad")
      diccionario.Add("nombreAlerta", "Alerta")
      diccionario.Add("nroTransporte", "IdMaster")
      diccionario.Add("tipoAlerta", "Tipo Alerta")
      diccionario.Add("localOrigenDescripcion", "Origen")
      diccionario.Add("localDestinoDescripcion", "Tienda Destino")
      diccionario.Add("localDestinoCodigo", "Determinante Tienda Destino")
      diccionario.Add("nombreConductor", "Operador")
      diccionario.Add("nombreTransportista", "Línea de Transporte")
      diccionario.Add("nroEscalamientoActual", "Escalamientos realizados")
      diccionario.Add("fechaHoraCreacion", "Fecha Creación")
      diccionario.Add("alertaMapa", "Mapa")

      'dibuja las columnas
      sb.Append("<div>")
      sb.Append("  <table border=""0"" cellpadding=""0"" cellspacing=""5"">")

      For Each item In diccionario
        templateFilaAux = templateFila
        key = item.Key
        label = item.Value
        texto = dr.Item(key)

        If (key = "prioridad") Then
          templatePrioridad = "<span style=""background-color:{BACKGROUND}; color:{COLOR}; padding:0px 5px; font-weight:bold; font-size:13px;"">{TEXTO}</span>"
          tipoAlerta = Utilidades.ObtenerStyleAlertaPorPrioridad(texto)

          templatePrioridad = templatePrioridad.Replace("{BACKGROUND}", slColorBackground.Item(tipoAlerta))
          templatePrioridad = templatePrioridad.Replace("{COLOR}", slColorFuente.Item(tipoAlerta))
          templatePrioridad = templatePrioridad.Replace("{TEXTO}", texto)
          texto = templatePrioridad
        End If

        If (key = "localDestinoCodigo" And texto = "-1") Then
          texto = ""
        End If

        If (key = "alertaMapa") Then
          texto = IIf(String.IsNullOrEmpty(texto), "<i>Sin mapa</i>", "<a href=""" & texto & """ target=""_blank"">Ver mapa</a>")
        End If

        templateFilaAux = templateFilaAux.Replace("{LABEL}", label)
        templateFilaAux = templateFilaAux.Replace("{TEXTO}", texto)
        sb.Append(templateFilaAux)
      Next

      sb.Append("  </table>")
      sb.Append("</div>")
      sb.Append("<br />")

      html = sb.ToString()
      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  Private Shared Function GestionAlerta_HistorialEscalamientos(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim sbEscalamiento As New StringBuilder
    Dim templateTitulo, template, templateAux, idHistorialEscalamiento, nroEscalamiento, fecha, nombreUsuario, cargo, telefono, explicacion As String
    Dim explicacionContacto, observacion, observacionContacto, agrupadoEn, atendidoPor As String
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet

    Try
      templateTitulo = "<div style=""font-family:Arial; font-size:14px; font-weight:bold; color:#fff; background-color:#333; padding:10px;"">{TITULO}</div>"

      template = "<tr valign=""top"">" & _
                 "  <td style=""font-family:Arial; font-size:12px;"">{FECHA}</td>" & _
                 "  <td style=""font-family:Arial; font-size:12px;"">{AGRUPADO_EN}</td>" & _
                 "  <td style=""font-family:Arial; font-size:12px;"">{NOMBRE_USUARIO}</td>" & _
                 "  <td style=""font-family:Arial; font-size:12px;"">{EXPLICACION}</td>" & _
                 "  <td style=""font-family:Arial; font-size:12px;"">{OBSERVACION}</td>" & _
                 "</tr>"

      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      totalRegistros = dt.Rows.Count

      'Titulo
      templateTitulo = templateTitulo.Replace("{TITULO}", "HISTORIAL DE ESCALAMIENTOS")
      sb.AppendLine(templateTitulo)

      For Each dr In dt.Rows
        sbEscalamiento = New StringBuilder
        idHistorialEscalamiento = dr.Item("IdHistorialEscalamiento")
        nroEscalamiento = dr.Item("NroEscalamiento")
        fecha = dr.Item("Fecha")
        explicacion = dr.Item("Explicacion")
        observacion = dr.Item("Observacion")
        atendidoPor = dr.Item("AtendidoPor")

        sbEscalamiento.Append("<br />")
        sbEscalamiento.Append("<div>")
        sbEscalamiento.Append("<div style=""font-family:Arial; font-size:14px; font-weight:bold; padding-bottom:5px;"">ESCALAMIENTO NRO. " & nroEscalamiento & " <em style=""font-size:12px; font-weight:normal;"">(atendido por " & atendidoPor & ")</em></div>")
        sbEscalamiento.Append("<div>")
        sbEscalamiento.Append("  <table border=""1"" cellpadding=""3"" cellspacing=""0"" style=""border-width:1px;width:100%;border-collapse:collapse; border: 1px solid #aaa"">")
        sbEscalamiento.Append("    <thead>")
        sbEscalamiento.Append("      <tr>")
        sbEscalamiento.Append("        <th style=""font-family:Arial; font-size:12px; background-color: #ccc; width:130px;"">Fecha</th>")
        sbEscalamiento.Append("        <th style=""font-family:Arial; font-size:12px; background-color: #ccc; width:100px;"">Grupo</th>")
        sbEscalamiento.Append("        <th style=""font-family:Arial; font-size:12px; background-color: #ccc; width:180px;"">Contacto</th>")
        sbEscalamiento.Append("        <th style=""font-family:Arial; font-size:12px; background-color: #ccc; width:150px;"">Explicaci&oacute;n</th>")
        sbEscalamiento.Append("        <th style=""font-family:Arial; font-size:12px; background-color: #ccc; "">Observaci&oacute;n</th>")
        sbEscalamiento.Append("      </tr>")
        sbEscalamiento.Append("    </thead>")
        sbEscalamiento.Append("    <tbody>")

        '---------------------------------------------------
        'obtiene los contactos por escalamiento
        dv = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos).DefaultView
        dv.RowFilter = "IdHistorialEscalamiento = '" & idHistorialEscalamiento & "'"
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        For Each drContacto As DataRow In dsFiltrado.Tables(0).Rows
          templateAux = template
          nombreUsuario = drContacto.Item("NombreUsuario")
          cargo = drContacto.Item("Cargo")
          telefono = drContacto.Item("Telefono")
          explicacionContacto = drContacto.Item("Explicacion")
          observacionContacto = drContacto.Item("Observacion")
          agrupadoEn = drContacto.Item("agrupadoEn")

          explicacionContacto = IIf(String.IsNullOrEmpty(explicacionContacto), explicacion, explicacionContacto)
          explicacionContacto = IIf(String.IsNullOrEmpty(explicacionContacto), "&nbsp;", explicacionContacto)

          nombreUsuario = "<b>" & nombreUsuario & "</b>"
          nombreUsuario &= IIf(String.IsNullOrEmpty(cargo), "", "<div><em>" & cargo & "</em></div>")
          nombreUsuario &= IIf(String.IsNullOrEmpty(telefono), "", "<div><em>" & telefono & "</em></div>")

          observacionContacto = IIf(String.IsNullOrEmpty(observacionContacto), observacion, observacionContacto)
          observacionContacto = IIf(String.IsNullOrEmpty(observacionContacto), "&nbsp;", observacionContacto)

          'remplaza marcas
          templateAux = templateAux.Replace("{FECHA}", fecha)
          templateAux = templateAux.Replace("{NOMBRE_USUARIO}", nombreUsuario)
          templateAux = templateAux.Replace("{EXPLICACION}", explicacionContacto)
          templateAux = templateAux.Replace("{OBSERVACION}", observacionContacto)
          templateAux = templateAux.Replace("{AGRUPADO_EN}", agrupadoEn.ToUpper())
          sbEscalamiento.Append(templateAux)
        Next
        '---------------------------------------------------

        sbEscalamiento.Append("    </tbody>")
        sbEscalamiento.Append("  </table>")
        sbEscalamiento.Append("</div>")
        sbEscalamiento.Append("</div>")

        sb.Append(sbEscalamiento.ToString())
      Next

      html = sb.ToString()
      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  Private Shared Function GestionAlerta_EmailContactosEscalamientoActual(ByVal ds As DataSet, ByVal nroEscalamientoActual As String, ByRef listadoMailConCopia As String, ByVal agrupadoEn As String) As String
    Dim listado As String = ""
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim idHistorialEscalamiento As String = "-1"

    Try
      '----------------------------------------------------------------------
      'busca EmailCopia asociado al escalamiento
      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      dv = dt.DefaultView
      dv.RowFilter = "NroEscalamiento = " & nroEscalamientoActual
      If (dv.Count = 0) Then
        idHistorialEscalamiento = "-1"
        listadoMailConCopia = ""
      Else
        idHistorialEscalamiento = dv.Item(0).Item("IdHistorialEscalamiento")
        If (agrupadoEn <> Alerta.GRUPO_CONTACTO_TRANSPORTISTA) Then listadoMailConCopia = dv.Item(0).Item("EmailCopia")
      End If
      dv.RowFilter = ""

      '----------------------------------------------------------------------
      'busca email de los contactos asociados en el historial de escalamientos
      dt = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos)
      dv = dt.DefaultView

      Select Case agrupadoEn
        Case Alerta.GRUPO_CONTACTO_TRANSPORTISTA
          dv.RowFilter = "NotificarPorEmail = 1 AND IdHistorialEscalamiento = " & idHistorialEscalamiento & " AND AgrupadoEn = '" & agrupadoEn & "'"
        Case Else
          dv.RowFilter = "NotificarPorEmail = 1 AND IdHistorialEscalamiento = " & idHistorialEscalamiento & " AND AgrupadoEn NOT IN('" & Alerta.GRUPO_CONTACTO_TRANSPORTISTA & "')"
      End Select

      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""
      listado = Utilidades.ObtenerListadoMailDesdeDataSet(dsFiltrado, 0, "Email")

      Return listado
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  Private Shared Function GestionAlerta_PiePagina(ByVal idAlerta As String, ByVal agrupadoEn As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim queryString As String = ""
    Dim url As String

    Try
      sb.Append("<br />")
      sb.Append("<br />")

      If (agrupadoEn = Alerta.GRUPO_CONTACTO_TRANSPORTISTA) Then
        'construye URL
        queryString &= "idAlerta=" & idAlerta
        queryString = Criptografia.EncriptarTripleDES(queryString)
        url = Utilidades.ObtenerUrlBase() & "/public/Alerta.RespuestaEmail.aspx?arg=" & queryString

        sb.Append("<div style=""background-color:#4485f4; padding:10px; text-align:center"">")
        sb.Append("  <a href=""" & url & """ style=""font-family:Arial; font-size:16px; font-weight:bold; color:#fff;"">SI DESEA RESPONDER ESTE EMAIL HAGA CLICK AQUI</a>")
        sb.Append("</div>")
        sb.Append("<br />")
      End If

      sb.Append("<div style=""font-family:Arial; font-size:14px; font-style: italic;"">(este mail se ejecutó automáticamente por el sistema. No responder por favor.)</div>")

      html = sb.ToString()
      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

#End Region

#Region "PlanificacionCamiones"
  ''' <summary>
  ''' envia mail con la planificacion del transportista
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub PlanificacionCamiones(ByVal ds As DataSet, ByVal tipoCarga As String)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, rutTransportista, emailTransportista, nombreArchivo, rutaArchivoExcel, contenidoLog As String
    Dim emailTransportistaConCopia As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim dr As DataRow
    Dim dv As New DataView
    Dim listadoMailConCopia As String = ""
    Dim dsFiltrado As New DataSet
    Dim fueGenerado As Boolean
    Dim nombreCarpetaLog As String = "EnvioPlanificacionCamiones"
    Dim T02_RutTransportista As Integer = 2
    Dim T03_PlanificacionTransportista As Integer = 3

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()

      If Not ds Is Nothing Then
        dv = ds.Tables(T03_PlanificacionTransportista).DefaultView
        listadoMailConCopiaOculta = Configuracion.Leer("webListadoEmailPlanificacionCamiones")

        'recorre los transportistas para enviar la planificacion
        For Each dr In ds.Tables(T02_RutTransportista).Rows
          rutTransportista = dr.Item("RutTransportista")
          emailTransportista = dr.Item("EmailTransportista")
          emailTransportistaConCopia = dr.Item("EmailTransportistaConCopia")

          'filtra por el rut del transportista
          dv.RowFilter = "[RUT LINEA DE TRANSPORTE] = '" & rutTransportista & "'"
          dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          'crea el archivo en disco
          Dim nombreHoja As String() = {"PLANIFICACION"}
          nombreArchivo = Archivo.CrearNombreUnicoArchivo("Planificacion")
          fueGenerado = GenerarExcel(dsFiltrado, nombreArchivo, nombreCarpetaLog, nombreHoja)

          If (fueGenerado) Then
            listadoMailPara = emailTransportista
            listadoMailConCopia = emailTransportistaConCopia
            listadoMailConCopiaOculta &= IIf(String.IsNullOrEmpty(listadoMailConCopiaOculta), Utilidades.MailAdministrador(), ";" & Utilidades.MailAdministrador())

            If (tipoCarga = "CARGA_MANUAL") Then
              asuntoMail = "[ALTOTRACK POCHTECA] Asignación de nuevo viaje planificado"
              bodyMensaje = "Estimado(a){BR}{BR}En el presente mail se adjunta un nuevo viaje planificado que fue ingresado y asignado manualmente.{BR}{BR}(este mail se ejecutó automáticamente por el sistema. No responder por favor.)"
            Else
              asuntoMail = "[ALTOTRACK POCHTECA] Planificación de camiones"
              bodyMensaje = "Estimado(a){BR}{BR}En el presente mail se adjunta el listado de viajes que debe planificar con sus camiones.{BR}{BR}(este mail se ejecutó automáticamente por el sistema. No responder por favor.)"
            End If
            bodyMensaje = bodyMensaje.Replace("{BR}", "<br />")

            'si esta en modo debug entonces envia el mail al correo de pruebas
            'sino lo envia a quien corresponda
            If Utilidades.MailModoDebug Then
              ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
              ColeccionEmailsCC = Nothing
              ColeccionEmailsCCO = Nothing
            Else
              ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
              ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
              ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
            End If

            'obtiene ruta del archivo excel generado
            rutaArchivoExcel = Utilidades.RutaLogsSitio() & "\" & nombreCarpetaLog & "\" & nombreArchivo & ".xls"

            'crea contenido para crear el log
            contenidoLog = Herramientas.ObtenerTablaHTMLDesdeDataSet(dsFiltrado)
            contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(contenidoLog & "<br/><div>Enviado a: " & listadoMailPara & "</div>", nombreCarpetaLog)

            'envia el mail
            msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                     Utilidades.MailEmisor, _
                                     Utilidades.MailPasswordEmisor, _
                                     Utilidades.MailNombreEmisor, _
                                     ColeccionEmailsPara, _
                                     asuntoMail, _
                                     bodyMensaje, _
                                     True, _
                                     ColeccionEmailsCC, _
                                     True, _
                                     rutaArchivoExcel, _
                                     "", _
                                     ColeccionEmailsCCO)
            Sistema.GrabarLogSesion(oUsuario.Id, "Envia planificacion Línea de Transporte: Rut " & rutTransportista & " / Email " & emailTransportista)
          Else
            contenidoLog = "Archivo excel no fue generado"
            msgProceso = "Error excel"
          End If

          'graba el mensaje enviado por mail en el disco
          If msgProceso <> String.Empty Then contadorErrores = 1
          Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
          nombreArchivo = Archivo.CrearNombreUnicoArchivo("Planificacion_" & sufijoArchivo)
          Archivo.CrearArchivoTextoEnDisco(contenidoLog & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), nombreCarpetaLog, nombreArchivo, "html")

        Next
      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

#Region "AsignacionCamiones"
  ''' <summary>
  ''' envia mail con la asignaciones de camiones
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub AsignacionCamiones(ByVal listIdUsuarioTransportista As String)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nombreTransportista, nombreArchivo, rutaArchivoExcel, contenidoLog As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim dr As DataRow
    Dim dv As New DataView
    Dim listadoMailConCopia As String = ""
    Dim ds As New DataSet
    Dim fueGenerado As Boolean
    Dim nombreCarpetaLog As String = "EnvioAsignacionCamiones"

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      ds = PlanificacionTransportista.ObtenerAsignacionCamionesTransportista(listIdUsuarioTransportista)

      If Not ds Is Nothing Then
        dr = ds.Tables(0).Rows(0)
        nombreTransportista = dr.Item("LINEA DE TRANSPORTE")
        nombreTransportista = nombreTransportista.Trim()

        'crea el archivo en disco
        Dim nombreHoja As String() = {"ASIGNACION CAMIONES"}
        nombreArchivo = Archivo.CrearNombreUnicoArchivo("Asignacion_" & nombreTransportista.Replace(" ", ""))
        fueGenerado = GenerarExcel(ds, nombreArchivo, nombreCarpetaLog, nombreHoja)

        If (fueGenerado) Then
          listadoMailConCopiaOculta = Utilidades.MailAdministrador()
          listadoMailPara = Configuracion.Leer("webListadoEmailAsignacionCamiones")

          'construye asunto del mail
          asuntoMail = "[ALTOTRACK POCHTECA] Asignación de camiones Línea de Transporte: " & nombreTransportista

          'construye cuerpo del mail
          bodyMensaje = "Estimado(a){BR}{BR}En el presente mail se adjunta el listado de camiones asignados por la Línea de Transporte {NOMBRE_TRANSPORTISTA}.{BR}{BR}(este mail se ejecutó automáticamente por el sistema. No responder por favor.)"
          bodyMensaje = bodyMensaje.Replace("{BR}", "<br />")
          bodyMensaje = bodyMensaje.Replace("{NOMBRE_TRANSPORTISTA}", nombreTransportista)

          'si esta en modo debug entonces envia el mail al correo de pruebas
          'sino lo envia a quien corresponda
          If Utilidades.MailModoDebug Then
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
            ColeccionEmailsCC = Nothing
            ColeccionEmailsCCO = Nothing
          Else
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
            ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
            ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
          End If

          'obtiene ruta del archivo excel generado
          rutaArchivoExcel = Utilidades.RutaLogsSitio() & "\" & nombreCarpetaLog & "\" & nombreArchivo & ".xls"

          'crea contenido para crear el log
          contenidoLog = Herramientas.ObtenerTablaHTMLDesdeDataSet(ds)
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(contenidoLog & "<br/><div>Enviado a: " & listadoMailPara & "</div>", nombreCarpetaLog)

          'envia el mail
          msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                   Utilidades.MailEmisor, _
                                   Utilidades.MailPasswordEmisor, _
                                   Utilidades.MailNombreEmisor, _
                                   ColeccionEmailsPara, _
                                   asuntoMail, _
                                   bodyMensaje, _
                                   True, _
                                   ColeccionEmailsCC, _
                                   True, _
                                   rutaArchivoExcel, _
                                   "", _
                                   ColeccionEmailsCCO)
          Sistema.GrabarLogSesion(oUsuario.Id, "Envia asignación de camiones: IdUsuarioTransportista " & listIdUsuarioTransportista)
        Else
          contenidoLog = "Archivo excel no fue generado"
          msgProceso = "Error excel"
        End If

        'graba el mensaje enviado por mail en el disco
        If msgProceso <> String.Empty Then contadorErrores = 1
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
        nombreArchivo = Archivo.CrearNombreUnicoArchivo("Asignacion_" & nombreTransportista.Replace(" ", "") & "_" & sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(contenidoLog & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), nombreCarpetaLog, nombreArchivo, "html")
      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

#Region "ResumenViaje"
  ''' <summary>
  ''' envia mail con el detalle de la operacion registrada
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub ResumenViaje(ByVal ds As DataSet)
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nroTransporte, localDestino, nombreArchivo, rutaArchivoExcel, contenidoLog As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim dt As DataTable
    Dim dr As DataRow
    Dim listadoMailConCopia As String = ""
    Dim dsAlertas As New DataSet
    Dim fueGenerado As Boolean
    Dim nombreCarpetaLog As String = "ResumenViaje"
    Dim T00_ResumenAlertas As Integer = 0
    Dim T01_ListadoEncargadoLocal As Integer = 1

    Try
      If Not ds Is Nothing Then
        dt = ds.Tables(T00_ResumenAlertas)
        dr = dt.Rows(0)
        nroTransporte = dr.Item("NroTransporte")
        localDestino = dr.Item("LocalDestinoCodigo")

        'obtiene resumen de alertas por el nro viaje
        dsAlertas = Alerta.ResumenViaje(nroTransporte, localDestino)

        'crea el archivo en disco
        Dim nombreHoja As String() = {"RESUMEN VIAJE"}
        nombreArchivo = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_ResumenAlertas")
        fueGenerado = GenerarExcel(dsAlertas, nombreArchivo, nombreCarpetaLog, nombreHoja)

        If (fueGenerado) Then
          listadoMailConCopiaOculta = Utilidades.MailAdministrador()

          'obtiene email del encargado del local
          listadoMailPara = Utilidades.ObtenerListadoMailDesdeDataSet(dsAlertas, T01_ListadoEncargadoLocal, "Email")

          'construye asunto del mail
          asuntoMail = "[ALTOTRACK POCHTECA] Resumen de alertas IdMaster {NRO_TRANSPORTE}"
          asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

          'construye cuerpo del mail
          bodyMensaje = "Estimado(a){BR}{BR}En el presente mail se adjunta el resumen de alertas para el Nro Transporte {NRO_TRANSPORTE}.{BR}{BR}(este mail se ejecutó automáticamente por el sistema. No responder por favor.)"
          bodyMensaje = bodyMensaje.Replace("{BR}", "<br />")
          bodyMensaje = bodyMensaje.Replace("{NRO_TRANSPORTE}", nroTransporte)

          'si esta en modo debug entonces envia el mail al correo de pruebas
          'sino lo envia a quien corresponda
          If Utilidades.MailModoDebug Then
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
            ColeccionEmailsCC = Nothing
            ColeccionEmailsCCO = Nothing
          Else
            ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
            ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
            ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
          End If

          'obtiene ruta del archivo excel generado
          rutaArchivoExcel = Utilidades.RutaLogsSitio() & "\" & nombreCarpetaLog & "\" & nombreArchivo & ".xls"

          'crea contenido para crear el log
          contenidoLog = Herramientas.ObtenerTablaHTMLDesdeDataSet(dsAlertas, T00_ResumenAlertas)
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(contenidoLog & "<br/><div>Enviado a: " & listadoMailPara & "</div>", nombreCarpetaLog)

          'envia el mail
          msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                   Utilidades.MailEmisor, _
                                   Utilidades.MailPasswordEmisor, _
                                   Utilidades.MailNombreEmisor, _
                                   ColeccionEmailsPara, _
                                   asuntoMail, _
                                   bodyMensaje, _
                                   True, _
                                   ColeccionEmailsCC, _
                                   True, _
                                   rutaArchivoExcel, _
                                   "", _
                                   ColeccionEmailsCCO)
        Else
          contenidoLog = "Archivo excel no fue generado"
          msgProceso = "Error excel"
        End If

        'graba el mensaje enviado por mail en el disco
        If msgProceso <> String.Empty Then contadorErrores = 1
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
        nombreArchivo = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(contenidoLog & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), nombreCarpetaLog, nombreArchivo, "html")

      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

#Region "RespuestaAlertaRoja"
  ''' <summary>
  ''' envia resumen con los planes de accion cuando se finaliza la gestion de la alerta roja
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub RespuestaAlertaRoja(ByVal nroTransporte As String, ByVal idAlertaRojaEstadoActual As String)
    Dim ds As DataSet
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim listadoMailConCopia As String = ""

    Try
      ds = Alerta.ObtenerDetalleRespuestaAlertaRoja(nroTransporte, idAlertaRojaEstadoActual, Alerta.ENTIDAD_ALERTA_ROJA_RESPUESTA)

      If Not ds Is Nothing Then
        'obtiene los correos a quienes debe enviar la informacion
        listadoMailPara = Configuracion.Leer("jobListadoMailRespuestaAlerta")
        listadoMailConCopiaOculta = Utilidades.MailAdministrador()

        'construye asunto del mail
        asuntoMail = "[ALTOTRACK POCHTECA] Viaje Finalizado - IdMaster {NRO_TRANSPORTE}"
        asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

        'construye cuerpo del mail
        bodyMensaje = RespuestaAlertaRoja(ds, nroTransporte)

        'si esta en modo debug entonces envia el mail al correo de pruebas
        'sino lo envia a quien corresponda
        If Utilidades.MailModoDebug Then
          ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
          ColeccionEmailsCC = Nothing
          ColeccionEmailsCCO = Nothing
        Else
          ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
          ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
          ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
        End If

        'envia el mail
        msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                 Utilidades.MailEmisor, _
                                 Utilidades.MailPasswordEmisor, _
                                 Utilidades.MailNombreEmisor, _
                                 ColeccionEmailsPara, _
                                 asuntoMail, _
                                 bodyMensaje, _
                                 True, _
                                 ColeccionEmailsCC, _
                                 False, _
                                 "", _
                                 "", _
                                 ColeccionEmailsCCO)
        'graba el mensaje enviado por mail en el disco
        If msgProceso <> String.Empty Then contadorErrores = 1
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
        Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), "RespuestaAlerta", nombreArchivo, "html")
      End If
    Catch ex As Exception
      Exit Sub
    End Try

  End Sub

  Public Shared Function RespuestaAlertaRoja(ByVal ds As DataSet, ByVal nroTransporte As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim sbEscalamiento As New StringBuilder
    Dim templateCabecera, templateTitulo, template, templateAux, templateFila, templateFilaAux As String
    Dim planAccion, montoRecuperar, montoRecuperado, observacion As String
    Dim dt As DataTable
    Dim dr As DataRow

    Try
      templateCabecera = "<div style=""font-family: Arial; font-size: 12px;"">" & _
                         "  Estimado:<br />" & _
                         "  A continuaci&oacute;n se detalla el siguiente viaje finalizado:" & _
                         "</div>"

      templateTitulo = "<div style=""font-family:Arial; font-size:14px; font-weight:bold; color:#fff; background-color:#333; padding:10px;"">{TITULO}</div>"

      template = "<tr valign=""top"">" & _
                 "  <td style=""font-family:Arial; font-size:12px;"">{PLAN_ACCION}</td>" & _
                 "</tr>"

      templateFila = "<tr valign=""top"">" & _
                     "  <td style=""font-family:Arial; font-size:12px; font-weight: bold; width:180px"">{LABEL}:</td>" & _
                     "  <td style=""font-family:Arial; font-size:12px;"">{TEXTO}</td>" & _
                     "</tr>"

      sb.AppendLine(templateCabecera)

      'Titulo
      sb.Append("<br />")
      templateTitulo = templateTitulo.Replace("{TITULO}", "DETALLE VIAJE")
      sb.AppendLine(templateTitulo)

      dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T03_EstadoAlertaRoja)
      Dim row As DataRow = dt.Rows(0)

      If (dt.Rows.Count > 0) Then
        montoRecuperar = row.Item("montoUnoTransportista")
        montoRecuperado = row.Item("montoDosTransportista")
        observacion = row.Item("AlertaRojaObservacionEstado")
      Else
        montoRecuperado = ""
        montoRecuperar = ""
        observacion = ""
      End If

      'Creo la tabla
      sb.Append("<div>")
      sb.Append("  <table border=""0"" cellpadding=""0"" cellspacing=""5"">")

      templateFilaAux = templateFila
      templateFilaAux = templateFilaAux.Replace("{LABEL}", "IdMaster")
      templateFilaAux = templateFilaAux.Replace("{TEXTO}", nroTransporte)
      sb.Append(templateFilaAux)

      templateFilaAux = templateFila
      templateFilaAux = templateFilaAux.Replace("{LABEL}", "Monto a Recuperar")
      templateFilaAux = templateFilaAux.Replace("{TEXTO}", "$" & montoRecuperar)
      sb.Append(templateFilaAux)

      templateFilaAux = templateFila
      templateFilaAux = templateFilaAux.Replace("{LABEL}", "Monto Recuperado")
      templateFilaAux = templateFilaAux.Replace("{TEXTO}", "$" & montoRecuperado)
      sb.Append(templateFilaAux)

      templateFilaAux = templateFila
      templateFilaAux = templateFilaAux.Replace("{LABEL}", "Observaci&oacute;n")
      templateFilaAux = templateFilaAux.Replace("{TEXTO}", observacion)
      sb.Append(templateFilaAux)

      sb.Append("  </table>")
      sb.Append("</div>")

      sb.Append("<br />")

      sb.Append("<div>")
      sb.Append("  <table border=""1"" cellpadding=""3"" cellspacing=""0"" style=""border-width:1px;width:80%;border-collapse:collapse; border: 1px solid #aaa"">")
      sb.Append("    <thead>")
      sb.Append("      <tr>")
      sb.Append("        <th style=""font-family:Arial; font-size:12px; background-color: #ccc; "">Plan de acción</th>")
      sb.Append("      </tr>")
      sb.Append("    </thead>")
      sb.Append("    <tbody>")

      dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T04_EstadoTransportista)
      Dim listadoFiltrado = dt.Select("Aprobada = 1")

      For Each dr In listadoFiltrado
        planAccion = dr.Item("texto")
        templateAux = template
        templateAux = templateAux.Replace("{PLAN_ACCION}", planAccion)
        sb.Append(templateAux)
      Next

      sb.Append("    </tbody>")
      sb.Append("  </table>")
      sb.Append("</div>")

      html = sb.ToString()
      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

#End Region

#Region "CierreViaje"
  ''' <summary>
  ''' envia mail con el detalle de la operacion registrada
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub CierreViaje(ByVal ds As DataSet)
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, asuntoMail, nroTransporte, idEmbarque As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim dt As DataTable
    Dim dr As DataRow
    Dim listadoMailConCopia As String = ""

    Try
      If Not ds Is Nothing Then
        dt = ds.Tables(Alerta.eTabla.T00_Detalle)
        dr = dt.Rows(0)
        nroTransporte = dr.Item("NroTransporte")
        idEmbarque = dr.Item("IdEmbarque")

        'construye asunto del mail
        asuntoMail = "[ALTOTRACK POCHTECA] Alerta con cierre de viaje: IdMaster {NRO_TRANSPORTE} - IdEmbarque {ID_EMBARQUE}"
        asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)
        asuntoMail = asuntoMail.Replace("{ID_EMBARQUE}", idEmbarque)

        'construye cuerpo del mail
        bodyMensaje = GestionAlerta_Detalle(ds, "CierreViaje")
        bodyMensaje &= GestionAlerta_HistorialEscalamientos(ds)

        'si esta en modo debug entonces envia el mail al correo de pruebas
        'sino lo envia a quien corresponda
        If Utilidades.MailModoDebug Then
          ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
          ColeccionEmailsCC = Nothing
        Else
          listadoMailPara = Configuracion.Leer("webListadoEmailCierreViaje")
          listadoMailConCopia = Utilidades.MailAdministrador()

          ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
          ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
        End If

        'envia el mail
        msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                 Utilidades.MailEmisor, _
                                 Utilidades.MailPasswordEmisor, _
                                 Utilidades.MailNombreEmisor, _
                                 ColeccionEmailsPara, _
                                 asuntoMail, _
                                 bodyMensaje, _
                                 True, _
                                 ColeccionEmailsCC, _
                                 False, _
                                 "", _
                                 "", _
                                 Nothing)
        'graba el mensaje enviado por mail en el disco
        If msgProceso <> String.Empty Then contadorErrores = 1
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
        Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), "GestionAlertaCierreViaje", nombreArchivo, "html")
      End If

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

#Region "CargaPoolPatente"
  ''' <summary>
  ''' envia carga de placas por email
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub CargaPoolPatente(ByVal idUsuario As String, ByVal fechaCarga As DateTime)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim msgProceso As String = ""
    Dim contadorErrores, totalRegistros As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nombreArchivo, rutaArchivoExcel, contenidoLog As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim ds As New DataSet
    Dim dv, dvUsuario As New DataView
    Dim listadoMailConCopia As String = ""
    Dim dsFiltrado, dsFiltradoUsuario As New DataSet
    Dim fueGenerado As Boolean
    Dim nombreCarpetaLog As String = "EnvioPoolCarga"
    Dim T00_ListadoCarga As Integer = 0
    Dim T01_ListadoEncargadosCedis As Integer = 1
    Dim arrayGroupByIdUsuario As New ArrayList
    Dim idUsuarioCedis, listadoCedisAsociados, emailEncargado As String
    Dim arrCedisAsociados As List(Of String)

    Try
      ds = Sistema.ObtenerCargaPoolPlaca(idUsuario, fechaCarga)

      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(T00_ListadoCarga).Rows.Count

        If (totalRegistros > 0) Then
          'obtiene idUsuarios unicos
          arrayGroupByIdUsuario = Utilidades.ObtenerArrayGroupByDeDataTable(ds.Tables(T01_ListadoEncargadosCedis), "IdUsuario")

          'recorre los usuarios para obtener los cedis asignados y filtrar los datos de la carga para enviarlo por email
          For Each idUsuarioCedis In arrayGroupByIdUsuario
            arrCedisAsociados = New List(Of String)
            Dim query = From row As DataRow In ds.Tables(T01_ListadoEncargadosCedis).Rows Where row.Item("IdUsuario") = idUsuarioCedis
            For Each row In query
              arrCedisAsociados.Add(row.Item("CodigoInterno"))
            Next
            listadoCedisAsociados = String.Join(",", arrCedisAsociados.ToArray())

            'filtra la carga de placas
            dv = ds.Tables(T00_ListadoCarga).DefaultView
            dv.RowFilter = "DETERMINANTE IN (" & listadoCedisAsociados & ")"
            dsFiltrado = Herramientas.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            'filtra los datos del usuario
            dvUsuario = ds.Tables(T01_ListadoEncargadosCedis).DefaultView
            dvUsuario.RowFilter = "IdUsuario = " & idUsuarioCedis
            dsFiltradoUsuario = Herramientas.CrearDataSetDesdeDataViewConRowFilter(dvUsuario)
            dvUsuario.RowFilter = ""
            emailEncargado = dsFiltradoUsuario.Tables(0).Rows(0).Item("Email")

            'crea el archivo en disco
            Dim nombreHoja As String() = {"POOL PLACA"}
            nombreArchivo = Archivo.CrearNombreUnicoArchivo("PoolPlaca")
            fueGenerado = GenerarExcel(dsFiltrado, nombreArchivo, nombreCarpetaLog, nombreHoja)

            If (fueGenerado) Then
              listadoMailPara = emailEncargado
              listadoMailConCopiaOculta = Utilidades.MailAdministrador()

              asuntoMail = "[ALTOTRACK POCHTECA] Listado carga pool de placas del día {FECHA_CARGA}"
              asuntoMail = asuntoMail.Replace("{FECHA_CARGA}", Replace(fechaCarga.ToString("dd/MM/yyyy"), "-", "/"))

              bodyMensaje = "Estimado(a){BR}{BR}En el presente mail se adjunta el listado de placas cargas del día {FECHA_CARGA}.{BR}{BR}(este mail se ejecutó automáticamente por el sistema. No responder por favor.)"
              bodyMensaje = bodyMensaje.Replace("{BR}", "<br />")
              bodyMensaje = bodyMensaje.Replace("{FECHA_CARGA}", Replace(fechaCarga.ToString("dd/MM/yyyy"), "-", "/"))

              'si esta en modo debug entonces envia el mail al correo de pruebas
              'sino lo envia a quien corresponda
              If Utilidades.MailModoDebug Then
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
                ColeccionEmailsCC = Nothing
                ColeccionEmailsCCO = Nothing
              Else
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
                ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
                ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
              End If

              'obtiene ruta del archivo excel generado
              rutaArchivoExcel = Utilidades.RutaLogsSitio() & "\" & nombreCarpetaLog & "\" & nombreArchivo & ".xls"

              'crea contenido para crear el log
              contenidoLog = Herramientas.ObtenerTablaHTMLDesdeDataSet(dsFiltrado)
              contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(contenidoLog & "<br/><div>Enviado a: " & listadoMailPara & "</div>", nombreCarpetaLog)

              'envia el mail
              msgProceso = Mail.Enviar(Utilidades.MailHostHD, _
                                       Utilidades.MailEmisor, _
                                       Utilidades.MailPasswordEmisor, _
                                       Utilidades.MailNombreEmisor, _
                                       ColeccionEmailsPara, _
                                       asuntoMail, _
                                       bodyMensaje, _
                                       True, _
                                       ColeccionEmailsCC, _
                                       True, _
                                       rutaArchivoExcel, _
                                       "", _
                                       ColeccionEmailsCCO)
              Sistema.GrabarLogSesion(oUsuario.Id, "Envia carga pool de placas por email: IdUsuarioCedis " & idUsuarioCedis & " / FechaCarga " & fechaCarga)
            Else
              contenidoLog = "Archivo excel no fue generado"
              msgProceso = "Error excel"
            End If

            'graba el mensaje enviado por mail en el disco
            If msgProceso <> String.Empty Then contadorErrores = 1
            Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
            nombreArchivo = Archivo.CrearNombreUnicoArchivo("PoolPlaca_" & sufijoArchivo)
            Archivo.CrearArchivoTextoEnDisco(contenidoLog & "<br / >" & msgProceso, Utilidades.RutaLogsSitio(), nombreCarpetaLog, nombreArchivo, "html")
          Next

        End If
      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

End Class
