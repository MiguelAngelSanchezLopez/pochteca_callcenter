﻿Imports System.Data

Partial Public Class wucMultiselectCombinados
  Inherits System.Web.UI.UserControl

#Region "Variables de Configuracion"
  'variables privadad
  Private Const NOMBRE_WEB_USER_CONTROL As String = "wucMultiselectCombinados"
  Private Const NOMBRE_VARIABLE_PREFIJO_CONTROL_JS As String = "prefijoControlMultiselectCombinados"
  Private Const NOMBRE_ARCHIVO_JS As String = NOMBRE_WEB_USER_CONTROL & ".js"

#End Region

#Region "Propiedades del control"
  'propiedades
  Public Property ValueListadoId() As String
    Get
      Return Me.txtIdsHija.Value
    End Get
    Set(ByVal value As String)
      Me.txtIdsHija.Value = value
    End Set
  End Property

  Public WriteOnly Property DataSourcePadre() As Object
    Set(ByVal value As Object)
      Me.lstPadre.DataSource = value
    End Set
  End Property

  Public WriteOnly Property DataTextFieldPadre() As String
    Set(ByVal value As String)
      Me.lstPadre.DataTextField = value
    End Set
  End Property

  Public WriteOnly Property DataValueFieldPadre() As String
    Set(ByVal value As String)
      Me.lstPadre.DataValueField = value
    End Set
  End Property

  Public WriteOnly Property DataSourceHija() As Object
    Set(ByVal value As Object)
      Me.lstHija.DataSource = value
    End Set
  End Property

  Public WriteOnly Property DataTextFieldHija() As String
    Set(ByVal value As String)
      Me.lstHija.DataTextField = value
    End Set
  End Property

  Public WriteOnly Property DataValueFieldHija() As String
    Set(ByVal value As String)
      Me.lstHija.DataValueField = value
    End Set
  End Property

  Public Property CssClassListBoxPadre() As String
    Get
      Return Me.lstPadre.CssClass
    End Get
    Set(ByVal value As String)
      Me.lstPadre.CssClass = value
    End Set
  End Property

  Public Property CssClassListBoxHija() As String
    Get
      Return Me.lstHija.CssClass
    End Get
    Set(ByVal value As String)
      Me.lstHija.CssClass = value
    End Set
  End Property

  Public Property RowsListBoxPadre() As Integer
    Get
      Return Me.lstPadre.Rows
    End Get
    Set(ByVal value As Integer)
      Me.lstPadre.Rows = value
    End Set
  End Property

  Public Property RowsListBoxHija() As Integer
    Get
      Return Me.lstHija.Rows
    End Get
    Set(ByVal value As Integer)
      Me.lstHija.Rows = value
    End Set
  End Property

  Public Property WidthListBoxPadre() As Unit
    Get
      Return Me.lstPadre.Width
    End Get
    Set(ByVal value As Unit)
      Me.lstPadre.Width = value
    End Set
  End Property

  Public Property WidthListBoxHija() As Unit
    Get
      Return Me.lstHija.Width
    End Get
    Set(ByVal value As Unit)
      Me.lstHija.Width = value
    End Set
  End Property

  Public Property EnabledListBoxPadre() As Boolean
    Get
      Return Me.lstPadre.Enabled
    End Get
    Set(ByVal value As Boolean)
      Me.lstPadre.Enabled = value
    End Set
  End Property

  Public Property EnabledListBoxHija() As Boolean
    Get
      Return Me.lstHija.Enabled
    End Get
    Set(ByVal value As Boolean)
      Me.lstHija.Enabled = value
    End Set
  End Property

  Public Property CssClassLabelPadre() As String
    Get
      Return Me.lblPadre.CssClass
    End Get
    Set(ByVal value As String)
      Me.lblPadre.CssClass = value
    End Set
  End Property

  Public Property CssClassLabelHija() As String
    Get
      Return Me.lblHija.CssClass
    End Get
    Set(ByVal value As String)
      Me.lblHija.CssClass = value
    End Set
  End Property

  Public Property VisibleLabelPadre() As Boolean
    Get
      Return Me.lblPadre.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.lblPadre.Visible = value
    End Set
  End Property

  Public Property VisibleLabelHija() As Boolean
    Get
      Return Me.lblHija.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.lblHija.Visible = value
    End Set
  End Property

  Public Property TextLabelPadre() As String
    Get
      Return Me.lblPadre.Text
    End Get
    Set(ByVal value As String)
      Me.lblPadre.Text = value
    End Set
  End Property

  Public Property TextLabelHija() As String
    Get
      Return Me.lblHija.Text
    End Get
    Set(ByVal value As String)
      Me.lblHija.Text = value
    End Set
  End Property

  Public Property CssClassCheckTodosPadre() As String
    Get
      Return Me.chkTodosPadre.CssClass
    End Get
    Set(ByVal value As String)
      Me.chkTodosPadre.CssClass = value
    End Set
  End Property

  Public Property CssClassCheckTodosHija() As String
    Get
      Return Me.chkTodosHija.CssClass
    End Get
    Set(ByVal value As String)
      Me.chkTodosHija.CssClass = value
    End Set
  End Property

  Public Property VisibleCheckTodosPadre() As Boolean
    Get
      Return Me.holderCheckTodosPadre.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.holderCheckTodosPadre.Visible = value
    End Set
  End Property

  Public Property VisibleCheckTodosHija() As Boolean
    Get
      Return Me.holderCheckTodosHija.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.holderCheckTodosHija.Visible = value
    End Set
  End Property

  Public Property TextCheckTodosPadre() As String
    Get
      Return Me.chkTodosPadre.Text
    End Get
    Set(ByVal value As String)
      Me.chkTodosPadre.Text = value
    End Set
  End Property

  Public Property TextCheckTodosHija() As String
    Get
      Return Me.chkTodosHija.Text
    End Get
    Set(ByVal value As String)
      Me.chkTodosHija.Text = value
    End Set
  End Property

  Public Property CssClassLabelAyuda() As String
    Get
      Return Me.lblAyuda.CssClass
    End Get
    Set(ByVal value As String)
      Me.lblAyuda.CssClass = value
    End Set
  End Property

  Public Property VisibleLabelAyuda() As Boolean
    Get
      Return Me.lblAyuda.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.lblAyuda.Visible = value
    End Set
  End Property

  Public Property TextLabelAyuda() As String
    Get
      Return Me.lblAyuda.Text
    End Get
    Set(ByVal value As String)
      Me.lblAyuda.Text = value
    End Set
  End Property

  Public Property InvocarFuncionesEspeciales() As String
    Get
      Return Me.txtFuncionesEspeciales.Value
    End Get
    Set(ByVal value As String)
      Me.txtFuncionesEspeciales.Value = value
    End Set
  End Property

#End Region

#Region "Privados"
  ''' <summary>
  ''' registra en la pantalla como variable global de javascript el nombre del prefijo de los controles para ser usado
  ''' en los metodos de validacion del js asociado
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RegistrarJavaScript()
    Dim clientId As String = Me.pnlBloque.ClientID
    Dim id As String = Me.pnlBloque.ID
    Dim prefijoControl As String = clientId.Replace(id, "")
    Dim DataScript As String = ""

    'escribe bloque js para registrar el prefijo del control
    If Me.Visible Then
      DataScript &= "<script language=""javascript"" type=""text/javascript"">" & vbCrLf
      DataScript &= NOMBRE_VARIABLE_PREFIJO_CONTROL_JS & " = """ & prefijoControl & """;" & vbCrLf
      DataScript &= "</script>" & vbCrLf
      DataScript &= "<script type=""text/javascript"" src=""../js/" & NOMBRE_ARCHIVO_JS & """></script>" & vbCrLf
      'registra bloque javascript en la pantalla
      Page.ClientScript.RegisterClientScriptBlock(Page.GetType, prefijoControl & "script", DataScript)
    End If
  End Sub

  Private Sub InicializaControles()
    Me.chkTodosHija.Attributes.Add("onclick", "wucmsSelectAll('" & Me.lstHija.ClientID & "','" & Me.chkTodosHija.ClientID & "')")
    Me.chkTodosPadre.Attributes.Add("onclick", "wucmsSelectAll('" & Me.lstPadre.ClientID & "','" & Me.chkTodosPadre.ClientID & "')")
    Me.lbtnAgregarItem.Attributes.Add("onclick", "return(wucmsAgregarItem())")
    Me.lbtnQuitarItem.Attributes.Add("onclick", "return(wucmsQuitarItem())")
    Me.lstHija.Attributes.Add("ondblclick", "return(wucmsQuitarItem())")
    Me.lstPadre.Attributes.Add("ondblclick", "return(wucmsAgregarItem())")
  End Sub
#End Region

#Region "Publicos"
  Public Sub DataBindPadre()
    Me.lstPadre.DataBind()
  End Sub

  Public Sub DataBindHija()
    Me.lstHija.DataBind()
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'registra los bloques javascript respectivos
    RegistrarJavaScript()
    InicializaControles()
  End Sub

End Class