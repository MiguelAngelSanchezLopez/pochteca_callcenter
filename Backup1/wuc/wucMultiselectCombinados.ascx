﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucMultiselectCombinados.ascx.vb" Inherits="WebTransportePochteca.wucMultiselectCombinados" %>

<asp:Panel ID="pnlBloque" runat="server">
  <div class="row">
    <div class="col-xs-4">
      <div><asp:Label ID="lblPadre" runat="server" CssClass="control-label" Text="Seleccione" Font-Bold="true"></asp:Label></div>
      <div><asp:ListBox ID="lstPadre" runat="server" Rows="6" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox></div>
      <asp:Panel ID="holderCheckTodosPadre" runat="server"><asp:CheckBox ID="chkTodosPadre" runat="server" Text="Todos" /></asp:Panel>
    </div>
    <div class="col-xs-1 text-center">
      <br /><br />
      <div id="lbtnAgregarItem" runat="server" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-right"></span></div>
      <br /><br />
      <div id="lbtnQuitarItem" runat="server" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left"></span></div>
      <asp:HiddenField ID="txtIdsHija" runat="server" Value="" />
    </div>
    <div class="col-xs-4">
      <div><asp:Label ID="lblHija" runat="server" CssClass="control-label" Text="Items Seleccionados" Font-Bold="true"></asp:Label></div>
      <div><asp:ListBox ID="lstHija" runat="server" Rows="6" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox></div>
      <asp:Panel ID="holderCheckTodosHija" runat="server"><asp:CheckBox ID="chkTodosHija" runat="server" Text="Todos" /></asp:Panel>
    </div>
    <input type="hidden" id="txtFuncionesEspeciales" runat="server" value="" />
  </div>
  <div><asp:Label ID="lblAyuda" runat="server" CssClass="help-block" Text="(Para seleccionar m&aacute;s de un valor deje presionada la tecla CTRL y clickee con el mouse sobre la opci&oacute;n a escoger)" Visible="false"></asp:Label></div>
</asp:Panel>

<script type="text/javascript" language="javascript">
  //obtiene los objetos
  var lstPadre              = document.getElementById("<%= lstPadre.ClientID %>");
  var lstHija               = document.getElementById("<%= lstHija.ClientID %>");
  var txtIdsHija            = document.getElementById("<%= txtIdsHija.ClientID %>");
  var chkTodosPadre         = document.getElementById("<%= chkTodosPadre.ClientID %>");
  var chkTodosHija          = document.getElementById("<%= chkTodosHija.ClientID %>");
  var holderCheckTodosPadre = document.getElementById("<%= holderCheckTodosPadre.ClientID %>");
  var holderCheckTodosHija  = document.getElementById("<%= holderCheckTodosHija.ClientID %>");
  var txtFuncionesEspeciales = document.getElementById("<%= txtFuncionesEspeciales.ClientID %>");

  //inicializa controles
  wucmsMostrarCheckTodos(lstPadre,chkTodosPadre,holderCheckTodosPadre);
  wucmsMostrarCheckTodos(lstHija,chkTodosHija,holderCheckTodosHija);
  wucmsObtenerIdSeleccionados();  
</script>

