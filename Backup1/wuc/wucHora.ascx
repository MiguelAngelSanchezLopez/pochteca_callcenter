﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucHora.ascx.vb" Inherits="WebTransportePochteca.wucHora" %>
<div class="row">
  <div class="col-xs-6">
    <asp:DropDownList ID="ddlHora" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>
    <em class="help-block text-center2 small">(hora)</em>
  </div>
  <div class="col-xs-6">
    <asp:DropDownList ID="ddlMinutos" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>
    <em class="help-block text-center2 small">(minutos)</em>
  </div>
</div>
