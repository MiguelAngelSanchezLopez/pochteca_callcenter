﻿var Alerta = {
  FORMULARIO_TO_GESTION_ALERTA: "TO.GestionAlerta",
  FORMULARIO_SUPERVISOR_GESTION_ALERTA: "Supervisor.GestionAlerta",
  FORMULARIO_MANTENEDOR_ALERTA_DETALLE: "Mantenedor.AlertaDetalle",
  FORMULARIO_COMMON_ASOCIAR_GRUPO: "Common.AsociarGrupo",
  FORMULARIO_COMMON_HISTORIAL_LLAMADAS_ALERTA: "Common.HistorialLlamadasAlerta",
  FORMULARIO_BUSCADOR_ALERTAS: "Buscador.Alertas",
  SETINTERVAL_CRONOMETRO: 0,
  SETINTERVAL_ALERTAS_POR_ATENDER: 0,
  SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_ATENDER: 20000, // 20000 (20 segundos)
  SETINTERVAL_ALERTAS_POR_GESTIONAR: 0,
  SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_GESTIONAR: 300000, // 300000 = 60000 * 5 (5 minutos)
  SETINTERVAL_CRONOMETRO_AUXILIAR: 0,
  SETINTERVAL_MILISEGUNDOS_MOSTRAR_FINALIZAR_LLAMADA: 5000, // 5000 (5 segundos)
  ARRAY_SETINTERVAL_CRONOMETRO_ALERTA_SIN_ATENDER: [],
  GRUPO_CONTACTO_CONDUCTOR: "CONDUCTOR",
  GRUPO_CONTACTO_TRANSPORTISTA: "TRANSPORTISTA",
  PREFIJO_ESCALAMIENTO: "Escalamiento",
  PREFIJO_GRUPO: "Grupo",
  NO_CONTESTA: "NO CONTESTA",
  ARRAY_SCRIPT_GRUPO_CONTACTO: [],
  SONIDO_ALARMA_NUEVA: "sms_ringtone",
  SONIDO_ALARMA_SIGUIENTE_ESCALAMIENTO: "button_tiny",
  EXPLICACION_NO_DISPONIBLE_HORARIO: "No disponible horario nocturno",
  FAC_MULTIPLICADOR_MINUTO_OCURRENCIA: 5,
  TIPO_GRUPO_PERSONALIZADO: "Personalizado",
  REPORTABILIDAD_GPS_OFFLINE: "OFFLINE",
  REPORTABILIDAD_GPS_ONLINE: "ONLINE",
  REPORTABILIDAD_GPS_NO_INTEGRADA: "NOINTEGRADA",

  obtenerAlertasPorAtender: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerAlertasPorAtender";

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener las alertas por atender");
        } else {
          var json = jQuery.parseJSON(respuesta);

          //primero revisa si tiene alguna alerta asignada con anterioridad, si es asi entonces obtiene sus datos,
          //sino revisa que cuando se habian terminado las alertas y llega una nueva, se asigna automaticamente al usuario conectado
          if (opciones.revisarAlertaAsignadaPreviamente || (!Sistema.contains(json.listadoColaAtencion, "No hay") && jQuery("#txtIdAlerta").val() == "")) {
            Alerta.obtenerDetalle({ idAlerta: "-1", desdeFormulario: Alerta.FORMULARIO_TO_GESTION_ALERTA })
          }

        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener las alertas por atender");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasPorAtender:\n" + e);
    }
  },

  obtenerAlertasAtendidasHoy: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerAlertasAtendidasHoy";

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener las alertas atendidas el día de hoy");
        } else {
          var json = jQuery.parseJSON(respuesta);
          jQuery("#hListadoAlertasAtendidasHoy").html(json.listado);

          if (opciones.cargaDatos == "inicial") {
            jQuery("#hAlertasAtendidasHoy").mCustomScrollbar();
          } else {
            jQuery("#hAlertasAtendidasHoy").mCustomScrollbar("update");
          }

          jQuery("#ulAlertasAtendidasHoy").makeSearchList({ elem_input: "txtBuscarAlertaHoy", tag_html_filtrar: "li" });
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener las alertas atendidas el día de hoy");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasAtendidasHoy:\n" + e);
    }
  },

  obtenerDetalle: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerDetalleAlerta";
      queryString += "&idAlerta=" + opciones.idAlerta;

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener el detalle de la alerta");
        } else {
          switch (opciones.desdeFormulario) {
            case Alerta.FORMULARIO_TO_GESTION_ALERTA:
              Alerta.cargarDatosFormulario_TeleOperador(respuesta, opciones);
              break;
            case Alerta.FORMULARIO_SUPERVISOR_GESTION_ALERTA:
              Alerta.cargarDatosFormulario_Supervisor(respuesta);
              break;
            case Alerta.FORMULARIO_COMMON_HISTORIAL_LLAMADAS_ALERTA:
              Alerta.cargarDatosFormulario_HistorialLlamadas(respuesta);
              break;
            default:
              alert("Debe especificarse el formulario que invoca el detalle de alerta");
              break;
          }
          jQuery(".show-tooltip").tooltip();
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener el detalle de la alerta");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.obtenerDetalle:\n" + e);
    }
  },

  cargarDatosFormulario_TeleOperador: function (elem_json, opciones) {
    var json, item, idAlerta, prioridad, tipoAlerta, proximoEscalamiento, nombreAlerta, nroTransporte, revisarAlertaAsignadaPreviamente, fechaCreacion;
    var fechaAsignacion, arrFechaAsignacion, permiso, idFormato, nombreFormato, idEmbarque, tipoFlota, patenteTracto, patenteTrailer;
    var reportabilidadTracto, reportabilidadTrailer;

    try {
      json = jQuery.parseJSON(elem_json);

      //carga los datos de la gestion acumulada
      item = json.alertaGestionAcumulada;
      if (item) {
        jQuery("#txtIdAlertaGestionAcumulada").val(item.idAlertaGestionAcumulada);
        jQuery("#txtIdAlertaHija").val(item.idAlertaHija);

        fechaAsignacion = item.fechaAsignacion;
        arrFechaAsignacion = fechaAsignacion.split(/[\/ :-]/);
        revisarAlertaAsignadaPreviamente = item.revisarAlertaAsignadaPreviamente;
        var opciones_cronometro = {
          year: arrFechaAsignacion[0],
          month: arrFechaAsignacion[1],
          day: arrFechaAsignacion[2],
          hours: arrFechaAsignacion[3],
          minutes: arrFechaAsignacion[4],
          seconds: arrFechaAsignacion[5],
          elem_cronometro: "hCronometro"
        };
      }

      //carga datos de la alerta
      item = json.detalle[0];
      if (item) {
        idAlerta = item.IdAlerta;
        nombreAlerta = item.NombreAlerta;
        nroTransporte = item.NroTransporte;
        idEmbarque = item.IdEmbarque;
        proximoEscalamiento = item.ProximoEscalamiento;
        prioridad = item.Prioridad;
        tipoAlerta = Sistema.obtenerStyleAlertaPorPrioridad(prioridad);
        prioridad = Sistema.obtenerLabelPrioridad(prioridad);
        permiso = Sistema.obtenerLabelPermisoZona(item.Permiso);
        idFormato = item.IdFormato;
        nombreFormato = item.NombreFormato;
        fechaCreacion = item.FechaHoraCreacion;
        tipoFlota = item.TipoFlota;

        if (item.NumeroEconomicoTracto === "") {
          patenteTracto = item.PatenteTracto;
        } else {
          patenteTracto = item.PatenteTracto + " / eco. " + item.NumeroEconomicoTracto;
        }

        if (item.NumeroEconomicoTrailer === "") {
          patenteTrailer = item.PatenteTrailer;
        } else {
          patenteTrailer = item.PatenteTrailer + " / eco. " + item.NumeroEconomicoTrailer;
        }

        if (Sistema.toUpper(item.Permiso) == "REPROGRAMADA") { jQuery("#lblAlertaReprogramada").show(); }
        jQuery("#lblTituloNombreAlerta").html(Sistema.toUpper(nombreFormato + " - " + nombreAlerta) + "<br />[IdMaster " + nroTransporte + " / IdEmbarque: " + idEmbarque + "] (" + fechaCreacion.slice(-10) + ") - Tipo Flota " + tipoFlota);
        jQuery("#lblProximoEscalamiento").html(proximoEscalamiento);
        jQuery("#txtIdAlerta").val(idAlerta);
        jQuery("#txtIdFormato").val(idFormato);
        jQuery("#txtProximoEscalamiento").val(proximoEscalamiento);
        jQuery("#txtNombreAlerta").val(nombreAlerta);
        jQuery("#txtTotalEscalamientosAlerta").val(item.TotalEscalamientosAlerta);
        jQuery("#txtLatTracto").val(item.LatTracto);
        jQuery("#txtLonTracto").val(item.LonTracto);
        jQuery("#txtLatTractoActual").val(item.LatTractoActual);
        jQuery("#txtLonTractoActual").val(item.LonTractoActual);
        jQuery("#txtAlertaMapa").val(item.AlertaMapa);
        jQuery("#txtDestinoLocal").val(item.LocalDestinoDescripcion);
        jQuery("#txtDestinoLocalCodigo").val(item.LocalDestinoCodigo);
        jQuery("#lblIdAlertaHija").html(jQuery("#txtIdAlertaHija").val());
        jQuery("#lblPopoverTitulo").html(nombreAlerta);
        jQuery("#lblPopoverPrioridad").html(prioridad);
        jQuery("#lblPopoverNroEscalamientoActual").html(item.NroEscalamientoActual);
        jQuery("#lblPopoverNroTransporte").html(nroTransporte);
        jQuery("#lblPopoverIdEmbarque").html(idEmbarque);
        jQuery("#lblPopoverNombreFormato").html(nombreFormato);
        jQuery("#lblPopoverOrigen").html(item.LocalOrigenDescripcion);
        jQuery("#lblPopoverDestino").html(item.LocalDestinoDescripcion + " (Cód. " + item.LocalDestinoCodigo + ")");
        jQuery("#lblPopoverNombreConductor").html(item.NombreConductor);
        jQuery("#lblPopoverNombreTransportista").html(item.NombreTransportista);
        jQuery("#lblPopoverPatenteTracto").html(patenteTracto);
        jQuery("#lblPopoverPatenteTrailer").html(patenteTrailer);
        jQuery("#lblPopoverVelocidad").html(item.Velocidad);
        jQuery("#lblPopoverTipoViaje").html(item.TipoViaje);
        jQuery("#lblPopoverLatTracto").html(item.LatTracto);
        jQuery("#lblPopoverLonTracto").html(item.LonTracto);
        jQuery("#lblPopoverFechaHoraCreacion").html(item.FechaHoraCreacion);
        jQuery("#lblPopoverTipoAlerta").html(item.TipoAlerta);
        jQuery("#lblPopoverPermiso").html(permiso);
        jQuery("#lblPermisoZona").html(permiso);
        jQuery("#lblPopoverTipoFlota").html(tipoFlota);
        jQuery("#lblPopoverTelefonoLocal").html(item.TelefonoLocal);
        jQuery("#lblPopoverVentanaHoraria").html(item.VentanaHoraria);
        jQuery("#lblPopoverHoraCita").html((item.HoraCita == "") ? "" : item.HoraCita + " hrs.");
        jQuery("#hBotones").show();
        jQuery("#hAtendiendoAlerta").attr("class", "panel panel-" + tipoAlerta);

        reportabilidadTracto = Alerta.obtenerIconoReportabilidadGPS({
          tipo: "Tracto",
          patente: item.PatenteTracto,
          estadoReportabilidad: item.ReportabilidadPatenteTracto,
          fechaReportabilidad: item.ReportabilidadPatenteTractoFecha
        });
        jQuery("#hReportabilidadTracto").html(reportabilidadTracto);

        reportabilidadTrailer = Alerta.obtenerIconoReportabilidadGPS({
          tipo: "Trailer",
          patente: item.PatenteTrailer,
          estadoReportabilidad: item.ReportabilidadPatenteTrailer,
          fechaReportabilidad: item.ReportabilidadPatenteTrailerFecha
        });
        jQuery("#hReportabilidadTrailer").html(reportabilidadTrailer);

        Alerta.mostrarModalNuevaAsignacion({ nombreAlerta: nombreAlerta, nroTransporte: nroTransporte, idEmbarque: idEmbarque, tipoAlerta: tipoAlerta, revisarAlertaAsignadaPreviamente: revisarAlertaAsignadaPreviamente, proximoEscalamiento: proximoEscalamiento, nombreFormato: nombreFormato });
        Sistema.cronometro(opciones_cronometro);
        Alerta.SETINTERVAL_CRONOMETRO = setInterval(function () { Sistema.cronometro(opciones_cronometro); }, 1000);
        Alerta.verEnGoogleMaps({ elem_contenedor_padre: "hMapaCanvas", elem_contenedor_mapa: "map-canvas", latTracto: item.LatTracto, lonTracto: item.LonTracto, alertaMapa: item.AlertaMapa, desdeFormulario: Alerta.FORMULARIO_TO_GESTION_ALERTA });

        //detiene la busqueda de alertas en cola de atencion
        setTimeout(function () { clearInterval(Alerta.SETINTERVAL_ALERTAS_POR_ATENDER); }, 100);
      }

      //carga los contactos
      jQuery("#hContactosEscalamiento").html(json.contactosEscalamiento);

      //carga el historial de escalamientos
      jQuery("#hTablaHistorialEscalamientos").html(json.historialEscalamientos);
      if (Sistema.contains(json.historialEscalamientos, "No hay")) {
        jQuery("#hHistorialEscalamientos").removeClass("mCustomScrollbar sist-height-historial-escalamientos-to");
        jQuery("#hHistorialEscalamientos").addClass("sist-height-historial-escalamientos-sin-datos-to");
        jQuery("#hHistorialEscalamientos").mCustomScrollbar("destroy");
      } else {
        jQuery("#hHistorialEscalamientos").removeClass("sist-height-historial-escalamientos-sin-datos-to");
        jQuery("#hHistorialEscalamientos").addClass("mCustomScrollbar sist-height-historial-escalamientos-to");
        jQuery("#hHistorialEscalamientos").mCustomScrollbar();
      }

      Alerta.dibujarPasosEscalamientos({ elem_json: elem_json, mostrarSuperEscalamiento: false });
      jQuery(".chosen-select").select2();
      Alerta.validarFuncionesContactosCargaInicial();
      Alerta.dibujarListadoAuxiliares(json.listadoAuxiliares);

      if (json.estadoAuxiliarActual.length > 0) {
        jQuery("#txtAuxiliar").val(json.estadoAuxiliarActual[0].Nombre);
        Alerta.mostrarMensajeEstadoAuxiliar({ fecha: json.estadoAuxiliarActual[0].FechaAccion });

        setTimeout(function () {
          clearInterval(Alerta.SETINTERVAL_CRONOMETRO);
          clearInterval(Alerta.SETINTERVAL_ALERTAS_POR_ATENDER);
          Alerta.limpiarFormulario_TeleOperador();
        }, 100);

      }

    } catch (e) {
      alert("Exception Alerta.cargarDatosFormulario_TeleOperador:\n" + e);
    }

  },

  mostrarDatosContacto: function (opciones) {
    var json, idEscalamientoPorAlertaContactoConsultado;

    try {
      json = jQuery("#" + opciones.prefijoControl + "_txtJSONContacto").val();
      json = jQuery.parseJSON(json);
      idEscalamientoPorAlertaContactoConsultado = jQuery("#" + opciones.prefijoControl + "_ddlContacto").val();

      //limpia los controles
      jQuery("#" + opciones.prefijoControl + "_lblCargo").html("-");
      jQuery("#" + opciones.prefijoControl + "_lblTelefono").html("-");
      jQuery("#" + opciones.prefijoControl + "_lblEmail").html("-");

      //busca informacion en json segun el contacto seleccionado
      json = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlertaContacto == idEscalamientoPorAlertaContactoConsultado);
      });
      if (json.length > 0) {
        var item = json[0];

        jQuery("#" + opciones.prefijoControl + "_lblCargo").html(item.Cargo);
        jQuery("#" + opciones.prefijoControl + "_lblTelefono").html(item.Telefono);
        jQuery("#" + opciones.prefijoControl + "_lblTelefono").attr("onclick", "Alerta.marcarTelefono({ idEscalamientoPorAlertaGrupoContacto: '" + opciones.prefijoControl + "' })");

        if (item.Telefono == "") {
          jQuery("#" + opciones.prefijoControl + "_hTelefono").hide();
        } else {
          jQuery("#" + opciones.prefijoControl + "_hTelefono").show();
        }
      }

      Alerta.reemplazarMarcasEspecialesScript({ prefijoControl: opciones.prefijoControl });
    } catch (e) {
      alert("Exception Alerta.mostrarDatosContacto:\n" + e);
    }
  },

  dibujarPasosEscalamientos: function (opciones) {
    var elem_json, json, item, templateActivo, templateInactivo, templateAux, tipoAlerta, superEscalamiento;
    var nroEscalamiento, nroEscalamientoActual;
    var html = "";

    try {
      elem_json = opciones.elem_json;
      mostrarSuperEscalamiento = opciones.mostrarSuperEscalamiento;

      json = jQuery.parseJSON(elem_json);
      item = json.detalle[0];
      superEscalamiento = (mostrarSuperEscalamiento) ? 1 : 0;

      templateActivo = "<li>" +
                       "  <a href=\"javascript:;\" class=\"sist-padding-5\">" +
                       "    <div class=\"alert alert-{TIPO_ALERTA} sist-margin-cero sist-padding-numero-escalamiento\">" +
                       "      <strong class=\"sist-font-size-16\">{NRO_ESCALAMIENTO}</strong>" +
                       "    </div>" +
                       "  </a>" +
                       "</li>";

      templateInactivo = "<li>" +
                         "  <a href=\"javascript:;\" class=\"sist-padding-5\">" +
                         "    <div class=\"panel panel-default sist-margin-cero sist-padding-numero-escalamiento\">" +
                         "      <strong class=\"text-muted sist-font-size-16\">{NRO_ESCALAMIENTO}</strong>" +
                         "    </div>" +
                         "  </a>" +
                         "</li>";

      if (item) {
        nroEscalamientoActual = item.NroEscalamientoActual
        proximoEscalamiento = item.ProximoEscalamiento;
        totalEscalamientosAlerta = (mostrarSuperEscalamiento) ? item.TotalEscalamientosRealizados : item.TotalEscalamientosAlerta;
        totalEscalamientosAlerta += superEscalamiento;

        //si se esta mostrando los pasos en el perfil del supervisor, debe marcar hasta los escalamientos que se completaron
        if (mostrarSuperEscalamiento) {
          proximoEscalamiento = item.TotalEscalamientosRealizados;
        }

        html += "<ul class=\"pagination sist-margin-cero\">";
        for (i = 1; i <= totalEscalamientosAlerta; i++) {
          //obtiene template de acuerdo al escalamiento que debe registrar
          nroEscalamiento = i;
          if (i == totalEscalamientosAlerta && mostrarSuperEscalamiento) {
            templateAux = templateActivo;
            tipoAlerta = "success";
            nroEscalamiento = "Supervisor";
          } else if (i <= proximoEscalamiento) {
            templateAux = templateActivo;
            if (mostrarSuperEscalamiento) {
              tipoAlerta = "info";
            } else {
              tipoAlerta = (i == proximoEscalamiento) ? "success" : "info";
            }
          } else {
            templateAux = templateInactivo;
            tipoAlerta = "";
          }

          templateAux = templateAux.replace("{TIPO_ALERTA}", tipoAlerta);
          templateAux = templateAux.replace("{NRO_ESCALAMIENTO}", nroEscalamiento);
          html += templateAux;
        }
        html += "</ul>";
      }

      jQuery("#hPasosEscalamientos").html(html);
    } catch (e) {
      alert("Exception Alerta.dibujarPasosEscalamientos:\n" + e);
    }
  },

  habilitarCampoOtro: function (opciones) {
    var valor, prefijoControl, ddlControl, txtControl;

    try {
      prefijoControl = opciones.prefijoControl + ((opciones.prefijoControl == "") ? "" : "_");
      ddlControl = opciones.ddlControl;
      txtControl = opciones.txtControl;


      valor = jQuery("#" + prefijoControl + ddlControl).val();
      jQuery("#" + prefijoControl + txtControl).val("");

      if (valor == "MostrarCampoOtro") {
        jQuery("#" + prefijoControl + txtControl).attr("disabled", false);
      } else {
        jQuery("#" + prefijoControl + txtControl).attr("disabled", true);
      }

    } catch (e) {
      alert("Exception Alerta.habilitarCampoOtro:\n" + e);
    }

  },

  validarFormularioAlerta_TeleOperador: function () {
    var explicacion, requeridoOtro, item;

    try {
      var jsonValidarCampos = [];

      //obtiene el listado de contactos seleccionados
      jQuery(".observacion-contacto-escalamiento").each(function (indice, obj) {
        var elem_txtObservacion = jQuery(obj).attr("id");
        var prefijoControl = elem_txtObservacion.replace("txtObservacion", "");
        var elem_ddlExplicacion = prefijoControl + "ddlExplicacion";
        var elem_txtExplicacionOtro = prefijoControl + "txtExplicacionOtro";
        var elem_ddlContacto = prefijoControl + "ddlContacto";
        var elem_hGrupoContacto = prefijoControl + "hGrupoContacto";

        if (jQuery("#" + elem_hGrupoContacto).is(":visible")) {
          //agrega validacion campo: ddlContacto
          item = { elemento_a_validar: elem_ddlContacto, requerido: true }
          jsonValidarCampos.push(item);

          //agrega validacion campo: ddlExplicacion
          item = { elemento_a_validar: elem_ddlExplicacion, requerido: true }
          jsonValidarCampos.push(item);

          //agrega validacion campo: txtExplicacionOtro
          explicacion = jQuery("#" + elem_ddlExplicacion).val();
          requeridoOtro = (explicacion == "MostrarCampoOtro") ? true : false;
          item = { elemento_a_validar: elem_txtExplicacionOtro, requerido: requeridoOtro, tipo_validacion: "caracteres-prohibidos" }
          jsonValidarCampos.push(item);

          //agrega validacion campo: txtObservacion
          item = { elemento_a_validar: elem_txtObservacion, requerido: false, tipo_validacion: "caracteres-prohibidos" }
          jsonValidarCampos.push(item);

        }
      });

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        jQuery("#hBotones").hide();
        jQuery("#hMensajeGrabando").show();
        setTimeout(function () { Alerta.grabarFormularioAlerta_TeleOperador(); }, 100);
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioAlerta_TeleOperador:\n" + e);
    }
  },

  grabarFormularioAlerta_TeleOperador: function () {
    var queryString = "";

    try {
      json = Alerta.generarJSONGestionAlerta_TeleOperador();

      //construye queryString
      queryString += "op=GrabarGestionAlerta_TeleOperador";
      queryString += "&json=" + Sistema.urlEncode(json);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo grabar la gestion de la alerta");
          jQuery("#hBotones").show();
          jQuery("#hMensajeGrabando").hide();
        } else {
          json = jQuery.parseJSON(respuesta);
          clearInterval(Alerta.SETINTERVAL_CRONOMETRO);
          clearInterval(Alerta.SETINTERVAL_ALERTAS_POR_ATENDER);
          Alerta.limpiarFormulario_TeleOperador();

          Alerta.obtenerAlertasPorAtender({ cargaDatos: "update", revisarAlertaAsignadaPreviamente: json.revisarAlertaAsignadaPreviamente });
          Alerta.SETINTERVAL_ALERTAS_POR_ATENDER = setInterval(function () { Alerta.obtenerAlertasPorAtender({ cargaDatos: "update", revisarAlertaAsignadaPreviamente: false }); }, Alerta.SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_ATENDER);
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo grabar la gestion de la alerta");
        jQuery("#hBotones").show();
        jQuery("#hMensajeGrabando").hide();
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.grabarFormularioAlerta_TeleOperador:\n" + e);
    }
  },

  limpiarFormulario_TeleOperador: function () {
    try {
      jQuery("#lblAlertaReprogramada").hide();
      jQuery("#txtIdFormato").val("");
      jQuery("#txtIdAlerta").val("");
      jQuery("#txtIdAlertaHija").val("");
      jQuery("#txtIdAlertaGestionAcumulada").val("");
      jQuery("#hAtendiendoAlerta").attr("class", "panel panel-success");
      jQuery("#txtNombreAlerta").val("");
      jQuery("#txtProximoEscalamiento").val("");
      jQuery("#txtLatTracto").val("");
      jQuery("#txtLonTracto").val("");
      jQuery("#txtAlertaMapa").val("");
      jQuery("#txtDestinoLocal").val("");
      jQuery("#txtDestinoLocalCodigo").val("");
      jQuery("#lblTituloNombreAlerta").html("");
      jQuery("#lblIdAlertaHija").html("");
      jQuery("#lblPopoverTitulo").html("");
      jQuery("#lblPopoverPrioridad").html("");
      jQuery("#lblPopoverNroEscalamientoActual").html("");
      jQuery("#lblPopoverNroTransporte").html("");
      jQuery("#lblPopoverIdEmbarque").html("");
      jQuery("#lblPopoverNombreFormato").html("");
      jQuery("#lblPopoverOrigen").html("");
      jQuery("#lblPopoverDestino").html("");
      jQuery("#lblPopoverNombreConductor").html("");
      jQuery("#lblPopoverNombreTransportista").html("");
      jQuery("#lblPopoverPatenteTracto").html("");
      jQuery("#lblPopoverPatenteTrailer").html("");
      jQuery("#lblPopoverVelocidad").html("");
      jQuery("#lblPopoverTipoViaje").html("");
      jQuery("#lblPopoverLatTracto").html("");
      jQuery("#lblPopoverLonTracto").html("");
      jQuery("#lblPopoverFechaHoraCreacion").html("");
      jQuery("#lblPopoverTipoAlerta").html("");
      jQuery("#lblPopoverPermiso").html("");
      jQuery("#lblPermisoZona").html("");
      jQuery("#lblProximoEscalamiento").html("-");
      jQuery("#lblPopoverTipoFlota").html("");
      jQuery("#lblPopoverTelefonoLocal").html("");
      jQuery("#lblPopoverVentanaHoraria").html("");
      jQuery("#lblPopoverHoraCita").html("");
      jQuery("#hPasosEscalamientos").html("");
      jQuery("#hContactosEscalamiento").html("");
      jQuery("#hTablaHistorialEscalamientos").html("<em class=\"help-block\">No hay escalamientos asociados</em>");
      jQuery("#hHistorialEscalamientos").removeClass("mCustomScrollbar sist-height-historial-escalamientos-to");
      jQuery("#hHistorialEscalamientos").addClass("sist-height-historial-escalamientos-sin-datos-to");
      jQuery("#hHistorialEscalamientos").mCustomScrollbar("destroy");
      jQuery("#hBotones").hide();
      jQuery("#hCronometro").html("00:00:00");
      jQuery("#hMensajeGrabando").hide();
      jQuery(".chosen-select").select2("destroy");
      jQuery("#map-canvas").html(Alerta.obtenerHtmlSinMapa());
      jQuery("#hReportabilidadTracto").html("");
      jQuery("#hReportabilidadTrailer").html("");
      Alerta.ARRAY_SCRIPT_GRUPO_CONTACTO = [];
    } catch (e) {
      alert("Exception Alerta.limpiarFormulario_TeleOperador:\n" + e);
    }
  },

  generarJSONGestionAlerta_TeleOperador: function () {
    var jsonAlerta, jsonContactos, item, jsContacto, telefono, nombreUsuario, tipoGrupo;
    var json = "";

    try {
      jsonAlerta = [];
      jsonContactos = [];

      //obtiene el listado de contactos seleccionados
      jQuery(".observacion-contacto-escalamiento").each(function (indice, obj) {
        var elem_txtObservacion = jQuery(obj).attr("id");
        var prefijoControl = elem_txtObservacion.replace("txtObservacion", "");
        var elem_ddlExplicacion = prefijoControl + "ddlExplicacion";
        var elem_ddlContacto = prefijoControl + "ddlContacto";
        var elem_txtAgrupadoEn = prefijoControl + "txtAgrupadoEn";
        var elem_hGrupoContacto = prefijoControl + "hGrupoContacto";

        //---------------------------------
        //busca el telefono del contacto
        telefono = "";
        nombreUsuario = "";
        tipoGrupo = "";
        jsContacto = jQuery("#" + prefijoControl + "txtJSONContacto").val();
        jsContacto = jQuery.parseJSON(jsContacto);
        jsContacto = jQuery.grep(jsContacto, function (item, indice) {
          return (item.IdEscalamientoPorAlertaContacto == jQuery("#" + elem_ddlContacto).val());
        });
        if (jsContacto.length > 0) {
          item = jsContacto[0];
          nombreUsuario = item.NombreUsuario;
          telefono = item.Telefono;
          tipoGrupo = item.TipoGrupo;
        }
        //--------------------------------

        if (jQuery("#" + elem_hGrupoContacto).is(":visible")) {
          item = {
            idEscalamientoPorAlertaContacto: jQuery("#" + elem_ddlContacto).val(),
            idUsuarioSinGrupoContacto: jQuery("#" + elem_ddlContacto).val(),
            explicacion: jQuery("#" + elem_ddlExplicacion).val(),
            observacion: jQuery("#" + elem_txtObservacion).val(),
            agrupadoEn: jQuery("#" + elem_txtAgrupadoEn).val(),
            tipoExplicacion: jQuery("#" + elem_ddlExplicacion + " option:selected").attr("data-tipoExplicacion"),
            idEscalamientoPorAlertaGrupoContacto: prefijoControl.replace("_", ""),
            telefono: telefono,
            nombreUsuario: nombreUsuario,
            tipoGrupo: tipoGrupo
          };
          jsonContactos.push(item);
        }

      });

      item = {
        idAlerta: jQuery("#txtIdAlerta").val(),
        idFormato: jQuery("#txtIdFormato").val(),
        proximoEscalamiento: jQuery("#txtProximoEscalamiento").val(),
        nombreAlerta: jQuery("#txtNombreAlerta").val(),
        totalEscalamientos: jQuery("#txtTotalEscalamientosAlerta").val(),
        explicacion: "",
        explicacionOtro: "",
        observacion: "",
        idAlertaGestionAcumulada: jQuery("#txtIdAlertaGestionAcumulada").val(),
        idAlertaHija: jQuery("#txtIdAlertaHija").val(),
        listadoContactos: jsonContactos
      };
      jsonAlerta.push(item);
      json = Sistema.convertirJSONtoString(jsonAlerta);
    } catch (e) {
      alert("Exception Alerta.generarJSONGestionAlerta_TeleOperador:\n" + e);
    }

    return json;
  },

  obtenerAlertasSinAtender: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerAlertasSinAtender";

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener las alertas sin atender");
        } else {
          var json = jQuery.parseJSON(respuesta);
          jQuery("#hListadoAlertasSinAtender").html(json.listadoColaSinAtencion);

          if (opciones.cargaDatos == "inicial") {
            jQuery("#hAlertasSinAtender").mCustomScrollbar();
          } else {
            jQuery("#hAlertasSinAtender").mCustomScrollbar("update");
          }

          jQuery("#ulAlertasSinAtender").makeSearchList({ elem_input: "txtBuscarAlertaSinAtender", tag_html_filtrar: "li" });
          Alerta.obtenerAlertasSinAtender_MostrarCronometros();
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener las alertas sin atender");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasSinAtender:\n" + e);
    }
  },

  obtenerAlertasSinAtender_MostrarCronometros: function () {
    try {
      //limpia los setInterval reistardo previamente
      jQuery.each(Alerta.ARRAY_SETINTERVAL_CRONOMETRO_ALERTA_SIN_ATENDER, function (indice, valor) {
        clearInterval(valor);
      });
      Alerta.ARRAY_SETINTERVAL_CRONOMETRO_ALERTA_SIN_ATENDER = [];

      //recorre los elementos para asignar el cronometro
      jQuery(".alertas-sin-atender-id").each(function (indice, obj) {
        var elem_txtIdAlerta = jQuery(this).attr("id");
        var idAlerta = elem_txtIdAlerta.replace("_txtIdAlerta", "");
        var elem_lblFecha = idAlerta + "_lblFecha";
        var elem_lblCronometro = idAlerta + "_lblCronometro";

        var fecha = jQuery("#" + elem_lblFecha).html();
        var arrFecha = fecha.split(/[\/ :-]/);
        var opciones_cronometro = {
          year: arrFecha[2],
          month: arrFecha[1],
          day: arrFecha[0],
          hours: arrFecha[3],
          minutes: arrFecha[4],
          seconds: 0,
          elem_cronometro: elem_lblCronometro
        };

        Sistema.cronometro(opciones_cronometro);
        var setIntervalCronometro = setInterval(function () { Sistema.cronometro(opciones_cronometro); }, 1000);
        Alerta.ARRAY_SETINTERVAL_CRONOMETRO_ALERTA_SIN_ATENDER.push(setIntervalCronometro);
      });

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasSinAtender_MostrarCronometros:\n" + e);
    }
  },

  cargarComboExplicacion: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerComboExplicacion";
      queryString += "&prefijoControl=" + opciones.prefijoControl;

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener el combo explicación");
        } else {
          jQuery("#hComboExplicacion").html(respuesta);
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener el combo explicación");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.cargarComboExplicacion:\n" + e);
    }
  },

  verPosicionEnMapa: function (opciones) {
    var latTracto, lonTracto, alertaMapa;
    try {
      switch (opciones.desdeFormulario) {
        case Alerta.FORMULARIO_TO_GESTION_ALERTA:
        case Alerta.FORMULARIO_SUPERVISOR_GESTION_ALERTA:

          if (opciones.sistemaDestino == "TrackPosition") {
            latTracto = jQuery("#txtLatTractoActual").val();
            lonTracto = jQuery("#txtLonTractoActual").val();
          }
          else {
            latTracto = jQuery("#txtLatTracto").val();
            lonTracto = jQuery("#txtLonTracto").val();
            alertaMapa = jQuery("#txtAlertaMapa").val();
          }

          break;
        case Alerta.FORMULARIO_BUSCADOR_ALERTAS:
          alertaMapa = opciones.mapa;
          break;
        default:
          latTracto = "";
          lonTracto = "";
          alertaMapa = "";
          break;
      }

      switch (opciones.sistemaDestino) {
        case "QAnalytics":
          if (alertaMapa != "") {
            Sistema.accederUrl({ location: "blank", url: alertaMapa });
          } else {
            alert("No hay mapa para visualizar");
          }
          break;
        case "TrackPosition":
          Sistema.accederUrl({ location: "blank", url: "http://controlruta.altotrack.com/UltimaPosicion.aspx?ID=" + jQuery("#txtIdAlerta").val() });
          break;
        default:
          if (latTracto != "" && lonTracto != "") {
            Sistema.accederUrl({ location: "blank", url: "https://www.google.cl/maps/place/" + latTracto + "," + lonTracto });
          } else {
            alert("No hay punto georeferencial para visualizar");
          }
          break;
      }

    } catch (e) {
      alert("Exception Alerta.verPosicionEnMapa:\n" + e);
    }
  },

  mostrarDetalleAlerta: function (opciones) {
    var queryString = "";
    var fancy_width, fancy_height, fancy_overlayColor, sinAtender, asignadoA, fechaRecepcion, clasificacion;

    try {
      if (opciones.fancy_width) { fancy_width = opciones.fancy_width } else { fancy_width = "100%" }
      if (opciones.fancy_height) { fancy_height = opciones.fancy_height } else { fancy_height = "100%" }
      if (opciones.fancy_overlayColor) { fancy_overlayColor = opciones.fancy_overlayColor } else { fancy_overlayColor = Sistema.FANCYBOX_OVERLAY_COLOR_DEFECTO }
      if (opciones.sinAtender) { sinAtender = opciones.sinAtender } else { sinAtender = false }
      if (opciones.asignadoA) { asignadoA = opciones.asignadoA } else { asignadoA = "" }
      if (opciones.fechaRecepcion) { fechaRecepcion = opciones.fechaRecepcion } else { fechaRecepcion = "" }
      if (opciones.clasificacion) { clasificacion = opciones.clasificacion } else { clasificacion = "" }

      queryString += "?idAlerta=" + opciones.idAlerta;
      queryString += "&idHistorialEscalamiento=" + opciones.idHistorialEscalamiento;
      queryString += "&sinAtender=" + ((sinAtender) ? "1" : "0");
      queryString += "&asignadoA=" + Sistema.urlEncode(asignadoA);
      queryString += "&fechaRecepcion=" + Sistema.urlEncode(fechaRecepcion);
      queryString += "&clasificacion=" + Sistema.urlEncode(clasificacion);

      jQuery.fancybox({
        width: fancy_width,
        height: fancy_height,
        modal: false,
        type: "iframe",
        helpers: {
          overlay: {
            css: {
              background: fancy_overlayColor
            }
          }
        },
        href: "../page/Common.DetalleAlerta.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Alerta.mostrarDetalleAlerta:\n" + e);
    }

  },

  verAlertasAtendidasHoy: function () {
    try {
      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Common.AlertasAtendidasHoy.aspx"
      });
    } catch (e) {
      alert("Exception Alerta.verAlertasAtendidasHoy:\n" + e);
    }

  },

  desvincular_TeleOperador: function (opciones) {
    var auxiliar, accion;
    try {
      auxiliar = opciones.auxiliar;
      accion = opciones.accion;

      bootbox.dialog({
        closeButton: false,
        message: "<span class=\"sist-font-size-30\">¿Seguro que desea marcar el auxiliar <strong class=\"text-danger\">" + auxiliar + "</strong> y reasignar la alerta?</span>",
        title: "<strong>AUXILIAR</strong>",
        buttons: {
          cancel: {
            label: "Cancelar",
            className: "btn-default btn-lg",
            callback: function () {
              //cierra la ventana modal
            }
          },
          success: {
            label: "Aceptar",
            className: "btn-success btn-lg",
            callback: function () {
              jQuery("#txtAuxiliar").val(auxiliar);
              Alerta.desvincularAlertaUsuario({ accion: accion });
              clearInterval(Alerta.SETINTERVAL_CRONOMETRO);
              clearInterval(Alerta.SETINTERVAL_ALERTAS_POR_ATENDER);
              Alerta.limpiarFormulario_TeleOperador();
            }
          }

        }
      });

    } catch (e) {
      alert("Exception Alerta.soltar_TeleOperador:\n" + e);
    }

  },

  desvincularAlertaUsuario: function (opciones) {
    var queryString = "";
    var idAlerta, idAlertaHija, idAlertaGestionAcumulada;

    try {
      idAlerta = jQuery("#txtIdAlerta").val();
      idAlertaHija = jQuery("#txtIdAlertaHija").val();
      idAlertaGestionAcumulada = jQuery("#txtIdAlertaGestionAcumulada").val();

      //construye queryString
      queryString += "op=DesvincularAlerta";
      queryString += "&idAlerta=" + idAlerta;
      queryString += "&idAlertaHija=" + idAlertaHija;
      queryString += "&idAlertaGestionAcumulada=" + idAlertaGestionAcumulada;
      queryString += "&accion=" + Sistema.urlEncode(opciones.accion);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo desvincular la alerta");
        } else {
          if (Sistema.contains(opciones.accion, "CERRAR_SESION")) {
            Sistema.accederUrl({ location: "top", url: Sistema.PAGINA_LOGOUT });
          } else {
            Alerta.mostrarMensajeEstadoAuxiliar({ fecha: "" });
          }
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo desvincular la alerta");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.desvincularAlertaUsuario:\n" + e);
    }
  },

  validarLogout_TeleOperador: function () {
    var idAlerta;

    try {
      idAlerta = jQuery("#txtIdAlerta").val();

      if (idAlerta == "") {
        Sistema.accederUrl({ location: "top", url: Sistema.PAGINA_LOGOUT });
      } else {
        bootbox.dialog({
          message: "<span class=\"sist-font-size-20\">Tiene una alerta por gestionar y desea finalizar la sesi&oacute;n.<br /><br />¿Seguro que NO desea gestionarla y reasignarla?</span>",
          title: "Finalizar sesi&oacute;n",
          buttons: {
            cancel: {
              label: "Cancelar",
              className: "btn-default btn-lg",
              callback: function () {
                //cierra la ventana modal
              }
            },
            success: {
              label: "Cerrar Sesi&oacute;n",
              className: "btn-success btn-lg",
              callback: function () {
                Alerta.desvincularAlertaUsuario({ accion: 'TELEOPERADOR_CERRAR_SESION' });
              }
            }

          }
        });
      }

    } catch (e) {
      alert("Exception Alerta.validarLogout_TeleOperador:\n" + e);
    }
  },

  mostrarModalNuevaAsignacion: function (opciones) {
    var template, titulo, registroEscalamiento, icono, sonidoAlarma, sonidoLoop;

    try {
      template = "<div class=\"row\">" +
                 "  <div class=\"col-xs-2 text-center sist-padding-top-20\">" +
                 "    <img src=\"../img/{ICONO}\" alt=\"\" />" +
                 "  </div>" +
                 "  <div class=\"col-xs-8\">" +
                 "    {REGISTRO_ESCALAMIENTO}" +
                 "    <div class=\"alert alert-{TIPO_ALERTA} sist-font-size-24\">" +
                 "      <div><strong><em>[IdAlerta: {ID_ALERTA_HIJA}]</em></strong></div>" +
                 "      <div><strong><em>{NOMBRE_ALERTA}</em></strong></div>" +
                 "      <div><strong>IdMaster:</strong> {NRO_TRANSPORTE}</div>" +
                 "      <div><strong>IdEmbarque:</strong> {ID_EMBARQUE}</div>" +
                 "    </div>" +
                 "  </div>" +
                 "</div>";

      if (opciones.revisarAlertaAsignadaPreviamente) {
        titulo = "SIGUIENTE ESCALAMIENTO ...";
        registroEscalamiento = "<div class=\"form-group sist-font-size-24\"><strong>Registro Escalamiento <label class=\"label label-success\"><strong>NRO. " + opciones.proximoEscalamiento + "</strong></label></strong></div>";
        icono = "next.png";
        sonidoAlarma = Alerta.SONIDO_ALARMA_SIGUIENTE_ESCALAMIENTO;
        sonidoLoop = false;
      } else {
        titulo = "NUEVA ALERTA ASIGNADA";
        registroEscalamiento = "";
        icono = "baliza.gif";
        sonidoAlarma = Alerta.SONIDO_ALARMA_NUEVA;
        sonidoLoop = true;
      }

      template = template.replace("{NOMBRE_ALERTA}", opciones.nombreFormato + ' - ' + opciones.nombreAlerta);
      template = template.replace("{NRO_TRANSPORTE}", opciones.nroTransporte);
      template = template.replace("{ID_EMBARQUE}", opciones.idEmbarque);
      template = template.replace("{TIPO_ALERTA}", opciones.tipoAlerta);
      template = template.replace("{REGISTRO_ESCALAMIENTO}", registroEscalamiento);
      template = template.replace("{ICONO}", icono);
      template = template.replace("{ID_ALERTA_HIJA}", jQuery("#txtIdAlertaHija").val());

      ion.sound.play(sonidoAlarma, {
        loop: sonidoLoop
      });

      bootbox.dialog({
        closeButton: false,
        message: template,
        title: "<strong>" + titulo + "</strong>",
        buttons: {
          success: {
            label: "Aceptar",
            className: "btn-success btn-lg",
            callback: function () {
              //cierra la ventana modal
              ion.sound.stop(sonidoAlarma);
            }
          }

        }
      });

    } catch (e) {
      alert("Exception Alerta.mostrarModalNuevaAsignacion:\n" + e);
    }

  },

  mostrarScriptGrupoContacto: function (opciones) {
    try {
      var visible = jQuery("#" + opciones.prefijoControl + "_hScript").is(":visible");
      if (visible) {
        jQuery("#" + opciones.prefijoControl + "_hScript").slideUp("fast");
      } else {
        jQuery("#" + opciones.prefijoControl + "_hScript").slideDown("fast");
      }
    } catch (e) {
      alert("Exception Alerta.mostrarScriptGrupoContacto:\n" + e);
    }
  },

  obtenerAlertasPorGestionar: function (opciones) {
    var queryString = "";

    try {
      jQuery("#lblMensajeCargaAlertasPorGestionar").html(Sistema.mensajeCargandoDatos());
      jQuery("#lblMensajeCargaAlertasPorGestionar").show();

      setTimeout(function () {
        //construye queryString
        queryString += "op=ObtenerAlertasPorGestionar";

        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo obtener las alertas por gestionar");
          } else {
            var json = jQuery.parseJSON(respuesta);
            jQuery("#hListadoAlertasPorGestionar").html(json.listado);
            jQuery("#lblTotalAlertasPorGestionar").html(json.totalRegistros);
            jQuery("#lblMensajeCargaAlertasPorGestionar").html("");
            jQuery("#lblMensajeCargaAlertasPorGestionar").hide();

            jQuery("#TablaAlertasPorGestionar").dataTable({
              "sScrollY": "315px",
              "bScrollCollapse": true
            });

            //Alerta.limpiarFormulario_Supervisor();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo obtener las alertas por gestionar");
        }
        jQuery.ajax({
          url: "../webAjax/waAlerta.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });

      }, 100);

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasPorGestionar:\n" + e);
    }
  },

  cargarDatosFormulario_Supervisor: function (elem_json) {
    var json, item, idAlerta, nombreAlerta, nroTransporte, prioridad, tipoAlerta, cerrado, cerradoSatisfactorio, valorCerrado, permiso, respuestaCategoria, respuestaObservacion;
    var ocurrencias, minutos, html, clasificacionAlertaSist, nombreFormato;

    try {
      Alerta.limpiarFormulario_Supervisor();

      jQuery("#hMensajeCargarAlerta").hide();
      jQuery("#hFormulario").show();
      jQuery(".chosen-select").select2("destroy");

      json = jQuery.parseJSON(elem_json);

      //carga datos de la alerta
      item = json.detalle[0];
      if (item) {
        idAlerta = item.IdAlerta;
        nombreAlerta = item.NombreAlerta;
        nroTransporte = item.NroTransporte;
        prioridad = item.Prioridad;
        cerrado = item.Cerrado;
        cerradoSatisfactorio = item.CerradoSatisfactorio;
        respuestaCategoria = item.RespuestaCategoria;
        respuestaObservacion = item.RespuestaObservacion;
        clasificacionAlertaSist = item.ClasificacionAlertaSist;
        nombreFormato = item.NombreFormato;
        tipoAlerta = Sistema.obtenerStyleAlertaPorPrioridad(prioridad);
        prioridad = Sistema.obtenerLabelPrioridad(prioridad);
        permiso = Sistema.obtenerLabelPermisoZona(item.Permiso);

        jQuery("#txtIdAlerta").val(idAlerta);
        jQuery("#txtIdAlertaGestionAcumulada").val(item.IdAlertaGestionAcumuladaPadre);
        jQuery("#lblTituloNombreAlerta").html(Sistema.toUpper(nombreFormato + " - " + nombreAlerta) + " [IDMASTER " + nroTransporte + "]");
        jQuery("#txtLatTracto").val(item.LatTracto);
        jQuery("#txtLonTracto").val(item.LonTracto);
        jQuery("#txtAlertaMapa").val(item.AlertaMapa);
        jQuery("#lblPopoverTitulo").html(nombreAlerta);
        jQuery("#lblPopoverPrioridad").html(prioridad);
        jQuery("#lblPopoverNroEscalamientoActual").html(item.NroEscalamientoActual);
        jQuery("#lblPopoverNroTransporte").html(nroTransporte);
        jQuery("#lblPopoverNombreFormato").html(item.NombreFormato);
        jQuery("#lblPopoverOrigen").html(item.LocalOrigenDescripcion);
        jQuery("#lblPopoverDestino").html(item.LocalDestinoDescripcion + " (Cód. " + item.LocalDestinoCodigo + ")");
        jQuery("#lblPopoverNombreConductor").html(item.NombreConductor);
        jQuery("#lblPopoverNombreTransportista").html(item.NombreTransportista);
        jQuery("#lblPopoverPatenteTracto").html(item.PatenteTracto);
        jQuery("#lblPopoverPatenteTrailer").html(item.PatenteTrailer);
        jQuery("#lblPopoverVelocidad").html(item.Velocidad);
        jQuery("#lblPopoverTipoViaje").html(item.TipoViaje);
        jQuery("#lblPopoverLatTracto").html(item.LatTracto);
        jQuery("#lblPopoverLonTracto").html(item.LonTracto);
        jQuery("#lblPopoverFechaHoraCreacion").html(item.FechaHoraCreacion);
        jQuery("#lblPopoverTipoAlerta").html(item.TipoAlerta);
        jQuery("#lblPopoverPermiso").html(permiso);
        jQuery("#hDetalleAlerta").attr("class", "panel panel-" + tipoAlerta);
        jQuery("#spanCategoriaRespuesta").html(respuestaCategoria);
        jQuery("#spanObsRespuesta").html(respuestaObservacion);
        jQuery("#hclasificacionAlertaSist").val(clasificacionAlertaSist);

        if (clasificacionAlertaSist == "Roja") {//Alertas Rojas

          minutos = item.Ocurrencias * Alerta.FAC_MULTIPLICADOR_MINUTO_OCURRENCIA;
          ocurrencias = item.Ocurrencias + " Ocurrencias (" + minutos + " minutos Aprox.)";

          //Bloque Alerta Roja
          html = "<div class=\"panel panel-default\">";
          html += "	<div class=\"panel-heading\">";
          html += "		<strong class=\"text-muted\">Alerta Roja</strong>";
          html += "	</div>";
          html += "	<div class=\"panel-body\">";
          html += "		<div class=\"table-responsive\">";
          html += "			<div class=\"form-group\">";
          html += "			  <strong>Tipo de Alerta</strong>";
          html += "			  <div class=\"form-group\">";
          html += "				<small><em class=\"help-block\">" + item.TipoAlerta + "</em></small>";
          html += "			  </div>";
          html += "			</div>";
          html += "			<div class=\"form-group\">";
          html += "			  <strong>Ocurrencias</strong>";
          html += "			  <div class=\"form-group\">";
          html += "				<small><em class=\"help-block\">" + ocurrencias + "</em></small>";
          html += "			  </div>";
          html += "			</div>";
          html += "		</div>";
          html += "	</div>";

          jQuery("#divInfoAlertaRoja").html(html);

          //Deshabilitar radio
          jQuery("input[name=groupCerrado][value=CerradoSatisfactorio]").prop("checked", true);
          jQuery("input[name=groupCerrado][value=CerradoPendiente]").prop("disabled", true);
          jQuery("input[name=groupCerrado][value=CerradoNoSatisfactorio]").prop("disabled", true);

          //Seleccion segun categoria
          jQuery("#ddlCategoriaAlerta").select2("val", item.TipoAlerta);
          Combo.cambiarComboTipoObservacion({ elem_contenedor: 'ddlCategoriaAlerta' });

        } else { //Otras Alertas
          if (cerrado == 0) {
            valorCerrado = "CerradoSatisfactorio";
          } else {
            valorCerrado = (cerrado == 1 && cerradoSatisfactorio == 1) ? "CerradoSatisfactorio" : "CerradoNoSatisfactorio";
          }
          jQuery("input[name=groupCerrado][value=" + valorCerrado + "]").prop("checked", true);
        }

      }

      //carga dll Contactado Escalamiento Anterior
      jQuery("#hComboContactadoEscalamientoAnterior").html(json.comboContactadoEscalamientoAnterior);

      Alerta.dibujarPasosEscalamientos({ elem_json: elem_json, mostrarSuperEscalamiento: true });
      jQuery(".chosen-select").select2();
    } catch (e) {
      alert("Exception Alerta.cargarDatosFormulario_Supervisor:\n" + e);
    }

  },

  validarFormularioAlerta_Supervisor: function () {
    var explicacion, tipoObservacion, requeridoOtro, requeridoOtro2;

    try {
      explicacion = jQuery("#ddlExplicacion").val();
      tipoObservacion = jQuery("#ddlTipoObservacion").val();

      requeridoOtro = (explicacion == "MostrarCampoOtro") ? true : false;
      requeridoOtro2 = (tipoObservacion == "MostrarCampoOtro") ? true : false;

      var jsonValidarCampos = [
      //  { elemento_a_validar: "ddlExplicacion", requerido: true }
      //, { elemento_a_validar: "ddlContactadoEscalamientoAnterior", requerido: true }
      //, { elemento_a_validar: "txtExplicacionOtro", requerido: requeridoOtro, tipo_validacion: "caracteres-prohibidos" }
        {elemento_a_validar: "txtObservacion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        Alerta.grabarFormularioAlerta_Supervisor();
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioAlerta_Supervisor:\n" + e);
    }
  },

  grabarFormularioAlerta_Supervisor: function () {
    var queryString = "";

    try {
      json = Alerta.generarJSONGestionAlerta_Supervisor();

      //construye queryString
      queryString += "op=GrabarGestionAlerta_Supervisor";
      queryString += "&json=" + Sistema.urlEncode(json);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo grabar la gestion de la alerta");
        } else {
          clearInterval(Alerta.SETINTERVAL_ALERTAS_POR_GESTIONAR);
          Alerta.obtenerAlertasPorGestionar();
          Alerta.SETINTERVAL_ALERTAS_POR_GESTIONAR = setInterval(function () { Alerta.obtenerAlertasPorGestionar(); }, Alerta.SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_GESTIONAR);
          Alerta.limpiarFormulario_Supervisor();
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo grabar la gestion de la alerta");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.grabarFormularioAlerta_Supervisor:\n" + e);
    }
  },

  limpiarFormulario_Supervisor: function () {
    try {
      jQuery("#txtIdAlerta").val("");
      jQuery("#txtIdAlertaGestionAcumulada").val("");
      jQuery("#hDetalleAlerta").attr("class", "panel panel-success");
      jQuery("#txtLatTracto").val("");
      jQuery("#txtLonTracto").val("");
      jQuery("#txtAlertaMapa").val("");
      jQuery("#lblTituloNombreAlerta").html("-");
      jQuery("#lblPopoverTitulo").html("");
      jQuery("#lblPopoverPrioridad").html("");
      jQuery("#lblPopoverNroEscalamientoActual").html("");
      jQuery("#lblPopoverNroTransporte").html("");
      jQuery("#lblPopoverNombreFormato").html("");
      jQuery("#lblPopoverOrigen").html("");
      jQuery("#lblPopoverDestino").html("");
      jQuery("#lblPopoverNombreConductor").html("");
      jQuery("#lblPopoverNombreTransportista").html("");
      jQuery("#lblPopoverPatenteTracto").html("");
      jQuery("#lblPopoverPatenteTrailer").html("");
      jQuery("#lblPopoverVelocidad").html("");
      jQuery("#lblPopoverTipoViaje").html("");
      jQuery("#lblPopoverLatTracto").html("");
      jQuery("#lblPopoverLonTracto").html("");
      jQuery("#lblPopoverFechaHoraCreacion").html("");
      jQuery("#lblPopoverTipoAlerta").html("");
      jQuery("#lblPopoverPermiso").html("");
      jQuery("#hPasosEscalamientos").html("");
      jQuery("#txtExplicacionOtro").val("");
      jQuery("#hComboContactadoEscalamientoAnterior").html("<small><em class=\"help-block\">No hay contactos asociados</em></small>");
      jQuery("#txtObservacion").val("");
      jQuery("input[name=groupCerrado][value=CerradoPendiente]").prop("checked", true);
      jQuery("#hMensajeCargarAlerta").show();
      jQuery("#hFormulario").hide();
      jQuery(".chosen-select").select2("destroy");
      Alerta.cargarComboExplicacion({ prefijoControl: "" });
      Alerta.habilitarCampoOtro({ prefijoControl: "", ddlControl: "ddlExplicacion", txtControl: "txtExplicacionOtro" });
      Sistema.destacarFilaTabla({ indice: "-1" });
      jQuery("#divDescTipoObservacion").html("");
      jQuery("#divInfoAlertaRoja").html("");
      jQuery("#ddlCategoriaAlerta").select2("enable", true);

      Combo.dibujarHTML({
        elem_contenedor: "ddlCategoriaAlerta",
        tipoCombo: 'CategoriaAlerta',
        multiple: false,
        placeholder: "",
        item_cabecera: "Seleccione",
        valores_seleccionados: ['-1'],
        onChange: "Combo.cambiarComboTipoObservacion( { elem_contenedor: 'ddlCategoriaAlerta' } );",
        fuenteDatos: "Tabla",
        data: [],
        combos_relacionados: [
                                {
                                  elem_contenedor: "ddlTipoObservacion",
                                  tipoCombo: 'TipoObservacion',
                                  multiple: false,
                                  placeholder: "",
                                  item_cabecera: "Seleccione",
                                  valores_seleccionados: ['-1'],
                                  onChange: "Alerta.mostrarClasificacionTipoObservacion();",
                                  fuenteDatos: "Tabla",
                                  data: [],
                                  filtro: jQuery("#ddlCategoriaAlerta").val()
                                }
                              ]

      });

      Combo.cambiarComboTipoObservacion({ elem_contenedor: 'ddlCategoriaAlerta' });


    } catch (e) {
      alert("Exception Alerta.limpiarFormulario_Supervisor:\n" + e);
    }
  },

  generarJSONGestionAlerta_Supervisor: function () {
    var jsonAlerta, item, cerrado, cerradoSatisfactorio;
    var json = "";

    try {
      jsonAlerta = [];
      cerrado = jQuery("input:radio[name=groupCerrado]:checked").val();

      switch (cerrado) {
        case "CerradoPendiente":
          cerrado = "0";
          cerradoSatisfactorio = "0";
          break;
        case "CerradoSatisfactorio":
          cerrado = "1";
          cerradoSatisfactorio = "1";
          break;
        case "CerradoNoSatisfactorio":
          cerrado = "1";
          cerradoSatisfactorio = "0";
          break;
        default:
          break;
      }

      item = {
        idAlerta: jQuery("#txtIdAlerta").val(),
        idAlertaGestionAcumulada: jQuery("#txtIdAlertaGestionAcumulada").val(),
        //idEscalamientoPorAlertaContacto: jQuery("#ddlContactadoEscalamientoAnterior").val(),
        idEscalamientoPorAlertaContacto: jQuery("#ddlContactadoEscalamientoAnterior optgroup:first option:first").val(),
        //idUsuarioSinGrupoContacto: jQuery("#ddlContactadoEscalamientoAnterior").val(),
        idUsuarioSinGrupoContacto: jQuery("#ddlContactadoEscalamientoAnterior optgroup:first option:first").val(),
        //agrupadoEn: jQuery("#ddlContactadoEscalamientoAnterior option:selected").attr("data-agrupadoEn"),
        agrupadoEn: jQuery("#ddlContactadoEscalamientoAnterior optgroup:first option:first").attr("data-agrupadoEn"),
        tipoGrupo: jQuery("#ddlContactadoEscalamientoAnterior optgroup:first option:first").attr("data-tipoGrupo"),
        explicacion: jQuery("#ddlExplicacion").val(),
        explicacionOtro: jQuery("#txtExplicacionOtro").val(),
        observacion: jQuery("#txtObservacion").val(),
        cerrado: cerrado,
        cerradoSatisfactorio: cerradoSatisfactorio,
        //idEscalamientoPorAlertaGrupoContacto: jQuery("#ddlContactadoEscalamientoAnterior option:selected").attr("data-idEscalamientoPorAlertaGrupoContacto"),
        idEscalamientoPorAlertaGrupoContacto: jQuery("#ddlContactadoEscalamientoAnterior optgroup:first option:first").attr("data-idEscalamientoPorAlertaGrupoContacto"),
        categoriaAlerta: jQuery("#ddlCategoriaAlerta").val(),
        tipoObservacion: jQuery("#ddlTipoObservacion").val(),
        clasificacionAlerta: jQuery("#ddlTipoObservacion option:selected").attr("data-clasificacion"),
        clasificacionAlertaSist: jQuery("#hclasificacionAlertaSist").val()
      };
      jsonAlerta.push(item);
      json = Sistema.convertirJSONtoString(jsonAlerta);
    } catch (e) {
      alert("Exception Alerta.generarJSONGestionAlerta_Supervisor:\n" + e);
    }

    return json;
  },

  cargarComboContactadoEscalamientoAnterior: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerComboContactadoEscalamientoAnterior";
      queryString += "&prefijoControl=" + opciones.prefijoControl;

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener el combo contactado escalamiento anterior");
        } else {
          jQuery("#hComboContactadoEscalamientoAnterior").html(respuesta);
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener el combo contactado escalamiento anterior");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.cargarComboContactadoEscalamientoAnterior:\n" + e);
    }
  },

  cerrarPopUp: function () {
    try {
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Alerta.cerrarPopUp:\n" + e);
    }

  },

  obtenerAlertasAtendidasHoyEnTabla: function () {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=ObtenerAlertasAtendidasHoyEnTabla";

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener las alertas atendidas el día de hoy");
        } else {
          var json = jQuery.parseJSON(respuesta);
          jQuery("#hListadoAlertasAtendidasHoy").html(json.listado);
          jQuery("#tblAlertasAtendidasHoy").dataTable({
            "sScrollY": "315px",
            "bScrollCollapse": true
          });

          if (Sistema.contains(json.listado, "No hay")) {
            jQuery("#btnRecargar").addClass("sist-display-none");
          } else {
            jQuery("#btnRecargar").removeClass("sist-display-none");
          }

        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener las alertas atendidas el día de hoy");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasAtendidasHoyEnTabla:\n" + e);
    }
  },

  mostrarCronometroEnDetalleAlerta: function (opciones) {
    try {
      var fecha = opciones.fechaRecepcion;
      var arrFecha = fecha.split(/[\/ :-]/);
      var opciones_cronometro = {
        year: arrFecha[2],
        month: arrFecha[1],
        day: arrFecha[0],
        hours: arrFecha[3],
        minutes: arrFecha[4],
        seconds: 0,
        elem_cronometro: opciones.elemContenedor
      };

      Sistema.cronometro(opciones_cronometro);
      setInterval(function () { Sistema.cronometro(opciones_cronometro); }, 1000);

    } catch (e) {
      alert("Exception Alerta.mostrarCronometroEnDetalleAlerta:\n" + e);
    }
  },

  validarFormularioBusquedaMantenedor: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioBusquedaMantenedor:\n" + e);
      return false;
    }
  },

  abrirFormularioMantenedor: function (opciones) {
    try {
      var queryString = "";
      var idAlertaConfiguracion = opciones.idAlertaConfiguracion;

      queryString += "?id=" + idAlertaConfiguracion;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.AlertaDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Alerta.abrirFormularioMantenedor:\n" + e);
    }
  },

  cerrarPopUpMantenedor: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Alerta.cargarDesdePopUpMantenedor(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Alerta.cerrarPopUpMantenedor:\n" + e);
    }

  },

  cargarDesdePopUpMantenedor: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Alerta.cargarDesdePopUpMantenedor:\n" + e);
    }
  },

  validarFormularioMantenedor: function () {
    var item, existeNombreAlerta, fechaInicio, fechaInicioAnterior, horaInicio, minutosInicio, idAlertaConfiguracion, horaInicioAnterior;

    try {
      idAlertaConfiguracion = jQuery("#txtIdAlertaConfiguracion").val();
      fechaInicioAnterior = jQuery("#txtFechaInicioAnterior").val();
      horaInicioAnterior = jQuery("#txtHoraInicioAnterior").val();
      fechaInicio = jQuery("#txtFechaInicio").val();
      horaInicio = jQuery("#wucHoraInicio_ddlHora").val();
      minutosInicio = jQuery("#wucHoraInicio_ddlMinutos").val();
      existeNombreAlerta = Alerta.existeNombreAlerta();

      if (horaInicio == "") { horaInicio = "00"; }
      if (minutosInicio == "") { minutosInicio = "00"; }
      horaInicio = horaInicio + ":" + minutosInicio;

      var jsonValidarCampos = [
          { elemento_a_validar: "ddlFormato_ddlCombo", requerido: true }
        , { elemento_a_validar: "ddlNombre", requerido: true }
        , { elemento_a_validar: "ddlPrioridad_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtFrecuenciaRepeticion", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero", contenedor_mensaje_validacion: "hMensajeErrorFrecuenciaRepeticion" }
        , { elemento_a_validar: "ddlTipoAlerta_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtFechaInicio", requerido: false, tipo_validacion: "fecha" }
      ];

      //valida que el nombre de la alerta sea unico
      if (jQuery("#ddlNombre").val() != "") {
        item = {
          valor_a_validar: existeNombreAlerta,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombre",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      //valida el ingreso de la fecha para los registros nuevos y los que no lo han ingresado que no sea menor a la fecha y hora actual
      if (idAlertaConfiguracion == "-1" || fechaInicioAnterior == "") {
        item = { valor_a_validar: fechaInicio, requerido: false, tipo_validacion: "fecha-menor-hoy", contenedor_mensaje_validacion: "lblMensajeErrorHoraInicio", mensaje_validacion: "La fecha no pueden ser menor a la actual", extras: { fecha: fechaInicio, hora: horaInicio} };
        jsonValidarCampos.push(item);
      } else {
        //si la fecha fue registrada con anterioridad, se valida que si se modifica no sea menor a la registrada actualmente
        if (fechaInicioAnterior != "") {
          item = { valor_a_validar: fechaInicioAnterior, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: "lblMensajeErrorHoraInicio", mensaje_validacion: "Fecha no puede ser menor a la registrada", extras: { fechaInicio: fechaInicioAnterior, horaInicio: horaInicioAnterior, fechaTermino: fechaInicio, horaTermino: horaInicio} };
          jsonValidarCampos.push(item);
        }
      }

      //valida los valores ingresados en los escalamientos
      jsonValidarCampos = Alerta.validarFormularioMantenedor_Escalamientos(jsonValidarCampos);

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Alerta.actualizarJSONEscalamientos();
        //habilita controles para poder grabarlos
        jQuery("#ddlFormato_ddlCombo").attr("disabled", false);
        jQuery("#ddlNombre").attr("disabled", false);

        return true;
      }

    } catch (e) {
      alert("Exception: Alerta.validarFormularioMantenedor\n" + e);
      return false;
    }

  },

  existeNombreAlerta: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var idAlertaConfiguracion = jQuery("#txtIdAlertaConfiguracion").val();
      var idFormato = jQuery("#ddlFormato_ddlCombo").val();
      var idAlertaDefinicionPorFormato = jQuery("#ddlNombre").val();
      queryString += "op=Alerta.Nombre";
      queryString += "&id=" + idAlertaConfiguracion;
      queryString += "&valor=" + Sistema.urlEncode(idAlertaDefinicionPorFormato);
      queryString += "&idFormato=" + Sistema.urlEncode(idFormato);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único de la alerta");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único de la alerta");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Alerta.existeNombreAlerta:\n" + e);
    }

    return existe;
  },

  validarEliminarMantenedor: function () {
    try {
      if (confirm("Si elimina la alerta se eliminarán todas las referencias asociadas a ellas y no se podrá volver a recuperar.\n¿Desea eliminar la alerta?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: Alerta.validarEliminarMantenedor\n" + e);
      return false;
    }
  },

  dibujarEscalamientos: function () {
    var json, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, nroEscalamiento;
    var arrGrupoContactos, prefijoControlEscalamiento, prefijoControlGrupo;

    try {
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);

      //dibuja los escalamientos
      jQuery.each(json, function (indice, obj) {
        idEscalamientoPorAlerta = obj.IdEscalamientoPorAlerta;
        nroEscalamiento = obj.Orden;
        Alerta.agregarEscalamiento({ json: obj });

        //------------------------------------------------
        //dibuja los grupos asociados
        jQuery.each(obj.ListadoGrupos, function (indice, obj) {
          prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;
          Alerta.agregarGrupo({ prefijoControl: prefijoControlEscalamiento, json: obj, idEscalamientoPorAlerta: idEscalamientoPorAlerta, nroEscalamiento: nroEscalamiento, nombreGrupo: obj.NombreGrupo, tipo: obj.TipoGrupo });

          //------------------------------------------------
          //dibuja los contactos y script del grupo
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;
          Alerta.dibujarContactosGrupo({ prefijoControl: prefijoControlGrupo, json: obj.ListadoContactos, idEscalamientoPorAlerta: idEscalamientoPorAlerta });
          Alerta.dibujarScriptGrupo({ prefijoControl: prefijoControlGrupo, json: obj.ListadoScript, idEscalamientoPorAlerta: idEscalamientoPorAlerta });

        }); // <- fin dibuja los grupos asociados
      }); // <- fin dibuja los escalamientos

    } catch (e) {
      alert("Exception: Alerta.dibujarEscalamientos\n" + e);
    }
  },

  eliminarRegistroMantenedor: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarAlertaConfiguracion";
      queryString += "&idAlertaConfiguracion=" + Sistema.urlEncode(opciones.idAlertaConfiguracion);

      if (confirm("Si elimina la alerta se eliminarán todas las referencias asociadas a ellas y no se podrá volver a recuperar.\n¿Desea eliminar la alerta?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waAlerta.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Alerta.eliminarRegistroMantenedor\n" + e);
    }
  },

  agregarEscalamiento: function (opciones) {
    var template = "";
    var idUnico, prefijoControl, jsonEscalamiento, emailCopia, nroEscalamiento, item, listadoPerfiles;

    try {
      jsonEscalamiento = opciones.json;
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);
      listadoPerfiles = Alerta.dibujarListadoPerfiles();

      template += "<div id=\"{PREFIJO_CONTROL}\" class=\"panel panel-success sist-margin-bottom-5\">";
      template += "  <div class=\"panel-heading\">";
      template += "    <h3 class=\"panel-title\">";
      template += "      <a data-toggle=\"collapse\" data-parent=\"#hEscalamientos\" href=\"#{PREFIJO_CONTROL}_hCollapse\">";
      template += "        <strong>ESCALAMIENTO NRO. <span id=\"{PREFIJO_CONTROL}_lblNroEscalamiento\">{NRO_ESCALAMIENTO}</span></strong>";
      template += "      </a>";
      template += "      &nbsp;&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-default show-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"elimina el escalamiento y todas sus referencias\" onclick=\"Alerta.eliminarEscalamiento({ elem_contenedor:'{PREFIJO_CONTROL}', idEscalamientoPorAlerta: '{ID_UNICO}' })\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
      template += "    </h3>";
      template += "  </div>";
      template += "  <div id=\"{PREFIJO_CONTROL}_hCollapse\" class=\"panel-collapse collapse\">";
      template += "    <div class=\"panel-body\">";
      template += "      <div class=\"form-group\">";
      template += "        <label class=\"col-sm-2 control-label sist-padding-cero\">Email con copia (CC)</label>";
      template += "        <div class=\"col-sm-10\">";
      template += "          <input type=\"text\" class=\"form-control\" id=\"{PREFIJO_CONTROL}_txtEmailConCopia\" value=\"{EMAIL_COPIA}\" />";
      template += "        </div>";
      template += "      </div>";
      template += "      <div class=\"sist-clear-both sist-padding-top-8\" id=\"{PREFIJO_CONTROL}_hGrupos\"></div>";
      template += "      <!-- boton nuevo grupo -->";
      template += "      <div class=\"btn-group dropup\">";
      template += "        <button type=\"button\" class=\"btn btn-sm btn-info dropdown-toggle\" data-toggle=\"dropdown\">";
      template += "          <span class=\"glyphicon glyphicon-plus\"></span>&nbsp;Agregar nuevo grupo <span class=\"caret\"></span>";
      template += "        </button>";
      template += "        <ul class=\"dropdown-menu\" role=\"menu\">";
      template += listadoPerfiles;
      template += "        </ul>";
      template += "      </div>";
      template += "    </div>";
      template += "  </div>";
      template += "</div>";

      if (jsonEscalamiento) {
        idUnico = jsonEscalamiento.IdEscalamientoPorAlerta;
        nroEscalamiento = jsonEscalamiento.Orden;
        emailCopia = jsonEscalamiento.EmailCopia;
      } else {
        idUnico = Sistema.crearIdUnico() * -1; // crea una id unico negativo para indicar que es nuevo
        nroEscalamiento = json.length + 1;
        emailCopia = "";

        item = {
          IdEscalamientoPorAlerta: idUnico,
          IdAlertaConfiguracion: jQuery("#txtIdAlertaConfiguracion").val(),
          Orden: nroEscalamiento,
          EmailCopia: emailCopia,
          ListadoGrupos: []
        };
        json.push(item);
      }

      prefijoControl = Alerta.PREFIJO_ESCALAMIENTO + idUnico;

      template = template.replace(/{NRO_ESCALAMIENTO}/ig, nroEscalamiento);
      template = template.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
      template = template.replace(/{ID_UNICO}/ig, idUnico);
      template = template.replace("{EMAIL_COPIA}", emailCopia);
      jQuery("#hEscalamientos").append(template);

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));
      jQuery(".show-tooltip").tooltip();

    } catch (e) {
      alert("Exception: Alerta.agregarEscalamiento\n" + e);
    }

  },

  dibujarListadoPerfiles: function () {
    var html = "";
    var template, templateAux, json, tipo;

    try {
      template = "<li><a href=\"javascript:;\" onclick=\"Alerta.agregarGrupo({ prefijoControl: '{PREFIJO_CONTROL}', json: null, idEscalamientoPorAlerta: '{ID_UNICO}', nroEscalamiento: '{NRO_ESCALAMIENTO}', nombreGrupo: '{NOMBRE_PERFIL_MAYUSCULA}', tipo: '{TIPO_PERFIL}' })\">{NOMBRE_PERFIL}</a></li>";
      json = jQuery("#txtJSONPerfilesPorFormato").val();
      json = jQuery.parseJSON(json);

      if (json.length == 0) {
        html += "<li><div class=\"text-center\"><em>Debe seleccionar un formato</em></div></li>";
      } else {
        html += "<li><div class=\"text-center\"><strong><em>-- ESPECIALES --</em></strong></div></li>";

        jQuery.each(json, function (indice, obj) {
          templateAux = template;
          nombrePerfil = obj.NombrePerfil;
          tipo = obj.Tipo;

          templateAux = templateAux.replace("{NOMBRE_PERFIL_MAYUSCULA}", Sistema.toUpper(nombrePerfil));
          templateAux = templateAux.replace("{TIPO_PERFIL}", tipo);
          templateAux = templateAux.replace("{NOMBRE_PERFIL}", nombrePerfil);
          html += templateAux;
        });

        html += "<li class=\"divider\"></li>";

        //agrega grupo personalizado
        templateAux = template;
        templateAux = templateAux.replace("{NOMBRE_PERFIL_MAYUSCULA}", "");
        templateAux = templateAux.replace("{TIPO_PERFIL}", Alerta.TIPO_GRUPO_PERSONALIZADO);
        templateAux = templateAux.replace("{NOMBRE_PERFIL}", Alerta.TIPO_GRUPO_PERSONALIZADO + " ...");
        html += templateAux;
      }

    } catch (e) {
      alert("Exception: Alerta.dibujarListadoPerfiles\n" + e);
      html = "";
    }

    return html;
  },

  agregarGrupo: function (opciones) {
    var templateNormal = "";
    var templateEspecial = "";
    var templateNombreGrupo = "";
    var templateAux, totalEscalamientos, idUnico, prefijoControlPadre, prefijoControl, nombreGrupo, tipo, jsonGrupo, json;
    var ordenGrupo, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, arrEscalamiento;
    var item, jsonListadoGrupos, disabledNombreGrupo, mostrarCuandoEscalamientoAnteriorNoContesta, totalGrupos, nroEscalamiento;
    var tieneDependenciaGrupoEscalamientoAnterior, codigo, idGrupoContactoEscalamientoAnterior, idGrupoContactoPadre, mostrarSiempre, notificarPorEmail;
    var horaAtencionInicio, horaAtencionTermino;

    try {
      jsonGrupo = opciones.json;
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);
      tipo = opciones.tipo;

      templateNombreGrupo += "  <div class=\"form-group\">";
      templateNombreGrupo += "    <div class=\"h5\">";
      templateNombreGrupo += "      <strong><span id=\"{PREFIJO_CONTROL}_lblCodigoGrupo\" class=\"text-danger\">[{CODIGO}]</span>&nbsp;<em>GRUPO CONTACTO</em></strong>";
      if (tipo != Alerta.TIPO_GRUPO_PERSONALIZADO) {
        templateNombreGrupo += "    &nbsp;<span class=\"help-block sist-display-inline sist-margin-cero sist-font-size-14\"><em>(Se mostrarán todos los usuario que pertenezcan al perfil {NOMBRE_GRUPO})</em></span>";
      }
      templateNombreGrupo += "      &nbsp;&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-default show-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"elimina el grupo y todas sus referencias\" onclick=\"Alerta.eliminarGrupo({ elem_contenedor:'{PREFIJO_CONTROL}', idEscalamientoPorAlerta: '{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto: '{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', nroEscalamiento: '{NRO_ESCALAMIENTO}' })\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
      templateNombreGrupo += "    </div>";
      templateNombreGrupo += "    <div class=\"row sist-padding-top-5\">";
      templateNombreGrupo += "      <div class=\"col-xs-3\">";
      templateNombreGrupo += "        <label class=\"control-label\">Nombre</label>";
      templateNombreGrupo += "        <input type=\"text\" id=\"{PREFIJO_CONTROL}_txtNombreGrupo\" class=\"form-control input-sm {PREFIJO_CONTROL_PADRE}-validar-NombreGrupo\" value=\"{NOMBRE_GRUPO}\" {DISABLED_NOMBRE_GRUPO} />";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorNombreGrupo\"></div>";
      templateNombreGrupo += "      </div>";
      templateNombreGrupo += "      <div class=\"col-xs-1\">";
      templateNombreGrupo += "        <label class=\"control-label\">Orden</label>";
      templateNombreGrupo += "        <input type=\"text\" id=\"{PREFIJO_CONTROL}_txtOrdenGrupo\" class=\"form-control input-sm {PREFIJO_CONTROL_PADRE}-validar-OrdenGrupo\" value=\"{ORDEN_GRUPO}\" />";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorOrdenGrupo\"></div>";
      templateNombreGrupo += "      </div>";
      templateNombreGrupo += "      <div class=\"col-xs-2\">";
      templateNombreGrupo += "        <label class=\"control-label\">Hora atenci&oacute;n inicio</label>";
      templateNombreGrupo += "        <input type=\"text\" id=\"{PREFIJO_CONTROL}_txtHoraAtencionInicio\" class=\"form-control input-sm\" value=\"{HORA_ATENCION_INICIO}\" />";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorHoraAtencionInicio\"></div>";
      templateNombreGrupo += "      </div>";
      templateNombreGrupo += "      <div class=\"col-xs-2\">";
      templateNombreGrupo += "        <label class=\"control-label\">Hora atenci&oacute;n t&eacute;rmino</label>";
      templateNombreGrupo += "        <input type=\"text\" id=\"{PREFIJO_CONTROL}_txtHoraAtencionTermino\" class=\"form-control input-sm\" value=\"{HORA_ATENCION_TERMINO}\" />";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorHoraAtencionTermino\"></div>";
      templateNombreGrupo += "      </div>";
      templateNombreGrupo += "      <div class=\"col-xs-2\">";
      templateNombreGrupo += "        <label class=\"control-label\">¿Notificar por email?</label>";
      templateNombreGrupo += "        <div class=\"small\">";
      templateNombreGrupo += "          <span><input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupNotificarPorEmail\" id=\"{PREFIJO_CONTROL}_rbtnNotificarPorEmailSI\" value=\"1\" /><label for=\"{PREFIJO_CONTROL}_rbtnNotificarPorEmailSI\" class=\"sist-font-weight-normal\">&nbsp;SI</label></span>";
      templateNombreGrupo += "          <span class=\"sist-padding-left-10\">";
      templateNombreGrupo += "            <input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupNotificarPorEmail\" id=\"{PREFIJO_CONTROL}_rbtnNotificarPorEmailNO\" value=\"0\" /><label for=\"{PREFIJO_CONTROL}_rbtnNotificarPorEmailNO\" class=\"sist-font-weight-normal\">&nbsp;NO</label>";
      templateNombreGrupo += "          </span>";
      templateNombreGrupo += "        </div>";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorNotificarPorEmail\"></div>";
      templateNombreGrupo += "      </div>";
      templateNombreGrupo += "    </div>";
      templateNombreGrupo += "  </div>";
      templateNombreGrupo += "  <div class=\"form-group\">";
      templateNombreGrupo += "    <div class=\"row\">";
      templateNombreGrupo += "      <div class=\"col-xs-6\">";
      templateNombreGrupo += "        <label class=\"control-label small\">Mantener siempre visible en escalamiento</label>";
      templateNombreGrupo += "        <div class=\"small\">";
      templateNombreGrupo += "          <span><input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupMostrarSiempre\" id=\"{PREFIJO_CONTROL}_rbtnMostrarSiempreSI\" value=\"1\" onclick=\"Alerta.asignarGrupoPadre({ invocadoDesde: Alerta.FORMULARIO_MANTENEDOR_ALERTA_DETALLE, prefijoControl: '{PREFIJO_CONTROL}', idEscalamientoPorAlerta: '{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto: '{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idGrupoContactoPadre: '-1', codigoGrupoPadre: '-1', nombreGrupoPadre: '', nroEscalamiento: '{NRO_ESCALAMIENTO}' }); Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: '{PREFIJO_CONTROL}', groupRadio: 'groupMostrarSiempre', elem_contenedor_boton: 'hBotonAsociarGrupoPadre', elem_boton: 'btnAsociarGrupoPadre' });\" /><label for=\"{PREFIJO_CONTROL}_rbtnMostrarSiempreSI\" class=\"sist-margin-cero sist-font-weight-normal\">&nbsp;SI</label></span>";
      templateNombreGrupo += "        </div>";
      templateNombreGrupo += "        <div class=\"small\">";
      templateNombreGrupo += "          <span>";
      templateNombreGrupo += "            <input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupMostrarSiempre\" id=\"{PREFIJO_CONTROL}_rbtnMostrarSiempreNO\" value=\"0\" onclick=\"Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: '{PREFIJO_CONTROL}', groupRadio: 'groupMostrarSiempre', elem_contenedor_boton: 'hBotonAsociarGrupoPadre', elem_boton: 'btnAsociarGrupoPadre' })\" /><label for=\"{PREFIJO_CONTROL}_rbtnMostrarSiempreNO\" class=\"sist-font-weight-normal\">&nbsp;NO, despu&eacute;s de</label>";
      templateNombreGrupo += "            <span id=\"{PREFIJO_CONTROL}_hBotonAsociarGrupoPadre\" class=\"sist-padding-left-5 sist-display-none\"><button type=\"button\" id=\"{PREFIJO_CONTROL}_btnAsociarGrupoPadre\" class=\"btn btn-sm btn-default\" onclick=\"Alerta.abrirFormularioAsociarGrupo({ prefijoControl:'{PREFIJO_CONTROL}', invocadoDesde: 'AsociarGrupoPadre', idEscalamientoPorAlerta: '{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto: '{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', nroEscalamiento: '{NRO_ESCALAMIENTO}', idGrupoContactoPadre: '{ID_GRUPO_CONTACTO_PADRE}' })\">--Seleccione--</button></span>";
      templateNombreGrupo += "          </span>";
      templateNombreGrupo += "        </div>";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorMostrarSiempre\"></div>";
      templateNombreGrupo += "      </div>";

      //*** NO CONTESTA: QUITAR CLASS "sist-display-none" POR SI SE QUIERE UTILIZAR LOS GRUPOS QUE NO CONTESTAN DEL ESCALAMIENTO ANTERIOR, COMO FUNCIONA EN WALMART (VSR, 08/07/2015)
      templateNombreGrupo += "      <div class=\"col-xs-4 sist-display-none\">";
      templateNombreGrupo += "        <label class=\"control-label small\">Tiene dependencia de grupo escalamiento anterior</label>";
      templateNombreGrupo += "        <div class=\"small\">";
      templateNombreGrupo += "          <span><input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupTieneDependencia\" id=\"{PREFIJO_CONTROL}_rbtnTieneDependenciaNO\" value=\"0\" onclick=\"Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: '{PREFIJO_CONTROL}', groupRadio: 'groupTieneDependencia', elem_contenedor_boton: 'hBotonAsociarTieneDependencia', elem_boton: 'btnAsociarTieneDependencia' })\" /><label for=\"{PREFIJO_CONTROL}_rbtnTieneDependenciaNO\" class=\"sist-font-weight-normal\">&nbsp;NO</label></span>";
      templateNombreGrupo += "        </div>";
      templateNombreGrupo += "        <div class=\"small\">";
      templateNombreGrupo += "          <span>";
      templateNombreGrupo += "            <input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupTieneDependencia\" id=\"{PREFIJO_CONTROL}_rbtnTieneDependenciaSI\" value=\"1\" onclick=\"Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: '{PREFIJO_CONTROL}', groupRadio: 'groupTieneDependencia', elem_contenedor_boton: 'hBotonAsociarTieneDependencia', elem_boton: 'btnAsociarTieneDependencia' })\" /><label for=\"{PREFIJO_CONTROL}_rbtnTieneDependenciaSI\" class=\"sist-font-weight-normal\">&nbsp;SI</label>";
      templateNombreGrupo += "            <span id=\"{PREFIJO_CONTROL}_hBotonAsociarTieneDependencia\" class=\"sist-padding-left-5 sist-display-none\"><button type=\"button\" id=\"{PREFIJO_CONTROL}_btnAsociarTieneDependencia\" class=\"btn btn-sm btn-default\" onclick=\"Alerta.abrirFormularioAsociarGrupo({ prefijoControl:'{PREFIJO_CONTROL}', invocadoDesde: 'AsociarTieneDependencia',  idEscalamientoPorAlerta: '{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto: '{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idGrupoContactoEscalamientoAnterior: '{ID_GRUPO_CONTACTO_ESCALAMIENTO_ANTERIOR}' })\">--Seleccione--</button></span>";
      templateNombreGrupo += "          </span>";
      templateNombreGrupo += "        </div>";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorTieneDependencia\"></div>";
      templateNombreGrupo += "      </div>";
      templateNombreGrupo += "      <div class=\"col-xs-3 sist-display-none\">";
      templateNombreGrupo += "        <label class=\"control-label small\">Mostrar cuando escalamiento anterior No Contesta</label>";
      templateNombreGrupo += "        <div class=\"small\">";
      templateNombreGrupo += "          <span><input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupMostrarNoContesta\" id=\"{PREFIJO_CONTROL}_rbtnMostrarNoContestaNO\" value=\"0\" /><label for=\"{PREFIJO_CONTROL}_rbtnMostrarNoContestaNO\" class=\"sist-font-weight-normal\">&nbsp;NO</label></span>";
      templateNombreGrupo += "          <span class=\"sist-padding-left-10\">";
      templateNombreGrupo += "            <input type=\"radio\" name=\"{PREFIJO_CONTROL}_groupMostrarNoContesta\" id=\"{PREFIJO_CONTROL}_rbtnMostrarNoContestaSI\" value=\"1\" /><label for=\"{PREFIJO_CONTROL}_rbtnMostrarNoContestaSI\" class=\"sist-font-weight-normal\">&nbsp;SI</label>";
      templateNombreGrupo += "          </span>";
      templateNombreGrupo += "        </div>";
      templateNombreGrupo += "        <div id=\"{PREFIJO_CONTROL}_hMensajeErrorMostrarNoContesta\"></div>";
      templateNombreGrupo += "      </div>";
      //-------------------------

      templateNombreGrupo += "    </div>";
      templateNombreGrupo += "  </div>";

      templateNormal += "<div id=\"{PREFIJO_CONTROL}\" class=\"panel panel-default sist-margin-bottom-5\"><div class=\"panel-body\">";
      templateNormal += templateNombreGrupo;
      templateNormal += "  <div>";
      templateNormal += "    <table class=\"table sist-margin-cero\" style=\"border:none;\">";
      templateNormal += "      <tr valign=\"top\">";
      templateNormal += "        <td style=\"width:50%; border-left:none; border-top:none;\">";
      templateNormal += "          <div class=\"h5 text-center\"><strong><em>CONTACTOS</em></strong></div>";
      templateNormal += "          <div class=\"table-responsive small\">";
      templateNormal += "            <div id=\"{PREFIJO_CONTROL}_hContactos\"><div class=\"help-block text-center sist-margin-cero2 sist-font-size-16\"><em>No hay contactos asociados</em></div></div>";
      templateNormal += "          </div>";
      templateNormal += "          <div id=\"{PREFIJO_CONTROL}_hMensajeErrorContactos\"></div>";
      templateNormal += "          <!-- boton nuevo contacto -->";
      templateNormal += "          <div class=\"btn btn-sm btn-success pull-right\" onclick=\"Alerta.abrirFormularioContacto({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idEscalamientoPorAlertaContacto: '-1' })\"><span class=\"glyphicon glyphicon-plus\"></span>&nbsp;Agregar nuevo contacto</div>";
      templateNormal += "        </td>";
      templateNormal += "        <td  style=\"width:50%; border-right:none; border-top:none;\">";
      templateNormal += "          <div class=\"h5 text-center\"><strong><em>SCRIPT</em></strong></div>";
      templateNormal += "          <div class=\"table-responsive small\">";
      templateNormal += "            <div id=\"{PREFIJO_CONTROL}_hScript\"><div class=\"help-block text-center sist-margin-cero2 sist-font-size-16\"><em>No hay script asociados</em></div></div>";
      templateNormal += "          </div>";
      templateNormal += "          <!-- boton nuevo script -->";
      templateNormal += "          <div class=\"btn btn-sm btn-success pull-right\" onclick=\"Alerta.abrirFormularioScript({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idScript: '-1' })\"><span class=\"glyphicon glyphicon-plus\"></span>&nbsp;Agregar nuevo script</div>";
      templateNormal += "        </td>";
      templateNormal += "      </tr>";
      templateNormal += "    </table>";
      templateNormal += "  </div>";
      templateNormal += "</div></div>";

      templateEspecial += "<div id=\"{PREFIJO_CONTROL}\" class=\"panel panel-default sist-margin-bottom-5\"><div class=\"panel-body\">";
      templateEspecial += templateNombreGrupo;
      templateEspecial += "  <div class=\"form-group\">";
      templateEspecial += "    <div class=\"h5 text-center\"><strong><em>SCRIPT</em></strong></div>";
      templateEspecial += "    <div class=\"table-responsive small\">";
      templateEspecial += "      <div id=\"{PREFIJO_CONTROL}_hScript\"><div class=\"help-block text-center sist-margin-cero2 sist-font-size-16\"><em>No hay script asociados</em></div></div>";
      templateEspecial += "    </div>";
      templateEspecial += "    <!-- boton nuevo script -->";
      templateEspecial += "    <div class=\"btn btn-sm btn-success pull-right\" onclick=\"Alerta.abrirFormularioScript({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idScript: '-1' })\"><span class=\"glyphicon glyphicon-plus\"></span>&nbsp;Agregar nuevo script</div>";
      templateEspecial += "  </div>";
      templateEspecial += "</div></div>";

      //obtiene el total de grupos creados y lo incrementa para asignar el codigo
      totalGrupos = parseInt(jQuery("#txtTotalGrupos").val()) + 1;
      jQuery("#txtTotalGrupos").val(totalGrupos);

      if (jsonGrupo) {
        idUnico = jsonGrupo.IdEscalamientoPorAlertaGrupoContacto;
        idEscalamientoPorAlertaGrupoContacto = idUnico;
        idEscalamientoPorAlerta = jsonGrupo.IdEscalamientoPorAlerta;
        nombreGrupo = jsonGrupo.NombreGrupo;
        ordenGrupo = jsonGrupo.OrdenGrupo;
        mostrarCuandoEscalamientoAnteriorNoContesta = jsonGrupo.MostrarCuandoEscalamientoAnteriorNoContesta;
        tieneDependenciaGrupoEscalamientoAnterior = jsonGrupo.TieneDependenciaGrupoEscalamientoAnterior;
        codigo = jsonGrupo.Codigo;
        idGrupoContactoEscalamientoAnterior = jsonGrupo.IdGrupoContactoEscalamientoAnterior;
        mostrarSiempre = jsonGrupo.MostrarSiempre;
        idGrupoContactoPadre = jsonGrupo.IdGrupoContactoPadre;
        notificarPorEmail = jsonGrupo.NotificarPorEmail;
        horaAtencionInicio = Sistema.left(jsonGrupo.HoraAtencionInicio, 2) + ":" + Sistema.right(jsonGrupo.HoraAtencionInicio, 2);
        horaAtencionTermino = Sistema.left(jsonGrupo.HoraAtencionTermino, 2) + ":" + Sistema.right(jsonGrupo.HoraAtencionTermino, 2);
      } else {
        idUnico = Sistema.crearIdUnico() * -1; // crea una id unico negativo para indicar que es nuevo
        idEscalamientoPorAlertaGrupoContacto = idUnico;
        idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
        nombreGrupo = (tipo == Alerta.TIPO_GRUPO_PERSONALIZADO) ? "" : opciones.nombreGrupo;
        mostrarCuandoEscalamientoAnteriorNoContesta = 0;
        tieneDependenciaGrupoEscalamientoAnterior = 0;
        codigo = "GP-" + totalGrupos;
        idGrupoContactoEscalamientoAnterior = -1;
        mostrarSiempre = 1;
        idGrupoContactoPadre = -1;
        notificarPorEmail = 1;
        horaAtencionInicio = "00:00";
        horaAtencionTermino = "23:59";

        //obtiene el escalamiento al que pertenecera el grupo
        arrEscalamiento = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
        });
        jsonListadoGrupos = arrEscalamiento[0].ListadoGrupos;
        ordenGrupo = jsonListadoGrupos.length + 1;

        item = {
          IdEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto,
          IdEscalamientoPorAlerta: idEscalamientoPorAlerta,
          NombreGrupo: nombreGrupo,
          OrdenGrupo: ordenGrupo,
          MostrarCuandoEscalamientoAnteriorNoContesta: mostrarCuandoEscalamientoAnteriorNoContesta,
          TieneDependenciaGrupoEscalamientoAnterior: tieneDependenciaGrupoEscalamientoAnterior,
          IdGrupoContactoEscalamientoAnterior: idGrupoContactoEscalamientoAnterior,
          MostrarSiempre: mostrarSiempre,
          IdGrupoContactoPadre: idGrupoContactoPadre,
          NotificarPorEmail: notificarPorEmail,
          HoraAtencionInicio: horaAtencionInicio,
          HoraAtencionTermino: horaAtencionTermino,
          Codigo: codigo,
          ListadoContactos: [],
          ListadoScript: []
        };
        jsonListadoGrupos.push(item);
      }

      prefijoControlPadre = opciones.prefijoControl;
      prefijoControl = prefijoControlPadre + "_" + Alerta.PREFIJO_GRUPO + idUnico;
      templateAux = (tipo == Alerta.TIPO_GRUPO_PERSONALIZADO) ? templateNormal : templateEspecial;
      disabledNombreGrupo = (tipo == Alerta.TIPO_GRUPO_PERSONALIZADO) ? "" : "disabled";
      nroEscalamiento = opciones.nroEscalamiento;

      //reemplaza marcas especiales
      templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
      templateAux = templateAux.replace(/{PREFIJO_CONTROL_PADRE}/ig, prefijoControlPadre);
      templateAux = templateAux.replace(/{ID_UNICO}/ig, idUnico);
      templateAux = templateAux.replace(/{NOMBRE_GRUPO}/ig, nombreGrupo);
      templateAux = templateAux.replace(/{ORDEN_GRUPO}/ig, ordenGrupo);
      templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
      templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
      templateAux = templateAux.replace(/{DISABLED_NOMBRE_GRUPO}/ig, disabledNombreGrupo);
      templateAux = templateAux.replace(/{CODIGO}/ig, codigo);
      templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_ESCALAMIENTO_ANTERIOR}/ig, idGrupoContactoEscalamientoAnterior);
      templateAux = templateAux.replace(/{NRO_ESCALAMIENTO}/ig, nroEscalamiento);
      templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_PADRE}/ig, idGrupoContactoPadre);
      templateAux = templateAux.replace(/{HORA_ATENCION_INICIO}/ig, horaAtencionInicio);
      templateAux = templateAux.replace(/{HORA_ATENCION_TERMINO}/ig, horaAtencionTermino);
      jQuery("#" + prefijoControlPadre + "_hGrupos").append(templateAux);

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));
      jQuery(".show-tooltip").tooltip();

      //asigna valor al radio button "... NoContesta"
      setTimeout(function () {
        jQuery("input[name=" + prefijoControl + "_groupMostrarNoContesta][value=" + mostrarCuandoEscalamientoAnteriorNoContesta + "]").prop("checked", true);
      }, 100);

      //asigna valor al radio button "...TieneDependencia"
      setTimeout(function () {
        jQuery("input[name=" + prefijoControl + "_groupTieneDependencia][value=" + tieneDependenciaGrupoEscalamientoAnterior + "]").prop("checked", true);
        Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: prefijoControl, groupRadio: "groupTieneDependencia", elem_contenedor_boton: "hBotonAsociarTieneDependencia", elem_boton: "btnAsociarTieneDependencia" });
      }, 100);

      //asigna valor al radio button "... MostrarSiempre"
      setTimeout(function () {
        jQuery("input[name=" + prefijoControl + "_groupMostrarSiempre][value=" + mostrarSiempre + "]").prop("checked", true);
        Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: prefijoControl, groupRadio: "groupMostrarSiempre", elem_contenedor_boton: "hBotonAsociarGrupoPadre", elem_boton: "btnAsociarGrupoPadre" });
      }, 100);

      //asigna valor al radio button "... NotificarPorEmail"
      setTimeout(function () {
        jQuery("input[name=" + prefijoControl + "_groupNotificarPorEmail][value=" + notificarPorEmail + "]").prop("checked", true);
      }, 100);

      //asigna mascara hora inicio - hora termino
      setTimeout(function () {
        jQuery("#" + prefijoControl + "_txtHoraAtencionInicio").mask("00:00", { placeholder: "__:__" });
        jQuery("#" + prefijoControl + "_txtHoraAtencionTermino").mask("00:00", { placeholder: "__:__" });
      }, 100);

      //cambia el texto del boton "..._btnAsociarTieneDependencia"
      Alerta.obtenerDatosGrupoContactoAsignado({ prefijoControl: prefijoControl, idEscalamientoPorAlertaGrupoContacto: idGrupoContactoEscalamientoAnterior, elem_boton: "btnAsociarTieneDependencia" });

      //cambia el texto del boton "..._btnAsociarGrupoPadre"
      Alerta.obtenerDatosGrupoContactoAsignado({ prefijoControl: prefijoControl, idEscalamientoPorAlertaGrupoContacto: idGrupoContactoPadre, elem_boton: "btnAsociarGrupoPadre" });

      //actualizacion jsonPadreHijo
      Alerta.actualizarJSONGrupoPadreHijo({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre, nroEscalamiento: nroEscalamiento });

      //mueve scroll al final de la pagina cuando se crea un grupo nuevo
      if (!jsonGrupo) {
        Sistema.moverScrollHastaControl(prefijoControl);
      }

    } catch (e) {
      alert("Exception: Alerta.agregarGrupo\n" + e);
    }

  },

  dibujarScriptGrupo: function (opciones) {
    var html = "";
    var template = "";
    var templateAux, nombreScript, activo, json, totalRegistros, idEscalamientoPorAlertaGrupoContacto, idScript, descripcion;

    try {
      json = opciones.json;
      if (json) {
        totalRegistros = json.length;
      } else {
        totalRegistros = 0;
      }

      template += "<tr>";
      template += "  <td>";
      template += "    <div class=\"sist-cursor-pointer show-popover\" data-toggle=\"popover\" data-trigger=\"hover\" data-placement=\"top\" title=\"{POPOVER_TITULO}\" data-content=\"{POPOVER_DESCRIPCION}\">{NOMBRE_SCRIPT}</div>";
      template += "    <div id=\"{PREFIJO_CONTROL}_hMensajeErrorNombreScript{ID_UNICO}\"></div>";
      template += "  </td>";
      template += "  <td>{ACTIVO}</td>";
      template += "  <td>";
      template += "    <button type=\"button\" class=\"btn btn-default show-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"edita datos del script\" onclick=\"Alerta.abrirFormularioScript({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idScript: '{ID_SCRIPT}' })\"><span class=\"glyphicon glyphicon-pencil\"></span></button>&nbsp;";
      template += "    <button type=\"button\" class=\"btn btn-default show-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"elimina script y todas sus referencias\" onclick=\"Alerta.eliminarScript({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idScript: '{ID_SCRIPT}', invocadoDesde: 'Formulario' })\"><span class=\"glyphicon glyphicon-trash\"></span></button>&nbsp;";
      template += "  </td>";
      template += "</tr>";

      if (totalRegistros == 0) {
        html = "<div class=\"help-block text-center sist-margin-cero2 sist-font-size-16\"><em>No hay script asociados</em></div>";
      } else {
        html += "<table class=\"table table-striped table-condensed sist-margin-bottom-5\">";
        html += "  <thead>";
        html += "    <tr>";
        html += "      <th>T&iacute;tulo</th>";
        html += "      <th>Activo</th>";
        html += "      <th style=\"width:120px\">Opciones</th>";
        html += "      </tr>";
        html += "  </thead>";
        html += "  <tbody>";

        jQuery.each(json, function (indice, obj) {
          templateAux = template;
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          idScript = obj.IdScript;
          nombreScript = obj.Nombre;
          descripcion = obj.Descripcion;
          activo = (obj.Activo == 1) ? "SI" : "NO";

          //formatea valores
          descripcion = "<strong><em>DESCRIPCI&Oacute;N</em></strong><br>" + descripcion;

          //reemplaza marcas
          templateAux = templateAux.replace("{NOMBRE_SCRIPT}", nombreScript);
          templateAux = templateAux.replace("{ACTIVO}", activo);
          templateAux = templateAux.replace("{POPOVER_TITULO}", nombreScript);
          templateAux = templateAux.replace("{POPOVER_DESCRIPCION}", descripcion);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, opciones.idEscalamientoPorAlerta);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
          templateAux = templateAux.replace(/{ID_SCRIPT}/ig, idScript);
          templateAux = templateAux.replace(/{ID_UNICO}/ig, idScript);
          templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, opciones.prefijoControl);
          html += templateAux;
        });

        html += "  </tbody>";
        html += "</table>";
      }

      jQuery("#" + opciones.prefijoControl + "_hScript").html(html);
      jQuery(".show-popover").popover({ html: true });
      jQuery(".show-tooltip").tooltip();
    } catch (e) {
      alert("Exception: Alerta.dibujarScriptGrupo\n" + e);
    }

  },

  dibujarContactosGrupo: function (opciones) {
    var html = "";
    var template = "";
    var templateAux, nombreContacto, cargo, ordenContacto, json, totalRegistros, idUsuario;

    try {
      json = opciones.json;
      if (json) {
        totalRegistros = json.length;
      } else {
        totalRegistros = 0;
      }

      template += "<tr>";
      template += "  <td>";
      template += "    <div>{NOMBRE_CONTACTO}</div>";
      template += "    <input id=\"{PREFIJO_CONTROL}_txtIdUsuario{ID_UNICO}\" type=\"hidden\" class=\"{PREFIJO_CONTROL}-validar-IdUsuario\" value=\"{ID_USUARIO}\" />";
      template += "    <div id=\"{PREFIJO_CONTROL}_hMensajeErrorIdUsuario{ID_UNICO}\"></div>";
      template += "  </td>";
      template += "  <td>{CARGO}</td>";
      template += "  <td>";
      template += "    <input id=\"{PREFIJO_CONTROL}_txtOrdenContacto{ID_UNICO}\" type=\"text\" class=\"form-control input-sm {PREFIJO_CONTROL}-validar-OrdenContacto\" value=\"{ORDEN_CONTACTO}\" />";
      template += "    <div id=\"{PREFIJO_CONTROL}_hMensajeErrorOrdenContacto{ID_UNICO}\"></div>";
      template += "  </td>";
      template += "  <td>";
      template += "    <button type=\"button\" class=\"btn btn-default show-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"edita datos del contacto\" onclick=\"Alerta.abrirFormularioContacto({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idEscalamientoPorAlertaContacto: '{ID_ESCALAMIENTO_ALERTA_CONTACTO}' })\"><span class=\"glyphicon glyphicon-pencil\"></span></button>&nbsp;";
      template += "    <button type=\"button\" class=\"btn btn-default show-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"elimina contacto y todas sus referencias\" onclick=\"Alerta.eliminarContacto({ idEscalamientoPorAlerta:'{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto:'{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idEscalamientoPorAlertaContacto: '{ID_ESCALAMIENTO_ALERTA_CONTACTO}', invocadoDesde: 'Formulario' })\"><span class=\"glyphicon glyphicon-trash\"></span></button>&nbsp;";
      template += "  </td>";
      template += "</tr>";

      if (totalRegistros == 0) {
        html = "<div class=\"help-block text-center sist-margin-cero2 sist-font-size-16\"><em>No hay contactos asociados</em></div>";
      } else {
        html += "<table class=\"table table-striped table-condensed sist-margin-bottom-5\">";
        html += "  <thead>";
        html += "    <tr>";
        html += "      <th>Contacto</th>";
        html += "      <th>Cargo</th>";
        html += "      <th style=\"width:100px\">Orden</th>";
        html += "      <th style=\"width:120px\">Opciones</th>";
        html += "      </tr>";
        html += "  </thead>";
        html += "  <tbody>";

        jQuery.each(json, function (indice, obj) {
          templateAux = template;
          idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          idEscalamientoPorAlertaContacto = obj.IdEscalamientoPorAlertaContacto;
          nombreContacto = obj.NombreContacto;
          cargo = obj.Cargo;
          ordenContacto = obj.OrdenContacto;
          idUsuario = obj.IdUsuario;

          //reemplaza marcasa
          templateAux = templateAux.replace("{NOMBRE_CONTACTO}", nombreContacto);
          templateAux = templateAux.replace("{CARGO}", cargo);
          templateAux = templateAux.replace("{ORDEN_CONTACTO}", ordenContacto);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_CONTACTO}/ig, idEscalamientoPorAlertaContacto);
          templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, opciones.prefijoControl);
          templateAux = templateAux.replace(/{ID_UNICO}/ig, idEscalamientoPorAlertaContacto);
          templateAux = templateAux.replace(/{ID_USUARIO}/ig, idUsuario);
          html += templateAux;
        });

        html += "  </tbody>";
        html += "</table>";
      }

      jQuery("#" + opciones.prefijoControl + "_hContactos").html(html);
      jQuery(".show-tooltip").tooltip();
    } catch (e) {
      alert("Exception: Alerta.dibujaContactosGrupo\n" + e);
    }

  },

  abrirFormularioScript: function (opciones) {
    var queryString = "";

    try {
      queryString += "?idScript=" + opciones.idScript;
      queryString += "&idEscalamientoPorAlerta=" + opciones.idEscalamientoPorAlerta;
      queryString += "&idEscalamientoPorAlertaGrupoContacto=" + opciones.idEscalamientoPorAlertaGrupoContacto;

      jQuery.fancybox({
        width: "80%",
        height: "85%",
        modal: false,
        type: "iframe",
        helpers: {
          overlay: {
            css: {
              background: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL
            }
          }
        },
        href: "../page/Mantenedor.ScriptDetalle.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Alerta.abrirFormularioScript:\n" + e);
    }

  },

  cargarDatosScriptPopUp: function (opciones) {
    var arrEscalamiento, arrListadoGrupos, arrListadoScript, jsonDetalleScript, idScript;
    var nombre, descripcion, activo;

    try {
      idScript = opciones.idScript;
      json = jQuery("#txtJSONEscalamientos", window.parent.document).val();
      json = jQuery.parseJSON(json);

      //------------------------------------------------
      //obtiene datos del script consultado

      if (idScript == "-1") {
        nombre = "";
        descripcion = "";
        activo = 1;
      } else {
        arrEscalamiento = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta);
        });
        arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
          return (item.IdEscalamientoPorAlertaGrupoContacto == opciones.idEscalamientoPorAlertaGrupoContacto);
        });
        arrListadoScript = jQuery.grep(arrListadoGrupos[0].ListadoScript, function (item, indice) {
          return (item.IdScript == idScript);
        });
        jsonDetalleScript = arrListadoScript[0];

        //obtiene valores
        nombre = jsonDetalleScript.Nombre;
        descripcion = jsonDetalleScript.Descripcion;
        activo = jsonDetalleScript.Activo;
      }

      //carga los controles
      jQuery("#lblTituloFormulario").html(((idScript == "-1") ? "Nuevo " : "Modificar ") + "Script");
      jQuery("#txtNombre").val(nombre);
      jQuery("#txtDescripcion").val(descripcion);
      jQuery("#chkActivo").attr("checked", (activo == 1) ? true : false);
    } catch (e) {
      alert("Exception Alerta.cargarDatosScriptPopUp:\n" + e);
    }

  },

  validarFormularioScript: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtDescripcion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        Alerta.grabarDatosScriptEnJSON();
      }

    } catch (e) {
      alert("Exception: Alerta.validarFormularioScript\n" + e);
    }

  },

  grabarDatosScriptEnJSON: function () {
    var idScript, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, nombre, descripcion;
    var item, jsonListadoScript, prefijoControlEscalamiento, arrListadoScript, jsonDetalleScript;

    try {
      //obtiene valores
      idScript = jQuery("#txtIdScript").val();
      idEscalamientoPorAlerta = jQuery("#txtIdEscalamientoPorAlerta").val();
      idEscalamientoPorAlertaGrupoContacto = jQuery("#txtIdEscalamientoPorAlertaGrupoContacto").val();
      nombre = jQuery("#txtNombre").val();
      descripcion = jQuery("#txtDescripcion").val();
      activo = (jQuery("#chkActivo").is(":checked")) ? 1 : 0;

      //obtiene el json donde debe realizar la modificacion
      json = jQuery("#txtJSONEscalamientos", window.parent.document).val();
      json = jQuery.parseJSON(json);

      arrEscalamiento = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
        return (item.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto);
      });
      jsonListadoScript = arrListadoGrupos[0].ListadoScript;

      //------------------------------------------------
      //si el registro es nuevo entonces lo agrega al json ListadoScript, sino lo actualiza en jsonDetalleScript
      if (idScript == "-1") {
        item = {
          IdScript: Sistema.crearIdUnico() * -1, // crea una id unico negativo para indicar que es nuevo
          IdEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto,
          Nombre: nombre,
          Descripcion: descripcion,
          Activo: activo
        }
        jsonListadoScript.push(item);
      } else {
        arrListadoScript = jQuery.grep(arrListadoGrupos[0].ListadoScript, function (item, indice) {
          return (item.IdScript == idScript);
        });
        jsonDetalleScript = arrListadoScript[0];
        jsonDetalleScript.Nombre = nombre;
        jsonDetalleScript.Descripcion = descripcion;
        jsonDetalleScript.Activo = activo;
      }

      //asigna actualizacion del json en campo oculto
      json = Sistema.convertirJSONtoString(json);
      jQuery("#txtJSONEscalamientos", window.parent.document).val(json);

      //dibuja los script del grupo
      prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;
      prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;
      parent.Alerta.dibujarScriptGrupo({ prefijoControl: prefijoControlGrupo, json: jsonListadoScript, idEscalamientoPorAlerta: idEscalamientoPorAlerta });

      //cierra popUp
      Alerta.cerrarPopUp();

    } catch (e) {
      alert("Exception: Alerta.grabarDatosScriptEnJSON\n" + e);
    }
  },

  abrirFormularioContacto: function (opciones) {
    var queryString = "";

    try {
      queryString += "?idEscalamientoPorAlertaContacto=" + opciones.idEscalamientoPorAlertaContacto;
      queryString += "&idEscalamientoPorAlerta=" + opciones.idEscalamientoPorAlerta;
      queryString += "&idEscalamientoPorAlertaGrupoContacto=" + opciones.idEscalamientoPorAlertaGrupoContacto;

      jQuery.fancybox({
        width: "60%",
        height: "70%",
        modal: false,
        type: "iframe",
        helpers: {
          overlay: {
            css: {
              background: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL
            }
          }
        },
        href: "../page/Mantenedor.ContactoDetalle.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Alerta.abrirFormularioContacto:\n" + e);
    }

  },

  asignarCargoContacto: function () {
    var texto, valor, cargo, arrNombre, idEscalamientoPorAlertaContacto;
    try {
      valor = jQuery("#ddlContacto_ddlCombo").val();
      idEscalamientoPorAlertaContacto = jQuery("#txtIdEscalamientoPorAlertaContacto").val();

      if (Validacion.tipoVacio(valor)) {
        cargo = "";
      } else {
        texto = jQuery("#ddlContacto_ddlCombo option:selected").text();
        cargo = Alerta.obtenerCampoArrayContacto({ texto: texto, campoExtraer: "Cargo" });
      }

      //si el registro es nuevo entonces asigna el perfil por defecto
      if (idEscalamientoPorAlertaContacto == "-1") {
        jQuery("#txtCargo").val(cargo);
      } else {
        if (Validacion.tipoVacio(jQuery("#txtCargo").val())) {
          jQuery("#txtCargo").val(cargo);
        }
      }

    } catch (e) {
      alert("Exception Alerta.asignarCargoContacto:\n" + e);
    }

  },

  obtenerCampoArrayContacto: function (opciones) {
    var valor, nombre, cargo;

    try {
      arrNombre = opciones.texto.split("[");

      //obtiene Nombre
      nombre = arrNombre[0];
      nombre = jQuery.trim(nombre);

      //obtiene Cargo
      cargo = arrNombre[1];
      cargo = cargo.replace("]", "");
      cargo = jQuery.trim(cargo);

      switch (opciones.campoExtraer) {
        case "Cargo":
          valor = cargo;
          break
        case "Nombre":
          valor = nombre;
          break;
        default:
          valor = "";
          break;
      }
    } catch (e) {
      alert("Exception Alerta.obtenerCampoArrayContacto:\n" + e);
      valor = "";
    }

    return valor;
  },

  cargarDatosContactoPopUp: function (opciones) {
    var arrEscalamiento, arrListadoGrupos, arrListadoContactos, jsonDetalleContacto, idEscalamientoPorAlertaContacto;
    var idUsuario, cargo;

    try {
      idEscalamientoPorAlertaContacto = opciones.idEscalamientoPorAlertaContacto;
      json = jQuery("#txtJSONEscalamientos", window.parent.document).val();
      json = jQuery.parseJSON(json);

      //------------------------------------------------
      //obtiene datos del contacto consultado
      if (idEscalamientoPorAlertaContacto == "-1") {
        idUsuario = "";
        cargo = "";
      } else {
        arrEscalamiento = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta);
        });
        arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
          return (item.IdEscalamientoPorAlertaGrupoContacto == opciones.idEscalamientoPorAlertaGrupoContacto);
        });
        arrListadoContactos = jQuery.grep(arrListadoGrupos[0].ListadoContactos, function (item, indice) {
          return (item.IdEscalamientoPorAlertaContacto == idEscalamientoPorAlertaContacto);
        });
        jsonDetalleContacto = arrListadoContactos[0];

        //obtiene valores
        idUsuario = jsonDetalleContacto.IdUsuario;
        cargo = jsonDetalleContacto.Cargo;
      }

      //carga los controles
      jQuery("#lblTituloFormulario").html(((idEscalamientoPorAlertaContacto == "-1") ? "Nuevo " : "Modificar ") + "Contacto");
      jQuery("#ddlContacto_ddlCombo").val(idUsuario);
      jQuery("#txtCargo").val(cargo);
    } catch (e) {
      alert("Exception Alerta.cargarDatosContactoPopUp:\n" + e);
    }

  },

  validarFormularioContacto: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: "ddlContacto_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtCargo", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        Alerta.grabarDatosContactoEnJSON();
      }

    } catch (e) {
      alert("Exception: Alerta.validarFormularioContacto\n" + e);
    }

  },

  grabarDatosContactoEnJSON: function () {
    var idEscalamientoPorAlertaContacto, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idUsuario, nombreContacto, cargo;
    var item, jsonListadoContactos, arrListadoContactos, prefijoControlEscalamiento, texto, jsonDetalleContacto;

    try {
      //obtiene valores
      idEscalamientoPorAlertaContacto = jQuery("#txtIdEscalamientoPorAlertaContacto").val();
      idEscalamientoPorAlerta = jQuery("#txtIdEscalamientoPorAlerta").val();
      idEscalamientoPorAlertaGrupoContacto = jQuery("#txtIdEscalamientoPorAlertaGrupoContacto").val();
      idUsuario = jQuery("#ddlContacto_ddlCombo").val();
      cargo = jQuery("#txtCargo").val();
      texto = jQuery("#ddlContacto_ddlCombo option:selected").text();
      nombreContacto = Alerta.obtenerCampoArrayContacto({ texto: texto, campoExtraer: "Nombre" });

      //obtiene el json donde debe realizar la modificacion
      json = jQuery("#txtJSONEscalamientos", window.parent.document).val();
      json = jQuery.parseJSON(json);

      arrEscalamiento = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
        return (item.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto);
      });
      jsonListadoContactos = arrListadoGrupos[0].ListadoContactos;

      //------------------------------------------------
      //si el registro es nuevo entonces lo agrega al json ListadoContactos, sino lo actualiza en jsonDetalleContacto
      if (idEscalamientoPorAlertaContacto == "-1") {
        item = {
          IdEscalamientoPorAlertaContacto: Sistema.crearIdUnico() * -1, // crea una id unico negativo para indicar que es nuevo
          IdEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto,
          IdUsuario: idUsuario,
          NombreContacto: nombreContacto,
          Cargo: cargo,
          OrdenContacto: jsonListadoContactos.length + 1
        };
        jsonListadoContactos.push(item);
      } else {
        arrListadoContactos = jQuery.grep(arrListadoGrupos[0].ListadoContactos, function (item, indice) {
          return (item.IdEscalamientoPorAlertaContacto == idEscalamientoPorAlertaContacto);
        });
        jsonDetalleContacto = arrListadoContactos[0];
        jsonDetalleContacto.IdUsuario = idUsuario;
        jsonDetalleContacto.NombreContacto = nombreContacto;
        jsonDetalleContacto.Cargo = cargo;
      }

      //asigna actualizacion del json en campo oculto
      json = Sistema.convertirJSONtoString(json);
      jQuery("#txtJSONEscalamientos", window.parent.document).val(json);

      //dibuja los script del grupo
      prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;
      prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;
      parent.Alerta.dibujarContactosGrupo({ prefijoControl: prefijoControlGrupo, json: jsonListadoContactos, idEscalamientoPorAlerta: idEscalamientoPorAlerta });

      //cierra popUp
      Alerta.cerrarPopUp();

    } catch (e) {
      alert("Exception: Alerta.grabarDatosContactoEnJSON\n" + e);
    }
  },

  eliminarEscalamiento: function (opciones) {
    var json, prefijoControlEscalamiento;

    try {
      if (confirm("Si elimina el escalamiento se borrarán todas sus referencias y no se podrá volver a recuperar.\n¿Desea eliminar el escalamiento?")) {
        json = jQuery("#txtJSONEscalamientos").val();
        json = jQuery.parseJSON(json);

        //elimina escalamiento
        jQuery.each(json, function (indice, obj) {
          if (obj.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta) {
            json.splice(indice, 1);
            return false;
          }
        });

        //elimina el elemento contenedor
        jQuery("#" + opciones.elem_contenedor).remove();

        //reasigna el orden
        jQuery.each(json, function (indice, obj) {
          obj.Orden = indice + 1;
          prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + obj.IdEscalamientoPorAlerta;
          jQuery("#" + prefijoControlEscalamiento + "_lblNroEscalamiento").html(obj.Orden);
        });

        //asigna actualizacion del json en campo oculto
        jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));

        //-------------------------------------------------------------------------------------------
        //desasigna todos los grupos que lo tenian como "PADRE"
        Alerta.actualizarJSONGrupoPadreHijo_EliminarGrupo({ idEscalamientoPorAlerta: opciones.idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: "-1", nroEscalamiento: "-1" });
      }

    } catch (e) {
      alert("Exception: Alerta.eliminarEscalamiento\n" + e);
    }

  },

  eliminarGrupo: function (opciones) {
    var json, prefijoControlEscalamiento, prefijoControlGrupo, arrEscalamiento, jsonListadoGrupos;

    try {
      if (confirm("Si elimina el grupo se borrarán todas sus referencias y no se podrá volver a recuperar.\n¿Desea eliminar el grupo?")) {
        json = jQuery("#txtJSONEscalamientos").val();
        json = jQuery.parseJSON(json);

        arrEscalamiento = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta);
        });

        //elimina grupo
        jsonListadoGrupos = arrEscalamiento[0].ListadoGrupos;
        jQuery.each(jsonListadoGrupos, function (indice, obj) {
          if (obj.IdEscalamientoPorAlertaGrupoContacto == opciones.idEscalamientoPorAlertaGrupoContacto) {
            jsonListadoGrupos.splice(indice, 1);
            return false;
          }
        });

        //elimina el elemento contenedor
        jQuery("#" + opciones.elem_contenedor).remove();

        //reasigna el orden
        jQuery.each(jsonListadoGrupos, function (indice, obj) {
          obj.OrdenGrupo = indice + 1;
          prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + obj.IdEscalamientoPorAlerta;
          prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + obj.IdEscalamientoPorAlertaGrupoContacto;
          jQuery("#" + prefijoControlGrupo + "_txtOrdenGrupo").val(obj.OrdenGrupo);
        });

        //-------------------------------------------------------------------------------------------
        //desasigna todos los grupos que lo tenian como dependencia "Grupo Escalamiento Anterior"
        jQuery.each(json, function (indice, obj) {
          jQuery.each(obj.ListadoGrupos, function (indice, obj) {
            if (obj.IdGrupoContactoEscalamientoAnterior == opciones.idEscalamientoPorAlertaGrupoContacto) {
              obj.TieneDependenciaGrupoEscalamientoAnterior = 0;
              obj.IdGrupoContactoEscalamientoAnterior = -1;
              prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + obj.IdEscalamientoPorAlerta;
              prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + obj.IdEscalamientoPorAlertaGrupoContacto;

              //marca el radio button = NO
              jQuery("input[name=" + prefijoControlGrupo + "_groupTieneDependencia][value=0]").prop("checked", true);
              Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: prefijoControlGrupo, groupRadio: "groupTieneDependencia", elem_contenedor_boton: "hBotonAsociarTieneDependencia", elem_boton: "btnAsociarTieneDependencia" });
            }
          });
        });
        //asigna actualizacion del json en campo oculto
        jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));

        //-------------------------------------------------------------------------------------------
        //desasigna todos los grupos que lo tenian como "PADRE"
        Alerta.actualizarJSONGrupoPadreHijo_EliminarGrupo({ idEscalamientoPorAlerta: opciones.idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: opciones.idEscalamientoPorAlertaGrupoContacto, nroEscalamiento: opciones.nroEscalamiento });

      }

    } catch (e) {
      alert("Exception: Alerta.eliminarGrupo\n" + e);
    }

  },

  eliminarContacto: function (opciones) {
    var json, prefijoControlEscalamiento, prefijoControlGrupo, arrEscalamiento, arrListadoGrupos, jsonListadoContactos;

    try {
      if (confirm("Si elimina el contacto se borrarán todas sus referencias y no se podrá volver a recuperar.\n¿Desea eliminar el contacto?")) {
        json = jQuery("#txtJSONEscalamientos").val();
        json = jQuery.parseJSON(json);

        arrEscalamiento = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta);
        });
        arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
          return (item.IdEscalamientoPorAlertaGrupoContacto == opciones.idEscalamientoPorAlertaGrupoContacto);
        });

        //elimina contacto
        jsonListadoContactos = arrListadoGrupos[0].ListadoContactos;
        jQuery.each(jsonListadoContactos, function (indice, obj) {
          if (obj.IdEscalamientoPorAlertaContacto == opciones.idEscalamientoPorAlertaContacto) {
            jsonListadoContactos.splice(indice, 1);
            return false;
          }
        });

        //reasigna el orden
        jQuery.each(jsonListadoContactos, function (indice, obj) {
          obj.OrdenContacto = indice + 1;
        });

        //dibuja los contactos
        prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + opciones.idEscalamientoPorAlerta;
        prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + opciones.idEscalamientoPorAlertaGrupoContacto;
        Alerta.dibujarContactosGrupo({ prefijoControl: prefijoControlGrupo, json: jsonListadoContactos, idEscalamientoPorAlerta: opciones.idEscalamientoPorAlerta });

        //asigna actualizacion del json en campo oculto
        jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));

        if (opciones.invocadoDesde == "PopUp") {
          jQuery.fancybox.close();
        }
      }

    } catch (e) {
      alert("Exception: Alerta.eliminarContacto\n" + e);
    }

  },

  eliminarScript: function (opciones) {
    var json, prefijoControlEscalamiento, prefijoControlGrupo, arrEscalamiento, arrListadoScript, jsonListadoScript;

    try {
      if (confirm("Si elimina el script se borrarán todas sus referencias y no se podrá volver a recuperar.\n¿Desea eliminar el script?")) {
        json = jQuery("#txtJSONEscalamientos").val();
        json = jQuery.parseJSON(json);

        arrEscalamiento = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta);
        });
        arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
          return (item.IdEscalamientoPorAlertaGrupoContacto == opciones.idEscalamientoPorAlertaGrupoContacto);
        });

        //elimina script
        jsonListadoScript = arrListadoGrupos[0].ListadoScript;
        jQuery.each(jsonListadoScript, function (indice, obj) {
          if (obj.IdScript == opciones.idScript) {
            jsonListadoScript.splice(indice, 1);
            return false;
          }
        });

        //dibuja los script
        prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + opciones.idEscalamientoPorAlerta;
        prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + opciones.idEscalamientoPorAlertaGrupoContacto;
        Alerta.dibujarScriptGrupo({ prefijoControl: prefijoControlGrupo, json: jsonListadoScript, idEscalamientoPorAlerta: opciones.idEscalamientoPorAlerta });

        //asigna actualizacion del json en campo oculto
        jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));

        if (opciones.invocadoDesde == "PopUp") {
          jQuery.fancybox.close();
        }
      }

    } catch (e) {
      alert("Exception: Alerta.eliminarScript\n" + e);
    }

  },

  actualizarJSONEscalamientos: function () {
    var json, prefijoControlEscalamiento, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, prefijoControlGrupo;
    var idEscalamientoPorAlertaContacto;

    try {
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);

      //--------------------------------------------------
      //recorre los escalamientos
      jQuery.each(json, function (indice, obj) {
        idEscalamientoPorAlerta = obj.IdEscalamientoPorAlerta;
        prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;

        /**
        * actualiza campos ESCALAMIENTO */
        obj.EmailCopia = jQuery("#" + prefijoControlEscalamiento + "_txtEmailConCopia").val();

        //--------------------------------------------------
        //recorre los grupos
        jQuery.each(obj.ListadoGrupos, function (indice, obj) {
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;

          /**
          * actualiza campos GRUPO */
          obj.NombreGrupo = jQuery("#" + prefijoControlGrupo + "_txtNombreGrupo").val();
          obj.OrdenGrupo = jQuery("#" + prefijoControlGrupo + "_txtOrdenGrupo").val();
          obj.MostrarCuandoEscalamientoAnteriorNoContesta = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupMostrarNoContesta]:checked").val();
          obj.TieneDependenciaGrupoEscalamientoAnterior = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupTieneDependencia]:checked").val();
          obj.MostrarSiempre = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupMostrarSiempre]:checked").val();
          obj.NotificarPorEmail = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupNotificarPorEmail]:checked").val();
          obj.HoraAtencionInicio = jQuery("#" + prefijoControlGrupo + "_txtHoraAtencionInicio").val();
          obj.HoraAtencionTermino = jQuery("#" + prefijoControlGrupo + "_txtHoraAtencionTermino").val();

          //--------------------------------------------------
          //recorre los contactos
          jQuery.each(obj.ListadoContactos, function (indice, obj) {
            idEscalamientoPorAlertaContacto = obj.IdEscalamientoPorAlertaContacto;

            /**
            * actualiza campos CONTACTO */
            obj.OrdenContacto = jQuery("#" + prefijoControlGrupo + "_txtOrdenContacto" + idEscalamientoPorAlertaContacto).val();

          }); //<- fin recorre los contactos
        }); //<!- fin recorre los grupos
      }); //<- fin recorre los escalamientos

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));
    } catch (e) {
      alert("Exception: Alerta.eliminarScript\n" + e);
    }

  },

  validarFormularioMantenedor_Escalamientos: function (jsonValidarCampos) {
    var json, prefijoControlEscalamiento, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, prefijoControlGrupo;
    var idEscalamientoPorAlertaContacto, item, mostrarNoContesta, tieneDependencia, valorBotonTieneDependencia, mostrarSiempre, valorBotonAsociarGrupoPadre;
    var listadoContactos, notificarPorEmail;

    try {
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);

      //------------------------------------------------------------
      //incluye las validaciones tradicionales por campo
      //recorre los escalamientos
      jQuery.each(json, function (indice, obj) {
        idEscalamientoPorAlerta = obj.IdEscalamientoPorAlerta;
        prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;

        /**
        * valida EmailCopia */
        item = { elemento_a_validar: prefijoControlEscalamiento + "_txtEmailConCopia", requerido: false, tipo_validacion: "email-listado" };
        jsonValidarCampos.push(item);

        //--------------------------------------------------
        //recorre los grupos
        jQuery.each(obj.ListadoGrupos, function (indice, obj) {
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;

          /**
          * valida NombreGrupo */
          item = { elemento_a_validar: prefijoControlGrupo + "_txtNombreGrupo", requerido: true, tipo_validacion: "caracteres-prohibidos" };
          jsonValidarCampos.push(item);

          /**
          * valida OrdenGrupo */
          item = { elemento_a_validar: prefijoControlGrupo + "_txtOrdenGrupo", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" };
          jsonValidarCampos.push(item);

          /**
          * valida MostrarNoContesta */
          mostrarNoContesta = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupMostrarNoContesta]:checked").val();
          if (!mostrarNoContesta) { mostrarNoContesta = -1; }
          item = { valor_a_validar: mostrarNoContesta, requerido: true, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: prefijoControlGrupo + "_hMensajeErrorMostrarNoContesta", mensaje_validacion: "Este campo es requerido" };
          jsonValidarCampos.push(item);

          /**
          * valida TieneDependencia */
          tieneDependencia = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupTieneDependencia]:checked").val();
          if (tieneDependencia == 1) {
            valorBotonTieneDependencia = jQuery("#" + prefijoControlGrupo + "_btnAsociarTieneDependencia").html();
            if (valorBotonTieneDependencia == "--Seleccione--") {
              tieneDependencia = -1; //indica que el valor no es valido
            }
          }
          item = { valor_a_validar: tieneDependencia, requerido: true, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: prefijoControlGrupo + "_hMensajeErrorTieneDependencia", mensaje_validacion: "Debe asignar grupo de escalamiento anterior" };
          jsonValidarCampos.push(item);

          /**
          * valida MostrarSiempre */
          mostrarSiempre = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupMostrarSiempre]:checked").val();
          if (mostrarSiempre == 0) {
            valorBotonAsociarGrupoPadre = jQuery("#" + prefijoControlGrupo + "_btnAsociarGrupoPadre").html();
            if (valorBotonAsociarGrupoPadre == "--Seleccione--") {
              mostrarSiempre = -1; //indica que el valor no es valido
            }
          }
          item = { valor_a_validar: mostrarSiempre, requerido: true, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: prefijoControlGrupo + "_hMensajeErrorMostrarSiempre", mensaje_validacion: "Debe asignar grupo de quien precede" };
          jsonValidarCampos.push(item);

          /**
          * valida NotificarPorEmail */
          notificarPorEmail = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupNotificarPorEmail]:checked").val();
          if (!notificarPorEmail) { notificarPorEmail = -1; }
          item = { valor_a_validar: notificarPorEmail, requerido: true, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: prefijoControlGrupo + "_hMensajeErrorNotificarPorEmail", mensaje_validacion: "Este campo es requerido" };
          jsonValidarCampos.push(item);

          /**
          * valida HoraAtencionInicio */
          item = { elemento_a_validar: prefijoControlGrupo + "_txtHoraAtencionInicio", requerido: true, tipo_validacion: "hora" };
          jsonValidarCampos.push(item);

          /**
          * valida HoraAtencionTermino */
          item = { elemento_a_validar: prefijoControlGrupo + "_txtHoraAtencionTermino", requerido: true, tipo_validacion: "hora" };
          jsonValidarCampos.push(item);

          /**
          * valida Listado Contactos */
          listadoContactos = jQuery("#" + prefijoControlGrupo + "_hContactos").html();
          if (listadoContactos) {
            listadoContactos = (Sistema.contains(listadoContactos, "No hay contactos asociados")) ? -1 : 1; //-1: indica que el valor no es valido, 1: es valido
            item = { valor_a_validar: listadoContactos, requerido: true, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: prefijoControlGrupo + "_hMensajeErrorContactos", mensaje_validacion: "Debe ingresar un contactos al menos" };
            jsonValidarCampos.push(item);
          }

          //--------------------------------------------------
          //recorre los contactos
          jQuery.each(obj.ListadoContactos, function (indice, obj) {
            idEscalamientoPorAlertaContacto = obj.IdEscalamientoPorAlertaContacto;

            /**
            * valida OrdenContacto */
            item = { elemento_a_validar: prefijoControlGrupo + "_txtOrdenContacto" + idEscalamientoPorAlertaContacto, requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" };
            jsonValidarCampos.push(item);

          }); //<- fin recorre los contactos
        }); //<!- fin recorre los grupos
      }); //<- fin recorre los escalamientos

      //------------------------------------------------------------
      //realiza validaciones especiales por bloques de informacion
      //jsonValidarCampos = Alerta.validarFormularioMantenedor_Escalamientos_Grupos(json, jsonValidarCampos);
      jsonValidarCampos = Alerta.validarFormularioMantenedor_Escalamientos_GrupoContactos(json, jsonValidarCampos);

    } catch (e) {
      alert("Exception: Alerta.validarFormularioMantenedor_Escalamientos\n" + e);
    }

    return jsonValidarCampos;
  },

  validarFormularioMantenedor_Escalamientos_Grupos: function (json, jsonValidarCampos) {
    var item, prefijoControlEscalamiento, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, prefijoControlGrupo;
    var idEscalamientoPorAlertaContacto, totalEncontrados, arrCamposPorRevisar, valor, valorComparar, hMensajeError;
    var mensajeValidacion, objElemento;

    try {
      //------------------------------------------------------------
      //recorre los escalamientos
      jQuery.each(json, function (indice, obj) {
        idEscalamientoPorAlerta = obj.IdEscalamientoPorAlerta;
        prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;

        //--------------------------------------------------
        //recorre los grupos
        jQuery.each(obj.ListadoGrupos, function (indice, obj) {
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;

          //------------------------------------------------------------
          //verifica que por columna no existan valores repetidos
          arrCamposPorRevisar = ["NombreGrupo", "OrdenGrupo"];

          //recorre por columna
          jQuery.each(arrCamposPorRevisar, function (indice, campo) {
            //recorre por fila verificando si ese valor existe en las demas filas
            jQuery("." + prefijoControlEscalamiento + "-validar-" + campo).each(function (indice, obj) {
              objElemento = jQuery(this).attr("id");
              valor = jQuery(this).val();
              hMensajeError = objElemento.replace("txt" + campo, "hMensajeError" + campo);
              totalEncontrados = 0;
              valorValidar = 1; // se inicializa que el valor no esta repetido (1: No repetido | -1: repetido)

              //verifica por cada fila si el valor existe (se incluye la misma fila que esta verificando, por eso el totalEncontrados debe ser mayor a 1)
              jQuery("." + prefijoControlEscalamiento + "-validar-" + campo).each(function (indice, obj) {
                valorComparar = jQuery(this).val();
                if (Sistema.toUpper(valor) == Sistema.toUpper(valorComparar)) {
                  totalEncontrados++;
                  if (totalEncontrados > 1) {
                    valorValidar = -1;
                    return false;
                  }
                }
              }); //<-  fin: verifica por cada fila si el valor existe (se incluye la misma fila que esta verificando, por eso el totalEncontrados debe ser mayor a 1)

              //asigna la validacion por control
              var item = {
                valor_a_validar: valorValidar,
                requerido: false,
                tipo_validacion: "numero-entero-positivo",
                contenedor_mensaje_validacion: hMensajeError,
                mensaje_validacion: "El valor est&aacute; repetido"
              };
              jsonValidarCampos.push(item);

            }); // <-- fin: recorre por fila verificando si ese valor existe en las demas filas
          }); // <- fin: recorre por columna
        }); //<!- fin recorre los grupos
      }); //<- fin recorre los escalamientos

    } catch (e) {
      alert("Exception: Alerta.validarFormularioMantenedor_Escalamientos_Grupos\n" + e);
    }

    return jsonValidarCampos;
  },

  validarFormularioMantenedor_Escalamientos_GrupoContactos: function (json, jsonValidarCampos) {
    var item, prefijoControlEscalamiento, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, prefijoControlGrupo;
    var idEscalamientoPorAlertaContacto, totalEncontrados, arrCamposPorRevisar, valor, valorComparar, hMensajeError;
    var mensajeValidacion, objElemento;

    try {
      //------------------------------------------------------------
      //recorre los escalamientos
      jQuery.each(json, function (indice, obj) {
        idEscalamientoPorAlerta = obj.IdEscalamientoPorAlerta;
        prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;

        //--------------------------------------------------
        //recorre los grupos
        jQuery.each(obj.ListadoGrupos, function (indice, obj) {
          idEscalamientoPorAlertaGrupoContacto = obj.IdEscalamientoPorAlertaGrupoContacto;
          prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;

          //--------------------------------------------------
          //recorre los contactos
          jQuery.each(obj.ListadoContactos, function (indice, obj) {
            idEscalamientoPorAlertaContacto = obj.IdEscalamientoPorAlertaContacto;

            //------------------------------------------------------------
            //verifica que por columna no existan valores repetidos
            arrCamposPorRevisar = ["IdUsuario", "OrdenContacto"];

            //recorre por columna
            jQuery.each(arrCamposPorRevisar, function (indice, campo) {
              //recorre por fila verificando si ese valor existe en las demas filas
              jQuery("." + prefijoControlGrupo + "-validar-" + campo).each(function (indice, obj) {
                objElemento = jQuery(this).attr("id");
                valor = jQuery(this).val();
                hMensajeError = objElemento.replace("txt" + campo, "hMensajeError" + campo);
                totalEncontrados = 0;
                valorValidar = 1; // se inicializa que el valor no esta repetido (1: No repetido | -1: repetido)

                //verifica por cada fila si el valor existe (se incluye la misma fila que esta verificando, por eso el totalEncontrados debe ser mayor a 1)
                jQuery("." + prefijoControlGrupo + "-validar-" + campo).each(function (indice, obj) {
                  valorComparar = jQuery(this).val();
                  if (Sistema.toUpper(valor) == Sistema.toUpper(valorComparar)) {
                    totalEncontrados++;
                    if (totalEncontrados > 1) {
                      valorValidar = -1;
                      return false;
                    }
                  }
                }); //<-  fin: verifica por cada fila si el valor existe (se incluye la misma fila que esta verificando, por eso el totalEncontrados debe ser mayor a 1)

                //asigna la validacion por control
                var item = {
                  valor_a_validar: valorValidar,
                  requerido: false,
                  tipo_validacion: "numero-entero-positivo",
                  contenedor_mensaje_validacion: hMensajeError,
                  mensaje_validacion: "Valor repetido"
                };
                jsonValidarCampos.push(item);

              }); // <-- fin: recorre por fila verificando si ese valor existe en las demas filas
            }); // <- fin: recorre por columna
          }); //<- fin recorre los contactos
        }); //<!- fin recorre los grupos
      }); //<- fin recorre los escalamientos

    } catch (e) {
      alert("Exception: Alerta.validarFormularioMantenedor_Escalamientos_GrupoContactos\n" + e);
    }

    return jsonValidarCampos;
  },

  verHistorialEscalamientosPopUp: function () {
    try {
      var idAlerta = jQuery("#txtIdAlerta").val();
      Alerta.mostrarDetalleAlerta({ idAlerta: idAlerta, idHistorialEscalamiento: -1, fancy_width: "100%", fancy_height: "100%" });
    } catch (e) {
      alert("Exception: Alerta.verHistorialEscalamientosPopUp\n" + e);
    }

  },

  obtenerAlertasSinAtenderEnTabla: function () {
    var queryString = "";

    try {
      jQuery("#hListadoAlertasSinAtender").html(Sistema.mensajeCargandoDatos());

      setTimeout(function () {
        //construye queryString
        queryString += "op=ObtenerAlertasSinAtenderEnTabla";

        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo obtener las alertas sin atender");
          } else {
            var json = jQuery.parseJSON(respuesta);
            jQuery("#hListadoAlertasSinAtender").html(json.listado);
            jQuery("#lblTotalAlertasSinAtender").html(json.totalRegistros);

            Alerta.obtenerAlertasSinAtender_MostrarCronometros();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo obtener las alertas sin atender");
        }
        jQuery.ajax({
          url: "../webAjax/waAlerta.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }, 100);

      setTimeout(function () {
        jQuery("#tblAlertasSinAtender").dataTable({
          "sScrollY": "315px",
          "bScrollCollapse": true
        });
      }, 100);

    } catch (e) {
      alert("Exception Alerta.obtenerAlertasSinAtenderEnTabla:\n" + e);
    }
  },

  recargarDatosTabGestionAlerta_Supervisor: function (opciones) {
    try {
      setTimeout(function () {
        switch (opciones.tab) {
          case "AlertasPorGestionar":
            clearInterval(Alerta.SETINTERVAL_ALERTAS_POR_GESTIONAR);
            Alerta.obtenerAlertasPorGestionar();
            Alerta.SETINTERVAL_ALERTAS_POR_GESTIONAR = setInterval(function () { Alerta.obtenerAlertasPorGestionar(); }, Alerta.SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_GESTIONAR);
            break;
          case "AlertasSinAtender":
            Alerta.obtenerAlertasSinAtenderEnTabla();
            break;
        }

      }, 100);
    } catch (e) {
      alert("Exception Alerta.setearTablaAlertasSinAtender:\n" + e);
    }

  },

  verEnGoogleMaps: function (opciones) {
    var htmlSinRegistro;

    try {
      jQuery("#hMapaCanvas").show();

      //muestra el mapa si vienen las coordenadas
      if (opciones.latTracto != "" && opciones.lonTracto != "" && opciones.alertaMapa != "") {
        var myLatlng = new google.maps.LatLng(opciones.latTracto, opciones.lonTracto);
        var mapOptions = {
          zoom: 13,
          center: myLatlng
        };
        map = new google.maps.Map(document.getElementById(opciones.elem_contenedor_mapa), mapOptions);

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          icon: "../img/camion.png"
        });

        google.maps.event.addListener(marker, "click", function () {
          Alerta.verPosicionEnMapa({ desdeFormulario: opciones.desdeFormulario, sistemaDestino: "QAnalytics" })
        });

      } else {
        htmlSinRegistro = Alerta.obtenerHtmlSinMapa();
        jQuery("#" + opciones.elem_contenedor_mapa).html(htmlSinRegistro);

      }
    } catch (e) {
      alert("Exception Alerta.verEnGoogleMaps:\n" + e);
    }
  },

  agregarContactoModoManual: function (opciones) {
    var queryString = "";

    try {
      queryString += "?prefijoControl=" + Sistema.urlEncode(opciones.prefijoControl);
      queryString += "&agrupadoEn=" + Sistema.urlEncode(opciones.agrupadoEn);
      queryString += "&localDestinoCodigo=" + Sistema.urlEncode(opciones.localDestinoCodigo);
      queryString += "&localDestinoNombre=" + Sistema.urlEncode(opciones.localDestinoNombre);
      queryString += "&localOrigenCodigo=" + Sistema.urlEncode(opciones.localOrigenCodigo);
      queryString += "&localOrigenNombre=" + Sistema.urlEncode(opciones.localOrigenNombre);
      queryString += "&nombreConductor=" + Sistema.urlEncode(opciones.nombreConductor);
      queryString += "&rutConductor=" + Sistema.urlEncode(opciones.rutConductor);
      queryString += "&nombreTransportista=" + Sistema.urlEncode(opciones.nombreTransportista);
      queryString += "&rutTransportista=" + Sistema.urlEncode(opciones.rutTransportista);
      queryString += "&llavePerfil=" + Sistema.urlEncode(opciones.llavePerfil);
      queryString += "&tipoGrupo=" + Sistema.urlEncode(opciones.tipoGrupo);
      queryString += "&idFormato=" + Sistema.urlEncode(opciones.idFormato);

      jQuery.fancybox({
        width: "70%",
        height: 350,
        modal: false,
        type: "iframe",
        href: "../page/TO.AgregarContacto.aspx" + queryString,
        afterClose: function () {
          //al cerrar el popUp selecciona en el combo el valor ingresado
          var idEscalamientoPorAlertaContacto = jQuery("#txtIdRegistroNuevoEnCombo").val();
          if (idEscalamientoPorAlertaContacto != "") {
            jQuery("#" + opciones.prefijoControl + "_ddlContacto").select2("val", idEscalamientoPorAlertaContacto);
            Alerta.mostrarDatosContacto({ prefijoControl: opciones.prefijoControl });
          }
          jQuery("#txtIdRegistroNuevoEnCombo").val("");
        }
      });

    } catch (e) {
      alert("Exception Alerta.agregarContactoModoManual:\n" + e);
    }
  },

  validarFormularioAgregarContacto: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtPaterno", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtMaterno", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtTelefono", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtEmail", requerido: false, tipo_validacion: "email" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception Alerta.validarFormularioAgregarContacto:\n" + e);
      return false;
    }

  },

  asignarContactoEnCombo: function (opciones) {
    var jsonGrupoContacto, jsonContacto, prefijoControl;

    try {
      prefijoControl = opciones.prefijoControl;
      jsonContacto = opciones.jsonContacto;

      //obtiene json del grupo y añade el nuevo contacto
      jsonGrupoContacto = jQuery("#" + prefijoControl + "_txtJSONContacto", window.parent.document).val();
      jsonGrupoContacto = jQuery.parseJSON(jsonGrupoContacto);
      jsonGrupoContacto.push(jsonContacto);
      jQuery("#" + prefijoControl + "_txtJSONContacto", window.parent.document).val(Sistema.convertirJSONtoString(jsonGrupoContacto));

      //agrega el nuevo contacto al combo
      idEscalamientoPorAlertaContacto = jsonContacto.IdEscalamientoPorAlertaContacto;
      nombreUsuario = jsonContacto.NombreUsuario;
      jQuery("#" + prefijoControl + "_ddlContacto", window.parent.document).append("<option value=\"" + idEscalamientoPorAlertaContacto + "\" data-frecuencia=\"-1\" data-tipoExplicacion=\"Normal\">" + nombreUsuario + "</option>");

      //asigna nuevo id para dejar seleccionado el combo una vez cerrado el popUp
      jQuery("#txtIdRegistroNuevoEnCombo", window.parent.document).val(idEscalamientoPorAlertaContacto);

      //cierra la ventana
      Alerta.cerrarPopUp();
    } catch (e) {
      alert("Exception Alerta.asignarContactoEnCombo:\n" + e);
    }

  },

  agregarExplicacionModoManual: function (opciones) {
    var queryString = "";

    try {
      queryString += "?prefijoControl=" + Sistema.urlEncode(opciones.prefijoControl);

      jQuery.fancybox({
        width: "50%",
        height: 260,
        modal: false,
        type: "iframe",
        href: "../page/TO.AgregarExplicacion.aspx" + queryString,
        afterClose: function () {
          //al cerrar el popUp selecciona en el combo el valor ingresado
          var nombre = jQuery("#txtIdRegistroNuevoEnCombo").val();
          if (nombre != "") {
            jQuery("#" + opciones.prefijoControl + "_ddlExplicacion").select2("val", nombre);
          }
          jQuery("#txtIdRegistroNuevoEnCombo").val("");
        }
      });

    } catch (e) {
      alert("Exception Alerta.agregarExplicacionModoManual:\n" + e);
    }
  },

  validarFormularioAgregarExplicacion: function () {
    try {
      var jsonValidarCampos = [
        { elemento_a_validar: "txtExplicacion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception Alerta.validarFormularioAgregarExplicacion:\n" + e);
      return false;
    }

  },

  asignarExplicacionEnCombo: function (opciones) {
    var prefijoControl, nombre;

    try {
      //agrega el nuevo registro al combo
      nombre = opciones.Nombre;
      prefijoControl = opciones.PrefijoControl;

      //recorre todos los combos para agregar el nuevo item
      jQuery("select[rel=combo-explicacion]", window.parent.document).each(function (indice, obj) {
        jQuery(this).append("<option value=\"" + nombre + "\">" + nombre + "</option>");
      });

      //asigna nuevo id para dejar seleccionado el combo una vez cerrado el popUp
      jQuery("#txtIdRegistroNuevoEnCombo", window.parent.document).val(nombre);

      //cierra la ventana
      Alerta.cerrarPopUp();
    } catch (e) {
      alert("Exception Alerta.asignarExplicacionEnCombo:\n" + e);
    }

  },

  obtenerHtmlSinMapa: function () {
    var htmlSinRegistro = "";

    try {
      htmlSinRegistro += "<div class=\"text-center\">";
      htmlSinRegistro += "  <div class=\"sist-padding-top-20\"></div>";
      htmlSinRegistro += "  <div class=\"form-group\"><img src=\"../img/sin_mapa.png\" alt=\"\" /></div>";
      htmlSinRegistro += "  <div class=\"help-block h3\"><em>No hay mapa para visualizar</em></div>";
      htmlSinRegistro += "</div>";
    } catch (e) {
      alert("Exception Alerta.obtenerHtmlSinMapa:\n" + e);
    }

    return htmlSinRegistro;
  },

  abrirFormularioAsociarGrupo: function (opciones) {
    var queryString = "";
    var json, prefijoControl, invocadoDesde, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoEscalamientoAnterior;
    var nroEscalamiento, idGrupoContactoPadre;
    var abrirPopUp = true;

    try {
      if (opciones.prefijoControl) { prefijoControl = opciones.prefijoControl; } else { prefijoControl = ""; }
      if (opciones.invocadoDesde) { invocadoDesde = opciones.invocadoDesde; } else { invocadoDesde = ""; }
      if (opciones.idEscalamientoPorAlerta) { idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta; } else { idEscalamientoPorAlerta = "-1"; }
      if (opciones.idEscalamientoPorAlertaGrupoContacto) { idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto; } else { idEscalamientoPorAlertaGrupoContacto = "-1"; }
      if (opciones.idGrupoContactoEscalamientoAnterior) { idGrupoContactoEscalamientoAnterior = opciones.idGrupoContactoEscalamientoAnterior; } else { idGrupoContactoEscalamientoAnterior = "-1"; }
      if (opciones.nroEscalamiento) { nroEscalamiento = opciones.nroEscalamiento; } else { nroEscalamiento = ""; }
      if (opciones.idGrupoContactoPadre) { idGrupoContactoPadre = opciones.idGrupoContactoPadre; } else { idGrupoContactoPadre = "-1"; }

      queryString += "?prefijoControl=" + opciones.prefijoControl;
      queryString += "&invocadoDesde=" + opciones.invocadoDesde;
      queryString += "&idEscalamientoPorAlerta=" + opciones.idEscalamientoPorAlerta;
      queryString += "&idEscalamientoPorAlertaGrupoContacto=" + opciones.idEscalamientoPorAlertaGrupoContacto;
      queryString += "&idGrupoContactoEscalamientoAnterior=" + opciones.idGrupoContactoEscalamientoAnterior;
      queryString += "&nroEscalamiento=" + opciones.nroEscalamiento;
      queryString += "&idGrupoContactoPadre=" + opciones.idGrupoContactoPadre;

      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);

      //si fue invocado desde la opcion "...TieneDependencia", entonces verifica que sea desde el escalamiento 2 en adelante
      if (invocadoDesde == "AsociarTieneDependencia") {
        jQuery.each(json, function (indice, obj) {
          if (obj.IdEscalamientoPorAlerta == opciones.idEscalamientoPorAlerta) {
            if (indice == 0) {
              alert("No hay escalamiento anterior");
              abrirPopUp = false;
            }
            return false;
          }
        });
      }

      if (abrirPopUp) {
        jQuery.fancybox({
          width: 440,
          height: 350,
          helpers: {
            overlay: {
              css: {
                background: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL
              }
            }
          },
          modal: false,
          type: "iframe",
          href: "../page/Common.AsociarGrupo.aspx" + queryString
        });
      }

    } catch (e) {
      alert("Exception Alerta.abrirFormularioAsociarGrupo:\n" + e);
    }

  },

  dibujarGruposEscalamientoAnterior: function (opciones) {
    var idEscalamientoPorAlerta, json, jsonEscalamientoAnterior, templateAux, codigo, nombreGrupo, ordenEscalamiento, prefijoControl;
    var idEscalamientoPorAlertaGrupoContacto, idGrupoContactoEscalamientoAnterior, idGrupoContactoEscalamientoAnteriorAsignado;
    var prefijoControlEscalamiento, prefijoControlGrupo, codigoGrupoAnterior, nombreGrupoAnterior;
    var template = "";
    var listado = "";

    try {
      json = jQuery("#txtJSONEscalamientos", window.parent.document).val();
      json = jQuery.parseJSON(json);
      prefijoControl = opciones.prefijoControl;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoEscalamientoAnteriorAsignado = opciones.idGrupoContactoEscalamientoAnterior;

      template += "<div class=\"radio\">";
      template += "  <label><input type=\"radio\" id=\"hModal_rbtnGupo{INDICE}\" name=\"hModal_groupGrupo\" value=\"{ID_GRUPO_CONTACTO_ESCALAMIENTO_ANTERIOR}\" onclick=\"Alerta.asignarGrupoEscalamientoAnterior({ prefijoControl: '{PREFIJO_CONTROL}', idEscalamientoPorAlerta: '{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto: '{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idGrupoContactoEscalamientoAnterior: '{ID_GRUPO_CONTACTO_ESCALAMIENTO_ANTERIOR}', codigoGrupoAnterior: '{CODIGO_GRUPO_ANTERIOR}', nombreGrupoAnterior: '{NOMBRE_GRUPO_ANTERIOR}' })\">&nbsp;{NOMBRE_GRUPO}</label>";
      template += "</div>";

      //dibuja los escalamientos
      jQuery.each(json, function (indice, obj) {
        if (obj.IdEscalamientoPorAlerta == idEscalamientoPorAlerta) {
          if (indice > 0) {
            //consulta por el escalamiento anterior
            jsonEscalamientoAnterior = json[indice - 1];
            ordenEscalamiento = jsonEscalamientoAnterior.Orden;

            //muestra titulo
            jQuery("#lblTitulo").html("Escalamiento Nro. " + ordenEscalamiento + ", depende de ...");

            //muestra item NINGUNO
            templateAux = template;
            templateAux = templateAux.replace(/{NOMBRE_GRUPO}/ig, "Ninguno");
            templateAux = templateAux.replace(/{INDICE}/ig, "-1");
            templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
            templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
            templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
            templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_ESCALAMIENTO_ANTERIOR}/ig, "-1");
            templateAux = templateAux.replace(/{CODIGO_GRUPO_ANTERIOR}/ig, "-1");
            templateAux = templateAux.replace(/{NOMBRE_GRUPO_ANTERIOR}/ig, "");
            listado += templateAux;

            //------------------------------------------------
            //dibuja los grupos asociados
            jQuery.each(jsonEscalamientoAnterior.ListadoGrupos, function (indice, obj) {
              templateAux = template;
              codigo = obj.Codigo;
              idGrupoContactoEscalamientoAnterior = obj.IdEscalamientoPorAlertaGrupoContacto;

              //obtiene nombre del campo de texto "...txtNombreGrupo"
              prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + obj.IdEscalamientoPorAlerta;
              prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + obj.IdEscalamientoPorAlertaGrupoContacto;
              nombreGrupo = jQuery("#" + prefijoControlGrupo + "_txtNombreGrupo", window.parent.document).val();

              //formatea valores
              codigoGrupoAnterior = codigo;
              nombreGrupoAnterior = nombreGrupo;
              codigo = "<strong><span class=\"text-danger\">[" + codigo + "]</span></strong>";
              nombreGrupo = codigo + ((nombreGrupo == "") ? "" : " " + nombreGrupo);

              templateAux = templateAux.replace(/{NOMBRE_GRUPO}/ig, nombreGrupo);
              templateAux = templateAux.replace(/{INDICE}/ig, indice);
              templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
              templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
              templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
              templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_ESCALAMIENTO_ANTERIOR}/ig, idGrupoContactoEscalamientoAnterior);
              templateAux = templateAux.replace(/{CODIGO_GRUPO_ANTERIOR}/ig, codigoGrupoAnterior);
              templateAux = templateAux.replace(/{NOMBRE_GRUPO_ANTERIOR}/ig, nombreGrupoAnterior);
              listado += templateAux;
            });
          }
          return false;
        }
      });

      //asigna el contenido
      jQuery("#hContenido").html(listado);

      //asigna valor al radio button
      setTimeout(function () {
        jQuery("input[name=hModal_groupGrupo][value=" + idGrupoContactoEscalamientoAnteriorAsignado + "]").prop("checked", true);
      }, 100);

    } catch (e) {
      alert("Exception Alerta.dibujarGruposEscalamientoAnterior:\n" + e);
    }

  },

  asignarGrupoEscalamientoAnterior: function (opciones) {
    var json, arrEscalamiento, arrListadoGrupos, prefijoControl, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto;
    var jsonGrupoContacto, codigo, nombreGrupo, codigoGrupoAnterior, nombreGrupoAnterior;

    try {
      json = jQuery("#txtJSONEscalamientos", window.parent.document).val();
      json = jQuery.parseJSON(json);
      prefijoControl = opciones.prefijoControl;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoEscalamientoAnterior = opciones.idGrupoContactoEscalamientoAnterior;
      codigoGrupoAnterior = opciones.codigoGrupoAnterior;
      nombreGrupoAnterior = opciones.nombreGrupoAnterior;

      //busca en json el grupo que debe asignar la asociacion
      arrEscalamiento = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
        return (item.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto);
      });

      //asigna el grupo anterior al grupo consultado
      jsonGrupoContacto = arrListadoGrupos[0];
      jsonGrupoContacto.IdGrupoContactoEscalamientoAnterior = idGrupoContactoEscalamientoAnterior;

      //cambia texto y onclick del boton SELECCIONE
      if (codigoGrupoAnterior == "-1") {
        nombreGrupoAnterior = "--Seleccione--";
        jQuery("input[name=" + prefijoControl + "_groupTieneDependencia][value=0]", window.parent.document).prop("checked", true);
      } else {
        codigoGrupoAnterior = "<strong><span class=\"text-danger\">[" + codigoGrupoAnterior + "]</span></strong>";
        nombreGrupoAnterior = codigoGrupoAnterior + ((nombreGrupoAnterior == "") ? "" : " " + nombreGrupoAnterior);
      }
      jQuery("#" + prefijoControl + "_btnAsociarTieneDependencia", window.parent.document).html(nombreGrupoAnterior);
      jQuery("#" + prefijoControl + "_btnAsociarTieneDependencia", window.parent.document).attr("onclick", "Alerta.abrirFormularioAsociarGrupo({ prefijoControl:'" + prefijoControl + "', invocadoDesde: 'AsociarTieneDependencia', idEscalamientoPorAlerta: '" + idEscalamientoPorAlerta + "', idEscalamientoPorAlertaGrupoContacto: '" + idEscalamientoPorAlertaGrupoContacto + "', idGrupoContactoEscalamientoAnterior: '" + idGrupoContactoEscalamientoAnterior + "' })");

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONEscalamientos", window.parent.document).val(Sistema.convertirJSONtoString(json));
      parent.Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: prefijoControl, groupRadio: "groupTieneDependencia", elem_contenedor_boton: "hBotonAsociarTieneDependencia", elem_boton: "btnAsociarTieneDependencia" });

      Alerta.cerrarPopUp();
    } catch (e) {
      alert("Exception Alerta.asignarGrupoEscalamientoAnterior:\n" + e);
    }

  },

  obtenerDatosGrupoContactoAsignado: function (opciones) {
    var json, prefijoControl, idEscalamientoPorAlertaGrupoContacto, codigo, nombreGrupo, elem_boton;
    var grupoEncontrado = false;

    try {
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);
      prefijoControl = opciones.prefijoControl;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      elem_boton = opciones.elem_boton;

      //cambia texto y onclick del boton SELECCIONE
      if (idEscalamientoPorAlertaGrupoContacto == "-1") {
        codigo = "";
        nombreGrupo = "--Seleccione--";
      } else {
        jQuery.each(json, function (indice, obj) {
          jQuery.each(obj.ListadoGrupos, function (indice, obj) {
            if (obj.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto) {
              codigo = obj.Codigo;
              nombreGrupo = obj.NombreGrupo;
              grupoEncontrado = true;
              return false;
            }
          });

          if (grupoEncontrado) { return false; }
        });

        codigo = "<strong><span class=\"text-danger\">[" + codigo + "]</span></strong>";
        nombreGrupo = codigo + ((nombreGrupo == "") ? "" : " " + nombreGrupo);
      }
      jQuery("#" + prefijoControl + "_" + elem_boton).html(nombreGrupo);

    } catch (e) {
      alert("Exception Alerta.obtenerDatosGrupoContactoAsignado:\n" + e);
    }

  },

  mostrarBotonSeleccioneAsociarGrupo: function (opciones) {
    var valor, prefijoControl, groupRadio, elem_contenedor_boton;

    try {
      prefijoControl = opciones.prefijoControl;
      groupRadio = opciones.groupRadio;
      elem_contenedor_boton = opciones.elem_contenedor_boton;
      elem_boton = opciones.elem_boton;

      //obtiene valor del radio
      valor = jQuery("input:radio[name=" + prefijoControl + "_" + groupRadio + "]:checked").val();

      //si el valor proviene desde groupRadio entonces invirte el valor SOLO para mantener la estructura del codigo
      if (groupRadio == "groupMostrarSiempre") { valor = (valor == "1") ? "0" : "1"; }

      if (valor == "1") {
        jQuery("#" + prefijoControl + "_" + elem_contenedor_boton).show();

        if (groupRadio == "groupTieneDependencia") {
          jQuery("input[name=" + prefijoControl + "_groupMostrarSiempre][value=0]").prop("disabled", true);
          jQuery("input[name=" + prefijoControl + "_groupMostrarSiempre][value=1]").click();
        }

      } else {
        jQuery("#" + prefijoControl + "_" + elem_boton).html("--Seleccione--");
        jQuery("#" + prefijoControl + "_" + elem_contenedor_boton).hide();

        if (groupRadio == "groupTieneDependencia") {
          jQuery("input[name=" + prefijoControl + "_groupMostrarSiempre][value=0]").prop("disabled", false);
        }

      }
    } catch (e) {
      alert("Exception Alerta.mostrarBotonSeleccioneAsociarGrupo:\n" + e);
    }

  },

  dibujarGruposPadre: function (opciones) {
    var idEscalamientoPorAlerta, json, prefijoControl, idEscalamientoPorAlertaGrupoContacto, jsonListadoGruposPadre, jsonListadoGruposHijo;
    var totalHijos, idGrupoContactoPadreNuevo, templateAux, ordenEscalamiento, codigo, nombreGrupo;
    var nroEscalamiento, codigoGrupoPadre, nombreGrupoPadre, idGrupoContactoPadreAsignado;
    var template = "";
    var listado = "";

    try {
      json = jQuery("#txtJSONPadreHijo", window.parent.document).val();
      json = jQuery.parseJSON(json);
      prefijoControl = opciones.prefijoControl;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      nroEscalamiento = opciones.nroEscalamiento;
      idGrupoContactoPadreAsignado = opciones.idGrupoContactoPadre;

      template += "<div class=\"radio\">";
      template += "  <label><input type=\"radio\" id=\"hModal_rbtnGupo{INDICE}\" name=\"hModal_groupGrupo\" value=\"{ID_GRUPO_CONTACTO_PADRE}\" onclick=\"parent.Alerta.asignarGrupoPadre({ invocadoDesde: Alerta.FORMULARIO_COMMON_ASOCIAR_GRUPO, prefijoControl: '{PREFIJO_CONTROL}', idEscalamientoPorAlerta: '{ID_ESCALAMIENTO_ALERTA}', idEscalamientoPorAlertaGrupoContacto: '{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}', idGrupoContactoPadre: '{ID_GRUPO_CONTACTO_PADRE}', codigoGrupoPadre: '{CODIGO_GRUPO_PADRE}', nombreGrupoPadre: '{NOMBRE_GRUPO_PADRE}', nroEscalamiento: '{NRO_ESCALAMIENTO}' })\">&nbsp;{NOMBRE_GRUPO}</label>";
      template += "</div>";

      //obtiene los grupos del escalamiento
      arrEscalamiento = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      jsonListadoGruposPadre = arrEscalamiento[0].ListadoGruposPadre;

      //muestra titulo
      jQuery("#lblTitulo").html("Escalamiento Nro. " + nroEscalamiento + ", despu&eacute;s de ...");

      //-------------------------
      //muestra item NINGUNO
      templateAux = template;
      templateAux = templateAux.replace(/{NOMBRE_GRUPO}/ig, "Ninguno");
      templateAux = templateAux.replace(/{INDICE}/ig, "-100");
      templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
      templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
      templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
      templateAux = templateAux.replace(/{NRO_ESCALAMIENTO}/ig, nroEscalamiento);
      templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_PADRE}/ig, "-1");
      templateAux = templateAux.replace(/{CODIGO_GRUPO_PADRE}/ig, "-1");
      templateAux = templateAux.replace(/{NOMBRE_GRUPO_PADRE}/ig, "");
      listado += templateAux;
      //fin: muestra item NINGUNO
      //-------------------------

      //------------------------------------------
      //muestra GRUPO PADRE ACTUALMENTE ASIGNADO
      if (idGrupoContactoPadreAsignado != "-1") {
        jsonDatosGrupo = parent.Alerta.obtenerDatosGrupo({ idEscalamientoPorAlertaGrupoContacto: idGrupoContactoPadreAsignado });
        if (jsonDatosGrupo.codigo) {
          codigo = jsonDatosGrupo.codigo;
          nombreGrupo = jsonDatosGrupo.nombreGrupo;

          //formatea valores
          codigoGrupoPadre = codigo;
          nombreGrupoPadre = nombreGrupo;
          codigo = "<strong><span class=\"text-danger\">[" + codigo + "]</span></strong>";
          nombreGrupo = codigo + ((nombreGrupo == "") ? "" : " " + nombreGrupo);

          templateAux = template;
          templateAux = templateAux.replace(/{NOMBRE_GRUPO}/ig, nombreGrupo);
          templateAux = templateAux.replace(/{INDICE}/ig, "-1");
          templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
          templateAux = templateAux.replace(/{NRO_ESCALAMIENTO}/ig, nroEscalamiento);
          templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_PADRE}/ig, idGrupoContactoPadreAsignado);
          templateAux = templateAux.replace(/{CODIGO_GRUPO_PADRE}/ig, codigoGrupoPadre);
          templateAux = templateAux.replace(/{NOMBRE_GRUPO_PADRE}/ig, nombreGrupoPadre);
          listado += templateAux;
        }
      }
      //fin: muestra GRUPO PADRE ACTUALMENTE ASIGNADO
      //------------------------------------------

      //------------------------------------------
      //dibuja los ultimos grupos padre disponibles
      jQuery.each(jsonListadoGruposPadre, function (indice, obj) {
        templateAux = template;
        jsonListadoGruposHijo = obj.ListadoGruposHijo;
        totalHijos = jsonListadoGruposHijo.length;

        //obtiene el ultimo grupo y lo dibuja
        idGrupoContactoPadreNuevo = jsonListadoGruposHijo[totalHijos - 1].IdEscalamientoPorAlertaGrupoContactoHijo;

        //si el ultimo grupo es el mismo que esta buscando un padre entonces no lo dibuja
        if (idGrupoContactoPadreNuevo != idEscalamientoPorAlertaGrupoContacto) {
          jsonDatosGrupo = parent.Alerta.obtenerDatosGrupo({ idEscalamientoPorAlertaGrupoContacto: idGrupoContactoPadreNuevo });
          codigo = jsonDatosGrupo.codigo;
          nombreGrupo = jsonDatosGrupo.nombreGrupo;

          //formatea valores
          codigoGrupoPadre = codigo;
          nombreGrupoPadre = nombreGrupo;
          codigo = "<strong><span class=\"text-danger\">[" + codigo + "]</span></strong>";
          nombreGrupo = codigo + ((nombreGrupo == "") ? "" : " " + nombreGrupo);

          templateAux = templateAux.replace(/{NOMBRE_GRUPO}/ig, nombreGrupo);
          templateAux = templateAux.replace(/{INDICE}/ig, indice);
          templateAux = templateAux.replace(/{PREFIJO_CONTROL}/ig, prefijoControl);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA}/ig, idEscalamientoPorAlerta);
          templateAux = templateAux.replace(/{ID_ESCALAMIENTO_ALERTA_GRUPO_CONTACTO}/ig, idEscalamientoPorAlertaGrupoContacto);
          templateAux = templateAux.replace(/{ID_GRUPO_CONTACTO_PADRE}/ig, idGrupoContactoPadreNuevo);
          templateAux = templateAux.replace(/{CODIGO_GRUPO_PADRE}/ig, codigoGrupoPadre);
          templateAux = templateAux.replace(/{NOMBRE_GRUPO_PADRE}/ig, nombreGrupoPadre);
          templateAux = templateAux.replace(/{NRO_ESCALAMIENTO}/ig, nroEscalamiento);
          listado += templateAux;
        }

      });

      //asigna el contenido
      jQuery("#hContenido").html(listado);

      //asigna valor al radio button
      setTimeout(function () {
        jQuery("input[name=hModal_groupGrupo][value=" + idGrupoContactoPadreAsignado + "]").prop("checked", true);
      }, 100);

    } catch (e) {
      alert("Exception Alerta.dibujarGruposPadre:\n" + e);
    }

  },

  obtenerDatosGrupo: function (opciones) {
    var json, item, idEscalamientoPorAlertaGrupoContacto, codigo, nombreGrupo, prefijoControlEscalamiento, prefijoControlGrupo, ordenGrupo;
    var mostrarCuandoEscalamientoAnteriorNoContesta;
    var grupoEncontrado = false;

    try {
      item = {};
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);

      jQuery.each(json, function (indice, obj) {
        jQuery.each(obj.ListadoGrupos, function (indice, obj) {
          if (obj.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto) {
            //obtiene nombre del campo de texto "...txtNombreGrupo"
            prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + obj.IdEscalamientoPorAlerta;
            prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + obj.IdEscalamientoPorAlertaGrupoContacto;
            nombreGrupo = jQuery("#" + prefijoControlGrupo + "_txtNombreGrupo").val();
            ordenGrupo = jQuery("#" + prefijoControlGrupo + "_txtOrdenGrupo").val();
            mostrarCuandoEscalamientoAnteriorNoContesta = jQuery("input:radio[name=" + prefijoControlGrupo + "_groupMostrarNoContesta]:checked").val();

            item = {
              idEscalamientoPorAlertaGrupoContacto: obj.IdEscalamientoPorAlertaGrupoContacto,
              codigo: obj.Codigo,
              nombreGrupo: nombreGrupo,
              ordenGrupo: ordenGrupo,
              mostrarCuandoEscalamientoAnteriorNoContesta: mostrarCuandoEscalamientoAnteriorNoContesta
            };
            grupoEncontrado = true;
            return false;
          }
        });

        if (grupoEncontrado) { return false; }
      });

    } catch (e) {
      alert("Exception Alerta.obtenerDatosGrupo:\n" + e);
    }

    return item;
  },

  asignarGrupoPadre: function (opciones) {
    var json, arrEscalamiento, arrListadoGrupos, prefijoControl, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre;
    var jsonGrupoContacto, codigo, nombreGrupo, codigoGrupoPadre, nombreGrupoPadre, nroEscalamiento, invocadoDesde;

    try {
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);
      prefijoControl = opciones.prefijoControl;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;
      codigoGrupoPadre = opciones.codigoGrupoPadre;
      nombreGrupoPadre = opciones.nombreGrupoPadre;
      nroEscalamiento = opciones.nroEscalamiento;
      invocadoDesde = opciones.invocadoDesde;

      //busca en json el grupo que debe asignar la asociacion
      arrEscalamiento = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
        return (item.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto);
      });

      //asigna el grupo PADRE al grupo consultado
      jsonGrupoContacto = arrListadoGrupos[0];
      jsonGrupoContacto.IdGrupoContactoPadre = idGrupoContactoPadre;

      //cambia texto y onclick del boton SELECCIONE
      if (codigoGrupoPadre == "-1") {
        nombreGrupoPadre = "--Seleccione--";
        jQuery("input[name=" + prefijoControl + "_groupMostrarSiempre][value=1]").prop("checked", true);
      } else {
        codigoGrupoPadre = "<strong><span class=\"text-danger\">[" + codigoGrupoPadre + "]</span></strong>";
        nombreGrupoPadre = codigoGrupoPadre + ((nombreGrupoPadre == "") ? "" : " " + nombreGrupoPadre);
      }
      jQuery("#" + prefijoControl + "_btnAsociarGrupoPadre").html(nombreGrupoPadre);
      jQuery("#" + prefijoControl + "_btnAsociarGrupoPadre").attr("onclick", "Alerta.abrirFormularioAsociarGrupo({ prefijoControl:'" + prefijoControl + "', invocadoDesde: 'AsociarGrupoPadre', idEscalamientoPorAlerta: '" + idEscalamientoPorAlerta + "', idEscalamientoPorAlertaGrupoContacto: '" + idEscalamientoPorAlertaGrupoContacto + "', nroEscalamiento: '" + nroEscalamiento + "', idGrupoContactoPadre: '" + idGrupoContactoPadre + "' })");

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));
      Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: prefijoControl, groupRadio: "groupMostrarSiempre", elem_contenedor_boton: "hBotonAsociarGrupoPadre", elem_boton: "btnAsociarGrupoPadre" });

      //actualizacion jsonPadreHijo
      Alerta.actualizarJSONGrupoPadreHijo({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre, nroEscalamiento: nroEscalamiento });

      if (invocadoDesde == Alerta.FORMULARIO_COMMON_ASOCIAR_GRUPO) {
        jQuery.fancybox.close();
      }
    } catch (e) {
      alert("Exception Alerta.asignarGrupoPadre:\n" + e);
    }

  },

  actualizarJSONGrupoPadreHijo: function (opciones) {
    var json, idEscalamientoPorAlerta, idGrupoContactoPadre, idEscalamientoPorAlertaGrupoContacto, item, arrEscalamientos, accion, nroEscalamiento;
    var grupoEncontrado = false;
    var accionEncontrada = false;

    try {
      json = jQuery("#txtJSONPadreHijo").val();
      json = jQuery.parseJSON(json);
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;
      nroEscalamiento = opciones.nroEscalamiento;

      //PASO 1: verifica si el escalamiento existe en el json, sino lo agrega
      if (!accionEncontrada) {
        arrEscalamientos = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
        });

        if (arrEscalamientos.length == 0) {
          accion = "AgregarEscalamiento";
          accionEncontrada = true;
        }
      }

      //PASO 2: verifica si el grupo existe en el escalamiento
      if (!accionEncontrada) {
        jQuery.each(arrEscalamientos[0].ListadoGruposPadre, function (indice, obj) {
          jQuery.each(obj.ListadoGruposHijo, function (indice, obj) {
            if (obj.IdEscalamientoPorAlertaGrupoContactoHijo == idEscalamientoPorAlertaGrupoContacto) {
              accion = "ActualizarGrupo";
              accionEncontrada = true;
              grupoEncontrado = true;
              return false;
            }
          });

          if (grupoEncontrado) { return false; }
        });

        if (!grupoEncontrado) {
          accion = "AgregarGrupo";
          accionEncontrada = true;
        }
      }

      //-------------------------------------------------
      //realiza la accion solicitada
      switch (accion) {
        case "AgregarEscalamiento":
          json = Alerta.actualizarJSONGrupoPadreHijo_AgregarEscalamiento({ json: json, idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre });
          break;
        case "AgregarGrupo":
          json = Alerta.actualizarJSONGrupoPadreHijo_AgregarGrupo({ json: json, idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre });
          break;
        case "ActualizarGrupo":
          json = Alerta.actualizarJSONGrupoPadreHijo_ActualizarGrupo({ json: json, idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre, nroEscalamiento: nroEscalamiento });
          break;
      }

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONPadreHijo").val(Sistema.convertirJSONtoString(json));

    } catch (e) {
      alert("Exception Alerta.actualizarJSONGrupoPadreHijo:\n" + e);
    }
  },

  actualizarJSONGrupoPadreHijo_AgregarEscalamiento: function (opciones) {
    var json, item, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre;

    try {
      json = opciones.json;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;

      item = {
        IdEscalamientoPorAlerta: idEscalamientoPorAlerta,
        ListadoGruposPadre: [
          {
            ListadoGruposHijo: [
              {
                IdEscalamientoPorAlertaGrupoContactoHijo: idEscalamientoPorAlertaGrupoContacto,
                IdGrupoContactoPadre: idGrupoContactoPadre
              }
            ]
          }
        ]
      };

      json.push(item);
    } catch (e) {
      alert("Exception Alerta.actualizarJSONGrupoPadreHijo_AgregarEscalamiento:\n" + e);
    }

    return json;
  },

  actualizarJSONGrupoPadreHijo_AgregarGrupo: function (opciones) {
    var json, item, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre, jsonListadoGruposPadre, jsonListadoGruposHijo;
    var grupoEncontrado = false;

    try {
      json = opciones.json;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;

      arrEscalamientos = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      jsonListadoGruposPadre = arrEscalamientos[0].ListadoGruposPadre;

      //verifica si el padre existe en el escalamiento, si lo encuentra entonces lo agrega al final de la lista, sino crea una nueva lista de hijos
      jQuery.each(jsonListadoGruposPadre, function (indice, obj) {
        jsonListadoGruposHijo = obj.ListadoGruposHijo;

        jQuery.each(jsonListadoGruposHijo, function (indice, obj) {
          if (obj.IdEscalamientoPorAlertaGrupoContactoHijo == idGrupoContactoPadre) {
            item = {
              IdEscalamientoPorAlertaGrupoContactoHijo: idEscalamientoPorAlertaGrupoContacto,
              IdGrupoContactoPadre: idGrupoContactoPadre
            };
            jsonListadoGruposHijo.push(item);
            grupoEncontrado = true;
            return false;
          }
        });

        if (grupoEncontrado) { return false; }
      });

      //si no encontro el padre, entonces crea nueva lista
      if (!grupoEncontrado) {
        item = {
          ListadoGruposHijo: [
            {
              IdEscalamientoPorAlertaGrupoContactoHijo: idEscalamientoPorAlertaGrupoContacto,
              IdGrupoContactoPadre: idGrupoContactoPadre
            }
          ]
        };
        jsonListadoGruposPadre.push(item);
      }
    } catch (e) {
      alert("Exception Alerta.actualizarJSONGrupoPadreHijo_AgregarGrupo:\n" + e);
    }

    return json;
  },

  actualizarJSONGrupoPadreHijo_ActualizarGrupo: function (opciones) {
    var json, item, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre, jsonListadoGruposPadre, jsonListadoGruposHijo;
    var idGrupoContactoPadreActual, nroEscalamiento;
    var grupoEncontrado = false;

    try {
      json = opciones.json;
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;
      nroEscalamiento = opciones.nroEscalamiento;

      arrEscalamientos = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });

      jsonListadoGruposPadre = arrEscalamientos[0].ListadoGruposPadre;

      //verifica si el grupo tiene asignado el mismo padre
      jQuery.each(jsonListadoGruposPadre, function (indice, obj) {
        jsonListadoGruposHijo = obj.ListadoGruposHijo;

        jQuery.each(jsonListadoGruposHijo, function (indice, obj) {
          if (obj.IdEscalamientoPorAlertaGrupoContactoHijo == idEscalamientoPorAlertaGrupoContacto && obj.IdGrupoContactoPadre == idGrupoContactoPadre) {
            grupoEncontrado = true;
            return false;
          }
        });

        if (grupoEncontrado) { return false; }
      });

      //si encuentra el grupo no hace nada y devuelve el mismo json. Sino actualiza el padre
      if (!grupoEncontrado) {
        //----------------------------------------------------
        //busca el grupo consultado y lo saca de la lista para asignarlo en el listado del escalamiento que corresponda
        jQuery.each(jsonListadoGruposPadre, function (indice, obj) {
          jsonListadoGruposHijo = obj.ListadoGruposHijo;

          jQuery.each(jsonListadoGruposHijo, function (indice, obj) {
            if (obj.IdEscalamientoPorAlertaGrupoContactoHijo == idEscalamientoPorAlertaGrupoContacto) {
              //obtiene el padre actual que tiene el grupo para heredarlo al grupo que le sigue
              idGrupoContactoPadreActual = obj.IdGrupoContactoPadre;

              //obtiene el siguiente grupo de la lista
              if (indice + 1 <= jsonListadoGruposHijo.length - 1) {
                itemGrupo = jsonListadoGruposHijo[indice + 1];
                itemGrupo.IdGrupoContactoPadre = idGrupoContactoPadreActual;

                //cambia el valor del boton del grupo siguiente, asigna el nuevo PADRE en el JSON de escalamientos y cambia el orden
                Alerta.cambiarBotonAsociarGrupoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: itemGrupo.IdEscalamientoPorAlertaGrupoContactoHijo, idGrupoContactoPadre: itemGrupo.IdGrupoContactoPadre, nroEscalamiento: nroEscalamiento });
                Alerta.asignarGrupoNuevoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: itemGrupo.IdEscalamientoPorAlertaGrupoContactoHijo, idGrupoContactoPadre: itemGrupo.IdGrupoContactoPadre });
                Alerta.asignarOrdenDespuesDeNuevoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: itemGrupo.IdEscalamientoPorAlertaGrupoContactoHijo, idGrupoContactoPadre: itemGrupo.IdGrupoContactoPadre });
              }

              //asigna nuevo orden al grupo despues de su PADRE
              Alerta.asignarOrdenDespuesDeNuevoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre });

              //elimina el grupo consultado
              jsonListadoGruposHijo.splice(indice, 1);

              grupoEncontrado = true;
              return false;
            }
          });

          if (grupoEncontrado) {
            //si jsonListadoGruposHijo queda sin hijos, entonces se elimina de la lista
            if (jsonListadoGruposHijo.length == 0) {
              jsonListadoGruposPadre.splice(indice, 1);
            }

            return false;
          }
        });

        //verifica que si un listado PADRE se queda sin hijos entonces lo elimina
        jQuery.each(json, function (indice, obj) {
          if (obj.IdEscalamientoPorAlerta == idEscalamientoPorAlerta) {
            if (obj.ListadoGruposPadre.length == 0) {
              json.splice(indice, 1);
            }
            return false;
          }
        });

        //----------------------------------------------------
        //agrega el grupo con su nuevo padre al final de la lista en el escalamiento que le corresponde
        json = Alerta.actualizarJSONGrupoPadreHijo_AgregarGrupo({ json: json, idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre: idGrupoContactoPadre });

      }
    } catch (e) {
      alert("Exception Alerta.actualizarJSONGrupoPadreHijo_ActualizarGrupo:\n" + e);
    }

    return json;
  },

  actualizarJSONGrupoPadreHijo_EliminarGrupo: function (opciones) {
    var json, item, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, jsonListadoGruposPadre, jsonListadoGruposHijo, idGrupoContactoPadreActual;
    var nroEscalamiento;
    var grupoEncontrado = false;

    try {
      json = jQuery("#txtJSONPadreHijo").val();
      json = jQuery.parseJSON(json);
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      nroEscalamiento = opciones.nroEscalamiento;

      //si idEscalamientoPorAlertaGrupoContacto != -1 entonces elimina el grupo, sino elimina todo el escalamiento y sus grupos
      if (idEscalamientoPorAlertaGrupoContacto != "-1") {
        arrEscalamientos = jQuery.grep(json, function (item, indice) {
          return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
        });
        jsonListadoGruposPadre = arrEscalamientos[0].ListadoGruposPadre;

        //busca el grupo para eliminarlo
        jQuery.each(jsonListadoGruposPadre, function (indice, obj) {
          jsonListadoGruposHijo = obj.ListadoGruposHijo;

          jQuery.each(jsonListadoGruposHijo, function (indice, obj) {
            if (obj.IdEscalamientoPorAlertaGrupoContactoHijo == idEscalamientoPorAlertaGrupoContacto) {
              //obtiene el padre actual que tiene el grupo para heredarlo al grupo que le sigue
              idGrupoContactoPadreActual = obj.IdGrupoContactoPadre;

              //obtiene el siguiente grupo de la lista
              if (indice + 1 <= jsonListadoGruposHijo.length - 1) {
                itemGrupo = jsonListadoGruposHijo[indice + 1];
                itemGrupo.IdGrupoContactoPadre = idGrupoContactoPadreActual;

                //cambia el valor del boton del grupo siguiente, asigna el nuevo PADRE en el JSON de escalamientos y cambia el orden
                Alerta.cambiarBotonAsociarGrupoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: itemGrupo.IdEscalamientoPorAlertaGrupoContactoHijo, idGrupoContactoPadre: itemGrupo.IdGrupoContactoPadre, nroEscalamiento: nroEscalamiento });
                Alerta.asignarGrupoNuevoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: itemGrupo.IdEscalamientoPorAlertaGrupoContactoHijo, idGrupoContactoPadre: itemGrupo.IdGrupoContactoPadre });
                Alerta.asignarOrdenDespuesDeNuevoPadre({ idEscalamientoPorAlerta: idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto: itemGrupo.IdEscalamientoPorAlertaGrupoContactoHijo, idGrupoContactoPadre: itemGrupo.IdGrupoContactoPadre });
              }

              //elimina el grupo consultado
              jsonListadoGruposHijo.splice(indice, 1);

              grupoEncontrado = true;
              return false;
            }
          });

          if (grupoEncontrado) {
            //si jsonListadoGruposHijo queda sin hijos, entonces se elimina de la lista
            if (jsonListadoGruposHijo.length == 0) {
              jsonListadoGruposPadre.splice(indice, 1);
            }

            return false;
          }
        });

        //verifica que si un listado PADRE se queda sin hijos entonces lo elimina
        jQuery.each(json, function (indice, obj) {
          if (obj.IdEscalamientoPorAlerta == idEscalamientoPorAlerta) {
            if (obj.ListadoGruposPadre.length == 0) {
              json.splice(indice, 1);
            }
            return false;
          }
        });

      } else {
        //busca el escalamiento y lo elimina de la lista
        jQuery.each(json, function (indice, obj) {
          if (obj.IdEscalamientoPorAlerta == idEscalamientoPorAlerta) {
            json.splice(indice, 1);
            return false;
          }
        });

      }

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONPadreHijo").val(Sistema.convertirJSONtoString(json));

    } catch (e) {
      alert("Exception Alerta.actualizarJSONGrupoPadreHijo_EliminarGrupo:\n" + e);
    }

  },

  cambiarBotonAsociarGrupoPadre: function (opciones) {
    var idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre, nroEscalamiento, codigo, nombreGrupo, ordenGrupo;
    var jsonDatosGrupo, prefijoControlEscalamiento, prefijoControlGrupo;

    try {
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;
      nroEscalamiento = opciones.nroEscalamiento;

      //construye prefijo del control
      prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;
      prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;

      if (idGrupoContactoPadre == "-1") {
        codigo = "-1";
        nombreGrupo = "--Seleccione--";
        jQuery("input[name=" + prefijoControlGrupo + "_groupMostrarSiempre][value=1]").prop("checked", true);
      } else {
        jsonDatosGrupo = Alerta.obtenerDatosGrupo({ idEscalamientoPorAlertaGrupoContacto: idGrupoContactoPadre });
        codigo = jsonDatosGrupo.codigo;
        nombreGrupo = jsonDatosGrupo.nombreGrupo;

        //formatea valores
        codigo = "<strong><span class=\"text-danger\">[" + codigo + "]</span></strong>";
        nombreGrupo = codigo + ((nombreGrupo == "") ? "" : " " + nombreGrupo);
      }
      //cambia el boton
      jQuery("#" + prefijoControlGrupo + "_btnAsociarGrupoPadre").html(nombreGrupo);
      jQuery("#" + prefijoControlGrupo + "_btnAsociarGrupoPadre").attr("onclick", "Alerta.abrirFormularioAsociarGrupo({ prefijoControl:'" + prefijoControlGrupo + "', invocadoDesde: 'AsociarGrupoPadre', idEscalamientoPorAlerta: '" + idEscalamientoPorAlerta + "', idEscalamientoPorAlertaGrupoContacto: '" + idEscalamientoPorAlertaGrupoContacto + "', nroEscalamiento: '" + nroEscalamiento + "', idGrupoContactoPadre: '" + idGrupoContactoPadre + "' })");

      Alerta.mostrarBotonSeleccioneAsociarGrupo({ prefijoControl: prefijoControlGrupo, groupRadio: "groupMostrarSiempre", elem_contenedor_boton: "hBotonAsociarGrupoPadre", elem_boton: "btnAsociarGrupoPadre" });

    } catch (e) {
      alert("Exception Alerta.cambiarBotonAsociarGrupoPadre:\n" + e);
    }
  },

  asignarOrdenDespuesDeNuevoPadre: function (opciones) {
    var idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre, prefijoControlEscalamiento, prefijoControlGrupo, jsonDatosGrupo;
    var mostrarCuandoEscalamientoAnteriorNoContesta;

    try {
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;

      //construye prefijo del control
      prefijoControlEscalamiento = Alerta.PREFIJO_ESCALAMIENTO + idEscalamientoPorAlerta;
      prefijoControlGrupo = prefijoControlEscalamiento + "_" + Alerta.PREFIJO_GRUPO + idEscalamientoPorAlertaGrupoContacto;

      //obtiene informacion del PADRE
      jsonDatosGrupo = Alerta.obtenerDatosGrupo({ idEscalamientoPorAlertaGrupoContacto: idGrupoContactoPadre });
      if (jsonDatosGrupo.ordenGrupo) {
        ordenGrupo = parseInt(jsonDatosGrupo.ordenGrupo);
        mostrarCuandoEscalamientoAnteriorNoContesta = jsonDatosGrupo.mostrarCuandoEscalamientoAnteriorNoContesta;
        //asigna nuevo orden despues del padre
        jQuery("#" + prefijoControlGrupo + "_txtOrdenGrupo").val(ordenGrupo + 1);
        jQuery("input[name=" + prefijoControlGrupo + "_groupMostrarNoContesta][value=" + mostrarCuandoEscalamientoAnteriorNoContesta + "]").prop("checked", true);
      }

    } catch (e) {
      alert("Exception Alerta.asignarOrdenDespuesDeNuevoPadre:\n" + e);
    }

  },

  asignarGrupoNuevoPadre: function (opciones) {
    var json, arrEscalamiento, arrListadoGrupos, idEscalamientoPorAlerta, idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre;
    var jsonGrupoContacto;

    try {
      json = jQuery("#txtJSONEscalamientos").val();
      json = jQuery.parseJSON(json);
      idEscalamientoPorAlerta = opciones.idEscalamientoPorAlerta;
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      idGrupoContactoPadre = opciones.idGrupoContactoPadre;

      //busca en json el grupo que debe asignar la asociacion
      arrEscalamiento = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlerta == idEscalamientoPorAlerta);
      });
      arrListadoGrupos = jQuery.grep(arrEscalamiento[0].ListadoGrupos, function (item, indice) {
        return (item.IdEscalamientoPorAlertaGrupoContacto == idEscalamientoPorAlertaGrupoContacto);
      });

      //asigna el grupo PADRE al grupo consultado
      jsonGrupoContacto = arrListadoGrupos[0];
      jsonGrupoContacto.IdGrupoContactoPadre = idGrupoContactoPadre;

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONEscalamientos").val(Sistema.convertirJSONtoString(json));

    } catch (e) {
      alert("Exception Alerta.asignarGrupoNuevoPadre:\n" + e);
    }

  },

  mostrarGrupoContactoHijo_TeleOperador: function (opciones) {
    var idEscalamientoPorAlertaGrupoContacto, tipoExplicacion, objDiv, invocadoDesde;

    try {
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      invocadoDesde = opciones.invocadoDesde;
      tipoExplicacion = jQuery("#" + idEscalamientoPorAlertaGrupoContacto + "_ddlExplicacion option:selected").attr("data-tipoExplicacion");
      objDiv = jQuery("div[rel=relGrupoHijo_GrupoPadre" + idEscalamientoPorAlertaGrupoContacto + "]");

      if (objDiv.length) {
        if (Sistema.toUpper(tipoExplicacion) == Alerta.NO_CONTESTA && invocadoDesde == "Combo") {
          objDiv.show();
        } else {
          objDiv.hide();

          //oculta todos los grupos relacionados en cascada
          elem_div = objDiv.attr("id");
          idGrupoContactoPadre_DeHijo = elem_div.replace("_hGrupoContacto", "");
          jQuery("#" + idGrupoContactoPadre_DeHijo + "_ddlExplicacion").select2("val", "");
          if (jQuery("div[rel=relGrupoHijo_GrupoPadre" + idGrupoContactoPadre_DeHijo + "]").length > 0) {
            Alerta.mostrarGrupoContactoHijo_TeleOperador({ idEscalamientoPorAlertaGrupoContacto: idGrupoContactoPadre_DeHijo, invocadoDesde: "OcultarEnCascada" });
          }
        }
      }

    } catch (e) {
      alert("Exception Alerta.mostrarGrupoContactoHijo_TeleOperador:\n" + e);
    }
  },

  reemplazarMarcasEspecialesScript: function (opciones) {
    var prefijoControl, explicacion, frecuenciaRepeticion, item, html, tiempoDetencion, horas, minutos;
    var cargoContactoCombo, nombreContactoCombo;

    try {
      prefijoControl = opciones.prefijoControl;
      explicacion = jQuery("#" + prefijoControl + "_ddlExplicacion").select2("val");
      frecuenciaRepeticion = jQuery("#" + prefijoControl + "_ddlExplicacion option:selected").attr("data-frecuencia");
      frecuenciaRepeticion = parseInt(frecuenciaRepeticion);

      //----------------------------------------------------
      // busca datos del contacto seleccionado en el combo
      json = jQuery("#" + opciones.prefijoControl + "_txtJSONContacto").val();
      json = jQuery.parseJSON(json);
      idEscalamientoPorAlertaContactoConsultado = jQuery("#" + opciones.prefijoControl + "_ddlContacto").val();

      json = jQuery.grep(json, function (item, indice) {
        return (item.IdEscalamientoPorAlertaContacto == idEscalamientoPorAlertaContactoConsultado);
      });
      if (json.length > 0) {
        item = json[0];
        cargoContactoCombo = item.Cargo;
        nombreContactoCombo = item.NombreUsuario;
      } else {
        cargoContactoCombo = "";
        nombreContactoCombo = "Desconocido";
      }
      //----------------------------------------------------

      //si el array global esta vacio, entonces se llena con los script originales y asi no perder las marcas especiales
      if (Alerta.ARRAY_SCRIPT_GRUPO_CONTACTO.length == 0) {
        jQuery(".script-descripcion").each(function (indice, obj) {
          item = {
            elem_script: jQuery(obj).attr("id"),
            html: jQuery(obj).html()
          };
          Alerta.ARRAY_SCRIPT_GRUPO_CONTACTO.push(item);
        });
      }

      //reemplaza las marcas especiales
      jQuery.each(Alerta.ARRAY_SCRIPT_GRUPO_CONTACTO, function (indice, obj) {
        if (Sistema.startsWith(obj.elem_script, opciones.prefijoControl + "_")) {
          html = obj.html;

          if (frecuenciaRepeticion == -1) {
            explicacion = "Desconocido";
            tiempoDetencion = "TIEMPO DESCONOCIDO";
          } else {
            if (frecuenciaRepeticion < 60) {
              tiempoDetencion = frecuenciaRepeticion + " min.";
            } else {
              horas = Math.floor(frecuenciaRepeticion / 60);
              minutos = frecuenciaRepeticion % 60;
              tiempoDetencion = horas + " hr.";
              tiempoDetencion += (minutos == 0) ? "" : " y " + minutos + " min.";
            }
          }

          html = html.replace("[(MOTIVO_DETENCION)]", "<strong><em>" + Sistema.toUpper(explicacion) + "</em></strong>");
          html = html.replace("[(TIEMPO_DETENCION)]", "<strong><em>" + tiempoDetencion + "</em></strong>");
          html = html.replace("[(CARGO_CONTACTO_COMBO)]", "<strong><em>" + ((cargoContactoCombo == "" ? "" : Sistema.toUpper(cargoContactoCombo) + ": ")) + "</em></strong>");
          html = html.replace("[(NOMBRE_CONTACTO_COMBO)]", "<strong><em>" + Sistema.toUpper(nombreContactoCombo) + "</em></strong>");

          jQuery("#" + obj.elem_script).html(html);
          return false;
        }
      });

    } catch (e) {
      alert("Exception Alerta.reemplazarMarcasEspecialesScript:\n" + e);
    }

  },

  marcarTelefono: function (opciones) {
    var telefono, idEscalamientoPorAlertaGrupoContacto, prefijoControl, telefonoLlamadaActual, estaOcupadoInconcert;

    try {
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;
      prefijoControl = idEscalamientoPorAlertaGrupoContacto;
      telefono = jQuery("#" + prefijoControl + "_lblTelefono").html();
      telefonoLlamadaActual = jQuery("#txtCallCenterTelefono").val();

      if (telefonoLlamadaActual != "") {
        alert("Debe finalizar la comunicación con el teléfono " + telefonoLlamadaActual + " antes de realizar una nueva llamada.");
      } else {
        estaOcupadoInconcert = Alerta.estaOcupadoInconcert({ idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto });
        if (estaOcupadoInconcert) {
          bootbox.dialog({
            closeButton: false,
            message: "<span class=\"sist-font-size-30\"><span class=\"text-danger\"><span class=\"glyphicon glyphicon-time\"></span>&nbsp;<em><strong>INCONCERT est&aacute; ocupado</strong></em></span>, intente el llamado nuevamente por favor.</span>",
            title: "<strong>INCONCERT OCUPADO</strong>",
            buttons: {
              success: {
                label: "Aceptar",
                className: "btn-success btn-lg",
                callback: function () {
                  //cierra la ventana modal
                }
              }
            }
          });

        } else {
          if (!Validacion.tipoNumeroEnteroPositivoMayorCero(telefono)) {
            alert("Debe marcar un número válido.");
          } else {
            telefono = Sistema.formatearTelefonoCallCenter(telefono);
            jQuery("#txtCallCenterTelefono").val(telefono);
            jQuery("#txtCallCenterIdEscalamientoPorAlertaGrupoContacto").val(idEscalamientoPorAlertaGrupoContacto);
            jQuery("#" + prefijoControl + "_lblTelefono").removeAttr("onclick");
            jQuery("#" + prefijoControl + "_lblTelefono").attr("data-original-title", "en proceso");

            if (!Sistema.SIMULAR_ACTIVEX) {
              var barAgent = new ActiveXObject("BarAgentControl.BAControl");
              barAgent.MakeCallDir(Sistema.CALLCENTER_CAMPAIGN, telefono);
            }

            jQuery("#" + prefijoControl + "_ddlContacto").attr("disabled", true);
            Sistema.SETINTERVAL_MOSTRAR_FINALIZAR_LLAMADA = setInterval(function () { jQuery("#" + prefijoControl + "_lblFinalizarLlamada").show(); }, Alerta.SETINTERVAL_MILISEGUNDOS_MOSTRAR_FINALIZAR_LLAMADA);

            Alerta.monitorearLlamada();
            Sistema.SETINTERVAL_CALLCENTER_LLAMADO = setInterval(function () { Alerta.monitorearLlamada(); }, 1000);
          }

        }

      }
    } catch (e) {
      alert("No es posible realizar la llamada desde un ambiente fuera del CallCentter\n(Error: " + e + ")");
      clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
      Alerta.limpiarControlesLlamada();
    }

  },

  monitorearLlamada: function () {
    var idLlamada, estadoLlamada, barAgent, mensaje, telefono, finalizarLlamadaModoManual, prefijoControl;
    var currentIntStateByID = "-1";

    try {
      idLlamada = jQuery("#txtCallCenterIdLlamada").val();
      estadoLlamada = jQuery("#txtCallCenterEstadoLlamada").val();
      telefono = jQuery("#txtCallCenterTelefono").val();
      finalizarLlamadaModoManual = jQuery("#txtCallCenterFinalizarLlamadaModoManual").val();
      prefijoControl = jQuery("#txtCallCenterIdEscalamientoPorAlertaGrupoContacto").val();

      if (Sistema.SIMULAR_ACTIVEX) {
        barAgent = Sistema.simularActiveX();
      } else {
        barAgent = new ActiveXObject("BarAgentControl.BAControl");
      }

      if (idLlamada == "") {
        if (Sistema.SIMULAR_ACTIVEX) {
          idLlamada = barAgent.GetIdCall;
        } else {
          idLlamada = barAgent.GetIdCall();
        }
        jQuery("#txtCallCenterIdLlamada").val(idLlamada);
      } else {
        if (Sistema.SIMULAR_ACTIVEX) {
          currentIntStateByID = barAgent.CurrentIntStateByID;
        } else {
          currentIntStateByID = barAgent.CurrentIntStateByID(idLlamada);
        }
      }

      //si se finaliza la llamada usando el boton, entonces asigna estado finalizado a la llamada
      if (finalizarLlamadaModoManual == "1") { currentIntStateByID = Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED; }

      //si la llamada cambia de estado entonces se graba el nuevo estado
      if (idLlamada != "" && currentIntStateByID != estadoLlamada) {
        jQuery("#txtCallCenterEstadoLlamada").val(currentIntStateByID);
        Alerta.grabarEstadoLlamada();
      }

      //muestra mensaje segun el estado de la llamada
      mensaje = Sistema.mensajeLlamadaPorEstado({ estado: currentIntStateByID, telefono: telefono });
      jQuery("#" + prefijoControl + "_lblMensajeLlamada").html(mensaje);
      jQuery("#" + prefijoControl + "_lblMensajeLlamada").show();

      //si la llamada finalizo cierra popUp y limpia controles
      if (currentIntStateByID == Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED || currentIntStateByID == "") {
        if (!Sistema.SIMULAR_ACTIVEX) {
          barAgent.WrapUpByID(idLlamada);
        }
        clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
        setTimeout(function () { Alerta.limpiarControlesLlamada(); }, 1000);
      }

    } catch (e) {
      alert("Exception Alerta.monitorearLlamada:\n" + e);
      clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
      Alerta.limpiarControlesLlamada();
    }

  },

  finalizarLlamado: function (opciones) {
    var telefono, idLlamada, currentIntStateByID;

    try {
      idLlamada = jQuery("#txtCallCenterIdLlamada").val();

      if (Sistema.SIMULAR_ACTIVEX) {
        barAgent = Sistema.simularActiveX();
        currentIntStateByID = barAgent.CurrentIntStateByID;
      } else {
        barAgent = new ActiveXObject("BarAgentControl.BAControl");
        currentIntStateByID = barAgent.CurrentIntStateByID(idLlamada);
      }

      if (idLlamada != "") {
        if (!Sistema.SIMULAR_ACTIVEX) {
          barAgent.HangUp();
        }
        jQuery("#txtCallCenterFinalizarLlamadaModoManual").val("1");
      }

    } catch (e) {
      alert("Exception Alerta.finalizarLlamado:\n" + e);
      clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
      Alerta.limpiarControlesLlamada();
    }
  },

  limpiarControlesLlamada: function () {
    var prefijoControl, telefono;

    try {
      prefijoControl = jQuery("#txtCallCenterIdEscalamientoPorAlertaGrupoContacto").val();
      jQuery("#txtCallCenterTelefono").val("");
      jQuery("#txtCallCenterIdLlamada").val("");
      jQuery("#txtCallCenterEstadoLlamada").val("-1");
      jQuery("#txtCallCenterFinalizarLlamadaModoManual").val("0");
      jQuery("#txtCallCenterIdEscalamientoPorAlertaGrupoContacto").val("");

      //ocultar boton finalizar llamada y volver la funcionalidad del telefono por default
      jQuery("#" + prefijoControl + "_lblTelefono").attr("onclick", "Alerta.marcarTelefono({ idEscalamientoPorAlertaGrupoContacto: '" + prefijoControl + "' })");
      jQuery("#" + prefijoControl + "_lblTelefono").attr("data-original-title", "haga click para llamar");
      jQuery("#" + prefijoControl + "_lblFinalizarLlamada").hide();
      jQuery("#" + prefijoControl + "_lblMensajeLlamada").html("");
      jQuery("#" + prefijoControl + "_lblMensajeLlamada").hide("");
      jQuery("#" + prefijoControl + "_ddlContacto").attr("disabled", false);
      clearInterval(Sistema.SETINTERVAL_MOSTRAR_FINALIZAR_LLAMADA);
    } catch (e) {
      alert("Exception Alerta.limpiarControlesLlamada:\n" + e);
    }
  },

  grabarEstadoLlamada: function (opciones) {
    var queryString = "";
    var idEscalamientoPorAlertaGrupoContacto, jsonContacto, nombreContacto, telefono, estadoLlamada, idEscalamientoPorAlertaContactoConsultado;
    var prefijoControl, idLlamada;

    try {
      if (opciones) {
        estadoLlamada = (opciones.estadoLlamada != "") ? opciones.estadoLlamada : jQuery("#txtCallCenterEstadoLlamada").val();
        idLlamada = (opciones.idLlamada != "") ? opciones.idLlamada : jQuery("#txtCallCenterIdLlamada").val();
        idEscalamientoPorAlertaGrupoContacto = (opciones.idEscalamientoPorAlertaGrupoContacto != "") ? opciones.idEscalamientoPorAlertaGrupoContacto : jQuery("#txtCallCenterIdEscalamientoPorAlertaGrupoContacto").val();
      } else {
        estadoLlamada = jQuery("#txtCallCenterEstadoLlamada").val();
        idLlamada = jQuery("#txtCallCenterIdLlamada").val();
        idEscalamientoPorAlertaGrupoContacto = jQuery("#txtCallCenterIdEscalamientoPorAlertaGrupoContacto").val();
      }

      prefijoControl = idEscalamientoPorAlertaGrupoContacto;
      jsonContacto = jQuery("#" + prefijoControl + "_txtJSONContacto").val();
      jsonContacto = jQuery.parseJSON(jsonContacto);
      idEscalamientoPorAlertaContactoConsultado = jQuery("#" + prefijoControl + "_ddlContacto").val();

      jsonContacto = jQuery.grep(jsonContacto, function (item, indice) {
        return (item.IdEscalamientoPorAlertaContacto == idEscalamientoPorAlertaContactoConsultado);
      });
      nombreContacto = jsonContacto[0].NombreUsuario;
      telefono = jsonContacto[0].Telefono;

      switch (Sistema.toUpper(estadoLlamada)) {
        case Sistema.CALLCENTER_ESTADO_LLAMADA_OCUPADO:
          estadoLlamada = Sistema.CALLCENTER_ESTADO_LLAMADA_OCUPADO;
          break;
        case Sistema.CALLCENTER_ESTADO_LLAMADA_LOCKED:
          estadoLlamada = "LLAMANDO";
          break;
        case Sistema.CALLCENTER_ESTADO_LLAMADA_TAKED:
          estadoLlamada = "HABLANDO";
          break;
        case Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED:
        case "":
          estadoLlamada = "FINALIZADO";
          break;
        default:
          estadoLlamada = "DESCONOCIDO";
          break;
      }

      //construye queryString
      queryString += "op=GrabarEstadoLlamada";
      queryString += "&idLlamada=" + Sistema.urlEncode(idLlamada);
      queryString += "&idAlertaPadre=" + jQuery("#txtIdAlerta").val();
      queryString += "&idAlertaHija=" + jQuery("#txtIdAlertaHija").val();
      queryString += "&idEscalamientoPorAlertaGrupoContacto=" + idEscalamientoPorAlertaGrupoContacto;
      queryString += "&nombreContacto=" + Sistema.urlEncode(nombreContacto);
      queryString += "&telefono=" + Sistema.urlEncode(telefono);
      queryString += "&destinoLocal=" + Sistema.urlEncode(jQuery("#txtDestinoLocal").val());
      queryString += "&destinoLocalCodigo=" + Sistema.urlEncode(jQuery("#txtDestinoLocalCodigo").val());
      queryString += "&accion=" + Sistema.urlEncode(estadoLlamada);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener grabar el estado de la llamada");
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener grabar el estado de la llamada");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.grabarEstadoLlamada:\n" + e);
    }

  },

  cargarDatosFormulario_HistorialLlamadas: function (elem_json) {
    var json, nroEscalamiento;

    try {
      jQuery("#hCargando").html(Sistema.mensajeCargandoDatos());
      nroEscalamiento = jQuery("#txtNroEscalamiento").val();
      json = jQuery.parseJSON(elem_json);

      setTimeout(function () {
        jQuery("#hHistorialLlamadas").html(json.historialLlamadas);
        if (jQuery("#txtCargaInicialRealizada").val() == "1") {
          Sistema.moverScrollHastaControl("body");
        } else {
          if (nroEscalamiento != "-1") {
            Sistema.moverScrollHastaControl("hNroEscalamiento_" + nroEscalamiento);
          }
        }

        jQuery("#txtCargaInicialRealizada").val("1");
        jQuery("#hCargando").html("");
      }, 100);

    } catch (e) {
      alert("Exception Alerta.cargarDatosFormulario_HistorialLlamadas:\n" + e);
    }

  },

  verHistorialLlamadas: function (opciones) {
    var queryString = "";
    var fancy_width, fancy_height, fancy_overlayColor, sinAtender, asignadoA, fechaRecepcion, clasificacion, nroEscalamiento;

    try {
      if (opciones.fancy_width) { fancy_width = opciones.fancy_width } else { fancy_width = "100%" }
      if (opciones.fancy_height) { fancy_height = opciones.fancy_height } else { fancy_height = "100%" }
      if (opciones.fancy_overlayColor) { fancy_overlayColor = opciones.fancy_overlayColor } else { fancy_overlayColor = Sistema.FANCYBOX_OVERLAY_COLOR_DEFECTO }
      if (opciones.nroEscalamiento) { nroEscalamiento = opciones.nroEscalamiento } else { nroEscalamiento = "-1" }

      queryString += "?idAlerta=" + opciones.idAlerta;
      queryString += "&nroEscalamiento=" + nroEscalamiento;

      jQuery.fancybox({
        width: fancy_width,
        height: fancy_height,
        modal: false,
        type: "iframe",
        helpers: {
          overlay: {
            css: {
              background: fancy_overlayColor
            }
          }
        },
        href: "../page/Common.HistorialLlamadasAlerta.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Alerta.verHistorialLlamadas:\n" + e);
    }

  },

  validarFormularioBusqueda: function () {
    try {
      var fechaDesde = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaDesde").val();
      var fechaHasta = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaHasta").val();

      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtLocalDestino", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaDesde", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaHasta", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { valor_a_validar: fechaDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaDesde, horaInicio: "00:00", fechaTermino: fechaHasta, horaTermino: "23:59"} }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioBusqueda:\n" + e);
      return false;
    }
  },

  validarFuncionesContactosCargaInicial: function () {
    var idEscalamientoPorAlertaGrupoContacto, idEscalamientoPorAlertaContacto;

    try {
      //obtiene el listado de contactos seleccionados
      jQuery(".observacion-contacto-escalamiento").each(function (indice, obj) {
        var elem_txtObservacion = jQuery(obj).attr("id");
        var prefijoControl = elem_txtObservacion.replace("txtObservacion", "");
        var elem_ddlContacto = prefijoControl + "ddlContacto";

        idEscalamientoPorAlertaGrupoContacto = prefijoControl.replace("_", "");
        idEscalamientoPorAlertaContacto = jQuery("#" + elem_ddlContacto).val();

        Alerta.reemplazarMarcasEspecialesScript({ prefijoControl: idEscalamientoPorAlertaGrupoContacto });
      });

    } catch (e) {
      alert("Exception Alerta.validarFuncionesContactosCargaInicial:\n" + e);
    }
  },

  filtrarComboExplicacion: function (opciones) {
    var json, prefijoControl, filtro;
    var options = [];

    try {
      prefijoControl = opciones.prefijoControl;
      filtro = opciones.filtro;
      var txtJSONExplicacion = jQuery("#" + prefijoControl + "_txtJSONExplicacion");
      var select = jQuery("#" + prefijoControl + "_ddlExplicacion");

      if (jQuery(txtJSONExplicacion).val() == "") {
        jQuery(select).find("option").each(function () {
          options.push({
            value: jQuery(this).val(),
            text: jQuery(this).text(),
            data_frecuencia: jQuery(this).attr("data-frecuencia"),
            data_tipoExplicacion: jQuery(this).attr("data-tipoExplicacion")
          });
        });
        jQuery(txtJSONExplicacion).val(Sistema.convertirJSONtoString(options));
      } else {
        json = jQuery(txtJSONExplicacion).val();
        options = jQuery.parseJSON(json);
      }

      jQuery(select).data("options", options);

      options = jQuery(select).empty().data("options");
      var search = filtro;
      var regex = new RegExp(search, "gi");

      if (filtro != "") {
        jQuery(select).append("<option value=\"\" data-frecuencia=\"-1\" data-tipoExplicacion=\"Normal\">--Seleccione--</option>");
      }

      jQuery.each(options, function (i) {
        var option = options[i];
        if (option.text.match(regex) !== null) {
          jQuery(select).append("<option value=\"" + option.value + "\" data-frecuencia=\"" + option.data_frecuencia + "\" data-tipoExplicacion=\"" + option.data_tipoExplicacion + "\">" + option.text + "</option>");
        }
      });

    } catch (e) {
      alert("Exception Alerta.filtrarComboExplicacion:\n" + e);
    }
  },

  reactivarAsignacionAlertas_TeleOperador: function () {
    var auxiliar, mensaje;
    var template = "";

    try {
      auxiliar = jQuery("#txtAuxiliar").val();

      template += "<div class=\"form-group row\">";
      template += "  <div class=\"col-xs-2\">";
      template += "    <img src=\"../img/reloj.png\" alt=\"\" />";
      template += "  </div>";
      template += "  <div class=\"col-xs-10\">";
      template += "    <div class=\"sist-font-size-30\">";
      template += "      <div>Auxiliar: <strong class=\"text-danger\">{AUXILIAR}</strong></div>";
      template += "      <div>Tiempo: <strong class=\"text-danger\"><span id=\"lblTiempoEstadoAuxiliar\">00h : 00m : 00s</span></strong></div>";
      template += "    </div>";
      template += "  </div>";
      template += "</div>";
      template += "<br />";
      template += "<div>";
      template += "  <div class=\"form-group sist-font-size-20\"><em>Para finalizar el estado auxiliar debe ingresar su clave</em></div>";
      template += "  <div class=\"row sist-font-size-20\">";
      template += "    <div class=\"col-xs-2\">";
      template += "      <strong>Clave:</strong>";
      template += "    </div>";
      template += "    <div class=\"col-xs-10\">";
      template += "      <div>";
      template += "        <input type=\"password\" id=\"modalAuxiliar_txtClave\" class=\"form-control\" >";
      template += "        <div id=\"modalAuxiliar_hErrorClave\"></div>";
      template += "      </div>";
      template += "    </div>";
      template += "  </div>";
      template += "</div>";

      mensaje = template.replace("{AUXILIAR}", auxiliar);
      bootbox.dialog({
        closeButton: false,
        message: mensaje,
        title: "<strong>EN ESTOS MOMENTOS ESTOY ...</strong>",
        buttons: {
          success: {
            label: "Finalizar auxiliar",
            className: "btn-success btn-lg",
            callback: function () {
              return (Alerta.validarFormularioEstadoAuxiliar_Teleoperador())
            }
          }

        }
      });

    } catch (e) {
      alert("Exception Alerta.reactivarAsignacionAlertas_TeleOperador:\n" + e);
    }
  },

  validarFormularioEstadoAuxiliar_Teleoperador: function () {
    var esValidaClave, clave, item;

    try {
      esValidaClave = Alerta.validarClaveUsuarioConectado();

      var jsonValidarCampos = [
        { elemento_a_validar: "modalAuxiliar_txtClave", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que la clave ingresada corresponda a la del usuario conectado
      if (jQuery("#modalAuxiliar_txtClave").val() != "") {
        item = {
          valor_a_validar: esValidaClave,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "modalAuxiliar_hErrorClave",
          mensaje_validacion: "La clave no es v&aacute;lida"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        jQuery("#txtAuxiliar").val("");
        jQuery("#modalAuxiliar_hErrorClave").addClass("sist-font-size-16");
        jQuery("#modalAuxiliar_hErrorClave").html(Sistema.mensajeCargandoDatos("Clave correcta, espere un momento por favor ..."));
        clearInterval(Alerta.SETINTERVAL_CRONOMETRO_AUXILIAR);
        Alerta.obtenerAlertasPorAtender({ cargaDatos: "update", revisarAlertaAsignadaPreviamente: true });
        Alerta.SETINTERVAL_ALERTAS_POR_ATENDER = setInterval(function () { Alerta.obtenerAlertasPorAtender({ cargaDatos: "update", revisarAlertaAsignadaPreviamente: false }); }, Alerta.SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_ATENDER);
        return true;
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioEstadoAuxiliar_Teleoperador:\n" + e);
      return false;
    }
  },

  validarClaveUsuarioConectado: function () {
    var queryString = "";
    var clave, retorno;

    try {
      clave = jQuery("#modalAuxiliar_txtClave").val();

      //construye queryString
      queryString += "op=ValidarClaveUsuarioConectado";
      queryString += "&clave=" + Sistema.urlEncode(clave);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else {
          retorno = respuesta;
        }
      }
      var errFunc = function (t) {
        retorno = "-1";
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Alerta.validarClaveUsuarioConectado:\n" + e);
      retorno = "-1";
    }

    return retorno;
  },

  dibujarListadoAuxiliares: function (json) {
    var template = "";
    var templateAux;

    try {
      template = "<li><a href=\"javascript:;\" onclick=\"Alerta.desvincular_TeleOperador({ auxiliar: '{NOMBRE}', accion: '{ACCION}' })\">{NOMBRE}</a></li>";

      jQuery("#ulAuxiliares").empty();
      jQuery.each(json, function (indice, obj) {
        templateAux = template;
        templateAux = templateAux.replace(/{NOMBRE}/ig, obj.Nombre);
        templateAux = templateAux.replace(/{ACCION}/ig, obj.Accion);
        jQuery("#ulAuxiliares").append(templateAux);
      });
    } catch (e) {
      alert("Exception Alerta.dibujarListadoAuxiliares:\n" + e);
    }
  },

  mostrarMensajeEstadoAuxiliar: function (opciones) {
    var fecha, arrFecha, anio, mes, dia, horas, minutos, segundos;

    try {
      fecha = opciones.fecha;
      if (fecha == "") {
        fecha = new Date();
        anio = fecha.getFullYear();
        mes = fecha.getMonth() + 1;
        dia = fecha.getDate();
        horas = fecha.getHours();
        minutos = fecha.getMinutes();
        segundos = fecha.getSeconds();
      } else {
        arrFecha = fecha.split(/[\/ :-]/);
        anio = arrFecha[2];
        mes = arrFecha[1];
        dia = arrFecha[0];
        horas = arrFecha[3];
        minutos = arrFecha[4];
        segundos = 0;
      }

      Alerta.reactivarAsignacionAlertas_TeleOperador();
      setTimeout(function () {
        var fecha = new Date();
        var opciones_cronometro = {
          year: anio,
          month: mes,
          day: dia,
          hours: horas,
          minutes: minutos,
          seconds: segundos,
          elem_cronometro: "lblTiempoEstadoAuxiliar"
        };

        Sistema.cronometro(opciones_cronometro);
        Alerta.SETINTERVAL_CRONOMETRO_AUXILIAR = setInterval(function () { Sistema.cronometro(opciones_cronometro); }, 1000);
      }, 100);
    } catch (e) {
      alert("Exception Alerta.mostrarMensajeEstadoAuxiliar:\n" + e);
    }
  },

  mostrarClasificacionTipoObservacion: function () {
    var queryString = "";
    var clasificacion;
    var elem_tipoObservacion = "ddlTipoObservacion";
    var elem_divDescTipoObservacion = "divDescTipoObservacion";
    var html = ""
    try {

      clasificacion = jQuery("#" + elem_tipoObservacion + " option:selected").attr("data-clasificacion");

      if (clasificacion) {
        html = "Alerta clasificada como: ";
        html = html + "<span class= \"alert alert-danger sist-padding-label-prioridad sist-margin-cero\">";
        html = html + clasificacion.toUpperCase();
        html = html + "</span>";
      }

      jQuery("#" + elem_divDescTipoObservacion).html(html);

    } catch (e) {
      alert("Exception Alerta.mostrarClasificacionTipoObservacion:\n" + e);
    }
  },

  validarFormularioBuscadorRespuestaAlertaRoja: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioBuscadorRespuestaAlertaRoja:\n" + e);
      return false;
    }
  },

  abrirFormularioRespuestaAlertaRoja: function (opciones) {
    try {
      var queryString = "";
      var idAlertaRojaEstadoActual = opciones.idAlertaRojaEstadoActual;
      var nroTransporte = opciones.nroTransporte;
      var nombreTransportista = opciones.nombreTransportista;
      var rutTransportista = opciones.rutTransportista;

      queryString += "?idAlertaRojaEstadoActual=" + idAlertaRojaEstadoActual;
      queryString += "&nroTransporte=" + nroTransporte;
      queryString += "&nombreTransportista=" + Sistema.urlEncode(nombreTransportista);
      queryString += "&rutTransportista=" + Sistema.urlEncode(rutTransportista);

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/OT.RespuestaAlertaRojaDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Alerta.abrirFormularioRespuestaAlertaRoja:\n" + e);
    }
  },

  validarFormularioRespuestaAlertaRoja: function () {
    var montoUnoTransportista, montoDosTransportista, item, esValidoMontoDos;

    try {
      if (jQuery("#hRechazado").val() == 0) {
        montoUnoTransportista = jQuery("#txtMontoUnoTransportista").val();
        montoDosTransportista = jQuery("#txtMontoDosTransportista").val();

        var jsonValidarCampos = [
            { elemento_a_validar: "msEstado_txtIdsHija", requerido: true, contenedor_mensaje_validacion: "lblMensajeErrormsEstado", mensaje_validacion: "Debe seleccionar un Estado" }
          , { elemento_a_validar: "txtMontoUnoTransportista", requerido: false, tipo_validacion: "numero-entero-positivo" }
          , { elemento_a_validar: "txtMontoDosTransportista", requerido: false, tipo_validacion: "numero-entero-positivo" }
          , { elemento_a_validar: "txtObservacion", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        ];

        //valida que montoDos no sea mayor a montoUno
        esValidoMontoDos = 1; //se inicializa que el valor montoDos es valido
        if (!Validacion.tipoVacio(montoUnoTransportista) && !Validacion.tipoVacio(montoDosTransportista)) {
          if (Validacion.tipoNumeroEnteroPositivo(montoUnoTransportista) && Validacion.tipoNumeroEnteroPositivo(montoDosTransportista)) {
            if (montoDosTransportista > montoUnoTransportista) {
              esValidoMontoDos = -1; // indica que valor no es valido
            } else {
              esValidoMontoDos = 1; // indica que valor no es valido            
            }
          }
        }
        item = {
          valor_a_validar: esValidoMontoDos,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hMensajeErrorMontoDosTransportista",
          mensaje_validacion: "No puede ser mayor al Monto a Recuperar"
        };
        jsonValidarCampos.push(item);
        //-----------------------------------

        var esValido = Validacion.validarControles(jsonValidarCampos);

        if (!esValido) {
          Sistema.alertaMensajeError();
          return false;
        } else {
          return true;
        }



      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioRespuestaAlertaRoja:\n" + e);
      return false;
    }
  },

  expandirContraer: function (opciones) {
    var elem_holderComentario, visible;

    try {
      elem_holderComentario = opciones.elem_holderComentario;
      idAlerta = opciones.idAlerta;

      jQuery("#" + elem_holderComentario).slideToggle(function () {
        visible = jQuery("#" + elem_holderComentario).is(":visible");
        if (visible) {
          jQuery("#" + idAlerta + "_btnVer").attr("class", "btn btn-danger btn-sm sist-width-100-porciento");
          jQuery("#" + idAlerta + "_btnVer").html("<span class=\"glyphicon glyphicon-remove\"></span>&nbsp; Cerrar");
        } else {
          jQuery("#" + idAlerta + "_btnVer").attr("class", "btn btn-success btn-sm sist-width-100-porciento");
          jQuery("#" + idAlerta + "_btnVer").html("<span class=\"glyphicon glyphicon-eye-open\"></span>&nbsp; Ver respuesta");
        }

      });

    } catch (e) {
      alert("Exception Alerta.expandirContraer:\n" + e);
    }
  },

  abrirFormularioDetalleAlertasRojas: function (opciones) {
    try {
      var queryString = "";
      var nombreTransportista = opciones.nombreTransportista;
      var gestionadas = opciones.gestionadas;
      var rutTransportista = opciones.rutTransportista;

      queryString += "?nombreTransportista=" + Sistema.urlEncode(nombreTransportista);
      queryString += "&gestionadas=" + gestionadas;
      queryString += "&rutTransportista=" + Sistema.urlEncode(rutTransportista);

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/OT.AlertaRojaDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Alerta.abrirFormularioMantenedor:\n" + e);
    }
  },

  activarComboExplicacionPorTelefono: function (opciones) {
    var prefijoControl, telefono;

    try {
      prefijoControl = opciones.prefijoControl;
      telefono = jQuery("#" + prefijoControl + "_lblTelefono").html();

      if (Validacion.tipoNumeroEnteroPositivoMayorCero(telefono)) {
        jQuery("#" + prefijoControl + "_ddlExplicacion").attr("disabled", true);
      } else {
        jQuery("#" + prefijoControl + "_ddlExplicacion").attr("disabled", false);
      }

    } catch (e) {
      alert("Exception Alerta.activarComboExplicacionPorTelefono:\n" + e);
    }
  },

  validarFormularioAlertaRojaPorCerrar: function () {
    try {

      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtRegistrosPorPagina", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioBusqueda:\n" + e);
      return false;
    }
  },

  estaOcupadoInconcert: function (opciones) {
    var idLlamada, barAgent, mensaje, finalizarLlamadaModoManual, estaOcupado, idEscalamientoPorAlertaGrupoContacto;
    var currentIntStateByID = "-1";

    try {
      idEscalamientoPorAlertaGrupoContacto = opciones.idEscalamientoPorAlertaGrupoContacto;

      if (Sistema.SIMULAR_ACTIVEX) {
        barAgent = Sistema.simularActiveX();
        idLlamada = barAgent.GetIdCall;
        currentIntStateByID = barAgent.CurrentIntStateByID;
      } else {
        barAgent = new ActiveXObject("BarAgentControl.BAControl");
        idLlamada = barAgent.GetIdCall();
        currentIntStateByID = barAgent.CurrentIntStateByID(idLlamada);
      }

      //si el estado del control es igual a TAKED o ENDED, indica que esta ocupado
      if (currentIntStateByID == Sistema.CALLCENTER_ESTADO_LLAMADA_TAKED || currentIntStateByID == Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED) {
        estaOcupado = true;
      } else {
        estaOcupado = false;
      }

      //si la llamada cambia de estado entonces se graba el nuevo estado
      if (estaOcupado) {
        Alerta.grabarEstadoLlamada({ estadoLlamada: Sistema.CALLCENTER_ESTADO_LLAMADA_OCUPADO, idLlamada: idLlamada, idEscalamientoPorAlertaGrupoContacto: idEscalamientoPorAlertaGrupoContacto });
      }

    } catch (e) {
      estaOcupado = false;
    }

    return estaOcupado;
  },

  validarFormularioBusquedaDefinicionAlerta: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Alerta.validarFormularioBusquedaDefinicionAlerta\n" + e);
      return false;
    }
  },

  abrirFormularioDefinicionAlerta: function (opciones) {
    try {
      var queryString = "";
      var idAlertaDefinicion = opciones.idAlertaDefinicion;

      queryString += "?id=" + idAlertaDefinicion;

      jQuery.fancybox({
        width: "100%",
        height: "90%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.DefinicionAlertaDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Alerta.abrirFormularioDefinicionAlerta:\n" + e);
    }
  },

  validarFormularioDefinicionAlerta: function () {
    var item;

    try {
      var existeNombreAlerta = Alerta.existeNombreDefinicionAlerta();

      var jsonValidarCampos = [
        { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que el nombre de la Formato sea unico
      if (jQuery("#txtNombre").val() != "") {
        item = {
          valor_a_validar: existeNombreAlerta,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombre",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Alerta.validarFormularioDefinicionAlerta\n" + e);
      return false;
    }

  },

  existeNombreDefinicionAlerta: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var nombreAlerta = jQuery("#txtNombre").val();
      var idAlertaDefinicion = jQuery("#txtIdAlertaDefinicion").val();
      queryString += "op=Alerta.NombreDefinicionAlerta";
      queryString += "&id=" + idAlertaDefinicion;
      queryString += "&valor=" + Sistema.urlEncode(nombreAlerta);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único de la definición de alerta");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único de la definición de alerta");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Alerta.existeNombreDefinicionAlerta:\n" + e);
    }

    return existe;
  },

  eliminarRegistroDefinicionAlerta: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarDefinicionAlerta";
      queryString += "&idAlertaDefinicion=" + Sistema.urlEncode(opciones.idAlertaDefinicion);

      if (confirm("Si elimina la definición de la alerta se eliminarán todas las referencias asociadas a ellas y no se podrá volver a recuperar.\n¿Desea eliminar la definición de alerta?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waAlerta.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Alerta.eliminarRegistroMantenedor\n" + e);
    }
  },

  validarEliminarDefinicionAlerta: function () {
    try {
      if (confirm("Si elimina la definición de la alerta se eliminarán todas las referencias asociadas a ellas y no se podrá volver a recuperar.\n¿Desea eliminar la definición de alerta?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: Alerta.validarEliminarDefinicionAlerta\n" + e);
      return false;
    }
  },

  activarControlesPorFormato: function (opciones) {
    var idFormato, idFormatoActual, jsonEscalamientos, realizarCambio;

    try {
      idFormato = jQuery("#" + opciones.elem_contenedor).val();
      idFormatoActual = jQuery("#txtIdFormatoActual").val();

      if (idFormato != idFormatoActual) {
        jsonEscalamientos = jQuery("#txtJSONEscalamientos").val();
        jsonEscalamientos = jQuery.parseJSON(jsonEscalamientos);

        if (jsonEscalamientos.length == 0) {
          realizarCambio = true;
        } else {
          realizarCambio = confirm("Si cambia el Formato perderá toda la configuración de los escalamientos realizada\n¿Desea cambiar el Formato?");
        }
      } else {
        realizarCambio = false;
      }

      if (realizarCambio) {
        //limpia los controles de los escalamientos
        jQuery("#txtJSONEscalamientos").val("[]");
        jQuery("#txtTotalGrupos").val(0);
        jQuery("#txtJSONPadreHijo").val("[]");
        jQuery("#hEscalamientos").html("");
        jQuery("#txtIdFormatoActual").val(idFormato);
      } else {
        jQuery("#" + opciones.elem_contenedor).select2("val", idFormatoActual);
      }

      Alerta.cambiarComboDefinicionAlerta(opciones);
      Alerta.obtenerPerfilesPorFormato(opciones);
    } catch (e) {
      alert("Exception: Alerta.activarControlesPorFormato\n" + e);
    }
  },

  cambiarComboDefinicionAlerta: function (opciones) {
    var id = jQuery("#" + opciones.elem_contenedor).val();
    var idAlertaDefinicionPorFormato = opciones.idAlertaDefinicionPorFormato.toString();
    var enabled = opciones.enabled;

    try {
      var combosRelacionados = [
                                {
                                  elem_contenedor: "ddlNombre",
                                  tipoCombo: "AlertaDefinicionPorFormato",
                                  multiple: false,
                                  placeholder: "",
                                  item_cabecera: "Seleccione",
                                  valores_seleccionados: [idAlertaDefinicionPorFormato],
                                  onChange: "",
                                  fuenteDatos: "Tabla",
                                  data: [],
                                  filtro: id,
                                  enabled: enabled
                                }
                             ];

      jQuery.each(combosRelacionados, function (indice, obj) {
        Combo.dibujarHTML(obj);
      });

    } catch (e) {
      alert("Exception: Alerta.cambiarComboDefinicionAlerta\n" + e);
    }
  },

  obtenerPerfilesPorFormato: function (opciones) {
    var queryString = "";
    var idFormato;

    try {
      idFormato = jQuery("#" + opciones.elem_contenedor).val();

      //construye queryString
      queryString += "op=ObtenerPerfilesPorFormato";
      queryString += "&idFormato=" + Sistema.urlEncode(idFormato);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener los perfiles por formato");
        } else {
          jQuery("#txtJSONPerfilesPorFormato").val(respuesta);
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener los perfiles por formato");
      }
      jQuery.ajax({
        url: "../webAjax/waAlerta.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception: Alerta.obtenerPerfilesPorFormato\n" + e);
    }
  },

  rechazarRespuesta: function (opciones) {
    var json = [];
    var item = {};

    try {
      if (Alerta.validarFormularioRespuestaRechazo(opciones)) {
        if (confirm("¿Está seguro de rechazar la respuesta?")) {
          Alerta.ocultarControlesRechazo(opciones);

          var jsonRechazo = jQuery("#hJsonRechazado").val();
          jsonRechazo = jQuery.parseJSON(jsonRechazo);

          var respuesta = jQuery("#txtObservacionR_" + opciones.idAlerta).val();
          item = {
            idAlerta: opciones.idAlerta,
            respuesta: respuesta
          };
          jsonRechazo.push(item);

          jQuery("#hJsonRechazado").val(Sistema.convertirJSONtoString(jsonRechazo));
        }
      }

    } catch (e) {
      alert("Exception: Alerta.rechazarRespuesta\n" + e);
    }
  },

  validarFormularioRespuestaRechazo: function (opciones) {
    try {
      var txtRespuestaRechazo = opciones.control
      var jsonValidarCampos = [
         { elemento_a_validar: txtRespuestaRechazo, requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception Alerta.validarFormularioRespuestaRechazo:\n" + e);
      return false;
    }
  },

  ocultarControlesRechazo: function (opciones) {
    try {
      //Oculto solo si existe respuesta rechazada
      jQuery("#hRechazado").val(1);
      jQuery("#pnlEstadoTransportista").hide();
      jQuery("#txtObservacionR_" + opciones.idAlerta).prop("disabled", true);
      jQuery("#btnRechazar_" + opciones.idAlerta).hide();
    } catch (e) {
      alert("Exception: Alerta.ocultarControlesRechazo\n" + e);
    }
  },

  obtenerIconoReportabilidadGPS: function (opciones) {
    var tipo, patente, estadoReportabilidad, fechaReportabilidad, imagen, classTexto, estado, fecha;
    var template = "";

    try {
      template += "<div><strong>{PATENTE}</strong></div>";
      template += "<img src=\"../img/{IMAGEN}\" alt=\"\" />";
      template += "<div class=\"{CLASS_TEXTO}\"><strong>{ESTADO}</strong></div>";
      template += "<div class=\"small\">{FECHA}</div>";

      tipo = opciones.tipo;
      patente = opciones.patente;
      estadoReportabilidad = opciones.estadoReportabilidad;
      fechaReportabilidad = opciones.fechaReportabilidad;

      switch (tipo) {
        case "Tracto":
          switch (Sistema.toUpper(estadoReportabilidad)) {
            case Alerta.REPORTABILIDAD_GPS_ONLINE:
              imagen = "truck_azul_ok.png";
              classTexto = "text-success";
              estado = "Reportando";
              fecha = fechaReportabilidad;
              break;
            case Alerta.REPORTABILIDAD_GPS_OFFLINE:
              imagen = "truck_azul_error.png";
              classTexto = "text-danger";
              estado = "No Reportando";
              fecha = fechaReportabilidad;
              break;
            case Alerta.REPORTABILIDAD_GPS_NO_INTEGRADA:
              imagen = "truck_azul_nointegrada.png";
              classTexto = "";
              estado = "No Integrada";
              fecha = "";
              break;
          }

          template = template.replace(/{PATENTE}/ig, patente);
          template = template.replace(/{IMAGEN}/ig, imagen);
          template = template.replace(/{CLASS_TEXTO}/ig, classTexto);
          template = template.replace(/{ESTADO}/ig, estado);
          template = template.replace(/{FECHA}/ig, fecha);
          break;
        case "Trailer":
          switch (Sistema.toUpper(estadoReportabilidad)) {
            case Alerta.REPORTABILIDAD_GPS_ONLINE:
              imagen = "trailer_ok.png";
              classTexto = "text-success";
              estado = "Reportando";
              fecha = fechaReportabilidad;
              break;
            case Alerta.REPORTABILIDAD_GPS_OFFLINE:
              imagen = "trailer_error.png";
              classTexto = "text-danger";
              estado = "No Reportando";
              fecha = fechaReportabilidad;
              break;
            case Alerta.REPORTABILIDAD_GPS_NO_INTEGRADA:
              imagen = "trailer_nointegrada.png";
              classTexto = "";
              estado = "No Integrada";
              fecha = "";
              break;
          }

          template = template.replace(/{PATENTE}/ig, patente);
          template = template.replace(/{IMAGEN}/ig, imagen);
          template = template.replace(/{CLASS_TEXTO}/ig, classTexto);
          template = template.replace(/{ESTADO}/ig, estado);
          template = template.replace(/{FECHA}/ig, fecha);
          break;
      }
    } catch (e) {
      alert("Exception: Alerta.obtenerIconoReportabilidadGPS\n" + e);
      template = "";
    }

    return template;
  }

};

