﻿var PoolPlaca = {
  validarFormularioCarga: function () {
    try {
      var jsonValidarCampos = [
		    { elemento_a_validar: Sistema.PREFIJO_CONTROL + "fileCarga", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        bootbox.dialog({
          closeButton: false,
          title: "<strong class=\"sist-font-size-24\">Validar Archivo</strong>",
          message: "<span class=\"sist-font-size-16\">¿Seguro que desea validar el archivo a cargar?</span>",
          buttons: {
            cancel: {
              label: "Cancelar",
              className: "btn-default btn-lg",
              callback: function () {
                //cierra la ventana modal
              }
            },
            success: {
              label: "Aceptar",
              className: "btn-success btn-lg",
              callback: function () {
                jQuery("#" + Sistema.PREFIJO_CONTROL + "hBotonValidar").hide();
                jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeBotonValidar").show();

                setTimeout(function () {
                  __doPostBack('ctl00$cphContent$btnValidar', '');
                }, 100);
              }
            }

          }
        });
        return false;
      }

    } catch (e) {
      alert("Exception: PoolPlaca.validarFormularioCarga\n" + e);
      return false;
    }
  },

  confirmar: function (opciones) {
    var titulo, mensaje, btnPostBack, mensajeBotones;

    try {
      switch (Sistema.toLower(opciones.tipo)) {
        case "grabar":
          titulo = "Grabar Datos";
          mensaje = "¿Seguro que desea grabar el pool de placas?";
          btnPostBack = "ctl00$cphContent$btnGrabar";
          mensajeBotones = "<em>Guardando informaci&oacute;n, espere un momento ...</em>"
          break;
        default:
          titulo = "Cancelar";
          mensaje = "¿Seguro que desea cancelar la carga y perder la informaci&oacute;n?";
          btnPostBack = "ctl00$cphContent$btnCancelar";
          mensajeBotones = "<em>Cancelado la carga, espere un momento ...</em>"
          break;
      }

      bootbox.dialog({
        closeButton: false,
        title: "<strong class=\"sist-font-size-24\">" + titulo + "</strong>",
        message: "<span class=\"sist-font-size-16\">" + mensaje + "</span>",
        buttons: {
          cancel: {
            label: "Cancelar",
            className: "btn-default btn-lg",
            callback: function () {
              //cierra la ventana modal
            }
          },
          success: {
            label: "Aceptar",
            className: "btn-success btn-lg",
            callback: function () {
              jQuery("#" + Sistema.PREFIJO_CONTROL + "pnlBotones").hide();
              jQuery("#" + Sistema.PREFIJO_CONTROL + "lblMensajeBotones").html(mensajeBotones).show();

              setTimeout(function () {
                __doPostBack(btnPostBack, '');
              }, 100);
            }
          }

        }
      });
    } catch (e) {
      alert("Exception: PoolPlaca.confirmar\n" + e);
    }

    return false;
  }

};