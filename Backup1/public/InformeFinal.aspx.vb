﻿Imports CapaNegocio

Public Class InformeFinal
  Inherits System.Web.UI.Page

  Private Function ObtenerHTMLItem(ByVal ds As DataSet, ByVal templatePlantilla As String) As String
    Dim templateTabla, templateDetalle, htmlTabla, htmlDetalle, html As String
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim drv As DataRowView
    Dim dsFiltrado As New DataSet
    Dim sbGeneral, sb As New StringBuilder
    Dim htmlContenido As String = ""
    Dim idAlerta, nroTransporte, patenteTracto, nombreConductor, nombreAlerta, patenteTrailer, localOrigenCodigo, localOrigenDescripcion As String
    Dim localDestinoCodigo, localDestinoDescripcion, tipoViaje, fechaHoraCreacion, FechaGestion, categoriaAlerta, TipoObservacion As String
    Dim observacion, montoDiscrepancia, alertaRojaEstadoTransportista, alertaRojaRespuestaObservacion, alertaRojaRespuestaFechaCreacion As String
    Dim rechazo As Boolean

    Try
      html = templatePlantilla

      '-------------------------------------------------------------------------------------------
      ' GENERAL
      '-------------------------------------------------------------------------------------------
      html = html.Replace("{$URL_BASE}", Utilidades.ObtenerUrlBase())

      'htmlTabla = templateTabla

      dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T00_ListadoAlertas)
      If (dt.Rows.Count > 0) Then
        dr = dt.Rows(0)

        nroTransporte = dr("NroTransporte")
        montoDiscrepancia = dr("MontoDiscrepancia")

        html = html.Replace("{$NRO_TRANSPORTE}", nroTransporte)
        html = html.Replace("{$MONTO_DISCREPANCIA}", montoDiscrepancia)
      End If

      'ALERTAS
      alertaRojaEstadoTransportista = ""
      dv = ds.Tables(Alerta.eTablaRespuestaAlerta.T04_EstadoTransportista).DefaultView
      dv.RowFilter = "Aprobada = 1"
      For Each drv In dv
        alertaRojaEstadoTransportista &= "- " & drv.Item("Texto") & "<br/>"
      Next
      dv.RowFilter = ""

      For Each dr In ds.Tables(0).Rows
        templateTabla = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_tabla -->", "<!-- end_tabla -->")
        htmlTabla = templateTabla

        'limpiar
        sb.Length = 0

        idAlerta = dr.Item("idAlerta")
        nombreAlerta = dr.Item("nombreAlerta")
        nombreConductor = dr.Item("nombreConductor")
        patenteTracto = dr.Item("patenteTracto")
        patenteTrailer = dr.Item("patenteTrailer")
        localOrigenCodigo = dr.Item("localOrigenCodigo")
        localOrigenDescripcion = dr.Item("localOrigenDescripcion")
        localDestinoCodigo = dr.Item("localDestinoCodigo")
        localDestinoDescripcion = dr.Item("localDestinoDescripcion")
        tipoViaje = dr.Item("tipoViaje")
        fechaHoraCreacion = dr.Item("fechaHoraCreacion")
        'alertaRojaEstadoTransportista = "" 'dr.Item("AlertaRojaEstadoTransportista")

        'construye el html del item
        templateDetalle = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_row_table1 -->", "<!-- end_row_table1 -->")
        htmlDetalle = templateDetalle

        'reemplaza marcas especiales
        htmlDetalle = htmlDetalle.Replace("{$ID_ALERTA}", idAlerta)
        htmlDetalle = htmlDetalle.Replace("{$ALERTA}", nombreAlerta)
        htmlDetalle = htmlDetalle.Replace("{$FECHA_ALERTA}", fechaHoraCreacion)
        htmlDetalle = htmlDetalle.Replace("{$CONDUCTOR}", nombreConductor)
        htmlDetalle = htmlDetalle.Replace("{$PATENTE}", patenteTracto)
        htmlDetalle = htmlDetalle.Replace("{$LOCAL_ORIGEN}", localOrigenCodigo)
        htmlDetalle = htmlDetalle.Replace("{$LOCAL_DESTINO}", localDestinoCodigo)
        htmlDetalle = htmlDetalle.Replace("{$TIPO_VIAJE}", tipoViaje)
        htmlDetalle = htmlDetalle.Replace("{$PLAN_ACCION}", alertaRojaEstadoTransportista)

        sb.AppendLine(htmlDetalle)

        htmlTabla = htmlTabla.Replace(templateDetalle, sb.ToString())

        dt = ds.Tables(1)
        dv = dt.DefaultView
        'filtra el contenido por el idAlerta
        dv.RowFilter = "IdAlerta='" & idAlerta & "'"

        'limpiar
        sb.Length = 0

        For Each drv In dv
          sb.Length = 0

          FechaGestion = drv.Item("FechaGestion")
          categoriaAlerta = drv.Item("categoriaAlerta")
          TipoObservacion = drv.Item("TipoObservacion")
          observacion = drv.Item("observacion")

          'construye el html del item
          templateDetalle = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_row_table2 -->", "<!-- end_row_table2 -->")
          htmlDetalle = templateDetalle

          'reemplaza marcas especiales
          htmlDetalle = htmlDetalle.Replace("{$FECHA_GESTION}", FechaGestion)
          htmlDetalle = htmlDetalle.Replace("{$CATEGORIA}", categoriaAlerta)
          htmlDetalle = htmlDetalle.Replace("{$TIPO_OBSERVACION}", TipoObservacion)
          htmlDetalle = htmlDetalle.Replace("{$OBSERVACION}", observacion)

          'agrego detalle
          sb.AppendLine(htmlDetalle)

        Next

        htmlTabla = htmlTabla.Replace(templateDetalle, sb.ToString())

        'limpiar
        sb.Length = 0
        dv.RowFilter = ""

        '----------------------------------------------------------------------------------
        'Observación
        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T05_RespuestaTransportista)
        dv = dt.DefaultView
        dv.RowFilter = "IdAlerta='" & idAlerta & "'"
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        For Each drRespuesta As DataRow In dsFiltrado.Tables(0).Rows
          alertaRojaRespuestaObservacion = drRespuesta.Item("Observacion")
          alertaRojaRespuestaFechaCreacion = drRespuesta.Item("FechaCreacion")
          rechazo = drRespuesta.Item("Rechazo")

          'formatea valores
          alertaRojaRespuestaObservacion = alertaRojaRespuestaObservacion & IIf(rechazo, " <span style=""color:#f00""><b>(Rechazo)</b></span>", "")

          'construye el html del item
          templateDetalle = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_row_table4 -->", "<!-- end_row_table4 -->")
          htmlDetalle = templateDetalle

          'reemplaza marcas especiales
          htmlDetalle = htmlDetalle.Replace("{$RESPUESTA_ALERTA}", alertaRojaRespuestaObservacion)
          htmlDetalle = htmlDetalle.Replace("{$RESPUESTA_HORA}", alertaRojaRespuestaFechaCreacion)
          htmlDetalle = htmlDetalle.Replace("<tr valign=""top"" style=""display:none"">", "<tr valign=""top"">")

          'agrego detalle
          sb.AppendLine(htmlDetalle)
        Next
        htmlTabla = htmlTabla.Replace("{$LISTADO_RESPUESTAS_TRANSPORTISTA}", sb.ToString())

        'limpiar
        sb.Length = 0
        '----------------------------------------------------------------------------------

        sbGeneral.AppendLine(htmlTabla)
      Next

      ' Reemplazo general
      templateTabla = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_tabla -->", "<!-- end_tabla -->")
      htmlTabla = templateTabla

      html = html.Replace(templateTabla, sbGeneral.ToString())

      'Reemplazo acentos
      html = Herramientas.ReemplazarAcentos(html)

      Me.ltlContenido.Text = html

      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim rutaPlantilla, templatePlantilla As String
    Dim htmlItem As String
    Dim ds As New DataSet
    Dim sb As New StringBuilder
    Dim urlBase As String
    Dim idAlertaRojaEstadoActual As String
    Dim nroTransporte As String

    Try
      urlBase = Utilidades.ObtenerUrlBase()
      rutaPlantilla = Server.MapPath("../template/InformeFinal.template.html")
      If Not Archivo.ExisteArchivoEnDisco(rutaPlantilla) Then
        templatePlantilla = ""
      Else
        templatePlantilla = Archivo.ObtenerStringPlantilla(rutaPlantilla)
      End If

      Dim argCrypt As String = Request.QueryString("a")
      Dim textoDesencriptado As String = Criptografia.DesencriptarTripleDES(argCrypt)
      Dim arrParametros As Array = Criptografia.ObtenerArrayQueryString(textoDesencriptado)

      idAlertaRojaEstadoActual = Criptografia.RequestQueryString(arrParametros, "idAlertaRojaEstadoActual")
      nroTransporte = Criptografia.RequestQueryString(arrParametros, "nroTransporte")

      ds = Alerta.ObtenerDetalleRespuestaAlertaRoja(nroTransporte, idAlertaRojaEstadoActual, Alerta.ENTIDAD_ALERTA_ROJA_RESPUESTA)

      If Not ds Is Nothing Then

        htmlItem = ObtenerHTMLItem(ds, templatePlantilla)

        sb.AppendLine(htmlItem)

        Me.ltlContenido.Text = sb.ToString()

      End If
    Catch ex As Exception
      'Exit Sub
      Me.ltlContenido.Text = ex.Message
    End Try
  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim argCrypt As String = Request.QueryString("a")
    Dim textoDesencriptado As String = Criptografia.DesencriptarTripleDES(argCrypt)
    Dim arrParametros As Array = Criptografia.ObtenerArrayQueryString(textoDesencriptado)
    Dim idUsuario As String = Criptografia.RequestQueryString(arrParametros, "idUsuario")

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(idUsuario, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())

      ActualizaInterfaz()
    End If
  End Sub

End Class