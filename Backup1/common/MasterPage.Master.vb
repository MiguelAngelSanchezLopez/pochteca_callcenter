﻿Imports CapaNegocio
Imports System.Data

Partial Public Class MasterPage
  Inherits System.Web.UI.MasterPage

#Region "Enum"
  Private Enum eTipoNotificacionTop As Integer
    Informacion = 1
    Warning = 2
  End Enum
#End Region

  Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    Dim oUtilidades As New Utilidades
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    ' page init se ejecuta antes del load de la pagina que llama a esta masterpage
    ' preguntamos si existre el objeto usuario enh la sesion, si no existe es que la sesion expiró
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If oUsuario Is Nothing Then
      Dim mensaje As String = Criptografia.EncriptarTripleDES("EXP")
      FormsAuthentication.RedirectToLoginPage("m=" & mensaje)
      Response.End()
    End If

  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim paginaActual As String = IO.Path.GetFileName(Request.FilePath)
    Dim ds As DataSet = oUtilidades.ObtenerDataSetSession(Usuario.SESION_MATRIZ_PERMISO)
    Dim html As String = ""
    Dim JavaScript As String = String.Empty

    ' revisar si en la sesion hay algun mensaje que desplegar
    If Not Utilidades.EsVacio(Session("Mensaje")) Then
      ' construimos una function js que desplegara un alert con el mensaje
      JavaScript &= "alert('" & Utilidades.EscaparComillasJS(Session("Mensaje").ToString) & "')" & vbCrLf
      Utilidades.RegistrarScript(Me.Page, JavaScript, Utilidades.eRegistrar.FINAL, "scriptOnLoad", False)
      Session.Remove("Mensaje")
    End If

    Try
      html = oUtilidades.ObtenerObjectSession(Utilidades.SESION_HTML_MENU)

      If (String.IsNullOrEmpty(html)) Then
        html = oUtilidades.DibujarMenu(ds)
        oUtilidades.GrabarObjectSession(Utilidades.SESION_HTML_MENU, html)
      Else
        html = CType(html, String)
      End If
    Catch ex As Exception
      html = ""
    End Try

    Me.hMenu.Text = html
    Me.lblVersionSistema.Text = "v" & Utilidades.VersionSistema()

    'si el password caduco, entonces muestra ventana para modificar la clave
    If (oUsuario.PasswordCaduco) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function () { Sistema.cambiarClave({ passwordCaduco: '1' }); }, 100);", Utilidades.eRegistrar.FINAL, "scriptOnLoad_CambiarClave", False)
    End If
  End Sub

End Class