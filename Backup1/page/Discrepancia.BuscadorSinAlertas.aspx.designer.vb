﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Discrepancia_BuscadorSinAlertas

  '''<summary>
  '''Control pnlMensaje.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensaje As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control ddlResolucionFinal.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlResolucionFinal As Global.WebTransportePochteca.wucCombo

  '''<summary>
  '''Control txtJSONLocales.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtJSONLocales As Global.System.Web.UI.HtmlControls.HtmlInputHidden

  '''<summary>
  '''Control ddlLocal.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlLocal As Global.System.Web.UI.HtmlControls.HtmlSelect

  '''<summary>
  '''Control chkSoloFocoTablet.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents chkSoloFocoTablet As Global.System.Web.UI.WebControls.CheckBox

  '''<summary>
  '''Control ddlSeccion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlSeccion As Global.WebTransportePochteca.wucCombo

  '''<summary>
  '''Control ddlCentroDistribucion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlCentroDistribucion As Global.WebTransportePochteca.wucCombo

  '''<summary>
  '''Control txtNroTransporte.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtNroTransporte As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtDM.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtDM As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtFechaContableDesde.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtFechaContableDesde As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtFechaContableHasta.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtFechaContableHasta As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control lblMensajeErrorFechaContableHasta.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblMensajeErrorFechaContableHasta As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control lblMensajeErrorFechaDiferenciaDias.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblMensajeErrorFechaDiferenciaDias As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtFiltroRegistrosPorPagina.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtFiltroRegistrosPorPagina As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control btnFiltrar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnFiltrar As Global.System.Web.UI.WebControls.LinkButton

  '''<summary>
  '''Control lblMensajeDiferenciaDias.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblMensajeDiferenciaDias As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtDiferenciaDias.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtDiferenciaDias As Global.System.Web.UI.HtmlControls.HtmlInputHidden

  '''<summary>
  '''Control txtCargaDesdePopUp.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtCargaDesdePopUp As Global.System.Web.UI.HtmlControls.HtmlInputHidden

  '''<summary>
  '''Control pnlMensajeUsuario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeUsuario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlHolderGrilla.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlHolderGrilla As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control lblTotalRegistros.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTotalRegistros As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control gvPrincipal.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents gvPrincipal As Global.System.Web.UI.WebControls.GridView
End Class
