﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="OT.AlertaRojaPorCerrar.aspx.vb" Inherits="WebTransportePochteca.OT_AlertaRojaPorCerrar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
  <script type="text/javascript" src="../js/alerta.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Alertas Rojas Por Cerrar</p>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlMensaje" runat="server"></asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3>
        </div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-2">
              <label class="control-label">IdMaster</label>
              <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4">50</asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioAlertaRojaPorCerrar())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
            </div>
          </div>
        </div>
      </div>
      <!-- RESULTADO -->
      <div class="form-group">
        <asp:Panel ID="pnlMensajeUsuario" runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered" EnableModelValidation="True" DataKeyNames="NroTransporte">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="NroTransporte" HeaderText="IdMaster" SortExpression="NroTransporte" />
              <asp:BoundField DataField="NombreTransportista" HeaderText="L&iacute;nea de Transporte" SortExpression="NombreTransportista" />
              <asp:BoundField DataField="MontoDiscrepancia" HeaderText="Monto Discrepancia" SortExpression="MontoDiscrepancia" />
              <asp:BoundField DataField="TipoAlerta" HeaderText="Tipo Alerta" SortExpression="TipoAlerta" />
              <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" />
              <asp:HyperLinkField DataNavigateUrlFields="Link" HeaderText="Link" SortExpression="Link" Text="Ver Mapa" Target="_blank" />
              <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
              <asp:BoundField DataField="Local" HeaderText="Tienda" SortExpression="Local" />
              <asp:BoundField DataField="NombreFormato" HeaderText="Formato" SortExpression="NombreFormato" />
            </Columns>
          </asp:GridView>
        </asp:Panel>
      </div>
    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });

    });
  </script>
</asp:Content>
