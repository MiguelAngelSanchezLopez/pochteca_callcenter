﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_ContactoDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim idEscalamientoPorAlertaContacto As String = Me.ViewState.Item("idEscalamientoPorAlertaContacto")
    Dim idEscalamientoPorAlerta As String = Me.ViewState.Item("idEscalamientoPorAlerta")
    Dim idEscalamientoPorAlertaGrupoContacto As String = Me.ViewState.Item("idEscalamientoPorAlertaGrupoContacto")
    Dim sbScript As New StringBuilder

    Me.txtidEscalamientoPorAlertaContacto.Value = idEscalamientoPorAlertaContacto
    Me.txtIdEscalamientoPorAlerta.Value = idEscalamientoPorAlerta
    Me.txtIdEscalamientoPorAlertaGrupoContacto.Value = idEscalamientoPorAlertaGrupoContacto

    sbScript.Append("Alerta.cargarDatosContactoPopUp({ idEscalamientoPorAlertaContacto: '" & idEscalamientoPorAlertaContacto & "', idEscalamientoPorAlerta: '" & idEscalamientoPorAlerta & "', idEscalamientoPorAlertaGrupoContacto: '" & idEscalamientoPorAlertaGrupoContacto & "' })")
    Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "scriptActualizaInterfaz", False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim idEscalamientoPorAlertaContacto As String = Me.ViewState.Item("idEscalamientoPorAlertaContacto")
    Dim idEscalamientoPorAlerta As String = Me.ViewState.Item("idEscalamientoPorAlerta")
    Dim idEscalamientoPorAlertaGrupoContacto As String = Me.ViewState.Item("idEscalamientoPorAlertaGrupoContacto")

    Me.btnEliminar.Attributes.Add("onclick", "parent.Alerta.eliminarContacto({ idEscalamientoPorAlerta:'" & idEscalamientoPorAlerta & "', idEscalamientoPorAlertaGrupoContacto: '" & idEscalamientoPorAlertaGrupoContacto & "', idEscalamientoPorAlertaContacto: '" & idEscalamientoPorAlertaContacto & "', invocadoDesde: 'PopUp' })")

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idEscalamientoPorAlertaContacto As String = Me.ViewState.Item("idEscalamientoPorAlertaContacto")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idEscalamientoPorAlertaContacto = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idEscalamientoPorAlertaContacto <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idEscalamientoPorAlertaContacto <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim idEscalamientoPorAlertaContacto As String = Utilidades.IsNull(Request("idEscalamientoPorAlertaContacto"), "-1")
    Dim idEscalamientoPorAlerta As String = Utilidades.IsNull(Request("idEscalamientoPorAlerta"), "-1")
    Dim idEscalamientoPorAlertaGrupoContacto As String = Utilidades.IsNull(Request("idEscalamientoPorAlertaGrupoContacto"), "-1")

    Me.ViewState.Add("idEscalamientoPorAlertaContacto", idEscalamientoPorAlertaContacto)
    Me.ViewState.Add("idEscalamientoPorAlerta", idEscalamientoPorAlerta)
    Me.ViewState.Add("idEscalamientoPorAlertaGrupoContacto", idEscalamientoPorAlertaGrupoContacto)

    If Not Page.IsPostBack Then
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
    End If
  End Sub

End Class