﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_DefinicionAlertaDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_Formatos = 1
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idAlertaDefinicion As String = Me.ViewState.Item("idAlertaDefinicion")

    Try
      ds = Alerta.ObtenerDetalleDefincionAlerta(idAlertaDefinicion)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaDefinicion As String = Me.ViewState.Item("idAlertaDefinicion")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim nombre, tieneRegistrosAsociados As String
    Dim totalRegistros As Integer
    Dim gestionarPorCemtra As Boolean
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    nombre = ""
    tieneRegistrosAsociados = "0"
    gestionarPorCemtra = False

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
          nombre = dr.Item("NombreAlerta")
          tieneRegistrosAsociados = dr.Item("TieneRegistrosAsociados")
          gestionarPorCemtra = dr.Item("GestionarPorCemtra")
        End If
      End If
    Catch ex As Exception
      nombre = ""
      tieneRegistrosAsociados = "0"
      gestionarPorCemtra = False
    End Try

    'asigna valores
    Me.lblTituloFormulario.Text = IIf(idAlertaDefinicion = "-1", "Nueva ", "Modificar ") & "Definición de Alerta"
    Me.lblIdRegistro.Text = IIf(idAlertaDefinicion = "-1", "", "[ID: " & idAlertaDefinicion & "]")
    Me.txtIdAlertaDefinicion.Value = idAlertaDefinicion
    Me.txtNombre.Text = nombre
    Me.chkGestionarPorCemtra.Checked = gestionarPorCemtra

    'primero verifica si tiene permiso para ver el boton y luego si tiene alertas asociadas
    Me.btnEliminar.Visible = Me.btnEliminar.Visible And IIf(tieneRegistrosAsociados = "1", False, True)

    'carga los datos en el multiselect
    CargarMultiselect(ds, eTabla.T01_Formatos)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()

    Me.ddlFormato.Attributes.Add("placeholder", "Escriba o seleccione el formato por asociar")
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    Dim idAlertaDefinicion As String = Me.ViewState.Item("idAlertaDefinicion")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idAlertaDefinicion = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idAlertaDefinicion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idAlertaDefinicion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idAlertaDefinicion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nombre, formatos, gestionarPorCemtra As String

    Try
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "")
      formatos = Utilidades.ObtenerValoresListBox(Me.ddlFormato)
      gestionarPorCemtra = IIf(Me.chkGestionarPorCemtra.Checked, "1", "0")

      'elimina los formatos que no tengan nada relacionado
      status = Alerta.EliminarFormatoDefinicionAlertaNoReferenciados(idAlertaDefinicion, formatos)
      If (status = Sistema.eCodigoSql.Error) Then
        Throw New Exception("No se pudo eliminar el formato de la definición de alerta")
      End If

      If idAlertaDefinicion = "-1" Then Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva Definición Alerta: " & nombre) Else Sistema.GrabarLogSesion(oUsuario.Id, "Modifica Definición Alerta: " & nombre)
      status = Alerta.GrabarDatosDefinicionAlerta(idAlertaDefinicion, nombre, formatos, gestionarPorCemtra)
      If status = "duplicado" Then idAlertaDefinicion = "-1"
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la definici&oacute;n de la alerta.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaDefinicion As String = Me.ViewState.Item("idAlertaDefinicion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosRegistro(status, idAlertaDefinicion)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La DEFINICION ALERTA fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La DEFINICION ALERTA fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}La DEFINICION ALERTA ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la DEFINICION ALERTA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la DEFINICION ALERTA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idAlertaDefinicion = "-1", "", "[ID: " & idAlertaDefinicion & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Alerta.cerrarPopUpMantenedor('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idAlertaDefinicion
      Response.Redirect("./Mantenedor.DefinicionAlertaDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' elimina los datos del registro
  ''' </summary>
  ''' <returns></returns>
  Private Function EliminarRegistro(ByRef status As String, ByVal idAlertaDefinicion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = Alerta.EliminarDatosDefinicionAlerta(idAlertaDefinicion)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina definición de alerta: IdAlertaDefinicion " & idAlertaDefinicion)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo eliminar la definici&oacute;n de alerta<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' carga los datos en el multiselect seleccionado
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CargarMultiselect(ByVal ds As DataSet, indice As eTabla)
    Dim ddlCombo As ListBox
    Dim dv As DataView
    Dim dt As DataTable
    Dim filtro As String

    Try
      Select Case indice
        Case eTabla.T01_Formatos
          ddlCombo = Me.ddlFormato
        Case Else
          ddlCombo = Nothing
      End Select

      dv = ds.Tables(indice).DefaultView
      ddlCombo.DataSource = dv
      ddlCombo.DataTextField = "Texto"
      ddlCombo.DataValueField = "Valor"
      ddlCombo.DataBind()

      'marca las opciones seleccionadas
      dt = ds.Tables(indice)
      For Each item As ListItem In ddlCombo.Items
        filtro = "Valor = " & item.Value & " AND Seleccionado = 1"
        item.Selected = (dt.Select(filtro).Length > 0)
      Next

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaDefinicion As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idAlertaDefinicion", idAlertaDefinicion)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaDefinicion As String = Me.ViewState.Item("idAlertaDefinicion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= EliminarRegistro(status, idAlertaDefinicion)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}La DEFINICION DE ALERTA fue eliminada satisfactoriamente. Complete el formulario para ingresar una nueva"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la DEFINICION DE ALERTA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la DEFINICION DE ALERTA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    queryStringPagina = "id=" & IIf(status = "eliminado", "-1", idAlertaDefinicion)
    Response.Redirect("./Mantenedor.DefinicionAlertaDetalle.aspx?" & queryStringPagina)

  End Sub

End Class