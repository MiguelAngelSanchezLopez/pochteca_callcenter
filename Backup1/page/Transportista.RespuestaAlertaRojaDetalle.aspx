﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Transportista.RespuestaAlertaRojaDetalle.aspx.vb" Inherits="WebTransportePochteca.Transportista_RespuestaAlertaRojaDetalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Detalle Respuesta Alerta Roja</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/transportista.js"></script>
</head>
<body>
  <form id="form1" runat="server" enctype="multipart/form-data">
  <input type="hidden" id="txtJSON" runat="server" value="[]" />
  <div class="container sist-margin-top-10">
    <div class="form-group text-center">
      <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
      <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger sist-font-size-20" Font-Bold="true"></asp:Label></div>
      <asp:Panel ID="pnlMensajeUsuario" runat="server"></asp:Panel>
    </div>
    <div class="form-group">
      <asp:Panel ID="pnlMensajeAcceso" runat="server"></asp:Panel>
      <asp:Panel ID="pnlContenido" runat="server" CssClass="small">
        <asp:Literal ID="ltlListado" runat="server"></asp:Literal>
      </asp:Panel>
      <div class="form-group text-center">
        <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Transportista.cerrarPopUp('1')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
        <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Transportista.validarFormularioAlertaRoja())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>
  </form>
</body>
</html>
