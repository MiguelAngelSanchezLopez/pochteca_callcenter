﻿Imports System.Web.Script.Serialization

Public Class Carrier_TractoRegresando
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim semaforo As String = Request("semaforo")
    Dim jsonString As String = Request("json")
    Dim jss As New JavaScriptSerializer
    Dim json As Object = jss.Deserialize(Of Object)(jsonString)

    If (semaforo = "ROJO") Then
      semaforo = "<div class=""text-danger""><img src=""../img/bullet_red.png"" alt="""" />12345</div>"
    Else
      semaforo = "<div class=""text-success""><img src=""../img/bullet_green.png"" alt="""" />12345</div>"
    End If

    'lblSemaforo.Text = semaforo
    lblCedis.Text = json.item("NombreCompletoCedis")
    lblPlacaTracto.Text = json.item("PlacaTracto")    
    lblTiempoLLegada.Text = json.item("TiempoLlegadaDestino")
    lblKilometroDestino.Text = json.item("KilometrosDestino")
  End Sub

End Class