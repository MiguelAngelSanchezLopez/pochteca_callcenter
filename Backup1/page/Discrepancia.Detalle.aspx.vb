﻿Imports CapaNegocio
Imports System.Data

Public Class Discrepancia_Detalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Private Enum eTabla As Integer
    T00_AuditoriaCD = 0
    T01_RecepcionLocal = 1
    T02_HistorialAlertas = 2
    T03_PlanAccion = 3
    T04_EstadoActualTransportista = 4
  End Enum
#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim codigoLocal As String = Me.ViewState.Item("codigoLocal")

    Try
      ds = Discrepancia.ObtenerDetalle(nroTransporte, codigoLocal)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim codigoLocal As String = Me.ViewState.Item("codigoLocal")
    Dim nombreLocal As String = Me.ViewState.Item("nombreLocal")
    Dim dm As String = Me.ViewState.Item("dm")
    Dim mensajeRetorno As String = Me.ViewState.Item("mensajeRetorno")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim cargaFecha, cargaRealizadoPor, cargaIdFormulario, recepcionFecha, recepcionRealizadoPor, recepcionIdFormulario As String
    Dim observacionEstadoTransportista As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim dv As DataView

    'inicializa controles en vacio
    cargaFecha = ""
    cargaRealizadoPor = ""
    cargaIdFormulario = "-1"
    recepcionFecha = ""
    recepcionRealizadoPor = ""
    recepcionIdFormulario = "-1"

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_AuditoriaCD).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_AuditoriaCD).Rows(0)
          cargaFecha = dr.Item("Fecha")
          cargaRealizadoPor = dr.Item("RealizadoPor")
          cargaIdFormulario = dr.Item("IdFormulario")
        End If

        totalRegistros = ds.Tables(eTabla.T01_RecepcionLocal).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T01_RecepcionLocal).Rows(0)
          recepcionFecha = dr.Item("Fecha")
          recepcionRealizadoPor = dr.Item("RealizadoPor")
          recepcionIdFormulario = dr.Item("IdFormulario")
        End If
      End If
    Catch ex As Exception
      cargaFecha = ""
      cargaRealizadoPor = ""
      cargaIdFormulario = "-1"
      recepcionFecha = ""
      recepcionRealizadoPor = ""
      recepcionIdFormulario = "-1"
    End Try

    'asigna valores
    Me.txtNroTransporte.Value = nroTransporte
    Me.txtCodigoLocal.Value = codigoLocal
    Me.lblNroTransporte.Text = IIf(nroTransporte = "-1", "<em>Sin IdMaster</em>", nroTransporte)
    Me.lblDM.Text = dm
    Me.lblNombreLocal.Text = nombreLocal
    Me.pnlMensajeCarga.Visible = IIf(String.IsNullOrEmpty(cargaFecha), True, False)
    Me.pnlDatosCarga.Visible = Not Me.pnlMensajeCarga.Visible
    Me.lblCargaFecha.Text = IIf(String.IsNullOrEmpty(cargaFecha), "Sin registro", cargaFecha)
    Me.lblCargaRealizadoPor.Text = IIf(String.IsNullOrEmpty(cargaRealizadoPor), "-", cargaRealizadoPor)
    Me.lblCargaInformePDF.Text = "<a href=""javascript:;"" onclick=""Discrepancia.verInformePDF({ tipoInforme:'Carga', idFormulario:'" & cargaIdFormulario & "', nroTransporte:'" & nroTransporte & "', codigoLocal:'" & codigoLocal & "', nombreLocal:'" & nombreLocal & "', dm:'" & dm & "' })""><img src=""../img/ico-pdf.png"" /></a>"
    Me.pnlMensajeRecepcion.Visible = IIf(String.IsNullOrEmpty(recepcionFecha), True, False)
    Me.pnlDatosRecepcion.Visible = Not Me.pnlMensajeRecepcion.Visible
    Me.lblRecepcionFecha.Text = IIf(String.IsNullOrEmpty(recepcionFecha), "Sin registro", recepcionFecha)
    Me.lblRecepcionRealizadoPor.Text = IIf(String.IsNullOrEmpty(recepcionRealizadoPor), "-", recepcionRealizadoPor)
    Me.lblRecepcionInformePDF.Text = "<a href=""javascript:;"" onclick=""Discrepancia.verInformePDF({ tipoInforme:'Recepcion', idFormulario:'" & recepcionIdFormulario & "', nroTransporte:'" & nroTransporte & "', codigoLocal:'" & codigoLocal & "', nombreLocal:'" & nombreLocal & "', dm:'" & dm & "' })""><img src=""../img/ico-pdf.png"" /></a>"

    '----------------------------------------
    'carga las alertas en la grilla
    dv = ds.Tables(eTabla.T02_HistorialAlertas).DefaultView
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = ds.Tables(eTabla.T02_HistorialAlertas).Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.lblMensajeGrilla.Visible = False
      Me.pnlHolderGrilla.Visible = True
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()
    Else
      Me.pnlHolderGrilla.Visible = False
      Me.lblMensajeGrilla.Visible = True
      Me.lblMensajeGrilla.Text = "<em class=""help-block"">No hay alertas asociados</em>"
      Me.gvPrincipal.Visible = False
    End If

    '----------------------------------------
    'carga los estados del transportista
    dv = ds.Tables(eTabla.T03_PlanAccion).DefaultView
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = ds.Tables(eTabla.T03_PlanAccion).Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.lblMensajeEstadoTransportista.Visible = False
      Me.pnlEstadoTransportista.Visible = True
      Me.dlEstadoTransportista.Visible = True
      Me.dlEstadoTransportista.DataSource = dv
      Me.dlEstadoTransportista.DataBind()
    Else
      Me.pnlEstadoTransportista.Visible = False
      Me.lblMensajeEstadoTransportista.Visible = True
      Me.lblMensajeEstadoTransportista.Text = "<em class=""help-block"">No hay estados asociados a la Línea de Transporte</em>"
      Me.dlEstadoTransportista.Visible = False
    End If

    '----------------------------------------
    'muestra el estado actual del transportista
    totalRegistros = ds.Tables(eTabla.T04_EstadoActualTransportista).Rows.Count
    If (totalRegistros = 0) Then
      observacionEstadoTransportista = ""
    Else
      observacionEstadoTransportista = ds.Tables(eTabla.T04_EstadoActualTransportista).Rows(0).Item("Observacion")
    End If
    observacionEstadoTransportista = IIf(String.IsNullOrEmpty(observacionEstadoTransportista), "<em class=""help-block"">Sin observaci&oacute;n</em>", observacionEstadoTransportista)
    Me.lblEstadoTransportistaObservacion.Text = observacionEstadoTransportista

    If (Not String.IsNullOrEmpty(mensajeRetorno)) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout( function() { alert(""" & mensajeRetorno & """); }, 100);", Utilidades.eRegistrar.FINAL, "script_ActualizaInterfaz", False)
    End If

  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
    Dim codigoLocal As String = Utilidades.IsNull(Request("codigoLocal"), "-1")
    Dim nombreLocal As String = Server.UrlDecode(Utilidades.IsNull(Request("nombreLocal"), "-1"))
    Dim dm As String = Utilidades.IsNull(Request("dm"), "-1")
    Dim mensajeRetorno As String = Server.UrlDecode(Utilidades.IsNull(Request("mensajeRetorno"), ""))

    Me.ViewState.Add("nroTransporte", nroTransporte)
    Me.ViewState.Add("codigoLocal", codigoLocal)
    Me.ViewState.Add("nombreLocal", nombreLocal)
    Me.ViewState.Add("dm", dm)
    Me.ViewState.Add("mensajeRetorno", mensajeRetorno)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

End Class