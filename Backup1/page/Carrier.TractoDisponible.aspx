﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Carrier.TractoDisponible.aspx.vb" Inherits="WebTransportePochteca.Carrier_TractoDisponible" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title></title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container">
      <div class="form-group text-center">
        <p class="h2">Tracto Disponible</p>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading"><span class="panel-title"><strong>INFORMACION TRACTO</strong></span></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-6 col-sm-3 col-md-2">
              <label class="control-label">Cedis</label>
              <div><asp:Label runat="server" ID="lblCedis"></asp:Label></div>
              <div class="small text-muted">&nbsp;</div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2">
              <label class="control-label">Placa Tracto</label>
              <div><asp:Label runat="server" ID="lblPlacaTracto"/></div>
              <div class="small text-muted">&nbsp;</div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2">
              <label class="control-label">Fecha llegada</label>
              <div><asp:Label runat="server" ID="lblFechaLLegada"/></div>
              <div class="small text-muted">&nbsp;</div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2">
              <label class="control-label">Tiempo de espera</label>
              <div><asp:Label runat="server" ID="lblTiempoEspera"/></div>
              <div class="small text-muted">&nbsp;</div>
            </div>
          </div>
        </div>
      </div>
  
    </div>
  </form>
</body>
</html>
