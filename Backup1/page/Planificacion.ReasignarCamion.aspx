﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Planificacion.ReasignarCamion.aspx.vb" Inherits="WebTransportePochteca.Planificacion_ReasignarCamion" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Reasignar cami&oacute;n</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/jquery.mask.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/planificacion.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdPlanificacion" runat="server" value="-1" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Reasignar cami&oacute;n"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS CAMION</h3></div>
            <div class="panel-body">

              <div class="form-group row">
                <div class="col-xs-4">
                  <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                  <uc1:wucCombo id="ddlTransportista" runat="server" FuenteDatos="Tabla" TipoCombo="Transportista" ItemSeleccione="true" CssClass="form-control chosen-select" funcionOnChange="Planificacion.asignarAutocompletarConductor()"></uc1:wucCombo>
                  <input type="hidden" id="ddlTransportista_ValorActual" runat="server" />
                </div>
                <div class="col-xs-2">
                  <label class="control-label">Carga<strong class="text-danger">&nbsp;*</strong></label>
                  <asp:TextBox ID="txtCarga" runat="server" CssClass="form-control"></asp:TextBox>
                  <input type="hidden" id="txtCarga_ValorActual" runat="server" />
                </div>
                <div class="col-xs-2">
                  <label class="control-label">Tipo Cami&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
                  <asp:TextBox ID="txtTipoCamion" runat="server" CssClass="form-control"></asp:TextBox>
                  <input type="hidden" id="txtTipoCamion_ValorActual" runat="server" />
                </div>
                <div class="col-xs-2">
                  <label class="control-label">Placa Tracto<strong class="text-danger">&nbsp;*</strong></label>
                  <asp:TextBox ID="txtPatenteTracto" runat="server" CssClass="form-control"></asp:TextBox>
                  <div id="hErrorPTracto" runat="server"></div>
                  <div id="hErrorPertenecePatenteTracto" runat="server"></div>
                  <input type="hidden" id="txtPatenteTracto_ValorActual" runat="server" />
                </div>
                <div class="col-xs-2">
                  <label class="control-label">Placa Remolque<strong class="text-danger">&nbsp;*</strong></label>
                  <asp:TextBox ID="txtPatenteTrailer" runat="server" CssClass="form-control"></asp:TextBox>
                  <div id="hErrorPTrailer" runat="server"></div>
                  <div id="hErrorPertenecePatenteTrailer" runat="server"></div>
                  <input type="hidden" id="txtPatenteTrailer_ValorActual" runat="server" />
                </div>
              </div>
              <div class="row">
                <div class="col-xs-4">
                  <label class="control-label">Operador<strong class="text-danger">&nbsp;*</strong></label>
                  <div class="input-group">
                    <asp:TextBox ID="txtNombreConductor" runat="server" CssClass="form-control autocomplete-conductor"></asp:TextBox>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                  </div>
                  <div id="hErrorNombreConductor" runat="server"></div>
                  <input type="hidden" id="txtNombreConductor_ValorActual" runat="server" />
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Rut Operador<strong class="text-danger">&nbsp;*</strong></label>
                  <div class="input-group">
                    <asp:TextBox ID="txtRutConductor" runat="server" CssClass="form-control autocomplete-conductor"></asp:TextBox>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                  </div>
                  <div id="hErrorRutConductor" runat="server"></div>
                  <input type="hidden" id="txtRutConductor_ValorActual" runat="server" />
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Celular<strong class="text-danger">&nbsp;*</strong></label>
                  <asp:TextBox ID="txtCelular" runat="server" CssClass="form-control"></asp:TextBox>
                  <input type="hidden" id="txtCelular_ValorActual" runat="server" />
                </div>
              </div>
            </div>
          </div>        
        
          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">TIENDAS DESTINO</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <input type="hidden" id="txtJSONLocalesSeleccionados_ValorActual" runat="server" value="[]" />
                <input type="hidden" id="txtJSONLocalesSeleccionados" runat="server" value="[]" />
                <input type="hidden" id="txtHayCambioLocales" runat="server" value="0" />

                <div class="row">
                  <div class="col-xs-3">
                    <label class="control-label">Fecha Presentaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtFechaPresentacion" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
                    <input type="hidden" id="txtFechaPresentacion_ValorActual" runat="server" />
                    <div id="lblMensajeErrorFechaPresentacion" runat="server"></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Hora Presentaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtHoraPresentacion" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                    <input type="hidden" id="txtHoraPresentacion_ValorActual" runat="server" />
                    <span class="help-block">(ejm: 09:00)</span>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">IdMaster<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtNroTransporte" runat="server" CssClass="form-control"></asp:TextBox>
                    <input type="hidden" id="txtNroTransporte_ValorActual" runat="server" />
                    <span id="hErrorNroTransporte" runat="server"></span>
                  </div>
                </div>

                <asp:Literal ID="ltlTablaLocales" runat="server"></asp:Literal>
                <span id="hErrorTablaLocales"></span>
              </div>
              <div class="row">
                <div class="col-xs-7">
                  <label class="control-label">Escriba nombre o determinante de la tienda para agregarlo</label>
                  <div class="input-group">
                    <input type="text" id="txtLocalAutocomplete" value="" class="form-control" placeholder="Escriba texto para buscar" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                  </div>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">MOTIVO DEL CAMBIO</h3></div>
            <div class="panel-body">
              <label class="control-label">Observaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
              <uc1:wucCombo id="ddlObservacion" runat="server" FuenteDatos="TipoGeneral" TipoCombo="ObservacionReasignacionCamion" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
          </div>        
        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Planificacion.cerrarPopUp('0')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Planificacion.validarFormularioReasignarCamion())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
          <asp:LinkButton ID="btnModificar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Planificacion.validarFormularioReasignarCamion())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</asp:LinkButton>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

        setTimeout(function () {
          jQuery("#txtLocalAutocomplete").autocomplete({
            source: "../webAjax/waSistema.aspx?op=AutocompletarLocalPlanficacionTransportista",
            select: function (event, json) { Planificacion.asociarLocal(json); },
            minLength: 1
          });
        }, 100);

        jQuery(".date-pick").datepicker({
          changeMonth: true,
          changeYear: true
        });

        jQuery("#txtHoraPresentacion").mask("00:00", { placeholder: "__:__" });

      });
    </script>

  </form>
</body>
</html>