﻿Imports CapaNegocio
Imports System.Data

Public Class Discrepancia_Buscador
  Inherits System.Web.UI.Page

#Region "Enum"
  Private NOMBRE_PAGINA As String = Utilidades.ObtenerNombrePaginaActual()

  Private Enum eFunciones
    Ver
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim dcMensajeRetorno As Dictionary(Of String, String) = CType(Session(Discrepancia.KEY_SESION_DISCREPANCIA_MENSAJE), Dictionary(Of String, String))
    Dim dcFiltros As Dictionary(Of String, String) = CType(Session(NOMBRE_PAGINA), Dictionary(Of String, String))
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario, seccion, codigoCentroDistribucion, dm, fechaContableDesde, fechaContableHasta, resolucionFinal As String
    Dim nroTransporte, carga, diferenciaDias, soloFocoTablet As String
    Dim codigoLocal As String = Nothing

    If (Not dcMensajeRetorno Is Nothing) Then
      If (Not dcFiltros Is Nothing) Then
        If dcFiltros.Item("seccion") <> "-1" Then Me.ddlSeccion.SelectedValue = dcFiltros.Item("seccion")
        If dcFiltros.Item("codigoCentroDistribucion") <> "-1" Then Me.ddlCentroDistribucion.SelectedValue = dcFiltros.Item("codigoCentroDistribucion")
        If dcFiltros.Item("resolucionFinal") <> "-1" Then Me.ddlResolucionFinal.SelectedValue = dcFiltros.Item("resolucionFinal")
        If dcFiltros.Item("carga") <> "-1" Then Me.ddlCarga.SelectedValue = dcFiltros.Item("carga")
        codigoLocal = dcFiltros.Item("codigoLocal")
        Me.txtDM.Text = dcFiltros.Item("dm")
        Me.txtFechaContableDesde.Text = dcFiltros.Item("fechaContableDesde")
        Me.txtFechaContableHasta.Text = dcFiltros.Item("fechaContableHasta")
        Me.txtNroTransporte.Text = dcFiltros.Item("nroTransporte")
        Me.txtDiferenciaDias.Value = dcFiltros.Item("diferenciaDias")
        Me.chkSoloFocoTablet.Checked = dcFiltros.Item("soloFocoTablet")
      End If
    End If

    'obtiene valores de los controles
    diferenciaDias = txtDiferenciaDias.Value
    idUsuario = oUsuario.Id
    codigoLocal = Utilidades.IsNull(codigoLocal, Utilidades.IsNull(Request(Me.ddlLocal.UniqueID), "-1"))
    seccion = Utilidades.IsNull(Me.ddlSeccion.SelectedValue, "-1")
    codigoCentroDistribucion = Utilidades.IsNull(Me.ddlCentroDistribucion.SelectedValue, "-1")
    dm = Utilidades.IsNull(Me.txtDM.Text, "-1")
    fechaContableDesde = Utilidades.IsNull(Me.txtFechaContableDesde.Text, "-1")
    fechaContableHasta = Utilidades.IsNull(Me.txtFechaContableHasta.Text, "-1")
    resolucionFinal = Utilidades.IsNull(Me.ddlResolucionFinal.SelectedValue, "-1")
    nroTransporte = Utilidades.IsNull(Me.txtNroTransporte.Text, "-1")
    carga = Utilidades.IsNull(Me.ddlCarga.SelectedValue, "-1")
    soloFocoTablet = IIf(Me.chkSoloFocoTablet.Checked, "1", "0")

    'obtiene listado
    ds = Discrepancia.Buscador(idUsuario, codigoLocal, seccion, codigoCentroDistribucion, dm, fechaContableDesde, fechaContableHasta, resolucionFinal, nroTransporte, carga, diferenciaDias, soloFocoTablet)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("dv") = Nothing
    Else
      Session("dv") = ds.Tables(0).DefaultView
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim totalRegistros As Integer
    Dim dcMensajeRetorno As Dictionary(Of String, String) = CType(Session(Discrepancia.KEY_SESION_DISCREPANCIA_MENSAJE), Dictionary(Of String, String))
    Dim nroTransporte, codigoLocal, nombreLocal, dm, mensajeRetorno As String

    dv = CType(Session("dv"), DataView)
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = dv.Table.Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.pnlMensajeUsuario.Visible = False
      Me.pnlHolderGrilla.Visible = True
      'carga la grilla a utilizar
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()
      Utilidades.MostrarTotalRegistrosGrilla(dv, Me.lblTotalRegistros)
      Me.btnExportar.Visible = True
    Else
      Me.pnlHolderGrilla.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = "No se encontraron registros"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
      'oculta la grilla
      Me.gvPrincipal.Visible = False
      Me.btnExportar.Visible = False
    End If

    If (Not dcMensajeRetorno Is Nothing) Then
      nroTransporte = dcMensajeRetorno.Item("nroTransporte")
      codigoLocal = dcMensajeRetorno.Item("codigoLocal")
      nombreLocal = dcMensajeRetorno.Item("nombreLocal")
      dm = dcMensajeRetorno.Item("dm")
      mensajeRetorno = dcMensajeRetorno.Item("mensajeRetorno")
      Utilidades.RegistrarScript(Me.Page, "setTimeout( function() { Discrepancia.abrirFormulario({ nroTransporte:'" & nroTransporte & "', codigoLocal:'" & codigoLocal & "', nombreLocal:'" & nombreLocal & "', dm:'" & dm & "', mensajeRetorno:'" & mensajeRetorno & "' }); }, 100);", Utilidades.eRegistrar.FINAL, "script_ActualizaInterfaz", False)
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim registrosPorPagina As String = Me.txtFiltroRegistrosPorPagina.Text

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    '-- inicializacion de controles que NO dependen del postBack --
    If registrosPorPagina = "" Then
      Me.txtFiltroRegistrosPorPagina.Text = Utilidades.ObtenerRegistrosPorPaginaDefault()
    End If

    Me.chkSoloFocoTablet.Attributes.Add("onclick", "Discrepancia.dibujarComboLocales({ valorSeleccionado:'-1' })")

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' metodo general para la paginacion de la grilla
  ''' </summary>
  Private Sub GridViewPageIndexChanging(ByVal gridView As GridView, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    Try
      'Cambia de pagina
      gridView.PageIndex = e.NewPageIndex
      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' metodo general para ordenar segun grilla seleccionada
  ''' </summary>
  Private Sub GridViewSorting(ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    Dim oUtilidades As New Utilidades
    Dim setearOrdenInicial As Boolean = False
    Try
      'Recupera el dataset para ordenar
      Dim dv As DataView = CType(Session("dv"), DataView)

      'determina el campo por el cual se ordenara
      Dim strOrdenarPor As String = e.SortExpression

      'Si ya se habia ordenado por ese campo, ordena pero en forma descendente
      If Not dv Is Nothing Then
        If strOrdenarPor = dv.Sort Then
          strOrdenarPor = strOrdenarPor & " DESC"
        End If
        'Ordena la grilla de acuerdo a la columna seleccionada
        dv.Sort = strOrdenarPor
      End If

      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba los filtros de busqueda en sesion
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarFiltrosEnSesion()
    Dim dcFiltros As New Dictionary(Of String, String)

    Try
      dcFiltros.Add("codigoLocal", Request(Me.ddlLocal.UniqueID))
      dcFiltros.Add("seccion", Me.ddlSeccion.SelectedValue)
      dcFiltros.Add("codigoCentroDistribucion", Me.ddlCentroDistribucion.SelectedValue)
      dcFiltros.Add("dm", Me.txtDM.Text)
      dcFiltros.Add("fechaContableDesde", Me.txtFechaContableDesde.Text)
      dcFiltros.Add("fechaContableHasta", Me.txtFechaContableHasta.Text)
      dcFiltros.Add("resolucionFinal", Me.ddlResolucionFinal.SelectedValue)
      dcFiltros.Add("nroTransporte", Me.txtNroTransporte.Text)
      dcFiltros.Add("carga", Me.ddlCarga.SelectedValue)
      dcFiltros.Add("soloFocoTablet", Me.chkSoloFocoTablet.Checked)
      dcFiltros.Add("diferenciaDias", Me.txtDiferenciaDias.Value)

      Session(NOMBRE_PAGINA) = dcFiltros
    Catch ex As Exception
      Session(NOMBRE_PAGINA) = Nothing
    End Try
  End Sub

  Private Sub DibujarComboLocales()
    Dim ds As New DataSet
    Dim json As String
    Dim idLocalSeleccionado As String = Utilidades.IsNull(Request(Me.ddlLocal.UniqueID), "-1")

    Try
      ds = Sistema.ObtenerComboDinamico("Local", "-1")
      json = MyJSON.ConvertDataTableToJSON(ds.Tables(0))
    Catch ex As Exception
      json = "[]"
    End Try

    Me.txtJSONLocales.Value = json
    Utilidades.RegistrarScript(Me.Page, "Discrepancia.dibujarComboLocales({ valorSeleccionado:'" & idLocalSeleccionado & "' })", Utilidades.eRegistrar.FINAL, "dibujarComboLocales", False)
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim cargadoDesdePopUp As String = Me.txtCargaDesdePopUp.Value
    Dim dcMensajeRetorno As Dictionary(Of String, String) = CType(Session(Discrepancia.KEY_SESION_DISCREPANCIA_MENSAJE), Dictionary(Of String, String))

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)

      Me.btnExportar.Visible = False
    End If

    If cargadoDesdePopUp = "1" Or Not dcMensajeRetorno Is Nothing Then
      ObtenerDatos()
      ActualizaInterfaz()
    End If

    DibujarComboLocales()
    MostrarMensajeUsuario()
    Me.txtCargaDesdePopUp.Value = "0"
    Session(Discrepancia.KEY_SESION_DISCREPANCIA_MENSAJE) = Nothing
  End Sub

  Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim pageSize As String = Trim(txtFiltroRegistrosPorPagina.Text)
    If pageSize = "" Or pageSize = "0" Then
      pageSize = Utilidades.ObtenerRegistrosPorPaginaDefault()
      txtFiltroRegistrosPorPagina.Text = pageSize
    End If
    'asigna total de registros por pagina
    Me.gvPrincipal.PageSize = pageSize
    'Muestra la primera pagina del paginador
    Me.gvPrincipal.PageIndex = 0

    GrabarFiltrosEnSesion()
    ObtenerDatos()
    ActualizaInterfaz()
    Sistema.GrabarLogSesion(oUsuario.Id, "Busca discrepancias")
  End Sub

  Protected Sub gvPrincipal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
    'llama metodo general para paginar
    GridViewPageIndexChanging(Me.gvPrincipal, e)
  End Sub

  Protected Sub gvPrincipal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound
    Dim lblNroTransporte, lblOpciones As Label
    Dim txtDM, txtNroTransporte, txtCodigoLocal, txtNombreLocal As HtmlControls.HtmlInputHidden
    Dim indicePrimeraFila As Integer = Utilidades.ObtenerIndicePrimeraFilaGrilla(Me.gvPrincipal)
    Dim indiceFila As Integer
    Dim opciones As String = ""
    Dim dm, nroTransporte, codigoLocal, nombreLocal As String

    If e.Row.RowType = DataControlRowType.DataRow Then
      txtDM = CType(e.Row.FindControl("txtDM"), HtmlControls.HtmlInputHidden)
      txtNroTransporte = CType(e.Row.FindControl("txtNroTransporte"), HtmlControls.HtmlInputHidden)
      txtCodigoLocal = CType(e.Row.FindControl("txtCodigoLocal"), HtmlControls.HtmlInputHidden)
      txtNombreLocal = CType(e.Row.FindControl("txtNombreLocal"), HtmlControls.HtmlInputHidden)
      lblNroTransporte = CType(e.Row.FindControl("lblNroTransporte"), Label)
      lblOpciones = CType(e.Row.FindControl("lblOpciones"), Label)

      dm = txtDM.Value
      nroTransporte = txtNroTransporte.Value
      codigoLocal = txtCodigoLocal.Value
      nombreLocal = txtNombreLocal.Value

      If (nroTransporte <> "-1") Then opciones &= "<button type=""button"" class=""btn btn-default"" onclick=""Discrepancia.abrirFormulario({ nroTransporte:'" & nroTransporte & "', codigoLocal:'" & codigoLocal & "', nombreLocal:'" & nombreLocal & "', dm:'" & dm & "', mensajeRetorno:'' })""><span class=""glyphicon glyphicon-search""></span></button>&nbsp;"

      lblNroTransporte.Text = IIf(nroTransporte = "-1", "<em>Sin IdMaster</em>", nroTransporte)
      lblOpciones.Text = opciones

      'limpia controles
      txtDM.Value = ""
      txtNroTransporte.Value = ""
      txtCodigoLocal.Value = ""
      txtNombreLocal.Value = ""

      'asigna indice de la fila
      indiceFila = indicePrimeraFila + e.Row.RowIndex
      'asigna valor del indice de la fila al control
      Utilidades.SetControlIndiceFila(e.Row, "lblIndiceFila", indiceFila)
    End If
  End Sub

  Protected Sub gvPrincipal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPrincipal.Sorting
    'llama metodo general para ordenar
    GridViewSorting(e)
  End Sub

End Class