﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Supervisor.EstadoConexionTeleoperador.aspx.vb" Inherits="WebTransportePochteca.Supervisor_EstadoConexionTeleoperador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/graficos/highcharts.js"></script>
  <script type="text/javascript" src="../js/graficos/exporting.js"></script>
  <script type="text/javascript" src="../js/grafico.js"></script>
  <script type="text/javascript" src="../js/usuario.js"></script>
  
  <div class="form-group text-center">
    <p class="h1">Monitoreo de Alertas Visibilidad</p>
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">
      <input type="hidden" id="txtJSONDatos" value="[]" />

      <div class="form-group">
        <div class="small">
          <table id="tableDatos" class="table table-striped table-condensed">
            <thead>
              <tr>
                <th>Nombre Usuario</th>
                <th style="width:120px;">Estado</th>
                <th style="width:320px;">&Uacute;ltima acci&oacute;n</th>
                <th style="width:140px;">Fecha &uacute;ltima acci&oacute;n</th>
                <th style="width:140px;">Tiempo &uacute;ltima acci&oacute;n</th>
                <th style="width:160px;">Fecha &uacute;ltimo inicio sesi&oacute;n</th>
                <th style="width:140px;">Tiempo inicio sesi&oacute;n</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <div id="hTablaSinDatos"></div>
        </div>
      </div>

      <div class="form-group">
        <table border="0" cellpadding="5" cellspacing="0" width="100%">
          <tr valign="top">
            <td style="width:660px;">
              <div id="hGrafico" style="width: 650px; height: 250px"></div>
            </td>
            <td>
              <table border="0" cellpadding="5" cellspacing="0" class="sist-font-size-20">
                <tr>
                  <td>Total alertas recibidas las &uacute;ltimas 4 hrs</td>
                  <td>: <strong><span id="lblTotalAlertasRecibidas"></span></strong></td>
                </tr>
                <tr>
                  <td>Total alertas atendidas las &uacute;ltimas 4 hrs</td>
                  <td>: <strong><span id="lblTotalAlertasAtendidas"></span></strong></td>
                </tr>
                <tr>
                  <td>Nivel contactabilidad</td>
                  <td>: <strong><span id="lblNivelContactabilidad"></span>%</strong></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>

      <script type="text/javascript">
        jQuery(document).ready(function () {
          Usuario.obtenerEstadoConexionTeleoperador();
          Usuario.SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.obtenerEstadoConexionTeleoperador(); }, Usuario.SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR);

          Usuario.dibujarTablaEstadoConexionTeleoperador();
          Usuario.SETINTERVAL_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.dibujarTablaEstadoConexionTeleoperador(); }, Usuario.SETINTERVAL_MILISEGUNDOS_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR);
        });
      </script>
    </asp:Panel>
  </div>

</asp:Content>