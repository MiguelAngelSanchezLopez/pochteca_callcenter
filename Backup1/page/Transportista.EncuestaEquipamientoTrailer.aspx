﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Transportista.EncuestaEquipamientoTrailer.aspx.vb" Inherits="WebTransportePochteca.Transportista_EncuestaEquipamientoTrailer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/transportista.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Equipamiento Remolque</p>
        <span class="text-danger sist-font-size-20" style="font-weight:bold;"><asp:Label ID="lblProveedor" runat="server"></asp:Label></span>
      </div>     
      <br />
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    
    <asp:Panel ID="pnlContenido" Runat="server">
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>      
        <asp:Panel ID="pnlFormulario" runat="server">
          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">1. EQUIPAMIENTO RAMPLA</h3></div>
            <div class="panel-body">

            <div class="form-group">
              <div class="row">
              <div class="col-xs-4">
                  <label class="control-label">1.1 Contiene GPS Portatil?</label>                                    
              </div>
              <div class="col-xs-8">
                <asp:RadioButton ID="rbSi_1_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_1_1" />&nbsp;&nbsp;
                <asp:RadioButton ID="rbNo_1_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_1_1"/>&nbsp;&nbsp;
                <asp:RadioButton ID="rbNa_1_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_1_1" Checked="True"/>
              </div>
              </div>
            </div>
            
              <div class="form-group">
            <div class="row">
            <div class="col-xs-4">
                <label class="control-label">1.2 Contiene GPS Fijo?	</label>                                    
            </div>
            <div class="col-xs-8">
              <asp:RadioButton ID="rbSi_1_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_1_2"  />&nbsp;&nbsp;
              <asp:RadioButton ID="rbNo_1_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_1_2"/>&nbsp;&nbsp;
              <asp:RadioButton ID="rbNa_1_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_1_2" Checked="True"/>
            </div>
            </div>
          </div>

              <div class="form-group">
            <div class="row">
            <div class="col-xs-4">
                <label class="control-label">1.3 Cuenta con sensor de Puerta?</label>                                    
            </div>
            <div class="col-xs-8">
              <asp:RadioButton ID="rbSi_1_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_1_3"  />&nbsp;&nbsp;
              <asp:RadioButton ID="rbNo_1_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_1_3"/>&nbsp;&nbsp;
              <asp:RadioButton ID="rbNa_1_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_1_3" Checked="True"/>
            </div>
            </div>
          </div>

              <div class="form-group">
            <div class="row">
            <div class="col-xs-4">
                <label class="control-label">1.4 Cuenta con sensor de Temperatura?</label>                                    
            </div>
            <div class="col-xs-8">
              <asp:RadioButton ID="rbSi_1_4" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_1_4"  />&nbsp;&nbsp;
              <asp:RadioButton ID="rbNo_1_4" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_1_4"/>&nbsp;&nbsp;
              <asp:RadioButton ID="rbNa_1_4" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_1_4" Checked="True"/>
            </div>
            </div>
          </div>

              <div class="form-group">
            <div class="row">
            <div class="col-xs-4">
                <label class="control-label">1.5 Contiene sistema de acoplamiento de rampla al tracto?</label>                                    
            </div>
            <div class="col-xs-8">
              <asp:RadioButton ID="rbSi_1_5" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_1_5"  />&nbsp;&nbsp;
              <asp:RadioButton ID="rbNo_1_5" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_1_5"/>&nbsp;&nbsp;
              <asp:RadioButton ID="rbNa_1_5" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_1_5" Checked="True"/>
            </div>
            </div>
          </div>

              <div class="form-group">
            <div class="row">
            <div class="col-xs-4">
                <label class="control-label">1.6 Contiene Termo para control de temperatura?	</label>                                    
            </div>
            <div class="col-xs-8">
              <asp:RadioButton ID="rbSi_1_6" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_1_6"  />&nbsp;&nbsp;
              <asp:RadioButton ID="rbNo_1_6" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_1_6"/>&nbsp;&nbsp;
              <asp:RadioButton ID="rbNa_1_6" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_1_6" Checked="True"/>
            </div>
            </div>
          </div>

            </div>
          </div>

          <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">2. CARACTERÍSTICAS DEL EQUIPAMIENTO GPS	</h3></div>
	          <div class="panel-body">
	          <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.1 El dispositivo GPS Fijo, cuenta con una autonomía mínima de una semana?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_1" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_1" Checked="True" />
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.2 El dispositivo GPS Portatil, cuenta con una autonomía minima de un mes?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_2"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_2" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.3 El dispositivo GPS cuenta con sensor de Movimiento?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_3" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_3"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_3" Checked="True" />
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.4 El dispositivo GPS cuenta con entradas Digitales?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_4" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_4" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_4" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_4"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_4" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_4"  Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.5 El dispositivo GPS cuenta con entradas análogas?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_5" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_5"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_5" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_5"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_5" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_5" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.6 El dispositivo GPS almacena geocercas en memoria?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_6" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_6"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_6" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_6"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_6" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_6" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.7 El dispositivo GPS contiene antenas GPS y GPRS exteriores?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_7" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_7"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_7" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_7"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_7" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_7" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">2.8 El dispositivo GPS contiene antenas GPS y GPRS internas?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_2_8" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_2_8" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_2_8" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_2_8"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_2_8" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_2_8" Checked="True" />
			          </div>
		          </div>
	          </div>	

          </div>
          </div>

          <div class="panel panel-primary">
	        <div class="panel-heading"><h3 class="panel-title">3. CARACTERISTICAS INSTALACIÓN GPS</h3></div>
  	      <div class="panel-body">
          	<div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">3.1 El dispositivo GPS se encuentra instalado a la ignición?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_3_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_3_1"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_3_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_3_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_3_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_3_1" Checked="True"/>
			          </div>
		          </div>
	          </div>
          	<div class="form-group">
		        <div class="row">
			        <div class="col-xs-7">
				        <label class="control-label">3.2 El dispositivo GPS portatil se encuentra conectado a la luz de freno de la rampla?</label>                                    
			        </div>
			        <div class="col-xs-5">
				        <asp:RadioButton ID="rbSi_3_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_3_2" />&nbsp;&nbsp;
				        <asp:RadioButton ID="rbNo_3_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_3_2"/>&nbsp;&nbsp;
				        <asp:RadioButton ID="rbNa_3_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_3_2" Checked="True" />
			        </div>
		        </div>
	        </div>
          	<div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">3.3 La instalación cuenta con un GPS señuelo para detectar manipulación del dispositivo?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_3_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_3_3"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_3_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_3_3"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_3_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_3_3" Checked="True"/>
			          </div>
		          </div>
	          </div>
          	<div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">3.4 El dispositivo GPS se encuentra instalado de tal manera que no es visible para el operador?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_3_4" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_3_4"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_3_4" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_3_4"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_3_4" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_3_4" Checked="True"/>
			          </div>
		          </div>
	          </div>
          </div>
          </div>  

          <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">4. CARACTERISTICAS SENSOR DE PUERTA</h3></div>
	          <div class="panel-body">
	          <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">4.1 El sensor de puerta corresponde a sensor imantado (magnéticos)?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_4_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_4_1"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_4_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_4_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_4_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_4_1" Checked="True"/>
			          </div>
		          </div>
	          </div>	
            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">4.2 El sensor de puerta corresponde a sensor inalambrico?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_4_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_4_2"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_4_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_4_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_4_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_4_2" Checked="True"/>
			          </div>
		          </div>
	          </div>	
          </div>
          </div>

          <div class="panel panel-primary">
          <div class="panel-heading"><h3 class="panel-title">5. CARACTERISTICAS INSTALACIÓN SENSOR DE PUERTA</h3></div>
          <div class="panel-body">
          <div class="form-group">
	          <div class="row">
		          <div class="col-xs-7">
			          <label class="control-label">5.1 El sensor se encuentra conectado directamente al GPS de la Tracto?</label>                                    
		          </div>
		          <div class="col-xs-5">
			          <asp:RadioButton ID="rbSi_5_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_5_1"  />&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNo_5_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_5_1"/>&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNa_5_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_5_1" Checked="True"/>
		          </div>
	          </div>
          </div>	

          <div class="form-group">
	          <div class="row">
		          <div class="col-xs-7">
			          <label class="control-label">5.2 El sensor se encuentra conectado directamente al GPS de la Rampla?</label>                                    
		          </div>
		          <div class="col-xs-5">
			          <asp:RadioButton ID="rbSi_5_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_5_2"  />&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNo_5_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_5_2"/>&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNa_5_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_5_2" Checked="True"/>
		          </div>
	          </div>
          </div>	

          <div class="form-group">
	          <div class="row">
		          <div class="col-xs-7">
			          <label class="control-label">5.3 El sensor cuenta con instalación por medio de canaletas blindando el cable del sensor?</label>                                    
		          </div>
		          <div class="col-xs-5">
			          <asp:RadioButton ID="rbSi_5_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_5_3"  />&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNo_5_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_5_3"/>&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNa_5_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_5_3" Checked="True"/>
		          </div>
	          </div>
          </div>	

          <div class="form-group">
	          <div class="row">
		          <div class="col-xs-7">
			          <label class="control-label">5.4 El sensor es visible en la puerta?(Cableado)</label>                                    
		          </div>
		          <div class="col-xs-5">
			          <asp:RadioButton ID="rbSi_5_4" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_5_4"  />&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNo_5_4" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_5_4"/>&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNa_5_4" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_5_4" Checked="True"/>
		          </div>
	          </div>
          </div>	

           <div class="form-group">
	          <div class="row">
		          <div class="col-xs-7">
			          <label class="control-label">5.5 El sensor se encuentra integrado en la puerta evitando ser vulnerado?	</label>                                    
		          </div>
		          <div class="col-xs-5">
			          <asp:RadioButton ID="rbSi_5_5" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_5_5"  />&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNo_5_5" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_5_5"/>&nbsp;&nbsp;
			          <asp:RadioButton ID="rbNa_5_5" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_5_5" Checked="True"/>
		          </div>
	          </div>
          </div>	

          


          </div>
          </div>

          <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">6. CARACTERISTICAS SENSOR DE TEMPERATURA</h3></div>
	          <div class="panel-body">
	          <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">6.1 El sensor de temperatura  corresponde a sensor digital?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_6_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_6_1"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_6_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_6_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_6_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_6_1" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">6.2 El sensor de temperatura  corresponde a sensor análogo?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_6_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_6_2"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_6_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_6_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_6_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_6_2" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">6.3 El sensor se encuentra  conectado al GPS de la Rampla?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_6_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_6_3"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_6_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_6_3"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_6_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_6_3" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">6.4 El sensor se encuentra  conectado al GPS del Tracto?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_6_4" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_6_4"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_6_4" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_6_4"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_6_4" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_6_4" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">6.5 El sensor de temperatura es un dispositivo propio del thermo de la rampla?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_6_5" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_6_5"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_6_5" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_6_5"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_6_5" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_6_5" Checked="True"/>
			          </div>
		          </div>
	          </div>	
          </div>
          </div>

          <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">7. CARACTERISTICAS INSTALACIÓN SENSOR DE TEMPERATURA</h3></div>
	          <div class="panel-body">
	          
            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">7.1 El sensor se encuentra instalado de manera que el cable del mismo no es visible dentro de la rampla?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_7_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_7_1" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_7_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_7_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_7_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_7_1" Checked="True" />
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">7.2 El sensor se encuentra instalado al lado del refrigerante del thermo dentro de la rampla?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_7_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_7_2"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_7_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_7_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_7_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_7_2" Checked="True"/>
			          </div>
		          </div>
	          </div>	

          </div>
          </div>

          <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">8. CARACTERISTICAS SISTEMA ACOPLAMIENTO DE RAMPLA</h3></div>
	          <div class="panel-body">
	          <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">8.1 El sistema se conecta directamente al cableado del Tracto?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_8_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_8_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_8_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_8_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_8_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_8_1"  Checked="True" />
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">8.2 El sistema corresponde a un sistema periférico del Tracto?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_8_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_8_2"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_8_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_8_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_8_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_8_2" Checked="True"/>
			          </div>
		          </div>
	          </div>	

             <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">8.3 El sistema esta compuesto de una solución GPS?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_8_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_8_3" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_8_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_8_3"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_8_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_8_3" Checked="True" />
			          </div>
		          </div>
	          </div>	

          </div>
          </div>

          <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">9. CARACTERISTICAS INSTALACIÓN SISTEMA ACOPLE DE RAMPLA	</h3></div>
	          <div class="panel-body">
	          <div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">9.1 El sistema va instalado en el cableado de acople del tracto y rampla, contando con un cable blindado?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_9_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_9_1"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_9_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_9_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_9_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_9_1" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">9.2 El sistema se encuentra instalado directamente en la Rampla?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_9_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_9_2"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_9_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_9_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_9_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_9_2" Checked="True"/>
			          </div>
		          </div>
	          </div>	

             <div class="form-group">
		          <div class="row">
			          <div class="col-xs-7">
				          <label class="control-label">9.3 El sistema se encuentra instalado directamente en el Tracto?</label>                                    
			          </div>
			          <div class="col-xs-5">
				          <asp:RadioButton ID="rbSi_9_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_9_3"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_9_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_9_3"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_9_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_9_3" Checked="True"/>
			          </div>
		          </div>
	          </div>	

          </div>
          </div>
         
         <div class="panel panel-primary">
	          <div class="panel-heading"><h3 class="panel-title">10. MANTENCIÓN</h3></div>
	          <div class="panel-body">

	          <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.1 Se realiza mantención al dispositivo GPS al menos dos veces por més?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_1" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_1"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_1" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_1"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_1" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_1" Checked="True"/>
			          </div>
		          </div>
	          </div>
            
            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.2 El equipo GPS lleva más de un año instalado en la Rampla?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_2" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_2" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_2" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_2"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_2" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_2"  Checked="True"/>
			          </div>
		          </div>
	          </div>	

             <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.3 Se a cambiado el equipo GPS en el transcurso de un año?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_3" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_3" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_3" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_3"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_3" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_3" Checked="True" />
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.4 La mantención esta asociada a manipulación del equipo? (Sensores y GPS)</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_4" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_4"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_4" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_4"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_4" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_4" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.5 Los Sensores de Puerta  Han sido cambiados a lo menos una vez por mes?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_5" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_5"  />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_5" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_5"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_5" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_5" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.6 Los sensores de temperatura han reportado problemas en su operación?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_6" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_6" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_6" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_6"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_6" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_6" Checked="True"/>
			          </div>
		          </div>
	          </div>	

            <div class="form-group">
		          <div class="row">
			          <div class="col-xs-6">
				          <label class="control-label">10.7 El sistema de acople de rampla a sido mantenido antes de 6 meses?</label>                                    
			          </div>
			          <div class="col-xs-6">
				          <asp:RadioButton ID="rbSi_10_7" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb_10_7" />&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNo_10_7" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb_10_7"/>&nbsp;&nbsp;
				          <asp:RadioButton ID="rbNa_10_7" runat="server" CssClass="control-label" Text="&nbsp;N/A" GroupName="rb_10_7" Checked="True" />
			          </div>
		          </div>
	          </div>	
            	
          </div>
          </div>

           <div class="form-group text-center">
            <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Transportista.validarFormularioEncuesta())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
          </div>
      </asp:Panel>
      
      </div>
      </asp:Panel>
   </div>          
 

   
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>

</asp:Content>
