﻿Imports CapaNegocio

Public Class OT_AlertaRojaDetalle

  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    ExportarDatos
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim gestionadas As Boolean = Utilidades.IsNull(Request("gestionadas"), "")
    Dim rutTransportista As String = Utilidades.IsNull(Request("rutTransportista"), "-1")

    'obtiene listado
    ds = Alerta.ObtenerDetalleAlertasRojasPorTransportista(rutTransportista, gestionadas)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("ds") = Nothing
    Else
      Session("ds") = ds
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    '-- inicializacion de controles que NO dependen del postBack --

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim ds As New DataSet
    Dim totalRegistros, totalRegistrosDetalle As Integer
    Dim sbPrincipal, sbDetalle As New StringBuilder
    Dim template, templateFila As String
    Dim nombreTransportista, nroTransporte, idAlerta, nombreConductor, patenteTractor, categoriaAlerta, fecha, observacion As String
    Dim cdOrigen, localDestino, tiempoRespuestaTransportista, montoDiscrepancia As String
    Dim gestionadas As Boolean = Utilidades.IsNull(Request("gestionadas"), "")

    ds = CType(Session("ds"), DataSet)

    If ds Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = ds.Tables(0).Rows.Count
    End If

    template = "<tr class=""sist-cursor-pointer"">" & _
              "  <td>{ID_ALERTA}</td>" & _
              "  <td>{CATEGORIA_ALERTA}</td>" & _
              "  <td>{NOMBRE_CONDUCTOR}</td>" & _
              "  <td>{PATENTE_TRACTOR}</td>" & _
              "  <td>{FECHA}</td>" & _
              "  <td>{OBSERVACION}</td>" & _
              "  <td>{CD_ORIGEN}</td>" & _
              "  <td>{LOCALDESTINO}</td>" & _
              "</tr>"

    If totalRegistros > 0 Then
      Me.pnlMensajeUsuario.Visible = False
      Me.pnlHolderGrilla.Visible = True

      For Each dr In ds.Tables(0).Rows
        nombreTransportista = dr("nombreTransportista")
        nroTransporte = dr("nroTransporte")
        tiempoRespuestaTransportista = dr("TiempoRespuestaTransportista")
        montoDiscrepancia = dr("MontoDiscrepancia")

        'Filtro el detalle por numero de viaje
        dv = ds.Tables(1).DefaultView
        dv.RowFilter = "nroTransporte = '" & nroTransporte & "'"
        totalRegistrosDetalle = dv.Count

        sbDetalle.Append("<div class=""panel panel-primary"">")
        sbDetalle.Append("<div class=""panel-heading"">")
        sbDetalle.Append("<strong>")
        sbDetalle.Append(nombreTransportista)

        If (gestionadas) Then
          If (tiempoRespuestaTransportista = "-1") Then
            sbDetalle.Append(" ( Sin respuesta de Línea de Transporte )")
          Else
            sbDetalle.Append(" ( Tiempo Respuesta de la Línea de Transporte ")
            sbDetalle.Append(tiempoRespuestaTransportista)
            sbDetalle.Append(" hora(s) )")
          End If
        End If

        sbDetalle.Append("</br>")
        sbDetalle.Append("IdMaster ")
        sbDetalle.Append(nroTransporte)
        sbDetalle.Append(" / ")
        sbDetalle.Append("Monto Discrepancia $")
        sbDetalle.Append(montoDiscrepancia)
        sbDetalle.Append("</strong>")
        sbDetalle.Append("</div>")
        sbDetalle.Append("<div class=""panel-body"">")
        sbDetalle.Append("  <div class=""table-responsive"">")
        sbDetalle.Append("    <div class=""form-group"">")
        sbDetalle.Append("      <div>")
        sbDetalle.Append("        <strong>")
        sbDetalle.Append(" Cantidad de Registros " & totalRegistrosDetalle)
        sbDetalle.Append("        </strong>")
        sbDetalle.Append("      </div>")
        sbDetalle.Append("<table class=""table table-hover table-condensed sist-width-100-porciento"">")
        sbDetalle.Append("  <thead>")
        sbDetalle.Append("    <tr>")
        sbDetalle.Append("      <th>ID Alerta</th>")
        sbDetalle.Append("      <th>Categoria Alerta</th>")
        sbDetalle.Append("      <th>Nombre Operador</th>")
        sbDetalle.Append("      <th>Pantente Tracto</th>")
        sbDetalle.Append("      <th>Fecha</th>")
        sbDetalle.Append("      <th>Observación</th>")
        sbDetalle.Append("      <th>Cedis Origen</th>")
        sbDetalle.Append("      <th>Tienda Destino</th>")
        sbDetalle.Append("    </tr>")
        sbDetalle.Append("  </thead>")
        sbDetalle.Append("  <tbody>")

        'Recorro el detalle
        For Each drView In dv
          idAlerta = drView("IdAlerta")
          nombreConductor = drView("NombreConductor")
          patenteTractor = drView("PatenteTracto")
          categoriaAlerta = drView("CategoriaAlerta")
          fecha = drView("FechaCreacion")
          observacion = drView("Observacion")
          cdOrigen = drView("CDOrigen")
          localDestino = drView("LocalDestino")

          templateFila = template

          'remplaza marcas
          templateFila = templateFila.Replace("{ID_ALERTA}", idAlerta)
          templateFila = templateFila.Replace("{CATEGORIA_ALERTA}", categoriaAlerta)
          templateFila = templateFila.Replace("{NOMBRE_CONDUCTOR}", nombreConductor)
          templateFila = templateFila.Replace("{PATENTE_TRACTOR}", patenteTractor)
          templateFila = templateFila.Replace("{FECHA}", fecha)
          templateFila = templateFila.Replace("{OBSERVACION}", observacion)
          templateFila = templateFila.Replace("{CD_ORIGEN}", cdOrigen)
          templateFila = templateFila.Replace("{LOCALDESTINO}", localDestino)

          sbDetalle.Append(templateFila)
        Next

        dv.RowFilter = ""

        sbDetalle.Append("  </tbody>")
        sbDetalle.Append("</table>")
        sbDetalle.Append("    </div>")
        sbDetalle.Append("  </div>")
        sbDetalle.Append("</div>")
        sbDetalle.Append("</div>")
        sbDetalle.Append("<BR>")

        sbPrincipal.Append(sbDetalle.ToString())

        sbDetalle.Length = 0 'limpiar
      Next

      ltlContenido.Text = sbPrincipal.ToString()

    Else
      Me.pnlHolderGrilla.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = "No se encontraron registros"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
    End If

  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    'Dim cargadoDesdePopUp As String = Me.txtCargaDesdePopUp.Value

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    ObtenerDatos()
    ActualizaInterfaz()

  End Sub
End Class