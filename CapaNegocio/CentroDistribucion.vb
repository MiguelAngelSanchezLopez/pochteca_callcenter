﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class CentroDistribucion

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "CentroDistribucion"

  Public Enum eTabla As Integer
    Detalle = 0
  End Enum
#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de CentroDistribucion
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String, ByVal codigoInterno As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre
      arParms(2) = New SqlParameter("@CodigoInterno", SqlDbType.Int)
      arParms(2).Value = codigoInterno

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_CentroDistribucion_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del CentroDistribucion consultado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idCentroDistribucion As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdCentroDistribucion", SqlDbType.Int)
      arParms(0).Value = idCentroDistribucion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_CentroDistribucion_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba un CentroDistribucion
  ''' </summary>
  Public Shared Function GrabarDatos(ByRef idCentroDistribucion As String, ByVal nombre As String, ByVal codigo As String, ByVal activo As String, _
                                     ByVal listadoUsuariosCargos As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(6) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdCentroDistribucionOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdCentroDistribucion", SqlDbType.Int)
      arParms(2).Value = idCentroDistribucion
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@CodigoInterno", SqlDbType.Int)
      arParms(4).Value = codigo
      arParms(5) = New SqlParameter("@Activo", SqlDbType.Int)
      arParms(5).Value = activo
      arParms(6) = New SqlParameter("@ListadoUsuariosCargos", SqlDbType.VarChar)
      arParms(6).Value = listadoUsuariosCargos

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_CentroDistribucion_GrabarDatos", arParms)
      valor = arParms(0).Value
      idCentroDistribucion = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar CentroDistribucion.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar CentroDistribucion.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para la CentroDistribucion seleccionada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function VerificarDuplicidad(ByVal idCentroDistribucion As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_CentroDistribucion_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdCentroDistribucion", SqlDbType.Int)
      arParms(1).Value = idCentroDistribucion
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' elimina un CentroDistribucion
  ''' </summary>
  Public Shared Function EliminarDatos(ByVal idCentroDistribucion As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdCentroDistribucion", SqlDbType.Int)
      arParms(1).Value = idCentroDistribucion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_CentroDistribucion_EliminarDatos", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar CentroDistribucion.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene los cargos asociados al centro distribucion
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerCargos(ByVal idCentroDistribucion As String, ByVal categoria As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@idCentroDistribucion", SqlDbType.Int)
      arParms(0).Value = idCentroDistribucion
      arParms(1) = New SqlParameter("@Categoria", SqlDbType.VarChar)
      arParms(1).Value = categoria

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_CentroDistribucion_ObtenerCargos", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class
