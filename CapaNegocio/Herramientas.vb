﻿Imports CapaNegocio
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Globalization.CultureInfo
Imports System.Text
Imports System.IO
Imports System.Threading

Public Class Herramientas

#Region "Constructor"
  'Metodo     : New()
  'Descripcion: constructor de la clase
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 10/04/2008
  Sub New()
    MyBase.New()
  End Sub
#End Region

#Region "Enum"
  Public Enum eUnidadMedidaArchivo As Integer
    Bytes = 1
    KiloBytes = 2
    MegaBytes = 3
    GigaBytes = 4
    TeraBytes = 5
  End Enum

  Private Const CARACTERES_NO_VALIDOS_REGEXP As String = "<|>" 'string separado por "|" usado para la expresion regular
  Public Const INDICE_DIA_LUNES As Integer = 1

#End Region

#Region "Tipo Datos"
  Public Class TipoDatos
    Public Class Respuesta
      Property Codigo As Integer
      Property Descripcion As String
    End Class
  End Class
#End Region

#Region "Metodos Generales"
  'Funcion    : IsNull
  'Descripcion: Retorna objCheck si no es nulo, sino retorna objReplacement
  'Por        : VSR, 24/04/2008
  Public Shared Function IsNull(ByVal objCheck As Object, ByVal objReplacement As Object) As String
    If IsDBNull(objCheck) Then
      Return objReplacement
    Else
      Return objCheck
    End If
  End Function

  'Funcion    : IsNull
  'Descripcion: Retorna ValorActual si no es nulo, sino retorna ValorReemplazo
  'Por        : VSR, 24/08/2008
  Public Shared Function IsNull(ByVal ValorActual As String, ByVal ValorReemplazo As String) As String
    If IsDBNull(ValorActual) Or ValorActual = "" Then
      Return ValorReemplazo
    Else
      Return ValorActual
    End If
  End Function

  'Metodo     : ObtenerFechaActualSistema()
  'Descripcion: obtiene la fecha actual del sistema
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 06/05/2008
  Public Shared Function ObtenerFechaActualSistema() As String
    Dim fecha As Date = Herramientas.MyNow()
    Dim diaActual As String = Day(fecha).ToString
    Dim mesActual As String = Month(fecha).ToString
    Dim anoActual As String = Year(fecha).ToString
    Dim nuevaFecha As String

    diaActual = IIf(diaActual <= 9, "0" & diaActual, diaActual)
    mesActual = IIf(mesActual <= 9, "0" & mesActual, mesActual)
    'construye nueva fecha
    nuevaFecha = diaActual & "/" & mesActual & "/" & anoActual
    'retorna valor
    Return nuevaFecha
  End Function

  'Metodo     : ObtenerFechaHoraActualSistema()
  'Descripcion: obtiene la fecha y hora actual del sistema
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 06/08/2008
  Public Shared Function ObtenerFechaHoraActualSistema(Optional ByVal comoFormatoISO As Boolean = True, Optional ByVal separarFechaHora As Boolean = False) As String
    Dim myNow As DateTime = Herramientas.MyNow()
    Dim ano As String = "0000" & myNow.Year.ToString
    Dim mes As String = "0" & myNow.Month.ToString
    Dim dia As String = "0" & myNow.Day.ToString
    Dim hora As String = "0" & myNow.Hour.ToString
    Dim minutos As String = "0" & myNow.Minute.ToString
    Dim segundos As String = "0" & myNow.Second.ToString
    Dim nuevaFecha As String

    If comoFormatoISO Then
      If separarFechaHora Then
        nuevaFecha = Right(ano, 4) & Right(mes, 2) & Right(dia, 2) & " " & Right(hora, 2) & Right(minutos, 2) & Right(segundos, 2)
      Else
        nuevaFecha = Right(ano, 4) & Right(mes, 2) & Right(dia, 2) & Right(hora, 2) & Right(minutos, 2) & Right(segundos, 2)
      End If
    Else
      nuevaFecha = Right(dia, 2) & "/" & Right(mes, 2) & "/" & Right(ano, 4) & " " & Right(hora, 2) & ":" & Right(minutos, 2) & " hrs."
    End If

    'retorna valor
    Return nuevaFecha
  End Function


#End Region

#Region "Funciones para Validacion"

  ' comprueba si una cadena es vacia
  Public Shared Function EsVacio(ByRef s As String) As Boolean

    Return String.IsNullOrEmpty(s)

  End Function

  ' comprueba el largo minimo y opcionalmente el maximo de un string
  Public Shared Function TieneLargoValido(ByRef s As String, ByVal minimo As Integer, Optional ByVal maximo As Integer = 0) As Boolean

    s.Trim()

    If minimo < 1 Then minimo = s.Length
    If maximo < 1 Then maximo = s.Length

    Return TieneRangoValido(s.Length, minimo, maximo)

  End Function

  ' comprueba si un integer esta entre un rango dado
  Public Shared Function TieneRangoValido(ByVal n As Integer, ByVal minimo As Integer, ByVal maximo As Integer) As Boolean

    ' para que sea true n tiene que ser mayor o igual que el minimo y menor o igual al maximo
    Return (n >= minimo AndAlso n <= maximo)

  End Function

  ' comprueba si un string tiene solo caracteres numericos
  Public Shared Function EsNumerico(ByRef s As String, _
                            Optional ByVal conDecimales As Boolean = False, _
                            Optional ByVal conNegativos As Boolean = False, _
                            Optional ByVal conExponenciales As Boolean = False) As Boolean

    s.Trim()

    Dim negativos As String = IIf(conNegativos, "[-]?", "")
    Dim decimales As String = IIf(conDecimales, "(\.\d+)?", "")
    Dim exponenciales As String = IIf(conExponenciales, "[x]\d*[Ee]([-]\d*)?", "")
    Dim regExp As New System.Text.RegularExpressions.Regex( _
                      "(^" & negativos & "\d+" & decimales & exponenciales & "$)")

    Return regExp.IsMatch(s)

  End Function

  ' comprueba si s tiene formato email
  Public Shared Function EsEmail(ByVal s As String) As Boolean
    If String.IsNullOrEmpty(s) Or s Is Nothing Then
      Return False
      Exit Function
    End If

    Dim regExp As New System.Text.RegularExpressions.Regex( _
    "^(([^<;>;()[\]\\.,;:\s@\""]+(\.[^<;>;()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@" & _
    "((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$")

    Return regExp.IsMatch(s)

  End Function

  ' comprueba si s tiene formato de telefono
  Public Shared Function EsTelefono(ByVal s As String) As Boolean

    ' si es numerico
    If Not EsNumerico(s) Then Return False

    ' comprueba si tiene largos validos en chile entre 6 y 12 
    ' (ej 281754, 171412510000, 5625849817, 0994919986)
    If Not TieneLargoValido(s, 6, 12) Then Return False

    Return True

  End Function

  ' comprueba si s tiene formato de fecha DD/MM/AAAA
  Public Shared Function EsFechaDDMMAAAA(ByVal s As String) As Boolean

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 10, 10) Then Return False

    ' verificar si se puede convertir a una fecha valida
    Dim fecha As Date

    Try

      fecha = Convert.ToDateTime(s)
      Return True

    Catch ex As Exception

      Return False

    End Try

  End Function

  ' comprueba si s tiene formato de fecha ISO (AAAAMMDD)
  Public Shared Function EsFechaISO(ByVal s As String) As Boolean

    ' si es numerico
    If Not EsNumerico(s) Then Return False

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 8, 8) Then Return False

    ' descomponer y probar si se puede convertir a una fecha
    Dim anio As String = s.Substring(0, 4)
    Dim mes As String = s.Substring(4, 2)
    Dim dia As String = s.Substring(6, 2)
    Dim fecha As Date

    Try

      fecha = Convert.ToDateTime(dia & "/" & mes & "/" & anio)
      Return True

    Catch ex As Exception

      Return False

    End Try


  End Function

  ' comprueba si s tiene formato de hora HH:MM
  Public Shared Function EsHora(ByVal s As String) As Boolean

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 5, 5) Then Return False

    ' descomponer
    Dim hora As String = s.Substring(1, 2)
    Dim minuto As String = s.Substring(4, 2)

    ' son numericos?
    If Not EsNumerico(hora) Then Return False
    If Not EsNumerico(minuto) Then Return False

    ' estan dentro del rango?
    If Not TieneRangoValido(CType(hora, Integer), 0, 23) Then Return False
    If Not TieneRangoValido(CType(minuto, Integer), 0, 59) Then Return False

    Return True

  End Function

  ' comprueba si s tiene formato de hora ISO HHMM
  Public Shared Function EsHoraISO(ByVal s As String) As Boolean

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 4, 4) Then Return False

    ' descomponer
    Dim hora As String = s.Substring(0, 2)
    Dim minuto As String = s.Substring(2, 2)

    ' son numericos?
    If Not EsNumerico(hora) Then Return False
    If Not EsNumerico(minuto) Then Return False

    ' estan dentro del rango?
    If Not TieneRangoValido(CType(hora, Integer), 0, 23) Then Return False
    If Not TieneRangoValido(CType(minuto, Integer), 0, 59) Then Return False

    Return True

  End Function

  ' comprueba si un string tiene caracteres validos
  Public Shared Function TieneCaracteresValidos(ByRef s As String) As Boolean
    s.Trim()
    Dim caracteresNoValidos As String = "(<|>)"
    Dim regExp As New System.Text.RegularExpressions.Regex(caracteresNoValidos)
    Dim contieneCaracteresNoValidos As Boolean = regExp.IsMatch(s)
    Return Not contieneCaracteresNoValidos
  End Function

  ' comprueba si dos passwords son validas y la segunda confirma a la primera
  Public Shared Function PasswordsValidos(ByVal password1 As String, ByVal password2 As String) As Boolean

    ' verificamos largos
    If Not TieneLargoValido(password1, 3, 20) OrElse Not TieneLargoValido(password2, 3, 20) Then
      Return False
    End If

    ' comprobamos qu sean iguales, si es cero son iguales
    Return (String.Compare(password1, password2, False) = 0)

  End Function

  ' comprueba si un rut ingresado es valido
  Public Shared Function EsRut(ByVal s As String) As Boolean

    Return True

  End Function

  ' revisa que una lista de Ids tenga formato correcto y le saca las comas de los extremos
  Public Shared Function ListaIdOK(ByRef lista As String) As Boolean

    Dim caracteresValidos As String = "1234567890,"

    If lista Is Nothing Then lista = String.Empty

    ' la lista solo debe tener los caracteres permitidos, sino devuelve falso
    For i As Integer = 0 To lista.Length - 1

      If caracteresValidos.IndexOf(lista.Chars(i)) = -1 Then Return False

    Next

    ' si hay una lista limpiamos las comas que puedan existir en los extremos
    If lista.StartsWith(",") Then lista = lista.Remove(0, 1)
    If lista.EndsWith(",") Then lista = lista.Remove(lista.Length - 1, 1)

    ' llegamos aca con la lista ok
    Return True

  End Function

  ' obtiene el nombre del mes a partir de su indice
  Public Shared Function ObtenerNombreMes(ByVal indiceMes As String) As String
    Dim nombreMes As String = ""
    Select Case indiceMes
      Case "01"
        nombreMes = "Enero"
      Case "02"
        nombreMes = "Febrero"
      Case "03"
        nombreMes = "Marzo"
      Case "04"
        nombreMes = "Abril"
      Case "05"
        nombreMes = "Mayo"
      Case "06"
        nombreMes = "Junio"
      Case "07"
        nombreMes = "Julio"
      Case "08"
        nombreMes = "Agosto"
      Case "09"
        nombreMes = "Septiembre"
      Case "10"
        nombreMes = "Octubre"
      Case "11"
        nombreMes = "Noviembre"
      Case "12"
        nombreMes = "Diciembre"
    End Select
    'retorna valor
    Return nombreMes
  End Function
#End Region

#Region "Funciones para Conversion"

  ' convierte una fecha de ISO a DD/MM/AAAA
  Public Shared Function ConvFechaISOaDDMMAAAA(ByVal s As String) As Date

    If Not EsFechaISO(s) Then Return Nothing

    ' descomponer y probar si se puede convertir a una fecha
    Dim anio As String = s.Substring(0, 4)
    Dim mes As String = s.Substring(4, 2)
    Dim dia As String = s.Substring(6, 2)
    Dim fecha As Date

    Try

      fecha = Convert.ToDateTime(dia & "/" & mes & "/" & anio)
      Return fecha

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  ' convierte una fecha de DD/MM/AAAA as ISO
  Public Shared Function ConvFechaDDMMAAAAaISO(ByVal s As String) As String
    Dim fecha As String = ""

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not EsFechaDDMMAAAA(s) Then Return Nothing

    ' verificar si se puede convertir a una fecha valida
    Dim dia As String = s.Substring(0, 2)
    Dim mes As String = s.Substring(3, 2)
    Dim ano As String = s.Substring(6, 4)

    Try

      fecha = ano & mes & dia
      Return fecha

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  Public Shared Function ConvFechaISOaDate(ByVal s As String) As Date

    If Not EsFechaISO(s) Then Return Nothing

    ' descomponer y probar si se puede convertir a una fecha
    Dim anio As String = s.Substring(0, 4)
    Dim mes As String = s.Substring(4, 2)
    Dim dia As String = s.Substring(6, 2)
    Dim fecha As Date

    Try
      fecha = DateSerial(anio, mes, dia)
      Return fecha
    Catch ex As Exception
      Return Nothing
    End Try

  End Function



  ' convierte una hora de ISO a HH:MM
  Public Shared Function ConvHoraISOaHHMM(ByVal s As String) As String

    If Not EsHoraISO(s) Then Return Nothing

    ' descomponer y probar si se puede convertir a una fecha
    Dim hora As String = s.Substring(0, 2)
    Dim minutos As String = s.Substring(2, 2)

    Try

      hora = hora & ":" & minutos
      Return hora

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  ' convierte una fecha de DD/MM/AAAA as ISO
  Public Shared Function ConvFechaHHMMaISO(ByVal s As String) As String

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not EsHora(s) Then Return Nothing

    ' descomponer y probar si se puede convertir a una fecha
    Dim hora As String = s.Substring(0, 2)
    Dim minutos As String = s.Substring(3, 2)

    Try

      hora = hora & minutos
      Return hora

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  'Funcion    : ConvertirUnidadMedidaArchivo
  'Descripcion: convierte una unidad de medida en otra
  'Por        : VSR, 15/07/2008
  Public Shared Function ConvertirUnidadMedidaArchivo(ByVal tamano As Double, ByVal aproximarMedidaMasCercana As Boolean, Optional ByVal cantidadDecimales As Integer = 2, Optional ByVal unidadMedidaOrigen As eUnidadMedidaArchivo = eUnidadMedidaArchivo.Bytes, Optional ByVal unidadMedidaDestino As eUnidadMedidaArchivo = eUnidadMedidaArchivo.KiloBytes) As String
    Dim UNIDAD_BASE As Integer = 1024
    Dim BYTES As Double = 1
    Dim KB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 1)
    Dim MB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 2)
    Dim GB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 3)
    Dim TB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 4)
    Dim dividendo As Double = 0
    Dim divisor As Double = 1
    Dim valor As Double = 0
    Dim retorno As String = ""
    Dim unidadMedida As String = ""

    'transforma el valor original en bytes
    Select Case unidadMedidaOrigen
      Case eUnidadMedidaArchivo.Bytes
        dividendo = tamano * BYTES
      Case eUnidadMedidaArchivo.KiloBytes
        dividendo = tamano * KB_EN_BYTES
      Case eUnidadMedidaArchivo.MegaBytes
        dividendo = tamano * MB_EN_BYTES
      Case eUnidadMedidaArchivo.GigaBytes
        dividendo = tamano * GB_EN_BYTES
      Case eUnidadMedidaArchivo.TeraBytes
        dividendo = tamano * TB_EN_BYTES
    End Select

    'determina en que unidad se va a convertir
    Select Case unidadMedidaDestino
      Case eUnidadMedidaArchivo.Bytes
        divisor = BYTES
        unidadMedida = "bytes"
      Case eUnidadMedidaArchivo.KiloBytes
        divisor = KB_EN_BYTES
        unidadMedida = "Kb"
      Case eUnidadMedidaArchivo.MegaBytes
        divisor = MB_EN_BYTES
        unidadMedida = "Mb"
      Case eUnidadMedidaArchivo.GigaBytes
        divisor = GB_EN_BYTES
        unidadMedida = "Gb"
      Case eUnidadMedidaArchivo.TeraBytes
        divisor = TB_EN_BYTES
        unidadMedida = "Tb"
    End Select

    'calcula el valor
    valor = Math.Round(dividendo / divisor, cantidadDecimales)
    retorno = valor.ToString & " " & unidadMedida

    'si se aproxima a la unidad mas cercana entonces se calcula de nuevo
    If aproximarMedidaMasCercana Then
      If dividendo < KB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.Bytes)
      ElseIf dividendo >= KB_EN_BYTES And dividendo < MB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.KiloBytes)
      ElseIf dividendo >= MB_EN_BYTES And dividendo < GB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.MegaBytes)
      ElseIf dividendo >= GB_EN_BYTES And dividendo < TB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.GigaBytes)
      ElseIf dividendo >= TB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.TeraBytes)
      End If
    End If

    'retorna valor
    Return retorno
  End Function

#End Region

#Region "Funciones que entregan datos"

  Public Shared Function FechaISO(Optional ByVal ahora As DateTime = Nothing, Optional ByVal incluirHora As Boolean = True, Optional ByVal incluirSegundoEnHora As Boolean = True) As String
    If ahora = Nothing Then ahora = Herramientas.MyNow()

    Dim anio As String = "0000" & ahora.Year.ToString
    Dim mes As String = "00" & ahora.Month.ToString
    Dim dia As String = "00" & ahora.Day.ToString
    Dim hora As String = "00" & ahora.Hour.ToString
    Dim minutos As String = "00" & ahora.Minute.ToString
    Dim segundos As String = "00" & ahora.Second.ToString
    Dim horaMinutos As String = Right(hora, 2) & Right(minutos, 2)
    Dim horaMinutosSegundos As String = Right(hora, 2) & Right(minutos, 2) & Right(segundos, 2)

    If incluirHora Then
      If incluirSegundoEnHora Then
        hora = horaMinutosSegundos
      Else
        hora = horaMinutos
      End If
    Else
      hora = ""
    End If

    Return Right(anio, 4) & Right(mes, 2) & Right(dia, 2) & hora
  End Function

  ' dada una variable datetime entrega la hora en formato ISO, por default entregara la hora actual
  Public Shared Function HoraISO(Optional ByVal ahora As DateTime = Nothing) As String

    If ahora = Nothing Then ahora = Herramientas.MyNow()

    Dim hora As String = "00" & ahora.Hour.ToString
    Dim minutos As String = "00" & ahora.Minute.ToString

    Return Right(hora, 2) & Right(minutos, 2)

  End Function

  ' devuelve un string con la fecha y la hora en formato iso yyyymmddhhnn
  Public Shared Function FechaHoraISO(Optional ByVal ahora As DateTime = Nothing) As String

    If ahora = Nothing Then ahora = Herramientas.MyNow()

    Dim anio As String = "0000" & ahora.Year.ToString
    Dim mes As String = "00" & ahora.Month.ToString
    Dim dia As String = "00" & ahora.Day.ToString
    Dim hora As String = "00" & ahora.Hour.ToString
    Dim minutos As String = "00" & ahora.Minute.ToString

    Return Right(anio, 4) & Right(mes, 2) & Right(dia, 2) & Right(hora, 2) & Right(minutos, 2)

  End Function

  ' a partir de una cadena con fecha y hora iso, devuelve la fecha "como se lee"
  Public Shared Function DesplegarFecha(ByVal fechaFormatoISO As String) As String

    If Not TieneLargoValido(fechaFormatoISO, 12, 12) Or Not EsNumerico(fechaFormatoISO) Then Return "No disponible"

    Dim anio As String = fechaFormatoISO.Substring(0, 4)
    Dim mes As String = fechaFormatoISO.Substring(4, 2)
    Dim dia As String = fechaFormatoISO.Substring(6, 2)
    Dim hora As String = fechaFormatoISO.Substring(8, 2)
    Dim minutos As String = fechaFormatoISO.Substring(10, 2)

    Return dia & "/" & mes & "/" & anio & " a las " & hora & ":" & minutos & " Hrs."

  End Function

  ' a partir del numero del mes obtiene su nombre
  Public Shared Function ObtenerNombreMes(ByVal mes As Integer) As String

    Dim nombreMeses As New SortedList

    nombreMeses.Add(1, "Enero")
    nombreMeses.Add(2, "Febrero")
    nombreMeses.Add(3, "Marzo")
    nombreMeses.Add(4, "Abril")
    nombreMeses.Add(5, "Mayo")
    nombreMeses.Add(6, "Junio")
    nombreMeses.Add(7, "Julio")
    nombreMeses.Add(8, "Agosto")
    nombreMeses.Add(9, "Septiembre")
    nombreMeses.Add(10, "Octubre")
    nombreMeses.Add(11, "Noviembre")
    nombreMeses.Add(12, "Diciembre")

    If mes >= 1 AndAlso mes <= 12 Then
      Return nombreMeses.Item(mes)
    Else
      Return String.Empty
    End If

  End Function

  'obtiene hora de una hora ISO
  Public Shared Function ObtenerHoraDeHoraISO(ByVal horaISO As String) As String
    Dim hora As String

    If Not EsHoraISO(horaISO) Then Return String.Empty

    Try
      hora = horaISO.Substring(0, 2)
      Return hora
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  'obtiene minutos de una hora ISO
  Public Shared Function ObtenerMinutosDeHoraISO(ByVal horaISO As String) As String
    Dim minutos As String

    If Not EsHoraISO(horaISO) Then Return String.Empty

    Try
      minutos = horaISO.Substring(2, 2)
      Return minutos
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ' a partir de un arraylist devuelve una lista <li> de los elementos
  Public Shared Function ObtenerListaErrores(ByVal errores As ArrayList) As String

    Dim resultado As String

    ' construir cadena para desplegar los errores
    resultado = "<ul><li>Los datos ingresados presentan errores</li>"
    For Each _error As String In errores
      resultado &= "<li>" & _error & "</li>"
    Next
    resultado &= "</ul>"

    Return resultado

  End Function

  ' escapar comillas enrutinas JS
  Public Shared Function EscaparComillasJS(ByVal texto As String) As String
    'cambio los \n por retorno de carro (esto es necesario, pues a continuacion voy a sacar los "\" para que no puedan dañar)
    texto = Replace(texto, "\n", vbCrLf)
    'escapa el caracter de escape
    texto = Replace(texto, "\", "\\")
    'escapa comillas simples
    texto = Replace(texto, "'", "\'")
    'cambia retorno de carro por \n
    texto = Replace(texto, vbCrLf, "\n")
    'cambia retorno de carro simple por \n
    texto = Replace(texto, vbCr, "\n")
    'cambia linefeed simple por \n
    texto = Replace(texto, vbLf, "\n")
    'escapa comillas dobles
    Return Trim(Replace(texto, """", "\042"))
  End Function

  ' devuelve el caracter que va entre dos items de una lista cuando se recorre (vacio, "," y " y ")
  Public Shared Function Pegamento(ByVal indice As Integer, ByVal tope As Integer) As String

    If indice = tope Then
      Return String.Empty
    End If

    If indice < tope And indice < tope - 1 Then
      Return ", "
    End If

    If indice < tope And indice = tope - 1 Then
      Return " y "
    End If

    Return String.Empty

  End Function

  ' reemplaza los caracteres de acentos y ñ por su entidad html
  Public Shared Function ReemplazarAcentos(ByVal s As String, Optional ByVal html As Boolean = True) As String

    If (html) Then
      s = Replace(s, "á", "&aacute;")
      s = Replace(s, "é", "&eacute;")
      s = Replace(s, "í", "&iacute;")
      s = Replace(s, "ó", "&oacute;")
      s = Replace(s, "ú", "&uacute;")

      s = Replace(s, "Á", "&Aacute;")
      s = Replace(s, "É", "&Eacute;")
      s = Replace(s, "Í", "&Iacute;")
      s = Replace(s, "Ó", "&Oacute;")
      s = Replace(s, "Ú", "&Uacute;")

      s = Replace(s, "ñ", "&ntilde;")
      s = Replace(s, "Ñ", "&Ntilde;")

    Else
      s = Replace(s, "á", "a")
      s = Replace(s, "é", "e")
      s = Replace(s, "í", "i")
      s = Replace(s, "ó", "o")
      s = Replace(s, "ú", "u")

      s = Replace(s, "Á", "A")
      s = Replace(s, "É", "E")
      s = Replace(s, "Í", "I")
      s = Replace(s, "Ó", "O")
      s = Replace(s, "Ú", "U")

      s = Replace(s, "ñ", "n")
      s = Replace(s, "Ñ", "N")
    End If

    Return s

  End Function

  ' wrapper para sanitizar las entradas de usuario
  Public Shared Function LimpiarInput(ByVal s As String) As String
    Dim aCaracteresInvalidos As String() = CARACTERES_NO_VALIDOS_REGEXP.Split("|")
    Dim caracterInvalido As String
    Dim i As Integer

    s = s.Trim()
    For i = 0 To aCaracteresInvalidos.Length - 1
      caracterInvalido = aCaracteresInvalidos(i)
      s = s.Replace(caracterInvalido, "")
    Next

    Return s
  End Function

  'Metodo     : ObtenerListadoMailDesdeDataSet()
  'Descripcion: construye listado de email separados por ";" a partir de una tabla de un dataset
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 11/07/2008
  Public Shared Function ObtenerListadoMailDesdeDataSet(ByVal ds As DataSet, ByVal indiceTabla As Integer, ByVal nombreColumnaMail As String, Optional ByVal separador As String = ";") As String
    If ds Is Nothing Then
      Return ""
      Exit Function
    End If

    Dim sb As New StringBuilder
    Dim i As Integer
    Dim dr As DataRow
    Dim email As String

    Try
      Dim totalRegistros As Integer = ds.Tables(indiceTabla).Rows.Count
      If totalRegistros > 0 Then
        For i = 0 To totalRegistros - 1
          dr = ds.Tables(indiceTabla).Rows(i)
          email = dr.Item(nombreColumnaMail)
          If Herramientas.EsEmail(email) AndAlso InStr(sb.ToString(), email & separador) = 0 Then
            sb.Append(email)
            If i < totalRegistros - 1 Then sb.Append(separador)
          End If
        Next
      Else
        sb.Append("")
      End If
      'retorna valor
      Return sb.ToString
    Catch ex As Exception
      Return ""
    End Try
  End Function

  'Metodo     : ObtenerTablaHTMLDesdeDataSet()
  'Descripcion: construye el cuerpo del mensaje
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 26/06/2008
  Public Shared Function ObtenerTablaHTMLDesdeDataSet(ByVal ds As DataSet, Optional ByVal indiceTabla As Integer = 0) As String
    Dim bodyMail As String = ""
    Dim i, j As Integer
    Dim sb As New StringBuilder
    Dim totalRegistros As Integer = ds.Tables(indiceTabla).Rows.Count
    Dim dt As DataTable = ds.Tables(indiceTabla)
    Dim dr As DataRow = Nothing
    Dim totalColumnas As Integer
    Dim nombreColumna, item, valorItem As String

    Try
      If totalRegistros > 0 Then
        'obtiene total de columnas
        totalColumnas = dt.Columns.Count

        sb.Append("<table border=""1"" cellpadding=""0"" cellspacing=""0"">")
        sb.Append(" <tr>")
        'construye cabeceras de la tabla
        For i = 0 To totalColumnas - 1
          If Not dt.Columns.Item(i).ColumnName.StartsWith("_") Then
            nombreColumna = dt.Columns.Item(i).ColumnName
            sb.Append("<th>" & ReemplazarMarcasEspecialesSQL(nombreColumna) & "</th>")
          End If
        Next
        sb.Append(" </tr>")
        'recorre el dataset como si fuera una matriz [i=>fila | j=>columna]
        For i = 0 To totalRegistros - 1
          sb.Append(" <tr>")
          dr = dt.Rows(i)
          'recorre las columnas para obtener sus valores
          For j = 0 To totalColumnas - 1
            If Not dt.Columns.Item(j).ColumnName.StartsWith("_") Then
              item = dt.Columns.Item(j).ColumnName
              valorItem = IsNull(dr.Item(item), "")
              sb.Append("<td>" & valorItem & "</td>")
            End If
          Next
          sb.Append(" </tr>")
        Next
        sb.Append("</table>")
        bodyMail = sb.ToString
      End If
      'retorna valor
      Return bodyMail
    Catch ex As Exception
      'retorna valor
      Return bodyMail
    End Try
  End Function

  'Metodo     : ObtenerContenidoExcelConTablaHTML()
  'Descripcion: construye html para guardarlo como excel
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 11/07/2008
  Public Shared Function ObtenerContenidoExcelConTablaHTML(ByVal bodyHTML As String) As String
    Dim sb As New StringBuilder
    sb.Append("<html>")
    sb.Append("<head>")
    sb.Append("</head>")
    sb.Append("<body>")
    sb.AppendLine(bodyHTML)
    sb.Append("</body>")
    sb.Append("</html>")
    Return sb.ToString
  End Function

  'Metodo     : ObtenerContenidoLogConTablaHTML()
  'Descripcion: construye html para guardarlo como log
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 11/07/2008
  Public Shared Function ObtenerContenidoLogConTablaHTML(ByVal bodyHTML As String, ByVal nombreJob As String) As String
    Dim sb As New StringBuilder
    sb.Append("<html>")
    sb.Append("<head>")
    sb.Append("</head>")
    sb.Append("<body>")
    sb.Append("<div>Iniciando tarea: " & nombreJob & "</div>")
    sb.Append("<div>Fecha ejecución: " & Herramientas.MyNow().ToString & "</div>")
    sb.Append("Ambiente: " & ObtenerAmbienteEjecucion())
    sb.Append("<div>Modo Debug: " & MailModoDebug.ToString & "<br><br></div>")
    sb.Append("<div>" & bodyHTML & "</div>")
    sb.Append("<div><br>Fin tarea: " & nombreJob & "</div>")
    sb.Append("</body>")
    sb.Append("</html>")
    Return sb.ToString
  End Function

  'Metodo     : CrearDataSetDesdeDataViewConRowFilter()
  'Descripcion: crea un dataset a partir de un dataview filtrado
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 27/08/2008
  Public Shared Function CrearDataSetDesdeDataViewConRowFilter(ByVal dv As DataView) As DataSet
    Dim ds As New DataSet
    Dim dt As DataTable = dv.Table.Clone()

    'llena la tabla con las filas del dataview
    For Each drv As DataRowView In dv
      dt.ImportRow(drv.Row)
    Next

    'agregar la nueva tabla al dataset
    ds.Tables.Add(dt)

    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' Obtiene el ambiente en donde se esta ejecutando la aplicacion:Desarrollo, Demo, Producción, etc.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAmbienteEjecucion() As String
    Dim ambiente As String = Configuracion.Leer("webAmbienteEjecucion")
    ambiente = IIf(String.IsNullOrEmpty(ambiente), "INDEFINIDO", ambiente).ToString.ToUpper

    If (ambiente = "PRODUCCION" Or ambiente = "PRODUCCIÓN") Then
      ambiente = "PRODUCCION"
    End If

    Return ambiente
  End Function

  ''' <summary>
  ''' Retorna el pais asociado al sitio
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function PaisSitio() As String
    Dim _texto As String = Configuracion.Leer("webPais")
    Return _texto
  End Function

  ''' <summary>
  ''' convierte una fecha "dd/mm/aaaa" en "dd de mm del aaaa"
  ''' </summary>
  ''' <param name="fechaDDMMAAAA"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertirFechaDDMMAAAAEnTexto(ByVal fechaDDMMAAAA As String) As String
    Dim texto As String = ""
    Dim aFecha As String()
    Dim dia, mes, ano As String
    Try
      If Not EsVacio(fechaDDMMAAAA) Then
        If EsFechaDDMMAAAA(fechaDDMMAAAA) Then
          fechaDDMMAAAA = fechaDDMMAAAA.Replace("-", "/")
          aFecha = fechaDDMMAAAA.Split("/")
          dia = aFecha(0)
          mes = aFecha(1)
          ano = aFecha(2)
          mes = ObtenerNombreMes(mes)
          texto = dia & " de " & mes & " del " & ano
        End If
      End If
    Catch ex As Exception
      texto = ""
    End Try
    Return texto
  End Function

  ''' <summary>
  ''' obtiene el primer dia del mes de una fecha dada
  ''' Para calcular el primer día del mes, creamos una fecha con la función DateSerial, pasándole como parámetros el año, el mes y un 1 para el día
  ''' </summary>
  ''' <param name="fecha"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 03/08/2009</remarks>
  Public Shared Function ObtenerPrimerDiaMes(ByVal fecha As Date) As Date
    Return DateSerial(Year(fecha), Month(fecha), 1)
  End Function

  ''' <summary>
  ''' obtiene ultimo dia del mes de una fecha dada
  ''' Para calcular el último día del mes, también creamos una fecha con la función DateSerial, pasándole como parámetros el año,
  ''' el mes + 1 y un 0. Con esto le decimos a la función que queremos el día anterior al primer día del siguiente mes
  ''' </summary>
  ''' <param name="fecha"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 03/08/2009</remarks>
  Public Shared Function ObtenerUltimoDiaMes(ByVal fecha As Date) As Date
    Return DateSerial(Year(fecha), Month(fecha) + 1, 0)
  End Function

  ''' <summary>
  ''' reemplaza marcas especiales en un texto
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 03/08/2009</remarks>
  Public Shared Function ReemplazarMarcasEspecialesSQL(ByVal texto As String) As String
    Dim nuevoTexto As String
    Try
      If Not String.IsNullOrEmpty(texto) Then
        texto = texto.Replace("{$FECHA_MES_ANTERIOR}", Format(Herramientas.ObtenerUltimoDiaMes(DateAdd(DateInterval.Month, -1, Herramientas.MyNow())), "MMMM yyyy"))
        texto = texto.Replace("{$FECHA_MES_ANTERIOR_1_MES}", Format(Herramientas.ObtenerUltimoDiaMes(DateAdd(DateInterval.Month, -2, Herramientas.MyNow())), "MMMM yyyy"))
        texto = texto.Replace("{$FECHA_MES_ANTERIOR_2_MESES}", Format(Herramientas.ObtenerUltimoDiaMes(DateAdd(DateInterval.Month, -3, Herramientas.MyNow())), "MMMM yyyy"))
      End If
      nuevoTexto = texto
    Catch ex As Exception
      nuevoTexto = texto
    End Try
    Return nuevoTexto.ToUpper
  End Function

  ''' <summary>
  ''' elimina ultimo caracter del texto
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EliminaUltimoCaracterTexto(ByVal texto As String) As String
    Dim largoTexto, strTextoSinComaFinal As String

    'Calcula largo de texto
    largoTexto = Len(texto)

    If largoTexto = 0 Then
      strTextoSinComaFinal = ""
    Else
      'Quita la coma final de la cadena
      strTextoSinComaFinal = Mid(texto, 1, largoTexto - 1)
    End If

    'Asigna valor de cadena
    Return strTextoSinComaFinal

  End Function

  ''' <summary>
  ''' elimina columnas de un dataset que comienzen con un cierto caracter
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="indiceTabla"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EliminarColumnasEspecialesDataset(ByVal ds As DataSet, Optional ByVal indiceTabla As Integer = 0, Optional ByVal caracterInicialColumna As String = "_") As DataSet
    Dim newDataset As New DataSet
    Dim totalColumnas, i As Integer
    Dim dt As DataTable
    Dim dc As DataColumn
    Dim listadoIndicesEliminar As String = ""
    Dim aListadoIndicesEliminar As String()
    Dim indiceColumna As Integer

    Try
      dt = ds.Tables(indiceTabla)
      totalColumnas = dt.Columns.Count

      'construye cabeceras de la tabla
      For i = 0 To totalColumnas - 1
        If dt.Columns.Item(i).ColumnName.StartsWith(caracterInicialColumna) Then
          listadoIndicesEliminar &= i & ","
        End If
      Next
      listadoIndicesEliminar = EliminaUltimoCaracterTexto(listadoIndicesEliminar)
      If Not String.IsNullOrEmpty(listadoIndicesEliminar) Then
        aListadoIndicesEliminar = listadoIndicesEliminar.Split(",")
        For i = aListadoIndicesEliminar.Length - 1 To 0 Step -1
          indiceColumna = aListadoIndicesEliminar(i)
          dc = dt.Columns(indiceColumna)
          dt.Columns.Remove(dc)
        Next
      End If
      newDataset = ds.Copy
    Catch ex As Exception
      newDataset = ds
    End Try
    Return newDataset
  End Function

  'Metodo     : ObtenerKeyXML()
  'Descripcion: construye listado de key desde el xml separados por ";" a partir de una tabla de un dataset
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 11/07/2008
  Public Shared Function ObtenerKeyXML(ByVal ds As DataSet, ByVal indiceTabla As Integer, ByVal nombreKey As String, Optional ByVal separador As String = ";") As String
    If ds Is Nothing Then
      Return ""
      Exit Function
    End If

    Dim sb As New StringBuilder
    Dim valor As String

    Try

      For Each row As DataRow In ds.Tables(indiceTabla).Rows
        valor = row.Item(nombreKey)
        sb.Append(valor)
        sb.Append(separador)
      Next

      If sb.Length = 0 Then
        Return ""
      Else
        'retorna valor
        Return sb.ToString.Remove(sb.ToString.Length - 1, 1)
      End If

    Catch ex As Exception
      Return ""
    End Try
  End Function

#End Region

#Region "Fechas"
  ''' <summary>
  ''' obtiene indice del dia de la semana consultada. Los indices devueltos son: 
  ''' domingo->0; lunes->1; martes->2; miercoles->3; jueves->4; viernes->5; sabado->6;
  ''' </summary>
  ''' <param name="dia"></param>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerIndiceDiaSemana(ByVal dia As Integer, ByVal mes As Integer, ByVal ano As Integer) As Integer
    Dim indiceDiaSemana As Integer
    'obtiene indice del dia de la semana
    indiceDiaSemana = Weekday(DateSerial(ano, mes, dia)) - 1
    'retorna valor
    Return indiceDiaSemana
  End Function

  ''' <summary>
  ''' verificar si la fecha dada corresponde al dia lunes
  ''' </summary>
  ''' <param name="dia"></param>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function EsLunes(ByVal dia As Integer, ByVal mes As Integer, ByVal ano As Integer) As Boolean
    Dim indiceDiaSemana As Integer
    Dim boolEsLunes As Boolean
    'obtiene indice del dia de la semana
    indiceDiaSemana = ObtenerIndiceDiaSemana(dia, mes, ano)

    'si es lunes devuelve verdadero
    If indiceDiaSemana = INDICE_DIA_LUNES Then
      boolEsLunes = True
    Else
      boolEsLunes = False
    End If
    'retorna valor
    Return boolEsLunes
  End Function

  ''' <summary>
  ''' obtiene el dia lunes de la semana en la cual esta la fecha consultada con formato dd/mm/aaaa
  ''' </summary>
  ''' <param name="dia"></param>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerLunesDeLaSemana(ByVal dia As Integer, ByVal mes As Integer, ByVal ano As Integer) As String
    Dim strFechaDada, newFecha, strFechaLunes As String
    Dim indiceDiaSemana As Integer
    strFechaDada = dia & "-" & mes & "-" & ano
    indiceDiaSemana = ObtenerIndiceDiaSemana(dia, mes, ano)
    'si es domingo la fecha dada entonces resta 6 dias para atras
    If indiceDiaSemana = 0 Then
      newFecha = DateAdd("d", -6, strFechaDada)
    Else
      newFecha = DateAdd("d", -1 * (indiceDiaSemana - INDICE_DIA_LUNES), strFechaDada)
    End If
    'construye nueva fecha para el dia lunes
    strFechaLunes = Right("00" & Day(newFecha), 2) & "/" & Right("00" & Month(newFecha), 2) & "/" & Right("0000" & Year(newFecha), 4)
    'retorna valor
    Return strFechaLunes
  End Function

  ''' <summary>
  ''' obtiene el ultimo dia del mes
  ''' </summary>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerUltimoDiaMes(ByVal mes As Integer, ByVal ano As Integer) As Integer
    Dim diaMes As Integer
    Select Case mes
      Case 1, 3, 5, 7, 8, 10, 12
        diaMes = 31
      Case 4, 6, 9, 11
        diaMes = 30
      Case 2
        If IsDate(ano & "-" & mes & "-" & "29") Then
          diaMes = 29
        Else
          diaMes = 28
        End If
      Case Else
        diaMes = 0
    End Select
    'retorna valor
    Return diaMes
  End Function

  ''' <summary>
  ''' obtiene la fecha del primer lunes del mes con formato dd/mm/aaaa
  ''' </summary>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerPrimerLunesMes(ByVal mes As Integer, ByVal ano As Integer) As String
    Dim strPrimerDiaMes, strFechaLunes As String
    Dim newFecha As String = ""
    Dim indiceDiaSemana As Integer

    strPrimerDiaMes = "1" & "-" & mes & "-" & ano
    indiceDiaSemana = ObtenerIndiceDiaSemana("1", mes, ano)

    'si es lunes la fecha dada entonces la asigna como fecha del primer lunes del mes, sino calcula cual es el siguiente lunes
    If indiceDiaSemana = 1 Then
      newFecha = strPrimerDiaMes
    Else
      Select Case indiceDiaSemana
        Case 0
          newFecha = DateAdd("d", 1, strPrimerDiaMes)
        Case 2
          newFecha = DateAdd("d", 6, strPrimerDiaMes)
        Case 3
          newFecha = DateAdd("d", 5, strPrimerDiaMes)
        Case 4
          newFecha = DateAdd("d", 4, strPrimerDiaMes)
        Case 5
          newFecha = DateAdd("d", 3, strPrimerDiaMes)
        Case 6
          newFecha = DateAdd("d", 2, strPrimerDiaMes)
      End Select
    End If
    'construye nueva fecha para el dia lunes
    strFechaLunes = Right("00" & Day(newFecha), 2) & "/" & Right("00" & Month(newFecha), 2) & "/" & Right("0000" & Year(newFecha), 4)
    'retorna valor
    Return strFechaLunes
  End Function

  ''' <summary>
  ''' obtiene el ultimo domingo del mes con formato dd/mm/aaaa
  ''' </summary>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerUltimoDomingoMes(ByVal mes As Integer, ByVal ano As Integer) As String
    Dim newFecha, strFechaDomingo, strPrimerLunesMesSiguiente As String
    'construye mes siguiente al consultado para obtener el primer lunes de ese mes
    'y asi poder obtener el ultimo domingo del mes consultado
    If mes = 12 Then
      mes = 1
      ano = ano + 1
    Else
      mes = mes + 1
    End If
    'obtiene fecha del primer lunes del mes siguiente
    strPrimerLunesMesSiguiente = ObtenerPrimerLunesMes(mes, ano)
    strPrimerLunesMesSiguiente = Replace(strPrimerLunesMesSiguiente, "/", "-")
    'obtiene ultimo domingo del mes actual
    newFecha = DateAdd("d", -1, strPrimerLunesMesSiguiente)
    'construye nueva fecha para el dia lunes
    strFechaDomingo = Right("00" & Day(newFecha), 2) & "/" & Right("00" & Month(newFecha), 2) & "/" & Right("0000" & Year(newFecha), 4)
    'retorna valor
    Return strFechaDomingo
  End Function

  ''' <summary>
  ''' obtiene el dia domingo de la semana en la cual esta la fecha consultada
  ''' </summary>
  ''' <param name="diaLunesSemana"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerDomingoDeLaSemana(ByVal diaLunesSemana As String) As String
    Dim arrFechaLunes As String()
    Dim newLunes, newDomingo, strFechaDomingo As String
    Dim dia, mes, ano As String
    'obtiene fecha valida para el sistema
    arrFechaLunes = Split(diaLunesSemana, "/")
    newLunes = DateSerial(arrFechaLunes(2), arrFechaLunes(1), arrFechaLunes(0))
    newDomingo = Replace(DateAdd("d", 6, newLunes), "-", "/")

    If Day(newDomingo) <= 9 Then dia = "0" & Day(newDomingo) Else dia = Day(newDomingo)
    If Month(newDomingo) <= 9 Then mes = "0" & Month(newDomingo) Else mes = Month(newDomingo)
    ano = Year(newDomingo)

    'construye fecha
    strFechaDomingo = dia & "/" & mes & "/" & ano
    'retorna valor
    Return strFechaDomingo
  End Function

  ''' <summary>
  ''' retorna el nombre del dia de la semana en formato largo y corto
  ''' </summary>
  ''' <param name="indiceDiaSemana"></param>
  ''' <param name="usarNombreCorto2Letras"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerNombreDiaSemanaPorIndice(ByVal indiceDiaSemana As Integer, Optional ByVal usarNombreCorto2Letras As Boolean = False, Optional ByVal usarAcento As Boolean = False) As String
    Dim nameDiaSemana As String = ""
    'segun el indice busce el nombre
    Select Case indiceDiaSemana
      Case 0, 7
        nameDiaSemana = IIf(usarNombreCorto2Letras, "DO", "DOMINGO")
      Case 1
        nameDiaSemana = IIf(usarNombreCorto2Letras, "LU", "LUNES")
      Case 2
        nameDiaSemana = IIf(usarNombreCorto2Letras, "MA", "MARTES")
      Case 3
        nameDiaSemana = IIf(usarNombreCorto2Letras, "MI", "MIERCOLES")
      Case 4
        nameDiaSemana = IIf(usarNombreCorto2Letras, "JU", "JUEVES")
      Case 5
        nameDiaSemana = IIf(usarNombreCorto2Letras, "VI", "VIERNES")
      Case 6
        nameDiaSemana = IIf(usarNombreCorto2Letras, "SA", "SABADO")
    End Select

    If usarAcento Then
      Select Case indiceDiaSemana
        Case 3
          nameDiaSemana = IIf(usarNombreCorto2Letras, "MI", "MIÉRCOLES")
        Case 6
          nameDiaSemana = IIf(usarNombreCorto2Letras, "SA", "SÁBADO")
      End Select
    End If

    'devuelve valor a la funcion
    Return nameDiaSemana
  End Function

#End Region

#Region "Shared"
  ''' <summary>
  ''' crea una espera para ejecutar el proceso que sigue
  ''' </summary>
  ''' <param name="milisegundos"></param>
  ''' <remarks></remarks>
  Public Shared Sub DormirProceso(ByVal milisegundos As Integer)
    Try
      Thread.Sleep(milisegundos)
    Catch ex As Exception
      'no hace nada
    End Try
  End Sub

  Public Shared Function PropCase(ByVal strTemp As String) As String

    Dim intFound As Integer
    Dim strTempName As String = String.Empty

    strTemp.ToLower()

    Do
      strTempName &= UCase(Left(strTemp, 1))
      strTemp = Right(strTemp, Len(strTemp) - 1)
      intFound = InStr(strTemp, " ")

      If intFound <> 0 Then
        strTempName = strTempName & Left(strTemp, intFound)
        strTemp = Right(strTemp, Len(strTemp) - intFound)
      Else
        strTempName = strTempName & strTemp
        Exit Do
      End If

    Loop While intFound <> 0
    Return strTempName

  End Function

  'Metodo     : FormatearConSeparadorMiles()
  'Descripcion: formate un numero con separador de miles
  'Parametros : <>
  'Retorno    : <nada>
  'Por        : VSR, 14/05/2008
  Public Shared Function FormatearConSeparadorMiles(ByVal valor As String, Optional ByVal cantidadDecimales As String = "0") As String
    Dim nuevoValor As String
    'si trae puntos se los quita
    valor = valor.Replace(".", "")
    If EsNumerico(valor) Then
      nuevoValor = FormatNumber(valor, cantidadDecimales, TriState.True, TriState.False, TriState.True)
    Else
      nuevoValor = ""
    End If
    'retorna valor
    Return nuevoValor
  End Function

  'Metodo     : ObtenerSimboloMoneda()
  'Descripcion: obtiene el signo de la moneda segun el pais del servidor
  'Parametros : <>
  'Retorno    : <nada>
  'Por        : VSR, 14/05/2008
  Public Shared Function ObtenerSimboloMoneda() As String
    Dim signoMoneda As String = CurrentCulture.NumberFormat.CurrencySymbol
    Return signoMoneda
  End Function

  'Metodo     : ObtenerSeparadorMil()
  'Descripcion: obtiene el caracter del separador de mil
  'Parametros : <>
  'Retorno    : <nada>
  'Por        : VSR, 14/05/2008
  Public Shared Function ObtenerSeparadorMil() As String
    Dim separador As String = CurrentCulture.NumberFormat.CurrencyGroupSeparator
    Return separador
  End Function

  'Metodo     : ObtenerSeparadorDecimal()
  'Descripcion: obtiene el caracter del separador de decimal
  'Parametros : <>
  'Retorno    : <nada>
  'Por        : VSR, 14/05/2008
  Public Shared Function ObtenerSeparadorDecimal() As String
    Dim separador As String = CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    Return separador
  End Function

  'Metodo     : ReemplazarPorImagenVacia()
  'Descripcion: reemplaza un valor vacio por una imagen en blanco
  'Parametros : <>
  'Retorno    : <nada>
  'Por        : VSR, 11/06/2008
  Public Shared Function ReemplazarPorImagenVacia(ByVal texto As String, Optional ByVal anchoImagenVacia As String = "1", Optional ByVal altoImagenVacia As String = "1") As String
    Dim valorRetorno As String
    Dim styleImage As String = "style=""width:@width;height:@height;"""
    styleImage = styleImage.Replace("@width", anchoImagenVacia)
    styleImage = styleImage.Replace("@height", altoImagenVacia)

    If texto <> String.Empty Then
      valorRetorno = texto
    Else
      valorRetorno = "<img src=""../img/_blank.gif"" alt="""" " & styleImage & " />"
    End If
    'retorna valor
    Return valorRetorno
  End Function

  'Metodo     : ObtenerTextoImputadoComodin()
  'Descripcion: devuelve texto del imputado cero
  'Parametros : <>
  'Retorno    : <nada>
  'Por        : VSR, 11/06/2008
  Public Shared Function ObtenerTextoImputadoComodin() As String
    'retorna valor
    Return "Todos los que resulten responsables"
  End Function

  ''' <summary>
  ''' Determina si el envio de mail esta en modo debug o no
  ''' </summary>
  ''' <returns>True o False</returns>
  ''' <remarks></remarks>
  Public Shared Function MailModoDebug() As Boolean
    Dim valor As String = ""
    Dim esModoDebug As Boolean = True
    Try
      valor = Configuracion.Leer("jobMailModoDebug").ToLower
      esModoDebug = IIf(valor = "true" Or valor = "verdadero" Or valor = "1", True, False)
      Return esModoDebug
    Catch ex As Exception
      Return True
    End Try
  End Function



  ''' <summary>
  ''' Obtiene el o los mails de pruebas
  ''' </summary>
  ''' <returns>String con los mail separados por ";"</returns>
  ''' <remarks></remarks>
  Public Shared Function MailPruebas() As String
    Dim valor As String = String.Empty
    Try
      valor = Configuracion.Leer("jobListadoMailPruebas")
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' Obtiene el o los mails de los administradores
  ''' </summary>
  ''' <returns>String con los mail separados por ";"</returns>
  ''' <remarks></remarks>
  Public Shared Function MailAdministrador() As String
    Dim valor As String = String.Empty
    Try
      valor = Configuracion.Leer("jobListadoMailAdministrador")
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' obtiene servidor correo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MailHostHD() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.mailHostHD
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' obtiene servidor correo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MailHost() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.mailHost
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' obtiene email del emisor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MailEmisor() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.mailEmailEmisor
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' obtiene password del email emisor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MailPasswordEmisor() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.mailPasswordEmailEmisor
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' obtiene nombre del email emisor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MailNombreEmisor() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.mailEmailNombre
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' rellena un string hasta el largo maximo con caracter determinado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RellenarString(ByVal s As String, ByVal largo As Integer, Optional ByVal caracter As String = " ") As String
    'crea un string de *caracter de *largo especificado
    Dim output As String = String.Empty
    output = StrDup(largo, caracter)
    'le pegamos el relleno a la cadena
    output = s & output
    'retorna valor
    Return output.Substring(0, largo)
  End Function

  ''' <summary>
  ''' extrae el texto de una cadena entre dos palabras dadas
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <param name="palabraInicial"></param>
  ''' <param name="palabraFinal"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExtraeTextoEntreDosPalabras(ByVal texto As String, ByVal palabraInicial As String, ByVal palabraFinal As String) As String
    'Busca texto
    Dim inicio, fin As Integer
    Dim textoExtraido As String = ""

    Try
      If Not String.IsNullOrEmpty(texto) Then
        inicio = InStr(1, texto, palabraInicial) + Len(palabraInicial)
        fin = InStr(inicio, texto, palabraFinal)

        'Extrae texto y le quita enters
        If fin > 0 And inicio > Len(palabraInicial) Then
          textoExtraido = Replace(Mid(texto, inicio, fin - inicio), vbCrLf, "").Trim
        End If
      End If
    Catch ex As Exception
      textoExtraido = texto
    End Try
    'retorna valor
    Return textoExtraido
  End Function

  ''' <summary>
  ''' retorna en un string el contenido de una plantilla html
  ''' </summary>
  ''' <param name="rutaPlantilla"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerContenidoPlantillaHTML(ByVal rutaPlantilla As String) As String
    Dim plantilla As String = ""
    Dim html As String = ""
    Try
      '-- comprobar que el archivo de la plantilla exista
      If System.IO.File.Exists(rutaPlantilla) Then
        ' leer la plantilla
        Dim reader As New System.IO.StreamReader(rutaPlantilla)

        'leer el contenido mientras no se llegue al final
        While reader.Peek() <> -1
          Dim s As String = reader.ReadLine()
          If Not String.IsNullOrEmpty(s) Then html &= s.Trim
        End While

        reader.Close()
        plantilla = html
      End If
    Catch ex As Exception
      plantilla = ""
    End Try
    Return plantilla
  End Function

  ''' <summary>
  ''' obtiene un arreglo con los "valores agrupados" del datatable
  ''' </summary>
  ''' <param name="dt"></param>
  ''' <param name="nombreColumna"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerArrayGroupByDeDataTable(ByVal dt As DataTable, ByVal nombreColumna As String, Optional ByVal ordenarPorColumna As Boolean = False) As ArrayList
    Dim array As New ArrayList
    Dim dv As New DataView
    Dim ds As New DataSet

    Try
      'ordena la tabla por la columna seleccionada
      If ordenarPorColumna Then
        dv = dt.DefaultView
        dv.Sort = nombreColumna
        ds = CrearDataSetDesdeDataViewConRowFilter(dv)
        dt = ds.Tables(0)
      End If
      For Each row As DataRow In dt.Rows
        Dim valor As String = row(nombreColumna).ToString().Trim()
        If Not array.Contains(valor) Then
          array.Add(valor)
        End If
      Next
      Return array
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' quita caracteres que no son permitidos en el nombre de un archivo
  ''' </summary>
  ''' <param name="s"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function QuitarCaracteresInvalidos(ByVal s As String) As String
    Dim nuevoTexto As String = ""
    nuevoTexto = s.Replace("\", "")
    nuevoTexto = nuevoTexto.Replace("/", "")
    nuevoTexto = nuevoTexto.Replace(":", "")
    nuevoTexto = nuevoTexto.Replace("*", "")
    nuevoTexto = nuevoTexto.Replace("?", "")
    nuevoTexto = nuevoTexto.Replace("""", "")
    nuevoTexto = nuevoTexto.Replace("<", "")
    nuevoTexto = nuevoTexto.Replace(">", "")
    nuevoTexto = nuevoTexto.Replace("|", "")
    'retorna valor
    Return nuevoTexto
  End Function

  ''' <summary>
  ''' formatea un numero sin separador de miles, y con el punto como separador de decimal
  ''' </summary>
  ''' <param name="valor"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function FormatearSinSeparadorFormatoAmericano(ByVal valor As String) As String
    Dim nuevoValor As String
    Try
      nuevoValor = valor.Replace(",", "").Replace(".", ",")
    Catch ex As Exception
      nuevoValor = valor
    End Try
    Return nuevoValor
  End Function

  ''' <summary>
  ''' formatea el valor de la patente
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function FormatearPatente(ByVal patente As String) As String
    Dim nuevaPatente As String
    Try
      nuevaPatente = UCase( _
                            patente _
                            .Replace(" ", "") _
                            .Replace("-", "") _
                            .Replace(".", "") _
                          )
    Catch ex As Exception
      nuevaPatente = patente
    End Try
    Return nuevaPatente
  End Function

  ''' <summary>
  ''' setea valores de prueba para el ingreso de una Alerta
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub DatosPruebaAlerta(ByRef NroTransporte As String, ByRef IdEmbarque As String, ByRef OrigenCodigo As String, ByRef PatenteTracto As String, _
                                    ByRef PatenteTrailer As String, ByRef RutTransportista As String, ByRef NombreTransportista As String, _
                                    ByRef RutConductor As String, ByRef NombreConductor As String, ByRef FechaUltimaTransmision As String, _
                                    ByRef DescripcionAlerta As String, ByRef FechaInicioAlerta As String, ByRef LatTrailer As String, ByRef LonTrailer As String, _
                                    ByRef LatTracto As String, ByRef LonTracto As String, ByRef TipoAlerta As String, _
                                    ByRef Permiso As String, ByRef Criticidad As String, ByRef TipoAlertaDescripcion As String, ByRef AlertaMapa As String, _
                                    ByRef NombreZona As String, ByRef Velocidad As String, ByRef EstadoGPSTracto As String, ByRef EstadoGPSRampla As String, _
                                    ByRef EstadoGPS As String, ByRef LocalDestino As String, ByRef TipoPunto As String, ByRef Temp1 As String, _
                                    ByRef Temp2 As String, ByRef DistanciaTT As String, ByRef TransportistaTrailer As String, ByRef CantidadSatelites As String, _
                                    ByRef Ocurrencia As String, ByRef GrupoAlerta As String)
    Try
      '[begin: tupla unica
      NroTransporte = IIf(String.IsNullOrEmpty(NroTransporte), "9802001130", NroTransporte)
      IdEmbarque = IIf(String.IsNullOrEmpty(IdEmbarque), "7464008769", NroTransporte)
      LocalDestino = IIf(String.IsNullOrEmpty(LocalDestino), "3356", LocalDestino)
      DescripcionAlerta = IIf(String.IsNullOrEmpty(DescripcionAlerta), "ATRASO DE REPORTE", DescripcionAlerta)
      Ocurrencia = IIf(String.IsNullOrEmpty(Ocurrencia), "1", Ocurrencia)
      GrupoAlerta = IIf(String.IsNullOrEmpty(GrupoAlerta), "GrupoAlerta - 42425", GrupoAlerta)
      TipoAlerta = IIf(String.IsNullOrEmpty(TipoAlerta), "RUTA", TipoAlerta)

      'DescripcionAlerta = "Local"
      'DescripcionAlerta = "Apertura puerta en Zona de Riesgo"
      'DescripcionAlerta = "Zona Cero"
      'end]

      TipoAlertaDescripcion = DescripcionAlerta
      LatTracto = "20.572113"
      LonTracto = "-100.474426"
      OrigenCodigo = "7464"
      PatenteTracto = "539AP7"
      PatenteTrailer = "478WS6"
      RutTransportista = "CULVERT"
      NombreTransportista = "CULVERT"
      RutConductor = ""
      NombreConductor = "SERGIO CRUZ"
      FechaInicioAlerta = "02/08/2017 08:26:53"
      FechaUltimaTransmision = "02/08/2017 08:26:46"
      LatTrailer = "20.572113"
      LonTrailer = "-100.474426"
      Permiso = "NO AUTORIZADA"
      Criticidad = "ALTA"
      AlertaMapa = "http://ws4.altotrack.com/GPSTrack_WalmartMX/ModuloMapa.aspx?ID=31517"
      NombreZona = ""
      Velocidad = "0"
      EstadoGPSTracto = ""
      EstadoGPSRampla = ""
      EstadoGPS = ""
      TipoPunto = ""
      Temp1 = ""
      Temp2 = ""
      DistanciaTT = "0"
      TransportistaTrailer = "539AP7"
      CantidadSatelites = "0"
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  ''' <summary>
  ''' setea valores de prueba para el ingreso de una TrazaViaje
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub DatosPruebaTrazaViaje(ByRef NroTransporte As String, ByRef IdEmbarque As String, ByRef Operacion As String, ByRef PatenteTracto As String, ByRef PatenteTrailer As String, _
                                         ByRef RutTransportista As String, ByRef NombreTransportista As String, ByRef RutConductor As String, ByRef NombreConductor As String, _
                                         ByRef OrigenCodigo As String, ByRef OrigenDescripcion As String, ByRef FHSalidaOrigen As String, ByRef LocalDestino As String, _
                                         ByRef DestinoDescripcion As String, ByRef FHLlegadaDestino As String, ByRef FHAperturaPuerta As String, ByRef FHSalidaDestino As String, _
                                         ByRef TipoViaje As String, ByRef ProveedorGPS As String, ByRef DestinoDomicilio As String, ByRef DestinoRegion As String, _
                                         ByRef DestinoDescripcionRegion As String, ByRef DestinoComuna As String, ByRef DestinoLat As String, ByRef DestinoLon As String, _
                                         ByRef EstadoViaje As String, ByRef SecuenciaDestino As String, ByRef RamplaProveedorGPS As String, ByRef EstadoPuerta As String, _
                                         ByRef EstadoLat As String, ByRef EstadoLon As String, ByRef TempSalidaCD As String)
    Try
      NroTransporte = "789456123"
      Operacion = "1"
      PatenteTracto = "UY-2503"
      PatenteTrailer = "JL.85 88"
      RutTransportista = "78952000-7"
      NombreTransportista = "COEMPRE"
      RutConductor = "12656749-9"
      NombreConductor = "Claudio Lorenzo Valdebenito Perez"
      OrigenCodigo = "124"
      OrigenDescripcion = "CEDIS PRUEBA"
      FHSalidaOrigen = "23/04/2015 12:13:38"
      LocalDestino = "8"
      DestinoDescripcion = "COLON"
      FHLlegadaDestino = "23/04/2015 12:13:26"
      FHAperturaPuerta = "23/04/2015 12:14:00"
      FHSalidaDestino = "23/04/2015 12:49:59"
      TipoViaje = "FRIO"
      ProveedorGPS = "Autotrack"
      DestinoDomicilio = "CUARTO CENTENARIO N°1016"
      DestinoRegion = "13"
      DestinoDescripcionRegion = "XIII - RM"
      DestinoComuna = "LASCO"
      DestinoLat = "-33.4186550000"
      DestinoLon = "-70.5515580000"
      EstadoViaje = "ASIGNADO"
      SecuenciaDestino = "1"
      RamplaProveedorGPS = "Cooldata"
      EstadoPuerta = "0"
      EstadoLat = "-33.3617000000"
      EstadoLon = "-70.6989100000"
      TempSalidaCD = "Prueba Salida CEDIS MODIFICADO"
    Catch ex As Exception
      Throw
    End Try

  End Sub

  ''' <summary>
  ''' setea valores de prueba para el ingreso de un LogPerdidaSenal
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub DatosPruebaLogPerdidaSenal(ByRef NroTransporte As String, ByRef PatenteTracto As String, ByRef PatenteTrailer As String, ByRef LocalDestino As String, _
                                               ByRef FechaHoraInicio As String, ByRef FechaHoraTermino As String)
    Try
      NroTransporte = "147258369"
      PatenteTracto = "UY-2503"
      PatenteTrailer = "JL.85 88"
      LocalDestino = "8"
      FechaHoraInicio = "23/04/2015 12:13:26"
      FechaHoraTermino = "23/04/2015 14:01:18"
    Catch ex As Exception
      Throw
    End Try

  End Sub

  ''' <summary>
  ''' consulta si un local es foco
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EsLocalFoco(ByVal localDestino As Integer) As Boolean
    Dim respuesta As Boolean

    Try
      respuesta = Local.EsFoco(localDestino)
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

    Return respuesta
  End Function

  ''' <summary>
  ''' envia SMS a un telefono especifico
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EnviarSMS(ByVal telefono As String, ByVal mensaje As String) As SortedList
    Dim ws As New wsAltoSMS.Service
    Dim oRespuesta As New wsAltoSMS.Respuesta
    Dim oLoginSoap As New wsAltoSMS.Autentificacion
    Dim sl As New SortedList
    Dim seDebeEnviar As Boolean = True
    Dim mensajeError As String = ""

    Try
      'datos conexion
      oLoginSoap.Usuario = "AltoSMS"
      oLoginSoap.Clave = "ALKTM78D258"

      'valida que el telefono sea numerico
      If (seDebeEnviar AndAlso Not Herramientas.EsNumerico(telefono)) Then seDebeEnviar = False : mensajeError = "El teléfono no es numérico"

      'valida que el telefono tenga largo 8
      If (seDebeEnviar AndAlso telefono.Length <> 8) Then seDebeEnviar = False : mensajeError = "El largo del teléfono no es válido"

      'valida que el telefono no comience con 2
      If (seDebeEnviar AndAlso telefono.StartsWith("2")) Then seDebeEnviar = False : mensajeError = "El teléfono es un número de red fija"

      If (seDebeEnviar) Then
        ws.AutentificacionValue = oLoginSoap
        oRespuesta = ws.EnviarSMS(telefono, mensaje)

        sl.Add("Folio", "")
        sl.Add("Codigo", oRespuesta.Codigo)
        sl.Add("Mensaje", oRespuesta.Descripcion)
      Else
        sl.Add("Folio", "")
        sl.Add("Codigo", "")
        sl.Add("Mensaje", mensajeError)
      End If
    Catch ex As Exception
      sl.Add("Folio", "-1")
      sl.Add("Codigo", "-200")
      sl.Add("Mensaje", ex.Message)
    End Try

    Return sl
  End Function

  ''' <summary>
  ''' Retorna la ruta raiz en la cual se guardaran todos los logs, archivos, etc..
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RutaRaiz() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.DirectorioRaizPrincipal
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' Retorna la ruta de la carpeta de los logs del sitio
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RutaLogsSitio() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.logPathDirectorioSitio
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' Retorna la ruta de la carpeta de los logs de las consolas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RutaLogsConsolas() As String
    Dim valor As String = String.Empty
    Try
      valor = Constantes.logPathDirectorioConsolas
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' define el estilo de la alerta segun la prioridad
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerStyleAlertaPorPrioridad(ByVal prioridad As String) As String
    Dim tipoAlerta As String

    Try
      Select Case prioridad.ToLower()
        Case "alta" : tipoAlerta = "danger"
        Case "media" : tipoAlerta = "warning"
        Case "baja" : tipoAlerta = "success"
        Case "black" : tipoAlerta = "black"
        Case Else : tipoAlerta = "success"
      End Select
    Catch ex As Exception
      tipoAlerta = "success"
    End Try

    Return tipoAlerta
  End Function

  ''' <summary>
  ''' verifica si el horario de atencion del contacto esta disponible
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EstaDentroHorarioAtencion(ByVal horaAtencionInicio As String, ByVal horaAtencionTermino As String) As Boolean
    Dim estaEnHorario As Boolean
    Dim fechaActual As DateTime = Herramientas.MyNow()
    Dim horaActual As String

    Try
      horaActual = fechaActual.ToString("HHmm")
      horaAtencionInicio = horaAtencionInicio.Replace(":", "")
      horaAtencionTermino = horaAtencionTermino.Replace(":", "")

      '1ra. verificacion de horario: si hora de inicio es menor o igual que la de termino, indica que esta dentro de un horario que no sobrepasa las 00:00
      If (horaAtencionInicio <= horaAtencionTermino) Then
        If ((horaActual >= horaAtencionInicio) And (horaActual <= horaAtencionTermino)) Then
          estaEnHorario = True
        Else
          estaEnHorario = False
        End If
      Else
        '2da. verificacion de horario: si hora de inicio es mayor que la hora de termino, indica que pasa las 00:00, por lo que hay que validar en dos tramos desde [horainicio - 23:59] a [00:00 - horatermino]
        If (
            ((horaActual >= horaAtencionInicio) And (horaActual <= "2359")) Or
            ((horaActual >= "0000") And (horaActual <= horaAtencionTermino))
           ) Then
          estaEnHorario = True
        Else
          estaEnHorario = False
        End If
      End If
    Catch ex As Exception
      estaEnHorario = True
    End Try

    Return estaEnHorario
  End Function

  ''' <summary>
  ''' verifica si el horario de gestion de la alerta esta dentro del dia y hora disponible
  ''' se valida formato (ejm): LU:0100-2359|MA:0000-2359|MI:0000-2359|JU:0000-2359|VI:0000-2359|SA:0000-2044
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EstaDentroHorarioGestionAlerta(ByVal horarioAtencion As String) As Boolean
    Dim estaEnHorario As Boolean
    Dim diaActual, mesActual, anioActual, indiceDiaActual As Integer
    Dim diaActualCorto, horaActual, diaAtencionCorto, rangoHoraAtencion, horaInicioAtencion, horaTerminoAtencion As String
    Dim fechaActual = Herramientas.MyNow()
    Dim arrHorarioAtencion, arrDiaAtencion, arrRangoHoraAtencion As String()

    Try
      'obtiene datos del dia actual
      diaActual = fechaActual.Day
      mesActual = fechaActual.Month
      anioActual = fechaActual.Year
      horaActual = fechaActual.ToString("HHmm")
      indiceDiaActual = Herramientas.ObtenerIndiceDiaSemana(diaActual, mesActual, anioActual)
      diaActualCorto = Herramientas.ObtenerNombreDiaSemanaPorIndice(indiceDiaActual, True, False)

      If (String.IsNullOrEmpty(horarioAtencion)) Then
        estaEnHorario = False
      Else
        estaEnHorario = False 'se inicializa que no esta en horario atencion
        arrHorarioAtencion = horarioAtencion.Split("|")
        For Each diaAtencion In arrHorarioAtencion
          arrDiaAtencion = diaAtencion.Split(":")
          diaAtencionCorto = arrDiaAtencion(0)
          rangoHoraAtencion = arrDiaAtencion(1)
          arrRangoHoraAtencion = rangoHoraAtencion.Split("-")
          horaInicioAtencion = arrRangoHoraAtencion(0)
          horaTerminoAtencion = arrRangoHoraAtencion(1)

          If (diaAtencionCorto = diaActualCorto And (horaActual >= horaInicioAtencion And horaActual <= horaTerminoAtencion)) Then
            estaEnHorario = True
            Exit For
          End If
        Next
      End If

    Catch ex As Exception
      estaEnHorario = False
    End Try

    Return estaEnHorario
  End Function

  ''' <summary>
  ''' valida si el pdf de auditoria existe en el servidor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExistePDFAuditoria(ByVal tipoInforme As String, ByVal idFormulario As String, ByRef arrArchivo As String()) As Boolean
    Dim existe As Boolean
    Dim rutaCarpetaFTP As String

    Try
      If (idFormulario = "-1") Then
        existe = False
      Else
        Select Case tipoInforme.ToUpper()
          Case "CARGA"
            rutaCarpetaFTP = Reclamo.RUTA_CARPETA_FTP_PDF_CARGA
          Case "RECEPCION"
            rutaCarpetaFTP = Reclamo.RUTA_CARPETA_FTP_PDF_RECEPCION
          Case Else
            rutaCarpetaFTP = ""
        End Select

        If (String.IsNullOrEmpty(rutaCarpetaFTP)) Then
          existe = False
        Else
          'permite el uso del recurso compartido del servidor
          Shell("cmd.exe /k net use m: " & rutaCarpetaFTP & " /user:APP-ALTO\" & Constantes.SERVICE_ALTO_USUARIO & " " & Constantes.SERVICE_ALTO_PASSWORD)

          'obtiene todos los archivos que cumplan con el filtro
          arrArchivo = IO.Directory.GetFiles(rutaCarpetaFTP, "*" & tipoInforme & "*-*" & idFormulario & ".pdf")

          Shell("cmd.exe /k net use m: /delete")

          If (arrArchivo.Length = 0) Then
            existe = False
          Else
            existe = True
          End If
        End If
      End If
    Catch ex As Exception
      existe = False
    End Try

    Return existe
  End Function

  ''' <summary>
  ''' verifica si la alerta esta dentro del horario de atencion del callcenter 08:00 - 22:00
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EstaDentroHorarioCallCenter() As Boolean
    Dim estaEnHorario As Boolean
    Dim fechaHoy As DateTime = Herramientas.MyNow()
    Dim horaHoy As String

    Try
      horaHoy = fechaHoy.ToString("HHmm")

      If (horaHoy >= "0800" And horaHoy <= "2200") Then
        estaEnHorario = True
      Else
        estaEnHorario = False
      End If

    Catch ex As Exception
      estaEnHorario = True
    End Try

    Return estaEnHorario
  End Function

  ''' <summary>
  ''' lee archivo xml y los devuelve en un dataset
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function LeerXML(ByVal nombreXML As String) As DataSet
    Dim dsXML As New DataSet
    Dim path As String

    Try
      path = System.AppDomain.CurrentDomain.BaseDirectory
      dsXML.ReadXml(path & "\" & nombreXML)
    Catch ex As Exception
      dsXML = Nothing
    End Try

    Return dsXML
  End Function

  ''' <summary>
  ''' obtiene la diferencia horaria
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDiferenciaHoraria() As Integer
    Dim _texto As String = Configuracion.Leer("webDiferenciaHoraria")
    Dim valor As Integer = 0

    If String.IsNullOrEmpty(_texto) Then
      valor = 0
    Else
      If EsNumerico(_texto, False, True, False) Then
        valor = CInt(_texto)
      Else
        valor = 0
      End If
    End If
    'retorna valor
    Return valor
  End Function

  ''' <summary>
  ''' obtiene la fecha actual segun la zona horaria
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MyNow() As DateTime
    Dim fechaHoy As DateTime
    Dim diferenciaHoraria As Integer

    Try
      diferenciaHoraria = ObtenerDiferenciaHoraria()
      fechaHoy = DateAdd(DateInterval.Hour, diferenciaHoraria, Now())
    Catch ex As Exception
      fechaHoy = Now()
    End Try
    Return fechaHoy
  End Function

  ''' <summary>
  ''' construye objeto usuario en json
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerJSONUsuario(ByVal oUsuario As Usuario, ByVal urlBase As String) As String
    Dim idUsuario, nombreCompleto, perfilNombre, perfilLlave, urlApiTicketSoporte As String
    Dim oDiccionario As New Dictionary(Of String, String)

    Try
      idUsuario = oUsuario.Id
      nombreCompleto = oUsuario.NombreCompleto
      perfilNombre = oUsuario.PerfilNombre
      perfilLlave = oUsuario.PerfilLlave

      If (Not Herramientas.MailModoDebug()) Then
        urlApiTicketSoporte = Constantes.DIRECTORIO_VIRTUAL_API_TICKET_SOPORTE_PRODUCCION
      Else
        If (urlBase.StartsWith("http://demo") Or urlBase.StartsWith("https://demo")) Then
          urlApiTicketSoporte = Constantes.DIRECTORIO_VIRTUAL_API_TICKET_SOPORTE_DEMO
        Else
          urlApiTicketSoporte = Constantes.DIRECTORIO_VIRTUAL_API_TICKET_SOPORTE_DESARROLLO
        End If
      End If

      oDiccionario.Add("IdUsuario", idUsuario)
      oDiccionario.Add("NombreCompleto", nombreCompleto)
      oDiccionario.Add("PerfilNombre", perfilNombre)
      oDiccionario.Add("PerfilLlave", perfilLlave)
      oDiccionario.Add("UrlApiTicketSoporte", urlApiTicketSoporte)
    Catch ex As Exception
      oDiccionario = New Dictionary(Of String, String)()
    End Try
    Return MyJSON.ConvertObjectToJSON(oDiccionario)
  End Function

#End Region

End Class
