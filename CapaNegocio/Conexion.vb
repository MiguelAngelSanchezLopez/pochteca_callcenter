Imports System.IO

Public Class Conexion

#Region "Constantes"

  'se debe modificar el archivo machine.config ubicado en esta url, en la seccion <connectionStrings />
  'C:\Windows\Microsoft.NET\Framework64\v2.0.50727\CONFIG\machine.config
  Public Const StrConexionBdAltotrackPochteca = "strConexionBD_AltotrackPochteca"

#End Region

#Region "shared"
  ''' <summary>
  ''' Obtiene el string de conexion desde el Machine.config
  ''' </summary>
  Public Shared Function StringConexion() As String
    Try

      Return System.Configuration.ConfigurationManager.ConnectionStrings(StrConexionBdAltotrackPochteca).ConnectionString

    Catch ex As Exception
      Return String.Empty
    End Try

  End Function
#End Region

End Class
