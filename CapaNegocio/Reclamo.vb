﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Reclamo

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Reclamo"
  Public Const KEY_SESION_RECLAMO_MENSAJE As String = "ReclamoMensaje"
  Public Const NOMBRE_CARPETA_PDF_CARGA As String = "Carga\Pdf"
  Public Const NOMBRE_CARPETA_PDF_RECEPCION As String = "Recepcion\Pdf"
  Public Const RUTA_CARPETA_SERVIDOR_PDF_CARGAR As String = Constantes.logPathDirectorioSitio & "\" & NombreEntidad & "\" & NOMBRE_CARPETA_PDF_CARGA
  Public Const RUTA_CARPETA_SERVIDOR_PDF_RECEPCION As String = Constantes.logPathDirectorioSitio & "\" & NombreEntidad & "\" & NOMBRE_CARPETA_PDF_RECEPCION
  Public Const RUTA_CARPETA_FTP As String = "\\10.0.0.6\C$\INETPUB\FTPROOT\FTP_TRANSPORTE_SMU"
  Public Const RUTA_CARPETA_FTP_PDF_CARGA As String = RUTA_CARPETA_FTP & "\" & NOMBRE_CARPETA_PDF_CARGA
  Public Const RUTA_CARPETA_FTP_PDF_RECEPCION As String = RUTA_CARPETA_FTP & "\" & NOMBRE_CARPETA_PDF_RECEPCION
#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene listado de reclamos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Buscador(ByVal idUsuario As String, ByVal codigoLocal As String, ByVal tipoReclamo As String, ByVal departamento As String, ByVal codigoCentroDistribucion As String, _
                                  ByVal nroReclamo As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal estadoReclamo As String, ByVal nroViaje As String, _
                                  ByVal tipoViaje As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(10) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal
      arParms(2) = New SqlParameter("@TipoReclamo", SqlDbType.VarChar)
      arParms(2).Value = tipoReclamo
      arParms(3) = New SqlParameter("@Departamento", SqlDbType.VarChar)
      arParms(3).Value = departamento
      arParms(4) = New SqlParameter("@CodigoCentroDistribucion", SqlDbType.Int)
      arParms(4).Value = codigoCentroDistribucion
      arParms(5) = New SqlParameter("@NroReclamo", SqlDbType.Int)
      arParms(5).Value = nroReclamo
      arParms(6) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(6).Value = fechaDesde
      arParms(7) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(7).Value = fechaHasta
      arParms(8) = New SqlParameter("@EstadoReclamo", SqlDbType.VarChar)
      arParms(8).Value = estadoReclamo
      arParms(9) = New SqlParameter("@NroViaje", SqlDbType.Int)
      arParms(9).Value = nroViaje
      arParms(10) = New SqlParameter("@TipoViaje", SqlDbType.VarChar)
      arParms(10).Value = tipoViaje

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reclamo_Buscador", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene el detalle del reclamo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalle(ByVal nroViaje As String, ByVal nroEnvio As String, ByVal codigoLocal As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@NroViaje", SqlDbType.Int)
      arParms(0).Value = nroViaje
      arParms(1) = New SqlParameter("@NroEnvio", SqlDbType.Int)
      arParms(1).Value = nroEnvio
      arParms(2) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(2).Value = codigoLocal

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reclamo_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de reclamos sin alertas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuscadorSinAlertas(ByVal idUsuario As String, ByVal codigoLocal As String, ByVal tipoReclamo As String, ByVal departamento As String, ByVal codigoCentroDistribucion As String, _
                                            ByVal nroReclamo As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal estadoReclamo As String, ByVal nroViaje As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(9) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal
      arParms(2) = New SqlParameter("@TipoReclamo", SqlDbType.VarChar)
      arParms(2).Value = tipoReclamo
      arParms(3) = New SqlParameter("@Departamento", SqlDbType.VarChar)
      arParms(3).Value = departamento
      arParms(4) = New SqlParameter("@CodigoCentroDistribucion", SqlDbType.Int)
      arParms(4).Value = codigoCentroDistribucion
      arParms(5) = New SqlParameter("@NroReclamo", SqlDbType.Int)
      arParms(5).Value = nroReclamo
      arParms(6) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(6).Value = fechaDesde
      arParms(7) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(7).Value = fechaHasta
      arParms(8) = New SqlParameter("@EstadoReclamo", SqlDbType.VarChar)
      arParms(8).Value = estadoReclamo
      arParms(9) = New SqlParameter("@NroViaje", SqlDbType.Int)
      arParms(9).Value = nroViaje

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reclamo_BuscadorSinAlertas", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class
