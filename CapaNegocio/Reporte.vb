﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Reporte
#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Reporte"
  Public Const KEY_SESION_REPORTE_GENERADO As String = "ReporteGenerado"
  Public Const KEY_SESION_MENSAJE As String = "ReporteMensaje"
  Public Const KEY_SESION_DATASET As String = "ReporteDataset"

#End Region

#Region "Reportes Genericos"
  ''' <summary>
  ''' obtiene detalle del reporte consultado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idReporte As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdReporte", SqlDbType.Int)
      arParms(0).Value = idReporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reporte_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' genera datos de un reporte generico
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDatosGenericos(ByVal procedimientoAlmacenado As String, ByVal fechaDesde As String, ByVal fechaHasta As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(0).Value = fechaDesde
      arParms(1) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(1).Value = fechaHasta

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, procedimientoAlmacenado, arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del reporte consultado por llave
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetallePorLLave(ByVal llave As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@llave", SqlDbType.VarChar)
      arParms(0).Value = llave

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reporte_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region


#Region "Reportes Formulario"
  ''' <summary>
  ''' obtiene detalle del reporte consultado por llave
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerProgramacion(ByVal idUsuario As String, ByVal nroTransporte As String, ByVal fechaPresentacion As String, ByVal patenteTracto As String, _
                                        ByVal patenteTrailer As String, ByVal carga As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte
      arParms(2) = New SqlParameter("@FechaPresentacion", SqlDbType.VarChar)
      arParms(2).Value = fechaPresentacion
      arParms(3) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(3).Value = patenteTracto
      arParms(4) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      arParms(4).Value = patenteTrailer
      arParms(5) = New SqlParameter("@Carga", SqlDbType.VarChar)
      arParms(5).Value = carga

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reporte_Programacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region


End Class
