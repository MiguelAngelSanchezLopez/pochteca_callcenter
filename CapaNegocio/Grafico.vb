﻿Imports System.Text

Public Class Grafico
  ''' <summary>
  ''' obtiene json para dibujar grafico de torta
  ''' </summary>
  ''' <param name="dt"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerJSONTorta(ByVal dt As DataTable, ByVal titulo As String, ByVal nombreSerie As String, ByVal columnaTexto As String, ByVal columnaValor As String, _
                                          ByVal animacion As String, ByVal width As String, ByVal height As String) As String
    Dim retorno, nombreEjeX, valorEjeY As String
    Dim totalRegistros, i As Integer
    Dim dr As DataRow
    Dim listadoNombresEjeX As String = ""
    Dim listadoDatos As String = ""
    Dim sb As New StringBuilder
    Dim dsFiltrado As New DataSet

    Try
      'construye estructura json para que lo lea el grafico
      totalRegistros = dt.Rows.Count
      For i = 0 To totalRegistros - 1
        dr = dt.Rows(i)
        nombreEjeX = dr.Item(columnaTexto)
        valorEjeY = dr.Item(columnaValor)

        'obtiene indice del color, que va desde el 0 al 9
        listadoDatos &= "['" & nombreEjeX & "', " & valorEjeY.Replace(",", ".") & "]"

        If (i < totalRegistros - 1) Then
          listadoDatos &= ","
        End If
      Next

      sb.Append("{")
      sb.Append(" ""tipo"":""torta"" ")
      sb.Append(",""titulo"": """ & titulo & """ ")
      sb.Append(",""nombreSerie"": """ & nombreSerie & """ ")
      sb.Append(",""animacion"": " & animacion)
      sb.Append(",""width"": " & width)
      sb.Append(",""height"": " & height)
      sb.Append(",""datos"": ""[" & listadoDatos & "]""")
      sb.Append("}")
      retorno = sb.ToString()
    Catch ex As Exception
      retorno = ""
    End Try

    Return retorno
  End Function

End Class
