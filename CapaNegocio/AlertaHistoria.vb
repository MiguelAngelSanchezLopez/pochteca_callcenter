﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class AlertaHistoria
  Inherits AlertaMasterClass

#Region "Properties"
  Public Const NombreEntidad As String = "AlertaHistoria"
  Public Const Directorio As String = "AlertaHistoria\"

  Public Property Existe As Boolean
#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal Id As Integer)
    Try
      Me.Id = Id
      ObtenerPorId()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

  Public Sub New(ByVal NroTransporte As String)
    Try
      Me.NroTransporte = NroTransporte
      ObtenerPorNroTransporte()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"

  Public Sub GuardarNuevo()

    Dim storedProcedure As String = "spu_AlertaHistoria_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(40) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("@DescripcionAlerta", SqlDbType.VarChar, 255)
      parms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      parms(4) = New SqlParameter("@OrigenCodigo", SqlDbType.Int)
      parms(5) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar, 255)
      parms(6) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar, 255)
      parms(7) = New SqlParameter("@RutTransportista", SqlDbType.VarChar, 255)
      parms(8) = New SqlParameter("@NombreTransportista", SqlDbType.VarChar, 255)
      parms(9) = New SqlParameter("@RutConductor", SqlDbType.VarChar, 255)
      parms(10) = New SqlParameter("@NombreConductor", SqlDbType.VarChar, 255)
      parms(11) = New SqlParameter("@FechaUltimaTransmision", SqlDbType.DateTime)
      parms(12) = New SqlParameter("@FechaInicioAlerta", SqlDbType.DateTime)
      parms(13) = New SqlParameter("@LatTrailer", SqlDbType.VarChar, 255)
      parms(14) = New SqlParameter("@LonTrailer", SqlDbType.VarChar, 255)
      parms(15) = New SqlParameter("@LatTracto", SqlDbType.VarChar, 255)
      parms(16) = New SqlParameter("@LonTracto", SqlDbType.VarChar, 255)
      parms(17) = New SqlParameter("@TipoAlerta", SqlDbType.VarChar, 255)
      parms(18) = New SqlParameter("@Permiso", SqlDbType.VarChar, 255)
      parms(19) = New SqlParameter("@Criticidad", SqlDbType.VarChar, 255)
      parms(20) = New SqlParameter("@TipoAlertaDescripcion", SqlDbType.VarChar, 255)
      parms(21) = New SqlParameter("@AlertaMapa", SqlDbType.VarChar, 255)
      parms(22) = New SqlParameter("@NombreZona", SqlDbType.VarChar, 255)
      parms(23) = New SqlParameter("@Velocidad", SqlDbType.VarChar, 255)
      parms(24) = New SqlParameter("@EstadoGPSTracto", SqlDbType.VarChar, 255)
      parms(25) = New SqlParameter("@EstadoGPSRampla", SqlDbType.VarChar, 255)
      parms(26) = New SqlParameter("@EstadoGPS", SqlDbType.VarChar, 255)
      parms(27) = New SqlParameter("@TipoPunto", SqlDbType.VarChar, 255)
      parms(28) = New SqlParameter("@Temp1", SqlDbType.VarChar, 255)
      parms(29) = New SqlParameter("@Temp2", SqlDbType.VarChar, 255)
      parms(30) = New SqlParameter("@DistanciaTT", SqlDbType.VarChar, 255)
      parms(31) = New SqlParameter("@TransportistaTrailer", SqlDbType.VarChar, 255)
      parms(32) = New SqlParameter("@CantidadSatelites", SqlDbType.Int)
      parms(33) = New SqlParameter("@FechaHoraCreacion", SqlDbType.DateTime)
      parms(34) = New SqlParameter("@FechaHoraActualizacion", SqlDbType.DateTime)
      parms(35) = New SqlParameter("@EnvioCorreo", SqlDbType.Bit)
      parms(36) = New SqlParameter("@FueraDeHorario", SqlDbType.Bit)
      parms(37) = New SqlParameter("@Activo", SqlDbType.Bit)
      parms(38) = New SqlParameter("@ConLog", SqlDbType.Bit)
      parms(39) = New SqlParameter("@Ocurrencia", SqlDbType.Int)
      parms(40) = New SqlParameter("@GrupoAlerta", SqlDbType.VarChar, 255)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.DescripcionAlerta
      parms(3).Value = Me.LocalDestino
      parms(4).Value = Me.OrigenCodigo
      parms(5).Value = Me.PatenteTracto
      parms(6).Value = Me.PatenteTrailer
      parms(7).Value = Me.RutTransportista
      parms(8).Value = Me.NombreTransportista
      parms(9).Value = Me.RutConductor
      parms(10).Value = Me.NombreConductor
      parms(11).Value = Me.FechaUltimaTransmision
      parms(12).Value = Me.FechaInicioAlerta
      parms(13).Value = Me.LatTrailer
      parms(14).Value = Me.LonTrailer
      parms(15).Value = Me.LatTracto
      parms(16).Value = Me.LonTracto
      parms(17).Value = Me.TipoAlerta
      parms(18).Value = Me.Permiso
      parms(19).Value = Me.Criticidad
      parms(20).Value = Me.TipoAlertaDescripcion
      parms(21).Value = Me.AlertaMapa
      parms(22).Value = Me.NombreZona
      parms(23).Value = Me.Velocidad
      parms(24).Value = Me.EstadoGPSTracto
      parms(25).Value = Me.EstadoGPSRampla
      parms(26).Value = Me.EstadoGPS
      parms(27).Value = Me.TipoPunto
      parms(28).Value = Me.Temp1
      parms(29).Value = Me.Temp2
      parms(30).Value = Me.DistanciaTT
      parms(31).Value = Me.TransportistaTrailer
      parms(32).Value = Me.CantidadSatelites
      parms(33).Value = Me.FechaHoraCreacion
      parms(34).Value = Me.FechaHoraActualizacion
      parms(35).Value = Me.EnvioCorreo
      parms(36).Value = Me.FueraDeHorario
      parms(37).Value = Me.Activo
      parms(38).Value = Me.ConLog
      parms(39).Value = Me.Ocurrencia
      parms(40).Value = Me.GrupoAlerta

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      Me.Id = parms(0).Value
      Me.Existe = True

      ObtenerPorId() ' refrescar la instancia
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  Public Sub Actualizar()
    Try
      If Not Me.Existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim storedProcedure As String = "spu_AlertaHistoria_Actualizar"
      Dim parms() As SqlParameter = New SqlParameter(40) {}

      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("@DescripcionAlerta", SqlDbType.VarChar, 255)
      parms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      parms(4) = New SqlParameter("@OrigenCodigo", SqlDbType.Int)
      parms(5) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar, 255)
      parms(6) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar, 255)
      parms(7) = New SqlParameter("@RutTransportista", SqlDbType.VarChar, 255)
      parms(8) = New SqlParameter("@NombreTransportista", SqlDbType.VarChar, 255)
      parms(9) = New SqlParameter("@RutConductor", SqlDbType.VarChar, 255)
      parms(10) = New SqlParameter("@NombreConductor", SqlDbType.VarChar, 255)
      parms(11) = New SqlParameter("@FechaUltimaTransmision", SqlDbType.DateTime)
      parms(12) = New SqlParameter("@FechaInicioAlerta", SqlDbType.DateTime)
      parms(13) = New SqlParameter("@LatTrailer", SqlDbType.VarChar, 255)
      parms(14) = New SqlParameter("@LonTrailer", SqlDbType.VarChar, 255)
      parms(15) = New SqlParameter("@LatTracto", SqlDbType.VarChar, 255)
      parms(16) = New SqlParameter("@LonTracto", SqlDbType.VarChar, 255)
      parms(17) = New SqlParameter("@TipoAlerta", SqlDbType.VarChar, 255)
      parms(18) = New SqlParameter("@Permiso", SqlDbType.VarChar, 255)
      parms(19) = New SqlParameter("@Criticidad", SqlDbType.VarChar, 255)
      parms(20) = New SqlParameter("@TipoAlertaDescripcion", SqlDbType.VarChar, 255)
      parms(21) = New SqlParameter("@AlertaMapa", SqlDbType.VarChar, 255)
      parms(22) = New SqlParameter("@NombreZona", SqlDbType.VarChar, 255)
      parms(23) = New SqlParameter("@Velocidad", SqlDbType.VarChar, 255)
      parms(24) = New SqlParameter("@EstadoGPSTracto", SqlDbType.VarChar, 255)
      parms(25) = New SqlParameter("@EstadoGPSRampla", SqlDbType.VarChar, 255)
      parms(26) = New SqlParameter("@EstadoGPS", SqlDbType.VarChar, 255)
      parms(27) = New SqlParameter("@TipoPunto", SqlDbType.VarChar, 255)
      parms(28) = New SqlParameter("@Temp1", SqlDbType.VarChar, 255)
      parms(29) = New SqlParameter("@Temp2", SqlDbType.VarChar, 255)
      parms(30) = New SqlParameter("@DistanciaTT", SqlDbType.VarChar, 255)
      parms(31) = New SqlParameter("@TransportistaTrailer", SqlDbType.VarChar, 255)
      parms(32) = New SqlParameter("@CantidadSatelites", SqlDbType.Int)
      parms(33) = New SqlParameter("@FechaHoraCreacion", SqlDbType.DateTime)
      parms(34) = New SqlParameter("@FechaHoraActualizacion", SqlDbType.DateTime)
      parms(35) = New SqlParameter("@EnvioCorreo", SqlDbType.Bit)
      parms(36) = New SqlParameter("@FueraDeHorario", SqlDbType.Bit)
      parms(37) = New SqlParameter("@Activo", SqlDbType.Bit)
      parms(38) = New SqlParameter("@ConLog", SqlDbType.Bit)
      parms(39) = New SqlParameter("@Ocurrencia", SqlDbType.Int)
      parms(40) = New SqlParameter("@GrupoAlerta", SqlDbType.VarChar, 255)

      parms(0).Value = Me.Id
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.DescripcionAlerta
      parms(3).Value = Me.LocalDestino
      parms(4).Value = Me.OrigenCodigo
      parms(5).Value = Me.PatenteTracto
      parms(6).Value = Me.PatenteTrailer
      parms(7).Value = Me.RutTransportista
      parms(8).Value = Me.NombreTransportista
      parms(9).Value = Me.RutConductor
      parms(10).Value = Me.NombreConductor
      parms(11).Value = Me.FechaUltimaTransmision
      parms(12).Value = Me.FechaInicioAlerta
      parms(13).Value = Me.LatTrailer
      parms(14).Value = Me.LonTrailer
      parms(15).Value = Me.LatTracto
      parms(16).Value = Me.LonTracto
      parms(17).Value = Me.TipoAlerta
      parms(18).Value = Me.Permiso
      parms(19).Value = Me.Criticidad
      parms(20).Value = Me.TipoAlertaDescripcion
      parms(21).Value = Me.AlertaMapa
      parms(22).Value = Me.NombreZona
      parms(23).Value = Me.Velocidad
      parms(24).Value = Me.EstadoGPSTracto
      parms(25).Value = Me.EstadoGPSRampla
      parms(26).Value = Me.EstadoGPS
      parms(27).Value = Me.TipoPunto
      parms(28).Value = Me.Temp1
      parms(29).Value = Me.Temp2
      parms(30).Value = Me.DistanciaTT
      parms(31).Value = Me.TransportistaTrailer
      parms(32).Value = Me.CantidadSatelites
      parms(33).Value = Me.FechaHoraCreacion
      parms(34).Value = Me.FechaHoraActualizacion
      parms(35).Value = Me.EnvioCorreo
      parms(36).Value = Me.FueraDeHorario
      parms(37).Value = Me.Activo
      parms(38).Value = Me.ConLog
      parms(39).Value = Me.Ocurrencia
      parms(40).Value = Me.GrupoAlerta

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)
    Catch ex As Exception
      Throw New System.Exception("Error al actualizar " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Privados"

  Private Sub ObtenerPorId()
    Try
      Existe = False

      Dim storedProcedure As String = "spu_AlertaHistoria_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("DescripcionAlerta")) Then Me.DescripcionAlerta = row("DescripcionAlerta")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("OrigenCodigo")) Then Me.OrigenCodigo = row("OrigenCodigo")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("RutTransportista")) Then Me.RutTransportista = row("RutTransportista")
        If Not IsDBNull(row("NombreTransportista")) Then Me.NombreTransportista = row("NombreTransportista")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("FechaUltimaTransmision")) Then Me.FechaUltimaTransmision = row("FechaUltimaTransmision")
        If Not IsDBNull(row("FechaInicioAlerta")) Then Me.FechaInicioAlerta = row("FechaInicioAlerta")
        If Not IsDBNull(row("LatTrailer")) Then Me.LatTrailer = row("LatTrailer")
        If Not IsDBNull(row("LonTrailer")) Then Me.LonTrailer = row("LonTrailer")
        If Not IsDBNull(row("LatTracto")) Then Me.LatTracto = row("LatTracto")
        If Not IsDBNull(row("LonTracto")) Then Me.LonTracto = row("LonTracto")
        If Not IsDBNull(row("TipoAlerta")) Then Me.TipoAlerta = row("TipoAlerta")
        If Not IsDBNull(row("Permiso")) Then Me.Permiso = row("Permiso")
        If Not IsDBNull(row("Criticidad")) Then Me.Criticidad = row("Criticidad")
        If Not IsDBNull(row("TipoAlertaDescripcion")) Then Me.TipoAlertaDescripcion = row("TipoAlertaDescripcion")
        If Not IsDBNull(row("AlertaMapa")) Then Me.AlertaMapa = row("AlertaMapa")
        If Not IsDBNull(row("NombreZona")) Then Me.NombreZona = row("NombreZona")
        If Not IsDBNull(row("Velocidad")) Then Me.Velocidad = row("Velocidad")
        If Not IsDBNull(row("EstadoGPSTracto")) Then Me.EstadoGPSTracto = row("EstadoGPSTracto")
        If Not IsDBNull(row("EstadoGPSRampla")) Then Me.EstadoGPSRampla = row("EstadoGPSRampla")
        If Not IsDBNull(row("EstadoGPS")) Then Me.EstadoGPS = row("EstadoGPS")
        If Not IsDBNull(row("TipoPunto")) Then Me.TipoPunto = row("TipoPunto")
        If Not IsDBNull(row("Temp1")) Then Me.Temp1 = row("Temp1")
        If Not IsDBNull(row("Temp2")) Then Me.Temp2 = row("Temp2")
        If Not IsDBNull(row("DistanciaTT")) Then Me.DistanciaTT = row("DistanciaTT")
        If Not IsDBNull(row("TransportistaTrailer")) Then Me.TransportistaTrailer = row("TransportistaTrailer")
        If Not IsDBNull(row("CantidadSatelites")) Then Me.CantidadSatelites = row("CantidadSatelites")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
        If Not IsDBNull(row("FechaHoraActualizacion")) Then Me.FechaHoraActualizacion = row("FechaHoraActualizacion")
        If Not IsDBNull(row("EnvioCorreo")) Then Me.EnvioCorreo = row("EnvioCorreo")
        If Not IsDBNull(row("FueraDeHorario")) Then Me.FueraDeHorario = row("FueraDeHorario")
        If Not IsDBNull(row("Activo")) Then Me.Activo = row("Activo")
        If Not IsDBNull(row("ConLog")) Then Me.ConLog = row("ConLog")
        If Not IsDBNull(row("Ocurrencia")) Then Me.Ocurrencia = row("Ocurrencia")
        If Not IsDBNull(row("GrupoAlerta")) Then Me.GrupoAlerta = row("GrupoAlerta")
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try

  End Sub

  Private Sub ObtenerPorNroTransporte()
    Try
      Me.Existe = False

      Dim storedProcedure As String = "spu_AlertaHistoria_ObtenerPorNroTransporte"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(0).Value = Me.NroTransporte

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("DescripcionAlerta")) Then Me.DescripcionAlerta = row("DescripcionAlerta")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("OrigenCodigo")) Then Me.OrigenCodigo = row("OrigenCodigo")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("RutTransportista")) Then Me.RutTransportista = row("RutTransportista")
        If Not IsDBNull(row("NombreTransportista")) Then Me.NombreTransportista = row("NombreTransportista")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("FechaUltimaTransmision")) Then Me.FechaUltimaTransmision = row("FechaUltimaTransmision")
        If Not IsDBNull(row("FechaInicioAlerta")) Then Me.FechaInicioAlerta = row("FechaInicioAlerta")
        If Not IsDBNull(row("LatTrailer")) Then Me.LatTrailer = row("LatTrailer")
        If Not IsDBNull(row("LonTrailer")) Then Me.LonTrailer = row("LonTrailer")
        If Not IsDBNull(row("LatTracto")) Then Me.LatTracto = row("LatTracto")
        If Not IsDBNull(row("LonTracto")) Then Me.LonTracto = row("LonTracto")
        If Not IsDBNull(row("TipoAlerta")) Then Me.TipoAlerta = row("TipoAlerta")
        If Not IsDBNull(row("Permiso")) Then Me.Permiso = row("Permiso")
        If Not IsDBNull(row("Criticidad")) Then Me.Criticidad = row("Criticidad")
        If Not IsDBNull(row("TipoAlertaDescripcion")) Then Me.TipoAlertaDescripcion = row("TipoAlertaDescripcion")
        If Not IsDBNull(row("AlertaMapa")) Then Me.AlertaMapa = row("AlertaMapa")
        If Not IsDBNull(row("NombreZona")) Then Me.NombreZona = row("NombreZona")
        If Not IsDBNull(row("Velocidad")) Then Me.Velocidad = row("Velocidad")
        If Not IsDBNull(row("EstadoGPSTracto")) Then Me.EstadoGPSTracto = row("EstadoGPSTracto")
        If Not IsDBNull(row("EstadoGPSRampla")) Then Me.EstadoGPSRampla = row("EstadoGPSRampla")
        If Not IsDBNull(row("EstadoGPS")) Then Me.EstadoGPS = row("EstadoGPS")
        If Not IsDBNull(row("TipoPunto")) Then Me.TipoPunto = row("TipoPunto")
        If Not IsDBNull(row("Temp1")) Then Me.Temp1 = row("Temp1")
        If Not IsDBNull(row("Temp2")) Then Me.Temp2 = row("Temp2")
        If Not IsDBNull(row("DistanciaTT")) Then Me.DistanciaTT = row("DistanciaTT")
        If Not IsDBNull(row("TransportistaTrailer")) Then Me.TransportistaTrailer = row("TransportistaTrailer")
        If Not IsDBNull(row("CantidadSatelites")) Then Me.CantidadSatelites = row("CantidadSatelites")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
        If Not IsDBNull(row("FechaHoraActualizacion")) Then Me.FechaHoraActualizacion = row("FechaHoraActualizacion")
        If Not IsDBNull(row("EnvioCorreo")) Then Me.EnvioCorreo = row("EnvioCorreo")
        If Not IsDBNull(row("FueraDeHorario")) Then Me.FueraDeHorario = row("FueraDeHorario")
        If Not IsDBNull(row("Activo")) Then Me.Activo = row("Activo")
        If Not IsDBNull(row("ConLog")) Then Me.ConLog = row("ConLog")
        If Not IsDBNull(row("Ocurrencia")) Then Me.Ocurrencia = row("Ocurrencia")
        If Not IsDBNull(row("GrupoAlerta")) Then Me.GrupoAlerta = row("GrupoAlerta")
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

End Class
