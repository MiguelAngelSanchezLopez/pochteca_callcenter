﻿Imports Microsoft.VisualBasic
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.Text

Public Class MyJSON

#Region "Private"
  ''' <summary>
  ''' escapar comillas enrutinas JS
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function EscaparComillasJS(ByVal texto As String) As String
    'cambio los \n por retorno de carro (esto es necesario, pues a continuacion voy a sacar los "\" para que no puedan dañar)
    texto = Replace(texto, "\n", vbCrLf)
    'escapa el caracter de escape
    texto = Replace(texto, "\", "\\")
    'escapa comillas simples
    texto = Replace(texto, "'", "\'")
    'cambia retorno de carro por \n
    texto = Replace(texto, vbCrLf, "\n")
    'cambia retorno de carro simple por \n
    texto = Replace(texto, vbCr, "\n")
    'cambia linefeed simple por \n
    texto = Replace(texto, vbLf, "\n")
    'escapa comillas dobles
    Return Trim(Replace(texto, """", "\"""))
  End Function

  ''' <summary>
  ''' escapar comillas enrutinas JS
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function LimpiarTexto(ByVal texto As String) As String
    texto = Replace(texto, "\n", "")
    texto = Replace(texto, "\", "")
    texto = Replace(texto, "'", "")
    texto = Replace(texto, """", "")
    texto = Replace(texto, "´", "")
    Return Trim(texto)
  End Function


#End Region

#Region "Shared"
  ''' <summary>
  ''' Retorna objCheck si no es nulo, sino retorna objReplacement
  ''' </summary>
  ''' <param name="objCheck"></param>
  ''' <param name="objReplacement"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function IsNull(ByVal objCheck As Object, ByVal objReplacement As Object) As String
    If IsDBNull(objCheck) Then
      Return objReplacement
    Else
      Return objCheck
    End If
  End Function

  ''' <summary>
  ''' Retorna ValorActual si no es nulo, sino retorna ValorReemplazo
  ''' </summary>
  ''' <param name="ValorActual"></param>
  ''' <param name="ValorReemplazo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function IsNull(ByVal ValorActual As String, ByVal ValorReemplazo As String) As String
    If IsDBNull(ValorActual) Or ValorActual = "" Then
      Return ValorReemplazo
    Else
      Return ValorActual
    End If
  End Function

  ''' <summary>
  ''' convierte json a object
  ''' </summary>
  ''' <param name="json"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertJSONToObject(ByVal json As String) As Object
    Dim obj As Object
    Dim jss As New JavaScriptSerializer
    Try
      obj = jss.Deserialize(Of Object)(json)
    Catch ex As Exception
      obj = Nothing
    End Try
    Return obj
  End Function

  ''' <summary>
  ''' convierte un hashTable en json
  ''' </summary>
  ''' <param name="obj"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertHashTableToJSON(ByVal obj As Hashtable) As String
    Dim json As String
    Dim jss As New JavaScriptSerializer

    Try
      json = jss.Serialize(obj)
    Catch ex As Exception
      json = ""
    End Try
    Return json
  End Function

  ''' <summary>
  ''' convierte un SortedList en json
  ''' </summary>
  ''' <param name="obj"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertSortedListToJSON(ByVal obj As SortedList) As String
    Dim json As String
    Dim jss As New JavaScriptSerializer

    Try
      json = jss.Serialize(obj)
    Catch ex As Exception
      json = ""
    End Try
    Return json
  End Function

  ''' <summary>
  ''' retorna el largo del object
  ''' </summary>
  ''' <param name="obj"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function LengthObject(ByVal obj As Object) As Integer
    Dim largo As Integer
    Try
      largo = obj.length
    Catch ex As Exception
      largo = -1
    End Try
    Return largo
  End Function

  ''' <summary>
  ''' retorna el valor del item consultado
  ''' </summary>
  ''' <param name="obj"></param>
  ''' <param name="nombreItem"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ItemObject(ByVal obj As Object, ByVal nombreItem As String) As String
    Dim valor As String
    Try
      valor = obj.item(nombreItem)
    Catch ex As Exception
      valor = ""
    End Try
    Return valor
  End Function

  ''' <summary>
  ''' retorna el valor del item consultado
  ''' </summary>
  ''' <param name="obj"></param>
  ''' <param name="nombreItem"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ItemObjectJSON(ByVal obj As Object, ByVal nombreItem As String) As Object
    Dim valor As Object
    Try
      valor = obj.item(nombreItem)
    Catch ex As Exception
      valor = Nothing
    End Try
    Return valor
  End Function

  Public Shared Function ConvertObjectToJSON(ByVal obj As Object) As String
    Dim json As String = ""
    Try
      Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()
      json = serializer.Serialize(obj)
    Catch ex As Exception
      json = ""
    End Try
    Return json
  End Function

  Public Shared Function HtmlEncode(ByVal s As String) As String
    Try
      s = Replace(s, "á", "&aacute;")
      s = Replace(s, "é", "&eacute;")
      s = Replace(s, "í", "&iacute;")
      s = Replace(s, "ó", "&oacute;")
      s = Replace(s, "ú", "&uacute;")

      s = Replace(s, "Á", "&Aacute;")
      s = Replace(s, "É", "&Eacute;")
      s = Replace(s, "Í", "&Iacute;")
      s = Replace(s, "Ó", "&Oacute;")
      s = Replace(s, "Ú", "&Uacute;")

      s = Replace(s, "ñ", "&ntilde;")
      s = Replace(s, "Ñ", "&Ntilde;")

      s = Replace(s, """", "&quot;")
      s = Replace(s, "\", "\\")
    Catch ex As Exception
      s = s
    End Try
    Return s
  End Function

  Public Shared Function HtmlDecode(ByVal s As String) As String
    Try
      If (Not String.IsNullOrEmpty(s)) Then
        s = Replace(s, "&aacute;", "á")
        s = Replace(s, "&eacute;", "é")
        s = Replace(s, "&iacute;", "í")
        s = Replace(s, "&oacute;", "ó")
        s = Replace(s, "&uacute;", "ú")

        s = Replace(s, "&Aacute;", "Á")
        s = Replace(s, "&Eacute;", "É")
        s = Replace(s, "&Iacute;", "Í")
        s = Replace(s, "&Oacute;", "Ó")
        s = Replace(s, "&Uacute;", "Ú")

        s = Replace(s, "&ntilde;", "ñ")
        s = Replace(s, "&Ntilde;", "Ñ")

        s = Replace(s, "&quot;", """")
      End If
    Catch ex As Exception
      s = s
    End Try
    Return s
  End Function

  ''' <summary>
  ''' convierte datatable en json
  ''' </summary>
  ''' <param name="dt"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertDataTableToJSON(ByVal dt As DataTable) As String
    Dim json As String = ""
    Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()
    Dim objColumna As Dictionary(Of String, Object)
    Dim sb As New StringBuilder
    Dim totalRegistros As Integer
    Dim contador As Integer = 0

    Try
      totalRegistros = dt.Rows.Count
      sb.Append("[")

      For Each dr As DataRow In dt.Rows
        objColumna = New Dictionary(Of String, Object)
        For Each col As DataColumn In dt.Columns
          objColumna.Add(col.ColumnName, dr(col))
        Next
        sb.Append(serializer.Serialize(objColumna))

        If (contador < totalRegistros - 1) Then sb.Append(",")
        contador += 1
      Next

      sb.Append("]")
      json = sb.ToString()
    Catch ex As Exception
      json = ""
    End Try

    Return json
  End Function

  ''' <summary>
  ''' convierte datatable en json
  ''' </summary>
  ''' <param name="dt"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertDataTableToJSONMobile(ByVal dt As DataTable) As String
    Dim json As String = ""
    Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()
    Dim objColumna As Dictionary(Of String, Object)
    Dim sb As New StringBuilder
    Dim totalRegistros As Integer
    Dim contador As Integer = 0
    Dim idRegistro As String

    Try
      totalRegistros = dt.Rows.Count
      sb.Append("[")

      For Each dr As DataRow In dt.Rows
        objColumna = New Dictionary(Of String, Object)
        idRegistro = dr.Item("Id")

        For Each col As DataColumn In dt.Columns
          objColumna.Add(col.ColumnName, dr(col))
        Next

        sb.Append("{")
        sb.Append("""Id"":" & idRegistro)
        sb.Append(",""Estado"":""""")
        sb.Append(",""JSON"":" & serializer.Serialize(objColumna))
        sb.Append("}")

        If (contador < totalRegistros - 1) Then sb.Append(",")
        contador += 1
      Next

      sb.Append("]")
      json = sb.ToString()
    Catch ex As Exception
      json = ""
    End Try

    Return json
  End Function

  ''' <summary>
  ''' convierte dataset en json
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertDatasetToJSON(ByVal ds As DataSet) As String
    Dim json As String = ""
    Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()
    Dim objColumna As Dictionary(Of String, Object)
    Dim sbDataset As New StringBuilder
    Dim sbTabla As New StringBuilder
    Dim totalTablas, totalRegistros As Integer
    Dim contadorTablas As Integer = 0
    Dim contadorFilas As Integer = 0

    Try
      totalTablas = ds.Tables.Count
      sbDataset.Append("[")

      For Each dt As DataTable In ds.Tables
        '-----------------------------
        'begin -> registros por tabla
        totalRegistros = dt.Rows.Count
        sbTabla = New StringBuilder
        contadorFilas = 0
        sbTabla.Append("[")

        For Each dr As DataRow In dt.Rows
          objColumna = New Dictionary(Of String, Object)
          For Each col As DataColumn In dt.Columns
            objColumna.Add(col.ColumnName, dr(col))
          Next
          sbTabla.Append(serializer.Serialize(objColumna))

          If (contadorFilas < totalRegistros - 1) Then sbTabla.Append(",")
          contadorFilas += 1
        Next

        sbTabla.Append("]")
        sbDataset.Append(sbTabla.ToString())
        'end -> registros por tabla
        '-----------------------------

        If (contadorTablas < totalTablas - 1) Then sbDataset.Append(",")
        contadorTablas += 1
      Next

      sbDataset.Append("]")
      json = sbDataset.ToString()
    Catch ex As Exception
      json = ""
    End Try

    Return json
  End Function

  ''' <summary>
  ''' convierte dataset en json agrupado por un campo en particular
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertDatasetToJSONMobile(ByVal ds As DataSet, ByVal indiceTablaRegistrosPorListar As Integer, ByVal nombreColumnaIdRegistro As String) As String
    Dim json As String = ""
    Dim totalTablas As Integer
    Dim jsonTabla As String
    Dim sbJSON As New StringBuilder
    Dim sbTabla As New StringBuilder
    Dim totalRegistrosJSON As Integer
    Dim contadorRegistrosJSON As Integer = 0
    Dim contadorTablas As Integer = 0
    Dim idRegistro As Integer = 0

    Try
      If ds Is Nothing Then
        totalTablas = 0
      Else
        totalTablas = ds.Tables.Count
      End If

      If totalTablas = 0 Then
        json = ""
      Else
        totalRegistrosJSON = ds.Tables(indiceTablaRegistrosPorListar).Rows.Count
        If (totalRegistrosJSON = 0) Then
          json = ""
        Else
          sbJSON.Append("[")

          For Each dr As DataRow In ds.Tables(indiceTablaRegistrosPorListar).Rows
            idRegistro = dr(nombreColumnaIdRegistro)

            '---------------------------------------------
            'begin -> obtiene registros por tabla
            sbTabla = New StringBuilder
            contadorTablas = 0
            sbTabla.Append("[")

            For Each dt As DataTable In ds.Tables
              sbTabla.Append("{")

              '---------------------------------------------
              'begin -> obtiene registros por columnas
              Dim query = From fila In dt.AsEnumerable()
                          Where fila(nombreColumnaIdRegistro).Equals(idRegistro)
                          Select fila

              If (query.Count = 0) Then
                jsonTabla = "[]"
              Else
                jsonTabla = ConvertDataTableToJSON(query.CopyToDataTable())
              End If
              'end -> obtiene registros por columnas
              '---------------------------------------------

              sbTabla.Append("""Estado"":""""")
              sbTabla.Append(",""JSON"":" & jsonTabla)
              sbTabla.Append("}")
              If (contadorTablas < totalTablas - 1) Then sbTabla.Append(",")
              contadorTablas += 1

            Next
            sbTabla.Append("]")
            'end -> obtiene registros por tabla
            '---------------------------------------------

            sbJSON.Append("{")
            sbJSON.Append("""Id"":" & idRegistro)
            sbJSON.Append(",""Estado"":""""")
            sbJSON.Append(",""JSON"":" & sbTabla.ToString())
            sbJSON.Append("}")

            If (contadorRegistrosJSON < totalRegistrosJSON - 1) Then sbJSON.Append(",")
            contadorRegistrosJSON += 1
          Next

          sbJSON.Append("]")
          json = sbJSON.ToString()
        End If
      End If
    Catch ex As Exception
      json = ""
    End Try

    Return json
  End Function

  ''' <summary>
  ''' convierte dataset de inciente en json
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertDatasetToStringMobile(ByVal ds As DataSet) As String
    Dim linea, valorItem As String
    Dim totalTablas, totalRegistrosPorListar, totalColumnas As Integer
    Dim sb As New StringBuilder
    Dim sbTablas As New StringBuilder
    Dim sbDatos As New StringBuilder
    Dim dr As DataRow
    Dim dtRegistrosPorListar As DataTable
    Dim dsFiltrado As New DataSet

    Try
      If ds Is Nothing Then
        totalTablas = -1
      Else
        totalTablas = ds.Tables.Count
      End If

      If totalTablas > 0 Then
        dtRegistrosPorListar = ds.Tables(0)
        totalRegistrosPorListar = dtRegistrosPorListar.Rows.Count
        totalColumnas = dtRegistrosPorListar.Columns.Count

        If (totalRegistrosPorListar = 0) Then
          linea = ""
        Else
          For i = 0 To totalRegistrosPorListar - 1

            For j = 0 To totalColumnas - 1

              dr = dtRegistrosPorListar.Rows(i)
              valorItem = dr.Item(j)
              sb.Append(valorItem)

              If j <> totalColumnas - 1 Then sb.Append("~")

            Next

            If i <> totalRegistrosPorListar - 1 Then sb.Append("|")

          Next
          linea = sb.ToString
        End If
      Else
        linea = ""
      End If
    Catch ex As Exception
      linea = ""
    End Try
    Return linea

  End Function


#End Region

End Class
