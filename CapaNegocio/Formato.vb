﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Formato

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Formato"

  Public Enum eTabla As Integer
    Detalle = 0
  End Enum
#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de Formatos
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle de la Formato consultada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idFormato As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(0).Value = idFormato

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba una Formato
  ''' </summary>
  Public Shared Function GrabarDatos(ByRef idFormato As String, ByVal nombre As String, ByVal activo As String, ByVal llave As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdFormatoOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(2).Value = idFormato
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@Activo", SqlDbType.Int)
      arParms(4).Value = activo
      arParms(5) = New SqlParameter("@Llave", SqlDbType.VarChar)
      arParms(5).Value = llave

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_GrabarDatos", arParms)
      valor = arParms(0).Value
      idFormato = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Formato.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Formato.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para la Formato seleccionada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function VerificarDuplicidad(ByVal idFormato As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Formato_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(1).Value = idFormato
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' elimina una Formato
  ''' </summary>
  Public Shared Function EliminarDatos(ByVal idFormato As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(1).Value = idFormato

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_EliminarDatos", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Formato.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene perfiles asociados al formato
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerPerfilesAsociados(ByVal idFormato As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(0).Value = idFormato

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_ObtenerPerfilesAsociados", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' elimina los perfiles asociados al formato
  ''' </summary>
  Public Shared Function EliminarPerfilesAsociados(ByVal idFormato As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(1).Value = idFormato

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_EliminarPerfilesAsociados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' graba perfil asociado al formato
  ''' </summary>
  Public Shared Function GrabarPerfilAsociado(ByVal idFormato As String, ByVal idPerfil As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(1).Value = idFormato
      arParms(2) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(2).Value = idPerfil

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Formato_GrabarPerfilAsociado", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

#End Region

End Class
