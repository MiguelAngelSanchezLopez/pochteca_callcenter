﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Usuario

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Usuario"
  Public Const SESION_MATRIZ_PERMISO As String = "UsuarioMatrizPermiso"
  Public Const CLAVE_POR_DEFECTO As String = "43995A5D1D7688C0D97336153DF2DDA8CF900F0A" 'es: 123

  Public Shared IdUsuario As String

  Public Enum ResultadoLogin
    Existe = 1
    ClaveIncorrecta = 2
    UsernameNoExiste = 3
  End Enum

  Public Enum eTablaPermiso
    T00_ListadoCategorias = 0
    T01_ListadoPaginas = 1
    T02_ListadoFunciones = 2
  End Enum
#End Region

#Region "Campos"
  Private _id As Integer
  Private _username As String
  Private _password As String
  Private _rut As String
  Private _dv As String
  Private _nombre As String
  Private _paterno As String
  Private _materno As String
  Private _email As String
  Private _telefono As String
  Private _idDireccion As Integer
  Private _fechaHoraUltimoAcceso As String
  Private _ipUltimoAcceso As String
  Private _fechaHoraCreacion As String
  Private _fechaHoraModificacion As String
  Private _loginDias As String
  Private _estado As Integer
  Private _super As Boolean
  Private _idPerfil As Integer
  Private _perfilNombre As String
  Private _perfilLlave As String
  Private _clasificacionConductor As String
  Private _idTransportista As Integer
  Private _fechaCaducidadPassword As DateTime
  Private _passwordCaduco As Boolean
  Private _existe As Boolean

#End Region

#Region "Properties"
  Public Property [Id]() As Integer
    Get
      Return _id
    End Get
    Set(ByVal Value As Integer)
      _id = Value
    End Set
  End Property

  Public Property Username() As String
    Get
      Return _username
    End Get
    Set(ByVal Value As String)
      _username = Value
    End Set
  End Property

  Public Property Password() As String
    Get
      Return _password
    End Get
    Set(ByVal Value As String)
      _password = Value
    End Set
  End Property

  Public Property Rut() As String
    Get
      Return _rut
    End Get
    Set(ByVal Value As String)
      _rut = Value
    End Set
  End Property

  Public Property Dv() As String
    Get
      Return _dv
    End Get
    Set(ByVal Value As String)
      _dv = Value
    End Set
  End Property

  Public Property Nombre() As String
    Get
      Return _nombre
    End Get
    Set(ByVal Value As String)
      _nombre = Value
    End Set
  End Property

  Public Property Paterno() As String
    Get
      Return _paterno
    End Get
    Set(ByVal Value As String)
      _paterno = Value
    End Set
  End Property

  Public Property Materno() As String
    Get
      Return _materno
    End Get
    Set(ByVal Value As String)
      _materno = Value
    End Set
  End Property

  Public Property Email() As String
    Get
      Return _email
    End Get
    Set(ByVal Value As String)
      _email = Value
    End Set
  End Property

  Public Property Telefono() As String
    Get
      Return _telefono
    End Get
    Set(ByVal Value As String)
      _telefono = Value
    End Set
  End Property

  Public Property IdDireccion() As Integer
    Get
      Return _idDireccion
    End Get
    Set(ByVal Value As Integer)
      _idDireccion = Value
    End Set
  End Property

  Public Property FechaHoraUltimoAcceso() As String
    Get
      Return _fechaHoraUltimoAcceso
    End Get
    Set(ByVal Value As String)
      _fechaHoraUltimoAcceso = Value
    End Set
  End Property

  Public Property IpUltimoAcceso() As String
    Get
      Return _ipUltimoAcceso
    End Get
    Set(ByVal Value As String)
      _ipUltimoAcceso = Value
    End Set
  End Property

  Public Property FechHoraCreacion() As String
    Get
      Return _fechaHoraCreacion
    End Get
    Set(ByVal Value As String)
      _fechaHoraCreacion = Value
    End Set
  End Property

  Public Property FechHoraModificacion() As String
    Get
      Return _fechaHoraModificacion
    End Get
    Set(ByVal Value As String)
      _fechaHoraModificacion = Value
    End Set
  End Property

  Public Property LoginDias() As String
    Get
      Return _loginDias
    End Get
    Set(ByVal value As String)
      _loginDias = value
    End Set
  End Property

  Public Property Super() As Boolean
    Get
      Return _super
    End Get
    Set(ByVal Value As Boolean)
      _super = Value
    End Set
  End Property

  Public Property Estado() As Integer
    Get
      Return _estado
    End Get
    Set(ByVal Value As Integer)
      _estado = Value
    End Set
  End Property

  Public Property IdPerfil() As Integer
    Get
      Return _idPerfil
    End Get
    Set(ByVal Value As Integer)
      _idPerfil = Value
    End Set
  End Property

  Public Property PerfilNombre() As String
    Get
      Return _perfilNombre
    End Get
    Set(ByVal Value As String)
      _perfilNombre = Value
    End Set
  End Property

  Public Property PerfilLlave() As String
    Get
      Return _perfilLlave
    End Get
    Set(ByVal Value As String)
      _perfilLlave = Value
    End Set
  End Property

  Public ReadOnly Property NombreCompleto() As String
    Get
      Return Trim(IIf(String.IsNullOrEmpty(_nombre), "", _nombre) & IIf(String.IsNullOrEmpty(_paterno), "", " " & _paterno) & IIf(String.IsNullOrEmpty(_materno), "", " " & _materno))
    End Get
  End Property

  Public ReadOnly Property RutCompleto() As String
    Get
      Return _rut & "-" & _dv
    End Get
  End Property

  Public Property ClasificacionConductor() As String
    Get
      Return _clasificacionConductor
    End Get
    Set(ByVal Value As String)
      _clasificacionConductor = Value
    End Set
  End Property

  Public ReadOnly Property Existe() As Boolean
    Get
      Return _existe
    End Get
  End Property

  Public ReadOnly Property IdTransportista() As Integer
    Get
      Return _idTransportista
    End Get
  End Property

  Public Property FechaCaducidadPassword() As DateTime
    Get
      Return _fechaCaducidadPassword
    End Get
    Set(ByVal Value As DateTime)
      _fechaCaducidadPassword = Value
    End Set
  End Property

  Public ReadOnly Property PasswordCaduco() As Boolean
    Get
      Return _passwordCaduco
    End Get
  End Property

#End Region

#Region "Constructores"
  Public Sub New()
    MyBase.New()
  End Sub

  ''' <summary>
  ''' obtiene usuario a partir de su ID
  ''' </summary>
  ''' <param name="Id"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal Id As Integer)
    Dim storedProcedure As String = "spu_Usuario_ObtenerPorId"
    Dim descripcionError As String = "Error al crear una instancia de Usuario (" & Id & ") "
    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        If Not IsDBNull(dr.Item("Id")) Then _id = dr.Item("Id")
        If Not IsDBNull(dr.Item("Username")) Then _username = dr.Item("Username")
        If Not IsDBNull(dr.Item("Password")) Then _password = dr.Item("Password")
        If Not IsDBNull(dr.Item("Rut")) Then _rut = dr.Item("Rut")
        If Not IsDBNull(dr.Item("Dv")) Then _dv = dr.Item("Dv")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Paterno")) Then _paterno = dr.Item("Paterno")
        If Not IsDBNull(dr.Item("Materno")) Then _materno = dr.Item("Materno")
        If Not IsDBNull(dr.Item("Email")) Then _email = dr.Item("Email")
        If Not IsDBNull(dr.Item("Telefono")) Then _telefono = dr.Item("Telefono")
        If Not IsDBNull(dr.Item("IdDireccion")) Then _idDireccion = dr.Item("IdDireccion")
        If Not IsDBNull(dr.Item("FechaHoraUltimoAcceso")) Then _fechaHoraUltimoAcceso = dr.Item("FechaHoraUltimoAcceso")
        If Not IsDBNull(dr.Item("IpUltimoAcceso")) Then _ipUltimoAcceso = dr.Item("IpUltimoAcceso")
        If Not IsDBNull(dr.Item("FechaHoraCreacion")) Then _fechaHoraCreacion = dr.Item("FechaHoraCreacion")
        If Not IsDBNull(dr.Item("FechaHoraModificacion")) Then _fechaHoraModificacion = dr.Item("FechaHoraModificacion")
        If Not IsDBNull(dr.Item("LoginDias")) Then _loginDias = dr.Item("LoginDias")
        If Not IsDBNull(dr.Item("Super")) Then _super = dr.Item("Super")
        If Not IsDBNull(dr.Item("Estado")) Then _estado = dr.Item("Estado")
        If Not IsDBNull(dr.Item("IdPerfil")) Then _idPerfil = dr.Item("IdPerfil")
        If Not IsDBNull(dr.Item("ClasificacionConductor")) Then _clasificacionConductor = dr.Item("ClasificacionConductor")
        If Not IsDBNull(dr.Item("idUsuarioTransportista")) Then _idTransportista = dr.Item("idUsuarioTransportista")
        If Not IsDBNull(dr.Item("FechaCaducidadPassword")) Then _fechaCaducidadPassword = dr.Item("FechaCaducidadPassword")
        If Not IsDBNull(dr.Item("PasswordCaduco")) Then _passwordCaduco = dr.Item("PasswordCaduco")

        _existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"
  ''' <summary>
  ''' guarda las propiedades de la instancia como un nuevo registro y obtiene su Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub GuardarNuevo()
    Dim storedProcedure As String = "spu_Usuario_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(18) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Username", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Password", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@Rut", SqlDbType.VarChar)
      parms(4) = New SqlParameter("@Dv", SqlDbType.VarChar)
      parms(5) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      parms(6) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@Materno", SqlDbType.VarChar)
      parms(8) = New SqlParameter("@Email", SqlDbType.VarChar)
      parms(9) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      parms(10) = New SqlParameter("@IdDireccion", SqlDbType.VarChar)
      parms(11) = New SqlParameter("@FechaHoraUltimoAcceso", SqlDbType.VarChar)
      parms(12) = New SqlParameter("@IpUltimoAcceso", SqlDbType.VarChar)
      parms(13) = New SqlParameter("@FechaHoraCreacion", SqlDbType.VarChar)
      parms(14) = New SqlParameter("@FechaHoraModificacion", SqlDbType.VarChar)
      parms(15) = New SqlParameter("@LoginDias", SqlDbType.VarChar)
      parms(16) = New SqlParameter("@Super", SqlDbType.Bit)
      parms(17) = New SqlParameter("@Estado", SqlDbType.Int)
      parms(18) = New SqlParameter("@IdPerfil", SqlDbType.Int)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = _username
      parms(2).Value = _password
      parms(3).Value = Sistema.Capitalize(_rut, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(4).Value = Sistema.Capitalize(_dv, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(5).Value = Sistema.Capitalize(_nombre, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(6).Value = Sistema.Capitalize(_paterno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(7).Value = Sistema.Capitalize(_materno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(8).Value = Sistema.Capitalize(_email, Sistema.eTipoCapitalizacion.TodoMinuscula)
      parms(9).Value = _telefono
      parms(10).Value = _idDireccion
      parms(11).Value = _fechaHoraUltimoAcceso
      parms(12).Value = _ipUltimoAcceso
      parms(13).Value = _fechaHoraCreacion
      parms(14).Value = _fechaHoraModificacion
      parms(15).Value = _loginDias
      parms(16).Value = _super
      parms(17).Value = _estado
      parms(18).Value = _idPerfil

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      _id = parms(0).Value
      _existe = True

      Me.Refrescar()
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  ''' <summary>
  ''' guarda (update) todas las propiedades de la instancia en su registro en la BDD (usando el Id)
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Actualizar()
    ' si la instancia no existe no podemos actualizar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_Usuario_Actualizar"
    Dim descripcionError As String = "Error al actualizar " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(18) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Username", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Password", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@Rut", SqlDbType.VarChar)
      parms(4) = New SqlParameter("@Dv", SqlDbType.VarChar)
      parms(5) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      parms(6) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@Materno", SqlDbType.VarChar)
      parms(8) = New SqlParameter("@Email", SqlDbType.VarChar)
      parms(9) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      parms(10) = New SqlParameter("@IdDireccion", SqlDbType.VarChar)
      parms(11) = New SqlParameter("@FechaHoraUltimoAcceso", SqlDbType.VarChar)
      parms(12) = New SqlParameter("@IpUltimoAcceso", SqlDbType.VarChar)
      parms(13) = New SqlParameter("@FechaHoraCreacion", SqlDbType.VarChar)
      parms(14) = New SqlParameter("@FechaHoraModificacion", SqlDbType.VarChar)
      parms(15) = New SqlParameter("@LoginDias", SqlDbType.VarChar)
      parms(16) = New SqlParameter("@Super", SqlDbType.Bit)
      parms(17) = New SqlParameter("@Estado", SqlDbType.Int)
      parms(18) = New SqlParameter("@IdPerfil", SqlDbType.Int)

      parms(0).Value = _id
      parms(1).Value = _username
      parms(2).Value = _password
      parms(3).Value = Sistema.Capitalize(_rut, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(4).Value = Sistema.Capitalize(_dv, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(5).Value = Sistema.Capitalize(_nombre, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(6).Value = Sistema.Capitalize(_paterno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(7).Value = Sistema.Capitalize(_materno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(8).Value = Sistema.Capitalize(_email, Sistema.eTipoCapitalizacion.TodoMinuscula)
      parms(9).Value = _telefono
      parms(10).Value = _idDireccion
      parms(11).Value = _fechaHoraUltimoAcceso
      parms(12).Value = _ipUltimoAcceso
      parms(13).Value = _fechaHoraCreacion
      parms(14).Value = _fechaHoraModificacion
      parms(15).Value = _loginDias
      parms(16).Value = _super
      parms(17).Value = _estado
      parms(18).Value = _idPerfil

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)
      Me.Refrescar()
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  ''' <summary>
  ''' elimina fisicamente (delete) el registro que corresponde a la instancia
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Eliminar()
    ' si la instancia contiene un identificador no valido no se puede eliminar
    If _id < 1 Then Exit Sub
    ' si la instancia no existe no podemos eliminar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_Usuario_Eliminar"
    Dim descripcionError As String = "Error al eliminar " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try

  End Sub

  Public Shared Function Login(ByVal username As String, ByVal password As String, ByVal puntoAcceso As String) As String
    Dim storedProcedure As String = "spu_Usuario_Login"
    Dim descripcionError As String = "Error al Loguear"
    Dim data As DataSet = Nothing
    Dim idUsuario As String = "-1"
    Dim parms() As SqlParameter = New SqlParameter(3) {}

    Try
      parms(0) = New SqlParameter("@Username", SqlDbType.VarChar)
      parms(1) = New SqlParameter("@Password", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Status", SqlDbType.Int)
      parms(3) = New SqlParameter("@Id", SqlDbType.Int)

      parms(0).Value = username
      parms(1).Value = password
      parms(2).Direction = ParameterDirection.Output
      parms(3).Direction = ParameterDirection.Output

      data = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      Dim resultado As ResultadoLogin = parms(2).Value
      If resultado = ResultadoLogin.Existe Then
        ' si el usuario existe, obtenemos el id que nos devolvio el sp
        idUsuario = parms(3).Value
      Else
        idUsuario = "-1"
      End If

      ' devolvemos el codigo de resuldado
      Return idUsuario
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    End Try

  End Function

  ''' <summary>
  ''' determina si el usuario puede ingresar al sistema o no
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Login() As ResultadoLogin
    Dim storedProcedure As String = "spu_Usuario_Login"
    Dim descripcionError As String = "Error al Loguear"
    Dim data As DataSet = Nothing
    Dim parms() As SqlParameter = New SqlParameter(3) {}

    Try
      parms(0) = New SqlParameter("@Username", SqlDbType.VarChar)
      parms(1) = New SqlParameter("@Password", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Status", SqlDbType.Int)
      parms(3) = New SqlParameter("@Id", SqlDbType.Int)

      parms(0).Value = Username
      parms(1).Value = Password
      parms(2).Direction = ParameterDirection.Output
      parms(3).Direction = ParameterDirection.Output

      data = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      Dim resultado As ResultadoLogin = parms(2).Value
      If resultado = ResultadoLogin.Existe Then
        ' si el usuario existe, obtenemos el id que nos devolvio el sp
        _id = parms(3).Value
        _existe = True

        ' refrescamos el resto de las propiedades de la instancia
        Me.Refrescar()
      End If

      ' devolvemos el codigo de resuldado
      Return resultado
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      If Not data Is Nothing Then data.Dispose()
      data = Nothing
    End Try

  End Function

  ''' <summary>
  ''' devuelve si el usuario se puede loguear un dia en especifico 
  ''' </summary>
  ''' <param name="diaSemana"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function PuedeLoguearse(ByVal diaSemana As Integer) As Boolean
    If _loginDias.Length <> 7 Then Return False
    If diaSemana = System.DayOfWeek.Sunday Then diaSemana = 7
    Return CType(_loginDias.Substring(diaSemana - 1, 1), Boolean)
  End Function


  ''' <summary>
  ''' valida si el usuario tiene permiso para acceder a la aplicacion el diaSemana
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="diaSemana"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function PuedeLoguearse(ByVal idUsuario As String, ByVal diaSemana As Integer) As Boolean

    Dim loginDias As String
    Dim oUsuario As New Usuario(idUsuario)

    loginDias = oUsuario.LoginDias
    If loginDias.Length <> 7 Then Return False
    If diaSemana = System.DayOfWeek.Sunday Then diaSemana = 7
    Return CType(loginDias.Substring(diaSemana - 1, 1), Boolean)

  End Function

  Public Shared Function ReportePulsoDiario(ByVal FechaInicio As String, ByVal FechaTermino As String, ByVal TeleOperador As String, ByVal Turno As String) As DataSet
    Try
      Dim data As DataSet = Nothing
      Dim parms() As SqlParameter = New SqlParameter(3) {}

      parms(0) = New SqlParameter("@FechaInicio", SqlDbType.VarChar)
      parms(1) = New SqlParameter("@FechaTermino", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Jornada", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@IdTeleOP", SqlDbType.VarChar)

      parms(0).Value = FechaInicio
      parms(1).Value = FechaTermino
      parms(2).Value = Turno
      parms(3).Value = TeleOperador

      data = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_ReportePulsoDiario", parms)

      Return data
    Catch ex As Exception
      Return New DataSet
    End Try
  End Function

#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' busca sus datos en la BDD (usando el Id) y refresca todas las propiedades de la instancia
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub Refrescar()
    ' si la instancia no existe no podemos refrescar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_Usuario_ObtenerPorId"
    Dim descripcionError As String = "Error al refrescar una instancia de Usuario (" & _id & ") "

    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        If Not IsDBNull(dr.Item("Username")) Then _username = dr.Item("Username")
        If Not IsDBNull(dr.Item("Password")) Then _password = dr.Item("Password")
        If Not IsDBNull(dr.Item("Rut")) Then _rut = dr.Item("Rut")
        If Not IsDBNull(dr.Item("DV")) Then _dv = dr.Item("DV")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Paterno")) Then _paterno = dr.Item("Paterno")
        If Not IsDBNull(dr.Item("Materno")) Then _materno = dr.Item("Materno")
        If Not IsDBNull(dr.Item("Email")) Then _email = dr.Item("Email")
        If Not IsDBNull(dr.Item("Telefono")) Then _telefono = dr.Item("Telefono")
        If Not IsDBNull(dr.Item("IdDireccion")) Then _idDireccion = dr.Item("IdDireccion")
        If Not IsDBNull(dr.Item("FechaHoraUltimoAcceso")) Then _fechaHoraUltimoAcceso = dr.Item("FechaHoraUltimoAcceso")
        If Not IsDBNull(dr.Item("IpUltimoAcceso")) Then _ipUltimoAcceso = dr.Item("IpUltimoAcceso")
        If Not IsDBNull(dr.Item("FechaHoraCreacion")) Then _fechaHoraCreacion = dr.Item("FechaHoraCreacion")
        If Not IsDBNull(dr.Item("FechaHoraModificacion")) Then _fechaHoraModificacion = dr.Item("FechaHoraModificacion")
        If Not IsDBNull(dr.Item("LoginDias")) Then _loginDias = dr.Item("LoginDias")
        If Not IsDBNull(dr.Item("Super")) Then _super = dr.Item("Super")
        If Not IsDBNull(dr.Item("Estado")) Then _estado = dr.Item("Estado")
        If Not IsDBNull(dr.Item("IdPerfil")) Then _idPerfil = dr.Item("IdPerfil")
        If Not IsDBNull(dr.Item("ClasificacionConductor")) Then _clasificacionConductor = dr.Item("ClasificacionConductor")
        If Not IsDBNull(dr.Item("idUsuarioTransportista")) Then _idTransportista = dr.Item("idUsuarioTransportista")
        If Not IsDBNull(dr.Item("FechaCaducidadPassword")) Then _fechaCaducidadPassword = dr.Item("FechaCaducidadPassword")
        If Not IsDBNull(dr.Item("PasswordCaduco")) Then _passwordCaduco = dr.Item("PasswordCaduco")

        '_existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de usuarios
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="filtroNombre"></param>
  ''' <param name="filtroUsername"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 24/07/2009</remarks>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String, ByVal filtroUsername As String, ByVal idPerfil As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre
      arParms(2) = New SqlParameter("@Username", SqlDbType.VarChar)
      arParms(2).Value = filtroUsername
      arParms(3) = New SqlParameter("@IdPerfil", SqlDbType.VarChar)
      arParms(3).Value = idPerfil

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del usuario consultado
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Public Shared Function ObtenerDetalle(ByVal idUsuario As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba un usuario
  ''' </summary>
  ''' <param name="rut"></param>
  ''' <param name="dv"></param>
  ''' <param name="nombre"></param>
  ''' <param name="paterno"></param>
  ''' <param name="materno"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Public Shared Function GrabarDatos(ByRef idUsuario As String, ByVal rut As String, ByVal dv As String, ByVal nombre As String, _
                                     ByVal paterno As String, ByVal materno As String, ByVal username As String, ByVal password As String, _
                                     ByVal email As String, ByVal telefono As String, ByVal loginDias As String, ByVal super As String, _
                                     ByVal estado As String, ByVal idPerfil As String, ByVal listadoPermisos As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Usuario_GrabarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(15) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(2).Value = rut
      arParms(3) = New SqlParameter("@DV", SqlDbType.VarChar)
      arParms(3).Value = dv
      arParms(4) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(4).Value = nombre
      arParms(5) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      arParms(5).Value = paterno
      arParms(6) = New SqlParameter("@Materno", SqlDbType.VarChar)
      arParms(6).Value = materno
      arParms(7) = New SqlParameter("@Username", SqlDbType.VarChar)
      arParms(7).Value = username
      arParms(8) = New SqlParameter("@Password", SqlDbType.VarChar)
      arParms(8).Value = password
      arParms(9) = New SqlParameter("@Email", SqlDbType.VarChar)
      arParms(9).Value = email
      arParms(10) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(10).Value = telefono
      arParms(11) = New SqlParameter("@LoginDias", SqlDbType.VarChar)
      arParms(11).Value = loginDias
      arParms(12) = New SqlParameter("@Super", SqlDbType.Int)
      arParms(12).Value = super
      arParms(13) = New SqlParameter("@Estado", SqlDbType.Int)
      arParms(13).Value = estado
      arParms(14) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(14).Value = idPerfil
      arParms(15) = New SqlParameter("@ListadoPermisos", SqlDbType.VarChar)
      arParms(15).Value = listadoPermisos

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      idUsuario = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Usuario.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Usuario.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' actualiza un usuario
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="rut"></param>
  ''' <param name="dv"></param>
  ''' <param name="nombre"></param>
  ''' <param name="paterno"></param>
  ''' <param name="materno"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Public Shared Function ModificarDatos(ByVal idUsuario As String, ByVal rut As String, ByVal dv As String, ByVal nombre As String, _
                                         ByVal paterno As String, ByVal materno As String, ByVal username As String, ByVal password As String, _
                                         ByVal email As String, ByVal telefono As String, ByVal loginDias As String, ByVal super As String, _
                                         ByVal estado As String, ByVal idPerfil As String, ByVal listadoPermisos As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Usuario_ModificarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(15) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(2).Value = rut
      arParms(3) = New SqlParameter("@DV", SqlDbType.VarChar)
      arParms(3).Value = dv
      arParms(4) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(4).Value = nombre
      arParms(5) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      arParms(5).Value = paterno
      arParms(6) = New SqlParameter("@Materno", SqlDbType.VarChar)
      arParms(6).Value = materno
      arParms(7) = New SqlParameter("@Username", SqlDbType.VarChar)
      arParms(7).Value = username
      arParms(8) = New SqlParameter("@Password", SqlDbType.VarChar)
      arParms(8).Value = password
      arParms(9) = New SqlParameter("@Email", SqlDbType.VarChar)
      arParms(9).Value = email
      arParms(10) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(10).Value = telefono
      arParms(11) = New SqlParameter("@LoginDias", SqlDbType.VarChar)
      arParms(11).Value = loginDias
      arParms(12) = New SqlParameter("@Super", SqlDbType.Int)
      arParms(12).Value = super
      arParms(13) = New SqlParameter("@Estado", SqlDbType.Int)
      arParms(13).Value = estado
      arParms(14) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(14).Value = idPerfil
      arParms(15) = New SqlParameter("@ListadoPermisos", SqlDbType.VarChar)
      arParms(15).Value = listadoPermisos

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Usuario.ModificarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Usuario.ModificarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para el usuario seleccionado
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="valor"></param>
  ''' <param name="campoARevisar"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function VerificarDuplicidad(ByVal idUsuario As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Usuario_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' obtiene la matriz de permisos asociadas al usuario
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerMatrizPermisos(ByVal idUsuario As String, ByVal esSuper As Boolean) As DataSet
    Dim ds As New DataSet
    Try
      Dim storedProcedure As String = "spu_Usuario_ObtenerMatrizPermisos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@EsSuper", SqlDbType.Bit)
      arParms(1).Value = esSuper

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      Return ds
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Usuario.ObtenerMatrizPermisos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' graba datos extras asociados al usuario
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarDatosExtras(ByRef idUsuario As String, ByVal emailConCopia As String, ByVal notificarPorEmailHorarioNocturno As String, ByVal idUsuarioTransportista As String, _
                                           ByVal gestionarSoloCemtra As String, ByVal transportistasAsociados As String, ByVal centroDistribucionAsociados As String, _
                                           ByVal localesAsociados As String) As Integer
    Dim valor As Integer

    Try
      Dim arParms() As SqlParameter = New SqlParameter(8) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@EmailConCopia", SqlDbType.Text)
      arParms(2).Value = emailConCopia
      arParms(3) = New SqlParameter("@NotificarPorEmailHorarioNocturno", SqlDbType.Int)
      arParms(3).Value = notificarPorEmailHorarioNocturno
      arParms(4) = New SqlParameter("@IdUsuarioTransportista", SqlDbType.Int)
      arParms(4).Value = idUsuarioTransportista
      arParms(5) = New SqlParameter("@GestionarSoloCemtra", SqlDbType.VarChar)
      arParms(5).Value = gestionarSoloCemtra
      arParms(6) = New SqlParameter("@TransportistasAsociados", SqlDbType.VarChar)
      arParms(6).Value = transportistasAsociados
      arParms(7) = New SqlParameter("@CentroDistribucionAsociados", SqlDbType.VarChar)
      arParms(7).Value = centroDistribucionAsociados
      arParms(8) = New SqlParameter("@LocalesAsociados", SqlDbType.VarChar)
      arParms(8).Value = localesAsociados

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_GrabarDatosExtras", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' obtiene el estado de conexion de los teleoperadores
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerEstadoConexionTeleoperador() As DataSet
    Dim ds As New DataSet
    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_ObtenerEstadoConexionTeleoperador")
      Return ds
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Usuario.ObtenerEstadoConexionTeleoperador" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene el estado de conexion de los teleoperadoresCemtra
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerEstadoConexionTeleoperadorCemtra() As DataSet
    Dim ds As New DataSet
    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra")
      Return ds
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Usuario.ObtenerEstadoConexionTeleoperadorCemtra" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' graba datos extras asociados al usuario
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarTelefonoPorUsuario(ByVal idTelefonoPorUsuario As String, ByVal idUsuario As String, ByVal telefono As String, ByVal horaInicio As String, _
                                                  ByVal horaTermino As String) As Integer
    Dim valor As Integer

    Try
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTelefonoPorUsuario", SqlDbType.Int)
      arParms(1).Value = idTelefonoPorUsuario
      arParms(2) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(2).Value = idUsuario
      arParms(3) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(3).Value = telefono
      arParms(4) = New SqlParameter("@HoraInicio", SqlDbType.VarChar)
      arParms(4).Value = horaInicio
      arParms(5) = New SqlParameter("@HoraTermino", SqlDbType.VarChar)
      arParms(5).Value = horaTermino

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_GrabarTelefonoPorUsuario", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' elimina los telefonos que no estan referenciados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarTelefonosPorUsuarioNoReferenciados(ByVal idUsuario As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(2).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Usuario_EliminarTelefonosPorUsuarioNoReferenciados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

#End Region


End Class
