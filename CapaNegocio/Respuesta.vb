﻿Public Class Respuesta

#Region "Tipos definidos por usuario públicos"

  Public Enum eCodigo As Integer
    Vacio = 0
    Exito = 1
    [Error] = 2
    AutentificacionNoValida = 3
    NoSeHanIngresadoDatos = 4
    IngresarNombreCompleto = 5
    LimiteDeConsultas = 6
  End Enum

#End Region

#Region "Métodos públicos"

  ''' <summary>
  ''' obtiene la descripcion del codigo seleccionado
  ''' </summary>
  ''' <param name="codigo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDescripcionPorCodigo(ByVal codigo As eCodigo, Optional ByVal textoAdicional As String = "") As String
    Dim descripcion As String = ""
    Dim sinDescripcion As String = "No tiene descripción asociada"
    Dim sl As SortedList

    Try
      sl = CargarLista()
      descripcion = Herramientas.IsNull(sl.Item(CStr(codigo)), "")
      If String.IsNullOrEmpty(descripcion) Then descripcion = sinDescripcion
      'le agrega el texto adicional si es que no esta vacion
      If Not String.IsNullOrEmpty(textoAdicional) Then descripcion &= ": " & textoAdicional
    Catch ex As Exception
      descripcion = sinDescripcion
    End Try
    Return descripcion
  End Function

#End Region

#Region "Métodos privados"

  ''' <summary>
  ''' carga un arreglo con todos los mensajes de salida
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function CargarLista() As SortedList
    Dim aListado As New SortedList
    Try
      'crea la lista (codigo, descripcion)
      aListado.Add("0", "")
      aListado.Add("1", "El proceso finalizo con exito")
      aListado.Add("2", "Error")
      aListado.Add("3", "Autentificación no válida")
      aListado.Add("4", "No se han ingresado los datos solicitados")
      aListado.Add("5", "Se necesita ingresar el nombre completo")
      aListado.Add("6", "Ha llegado al limite de consultas permitidas")

    Catch ex As Exception
      aListado = Nothing
    End Try
    Return aListado
  End Function

#End Region

End Class

  
