﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class LogPerdidaSenal

#Region "Properties"
  Public Const NombreEntidad = "LogPerdidaSenal"
  Public Const Directorio = "LogPerdidaSenal\"

  Public Property Id As Integer
  Public Property NroTransporte As Integer
  Public Property PatenteTracto As String
  Public Property PatenteTrailer As String
  Public Property LocalDestino As Integer
  Public Property FechaHoraInicio As DateTime
  Public Property FechaHoraTermino As DateTime
  Public Property FechaHoraCreacion As DateTime
  Public Property Existe As Boolean

#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal Id As Integer)
    Try
      Me.Id = Id
      ObtenerPorId()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"

  Public Sub GuardarNuevo()
    Dim storedProcedure As String = "spu_LogPerdidaSenal_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(7) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar, 255)
      parms(3) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar, 255)
      parms(4) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      parms(5) = New SqlParameter("@FechaHoraInicio", SqlDbType.DateTime)
      parms(6) = New SqlParameter("@FechaHoraTermino", SqlDbType.DateTime)
      parms(7) = New SqlParameter("@FechaHoraCreacion", SqlDbType.DateTime)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.PatenteTracto
      parms(3).Value = Me.PatenteTrailer
      parms(4).Value = Me.LocalDestino
      parms(5).Value = Me.FechaHoraInicio
      parms(6).Value = Me.FechaHoraTermino
      parms(7).Value = Me.FechaHoraCreacion

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      Me.Id = parms(0).Value
      Me.Existe = True
      ObtenerPorId() ' refrescar la instancia
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub


#End Region

#Region "Metodos Privados"

  Private Sub ObtenerPorId()
    Try
      Me.Existe = False

      Dim storedProcedure As String = "spu_LogPerdidaSenal_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("FechaHoraInicio")) Then Me.FechaHoraInicio = row("FechaHoraInicio")
        If Not IsDBNull(row("FechaHoraTermino")) Then Me.FechaHoraTermino = row("FechaHoraTermino")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
      End If

    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

End Class
