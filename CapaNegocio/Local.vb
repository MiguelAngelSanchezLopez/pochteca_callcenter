﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Local

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Local"

  Public Enum eTabla As Integer
    Detalle = 0
  End Enum
#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de locales
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String, ByVal codigoInterno As String, ByVal idFormato As String, _
                                                 ByVal etapa As String, ByVal focoAlto As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre
      arParms(2) = New SqlParameter("@CodigoInterno", SqlDbType.Int)
      arParms(2).Value = codigoInterno
      arParms(3) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(3).Value = idFormato
      arParms(4) = New SqlParameter("@Etapa", SqlDbType.Int)
      arParms(4).Value = etapa
      arParms(5) = New SqlParameter("@FocoAlto", SqlDbType.Int)
      arParms(5).Value = focoAlto

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Local_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del local consultado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idLocal As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdLocal", SqlDbType.Int)
      arParms(0).Value = idLocal

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Local_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba un Local
  ''' </summary>
  Public Shared Function GrabarDatos(ByRef idLocal As String, ByVal nombre As String, ByVal idFormato As String, ByVal codigo As String, ByVal activo As String, _
                                     ByVal maximoHorasAlertaActiva As String, ByVal listadoUsuariosCargos As String, ByVal etapa As String, ByVal focoAlto As String, _
                                     ByVal focoTablet As String, ByVal telefono As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(12) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdLocalOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdLocal", SqlDbType.Int)
      arParms(2).Value = idLocal
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(4).Value = idFormato
      arParms(5) = New SqlParameter("@CodigoInterno", SqlDbType.Int)
      arParms(5).Value = codigo
      arParms(6) = New SqlParameter("@Activo", SqlDbType.Int)
      arParms(6).Value = activo
      arParms(7) = New SqlParameter("@MaximoHorasAlertaActiva", SqlDbType.Int)
      arParms(7).Value = maximoHorasAlertaActiva
      arParms(8) = New SqlParameter("@ListadoUsuariosCargos", SqlDbType.VarChar)
      arParms(8).Value = listadoUsuariosCargos
      arParms(9) = New SqlParameter("@Etapa", SqlDbType.Int)
      arParms(9).Value = etapa
      arParms(10) = New SqlParameter("@FocoAlto", SqlDbType.Int)
      arParms(10).Value = focoAlto
      arParms(11) = New SqlParameter("@FocoTablet", SqlDbType.Int)
      arParms(11).Value = focoTablet
      arParms(12) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(12).Value = telefono


      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Local_GrabarDatos", arParms)
      valor = arParms(0).Value
      idLocal = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Local.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Local.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para la Local seleccionada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function VerificarDuplicidad(ByVal idLocal As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Local_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdLocal", SqlDbType.Int)
      arParms(1).Value = idLocal
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' elimina un local
  ''' </summary>
  Public Shared Function EliminarDatos(ByVal idLocal As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdLocal", SqlDbType.Int)
      arParms(1).Value = idLocal

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Local_EliminarDatos", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Local.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene los cargos asociados al formato y local
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerCargosPorFormato(ByVal idFormato As String, ByVal idLocal As String, ByVal categoria As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(0).Value = idFormato
      arParms(1) = New SqlParameter("@IdLocal", SqlDbType.Int)
      arParms(1).Value = idLocal
      arParms(2) = New SqlParameter("@Categoria", SqlDbType.VarChar)
      arParms(2).Value = categoria

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Local_ObtenerCargosPorFormato", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' determina si el local es foco
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EsFoco(ByVal codigoInterno As Integer) As Boolean
    Dim status As Boolean = False
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Bit)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@CodigoInterno", SqlDbType.Int)
      arParms(1).Value = codigoInterno

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, "spu_Local_EsFoco", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

    Return status
  End Function

  ''' <summary>
  '''obtiene informacion del local si tiene alguno asociado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerInformacionLocalAsociadoUsuario(ByVal idUsuario As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Local_ObtenerInformacionLocalAsociadoUsuario", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    Return ds
  End Function


#End Region

End Class
