Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Net.Mail
Imports System.Data
Imports System.Net.Mime

Public Class Mail
  ''' <summary>
  ''' construye un string de mail a partir de un datatable y un separador
  ''' </summary>
  ''' <param name="dataTable"></param>
  ''' <param name="indiceColumnaMailDataTable"></param>
  ''' <param name="caracterSeparadorMail"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 25/06/2008</remarks>
  Public Shared Function ConstruirStringMailConSeparador(ByVal dataTable As DataTable, ByVal indiceColumnaMailDataTable As Integer, Optional ByVal caracterSeparadorMail As String = ";") As String
    'si el dataTable es vacio entonces retorna nada
    If dataTable Is Nothing Then
      Return ""
      Exit Function
    End If

    Dim totalRegistros As Integer = dataTable.Rows.Count
    Dim i As Integer
    Dim email As String
    Dim listadoEmail As String = ""

    'recorre el datatable
    For i = 0 To totalRegistros - 1
      email = dataTable.Rows(i).Item(indiceColumnaMailDataTable)
      If Herramientas.EsEmail(email) Then
        listadoEmail += email
        'si no esta escribiendo el ultimo registro entonces coloca un separador al final
        If i < (totalRegistros - 1) Then listadoEmail += caracterSeparadorMail
      End If
    Next
    'retorna valor
    Return listadoEmail
  End Function

  ''' <summary>
  ''' construye un string de mail a partir de un datatable y un separador, sin que se repita el lista a revisar
  ''' </summary>
  ''' <param name="dataTable"></param>
  ''' <param name="indiceColumnaMailDataTable"></param>
  ''' <param name="listadoMailRevisar"></param>
  ''' <param name="caracterSeparadorMail"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConstruirStringMailConSeparadorSinRepetirDeLista(ByVal dataTable As DataTable, ByVal indiceColumnaMailDataTable As Integer, ByVal listadoMailRevisar As String, Optional ByVal caracterSeparadorMail As String = ";") As String
    'si el dataTable es vacio entonces retorna nada
    If dataTable Is Nothing Then
      Return ""
      Exit Function
    End If

    Dim totalRegistros As Integer = dataTable.Rows.Count
    Dim i As Integer
    Dim email As String
    Dim listadoEmail As String = ""

    'formate el listado de mail a revisar para poder realizar la busqueda del mail y que no se repita
    listadoMailRevisar = listadoMailRevisar.Replace(",", caracterSeparadorMail)
    listadoMailRevisar = caracterSeparadorMail & listadoMailRevisar & caracterSeparadorMail

    'recorre el datatable
    For i = 0 To totalRegistros - 1
      email = dataTable.Rows(i).Item(indiceColumnaMailDataTable)
      If Herramientas.EsEmail(email) Then
        'si no encuentra coincidencia entonces agrega el mail al string
        If InStr(listadoMailRevisar, caracterSeparadorMail & email & caracterSeparadorMail) = 0 Then
          listadoEmail += email
          'si no esta escribiendo el ultimo registro entonces coloca un separador al final
          If i < (totalRegistros - 1) Then listadoEmail += caracterSeparadorMail
        End If
      End If
    Next
    'retorna valor
    Return listadoEmail
  End Function

  ''' <summary>
  ''' construye coleccion de mail a partir de un string separado por ";" o ","
  ''' </summary>
  ''' <param name="listadoCorreos"></param>
  ''' <param name="caracterSeparadorMail"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 25/06/2008</remarks>
  Public Shared Function ConstruirCollectionMail(ByVal listadoCorreos As String, Optional ByVal caracterSeparadorMail As String = ";") As MailAddressCollection
    'si la lista es vacia entonces retorna nada
    If listadoCorreos = String.Empty Then
      Return Nothing
      Exit Function
    End If

    ' obtenemos el (o los) correo para las notificaciones
    Dim aListadoEmail As String() = Split(listadoCorreos.Replace(",", caracterSeparadorMail), caracterSeparadorMail)

    Dim ColeccionEmails As New System.Net.Mail.MailAddressCollection
    For Each direccion As String In aListadoEmail
      direccion = direccion.Trim()
      If Not String.IsNullOrEmpty(direccion) Then
        Dim email As MailAddress = New MailAddress(direccion)
        If Herramientas.EsEmail(direccion) And Not ColeccionEmails.Contains(email) Then ColeccionEmails.Add(direccion)
      End If
    Next
    Return ColeccionEmails
  End Function

  ''' <summary>
  ''' envia mail
  ''' </summary>
  ''' <param name="mailHost"></param>
  ''' <param name="emailEmisor"></param>
  ''' <param name="passEmailEmisor"></param>
  ''' <param name="nombreEmisor"></param>
  ''' <param name="coleccionEmailDestino"></param>
  ''' <param name="asunto"></param>
  ''' <param name="mensaje"></param>
  ''' <param name="esHTML"></param>
  ''' <param name="coleccionEmailDestinoConCopia"></param>
  ''' <param name="enviarArchivoAdjunto"></param>
  ''' <param name="rutaArchivoAdjunto"></param>
  ''' <param name="coleccionEmailDestinoConCopiaOculta"></param>
  ''' <param name="emailResponderA"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 25/06/2008</remarks>
  Public Shared Function Enviar(ByVal mailHost As String, ByVal emailEmisor As String, ByVal passEmailEmisor As String, _
                                ByVal nombreEmisor As String, ByVal coleccionEmailDestino As System.Net.Mail.MailAddressCollection, _
                                ByVal asunto As String, ByVal mensaje As String, Optional ByVal esHTML As Boolean = False, _
                                Optional ByVal coleccionEmailDestinoConCopia As System.Net.Mail.MailAddressCollection = Nothing, _
                                Optional ByVal enviarArchivoAdjunto As Boolean = False, Optional ByVal rutaArchivoAdjunto As String = "", _
                                Optional ByVal emailResponderA As String = "", _
                                Optional ByVal coleccionEmailDestinoConCopiaOculta As System.Net.Mail.MailAddressCollection = Nothing) As String


    Dim msgError As String = ""
    Dim data As Attachment = Nothing
    Dim rutaParseada As String()
    Dim saltoLinea As String

    Try
      Dim correo As New MailMessage()

      If coleccionEmailDestino Is Nothing And coleccionEmailDestinoConCopia Is Nothing And coleccionEmailDestinoConCopiaOculta Is Nothing Then
        Return "No se puede enviar correo porque no hay destinatarios."
        Exit Function
      End If

      If Not coleccionEmailDestino Is Nothing Then
        For Each Email As MailAddress In coleccionEmailDestino
          correo.To.Add(Email)
        Next
      End If

      If Not coleccionEmailDestinoConCopia Is Nothing Then
        For Each EmailCC As MailAddress In coleccionEmailDestinoConCopia
          correo.CC.Add(EmailCC)
        Next
      End If

      If Not coleccionEmailDestinoConCopiaOculta Is Nothing Then
        For Each EmailCCO As MailAddress In coleccionEmailDestinoConCopiaOculta
          correo.Bcc.Add(EmailCCO)
        Next
      End If

      If Not String.IsNullOrEmpty(emailResponderA) Then
        If Herramientas.EsEmail(emailResponderA) Then
          Dim ColeccionEmailsReplyTo As New System.Net.Mail.MailAddress(emailResponderA)
          correo.ReplyTo = ColeccionEmailsReplyTo
        End If
      End If

      If enviarArchivoAdjunto Then
        If rutaArchivoAdjunto <> String.Empty Then
          'parseamos la ruta que quedara separada por ;
          rutaParseada = rutaArchivoAdjunto.Split(";")
          For x As Integer = 0 To rutaParseada.Length - 1 'hasta parseo finalizado
            'Comprobamos que exista la ruta
            If Archivo.ExisteArchivoEnDisco(rutaParseada(x)) Then

              'Creamos el archivo attachment para este correo electr�nico.
              data = New Attachment(rutaParseada(x), MediaTypeNames.Application.Octet)
              'A�adimos la informaci�n de time stamp para el archivo.
              Dim disposition As ContentDisposition = data.ContentDisposition
              disposition.CreationDate = System.IO.File.GetCreationTime(rutaParseada(x))
              disposition.ModificationDate = System.IO.File.GetLastWriteTime(rutaParseada(x))
              disposition.ReadDate = System.IO.File.GetLastAccessTime(rutaParseada(x))
              'A�adimos el archivo attachment a este correo.
              correo.Attachments.Add(data)

            End If
          Next x
        End If
      End If

      correo.From = New MailAddress(emailEmisor, IIf(String.IsNullOrEmpty(nombreEmisor), emailEmisor, nombreEmisor))
      correo.Subject = asunto
      correo.Body = mensaje
      correo.IsBodyHtml = esHTML
      correo.BodyEncoding = System.Text.Encoding.UTF8
      correo.Priority = MailPriority.High
      Dim smtp As New SmtpClient
      smtp.Host = mailHost
      '[NOTA]: usar esta configuracion para usar el serviro smpt.gmail.com
      'smtp.Credentials = New NetworkCredential(emailEmisor, passEmailEmisor)
      'smtp.EnableSsl = True
      'smtp.Port = 587
      smtp.EnableSsl = False
      smtp.Port = 25
      smtp.Send(correo)
    Catch ex As SmtpException
      saltoLinea = IIf(esHTML, "<br/>", vbCrLf)
      msgError = "[StatusCode]: " & ex.StatusCode & saltoLinea & _
                 "[Message]: " & ex.Message & saltoLinea & _
                 "[Source]: " & ex.Source & saltoLinea & _
                 "[StackTrace]: " & ex.StackTrace & saltoLinea
    End Try

    If Not data Is Nothing Then data.Dispose()
    'retorna valor
    Return msgError
  End Function
End Class
