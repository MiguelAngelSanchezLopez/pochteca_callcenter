﻿Imports CapaNegocio
Imports System.Data

Public Class waReporte
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim opcion As String = Server.UrlDecode(Request("op"))
    Dim respuesta As String
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      Select Case opcion
        Case "ObtenerDatosJSON"
          respuesta = ObtenerDatosJSON()
        Case "ObtenerGeneracionInforme"
          respuesta = ObtenerGeneracionInforme()
        Case "ObtenerTablaDatos"
          respuesta = ObtenerTablaDatos()
        Case Else
          respuesta = Sistema.eCodigoSql.Error
      End Select
      Response.Write(respuesta)
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub

#Region "ObtenerDatosJSON"
  ''' <summary>
  ''' obtiene informacion del reporte y lo devuelve en json
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerDatosJSON() As String
    Dim retorno As String = ""
    Dim idReporte As String = Utilidades.IsNull(Request("idReporte"), "-1")
    Dim ds As New DataSet

    Try
      ds = Reporte.ObtenerDetalle(idReporte)
      If (Not ds Is Nothing) Then
        retorno = MyJSON.ConvertDataTableToJSON(ds.Tables(0))
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

#End Region

#Region "ObtenerGeneracionInforme"
  ''' <summary>
  ''' obtiene informacion del local en estructura JSON
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerGeneracionInforme() As String
    Dim retorno As String = ""

    Try
      If Session(Reporte.KEY_SESION_REPORTE_GENERADO) Is Nothing Then
        retorno = "0"
      Else
        retorno = CType(Session(Reporte.KEY_SESION_REPORTE_GENERADO), String)
        If retorno = "1" Then Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function
#End Region

#Region "ObtenerTablaDatos"
  ''' <summary>
  ''' obtiene datos del reporte solicitado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerTablaDatos() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""
    Dim idReporte As String = Utilidades.IsNull(Request("idReporte"), "-1")
    Dim ds As New DataSet
    Dim dr As DataRow
    Dim llaveReporte, procedimientoAlmacenado As String
    Dim totalRegistros, estado As Integer
    Dim sb As New StringBuilder

    Try
      ds = Reporte.ObtenerDetalle(idReporte)
      dr = ds.Tables(0).Rows(0)

      llaveReporte = dr.Item("Llave")
      procedimientoAlmacenado = dr.Item("ProcedimientoAlmacenado")

      Select Case llaveReporte
        Case "Generico"
          ds = ObtenerTablaDatos_ReporteGenerico(procedimientoAlmacenado)
        Case Else
          Throw New Exception("No tiene llave asociada el reporte")
      End Select

      If (ds Is Nothing) Then
        estado = Sistema.eCodigoSql.SinDatos
        retorno = Utilidades.MostrarMensajeUsuario("No se encontraron registros", Utilidades.eTipoMensajeAlert.Warning, True)
      Else
        totalRegistros = ds.Tables(0).Rows.Count
        If (totalRegistros = 0) Then
          estado = Sistema.eCodigoSql.SinDatos
          retorno = Utilidades.MostrarMensajeUsuario("No se encontraron registros", Utilidades.eTipoMensajeAlert.Warning, True)
        Else
          estado = Sistema.eCodigoSql.Exito
          retorno = oUtilidades.ConvertirDatatableToHTML(ds.Tables(0))
          oUtilidades.GrabarObjectSession(Reporte.KEY_SESION_DATASET, ds)
        End If
      End If

      Sistema.GrabarLogSesion(oUsuario.Id, "Busca datos reporte: " & procedimientoAlmacenado)
    Catch ex As Exception
      estado = Sistema.eCodigoSql.Error
      retorno = Utilidades.MostrarMensajeUsuario(ex.Message, Utilidades.eTipoMensajeAlert.Danger, True)
    End Try

    sb.Append("{")
    sb.Append(" ""estado"":""" & estado & """")
    sb.Append(",""listado"":""" & Utilidades.EscaparComillasJS(retorno) & """")
    sb.Append("}")

    Return sb.ToString()
  End Function

  ''' <summary>
  ''' obtiene reporte que tiene como filtro datos comunes (fechaDesde, fechaHasta, entre otros)
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerTablaDatos_ReporteGenerico(ByVal procedimientoAlmacenado As String) As DataSet
    Dim ds As New DataSet
    Dim jsonDatos As String = Server.UrlDecode(Utilidades.IsNull(Request("jsonDatos"), "[]"))
    Dim objJSON, row As Object
    Dim fechaDesde, fechaHasta As String

    Try
      If (String.IsNullOrEmpty(procedimientoAlmacenado)) Then
        Throw New Exception("No tiene procedimiento almacenado asociado")
      End If

      objJSON = MyJSON.ConvertJSONToObject(jsonDatos)
      row = objJSON(0)
      fechaDesde = Utilidades.IsNull(MyJSON.ItemObject(row, "fechaDesde"), "-1")
      fechaHasta = Utilidades.IsNull(MyJSON.ItemObject(row, "fechaHasta"), "-1")
      ds = Reporte.ObtenerDatosGenericos(procedimientoAlmacenado, fechaDesde, fechaHasta)

      Return ds
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

#End Region

End Class