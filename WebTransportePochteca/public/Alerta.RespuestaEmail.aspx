﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Alerta.RespuestaEmail.aspx.vb" Inherits="WebTransportePochteca.Alerta_RespuestaEmail" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
    <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
    <link rel="stylesheet" type="text/css" href="../css/select2.css" />
    <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/jquery.select2.js"></script>
    <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="../js/validacion.js"></script>
    <script type="text/javascript" src="../js/sistema.js"></script>
    <script type="text/javascript" src="../js/alerta.RespuestaEmail.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
        <div class="form-group text-center">
            <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3">Responder Alerta</asp:Label>
            <div>
                <asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
            <asp:Panel ID="pnlMensajeUsuario" runat="server">
            </asp:Panel>
        </div>
        <div class="form-group">
            <asp:Panel ID="pnlMensajeAcceso" runat="server">
            </asp:Panel>
            <asp:Panel ID="pnlContenido" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <asp:Label ID="lblTipoAlerta" runat="server"></asp:Label>
                            -
                            <asp:Label ID="lblTipoAlertaDescripcion" runat="server"></asp:Label>
                            &nbsp;
                            <asp:Label ID="lblFechaHora" runat="server"></asp:Label>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group row">
                            <div class="col-xs-2">
                                <label class="control-label">IdMaster</label>
                                <div><asp:Label ID="lblNroTransporte" runat="server"></asp:Label></div>
                            </div>
                            <div class="col-xs-2">
                                <label class="control-label">
                                    Determinante Tienda Destino
                                </label>
                                <div>
                                    <asp:Label ID="lblCodDestino" runat="server"></asp:Label>
                                    -
                                    <asp:Label ID="lblDescripcionDestino" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <label class="control-label">
                                    Placa Tracto</label>
                                <div>
                                    <asp:Label ID="lblPatenteTracto" runat="server"></asp:Label></div>
                            </div>
                            <div class="col-xs-2">
                                <label class="control-label">
                                    Placa Remolque</label>
                                <div>
                                    <asp:Label ID="lblPatenteTrailer" runat="server"></asp:Label></div>
                            </div>
                            <div class="col-xs-2">
                                <div>
                                    <div id="btnVerMapa" runat="server" class="btn btn-success">&nbsp;Ver ubicación Incidente</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-5">
                                <label class="control-label">
                                    Respuesta<strong class="text-danger">&nbsp;*</strong></label>
                                <uc1:wucCombo ID="ddlRespuestaCategoria" runat="server" FuenteDatos="Tabla" TipoCombo="RespuestaCategoria"
                                    CssClass="form-control chosen-select" DeshabilitarConUnItem="true" ItemSeleccione="true"></uc1:wucCombo>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-5">
                                <label class="control-label">
                                    Observación</label>
                                <asp:TextBox ID="txtObservacion" runat="server" MaxLength="300" CssClass="form-control"
                                    TextMode="MultiLine" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form-group text-center">
                <%--<div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="cerrar();">
                    <span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>--%>
                <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return AlertaRespuesta.validarFormulario()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Responder</asp:LinkButton>
            </div>
        </div>
    </div>
    </form>
</body>

  <script type="text/javascript">
      jQuery(document).ready(function () {
          jQuery(".chosen-select").select2();

      });
  </script>

</html>
