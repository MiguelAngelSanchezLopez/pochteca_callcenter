﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Alerta_RespuestaEmail

  '''<summary>
  '''Control form1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

  '''<summary>
  '''Control lblTituloFormulario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTituloFormulario As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblIdRegistro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblIdRegistro As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control pnlMensajeUsuario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeUsuario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control lblTipoAlerta.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTipoAlerta As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblTipoAlertaDescripcion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTipoAlertaDescripcion As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblFechaHora.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblFechaHora As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblNroTransporte.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblNroTransporte As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblCodDestino.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblCodDestino As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblDescripcionDestino.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblDescripcionDestino As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblPatenteTracto.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblPatenteTracto As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblPatenteTrailer.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblPatenteTrailer As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control btnVerMapa.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnVerMapa As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control ddlRespuestaCategoria.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlRespuestaCategoria As Global.WebTransportePochteca.wucCombo

  '''<summary>
  '''Control txtObservacion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtObservacion As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control btnGrabar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnGrabar As Global.System.Web.UI.WebControls.LinkButton
End Class
