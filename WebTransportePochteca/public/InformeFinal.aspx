﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InformeFinal.aspx.vb" Inherits="WebTransportePochteca.InformeFinal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title></title>
</head>
<body>
  <form id="form1" runat="server">
  <div>
    <!-- 
    dimensiones hoja informe aprox:
    width: 100%
    height: 815px
    -->
    <asp:Literal ID="ltlContenido" runat="server"></asp:Literal>
  </div>
  </form>
</body>
</html>
