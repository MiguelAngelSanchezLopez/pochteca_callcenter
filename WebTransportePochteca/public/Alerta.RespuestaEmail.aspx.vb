﻿Imports CapaNegocio

Public Class Alerta_RespuestaEmail
  Inherits System.Web.UI.Page

  Private Sub ObtenerDatos()
    Dim ds As New DataSet

    Try
      ds = Alerta.ObtenerDetalle(Me.ViewState.Item("idAlerta"))
    Catch ex As Exception
      ds = Nothing
    End Try

    Session("ds") = ds
  End Sub

  Private Sub ActualizaInterfaz()

    Dim ds As DataSet = CType(Session("ds"), DataSet)

    If (ds.Tables.Count > 0) Then
      Dim dr As DataRow = ds.Tables(0).Rows(0)

      lblNroTransporte.Text = dr.Item("NroTransporte")
      lblCodDestino.Text = dr.Item("LocalDestinoCodigo")
      lblDescripcionDestino.Text = dr.Item("LocalDestinoDescripcion")
      lblPatenteTracto.Text = dr.Item("PatenteTracto")
      lblPatenteTrailer.Text = dr.Item("PatenteTrailer")
      lblFechaHora.Text = dr.Item("FechaHoraCreacion")
      lblTipoAlerta.Text = dr.Item("TipoAlerta")
      lblTipoAlertaDescripcion.Text = dr.Item("TipoAlertaDescripcion")

      btnVerMapa.Attributes.Add("onclick", "AlertaRespuesta.abrirFormulario('" & dr.Item("AlertaMapa") & "')")
    End If

  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then

      Dim argCrypt As String = Request.QueryString("arg")
      Dim textoDesencriptado As String = Criptografia.DesencriptarTripleDES(argCrypt)
      If textoDesencriptado <> "" Then
        Dim arrParametros As Array = Criptografia.ObtenerArrayQueryString(textoDesencriptado)

        Me.ViewState.Add("idAlerta", Criptografia.RequestQueryString(arrParametros, "idAlerta"))

        ObtenerDatos()
        ActualizaInterfaz()
      Else
        pnlContenido.Visible = False
        btnGrabar.Visible = False

        Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
      End If

    End If
  End Sub

  Protected Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
    GrabarDatos()
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Sub GrabarDatos()
    Dim idAlerta As Integer = Me.ViewState.Item("idAlerta")
    Dim idCategoriaResp As Integer = ddlRespuestaCategoria.SelectedValue
    Dim observacion As String = txtObservacion.Text
    Dim msg As String = ""

    Try

      Alerta.GrabarRespuesta(idAlerta, idCategoriaResp, observacion)

      enableControls(False)

      msg = "La respuesta fue grabada satisfactoriamente"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, msg, Utilidades.eTipoMensajeAlert.Success)

    Catch ex As Exception

      msg = "Se produjo un error interno, no se pudo enviar la respuesta.<br />" & ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, msg, Utilidades.eTipoMensajeAlert.Danger)

    End Try
    'reto

  End Sub

  Private Sub enableControls(ByVal status As Boolean)
    btnGrabar.Enabled = status
    ddlRespuestaCategoria.Enabled = status
    txtObservacion.Enabled = status
  End Sub
End Class