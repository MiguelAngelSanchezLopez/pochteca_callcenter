Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Globalization.CultureInfo
Imports System.Configuration.ConfigurationManager
Imports System.Web.HttpServerUtility
Imports System.IO
Imports CapaNegocio
Imports System.Threading
Imports System.Drawing
Imports System.Xml

Public Class Utilidades

  Inherits System.Web.UI.Page

#Region "Constructor"
  ''' <summary>
  ''' constructor de la clase
  ''' </summary>
  ''' <remarks></remarks>
  Sub New()
    MyBase.New()
  End Sub
#End Region

#Region "Enum & Constantes"
  Public Const TamanoMaximoArchivoUploadEnKb As Integer = 10240
  Public Const INDICE_DIA_LUNES As Integer = 1
  Public Const SESION_HTML_MENU As String = "HtmlMenu"
  Public Const KEY_SESION_MENSAJE As String = "MSJ_Mensaje"

  Public Enum eUnidadMedidaArchivo As Integer
    Bytes = 1
    KiloBytes = 2
    MegaBytes = 3
    GigaBytes = 4
    TeraBytes = 5
  End Enum

  Public Enum eAmbienteEjecucion
    Demo
    Desarrollo
    Produccion
  End Enum

  Public Enum eRegistrar
    INICIO = 0
    FINAL = 1
  End Enum

  Public Enum eFormatoConvertirNumero
    Numerico = 0
    Texto = 1
  End Enum

  Public Enum eTipoMensajeAlert
    Danger
    Info
    Success
    Warning
  End Enum


  Private Const CARACTERES_NO_VALIDOS_REGEXP As String = "<|>" 'string separado por "|" usado para la expresion regular
  Private Const PREFIJO_SESION_CONTADOR_CARACTERES As String = "ContadorCaracteres_"
  Public Const VALOR_VALIDAR_RUT_MODULO11 As String = "ValidarRutModulo11"

#End Region

#Region "Sesiones"
  Public Sub CerrarSesion()
    ' eliminamos los datos de la sesion, los tickets de autorizacion y redireccionamos al login
    Session.Clear()
    Session.Abandon()
    FormsAuthentication.SignOut()
    FormsAuthentication.RedirectToLoginPage()

  End Sub

  Public Sub InicializarSesion(ByVal oUsuario As Usuario)
    Dim oPerfil As New Perfil(oUsuario.IdPerfil)
    Dim paginaInicio As String

    ' limpiamos la sesion y desautenticamos por si acaso
    Session.Clear()
    FormsAuthentication.SignOut()

    ' asigna valores del perfil
    oUsuario.PerfilLlave = oPerfil.Llave
    oUsuario.PerfilNombre = oPerfil.Nombre

    If (oUsuario.PasswordCaduco) Then
      paginaInicio = Configuracion.Leer("webPaginaInicio")
    Else
      paginaInicio = IIf(String.IsNullOrEmpty(oPerfil.PaginaInicio), Configuracion.Leer("webPaginaInicio"), oPerfil.PaginaInicio)
    End If

    ' se asignan los nuevos valores a la sesion
    Session("Usuario") = oUsuario
    Session("PaginaDeInicio") = paginaInicio
    Session("FechaUltimoAccesoUsuario") = oUsuario.FechaHoraUltimoAcceso
    Session.Timeout = Configuracion.Leer("webSessionTimeout")

    ' creamos la sesion formalmente
    Dim hashCookieSesion As String = Criptografia.obtenerSHA1(oUsuario.Username)
    FormsAuthentication.SetAuthCookie(hashCookieSesion, False)

    ' actualizamos algunos datos del usuario
    oUsuario.FechaHoraUltimoAcceso = Utilidades.FechaHoraISO
    oUsuario.Actualizar()
  End Sub

  ''' <summary>
  ''' obtiene dataset guardado en session
  ''' </summary>
  ''' <param name="nombreSesion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ObtenerDataSetSession(ByVal nombreSesion As String) As DataSet
    Try
      If (Session(nombreSesion) Is Nothing) Then
        Return Nothing
      Else
        Return CType(Session(nombreSesion), DataSet)
      End If
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' graba dataset en session, registrandolo con un nombre
  ''' </summary>
  ''' <param name="nombreSesion"></param>
  ''' <param name="ds"></param>
  ''' <remarks>VSR, 27/11/2008</remarks>
  Public Sub GrabarDataSetSession(ByVal nombreSesion As String, ByVal ds As DataSet)
    Try
      Session(nombreSesion) = ds
    Catch ex As Exception
      Session(nombreSesion) = Nothing
    End Try
  End Sub

#End Region

#Region "Email"

  Public Shared Function EmailError(ByVal objError As Exception, ByVal colEmails As System.Net.Mail.MailAddressCollection) As Boolean
    Try
      Dim correoErrores As String = MailEmisor()

      Dim strError As String = objError.ToString
      Dim strErrorPath As String = HttpContext.Current.Request.FilePath
      Dim mensaje As String = String.Empty
      Dim ipError As String = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
      Dim url As String = HttpContext.Current.Request.ServerVariables("SERVER_NAME") & HttpContext.Current.Request.ServerVariables("PATH_INFO")

      mensaje &= "Se ha reportado una excepci�n no controlada en la Aplicacion Web TRANSPORTE. " & vbCrLf & vbCrLf
      mensaje &= "Origen del error: " & strErrorPath & " el " & Herramientas.MyNow().ToString & vbCrLf & vbCrLf
      mensaje &= "Servidor: " & System.Environment.MachineName & vbCrLf & vbCrLf
      mensaje &= "URL: " & url & vbCrLf & vbCrLf
      mensaje &= "Ip donde se origin�: " & ipError & vbCrLf & vbCrLf
      mensaje &= "Mensaje: " & strError & vbCrLf & vbCrLf
      mensaje &= ObtenerVolcadorRequest()
      mensaje &= ObtenerVolcadoSesion()
      mensaje &= vbCrLf & "Este mensaje ha sido enviado en forma autom�tica desde TRANSPORTE. No responder." & vbCrLf & vbCrLf

      Dim correo As New System.Net.Mail.MailMessage()

      correo.From = New System.Net.Mail.MailAddress(correoErrores) ' debe ser el mismo que el user de la cuenta
      For Each Email As System.Net.Mail.MailAddress In colEmails

        correo.To.Add(Email)

      Next
      correo.Subject = "Reporte de error no controlado TRANSPORTE"
      correo.Body = mensaje
      correo.IsBodyHtml = False
      correo.BodyEncoding = System.Text.Encoding.UTF8
      correo.Priority = Net.Mail.MailPriority.High

      Dim smtp As New System.Net.Mail.SmtpClient

      smtp.Host = MailHost()
      smtp.Credentials = New System.Net.NetworkCredential(correoErrores, MailPasswordEmisor())

      smtp.Send(correo)

      Return True

    Catch

      Return False

    End Try

  End Function

  Public Shared Function EmailError(ByVal objError As Exception, ByVal Email As System.Net.Mail.MailAddress) As Boolean

    Try
      Dim correoErrores As String = MailEmisor()
      Dim strError As String = objError.ToString
      Dim strErrorPath As String = HttpContext.Current.Request.FilePath
      Dim mensaje As String = String.Empty
      Dim ipError As String = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
      Dim url As String = HttpContext.Current.Request.ServerVariables("SERVER_NAME") & HttpContext.Current.Request.ServerVariables("PATH_INFO")

      mensaje &= "Se ha reportado una excepci�n no controlada en la Aplicacion Web TRANSPORTE. " & vbCrLf & vbCrLf
      mensaje &= "Origen del error: " & strErrorPath & " el " & Herramientas.MyNow().ToString & vbCrLf & vbCrLf
      mensaje &= "Servidor: " & System.Environment.MachineName & vbCrLf & vbCrLf
      mensaje &= "URL: " & url & vbCrLf & vbCrLf
      mensaje &= "Ip donde se origin�: " & ipError & vbCrLf & vbCrLf
      mensaje &= "Mensaje: " & strError & vbCrLf & vbCrLf
      mensaje &= ObtenerVolcadorRequest()
      mensaje &= ObtenerVolcadoSesion()
      mensaje &= vbCrLf & "Este mensaje ha sido enviado en forma autom�tica desde TRANSPORTE. No responder." & vbCrLf & vbCrLf

      Dim correo As New System.Net.Mail.MailMessage()

      correo.From = New System.Net.Mail.MailAddress(correoErrores)
      correo.To.Add(Email)
      correo.Subject = "Reporte de error no controlado TRANSPORTE"
      correo.Body = mensaje
      correo.IsBodyHtml = False
      correo.BodyEncoding = System.Text.Encoding.UTF8
      correo.Priority = Net.Mail.MailPriority.High

      Dim smtp As New System.Net.Mail.SmtpClient

      smtp.Host = MailHost()
      smtp.Credentials = New System.Net.NetworkCredential(correoErrores, MailPasswordEmisor())

      smtp.Send(correo)

      Return True

    Catch ex As Exception

      Return False

    End Try

  End Function

  Public Shared Function ObtenerVolcadorRequest() As String
    Dim respuesta As String = vbCrLf & "Volcado Informacion Request" & vbCrLf & vbCrLf

    Try

      respuesta &= "[POST]" & vbCrLf & vbCrLf
      For Each item As String In HttpContext.Current.Request.Form.AllKeys
        respuesta &= item & vbTab & vbTab & vbTab & HttpContext.Current.Request.Form(item) & vbCrLf
      Next

      respuesta &= vbCrLf & "[QUERYSTRING]" & vbCrLf & vbCrLf
      For Each item As String In HttpContext.Current.Request.QueryString.AllKeys
        respuesta &= item & vbTab & vbTab & vbTab & HttpContext.Current.Request.QueryString(item) & vbCrLf
      Next

      respuesta &= vbCrLf & "[SERVER]" & vbCrLf & vbCrLf
      For Each item As String In HttpContext.Current.Request.ServerVariables.AllKeys
        respuesta &= item & vbTab & vbTab & vbTab & HttpContext.Current.Request.ServerVariables(item) & vbCrLf
      Next

      respuesta &= vbCrLf & "[COOKIES]" & vbCrLf & vbCrLf
      For Each item As String In HttpContext.Current.Request.Cookies.AllKeys
        respuesta &= item & vbTab & vbTab & vbTab & HttpContext.Current.Request.Cookies(item).ToString & vbCrLf
      Next

      respuesta &= vbCrLf & "[CLIENTCERTIFICATE]" & vbCrLf & vbCrLf
      For Each item As String In HttpContext.Current.Request.ClientCertificate.AllKeys
        respuesta &= item & vbTab & vbTab & vbTab & HttpContext.Current.Request.ClientCertificate(item).ToString & vbCrLf
      Next

      respuesta &= vbCrLf & "[TOTAL BYTES ENVIADOS]" & HttpContext.Current.Request.TotalBytes & vbCrLf & vbCrLf

    Catch ex As Exception

      respuesta &= vbCrLf & "Ocurri� un error al obtener los valores!!!"

    End Try

    Return respuesta

  End Function

#End Region

#Region "Logs"
  'Funcion    : RutaRaiz
  'Descripcion: Retorna la ruta raiz en la cual se guardaran todos los logs, archivos, etc..
  'Por        : VSR, 07/07/2008
  Public Shared Function RutaRaiz() As String
    Dim _rutaRaiz As String = Herramientas.RutaRaiz()
    Return _rutaRaiz
  End Function

  Public Shared Function ObtenerVolcadoSesion() As String
    Dim respuesta As String = vbCrLf & "Volcado Informacion Sesi�n" & vbCrLf & vbCrLf
    respuesta &= vbCrLf & "[SESION DE USUARIO]" & vbCrLf & vbCrLf

    Try
      For Each item As String In HttpContext.Current.Session.Keys
        Try
          If HttpContext.Current.Session.Item(item).ToString = "CapaNegocio.Usuario" Then
            Dim tmpUsuario As Usuario = HttpContext.Current.Session.Item(item)
            respuesta &= "Objeto Usuario" & vbCrLf
            respuesta &= "Id" & vbTab & vbTab & vbTab & tmpUsuario.Username & vbCrLf
            respuesta &= "Username" & vbTab & vbTab & vbTab & tmpUsuario.Username & vbCrLf
            respuesta &= "UltimoAccesoIp" & vbTab & vbTab & vbTab & tmpUsuario.IpUltimoAcceso & vbCrLf
            respuesta &= "UltimoAccesoHora" & vbTab & vbTab & vbTab & tmpUsuario.FechaHoraUltimoAcceso & vbCrLf
            respuesta &= "Activo" & vbTab & vbTab & vbTab & tmpUsuario.Estado.ToString & vbCrLf
            respuesta &= "Nombre" & vbTab & vbTab & vbTab & tmpUsuario.Nombre & " " & tmpUsuario.Paterno & " " & tmpUsuario.Materno & vbCrLf
          Else
            respuesta &= item & vbTab & vbTab & vbTab & HttpContext.Current.Session.Item(item).ToString & vbCrLf
          End If
        Catch
          respuesta &= item & vbTab & vbTab & vbTab & "(ERROR AL OBTENER!)"
        End Try
      Next

    Catch ex As Exception

      respuesta &= vbCrLf & "Ocurri� un error al obtener los valores!!!"

    End Try

    Return respuesta



  End Function

  'Funcion    : RutaLogsSitio
  'Descripcion: Retorna la ruta de la carpeta de los logs del sitio
  'Por        : VSR, 07/07/2008
  Public Shared Function RutaLogsSitio() As String
    Dim _rutaRaiz As String = Herramientas.RutaLogsSitio()
    Return _rutaRaiz
  End Function

#End Region

#Region "Funciones para Validacion"

  ' comprueba si una cadena es vacia
  Public Shared Function EsVacio(ByRef s As String) As Boolean

    Return String.IsNullOrEmpty(s)

  End Function

  ' comprueba el largo minimo y opcionalmente el maximo de un string
  Public Shared Function TieneLargoValido(ByRef s As String, ByVal minimo As Integer, Optional ByVal maximo As Integer = 0) As Boolean

    s.Trim()

    If minimo < 1 Then minimo = s.Length
    If maximo < 1 Then maximo = s.Length

    Return TieneRangoValido(s.Length, minimo, maximo)

  End Function

  ' comprueba si un integer esta entre un rango dado
  Public Shared Function TieneRangoValido(ByVal n As Integer, ByVal minimo As Integer, ByVal maximo As Integer) As Boolean

    ' para que sea true n tiene que ser mayor o igual que el minimo y menor o igual al maximo
    Return (n >= minimo AndAlso n <= maximo)

  End Function

  ' comprueba si un string tiene solo caracteres numericos
  Public Shared Function EsNumerico(ByRef s As String, _
                            Optional ByVal conDecimales As Boolean = False, _
                            Optional ByVal conNegativos As Boolean = False, _
                            Optional ByVal conExponenciales As Boolean = False) As Boolean

    Return Herramientas.EsNumerico(s, conDecimales, conNegativos, conExponenciales)
  End Function

  ' comprueba si s tiene formato email
  Public Shared Function EsEmail(ByVal s As String) As Boolean
    If String.IsNullOrEmpty(s) Or s Is Nothing Then
      Return False
      Exit Function
    End If

    Dim regExp As New System.Text.RegularExpressions.Regex( _
    "^(([^<;>;()[\]\\.,;:\s@\""]+(\.[^<;>;()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@" & _
    "((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$")

    Return regExp.IsMatch(s)

  End Function

  ' comprueba si s tiene formato de telefono
  Public Shared Function EsTelefono(ByVal s As String) As Boolean

    ' si es numerico
    If Not EsNumerico(s) Then Return False

    ' comprueba si tiene largos validos en chile entre 6 y 12 
    ' (ej 281754, 171412510000, 5625849817, 0994919986)
    If Not TieneLargoValido(s, 6, 12) Then Return False

    Return True

  End Function

  ' comprueba si s tiene formato de fecha DD/MM/AAAA
  Public Shared Function EsFechaDDMMAAAA(ByVal s As String) As Boolean

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 10, 10) Then Return False

    ' verificar si se puede convertir a una fecha valida
    Dim fecha As Date

    Try

      fecha = Convert.ToDateTime(s)
      Return True

    Catch ex As Exception

      Return False

    End Try

  End Function

  ' comprueba si s tiene formato de fecha gringa
  Public Shared Function EsFechaMMDDAAAA(ByVal s As String) As Boolean
    Dim fecha As Date
    Try
      fecha = Convert.ToDateTime(s)
      Return True
    Catch ex As Exception
      Return False
    End Try
  End Function

  ' comprueba si s tiene formato de fecha ISO (AAAAMMDD)
  Public Shared Function EsFechaISO(ByVal s As String) As Boolean

    ' si es numerico
    If Not EsNumerico(s) Then Return False

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 8, 8) Then Return False

    ' descomponer y probar si se puede convertir a una fecha
    Dim anio As String = s.Substring(0, 4)
    Dim mes As String = s.Substring(4, 2)
    Dim dia As String = s.Substring(6, 2)
    Dim fecha As Date

    Try

      fecha = Convert.ToDateTime(dia & "/" & mes & "/" & anio)
      Return True

    Catch ex As Exception

      Return False

    End Try


  End Function

  ' comprueba si s tiene formato de hora HH:MM
  Public Shared Function EsHora(ByVal s As String) As Boolean

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 5, 5) Then Return False

    ' descomponer
    Dim hora As String = s.Substring(0, 2)
    Dim minuto As String = s.Substring(3, 2)

    ' son numericos?
    If Not EsNumerico(hora) Then Return False
    If Not EsNumerico(minuto) Then Return False

    ' estan dentro del rango?
    If Not TieneRangoValido(CType(hora, Integer), 0, 23) Then Return False
    If Not TieneRangoValido(CType(minuto, Integer), 0, 59) Then Return False

    Return True

  End Function

  ' comprueba si s tiene formato de hora ISO HHMM
  Public Shared Function EsHoraISO(ByVal s As String) As Boolean

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not TieneLargoValido(s, 4, 4) Then Return False

    ' descomponer
    Dim hora As String = s.Substring(0, 2)
    Dim minuto As String = s.Substring(2, 2)

    ' son numericos?
    If Not EsNumerico(hora) Then Return False
    If Not EsNumerico(minuto) Then Return False

    ' estan dentro del rango?
    If Not TieneRangoValido(CType(hora, Integer), 0, 23) Then Return False
    If Not TieneRangoValido(CType(minuto, Integer), 0, 59) Then Return False

    Return True

  End Function

  ' comprueba si un string tiene caracteres validos
  Public Shared Function TieneCaracteresValidos(ByRef s As String) As Boolean
    s.Trim()
    Dim caracteresNoValidos As String = "(<|>)"
    Dim regExp As New System.Text.RegularExpressions.Regex(caracteresNoValidos)
    Dim contieneCaracteresNoValidos As Boolean = regExp.IsMatch(s)
    Return Not contieneCaracteresNoValidos
  End Function

  ' comprueba si dos passwords son validas y la segunda confirma a la primera
  Public Shared Function PasswordsValidos(ByVal password1 As String, ByVal password2 As String) As Boolean

    ' verificamos largos
    If Not TieneLargoValido(password1, 3, 20) OrElse Not TieneLargoValido(password2, 3, 20) Then
      Return False
    End If

    ' comprobamos qu sean iguales, si es cero son iguales
    Return (String.Compare(password1, password2, False) = 0)

  End Function

  ''' <summary>
  ''' determina si el RUT ingresado es v�lido
  ''' </summary>
  ''' <param name="rut"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 08/07/2009</remarks>
  Public Shared Function EsRut(ByVal rut As String, Optional ByVal largoMinimo As Integer = 2, Optional ByVal largoMaximo As Integer = 9) As Boolean
    Dim esValido As Boolean = False
    Dim dv, parteNumerica As String

    Try
      If Not String.IsNullOrEmpty(rut) Then
        'le quita al rut los espacios en blanco, puntos y guion
        rut = rut.Replace(" ", "").Replace(".", "").Replace("-", "")
        'valida que el largo del rut sea de X digitos, incluyendo el digito Verificador
        If (rut.Length < largoMinimo) Or (rut.Length > largoMaximo) Then
          esValido = False
        Else
          'separo el ultimo digito del resto asumiendo que es el Digito Verificador
          'Si el �ltimo digito no es un numero, imagino que es una k, la transformo en mayuscula
          dv = Right(rut, 1).ToUpper
          'obtiene la parte numerica del rut
          parteNumerica = rut.Substring(0, rut.Length - 1)
          'verifica si el DV es el que corresponde
          esValido = IIf(CalcularDigitoVerificador(CLng(parteNumerica)) = dv, True, False)
        End If
      End If
    Catch ex As Exception
      esValido = False
    End Try
    Return esValido
  End Function

  ''' <summary>
  ''' calcula el digito verificador
  ''' </summary>
  ''' <param name="rut"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CalcularDigitoVerificador(ByVal rut As Long) As String
    Dim digito, contador, multiplo, acumulador As Integer
    Dim digitoVerificador As String

    contador = 2
    acumulador = 0
    While rut <> 0
      multiplo = (rut Mod 10) * contador
      acumulador = acumulador + multiplo
      rut = rut \ 10
      contador = contador + 1
      If contador = 8 Then
        contador = 2
      End If
    End While
    digito = 11 - (acumulador Mod 11)
    digitoVerificador = CStr(digito)
    If digito = 10 Then digitoVerificador = "K"
    If digito = 11 Then digitoVerificador = "0"

    'retorna el digito verificador
    Return digitoVerificador
  End Function


  ' revisa que una lista de Ids tenga formato correcto y le saca las comas de los extremos
  Public Shared Function ListaIdOK(ByRef lista As String) As Boolean

    Dim caracteresValidos As String = "1234567890,"

    If lista Is Nothing Then lista = String.Empty

    ' la lista solo debe tener los caracteres permitidos, sino devuelve falso
    For i As Integer = 0 To lista.Length - 1

      If caracteresValidos.IndexOf(lista.Chars(i)) = -1 Then Return False

    Next

    ' si hay una lista limpiamos las comas que puedan existir en los extremos
    If lista.StartsWith(",") Then lista = lista.Remove(0, 1)
    If lista.EndsWith(",") Then lista = lista.Remove(lista.Length - 1, 1)

    ' llegamos aca con la lista ok
    Return True

  End Function

  ' obtiene el nombre del mes a partir de su indice
  Public Shared Function ObtenerNombreMes(ByVal indiceMes As String) As String
    Dim nombreMes As String = ""
    indiceMes = Right("0" & indiceMes, 2)
    Select Case indiceMes
      Case "01"
        nombreMes = "Enero"
      Case "02"
        nombreMes = "Febrero"
      Case "03"
        nombreMes = "Marzo"
      Case "04"
        nombreMes = "Abril"
      Case "05"
        nombreMes = "Mayo"
      Case "06"
        nombreMes = "Junio"
      Case "07"
        nombreMes = "Julio"
      Case "08"
        nombreMes = "Agosto"
      Case "09"
        nombreMes = "Septiembre"
      Case "10"
        nombreMes = "Octubre"
      Case "11"
        nombreMes = "Noviembre"
      Case "12"
        nombreMes = "Diciembre"
    End Select
    'retorna valor
    Return nombreMes
  End Function

  ''' <summary>
  ''' obtiene expresion regular para validar caracteres repetidos
  ''' </summary>
  ''' <param name="maximoRepeticiones"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerExpresionRegularParaRepeticion(Optional ByVal maximoRepeticiones As Integer = 3, Optional ByVal textoRepeticion As String = "") As String
    Dim regularExpression As String = ""
    Dim currChar As Char
    Dim i As Integer

    If Not String.IsNullOrEmpty(textoRepeticion) Then
      regularExpression &= textoRepeticion & "{" & maximoRepeticiones & ",}|"
    Else
      For i = 32 To 126
        currChar = Convert.ToChar(i)
        If currChar = "*" Or currChar = "$" Or currChar = "[" Or currChar = "]" Or currChar = "{" Or currChar = "}" Or currChar = "|" Or currChar = "?" Or currChar = "(" Or currChar = ")" Or currChar = "+" Or currChar = "." Or currChar = "^" Then
          regularExpression &= "\" & currChar & "{" & maximoRepeticiones & ",}|"
        Else
          regularExpression &= currChar & "{" & maximoRepeticiones & ",}|"
        End If
      Next
    End If

    If (regularExpression.EndsWith("|")) Then
      regularExpression = regularExpression.Substring(0, regularExpression.Length - 1)
    End If
    Return regularExpression
  End Function

  ''' <summary>
  ''' determina si tiene caracteres repetidos dentro de un texto
  ''' </summary>
  ''' <param name="s"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function TieneCaracteresRepetidos(ByVal s As String, Optional ByVal maximoRepeticiones As Integer = 3, Optional ByVal textoRepeticion As String = "") As Boolean
    Try
      s.Trim()
      Dim regExp As New System.Text.RegularExpressions.Regex(ObtenerExpresionRegularParaRepeticion(maximoRepeticiones, textoRepeticion))
      Return regExp.IsMatch(s)
    Catch ex As Exception
      Return True
    End Try
  End Function

#End Region

#Region "Funciones para Conversion"

  Public Shared Function StripHtml(ByVal html As String) As String
    Dim texto As String
    texto = System.Text.RegularExpressions.Regex.Replace(html, "<[^>]*>", String.Empty)
    texto = System.Text.RegularExpressions.Regex.Replace(texto, "&nbsp;", " ")

    Return texto
  End Function

  ' convierte una fecha de ISO a DD/MM/AAAA
  Public Shared Function ConvFechaISOaDDMMAAAA(ByVal s As String) As String

    If Not EsFechaISO(s) Then Return ""

    ' descomponer y probar si se puede convertir a una fecha
    Dim anio As String = s.Substring(0, 4)
    Dim mes As String = s.Substring(4, 2)
    Dim dia As String = s.Substring(6, 2)
    Dim fecha As String

    Try

      fecha = dia & "/" & mes & "/" & anio
      Return fecha

    Catch ex As Exception

      Return ""

    End Try

  End Function

  ' convierte una fecha de DD/MM/AAAA as ISO
  Public Shared Function ConvFechaDDMMAAAAaISO(ByVal s As String) As String
    Dim fecha As String = ""

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    'If Not EsFechaDDMMAAAA(s) Then Return Nothing
    If Not s.Length >= 10 Then Return Nothing

    ' verificar si se puede convertir a una fecha valida
    Dim dia As String = s.Substring(0, 2)
    Dim mes As String = s.Substring(3, 2)
    Dim ano As String = s.Substring(6, 4)

    Try

      fecha = ano & mes & dia
      Return fecha

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  ' convierte una hora de ISO a HH:MM
  Public Shared Function ConvHoraISOaHHMM(ByVal s As String) As String

    If Not EsHoraISO(s) Then Return Nothing

    ' descomponer y probar si se puede convertir a una fecha
    Dim hora As String = s.Substring(0, 2)
    Dim minutos As String = s.Substring(2, 2)

    Try

      hora = hora & ":" & minutos
      Return hora

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  ' convierte una fecha de DD/MM/AAAA as ISO
  Public Shared Function ConvFechaHHMMaISO(ByVal s As String) As String

    ' verirficar largo (tiene extricto 10 incluyendo los separadores)
    If Not EsHora(s) Then Return Nothing

    ' descomponer y probar si se puede convertir a una fecha
    Dim hora As String = s.Substring(0, 2)
    Dim minutos As String = s.Substring(3, 2)

    Try

      hora = hora & minutos
      Return hora

    Catch ex As Exception

      Return Nothing

    End Try

  End Function

  'Funcion    : ConvertirUnidadMedidaArchivo
  'Descripcion: convierte una unidad de medida en otra
  'Por        : VSR, 15/07/2008
  Public Shared Function ConvertirUnidadMedidaArchivo(ByVal tamano As Double, ByVal aproximarMedidaMasCercana As Boolean, Optional ByVal cantidadDecimales As Integer = 2, Optional ByVal unidadMedidaOrigen As eUnidadMedidaArchivo = eUnidadMedidaArchivo.Bytes, Optional ByVal unidadMedidaDestino As eUnidadMedidaArchivo = eUnidadMedidaArchivo.KiloBytes) As String
    Dim UNIDAD_BASE As Integer = 1024
    Dim BYTES As Double = 1
    Dim KB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 1)
    Dim MB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 2)
    Dim GB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 3)
    Dim TB_EN_BYTES As Double = Math.Pow(UNIDAD_BASE, 4)
    Dim dividendo As Double = 0
    Dim divisor As Double = 1
    Dim valor As Double = 0
    Dim retorno As String = ""
    Dim unidadMedida As String = ""

    'transforma el valor original en bytes
    Select Case unidadMedidaOrigen
      Case eUnidadMedidaArchivo.Bytes
        dividendo = tamano * BYTES
      Case eUnidadMedidaArchivo.KiloBytes
        dividendo = tamano * KB_EN_BYTES
      Case eUnidadMedidaArchivo.MegaBytes
        dividendo = tamano * MB_EN_BYTES
      Case eUnidadMedidaArchivo.GigaBytes
        dividendo = tamano * GB_EN_BYTES
      Case eUnidadMedidaArchivo.TeraBytes
        dividendo = tamano * TB_EN_BYTES
    End Select

    'determina en que unidad se va a convertir
    Select Case unidadMedidaDestino
      Case eUnidadMedidaArchivo.Bytes
        divisor = BYTES
        unidadMedida = "bytes"
      Case eUnidadMedidaArchivo.KiloBytes
        divisor = KB_EN_BYTES
        unidadMedida = "Kb"
      Case eUnidadMedidaArchivo.MegaBytes
        divisor = MB_EN_BYTES
        unidadMedida = "Mb"
      Case eUnidadMedidaArchivo.GigaBytes
        divisor = GB_EN_BYTES
        unidadMedida = "Gb"
      Case eUnidadMedidaArchivo.TeraBytes
        divisor = TB_EN_BYTES
        unidadMedida = "Tb"
    End Select

    'calcula el valor
    valor = Math.Round(dividendo / divisor, cantidadDecimales)
    retorno = valor.ToString & " " & unidadMedida

    'si se aproxima a la unidad mas cercana entonces se calcula de nuevo
    If aproximarMedidaMasCercana Then
      If dividendo < KB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.Bytes)
      ElseIf dividendo >= KB_EN_BYTES And dividendo < MB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.KiloBytes)
      ElseIf dividendo >= MB_EN_BYTES And dividendo < GB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.MegaBytes)
      ElseIf dividendo >= GB_EN_BYTES And dividendo < TB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.GigaBytes)
      ElseIf dividendo >= TB_EN_BYTES Then
        retorno = ConvertirUnidadMedidaArchivo(dividendo, False, cantidadDecimales, eUnidadMedidaArchivo.Bytes, eUnidadMedidaArchivo.TeraBytes)
      End If
    End If

    'retorna valor
    Return retorno
  End Function

#End Region

#Region "Funciones que entregan datos"

  ' dada una variable datetime entrega la fecha en formato ISO, por default entregara la fecha actual
  Public Shared Function FechaISO(Optional ByVal ahora As DateTime = Nothing) As String

    If ahora = Nothing Then ahora = Herramientas.MyNow()

    Dim anio As String = "0000" & ahora.Year.ToString
    Dim mes As String = "00" & ahora.Month.ToString
    Dim dia As String = "00" & ahora.Day.ToString

    Return Right(anio, 4) & Right(mes, 2) & Right(dia, 2)

  End Function

  ' dada una variable datetime entrega la hora en formato ISO, por default entregara la hora actual
  Public Shared Function HoraISO(Optional ByVal ahora As DateTime = Nothing) As String

    If ahora = Nothing Then ahora = Herramientas.MyNow()

    Dim hora As String = "00" & ahora.Hour.ToString
    Dim minutos As String = "00" & ahora.Minute.ToString

    Return Right(hora, 2) & Right(minutos, 2)

  End Function

  ' devuelve un string con la fecha y la hora en formato iso yyyymmddhhnn
  Public Shared Function FechaHoraISO(Optional ByVal ahora As DateTime = Nothing) As String

    If ahora = Nothing Then ahora = Herramientas.MyNow()

    Dim anio As String = "0000" & ahora.Year.ToString
    Dim mes As String = "00" & ahora.Month.ToString
    Dim dia As String = "00" & ahora.Day.ToString
    Dim hora As String = "00" & ahora.Hour.ToString
    Dim minutos As String = "00" & ahora.Minute.ToString

    Return Right(anio, 4) & Right(mes, 2) & Right(dia, 2) & Right(hora, 2) & Right(minutos, 2)

  End Function

  ' a partir de una cadena con fecha y hora iso, devuelve la fecha "como se lee"
  Public Shared Function DesplegarFecha(ByVal fechaFormatoISO As String) As String

    If Not TieneLargoValido(fechaFormatoISO, 12, 12) Or Not EsNumerico(fechaFormatoISO) Then Return "No disponible"

    Dim anio As String = fechaFormatoISO.Substring(0, 4)
    Dim mes As String = fechaFormatoISO.Substring(4, 2)
    Dim dia As String = fechaFormatoISO.Substring(6, 2)
    Dim hora As String = fechaFormatoISO.Substring(8, 2)
    Dim minutos As String = fechaFormatoISO.Substring(10, 2)

    Return dia & "/" & mes & "/" & anio & " a las " & hora & ":" & minutos & " Hrs."

  End Function

  ' a partir del numero del mes obtiene su nombre
  Public Shared Function ObtenerNombreMes(ByVal mes As Integer) As String

    Dim nombreMeses As New SortedList

    nombreMeses.Add(1, "Enero")
    nombreMeses.Add(2, "Febrero")
    nombreMeses.Add(3, "Marzo")
    nombreMeses.Add(4, "Abril")
    nombreMeses.Add(5, "Mayo")
    nombreMeses.Add(6, "Junio")
    nombreMeses.Add(7, "Julio")
    nombreMeses.Add(8, "Agosto")
    nombreMeses.Add(9, "Septiembre")
    nombreMeses.Add(10, "Octubre")
    nombreMeses.Add(11, "Noviembre")
    nombreMeses.Add(12, "Diciembre")

    If mes >= 1 AndAlso mes <= 12 Then
      Return nombreMeses.Item(mes)
    Else
      Return String.Empty
    End If

  End Function

  'obtiene hora de una hora ISO
  Public Shared Function ObtenerHoraDeHoraISO(ByVal horaISO As String) As String
    Dim hora As String

    If Not EsHoraISO(horaISO) Then Return String.Empty

    Try
      hora = horaISO.Substring(0, 2)
      Return hora
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  'obtiene minutos de una hora ISO
  Public Shared Function ObtenerMinutosDeHoraISO(ByVal horaISO As String) As String
    Dim minutos As String

    If Not EsHoraISO(horaISO) Then Return String.Empty

    Try
      minutos = horaISO.Substring(2, 2)
      Return minutos
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ' a partir de un arraylist devuelve una lista <li> de los elementos
  Public Shared Function ObtenerListaErrores(ByVal errores As ArrayList) As String

    Dim resultado As String

    ' construir cadena para desplegar los errores
    resultado = "<ul><li>Los datos ingresados presentan errores:</li>"
    For Each _error As String In errores
      resultado &= "<li>- " & _error & "</li>"
    Next
    resultado &= "</ul>"

    Return resultado

  End Function

  ' escapar comillas enrutinas JS
  Public Shared Function EscaparComillasJS(ByVal texto As String) As String
    'cambio los \n por retorno de carro (esto es necesario, pues a continuacion voy a sacar los "\" para que no puedan da�ar)
    texto = Replace(texto, "\n", vbCrLf)
    'escapa el caracter de escape
    texto = Replace(texto, "\", "\\")
    'escapa comillas simples
    texto = Replace(texto, "'", "\'")
    'cambia retorno de carro por \n
    texto = Replace(texto, vbCrLf, "\n")
    'cambia retorno de carro simple por \n
    texto = Replace(texto, vbCr, "\n")
    'cambia linefeed simple por \n
    texto = Replace(texto, vbLf, "\n")
    'escapa comillas dobles
    Return Trim(Replace(texto, """", "\"""))
  End Function

  ''' <summary>
  ''' limpia el string quitandole los espacios en blanco, saltos de linea, tabulacion, etc
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function LimpiarStringConSeparadores(ByVal texto As String, Optional ByVal separador As String = ";") As String
    'reemplaza las comas por el separador
    texto = Replace(texto, ",", separador)
    'quita los espacios en blanco
    texto = Replace(texto, " ", "")
    'quita retorno de carro
    texto = Replace(texto, vbCrLf, "")
    'quita retorno de carro simple
    texto = Replace(texto, vbCr, "")
    'quita linefeed simple
    texto = Replace(texto, vbLf, "")
    'quita tabulacion
    texto = Replace(texto, vbTab, "")
    'quita backspace
    texto = Replace(texto, vbBack, "")
    If texto Is Nothing Then
      texto = ""
    End If
    Return texto.Trim
  End Function

  ''' <summary>
  ''' elimina ultimo caracter del texto
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EliminaUltimoCaracterTexto(ByVal texto As String, Optional ByVal caracteresAQuitar As Integer = 1) As String
    Dim largoTexto, strTextoSinComaFinal As String

    'Calcula largo de texto
    largoTexto = Len(texto)

    If largoTexto = 0 Then
      strTextoSinComaFinal = ""
    Else
      'Quita la coma final de la cadena
      strTextoSinComaFinal = Mid(texto, 1, largoTexto - caracteresAQuitar)
    End If

    'Asigna valor de cadena
    Return strTextoSinComaFinal

  End Function

  ' devuelve el caracter que va entre dos items de una lista cuando se recorre (vacio, "," y " y ")
  Public Shared Function Pegamento(ByVal indice As Integer, ByVal tope As Integer) As String

    If indice = tope Then
      Return String.Empty
    End If

    If indice < tope And indice < tope - 1 Then
      Return ", "
    End If

    If indice < tope And indice = tope - 1 Then
      Return " y "
    End If

    Return String.Empty

  End Function

  ' reemplaza los caracteres de acentos y � por su entidad html
  Public Shared Function ReemplazarAcentos(ByVal s As String) As String

    s = Replace(s, "�", "&aacute;")
    s = Replace(s, "�", "&eacute;")
    s = Replace(s, "�", "&iacute;")
    s = Replace(s, "�", "&oacute;")
    s = Replace(s, "�", "&uacute;")

    s = Replace(s, "�", "&Aacute;")
    s = Replace(s, "�", "&Eacute;")
    s = Replace(s, "�", "&Iacute;")
    s = Replace(s, "�", "&Oacute;")
    s = Replace(s, "�", "&Uacute;")

    s = Replace(s, "�", "&ntilde;")
    s = Replace(s, "�", "&Ntilde;")

    Return s

  End Function

  ' wrapper para sanitizar las entradas de usuario
  Public Shared Function LimpiarInput(ByVal s As String) As String
    Dim aCaracteresInvalidos As String() = CARACTERES_NO_VALIDOS_REGEXP.Split("|")
    Dim caracterInvalido As String
    Dim i As Integer

    s = s.Trim()
    For i = 0 To aCaracteresInvalidos.Length - 1
      caracterInvalido = aCaracteresInvalidos(i)
      s = s.Replace(caracterInvalido, "")
    Next

    Return s
  End Function

  'Metodo     : VisibleColumnaGrillaPorIndiceColumna()
  'Descripcion: muestra u oculta una columna de la grilla segun el indice de la columna
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 30/05/2008
  Public Shared Sub VisibleColumnaGrillaPorIndiceColumna(ByVal gridView As GridView, ByVal indiceColumna As Integer, ByVal esVisible As Boolean)
    gridView.Columns(indiceColumna).Visible = esVisible
  End Sub

  Public Function ajustarString(ByVal s As String, _
                                ByVal largo As Integer, _
                                Optional ByVal caracter As String = " ") As String

    ' crea un string de *caracter de *largo especificado
    Dim output As String = String.Empty
    For i As Integer = 1 To largo
      output &= caracter
    Next

    ' le pegamos el relleno a la cadena
    output = s & output
    Return output.Substring(1, largo)

  End Function

  'Metodo     : ObtenerListadoMailDesdeDataSet()
  'Descripcion: construye listado de email separados por ";" a partir de una tabla de un dataset
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 11/07/2008
  Public Shared Function ObtenerListadoMailDesdeDataSet(ByVal ds As DataSet, ByVal indiceTabla As Integer, ByVal nombreColumnaMail As String, Optional ByVal separador As String = ";") As String
    If ds Is Nothing Then
      Return ""
      Exit Function
    End If

    Dim sb As New StringBuilder
    Dim i As Integer
    Dim dr As DataRow
    Dim email As String

    Try
      Dim totalRegistros As Integer = ds.Tables(indiceTabla).Rows.Count
      If totalRegistros > 0 Then
        For i = 0 To totalRegistros - 1
          dr = ds.Tables(indiceTabla).Rows(i)
          email = dr.Item(nombreColumnaMail)
          If Utilidades.EsEmail(email) Then
            sb.Append(email)
            If i < totalRegistros - 1 Then sb.Append(separador)
          End If
        Next
      Else
        sb.Append("")
      End If
      'retorna valor
      Return sb.ToString
    Catch ex As Exception
      Return ""
    End Try
  End Function

  'Metodo     : ObtenerTablaHTMLDesdeDataSet()
  'Descripcion: construye el cuerpo del mensaje
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 26/06/2008
  Public Shared Function ObtenerTablaHTMLDesdeDataSet(ByVal ds As DataSet, Optional ByVal indiceTabla As Integer = 0) As String
    Dim oUtilidades As New Utilidades
    Dim bodyMail As String = ""
    Dim i, j As Integer
    Dim sb As New StringBuilder
    Dim totalRegistros As Integer = ds.Tables(indiceTabla).Rows.Count
    Dim dt As DataTable = ds.Tables(indiceTabla)
    Dim dr As DataRow = Nothing
    Dim totalColumnas As Integer
    Dim nombreColumna, item, valorItem As String

    Try
      If totalRegistros > 0 Then
        'obtiene total de columnas
        totalColumnas = dt.Columns.Count

        sb.Append("<table border=""1"" cellpadding=""0"" cellspacing=""0"">")
        sb.Append(" <tr>")
        'construye cabeceras de la tabla
        For i = 0 To totalColumnas - 1
          If Not dt.Columns.Item(i).ColumnName.StartsWith("_") Then
            nombreColumna = dt.Columns.Item(i).ColumnName
            sb.Append("<th>" & nombreColumna & "</th>")
          End If
        Next
        sb.Append(" </tr>")
        'recorre el dataset como si fuera una matriz [i=>fila | j=>columna]
        For i = 0 To totalRegistros - 1
          sb.Append(" <tr>")
          dr = dt.Rows(i)
          'recorre las columnas para obtener sus valores
          For j = 0 To totalColumnas - 1
            If Not dt.Columns.Item(j).ColumnName.StartsWith("_") Then
              item = dt.Columns.Item(j).ColumnName
              valorItem = Utilidades.IsNull(dr.Item(item), "")
              sb.Append("<td>" & valorItem & "</td>")
            End If
          Next
          sb.Append(" </tr>")
        Next
        sb.Append("</table>")
        bodyMail = sb.ToString
      End If
      'retorna valor
      Return bodyMail
    Catch ex As Exception
      'retorna valor
      Return bodyMail
    End Try
  End Function

  'Metodo     : ObtenerContenidoExcelConTablaHTML()
  'Descripcion: construye html para guardarlo como excel
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 11/07/2008
  Public Shared Function ObtenerContenidoExcelConTablaHTML(ByVal bodyHTML As String) As String
    Dim sb As New StringBuilder
    sb.Append("<html>")
    sb.Append("<head>")
    sb.Append("</head>")
    sb.Append("<body>")
    sb.AppendLine(bodyHTML)
    sb.Append("</body>")
    sb.Append("</html>")
    Return sb.ToString
  End Function

  'Funcion    : ObtenerIconoTipoArchivo
  'Descripcion: obtiene un icono asociado al tipo de archivo
  'Por        : VSR, 15/07/2008
  Public Shared Function ObtenerIconoTipoArchivo(ByVal extension As String, Optional ByVal contentType As String = "") As String
    Dim iconoAplicacion As String = "i-aplicacion.png"
    Dim iconoAudio As String = "i-audio.png"
    Dim iconoBlank As String = "i-blank.png"
    Dim iconoExcel As String = "i-excel.png"
    Dim iconoImage As String = "i-image.png"
    Dim iconoOffice As String = "i-office.png"
    Dim iconoOtro As String = "i-otro.png"
    Dim iconoPDF As String = "i-pdf.png"
    Dim iconoPPT As String = "i-ppt.png"
    Dim iconoText As String = "i-txt.png"
    Dim iconoVideo As String = "i-video.png"
    Dim iconoWord As String = "i-word.png"
    Dim iconoZip As String = "i-zip.png"
    Dim iconoAsociado As String = ""
    Dim buscarUsandoContentType As Boolean = False

    'primero busca en las extensiones mas usadas
    Select Case extension.ToLower
      Case "pdf" : iconoAsociado = iconoPDF
      Case "doc", "docx" : iconoAsociado = iconoWord
      Case "xls", "xlsx" : iconoAsociado = iconoExcel
      Case "pps", "ppt", "pptx" : iconoAsociado = iconoPPT
      Case "txt" : iconoAsociado = iconoText
      Case "zip", "rar" : iconoAsociado = iconoZip
      Case "mp3", "wav", "wma" : iconoAsociado = iconoAudio
      Case Else : buscarUsandoContentType = True
    End Select

    'si no encuentra el icono en las extension conocidas busca en los iconos
    'asociados al contentType
    If buscarUsandoContentType Then
      Select Case Archivo.EsTipoArchivo(contentType)
        Case Archivo.eTipoArchivo.application : iconoAsociado = iconoAplicacion
        Case Archivo.eTipoArchivo.audio : iconoAsociado = iconoAudio
        Case Archivo.eTipoArchivo.image : iconoAsociado = iconoImage
        Case Archivo.eTipoArchivo.office : iconoAsociado = iconoOffice
        Case Archivo.eTipoArchivo.others : iconoAsociado = iconoOtro
        Case Archivo.eTipoArchivo.text : iconoAsociado = iconoText
        Case Archivo.eTipoArchivo.unknow : iconoAsociado = iconoBlank
        Case Archivo.eTipoArchivo.video : iconoAsociado = iconoVideo
        Case Archivo.eTipoArchivo.zip : iconoAsociado = iconoZip
        Case Else : iconoAsociado = iconoBlank
      End Select
    End If

    'retorna valor
    Return iconoAsociado
  End Function

  'Funcion    : ObtenerRutaIconoTipoArchivo
  'Descripcion: obtiene la ruta de la imagen
  'Por        : VSR, 15/07/2008
  Public Shared Function ObtenerRutaIconoTipoArchivo(ByVal extension As String, Optional ByVal contentType As String = "") As String
    Dim rutaCarpetaIcono As String = "../img/"
    Dim iconoAsociado As String = ObtenerIconoTipoArchivo(extension, contentType)
    Dim sourceIcono As String = ""

    'construye ruta
    sourceIcono = rutaCarpetaIcono & iconoAsociado
    'retorna valor
    Return sourceIcono
  End Function

  'Metodo     : CrearDataSetDesdeDataViewConRowFilter()
  'Descripcion: crea un dataset a partir de un dataview filtrado
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 27/08/2008
  Public Shared Function CrearDataSetDesdeDataViewConRowFilter(ByVal dv As DataView) As DataSet
    Dim ds As New DataSet
    Dim dt As DataTable = dv.Table.Clone()

    'llena la tabla con las filas del dataview
    For Each drv As DataRowView In dv
      dt.ImportRow(drv.Row)
    Next

    'agregar la nueva tabla al dataset
    ds.Tables.Add(dt)

    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' convierte una fecha "dd/mm/aaaa" en "dd de mm del aaaa"
  ''' </summary>
  ''' <param name="fechaDDMMAAAA"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertirFechaDDMMAAAAEnTexto(ByVal fechaDDMMAAAA As String) As String
    Dim texto As String = ""
    Dim aFecha As String()
    Dim dia, mes, ano As String
    Try
      If Not EsVacio(fechaDDMMAAAA) Then
        If EsFechaDDMMAAAA(fechaDDMMAAAA) Then
          fechaDDMMAAAA = fechaDDMMAAAA.Replace("-", "/")
          aFecha = fechaDDMMAAAA.Split("/")
          dia = aFecha(0)
          mes = aFecha(1)
          ano = aFecha(2)
          mes = ObtenerNombreMes(mes)
          texto = dia & " de " & mes & " del " & ano
        End If
      End If
    Catch ex As Exception
      texto = ""
    End Try
    Return texto
  End Function

  ''' <summary>
  ''' retorna la estructura de la tabla en un dataset vacio
  ''' </summary>
  ''' <param name="nombreTabla"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerEstructuraDataSet(ByVal nombreTabla As String) As DataSet
    Dim ds As New DataSet
    Dim dr As DataRow
    Dim newDataset As New DataSet
    Dim totalRegistros, i As Integer

    Try
      'obtiene la estructura
      ds = Sistema.ObtenerEstructuraTablaComoDataSet(nombreTabla)
      totalRegistros = ds.Tables(0).Rows.Count
      'si tiene un valor lo limpia
      If (totalRegistros > 0) Then
        'elimina todas las filas
        For i = 0 To totalRegistros - 1
          dr = ds.Tables(0).Rows(0)
          ds.Tables(0).Rows.Remove(dr)
        Next
        'asigna dataset vacio
        newDataset = ds.Copy
      Else
        newDataset = ds
      End If
    Catch ex As Exception
      newDataset = Nothing
    End Try
    'retorna dataset
    Return newDataset
  End Function

  ''' <summary>
  ''' obtiene el titulo del campo desde un xml asociado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ObtenerTituloDesdeXML(ByVal nombreArchivoXML As String, ByVal nombreColumnaRowFilter As String, ByVal valorRowFilter As String, ByVal nombreColumnaBusqueda As String) As String
    Dim dv As New DataView
    Dim totalRegistros As Integer
    Dim ds As New DataSet
    Dim titulo As String = ""

    Try
      'obtiene archivo xml en el cual se encuentran las definiciones de las columnas
      ds.ReadXml(Server.MapPath("../xml/" & nombreArchivoXML))
      dv = ds.Tables(0).DefaultView
      dv.RowFilter = nombreColumnaRowFilter & "='" & valorRowFilter & "'"
      totalRegistros = dv.Count

      If totalRegistros > 0 Then
        titulo = dv.Item(0).Item(nombreColumnaBusqueda)
      Else
        titulo = nombreColumnaBusqueda
      End If
    Catch ex As Exception
      titulo = nombreColumnaBusqueda
    End Try

    'retorna valor
    Return titulo
  End Function

  ''' <summary>
  ''' obtiene el valor de una columna filtrando un dataset
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="nombreColumnaRowFilter"></param>
  ''' <param name="valorRowFilter"></param>
  ''' <param name="nombreColumnaBusqueda"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerValorDesdeDatasetFiltrado(ByVal ds As DataSet, ByVal nombreColumnaRowFilter As String, ByVal valorRowFilter As String, ByVal nombreColumnaBusqueda As String, Optional ByVal indiceTablaBusqueda As Integer = 0) As String
    Dim oUtilidades As New Utilidades
    Dim valor As String = ""
    Dim dt As DataTable
    Dim dv As DataView
    Dim totalRegistros, totalRegistrosEncontrados As Integer
    Try
      If Not ds Is Nothing Then
        dt = ds.Tables(indiceTablaBusqueda)
        totalRegistros = dt.Rows.Count
        If totalRegistros > 0 Then
          dv = dt.DefaultView
          dv.RowFilter = nombreColumnaRowFilter & "='" & valorRowFilter & "'"
          totalRegistrosEncontrados = dv.Count
          If totalRegistrosEncontrados > 0 Then
            valor = Utilidades.IsNull(dv.Item(0).Item(nombreColumnaBusqueda), "")
          End If
        End If
      End If
    Catch ex As Exception
      valor = ""
    End Try

    Return valor
  End Function


  ''' <summary>
  ''' desde un texto entregado por parametro , se entrega un arreglo de texto
  ''' </summary>
  ''' <param name="texto">texto ingresado para ser almcenado en un arreglo</param>
  ''' <param name="Largo">cantidad de cifras que soportara cada elemento de un arreglo</param>
  ''' <returns></returns>
  ''' <remarks></remarks>

  Public Shared Function TextoEnTrosos(ByVal texto As String, ByVal Largo As Integer) As ArrayList
    Dim i As Integer = 0
    Dim arrTexto As New ArrayList
    Dim maxLengthOfALine As Integer = Largo
    Dim startingPosition As Integer = 0
    Dim endingPosition As Integer = startingPosition + Largo
    Dim lineLength As Integer = Largo
    Dim line As String

    While lineLength + startingPosition <= endingPosition

      If lineLength + startingPosition > texto.Length Then
        line = texto.Substring(startingPosition, texto.Length - startingPosition)
        arrTexto.Add(line)
        Exit While
      End If

      While texto.Substring(startingPosition + lineLength - 1, 1) <> Chr(32)

        lineLength -= 1

      End While

      ' Get the line.
      line = texto.Substring(startingPosition, lineLength)

      ' Write the line.
      arrTexto.Add(line)

      ' If we had to backtrack, we can't add 1 to the count.  This

      ' prevents us from cutting off the first letter of the next word.

      If lineLength < maxLengthOfALine Then

        startingPosition += lineLength

      Else

        startingPosition += lineLength + 1

      End If

      ' Reset the lineLength back to the default.

      lineLength = maxLengthOfALine
      endingPosition = startingPosition + Largo

    End While

    Return arrTexto

  End Function

  ''' <summary>
  ''' calcula la edad a partir de una fecha dada
  ''' </summary>
  ''' <param name="fecha"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CalcularEdadEstimada(ByVal fecha As String) As String
    Dim edad As String = ""
    Dim nuevaFecha As Date
    Dim fechaISO As String
    Dim fechaHoyISO As String = Left(Herramientas.ObtenerFechaHoraActualSistema(), 8)
    Dim mesDiaIso As String
    Dim mesDiaHoyISO As String

    Try
      If Not String.IsNullOrEmpty(fecha) Then
        fechaISO = Utilidades.FechaISO(fecha)
        mesDiaIso = Right(fechaISO, 4)
        mesDiaHoyISO = Right(fechaHoyISO, 4)
        nuevaFecha = Convert.ToDateTime(fecha)
        edad = DateDiff(DateInterval.Year, nuevaFecha, Herramientas.MyNow())
        edad = IIf(mesDiaHoyISO < mesDiaIso, edad - 1, edad)
        If edad = "0" Then
          edad = ""
        End If
      End If
    Catch ex As Exception
      edad = ""
    End Try
    Return edad
  End Function

  ''' <summary>
  ''' recorta el largo de un texto a un maximo especificado
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <param name="largoMaximo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RecortarTexto(ByVal texto As String, ByVal largoMaximo As Integer, Optional ByVal sufijo As String = "") As String
    Dim nuevoTexto As String = ""
    Dim largoTexto As Integer = texto.Length
    Try
      If largoTexto <= largoMaximo Then
        nuevoTexto = texto
      Else
        nuevoTexto = texto.Substring(0, largoMaximo) & sufijo
      End If
    Catch ex As Exception
      nuevoTexto = texto
    End Try
    Return nuevoTexto
  End Function

  ''' <summary>
  ''' crea una espera para ejecutar el proceso que sigue
  ''' </summary>
  ''' <param name="milisegundos"></param>
  ''' <remarks></remarks>
  Public Shared Sub DormirProceso(ByVal milisegundos As Integer)
    Try
      Thread.Sleep(milisegundos)
    Catch ex As Exception
      'no hace nada
    End Try
  End Sub

  ''' <summary>
  ''' elimina columnas de un dataset que comienzen con un cierto caracter
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="indiceTabla"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EliminarColumnasEspecialesDataset(ByVal ds As DataSet, Optional ByVal indiceTabla As Integer = 0, Optional ByVal caracterInicialColumna As String = "_") As DataSet
    Dim newDataset As New DataSet
    Dim totalColumnas, i As Integer
    Dim dt As DataTable
    Dim dc As DataColumn
    Dim listadoIndicesEliminar As String = ""
    Dim aListadoIndicesEliminar As String()
    Dim indiceColumna As Integer

    Try
      dt = ds.Tables(indiceTabla)
      totalColumnas = dt.Columns.Count

      'construye cabeceras de la tabla
      For i = 0 To totalColumnas - 1
        If dt.Columns.Item(i).ColumnName.StartsWith(caracterInicialColumna) Then
          listadoIndicesEliminar &= i & ","
        End If
      Next
      listadoIndicesEliminar = Utilidades.EliminaUltimoCaracterTexto(listadoIndicesEliminar)
      If Not String.IsNullOrEmpty(listadoIndicesEliminar) Then
        aListadoIndicesEliminar = listadoIndicesEliminar.Split(",")
        For i = aListadoIndicesEliminar.Length - 1 To 0 Step -1
          indiceColumna = aListadoIndicesEliminar(i)
          dc = dt.Columns(indiceColumna)
          dt.Columns.Remove(dc)
        Next
      End If
      newDataset = ds.Copy
    Catch ex As Exception
      newDataset = ds
    End Try
    Return newDataset
  End Function

#End Region

#Region "Fechas"
  ''' <summary>
  ''' obtiene indice del dia de la semana consultada. Los indices devueltos son: 
  ''' domingo->0; lunes->1; martes->2; miercoles->3; jueves->4; viernes->5; sabado->6;
  ''' </summary>
  ''' <param name="dia"></param>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerIndiceDiaSemana(ByVal dia As Integer, ByVal mes As Integer, ByVal ano As Integer) As Integer
    Dim indiceDiaSemana As Integer
    'obtiene indice del dia de la semana
    indiceDiaSemana = Weekday(DateSerial(ano, mes, dia)) - 1
    'retorna valor
    Return indiceDiaSemana
  End Function

  ''' <summary>
  ''' verificar si la fecha dada corresponde al dia lunes
  ''' </summary>
  ''' <param name="dia"></param>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function EsLunes(ByVal dia As Integer, ByVal mes As Integer, ByVal ano As Integer) As Boolean
    Dim indiceDiaSemana As Integer
    Dim boolEsLunes As Boolean
    'obtiene indice del dia de la semana
    indiceDiaSemana = ObtenerIndiceDiaSemana(dia, mes, ano)

    'si es lunes devuelve verdadero
    If indiceDiaSemana = INDICE_DIA_LUNES Then
      boolEsLunes = True
    Else
      boolEsLunes = False
    End If
    'retorna valor
    Return boolEsLunes
  End Function

  ''' <summary>
  ''' obtiene el dia lunes de la semana en la cual esta la fecha consultada con formato dd/mm/aaaa
  ''' </summary>
  ''' <param name="dia"></param>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerLunesDeLaSemana(ByVal dia As Integer, ByVal mes As Integer, ByVal ano As Integer) As String
    Dim strFechaDada, newFecha, strFechaLunes As String
    Dim indiceDiaSemana As Integer
    strFechaDada = dia & "-" & mes & "-" & ano
    indiceDiaSemana = ObtenerIndiceDiaSemana(dia, mes, ano)
    'si es domingo la fecha dada entonces resta 6 dias para atras
    If indiceDiaSemana = 0 Then
      newFecha = DateAdd("d", -6, strFechaDada)
    Else
      newFecha = DateAdd("d", -1 * (indiceDiaSemana - INDICE_DIA_LUNES), strFechaDada)
    End If
    'construye nueva fecha para el dia lunes
    strFechaLunes = Right("00" & Day(newFecha), 2) & "/" & Right("00" & Month(newFecha), 2) & "/" & Right("0000" & Year(newFecha), 4)
    'retorna valor
    Return strFechaLunes
  End Function

  ''' <summary>
  ''' obtiene el ultimo dia del mes
  ''' </summary>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerUltimoDiaMes(ByVal mes As Integer, ByVal ano As Integer) As Integer
    Dim diaMes As Integer
    Select Case mes
      Case 1, 3, 5, 7, 8, 10, 12
        diaMes = 31
      Case 4, 6, 9, 11
        diaMes = 30
      Case 2
        If IsDate(ano & "-" & mes & "-" & "29") Then
          diaMes = 29
        Else
          diaMes = 28
        End If
      Case Else
        diaMes = 0
    End Select
    'retorna valor
    Return diaMes
  End Function

  ''' <summary>
  ''' obtiene la fecha del primer lunes del mes con formato dd/mm/aaaa
  ''' </summary>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerPrimerLunesMes(ByVal mes As Integer, ByVal ano As Integer) As String
    Dim strPrimerDiaMes, strFechaLunes As String
    Dim newFecha As String = ""
    Dim indiceDiaSemana As Integer

    strPrimerDiaMes = "1" & "-" & mes & "-" & ano
    indiceDiaSemana = ObtenerIndiceDiaSemana("1", mes, ano)

    'si es lunes la fecha dada entonces la asigna como fecha del primer lunes del mes, sino calcula cual es el siguiente lunes
    If indiceDiaSemana = 1 Then
      newFecha = strPrimerDiaMes
    Else
      Select Case indiceDiaSemana
        Case 0
          newFecha = DateAdd("d", 1, strPrimerDiaMes)
        Case 2
          newFecha = DateAdd("d", 6, strPrimerDiaMes)
        Case 3
          newFecha = DateAdd("d", 5, strPrimerDiaMes)
        Case 4
          newFecha = DateAdd("d", 4, strPrimerDiaMes)
        Case 5
          newFecha = DateAdd("d", 3, strPrimerDiaMes)
        Case 6
          newFecha = DateAdd("d", 2, strPrimerDiaMes)
      End Select
    End If
    'construye nueva fecha para el dia lunes
    strFechaLunes = Right("00" & Day(newFecha), 2) & "/" & Right("00" & Month(newFecha), 2) & "/" & Right("0000" & Year(newFecha), 4)
    'retorna valor
    Return strFechaLunes
  End Function

  ''' <summary>
  ''' obtiene el ultimo domingo del mes con formato dd/mm/aaaa
  ''' </summary>
  ''' <param name="mes"></param>
  ''' <param name="ano"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerUltimoDomingoMes(ByVal mes As Integer, ByVal ano As Integer) As String
    Dim newFecha, strFechaDomingo, strPrimerLunesMesSiguiente As String
    'construye mes siguiente al consultado para obtener el primer lunes de ese mes
    'y asi poder obtener el ultimo domingo del mes consultado
    If mes = 12 Then
      mes = 1
      ano = ano + 1
    Else
      mes = mes + 1
    End If
    'obtiene fecha del primer lunes del mes siguiente
    strPrimerLunesMesSiguiente = ObtenerPrimerLunesMes(mes, ano)
    strPrimerLunesMesSiguiente = Replace(strPrimerLunesMesSiguiente, "/", "-")
    'obtiene ultimo domingo del mes actual
    newFecha = DateAdd("d", -1, strPrimerLunesMesSiguiente)
    'construye nueva fecha para el dia lunes
    strFechaDomingo = Right("00" & Day(newFecha), 2) & "/" & Right("00" & Month(newFecha), 2) & "/" & Right("0000" & Year(newFecha), 4)
    'retorna valor
    Return strFechaDomingo
  End Function

  ''' <summary>
  ''' obtiene el dia domingo de la semana en la cual esta la fecha consultada
  ''' </summary>
  ''' <param name="diaLunesSemana"></param>
  ''' <returns></returns>
  ''' <remarks>VSR, 10/03/2010</remarks>
  Public Shared Function ObtenerDomingoDeLaSemana(ByVal diaLunesSemana As String) As String
    Dim arrFechaLunes As String()
    Dim newLunes, newDomingo, strFechaDomingo As String
    Dim dia, mes, ano As String
    'obtiene fecha valida para el sistema
    arrFechaLunes = Split(diaLunesSemana, "/")
    newLunes = DateSerial(arrFechaLunes(2), arrFechaLunes(1), arrFechaLunes(0))
    newDomingo = Replace(DateAdd("d", 6, newLunes), "-", "/")

    If Day(newDomingo) <= 9 Then dia = "0" & Day(newDomingo) Else dia = Day(newDomingo)
    If Month(newDomingo) <= 9 Then mes = "0" & Month(newDomingo) Else mes = Month(newDomingo)
    ano = Year(newDomingo)

    'construye fecha
    strFechaDomingo = dia & "/" & mes & "/" & ano
    'retorna valor
    Return strFechaDomingo
  End Function

  ''' <summary>
  ''' retorna el nombre del dia de la semana en formato largo y corto
  ''' </summary>
  ''' <param name="indiceDiaSemana"></param>
  ''' <param name="usarNombreCorto2Letras"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerNombreDiaSemanaPorIndice(ByVal indiceDiaSemana As Integer, Optional ByVal usarNombreCorto2Letras As Boolean = False, Optional ByVal usarAcento As Boolean = False) As String
    Dim nameDiaSemana As String = ""
    'segun el indice busce el nombre
    Select Case indiceDiaSemana
      Case 0, 7
        nameDiaSemana = IIf(usarNombreCorto2Letras, "DO", "DOMINGO")
      Case 1
        nameDiaSemana = IIf(usarNombreCorto2Letras, "LU", "LUNES")
      Case 2
        nameDiaSemana = IIf(usarNombreCorto2Letras, "MA", "MARTES")
      Case 3
        nameDiaSemana = IIf(usarNombreCorto2Letras, "MI", "MIERCOLES")
      Case 4
        nameDiaSemana = IIf(usarNombreCorto2Letras, "JU", "JUEVES")
      Case 5
        nameDiaSemana = IIf(usarNombreCorto2Letras, "VI", "VIERNES")
      Case 6
        nameDiaSemana = IIf(usarNombreCorto2Letras, "SA", "SABADO")
    End Select

    If usarAcento Then
      Select Case indiceDiaSemana
        Case 3
          nameDiaSemana = IIf(usarNombreCorto2Letras, "MI", "MI�RCOLES")
        Case 6
          nameDiaSemana = IIf(usarNombreCorto2Letras, "SA", "S�BADO")
      End Select
    End If

    'devuelve valor a la funcion
    Return nameDiaSemana
  End Function

#End Region

#Region "Shared"
  ''' <summary>
  ''' Retorna objCheck si no es nulo, sino retorna objReplacement
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function IsNull(ByVal objCheck As Object, ByVal objReplacement As Object) As String
    If IsDBNull(objCheck) Then
      Return objReplacement
    Else
      Return objCheck
    End If
  End Function

  ''' <summary>
  ''' Retorna ValorActual si no es nulo, sino retorna ValorReemplazo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function IsNull(ByVal ValorActual As String, ByVal ValorReemplazo As String) As String
    If IsDBNull(ValorActual) Or ValorActual = "" Then
      Return ValorReemplazo
    Else
      Return ValorActual
    End If
  End Function

  ''' <summary>
  ''' construye mensaje en la pantalla
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub setPanelMensajeUsuario(ByVal panel As Panel, ByVal textoMensaje As String, ByVal tipoMensaje As eTipoMensajeAlert, Optional ByVal mostrarBotonCerrar As Boolean = False)
    If textoMensaje = "" Then Exit Sub

    Dim lblMensaje As New Label
    Dim template As String = "<div class=""alert alert-{TIPO} {DISMISSABLE}"">{BOTON_CERRAR}{MENSAJE}</div>"
    Dim tipo As String = tipoMensaje.ToString().ToLower
    Dim dismissable As String = IIf(mostrarBotonCerrar, "alert-dismissable", "")
    Dim botonCerrar As String = IIf(mostrarBotonCerrar, "<button type=""button"" class=""close"" data-dismiss=""alert"" aria-hidden=""true"">&times;</button>", "")

    template = template.Replace("{TIPO}", tipo)
    template = template.Replace("{DISMISSABLE}", dismissable)
    template = template.Replace("{BOTON_CERRAR}", botonCerrar)
    template = template.Replace("{MENSAJE}", textoMensaje)

    lblMensaje.Text = template
    panel.Controls.Add(lblMensaje)
    panel.HorizontalAlign = HorizontalAlign.Center
  End Sub

  ''' <summary>
  ''' obtiene el numero de registros por pagina definidos por defecto en el web.config
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerRegistrosPorPaginaDefault() As Integer
    Dim registrosPorPagina As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("RegistrosPorPagina"))
    Return registrosPorPagina
  End Function

  ''' <summary>
  ''' Asigna el tama�o de la pagina sacando el valor del web.config
  ''' </summary>
  ''' <param name="gridView"></param>
  ''' <remarks></remarks>
  Public Sub InicializaGrilla(ByRef gridView As GridView)
    gridView.PageSize = ObtenerRegistrosPorPaginaDefault()
  End Sub

  ''' <summary>
  ''' obtiene la fecha actual del sistema
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerFechaActualSistema() As String
    Dim fecha As Date = Herramientas.MyNow()
    Dim diaActual As String = Day(fecha).ToString
    Dim mesActual As String = Month(fecha).ToString
    Dim anoActual As String = Year(fecha).ToString
    Dim nuevaFecha As String

    diaActual = IIf(diaActual <= 9, "0" & diaActual, diaActual)
    mesActual = IIf(mesActual <= 9, "0" & mesActual, mesActual)
    'construye nueva fecha
    nuevaFecha = diaActual & "/" & mesActual & "/" & anoActual
    'retorna valor
    Return nuevaFecha
  End Function

  ''' <summary>
  ''' graba la url de referencia en session
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub GrabarURLRefererSession(ByVal nombrePaginaActual As String, ByVal url As String)
    Dim urlActual As String
    Dim sl As SortedList
    If String.IsNullOrEmpty(nombrePaginaActual) Or String.IsNullOrEmpty(url) Then
      Exit Sub
    End If
    If (Session("UrlReferer") Is Nothing) Then
      sl = New SortedList
      'agrega la nueva key con su contenido
      sl.Add(nombrePaginaActual, url)
      Session("UrlReferer") = sl
    Else
      sl = CType(Session("UrlReferer"), SortedList)
      urlActual = sl.Item(nombrePaginaActual)
      'si no existe una referencia a la pagina entonces la guarda
      If String.IsNullOrEmpty(urlActual) Then
        'agrega la nueva key con su contenido
        sl.Add(nombrePaginaActual, url)
        Session("UrlReferer") = sl
      End If
    End If
  End Sub

  ''' <summary>
  ''' obtiene url de la sesion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ObtenerURLRefererSession(ByVal nombrePaginaActual As String) As String
    Dim sl As SortedList
    Dim url As String
    Dim queryString As String = ""

    If (Session("UrlReferer") Is Nothing) Then
      Return Nothing
    Else
      sl = CType(Session("UrlReferer"), SortedList)
      url = sl.Item(nombrePaginaActual)
      Dim aUrl As String() = url.Split("?")
      'se separa la URL para poder encodear el valor encriptado(si es que lo tiene)
      If aUrl.Length > 1 Then
        url = aUrl(0)
        queryString = aUrl(1)
        'si el queryString comienza con "a=" indica que su valor esta encriptado, entonces hay que encodearlo
        If queryString.StartsWith("a=") Then
          queryString = "a=" & Server.UrlEncode(queryString.Substring(2, queryString.Length - 2))
        End If
        queryString = "?" & queryString
      End If
      Return url & queryString
    End If
  End Function

  ''' <summary>
  ''' limpia la sesion
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub LimpiarURLRefererSession(ByVal nombrePaginaActual As String)
    Dim sl As SortedList
    sl = CType(Session("UrlReferer"), SortedList)
    sl.Remove(nombrePaginaActual)
    If sl.Count = 0 Then
      Session("UrlReferer") = Nothing
    End If
  End Sub

  ''' <summary>
  ''' graba el objeto Usuario en sesion
  ''' </summary>
  ''' <param name="oUsuario"></param>
  ''' <remarks>Por VSR, 08/10/2008</remarks>
  Public Sub GrabarUsuarioSession(ByVal oUsuario As Usuario)
    Try
      Session("Usuario") = oUsuario
    Catch ex As Exception
      Session("Usuario") = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' obtiene el objeto usuario de la sesion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 08/10/2008</remarks>
  Public Function ObtenerUsuarioSession() As Usuario
    Try
      If (Session("Usuario") Is Nothing) Then
        Return Nothing
      Else
        Return CType(Session("Usuario"), Usuario)
      End If
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' limpia la sesion del objeto Usuario
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub LimpiarUsuarioSession()
    Try
      Session("Usuario") = Nothing
    Catch ex As Exception
      Session("Usuario") = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' graba un objeto en session
  ''' </summary>
  ''' <param name="nombreSesion"></param>
  ''' <param name="valor"></param>
  ''' <remarks></remarks>
  Public Sub GrabarObjectSession(ByVal nombreSesion As String, ByVal valor As Object)
    Try
      Session("obj_" & nombreSesion) = valor
    Catch ex As Exception
      Session("obj_" & nombreSesion) = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' obtiene valor booleando en session
  ''' </summary>
  ''' <param name="nombreSesion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ObtenerObjectSession(ByVal nombreSesion As String) As Object
    Try
      If (Session("obj_" & nombreSesion) Is Nothing) Then
        Return Nothing
      Else
        Return CType(Session("obj_" & nombreSesion), Object)
      End If
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' limpia sesion del boolean
  ''' </summary>
  ''' <param name="nombreSesion"></param>
  ''' <remarks></remarks>
  Public Sub LimpiarObjectSession(ByVal nombreSesion As String)
    Try
      Session("obj_" & nombreSesion) = Nothing
    Catch ex As Exception
      Session("obj_" & nombreSesion) = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' obtiene la url base del sitio
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/08/2009</remarks>
  Public Shared Function ObtenerUrlBase() As String
    Dim url As String = "./"
    Try
      url = HttpContext.Current.Request.Url.Scheme & "://" & HttpContext.Current.Request.Url.Authority & HttpContext.Current.Request.ApplicationPath.TrimEnd("/")
    Catch ex As Exception
      url = "./"
    End Try
    Return url
  End Function

  ''' <summary>
  ''' obtiene el indice de la primera fila de la grilla dependiendo en que pagina este
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerIndicePrimeraFilaGrilla(ByVal gridView As GridView) As Integer
    'obtiene valor actual de la pagina de la grilla
    Dim indicePaginaInicio As Integer = gridView.PageIndex + 1
    'obtiene el numero de registros por pagina de la grilla
    Dim numeroRegistrosPorPagina As Integer = gridView.PageSize
    'calcula cual es el valor del indice de la primera fila de la grilla
    Dim indicePrimeraFila As Integer = indicePaginaInicio * numeroRegistrosPorPagina - (numeroRegistrosPorPagina - 1)
    'retorna valor
    Return indicePrimeraFila
  End Function

  ''' <summary>
  ''' asigna valor del indice de la fila al control
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub SetControlIndiceFila(ByVal row As GridViewRow, ByVal nombreControlIndiceFila As String, ByVal indiceFila As Integer)
    Dim lblIndiceFila As Label
    lblIndiceFila = CType(row.FindControl(nombreControlIndiceFila), Label)
    lblIndiceFila.Text = indiceFila
  End Sub

  ''' <summary>
  ''' formate un numero con separador de miles
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function FormatearConSeparadorMiles(ByVal valor As String, Optional ByVal cantidadDecimales As String = "0") As String
    Dim nuevoValor As String
    Dim conDecimales As Boolean
    'si trae puntos se los quita
    valor = valor.Replace(".", "")

    If (cantidadDecimales <> "0") Then
      conDecimales = True
    Else
      conDecimales = IIf(valor.Contains(","), True, False)
      cantidadDecimales = IIf(conDecimales, "2", cantidadDecimales)
    End If

    If EsNumerico(valor, conDecimales) Then
      nuevoValor = FormatNumber(valor, cantidadDecimales, TriState.True, TriState.False, TriState.True)
    Else
      nuevoValor = ""
    End If
    'retorna valor
    Return nuevoValor
  End Function

  ''' <summary>
  ''' obtiene el signo de la moneda segun el pais del servidor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerSimboloMoneda() As String
    Dim signoMoneda As String = CurrentCulture.NumberFormat.CurrencySymbol
    Return signoMoneda
  End Function

  ''' <summary>
  ''' obtiene el caracter del separador de mil
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerSeparadorMil() As String
    Dim separador As String = CurrentCulture.NumberFormat.CurrencyGroupSeparator
    Return separador
  End Function

  ''' <summary>
  ''' obtiene el caracter del separador de decimal
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerSeparadorDecimal() As String
    Dim separador As String = CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    Return separador
  End Function

  ''' <summary>
  ''' muestra el total de registros de una grilla
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub MostrarTotalRegistrosGrilla(ByVal dv As DataView, ByVal label As Label, Optional ByVal nombreElementoContar As String = "Registros", Optional ByVal ocultarTextoTotal As Boolean = False)
    Dim totalRegistros As String = dv.Table.Rows.Count
    Dim textoSugerencia As String = ""

    If totalRegistros = 500 Then
      textoSugerencia = "&nbsp;o m&aacute;s"
    End If

    label.Visible = True
    label.Text = IIf(ocultarTextoTotal, "", "Total de ") & nombreElementoContar & ": " & totalRegistros & textoSugerencia
  End Sub

  ''' <summary>
  ''' reemplaza un valor vacio por una imagen en blanco
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ReemplazarPorImagenVacia(ByVal texto As String, Optional ByVal anchoImagenVacia As String = "1", Optional ByVal altoImagenVacia As String = "1") As String
    Dim valorRetorno As String
    Dim styleImage As String = "style=""width:@width;height:@height;"""
    styleImage = styleImage.Replace("@width", anchoImagenVacia)
    styleImage = styleImage.Replace("@height", altoImagenVacia)

    If texto <> String.Empty Then
      valorRetorno = texto
    Else
      valorRetorno = "<img src=""../img/_blank.gif"" alt="""" " & styleImage & " />"
    End If
    'retorna valor
    Return valorRetorno
  End Function

  ''' <summary>
  ''' limpia el cache del navegador
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub LimpiarCache()
    Context.Response.AppendHeader("CACHE-CONTROL", "NO-CACHE")
    Context.Response.AppendHeader("Pragma", "NO-CACHE")
    Context.Response.Expires = 0
    Context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
  End Sub

  ''' <summary>
  ''' Retorna el lenguaje del sitio definido en web.config
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function LenguajeSitio() As String
    Dim _lenguaje As String = AppSettings("lenguajeSitio").ToString
    Return _lenguaje
  End Function

  ''' <summary>
  ''' Obtiene el ambiente en donde se esta ejecutando la aplicacion:Desarrollo, Demo, Producci�n, etc.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAmbienteEjecucion() As String
    Dim ambiente As String = Configuracion.Leer("webAmbienteEjecucion")
    ambiente = IIf(String.IsNullOrEmpty(ambiente), "INDEFINIDO", ambiente).ToString.ToUpper

    If (ambiente = "PRODUCCION" Or ambiente = "PRODUCCI�N") Then
      ambiente = "PRODUCCION"
    End If

    Return ambiente
  End Function

  ''' <summary>
  ''' Retorna el pais asociado al sitio
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function PaisSitio() As String
    Dim _texto As String = Configuracion.Leer("webPais")
    Return _texto
  End Function

  ''' <summary>
  ''' obtiene el tama�o maximo permitido para subir un archivo configurado en web.config
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function TamanoMaximoUploadEnKb(ByVal valor As String) As String
    Dim _tamano As String = valor
    If _tamano = "" Or _tamano Is Nothing Then _tamano = TamanoMaximoArchivoUploadEnKb
    Return _tamano
  End Function

  ''' <summary>
  ''' verifica si existe un valor seleccionado
  ''' </summary>
  ''' <param name="Valor"></param>
  ''' <param name="IdValorSeleccionado"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExisteValorSeleccionado(ByVal Valor As String, ByVal IdValorSeleccionado As String) As Boolean
    'si no hay lista donde comparar entonces
    If IdValorSeleccionado = "" Then
      Return False
      Exit Function
    End If

    Dim existeValor As Boolean
    'formatea el string de los locales para facilitar la busqueda de un valor dado
    Dim newIdValoresSeleccionados As String = "," & IdValorSeleccionado & ","
    Dim newValor As String = "," & Valor & ","
    If InStr(newIdValoresSeleccionados, newValor, CompareMethod.Text) > 0 Then
      existeValor = True
    Else
      existeValor = False
    End If
    'retorna valor
    Return existeValor
  End Function

  ''' <summary>
  ''' extrae el texto de una cadena entre dos palabras dadas
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <param name="palabraInicial"></param>
  ''' <param name="palabraFinal"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExtraeTextoEntreDosPalabras(ByVal texto As String, ByVal palabraInicial As String, ByVal palabraFinal As String) As String
    'Busca texto
    Dim inicio, fin As Integer
    Dim textoExtraido As String = ""

    Try
      If Not String.IsNullOrEmpty(texto) Then
        inicio = InStr(1, texto, palabraInicial) + Len(palabraInicial)
        fin = InStr(inicio, texto, palabraFinal)

        'Extrae texto y le quita enters
        If fin > 0 And inicio > Len(palabraInicial) Then
          textoExtraido = Replace(Mid(texto, inicio, fin - inicio), vbCrLf, "").Trim
        End If
      End If
    Catch ex As Exception
      textoExtraido = texto
    End Try
    'retorna valor
    Return textoExtraido
  End Function

  ''' <summary>
  ''' al texto le concatena el mail del(los) administrador
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConcatenarMailAdministrador(ByVal texto As String) As String
    texto &= IIf(String.IsNullOrEmpty(texto), MailAdministrador(), ";" & MailAdministrador())
    Return texto
  End Function

  'Funcion    : MailHost
  'Descripcion: Retorna el host del mail
  'Por        : VSR, 07/07/2008
  Public Shared Function MailHost() As String
    Dim _mailHost As String = Herramientas.MailHost()
    Return _mailHost
  End Function

  'Descripcion: Retorna el host del mail
  'Por        : VSR, 07/07/2008
  Public Shared Function MailHostHD() As String
    Dim _mailHost As String = Herramientas.MailHostHD()
    Return _mailHost
  End Function

  'Funcion    : MailEmisor
  'Descripcion: Retorna el email del emisor
  'Por        : VSR, 07/07/2008
  Public Shared Function MailEmisor() As String
    Dim _emailEmisor As String = Herramientas.MailEmisor()
    Return _emailEmisor
  End Function

  'Funcion    : MailPasswordEmisor
  'Descripcion: Retorna el password del email del emisor
  'Por        : VSR, 07/07/2008
  Public Shared Function MailPasswordEmisor() As String
    Dim _passEmailEmisor As String = Herramientas.MailPasswordEmisor()
    Return _passEmailEmisor
  End Function

  'Funcion    : MailNombreEmisor
  'Descripcion: Retorna el nombre del emisor del emisor
  'Por        : VSR, 07/07/2008
  Public Shared Function MailNombreEmisor() As String
    Dim _nombreEmailEmisor As String = Herramientas.MailNombreEmisor()
    Return _nombreEmailEmisor
  End Function

  'Funcion    : MailPruebas
  'Descripcion: Retorna el o los mails definidos para pruebas
  'Por        : VSR, 07/07/2008
  Public Shared Function MailPruebas() As String
    Dim _emailPruebas As String = Herramientas.MailPruebas()
    Return _emailPruebas
  End Function

  'Funcion    : MailAdministrador
  'Descripcion: Retorna el o los mails definidos para los administradores
  'Por        : VSR, 07/07/2008
  Public Shared Function MailAdministrador() As String
    Dim _emailAdministrador As String = Herramientas.MailAdministrador()
    Return _emailAdministrador
  End Function

  'Funcion    : MailModoDebug
  'Descripcion: Retorna si esta en modo debug o no
  'Por        : VSR, 07/07/2008
  Public Shared Function MailModoDebug() As Boolean
    Dim _modoDebug As Boolean = Herramientas.MailModoDebug()
    Return _modoDebug
  End Function

  'rellena un string hasta el largo maximo con caracter determinado
  Public Shared Function RellenarString(ByVal s As String, ByVal largo As Integer, Optional ByVal caracter As String = " ") As String
    'crea un string de *caracter de *largo especificado
    Dim output As String = String.Empty
    output = StrDup(largo, caracter)

    'le pegamos el relleno a la cadena
    output = s & output
    'retorna valor
    Return output.Substring(0, largo)

  End Function

  Public Shared Function RellenarStringIzquierda(ByVal s As String, ByVal largo As Integer, Optional ByVal caracter As String = " ") As String
    'crea un string de *caracter de *largo especificado
    Dim output As String = String.Empty
    output = StrDup(largo, caracter)

    'le pegamos el relleno a la cadena
    output = output & s
    'retorna valor
    Return output

  End Function

  ''' <summary>Browser capabilities: 2D array of Name/Values</summary>
  Public Shared Function NavegadorAtributos() As String(,)
    Dim _agent As String = [String].Empty

    If HttpContext.Current Is Nothing Then
      Return Nothing
    End If

    Try
      ' detailed string describing some of browser capabilities
      _agent = HttpContext.Current.Request.UserAgent

      ' browser capabilities object
      Dim _browser As HttpBrowserCapabilities = HttpContext.Current.Request.Browser
      Dim arrFieldValue As String(,) = {{"Name", "Version", "Platform", "ECMA Script Version", "Is Mobile Device", "Is Beta", _
       "Is Win16", "Is Win32", "Supports Frames", "Supports Tables", "Supports Cookies", "Supports CSS", _
       "Supports VB Script", "Supports JavaScript", "Supports Java Applets", "Supports ActiveX Controls", "Supports CallBack", "Supports XMLHttp", _
       [String].Empty, "User Agent Details"}, {IIf((_agent.ToLower().Contains("chrome")), "Chrome", _browser.Browser), IIf((_agent.ToLower().Contains("chrome")), "See User Agent Details below", _browser.Version), _browser.Platform, _browser.EcmaScriptVersion.ToString(), IIf((_browser.IsMobileDevice), "YES", "NO"), IIf((_browser.Beta), "YES", "NO"), _
       IIf((_browser.Win16), "YES", "NO"), IIf((_browser.Win32), "YES", "NO"), IIf((_browser.Frames), "YES", "NO"), IIf((_browser.Tables), "YES", "NO"), IIf((_browser.Cookies), "YES", "NO"), IIf((_browser.SupportsCss), "YES", "NO"), _
       IIf((_browser.VBScript), "YES", "NO"), IIf(True, "YES", "NO"), IIf((_browser.JavaApplets), "YES", "NO"), IIf((_browser.ActiveXControls), "YES", "NO"), IIf((_browser.SupportsCallback), "YES", "NO"), IIf((_browser.SupportsXmlHttp), "YES", "NO"), _
       [String].Empty, _agent}}
      Return arrFieldValue
    Catch
      Return Nothing
    End Try
  End Function

  ''' <summary>JavaScript string to containing Browsers capabilities</summary>
  Public Shared Function NavegadorJavaScript() As String
    ' return string contains JavaScript
    Dim MsgBrowser As String = String.Empty

    Dim arrBrowser As String(,) = NavegadorAtributos()
    If arrBrowser Is Nothing Then
      Return String.Empty
    End If

    Try
      ' pop-up message using JavaScript:alert function
      MsgBrowser = "javascript:alert('Your Browser properties: \n"

      Dim i As Integer = 0
      While i < arrBrowser.GetLength(1)
        MsgBrowser += "\n" + arrBrowser(0, i) + " : " + arrBrowser(1, i)
        System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
      End While

      Return MsgBrowser & "')"
    Catch
      Return [String].Empty
    End Try
  End Function

  ''' <summary>
  ''' obtiene nombre de la pagina actual
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerNombrePaginaActual() As String
    Dim pagina As String

    Try
      pagina = System.IO.Path.GetFileName(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"))
    Catch ex As Exception
      pagina = ""
    End Try

    Return pagina
  End Function

  ''' <summary>
  ''' obtiene un arreglo con los "valores agrupados" del datatable
  ''' </summary>
  ''' <param name="dt"></param>
  ''' <param name="nombreColumna"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerArrayGroupByDeDataTable(ByVal dt As DataTable, ByVal nombreColumna As String, Optional ByVal ordenarPorColumna As Boolean = False) As ArrayList
    Dim array As New ArrayList
    Dim valorAnterior As String = "-1"
    Dim dv As New DataView
    Dim ds As New DataSet

    Try
      'ordena la tabla por la columna seleccionada
      If ordenarPorColumna Then
        dv = dt.DefaultView
        dv.Sort = nombreColumna
        ds = CrearDataSetDesdeDataViewConRowFilter(dv)
        dt = ds.Tables(0)
      End If
      For Each row As DataRow In dt.Rows
        Dim valor As String = row(nombreColumna).ToString().Trim()
        If Not array.Contains(valor) Then
          array.Add(valor)
        End If
      Next
      Return array
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' obtiene el singular o plural de la palabra segun como corresponda
  ''' </summary>
  ''' <param name="total"></param>
  ''' <param name="palabra"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerTextoSingularPlural(ByVal total As Integer, ByVal palabra As String) As String
    Dim texto As String = ""
    Try
      Select Case palabra
        Case "respectivamente"
          texto = IIf(total = 1, "", " respectivamente")
        Case "el imputado"
          texto = IIf(total = 1, " el imputado", " los imputados")
        Case "ingres�"
          texto = IIf(total = 1, " ingres�", " ingresaron")
        Case "pretend�a"
          texto = IIf(total = 1, " pretend�a", " pretend�an")
        Case "procedi�"
          texto = IIf(total = 1, " procedi�", " procedieron")
        Case "est� avaluado"
          texto = IIf(total = 1, " est� avaluado", " est�n avaluados")
        Case "la especie"
          texto = IIf(total = 1, " la especie", " las especies")
        Case "testigo"
          texto = IIf(total = 1, " testigo", " testigos")
        Case "quien presenci�"
          texto = IIf(total = 1, " quien presenci�", " quienes presenciaron")
        Case "apoder�"
          texto = IIf(total = 1, " apoder�", " apoderaron")
        Case "puede ser notificado"
          texto = IIf(total = 1, " puede ser notificado", " pueden ser notificados")
        Case "el testigo"
          texto = IIf(total = 1, " el testigo", " los testigos")
        Case "recibir� notificaci�n"
          texto = IIf(total = 1, " recibir� notificaci�n", " recibir�n notificaciones")
        Case "domiciliado"
          texto = IIf(total = 1, " domiciliado", " domiciliados")
        Case "el delito"
          texto = IIf(total = 1, " el delito", " los delitos")
        Case "el(la) se�or(a)"
          texto = IIf(total = 1, " el(la) se�or(a)", " los(las) se�ores(as)")
        Case "el deudor"
          texto = IIf(total = 1, " el deudor", " los deudores")
        Case "fue detectado"
          texto = IIf(total = 1, " fue detectado", " fueron detectados")
        Case "trataba"
          texto = IIf(total = 1, " trataba", " trataban")
        Case "caus� da�o"
          texto = IIf(total = 1, " caus� da�o", " causaron da�os")
        Case "otorg�"
          texto = IIf(total = 1, " otorg�", " otorgaron")
        Case "la persona"
          texto = IIf(total = 1, " la persona", " las personas")
        Case "ha causado"
          texto = IIf(total = 1, " ha causado", " han causado")
        Case "manifiesta"
          texto = IIf(total = 1, " manifiesta", " manifiestan")
        Case "ha pagado"
          texto = IIf(total = 1, " ha pagado", " hayan pagado")
        Case "incidente"
          texto = IIf(total = 1, " incidente", " incidentes")
        Case "imputado individualizado"
          texto = IIf(total = 1, " imputado individualizado", " imputados individualizados")
        Case "imputado"
          texto = IIf(total = 1, " imputado", " imputados")
        Case "hurto"
          texto = IIf(total = 1, " hurto", " hurtos")
        Case "tienda"
          texto = IIf(total = 1, " tienda", " tiendas")
        Case "registr�"
          texto = IIf(total = 1, " registr�", " registraron")
        Case "tienda registr�"
          texto = IIf(total = 1, " tienda registr�", " tiendas registraron")
        Case "fue reincidente"
          texto = IIf(total = 1, " fue reincidente", " fueron reincidentes")
      End Select
    Catch ex As Exception
      texto = " " & palabra
    End Try

    Return texto
  End Function

  ''' <summary>
  ''' obtiene el numero correlativo de un indice de acuerdo a su nivel
  ''' ejm: 1
  '''      1.1
  '''      1.1.1
  '''      1.1.2
  '''      2
  '''      3
  '''      3.1
  '''      ... etc
  ''' </summary>
  ''' <param name="arrayIndice"></param>
  ''' <param name="nivel"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerNumeroIndiceTablaContenido(ByRef arrayIndice As Integer(), ByVal nivel As Integer) As String
    Dim numeroIndice As String = ""
    Dim valorIndiceNivel As Integer
    Dim largoArray, i As Integer
    Dim DIMENSION_MAXIMAS As Integer = 9
    Dim signoSeparacion As String

    Try
      'si el arreglo no existe entonces lo inicializa
      If arrayIndice Is Nothing Then
        ReDim arrayIndice(DIMENSION_MAXIMAS)
        For i = 0 To arrayIndice.Length - 1
          arrayIndice(i) = 0
        Next
      End If

      'calcula el valor del nuevo indice
      largoArray = arrayIndice.Length
      valorIndiceNivel = (arrayIndice(nivel)) + 1
      arrayIndice(nivel) = valorIndiceNivel

      'construye numero indice
      For i = 0 To nivel
        valorIndiceNivel = arrayIndice(i)
        numeroIndice &= CStr(valorIndiceNivel)
        If i < nivel Then numeroIndice &= "."
      Next

      'deja en 0 todos los niveles que le siguen
      For i = (nivel + 1) To largoArray - 1
        arrayIndice(i) = 0
      Next

      'segun el nivel le coloca un signo de separacion
      Select Case nivel
        Case 0 : signoSeparacion = ".- "
        Case Else : signoSeparacion = "  "
      End Select
      numeroIndice &= signoSeparacion
    Catch ex As Exception
      numeroIndice = ex.Message
    End Try
    Return numeroIndice
  End Function

  ''' <summary>
  ''' oculta el contenido de un mail, ejm: vsepulveda@alto.cl -> vsxxxxxx@alto.cl
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function OcultarEmail(ByVal texto As String) As String
    Dim email As String = ""
    Dim aEmail As String()
    Dim username As String

    Try
      If Not String.IsNullOrEmpty(texto) Then
        aEmail = texto.Split("@")
        username = aEmail(0)
        username = username.Substring(0, 2) & "xxxxxxx"
        email = username & "@" & aEmail(1)
      End If
    Catch ex As Exception
      email = ""
    End Try
    Return email
  End Function

  ''' <summary>
  ''' obtiene informacion del navegador
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerInformacionBrowser() As String
    Dim info As String = ""
    Try
      info = HttpContext.Current.Request.Browser.Browser & " " & HttpContext.Current.Request.Browser.Version
    Catch ex As Exception
      info = ""
    End Try
    Return info
  End Function

  Public Shared Sub RegistrarScript(ByVal page As System.Web.UI.Page, ByVal script As String, Optional ByVal eRegistrar As eRegistrar = eRegistrar.FINAL, Optional ByVal nombreRegistro As String = "-1", Optional ByVal esjQuery As Boolean = False)
    Dim dataScript As String = ""
    nombreRegistro = IIf(nombreRegistro.Equals("-1"), "js_" & Herramientas.MyNow().Ticks.ToString(), nombreRegistro)
    dataScript &= "<script type=""text/javascript"">" & vbCrLf
    If (esjQuery) Then dataScript &= "jQuery(document).ready(function () {" & vbCrLf
    dataScript &= script & vbCrLf
    If (esjQuery) Then dataScript &= "});" & vbCrLf
    dataScript &= "</script>" & vbCrLf

    If eRegistrar = Utilidades.eRegistrar.INICIO Then
      page.ClientScript.RegisterClientScriptBlock(page.GetType, nombreRegistro, dataScript)
    Else
      page.ClientScript.RegisterStartupScript(page.GetType, nombreRegistro, dataScript)
    End If
  End Sub

  ''' <summary>
  ''' obtiene mail especiales para el envio del email por empresa
  ''' </summary>
  ''' <param name="idEmpresa"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerListadoMailEstructura(ByVal estructuraMail As String, ByVal idEmpresa As String) As String
    Dim listadoMail, mail As String
    Dim sl As New SortedList
    Dim aEstructura, aMail As String()
    Dim i As Integer

    Try
      If String.IsNullOrEmpty(estructuraMail) Then
        listadoMail = ""
      Else
        'transforma la estructura de mail en un SortedList
        aEstructura = estructuraMail.Split(",")
        For i = 0 To aEstructura.Length - 1
          mail = aEstructura(i)
          mail = mail.Replace("{", "").Replace("}", "")
          aMail = mail.Split(":")
          'asigna valores al arreglo
          sl.Add(aMail(0), aMail(1))
        Next

        'obtiene los mail que siempre van
        listadoMail = Utilidades.IsNull(sl.Item("-1"), "")

        If InStr(estructuraMail, "{" & idEmpresa & ":") > 0 Then
          mail = Utilidades.IsNull(sl.Item(idEmpresa), "")
          listadoMail &= IIf(String.IsNullOrEmpty(listadoMail), mail, ";" & mail)
        End If
        listadoMail = IIf(String.IsNullOrEmpty(listadoMail), "", listadoMail)
      End If
    Catch ex As Exception
      listadoMail = ""
    End Try
    Return listadoMail
  End Function

  ''' <summary>
  ''' elimina la fila del dataset en sesion
  ''' </summary>
  ''' <param name="indice"></param>
  ''' <param name="nombreSesion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function EliminarFilaSesion(ByVal indice As String, ByVal nombreSesion As String, ByVal indiceTabla As Integer) As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim dt As DataTable = Nothing

    Try
      ds = Me.ObtenerObjectSession(nombreSesion)
      If (ds Is Nothing) Then
        totalRegistros = 0
      Else
        totalRegistros = ds.Tables(indiceTabla).Rows.Count
        dt = ds.Tables(indiceTabla)
      End If

      'si hay registros entonces los elimina
      If (totalRegistros > 0) Then
        'obtiene fila del datatable
        dr = dt.Rows(indice)
        'elimina fila del dataset
        dt.Rows.Remove(dr)
        dt.AcceptChanges()
        Me.GrabarObjectSession(nombreSesion, ds)
      End If

    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

  ''' <summary>
  ''' verifica si el registro existe en la sesion
  ''' </summary>
  ''' <param name="indice"></param>
  ''' <param name="ds"></param>
  ''' <param name="rowFilter"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function VerificarDuplicidadSesion(ByVal indice As Integer, ByVal ds As DataSet, ByVal rowFilter As String, ByVal indiceTabla As Integer) As Boolean
    Dim estaDuplicado As Boolean = False
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsCopy As New DataSet

    Try
      dsCopy = ds.Copy
      'elimina la fila que le corresponde al registro para no revisarlo
      If (indice <> -1) Then
        dr = dsCopy.Tables(indiceTabla).Rows(indice)
        dsCopy.Tables(indiceTabla).Rows.Remove(dr)
      End If

      'comprueba si esta duplicado
      dv = dsCopy.Tables(indiceTabla).DefaultView
      dv.RowFilter = rowFilter

      If dv.Count > 0 Then
        estaDuplicado = True
      End If
    Catch ex As Exception
      estaDuplicado = False
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' elimina todos las filas del dataset en sesion
  ''' </summary>
  ''' <param name="nombreSesion"></param>
  ''' <remarks></remarks>
  Public Sub EliminarTodoDataRow(ByVal nombreSesion As String, ByVal indiceTabla As Integer)
    Dim ds As New DataSet
    Dim totalRegistros As Integer
    Dim dr As DataRow

    Try
      ds = Me.ObtenerObjectSession(nombreSesion)
      If (Not ds Is Nothing) Then
        totalRegistros = ds.Tables(indiceTabla).Rows.Count
        If (totalRegistros > 0) Then
          For i = totalRegistros - 1 To 0 Step -1
            'obtiene fila del datatable
            dr = ds.Tables(indiceTabla).Rows(i)
            'elimina fila del dataset
            ds.Tables(indiceTabla).Rows.Remove(dr)
            ds.Tables(indiceTabla).AcceptChanges()
          Next
          Me.GrabarObjectSession(nombreSesion, ds)
        End If
      End If
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' convierte un numero en letras
  ''' http://www.elguille.info/colabora/NET2005/alextaya_numeroaletra.htm
  ''' </summary>
  ''' <param name="numero"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ConvertirNumero2Letras(ByVal numero As String, Optional ByVal formatoTexto As eFormatoConvertirNumero = eFormatoConvertirNumero.Numerico) As String
    '********Declara variables de tipo cadena************
    Dim palabras, entero, dec, flag As String
    Dim literal As String = ""

    '********Declara variables de tipo entero***********
    Dim num, x, y As Integer

    flag = "N"
    entero = ""
    dec = ""
    palabras = ""

    '**********N�mero Negativo***********
    If Mid(numero, 1, 1) = "-" Then
      numero = Mid(numero, 2, numero.ToString.Length - 1).ToString
      palabras = "menos "
    End If

    '**********Si tiene ceros a la izquierda*************
    For x = 1 To numero.ToString.Length
      If Mid(numero, 1, 1) = "0" Then
        numero = Trim(Mid(numero, 2, numero.ToString.Length).ToString)
        If Trim(numero.ToString.Length) = 0 Then palabras = ""
      Else
        Exit For
      End If
    Next

    '*********Dividir parte entera y decimal************
    For y = 1 To Len(numero)
      If Mid(numero, y, 1) = "." Then
        flag = "S"
      Else
        If flag = "N" Then
          entero = entero + Mid(numero, y, 1)
        Else
          dec = dec + Mid(numero, y, 1)
        End If
      End If
    Next y

    If Len(dec) = 1 Then dec = dec & "0"

    '**********proceso de conversi�n***********
    flag = "N"

    If Val(numero) <= 999999999 Then
      For y = Len(entero) To 1 Step -1
        num = Len(entero) - (y - 1)
        Select Case y
          Case 3, 6, 9
            '**********Asigna las palabras para las centenas***********
            Select Case Mid(entero, num, 1)
              Case "1"
                If Mid(entero, num + 1, 1) = "0" And Mid(entero, num + 2, 1) = "0" Then
                  palabras = palabras & "cien "
                Else
                  palabras = palabras & "ciento "
                End If
              Case "2"
                palabras = palabras & "doscientos "
              Case "3"
                palabras = palabras & "trescientos "
              Case "4"
                palabras = palabras & "cuatrocientos "
              Case "5"
                palabras = palabras & "quinientos "
              Case "6"
                palabras = palabras & "seiscientos "
              Case "7"
                palabras = palabras & "setecientos "
              Case "8"
                palabras = palabras & "ochocientos "
              Case "9"
                palabras = palabras & "novecientos "
            End Select
          Case 2, 5, 8
            '*********Asigna las palabras para las decenas************
            Select Case Mid(entero, num, 1)
              Case "1"
                If Mid(entero, num + 1, 1) = "0" Then
                  flag = "S"
                  palabras = palabras & "diez "
                End If
                If Mid(entero, num + 1, 1) = "1" Then
                  flag = "S"
                  palabras = palabras & "once "
                End If
                If Mid(entero, num + 1, 1) = "2" Then
                  flag = "S"
                  palabras = palabras & "doce "
                End If
                If Mid(entero, num + 1, 1) = "3" Then
                  flag = "S"
                  palabras = palabras & "trece "
                End If
                If Mid(entero, num + 1, 1) = "4" Then
                  flag = "S"
                  palabras = palabras & "catorce "
                End If
                If Mid(entero, num + 1, 1) = "5" Then
                  flag = "S"
                  palabras = palabras & "quince "
                End If
                If Mid(entero, num + 1, 1) > "5" Then
                  flag = "N"
                  palabras = palabras & "dieci"
                End If
              Case "2"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "veinte "
                  flag = "S"
                Else
                  palabras = palabras & "veinti"
                  flag = "N"
                End If
              Case "3"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "treinta "
                  flag = "S"
                Else
                  palabras = palabras & "treinta y "
                  flag = "N"
                End If
              Case "4"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "cuarenta "
                  flag = "S"
                Else
                  palabras = palabras & "cuarenta y "
                  flag = "N"
                End If
              Case "5"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "cincuenta "
                  flag = "S"
                Else
                  palabras = palabras & "cincuenta y "
                  flag = "N"
                End If
              Case "6"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "sesenta "
                  flag = "S"
                Else
                  palabras = palabras & "sesenta y "
                  flag = "N"
                End If
              Case "7"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "setenta "
                  flag = "S"
                Else
                  palabras = palabras & "setenta y "
                  flag = "N"
                End If
              Case "8"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "ochenta "
                  flag = "S"
                Else
                  palabras = palabras & "ochenta y "
                  flag = "N"
                End If
              Case "9"
                If Mid(entero, num + 1, 1) = "0" Then
                  palabras = palabras & "noventa "
                  flag = "S"
                Else
                  palabras = palabras & "noventa y "
                  flag = "N"
                End If
            End Select
          Case 1, 4, 7
            '*********Asigna las palabras para las unidades*********
            Select Case Mid(entero, num, 1)
              Case "1"
                If flag = "N" Then
                  If y = 1 And formatoTexto = eFormatoConvertirNumero.Numerico Then
                    palabras = palabras & "uno "
                  Else
                    palabras = palabras & "un(a) "
                  End If
                End If
              Case "2"
                If flag = "N" Then palabras = palabras & "dos "
              Case "3"
                If flag = "N" Then palabras = palabras & "tres "
              Case "4"
                If flag = "N" Then palabras = palabras & "cuatro "
              Case "5"
                If flag = "N" Then palabras = palabras & "cinco "
              Case "6"
                If flag = "N" Then palabras = palabras & "seis "
              Case "7"
                If flag = "N" Then palabras = palabras & "siete "
              Case "8"
                If flag = "N" Then palabras = palabras & "ocho "
              Case "9"
                If flag = "N" Then palabras = palabras & "nueve "
            End Select
        End Select

        '***********Asigna la palabra mil***************
        If y = 4 Then
          If Mid(entero, 6, 1) <> "0" Or Mid(entero, 5, 1) <> "0" Or Mid(entero, 4, 1) <> "0" Or _
          (Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
          Len(entero) <= 6) Then palabras = palabras & "mil "
        End If

        '**********Asigna la palabra mill�n*************
        If y = 7 Then
          If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
            palabras = palabras & "mill�n "
          Else
            palabras = palabras & "millones "
          End If
        End If
      Next y

      '**********Une la parte entera y la parte decimal*************
      If dec <> "" Then
        literal = palabras & "con " & dec
      Else
        literal = palabras
      End If
    Else
      literal = ""
    End If

    Return literal
  End Function

  ''' <summary>
  ''' separa un rut en parte numerica y digito verificador
  ''' </summary>
  ''' <param name="rut"></param>
  ''' <param name="dv"></param>
  ''' <remarks></remarks>
  Public Shared Sub SepararRut(ByRef rut As String, ByRef dv As String)
    Dim esValido As Boolean = False
    Dim parteNumerica, rutCompleto, dvVerificar As String

    Try
      If EsRut(rut & dv, 2, 20) Then
        'le quita al rut los espacios en blanco, puntos y guion
        rutCompleto = rut & dv
        rutCompleto = rutCompleto.Replace(" ", "").Replace(".", "").Replace("-", "")

        'separo el ultimo digito del resto asumiendo que es el Digito Verificador
        'Si el �ltimo digito no es un numero, imagino que es una k, la transformo en mayuscula
        dvVerificar = Right(rutCompleto, 1).ToUpper
        'obtiene la parte numerica del rut
        parteNumerica = rutCompleto.Substring(0, rutCompleto.Length - 1)
        'verifica si el DV es el que corresponde
        esValido = IIf(CalcularDigitoVerificador(CLng(parteNumerica)) = dvVerificar, True, False)

        If esValido Then
          rut = parteNumerica
          dv = dvVerificar
        End If
      End If
    Catch ex As Exception
      'no hace nada
    End Try
  End Sub

  ''' <summary>
  ''' determina si tiene permiso sobre la funcion de la pagina consultada
  ''' </summary>
  ''' <param name="nombrePagina"></param>
  ''' <param name="nombreFuncion"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 30/07/2009</remarks>
  Public Shared Function TienePermiso(ByVal nombrePagina As String, ByVal nombreFuncion As String) As Boolean
    Dim oUtilidades As New Utilidades
    Dim existePermiso As Boolean = False
    Dim oUsuario As Usuario
    Dim ds As New DataSet
    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      ds = oUtilidades.ObtenerDataSetSession(Usuario.SESION_MATRIZ_PERMISO)

      If oUsuario.Super Then
        existePermiso = True
      Else
        If Not ds Is Nothing Then
          Dim filas() As DataRow = ds.Tables(Usuario.eTablaPermiso.T02_ListadoFunciones).Select("NombreArchivo='" & nombrePagina & "' AND LlaveIdentificador='" & nombreFuncion & "'")
          Dim totalFilas As Integer = filas.Length
          existePermiso = IIf(totalFilas > 0, True, False)
        End If
      End If
    Catch ex As Exception
      existePermiso = False
    End Try
    Return existePermiso
  End Function

  ''' <summary>
  ''' define el estilo de la alerta segun la prioridad
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerStyleAlertaPorPrioridad(ByVal prioridad As String) As String
    Dim tipoAlerta As String

    Try
      tipoAlerta = Herramientas.ObtenerStyleAlertaPorPrioridad(prioridad)
    Catch ex As Exception
      tipoAlerta = "success"
    End Try

    Return tipoAlerta
  End Function

  ''' <summary>
  ''' dibuja el menu principal
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 31/07/2009</remarks>
  Public Function DibujarMenu(ByVal ds As DataSet) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim html As String = ""
    Dim totalRegistros, totalPaginasEnMenu As Integer
    Dim dt As DataTable
    Dim sb As New StringBuilder
    Dim templateMenu As String = ""
    Dim idCategoria, nombreCategoria, listadoPaginas, templateMenuAxu As String
    Dim paginaInicio As String = Session("PaginaDeInicio")

    sb.AppendLine("<ul class=""nav navbar-nav"">")

    Try
      templateMenu &= "<li class=""dropdown"">" & _
                      "  <a href=""#"" class=""dropdown-toggle"" data-toggle=""dropdown"">{NOMBRE_CATEGORIA} <b class=""caret""></b></a>" & _
                      "  {LISTADO_PAGINAS}" & _
                      "</li>"

      If Not ds Is Nothing Then
        dt = ds.Tables(Usuario.eTablaPermiso.T00_ListadoCategorias)
        totalRegistros = dt.Rows.Count

        If totalRegistros > 0 Then
          'recorre las categorias para ir dibujando su contenido
          For Each dr As DataRow In dt.Rows
            templateMenuAxu = templateMenu
            idCategoria = dr.Item("IdCategoria")
            nombreCategoria = dr.Item("Categoria")
            totalPaginasEnMenu = dr.Item("TotalPaginasEnMenu")

            If (totalPaginasEnMenu > 0) Then
              listadoPaginas = DibujarPaginas(ds, idCategoria)

              'reemplaza marcas especiales
              templateMenuAxu = templateMenuAxu.Replace("{NOMBRE_CATEGORIA}", nombreCategoria)
              templateMenuAxu = templateMenuAxu.Replace("{LISTADO_PAGINAS}", listadoPaginas)
              sb.AppendLine(templateMenuAxu)
            End If
          Next

        End If
      End If

    Catch ex As Exception
      'no hace nada
    End Try

    sb.AppendLine("</ul>")
    sb.AppendLine("<ul class=""nav navbar-nav navbar-right"">")

    sb.AppendLine("  <li><a href=""../" & paginaInicio & """>Inicio</a></li>")
    sb.AppendLine("  <li class=""dropdown"">")
    sb.AppendLine("    <a href=""#"" class=""dropdown-toggle"" data-toggle=""dropdown"">" & oUsuario.NombreCompleto & " <b class=""caret""></b></a>")
    sb.AppendLine("    <ul class=""dropdown-menu"">")
    sb.AppendLine("      <li><a href=""#"" onclick=""Sistema.cambiarClave({ passwordCaduco: '0' })"">Cambiar clave</a></li>")
    sb.AppendLine("      <li><a href=""#"" onclick=""Sistema.validarLogout({ llavePerfil:'" & oUsuario.PerfilLlave & "' })"">Cerrar Sesi&oacute;n</a></li>")
    sb.AppendLine("      <li class=""divider""></li>")
    sb.AppendLine("      <li class=""text-center""><a href=""#""><small><em><strong>Perfil</strong><br />" & oUsuario.PerfilNombre & "</em></small></a></li>")
    sb.AppendLine("    </ul>")
    sb.AppendLine("  </li>")
    sb.AppendLine("</ul>")

    'retorna valor
    html = sb.ToString

    Return html
  End Function

  ''' <summary>
  ''' dibuja las categorias con sus paginas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarPaginas(ByVal ds As DataSet, ByVal idCategoria As String) As String
    Dim html As String = ""
    Dim templateMenu As String = ""
    Dim templatePagina As String = ""
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dv As DataView
    Dim sb As New StringBuilder
    Dim dsPaginas As New DataSet
    Dim tituloPagina, nombreArchivo, target, templatePaginaAux As String
    Dim mostrarEnMenu As Boolean

    Try
      templateMenu &= "<ul class=""dropdown-menu"">" & _
                      "  {LISTADO_PAGINAS}" & _
                      "</ul>"

      templatePagina = "<li><a href=""../page/{NOMBRE_ARCHIVO}"" target=""{TARGET}"">{TITULO_PAGINA}</a></li>"

      'obtiene listado de paginas
      dt = ds.Tables(Usuario.eTablaPermiso.T01_ListadoPaginas)
      If Not dt Is Nothing Then
        'filtra la vista por la categoria
        dv = dt.DefaultView
        dv.RowFilter = "IdCategoria = " & idCategoria
        'convierte la vista en dataset
        dsPaginas = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""
        totalRegistros = dsPaginas.Tables(0).Rows.Count

        If totalRegistros > 0 Then
          For Each dr As DataRow In dsPaginas.Tables(0).Rows
            templatePaginaAux = templatePagina
            tituloPagina = dr.Item("TituloPagina")
            nombreArchivo = dr.Item("NombreArchivo")
            mostrarEnMenu = dr.Item("MostrarEnMenu")
            target = IIf(nombreArchivo.StartsWith("http"), "_blank", "_top")

            If Utilidades.TienePermiso(nombreArchivo, "Ver") And mostrarEnMenu Then
              'reemplaza las marcas en el template de la pagina
              templatePaginaAux = templatePaginaAux.Replace("{NOMBRE_ARCHIVO}", nombreArchivo)
              templatePaginaAux = templatePaginaAux.Replace("{TARGET}", target)
              templatePaginaAux = templatePaginaAux.Replace("{TITULO_PAGINA}", tituloPagina)
              sb.AppendLine(templatePaginaAux)
            End If
          Next

          html = templateMenu.Replace("{LISTADO_PAGINAS}", sb.ToString())
        End If
      End If
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' obtiene el tipo de label segun la prioridad
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerLabelPrioridad(ByVal prioridad As String) As String
    Dim html As String = ""
    Dim template, tipoAlerta As String

    Try
      template = "<span class=""alert alert-{TIPO_ALERTA} sist-padding-label-prioridad sist-margin-cero""><strong>{PRIORIDAD}</strong></span>"
      tipoAlerta = ObtenerStyleAlertaPorPrioridad(prioridad)

      template = template.Replace("{TIPO_ALERTA}", tipoAlerta)
      template = template.Replace("{PRIORIDAD}", prioridad)
      html = template
    Catch ex As Exception
      html = prioridad
    End Try

    Return html
  End Function

  ''' <summary>
  ''' obtiene dataset para dibujar combo de grupos especiales
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDatasetComboGrupoContactoEspecial(ByRef mostrarComboItemSeleccione As Boolean, ByVal dt As DataTable, ByVal agrupadoEn As String, ByVal rutConductor As String, _
                                                                  ByVal localDestinoCodigo As String, ByVal rutTransportista As String, ByVal idEscalamientoPorAlertaGrupoContacto As String, _
                                                                  ByVal nombreConductor As String, ByVal nombreTransportista As String, ByVal llavePerfil As String, ByVal tipoGrupo As String, _
                                                                  ByVal localOrigenCodigo As String) As DataSet
    Dim ds As New DataSet
    Dim totalRegistros As Integer
    Dim dv As DataView

    Try
      Select Case tipoGrupo
        Case "MostrarSiempre"
          Select Case agrupadoEn.ToUpper()
            Case Alerta.GRUPO_CONTACTO_CONDUCTOR
              ds = Alerta.ObtenerConductores(rutConductor)
              ds = Utilidades.FiltrarDatasetComboGrupoContactoEspecial(ds, nombreConductor)
            Case Alerta.GRUPO_CONTACTO_TRANSPORTISTA
              Utilidades.SepararRut(rutTransportista, "")
              ds = Alerta.ObtenerTransportistas(nombreTransportista)
            Case Else
              If (llavePerfil = Perfil.KEY_COORDINADOR_LINEA_TRANSPORTE) Then
                ds = Alerta.ObtenerCoordinadorLineaTransporte(rutTransportista)
                ds = Utilidades.FiltrarDatasetComboGrupoContactoEspecial(ds, nombreTransportista)
              End If
          End Select
        Case CentroDistribucion.NombreEntidad
          ds = Alerta.ObtenerCargoPorCentroDistribucionyPerfil(localOrigenCodigo, llavePerfil)
        Case Local.NombreEntidad
          ds = Alerta.ObtenerCargoPorLocalyPerfil(localDestinoCodigo, llavePerfil)
        Case Else
          dv = dt.DefaultView
          dv.RowFilter = "idEscalamientoPorAlertaGrupoContacto = " & idEscalamientoPorAlertaGrupoContacto
          ds = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""
      End Select

      Select Case tipoGrupo
        Case Alerta.TIPO_GRUPO_PERSONALIZADO
          mostrarComboItemSeleccione = False
        Case Else
          totalRegistros = ds.Tables(0).Rows.Count
          mostrarComboItemSeleccione = IIf(totalRegistros > 1, True, False)
      End Select

    Catch ex As Exception
      ds = Nothing
      mostrarComboItemSeleccione = True
    End Try

    Return ds
  End Function

  ''' <summary>
  ''' filtra el dataset por la cantidad de palabras que contenga el nombre
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function FiltrarDatasetComboGrupoContactoEspecial(ByVal ds As DataSet, ByVal nombre As String)
    Dim totalRegistros As Integer
    Dim dsFiltrado As New DataSet
    Dim aNombre As String()
    Dim texto As String
    Dim sb As New StringBuilder
    Dim dv As DataView

    Try
      totalRegistros = ds.Tables(0).Rows.Count
      If (totalRegistros = 1) Then
        dsFiltrado = ds
      Else
        aNombre = nombre.Split(" ")
        For i = 0 To aNombre.Count - 1
          texto = aNombre(i)
          sb.Append("NombreUsuario LIKE '%" & texto.Trim & "%'")
          If i < aNombre.Count - 1 Then sb.Append(" OR ")
        Next

        dv = ds.Tables(0).DefaultView
        dv.RowFilter = sb.ToString()
        dsFiltrado = CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        'revisa si el filtro devuelve registros, si no trae nada entonces devuelve todo nuevamente
        totalRegistros = dsFiltrado.Tables(0).Rows.Count
        If (totalRegistros = 0) Then
          dsFiltrado = ds
        End If

      End If
    Catch ex As Exception
      dsFiltrado = ds
    End Try

    Return dsFiltrado
  End Function

  ''' <summary>
  ''' verifica si el grupo consultado es especial
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EsGrupoContactoEspecial(ByVal tipoGrupo As String) As Boolean
    Dim esGrupoEspecial As Boolean

    Try
      esGrupoEspecial = IIf(tipoGrupo = Alerta.TIPO_GRUPO_PERSONALIZADO, False, True)
    Catch ex As Exception
      esGrupoEspecial = False
    End Try

    Return esGrupoEspecial
  End Function

  ''' <summary>
  ''' verifica si en el escalamiento anterior (que es el actual que tiene asignada la alerta) hubo una NO CONTACTABILIDAD
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function VerificaEscalamientoAnteriorNoContesta(ByVal ds As DataSet, ByVal nroEscalamientoActual As String, ByVal nombreAlerta As String) As Boolean
    Dim idHistorialEscalamiento, agrupadoEn, tipoExplicacion As String
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim dsFiltrado As New DataSet
    Dim noContesta As Boolean = False
    Dim totalContactos As Integer
    Dim totalNoContesta As Integer = 0

    Try
      'busca el historial asociado al escalamiento actual
      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      dv = dt.DefaultView
      dv.RowFilter = "NroEscalamiento = " & nroEscalamientoActual
      If (dv.Count = 0) Then
        idHistorialEscalamiento = "-1"
      Else
        idHistorialEscalamiento = dv.Item(0).Item("IdHistorialEscalamiento")
      End If
      dv.RowFilter = ""

      '----------------------------------------------------------------------
      'busca los contactos registrados en el historial encontrado
      dt = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos)
      dv = dt.DefaultView
      dv.RowFilter = "IdHistorialEscalamiento = " & idHistorialEscalamiento
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""
      totalContactos = dsFiltrado.Tables(0).Rows.Count

      For Each dr In dsFiltrado.Tables(0).Rows
        agrupadoEn = dr.Item("AgrupadoEn")
        tipoExplicacion = dr.Item("TipoExplicacion")

        'si el contacto pertenece al grupo "CONDUCTOR" o "TRANSPORTISTA" entonces lo excluye de los NO CONTESTA para avanzar al siguiente escalamiento cuando la alerta sea diferente a MULTIPUNTO
        If (Not nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO) And (agrupadoEn.ToUpper() = Alerta.GRUPO_CONTACTO_CONDUCTOR Or agrupadoEn.ToUpper() = Alerta.GRUPO_CONTACTO_TRANSPORTISTA)) Then
          totalContactos -= 1
        End If

        If (tipoExplicacion.ToUpper() = Alerta.NO_CONTESTA) Then
          'cuenta los NO CONTESTA cuando la alerta es MULTIPUNTO o cuando es el grupo es distinto a CONDUCTOR Y TRANSPORTISTA
          If (nombreAlerta.ToUpper().Contains(Alerta.MULTIPUNTO)) Or (agrupadoEn.ToUpper() <> Alerta.GRUPO_CONTACTO_CONDUCTOR And agrupadoEn.ToUpper() <> Alerta.GRUPO_CONTACTO_TRANSPORTISTA) Then
            totalNoContesta += 1
          End If
        End If
      Next

      'si los contactos del escalamiento anterior no contesto ninguno, entonces devuelve el listado
      If (totalContactos > 0) Then
        If (totalContactos = totalNoContesta) Then
          noContesta = True
        End If
      End If
    Catch ex As Exception
      noContesta = False
    End Try

    Return noContesta
  End Function

  ''' <summary>
  ''' obtiene informacion del ultimo contacto registrado en el escalamiento anterior
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerInformacionUltimoContactoEscalamientoAnterior(ByVal ds As DataSet, ByVal nroEscalamientoActual As String) As SortedList
    Dim sl As New SortedList
    Dim idHistorialEscalamiento As String
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim dsFiltrado As New DataSet
    Dim totalContactos As Integer
    Dim totalNoContesta As Integer = 0

    Try
      'busca el historial asociado al escalamiento actual
      dt = ds.Tables(Alerta.eTabla.T01_HistorialEscalamientos)
      dv = dt.DefaultView
      dv.RowFilter = "NroEscalamiento = " & nroEscalamientoActual
      If (dv.Count = 0) Then
        idHistorialEscalamiento = "-1"
      Else
        idHistorialEscalamiento = dv.Item(0).Item("IdHistorialEscalamiento")
      End If
      dv.RowFilter = ""

      '----------------------------------------------------------------------
      'busca los contactos registrados en el historial encontrado, que no sean ni: CONDUCTOR, TRANSPORTISTA
      dt = ds.Tables(Alerta.eTabla.T02_HistorialEscalamientosContactos)
      dv = dt.DefaultView
      dv.RowFilter = "IdHistorialEscalamiento = " & idHistorialEscalamiento
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""
      totalContactos = dsFiltrado.Tables(0).Rows.Count

      'obtiene el ultimo registro del dataset y lo devuelve en un SortedList
      If (totalContactos > 0) Then
        dt = dsFiltrado.Tables(0)
        dr = dt.Rows(totalContactos - 1)

        For Each col As DataColumn In dt.Columns
          sl.Add(col.ColumnName, dr(col))
        Next

      End If

    Catch ex As Exception
      sl = New SortedList
    End Try

    Return sl
  End Function

  ''' <summary>
  ''' obtiene informacion de la alerta y lo guarda en una lista
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerInformacionAlerta(ByVal ds As DataSet) As SortedList
    Dim sl As New SortedList
    Dim dt As DataTable
    Dim dr As DataRow

    Try
      dt = ds.Tables(Alerta.eTabla.T00_Detalle)
      dr = dt.Rows(0)

      For Each col As DataColumn In dt.Columns
        sl.Add(col.ColumnName, dr(col))
      Next

    Catch ex As Exception
      sl = New SortedList
    End Try

    Return sl
  End Function

  ''' <summary>
  ''' cambia marcas especiales que tiene el script
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReemplazarMarcasScript(ByVal descripcion As String, ByVal slInformacionAlerta As SortedList, ByVal slInformacionUltimoContactoEscalamientoAnterior As SortedList) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim nuevaDescripcion, nombreConductor, nombreTransportista, patenteTracto, patenteTrailer, patente, numeroLocal, nombreUltimoContacto, nroTransporte As String
    Dim nombreAlerta, tiempoDetencion, horas, minutos, horaHoy, buenosDias, cargoUltimoContacto, tipoViaje, motivoRechazo As String
    Dim fechaHoy As Date = Herramientas.MyNow()
    Dim dsMotivoRechazo As New DataSet
    Dim sbMotivoRechazo As New StringBuilder
    Dim listadoMotivoRechazo As String = ""

    descripcion = Server.HtmlEncode(descripcion)

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      horaHoy = fechaHoy.ToString("HHmm")

      'obtiene valores
      nombreConductor = slInformacionAlerta.Item("NombreConductor")
      nombreTransportista = slInformacionAlerta.Item("NombreTransportista")
      patenteTracto = slInformacionAlerta.Item("PatenteTracto")
      patenteTrailer = slInformacionAlerta.Item("PatenteTrailer")
      numeroLocal = slInformacionAlerta.Item("LocalDestinoCodigo")
      nroTransporte = slInformacionAlerta.Item("NroTransporte")
      nombreAlerta = slInformacionAlerta.Item("NombreAlerta")
      tiempoDetencion = slInformacionAlerta.Item("FrecuenciaRepeticionMinutos")
      tipoViaje = slInformacionAlerta.Item("TipoViaje")

      nombreUltimoContacto = slInformacionUltimoContactoEscalamientoAnterior.Item("NombreUsuario")
      cargoUltimoContacto = slInformacionUltimoContactoEscalamientoAnterior.Item("Cargo")
      If (String.IsNullOrEmpty(nombreUltimoContacto)) Then nombreUltimoContacto = ""
      If (String.IsNullOrEmpty(cargoUltimoContacto)) Then cargoUltimoContacto = ""

      If (String.IsNullOrEmpty(patenteTrailer)) Then
        patente = "tracto " & Me.DestacarTextoNegritaHTML(patenteTracto)
      Else
        patente = "tracto " & Me.DestacarTextoNegritaHTML(patenteTracto) & ", remolque " & Me.DestacarTextoNegritaHTML(patenteTrailer)
      End If

      If (tiempoDetencion < 60) Then
        tiempoDetencion = tiempoDetencion & " min."
      Else
        horas = tiempoDetencion \ 60
        minutos = tiempoDetencion Mod 60
        tiempoDetencion = horas & " hr."
        tiempoDetencion &= IIf(minutos = 0, "", " y " & minutos & " min.")
      End If
      tiempoDetencion = "<strong><em>" & tiempoDetencion & "</em></strong>"

      If (horaHoy >= "0000" And horaHoy <= "1159") Then
        buenosDias = "Buenos d�as"
      ElseIf (horaHoy >= "1200" And horaHoy <= "1959") Then
        buenosDias = "Buenas tardes"
      Else
        buenosDias = "Buenas noches"
      End If

      'si la alerta es RECEPCION RECHAZADA entonces obtiene los motivos del rechazo
      If (descripcion.Contains("[MOTIVO_RECHAZO_RECEPCION]")) Then
        dsMotivoRechazo = Alerta.ObtenerMotivoRecepcionRechazada(nroTransporte)
        If (dsMotivoRechazo Is Nothing) Then
          listadoMotivoRechazo = ""
        Else
          If (dsMotivoRechazo.Tables(0).Rows.Count = 0) Then
            listadoMotivoRechazo = ""
          Else
            sbMotivoRechazo.Append("<div>")
            sbMotivoRechazo.Append("  <div><strong>Motivos del rechazo</strong></div>")
            sbMotivoRechazo.Append("  <div>")
            sbMotivoRechazo.Append("    <ul>")

            For Each dr As DataRow In dsMotivoRechazo.Tables(0).Rows
              motivoRechazo = dr.Item("Motivo")
              sbMotivoRechazo.Append("<li>" & motivoRechazo & "</li>")
            Next

            sbMotivoRechazo.Append("    </ul>")
            sbMotivoRechazo.Append("  </div>")
            sbMotivoRechazo.Append("</div>")
            listadoMotivoRechazo = sbMotivoRechazo.ToString()
          End If
        End If
      End If

      'reemplaza marcas
      descripcion = descripcion.Replace("[NOMBRE_CONDUCTOR]", Me.DestacarTextoNegritaHTML(nombreConductor))
      descripcion = descripcion.Replace("[NOMBRE_TRANSPORTISTA]", Me.DestacarTextoNegritaHTML(nombreTransportista))
      descripcion = descripcion.Replace("[LINEA_TRANSPORTISTA]", Me.DestacarTextoNegritaHTML(nombreTransportista))
      descripcion = descripcion.Replace("[PATENTE]", patente)
      descripcion = descripcion.Replace("[PATENTE_TRACTO]", Me.DestacarTextoNegritaHTML(patenteTracto))
      descripcion = descripcion.Replace("[NUMERO_LOCAL]", Me.DestacarTextoNegritaHTML(numeroLocal))
      descripcion = descripcion.Replace("[TIENDA]", Me.DestacarTextoNegritaHTML(numeroLocal))
      descripcion = descripcion.Replace("[NOMBRE_ULTIMO_CONTACTO]", Me.DestacarTextoNegritaHTML(nombreUltimoContacto))
      descripcion = descripcion.Replace("[CARGO_ULTIMO_CONTACTO]", Me.DestacarTextoNegritaHTML(cargoUltimoContacto))
      descripcion = descripcion.Replace("[NRO_TRANSPORTE]", Me.DestacarTextoNegritaHTML(nroTransporte))
      descripcion = descripcion.Replace("[MOTIVO_DETENCION]", Me.DestacarTextoNegritaHTML(nombreAlerta))
      descripcion = descripcion.Replace("[DESCRIPCION_ALERTA]", Me.DestacarTextoNegritaHTML(nombreAlerta))
      descripcion = descripcion.Replace("[TIEMPO_DETENCION]", tiempoDetencion)
      descripcion = descripcion.Replace("[BUENOS_DIAS]", buenosDias)
      descripcion = descripcion.Replace("[USUARIO_CONECTADO]", Me.DestacarTextoNegritaHTML(oUsuario.NombreCompleto))
      descripcion = descripcion.Replace("[TIPO_VIAJE]", Me.DestacarTextoNegritaHTML(tipoViaje))
      descripcion = descripcion.Replace("[MOTIVO_RECHAZO_RECEPCION]", listadoMotivoRechazo)

      nuevaDescripcion = descripcion
    Catch ex As Exception
      nuevaDescripcion = descripcion
    End Try

    Return nuevaDescripcion
  End Function

  ''' <summary>
  ''' destaca texto en negrita para mostrarlo en html
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function DestacarTextoNegritaHTML(ByVal texto As String) As String
    Dim nuevoTexto As String

    Try
      If (String.IsNullOrEmpty(texto)) Then
        nuevoTexto = ""
      Else
        texto = texto.ToUpper()
        nuevoTexto = "<strong><em>" & Server.HtmlEncode(texto) & "</em></strong>"
      End If
    Catch ex As Exception
      nuevoTexto = texto
    End Try

    Return nuevoTexto
  End Function

  ''' <summary>
  ''' muestra el estado de la llamada con un estilo definido
  ''' </summary>
  ''' <param name="accion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function MensajeLlamadaPorAccion(ByVal accion As String) As String
    Dim html As String

    Try
      Select Case accion.ToUpper()
        Case "-1"
          html = "<div class=""alert alert-info sist-margin-cero sist-padding-5""><span class=""glyphicon glyphicon-hand-up""></span>&nbsp;INICIANDO LLAMADA</div>"
        Case Alerta.CALLCENTER_ESTADO_LLAMADA_LLAMANDO
          html = "<div class=""alert alert-info sist-margin-cero sist-padding-5""><span class=""glyphicon glyphicon-phone-alt""></span>&nbsp;" & accion & "</div>"
        Case Alerta.CALLCENTER_ESTADO_LLAMADA_HABLANDO
          html = "<div class=""alert alert-success sist-margin-cero sist-padding-5""><span class=""glyphicon glyphicon-earphone""></span>&nbsp;<span class=""glyphicon glyphicon-user""></span>&nbsp;" & accion & "</div>"
        Case Alerta.CALLCENTER_ESTADO_LLAMADA_FINALIZADO, ""
          html = "<div class=""alert alert-danger sist-margin-cero sist-padding-5""><span class=""glyphicon glyphicon-remove""></span>&nbsp;" & accion & "</div>"
        Case Else
          html = "<div class=""alert alert-danger sist-margin-cero sist-padding-5""><span class=""glyphicon glyphicon-question-sign""></span>&nbsp;DESCONOCIDO</span></div>"
      End Select
    Catch ex As Exception
      html = accion
    End Try

    Return html
  End Function

  ''' <summary>
  ''' dibuja estilo de etiqueta segun el permiso
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerLabelPermisoZona(ByVal permiso As String) As String
    Dim html, template, tipoAlerta As String

    Try
      template = "<span class=""alert alert-{TIPO_ALERTA} sist-padding-label-prioridad sist-margin-cero""><strong>ZONA {PERMISO}</strong></span>"
      If (String.IsNullOrEmpty(permiso)) Then permiso = ""

      tipoAlerta = IIf(permiso.ToUpper() = "AUTORIZADA", "success", "danger")
      template = template.Replace("{TIPO_ALERTA}", tipoAlerta)
      template = template.Replace("{PERMISO}", permiso)
      html = template
    Catch ex As Exception
      html = permiso
    End Try

    Return html
  End Function

  ''' <summary>
  ''' construye mensaje en la pantalla
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Function MostrarMensajeUsuario(ByVal textoMensaje As String, ByVal tipoMensaje As eTipoMensajeAlert, Optional ByVal mostrarBotonCerrar As Boolean = False, Optional alineacionTexto As String = "text-center") As String
    Dim template As String = "<div class=""alert alert-{TIPO} {DISMISSABLE} {ALINEACION_TEXTO}"">{BOTON_CERRAR}{MENSAJE}</div>"
    Dim tipo As String = tipoMensaje.ToString().ToLower
    Dim dismissable As String = IIf(mostrarBotonCerrar, "alert-dismissable", "")
    Dim botonCerrar As String = IIf(mostrarBotonCerrar, "<button type=""button"" class=""close"" data-dismiss=""alert"" aria-hidden=""true"">&times;</button>", "")

    template = template.Replace("{TIPO}", tipo)
    template = template.Replace("{DISMISSABLE}", dismissable)
    template = template.Replace("{BOTON_CERRAR}", botonCerrar)
    template = template.Replace("{MENSAJE}", textoMensaje)
    template = template.Replace("{ALINEACION_TEXTO}", alineacionTexto)

    Return template
  End Function

  ''' <summary>
  ''' convierte datatable en tabla html
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ConvertirDatatableToHTML(ByVal dt As DataTable) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim valor, textAlign As String

    Try
      textAlign = IIf(dt.Columns.Count <= 8, "text-left", "text-center")

      sb.Append("<table id=""tblPrincipal"" class=""table table-hover table-condensed"">")
      sb.Append("  <thead>")
      sb.Append("    <tr>")
      For Each col As DataColumn In dt.Columns
        sb.Append("<th class=""sist-nowrap " & textAlign & """>" & Server.HtmlEncode(col.ColumnName) & "</th>")
      Next
      sb.Append("    </tr>")
      sb.Append("  </thead>")

      sb.Append("  <tbody>")
      For Each dr As DataRow In dt.Rows
        sb.Append("<tr>")
        For Each col As DataColumn In dt.Columns
          valor = Utilidades.IsNull(dr.Item(col.ColumnName), "")
          sb.Append("<td class=""sist-nowrap " & textAlign & """>" & Server.HtmlEncode(valor) & "</td>")
        Next
        sb.Append("</tr>")
      Next
      sb.Append("  </tbody>")

      sb.Append("</table>")
      html = sb.ToString()
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' Retorna la version del sistema definido en web.config
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function VersionSistema() As String
    Dim texto As String
    Try
      texto = AppSettings("version").ToString
    Catch ex As Exception
      texto = ""
    End Try
    Return texto
  End Function

  ''' <summary>
  ''' detiene un proceso por N milisegundos
  ''' </summary>
  ''' <param name="milisegundos"></param>
  ''' <remarks></remarks>
  Public Shared Sub Delay(ByVal milisegundos As Integer)
    Try
      System.Threading.Thread.Sleep(milisegundos)
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  ''' <summary>
  ''' obtiene el ancho maximo que debe tener una thumbnail
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AnchoMaximoThumbnail() As String
    Dim valor As String = AppSettings("anchoMaximoThumbnail").ToString
    Return valor
  End Function

  ''' <summary>
  ''' obtiene el ancho maximo que debe tener una thumbnail
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AltoMaximoThumbnail() As String
    Dim valor As String = AppSettings("altoMaximoThumbnail").ToString
    Return valor
  End Function

  ''' <summary>
  ''' obtiene los valores de un listbox separados por coma
  ''' </summary>
  ''' <param name="list"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerValoresListBox(ByVal list As ListBox) As String
    Dim valores As String
    Dim lstValores As New List(Of String)

    Try
      For Each item As ListItem In list.Items
        If (item.Selected) Then
          lstValores.Add(item.Value)
        End If
      Next

      valores = String.Join(",", lstValores.ToArray())
    Catch ex As Exception
      valores = ""
    End Try

    Return valores
  End Function

  ''' <summary>
  ''' dibuja los canales de comunicacion y marca los que estan seleccionados
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function DibujarCanalComunicacion(ByVal ds As DataSet, ByVal indiceTabla As Integer, ByVal canalComunicacion As String, ByVal prefijoControl As String) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim dt As DataTable
    Dim totalRegistros As Integer
    Dim templateAux, nombre, valor, checked As String
    Dim sb As New StringBuilder

    Try
      template &= "<div class=""col-xs-3"">"
      template &= "  <div class=""checkbox"">"
      template &= "    <label>"
      template &= "      <input type=""checkbox"" id=""{PREFIJO_CONTROL}_chk{NOMBRE_CONTROL}"" value=""{VALOR}"" data-tipo=""{PREFIJO_CONTROL}-canal-comunicacion"" {CHECKED}> {NOMBRE}"
      template &= "    </label>"
      template &= "  </div>"
      template &= "</div>"

      dt = ds.Tables(indiceTabla)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        html = ""
      Else
        sb.Append("<div class=""row"">")
        For Each dr As DataRow In dt.Rows
          templateAux = template
          nombre = dr.Item("Nombre")
          valor = dr.Item("Valor")
          checked = IIf(canalComunicacion.Contains("(" & valor & ")"), "checked", "")

          templateAux = templateAux.Replace("{PREFIJO_CONTROL}", prefijoControl)
          templateAux = templateAux.Replace("{NOMBRE}", Server.HtmlEncode(nombre))
          templateAux = templateAux.Replace("{NOMBRE_CONTROL}", nombre.Replace(" ", ""))
          templateAux = templateAux.Replace("{VALOR}", Server.HtmlEncode(valor))
          templateAux = templateAux.Replace("{CHECKED}", checked)
          sb.Append(templateAux)
        Next

        sb.Append("</div>")
        html = sb.ToString()
      End If

    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

  ''' <summary>
  ''' obtiene el ancho maximo que debe tener una imagen
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AnchoMaximoImagen() As String
    Dim valor As String = AppSettings("anchoMaximoImagen").ToString
    Return valor
  End Function

  ''' <summary>
  ''' obtiene el ancho maximo que debe tener una imagen
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AltoMaximoImagen() As String
    Dim valor As String = AppSettings("altoMaximoImagen").ToString
    Return valor
  End Function

  ''' <summary>
  ''' obtiene semaforo de la reportabilidad de un camion
  ''' </summary>
  ''' <param name="estado"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerSemaforoReportabilidad(ByVal estado As String, ByVal fecha As String) As String
    Dim html, template, imagen, mensaje As String

    Try
      template = "<img src=""../img/{IMAGEN}"" alt="""" class=""show-tooltip"" data-toggle=""tooltip"" data-placement=""top"" data-original-title=""{MENSAJE}"" title="""" />"

      Select Case estado.ToUpper()
        Case "ONLINE"
          imagen = "bullet_green.png"
          mensaje = "reportando en l&iacute;nea"
        Case "OFFLINE"
          imagen = "bullet_red.png"
          mensaje = "no reporta " & fecha
        Case Else
          imagen = "bullet_grey.png"
          mensaje = "placa no est&aacute; integrada"
      End Select

      template = template.Replace("{IMAGEN}", imagen)
      template = template.Replace("{MENSAJE}", mensaje)
      html = template
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function
#End Region


End Class