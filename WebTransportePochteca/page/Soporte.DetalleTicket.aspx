﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Soporte.DetalleTicket.aspx.vb" Inherits="WebTransportePochteca.Soporte_DetalleTicket" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Ticket</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/soporte.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdTicket" runat="server" value="-1" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Detalle Ticket"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-default">
            <div class="panel-heading"><span class="panel-title"><strong>DATOS GENERALES</strong></span></div>
            <div class="panel-body">
              <div class="form-group row">
                <div class="col-xs-4">
                  <label class="control-label">Nro Ticket</label>
                  <div><asp:Label ID="lblIdTicket" runat="server"></asp:Label></div>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Fecha Ticket</label>
                  <div><asp:Label ID="lblFechaHoraTicket" runat="server"></asp:Label></div>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Origen Ticket</label>
                  <div><asp:Label ID="lblOrigenTicket" runat="server"></asp:Label></div>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-4">
                  <label class="control-label">Aplicaci&oacute;n</label>
                  <div><asp:Label ID="lblAplicacion" runat="server"></asp:Label></div>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">M&oacute;dulo</label>
                  <div><asp:Label ID="lblModulo" runat="server"></asp:Label></div>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Motivo Ticket</label>
                  <div><asp:Label ID="lblMotivoTicket" runat="server"></asp:Label></div>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-12">
                  <label class="control-label">Observaci&oacute;n</label>
                  <div><asp:Label ID="lblObservacion" runat="server"></asp:Label></div>
                </div>
              </div>

            </div>
          </div>
          
          <div class="panel panel-default">
            <div class="panel-heading"><span class="panel-title"><strong>ESTADO TICKET</strong></span></div>
            <div class="panel-body">
              <div class="form-group row">
                <div class="col-xs-12">
                  <label class="control-label">Estado Ticket</label>
                  <div><asp:Label ID="lblEstado" runat="server"></asp:Label></div>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-12">
                  <label class="control-label">Resultado Gesti&oacute;n</label>
                  <div><asp:Label ID="lblResultadoGestion" runat="server"></asp:Label></div>
                </div>
              </div>
            </div>
          </div>
        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Soporte.cerrarPopUp('0')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
        </div>
      </div>
    </div>

  </form>
</body>
</html>
