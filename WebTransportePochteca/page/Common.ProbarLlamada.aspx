﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Common.ProbarLlamada.aspx.vb" Inherits="WebTransportePochteca.Common_ProbarLlamada" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <input type="hidden" id="txtSimulacionActiveX" value="" />
  <input type="hidden" id="txtCallCenterIdLlamada" value="" />
  <input type="hidden" id="txtCallCenterEstadoLlamada" value="-1" />
  <input type="hidden" id="txtCallCenterFinalizarLlamadaModoManual" value="0" />
  
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Probar llamada</p>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="form-group">
        <div class="row">
          <div class="col-xs-4">&nbsp;</div>
          <div class="col-xs-4">

            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
                <input type="text" id="txtTelefono" class="form-control input-lg text-center" value="" placeholder="Ingrese n&uacute;mero telef&oacute;nico" />
              </div>
              <div id="lblMensajeErrorTelefono"></div>
            </div>

            <div class="form-group text-center">
              <div id="btnLlamar" class="btn btn-lg btn-block btn-success" onclick="Sistema.marcarTelefono()"><span class="glyphicon glyphicon-phone-alt"></span>&nbsp;Llamar</div>
            </div>

            <br />
            <div class="form-group text-center">
              <div id="lblMensajeLlamada"></div>
            </div>

          </div>
          <div class="col-xs-4">&nbsp;</div>
        </div>
      </div>

    </asp:Panel>
  </div>

</asp:Content>
