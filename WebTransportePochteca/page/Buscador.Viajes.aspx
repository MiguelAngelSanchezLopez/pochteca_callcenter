﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Buscador.Viajes.aspx.vb" Inherits="WebTransportePochteca.Buscador_Viajes" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script language="javascript" type="text/javascript" src="../js/trazaviaje.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Viajes</p>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-3">
              <label class="control-label">Tienda Destino</label>
              <uc1:wucCombo id="ddlLocalDestino" runat="server" FuenteDatos="Tabla" TipoCombo="Local" ItemTodos="true" CssClass="form-control chosen-select" DeshabilitarConUnItem="true"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Desde</label>
              <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Hasta</label>
              <asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              <div id="lblMensajeErrorFechaHasta" runat="server"></div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">IdMaster</label>
              <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Estado Viaje</label>
              <uc1:wucCombo id="ddlEstadoViaje" runat="server" FuenteDatos="TipoGeneral" TipoCombo="EstadoViaje" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-3">
              <label class="control-label">Cedis Origen</label>
              <uc1:wucCombo id="ddlCDOrigen" runat="server" FuenteDatos="Tabla" TipoCombo="CentroDistribucion" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Placa Tracto / Remolque</label>
              <asp:TextBox ID="txtPatente" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(TrazaViaje.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="1" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered small">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="NroTransporte" HeaderText="IdMaster" SortExpression="NroTransporte" HeaderStyle-Width="100px" />
              <asp:BoundField DataField="NombreConductor" HeaderText="Operador" SortExpression="NombreConductor" />
              <asp:BoundField DataField="PatenteTracto" HeaderText="Placa Tracto" SortExpression="PatenteTracto" HeaderStyle-Width="100px" />
              <asp:BoundField DataField="PatenteTrailer" HeaderText="Placa Remolque" SortExpression="PatenteTrailer" HeaderStyle-Width="100px" />
              <asp:BoundField DataField="CDOrigen" HeaderText="Cedis Origen" SortExpression="CDOrigen" />
              <asp:BoundField DataField="LocalDestino" HeaderText="Tienda Destino" SortExpression="LocalDestino" />
              <asp:BoundField DataField="FHSalidaOrigen" HeaderText="Fecha Salida" SortExpression="FHSalidaOrigen" HeaderStyle-Width="130px" />
              <asp:BoundField DataField="TipoViaje" HeaderText="Tipo Viaje" SortExpression="TipoViaje" HeaderStyle-Width="80px" />
              <asp:BoundField DataField="EstadoViaje" HeaderText="Estado Viaje" SortExpression="EstadoViaje" HeaderStyle-Width="110px" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="80px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdTrazaViaje" runat="server" value='<%# Bind("IdTrazaViaje") %>' />
                  <input type="hidden" id="txtNroTransporte" runat="server" value='<%# Bind("NroTransporte") %>' />
                  <input type="hidden" id="txtPatenteTracto" runat="server" value='<%# Bind("PatenteTracto") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

      <script type="text/javascript">
        jQuery(document).ready(function () {
          jQuery(".chosen-select").select2();

          jQuery(".date-pick").datepicker({
            changeMonth: true,
            changeYear: true
          });

        });
      </script>

    </asp:Panel>
  </div>

</asp:Content>
