﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_CambiarClave
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_Formatos = 1
  End Enum

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim passwordCaduco As String = Utilidades.IsNull(Request("passwordCaduco"), "0")

    Me.ViewState.Add("passwordCaduco", passwordCaduco)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    GrabarRegistro()
  End Sub

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim passwordCaduco As String = Me.ViewState.Item("passwordCaduco")
    Dim mensaje, fechaCaducidadPassword As String

    Me.txtClaveEncriptada.Value = oUsuario.Password

    If (passwordCaduco = "1") Then
      'si la fecha es nula (trae por defecto el año 1) entonces no muestra la fecha en el mensaje
      If (oUsuario.FechaCaducidadPassword.Year = 1) Then
        fechaCaducidadPassword = ""
      Else
        fechaCaducidadPassword = " el d&iacute;a " & Replace(oUsuario.FechaCaducidadPassword.ToString("dd/MM/yyyy"), "-", "/")
      End If

      mensaje = "<strong>Su clave expir&oacute;" & fechaCaducidadPassword & ".</strong><br />Debe registrar una nueva clave para poder ingresar al sistema."
      mensaje = Utilidades.MostrarMensajeUsuario(mensaje, Utilidades.eTipoMensajeAlert.Danger, False, "text-center")
      Me.ltlMensajeCaducidadPassword.Text = mensaje
      Me.btnCerrarSesion.Visible = True
      Me.btnCerrarSesion.Attributes.Add("onclick", "Sistema.validarLogout({ llavePerfil:'" & oUsuario.PerfilLlave & "' })")
      Me.btnCerrar.Visible = False
    End If

  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = True

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          Utilidades.RegistrarScript(Me.Page, "setTimeout(function () { Sistema.cerrarPopUp(); }, 2000);", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          Utilidades.RegistrarScript(Me.Page, "setTimeout(function () { Sistema.cerrarPopUp(); }, 2000);", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nuevaClave As String

    Try
      nuevaClave = Utilidades.IsNull(Me.txtNuevaClave.Text, "")
      oUsuario.Password = Criptografia.obtenerSHA1(nuevaClave)
      oUsuario.Actualizar()
      Session("Usuario") = oUsuario

      Sistema.GrabarLogSesion(oUsuario.Id, "Cambia clave: Username " & oUsuario.Username)
      status = "modificado"
    Catch ex As Exception
      msgError = "No se pudo actualizar la clave.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim passwordCaduco As String = Me.ViewState.Item("passwordCaduco")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "modificado"
          textoMensaje = "{MODIFICADO}La clave fue actualizada satisfactoriamente"
          passwordCaduco = "0"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo actualizar la clave. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo actualizar la clave. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje
    Response.Redirect("./Mantenedor.CambiarClave.aspx?passwordCaduco=" & passwordCaduco)
  End Sub

#End Region

End Class