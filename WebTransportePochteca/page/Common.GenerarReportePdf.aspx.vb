﻿Imports Persits.PDF
Imports CapaNegocio
Imports Ionic.Zip
Imports System.IO

Public Class Common_GenerarReportePdf
  Inherits System.Web.UI.Page
  Public Const INF_ATRIBUTOS_CONFIGURACION_PDF As String = "leftMargin=0; topMargin=0; rightMargin=0; bottomMargin=0; hyperlinks=true;"
  Dim carpetaLog As String = "AlertasRojas\PDF"

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())

      Dim pag As String = Utilidades.IsNull(Request("pag"), "")

      Select Case pag
        Case "AlertaRojaListado"
          ObtenerReporte_AlertaRoja()
        Case "RespuestaAlertaRoja"
          ObtenerReporte_RespuestaAlertaRoja()
        Case Else
      End Select

    End If
  End Sub

#Region "Metodos Privados"
  Private Sub ObtenerReporte_AlertaRoja()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nombreArchivoPDF, pathPDF As String
    Dim nombreTransportista, emailTransportista, rutTransportista As String
    Dim enviado As Boolean
    Dim accion As String = Utilidades.IsNull(Request("op"), "")
    Dim json As String = Utilidades.IsNull(Request("json"), "")
    Dim zip As New ZipFile
    Dim objJSON As Object

    objJSON = MyJSON.ConvertJSONToObject(json)

    For Each row In objJSON
      nombreTransportista = MyJSON.ItemObject(row, "nombreTransportista")
      emailTransportista = Convert.ToString(row("emailTransportista"))
      rutTransportista = MyJSON.ItemObject(row, "rutTransportista")

      'Genera el achivo PDF
      nombreArchivoPDF = GenerarPDFAlertasRojas(nombreTransportista, rutTransportista)
      pathPDF = Utilidades.RutaLogsSitio() & "\" & carpetaLog & "\" & nombreArchivoPDF & ".pdf"

      Select Case accion
        Case "download"
          zip.AddFile(pathPDF, "AlertasRojas")
        Case "email"
          'Envio de archivos por correo
          enviado = EnviarMail(nombreTransportista, emailTransportista, pathPDF)

          If (enviado) Then
            GrabarNotificacion(rutTransportista)
          End If

          'Se elimina el archivo
          Archivo.EliminarArchivoEnDisco(pathPDF)
      End Select

    Next

    'Si existen archivos a comprimir
    If (zip.Count > 0) Then
      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"

      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=ArchivosAlertasRojas.zip")
      Response.ContentType = "application/zip"
      zip.Save(Response.OutputStream)

      'Se eliminan los archivos comprimidos
      For Each file As String In Directory.GetFiles(Utilidades.RutaLogsSitio() & "\" & carpetaLog)
        Archivo.EliminarArchivoEnDisco(file)
      Next

      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "1"
      Response.End()
    Else
      Response.Redirect("./OT.AlertaRojaListado.aspx")
    End If
  End Sub

  Private Sub ObtenerReporte_RespuestaAlertaRoja()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nombreArchivo, path As String
    Dim zip As New ZipFile

    'Genera el achivo PDF
    nombreArchivo = GenerarPDFRespuestasAlertasRojas() + ".pdf"
    path = Utilidades.RutaLogsSitio() & "\" & carpetaLog & "\" & nombreArchivo

    Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"

    'convierte el archivo que esta en disco en binario para poder mostrarlo en la descarga
    Dim objStream As Object
    objStream = Server.CreateObject("ADODB.Stream")
    objStream.Open()
    objStream.Type = 1    'tipo binario
    objStream.LoadFromFile(path)

    'los carga en la pagina
    Response.Buffer = True
    Response.Clear()
    Response.AppendHeader("content-disposition", "attachment; filename=" & nombreArchivo)
    Response.ContentType = "application/pdf"
    Response.BinaryWrite(objStream.Read)

    objStream.Close()
    objStream = Nothing

    Archivo.EliminarArchivoEnDisco(path)
    Response.End()

    Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "1"

  End Sub

  ''' <summary>
  ''' Genera el archivo PDF
  ''' </summary>
  ''' <remarks></remarks>
  Private Function GenerarPDFAlertasRojas(ByVal nombreTransportista As String, ByVal rutTransportista As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nombreArchivoPDF As String = ""
    Dim pathPDF As String = ""

    Try

      Dim queryString As String
      Dim url As String

      'Reemplazo caracteres
      nombreTransportista = nombreTransportista.Replace("Ñ", "\N")

      queryString = Criptografia.EncriptarTripleDES("idUsuario=" & oUsuario.Id & "&rutTransportista=" & rutTransportista)
      url = Utilidades.ObtenerUrlBase() & "/public/InformeAlertasRojas.aspx?a=" & queryString

      nombreArchivoPDF = Archivo.CrearNombreUnicoArchivo("AlertasRojas_" & nombreTransportista)
      pathPDF = Utilidades.RutaLogsSitio() & "\" & carpetaLog & "\" & nombreArchivoPDF & ".pdf"

      Dim objPdf As PdfManager = New PdfManager()
      Dim objDoc As PdfDocument = objPdf.CreateDocument()
      objDoc.ImportFromUrl(url, INF_ATRIBUTOS_CONFIGURACION_PDF)
      objDoc.PageMode = pdfPageMode.pdfUseThumbs
      objDoc.PageLayout = pdfPageLayout.pdfSinglePage
      objDoc.Save(pathPDF, False)

      Sistema.GrabarLogSesion(oUsuario.Id, "Se genera archivo pdf: archivo " & pathPDF)

    Catch ex As Exception
      Sistema.GrabarLogSesion(oUsuario.Id, "No se pudo generar archivo pdf: archivo " & pathPDF)
    End Try

    Return nombreArchivoPDF

  End Function

  ''' <summary>
  ''' Genera el archivo PDF
  ''' </summary>
  ''' <remarks></remarks>
  Private Function GenerarPDFRespuestasAlertasRojas() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nombreArchivoPDF As String = ""
    Dim pathPDF As String = ""
    Dim idAlertaRojaEstadoActual As String = Utilidades.IsNull(Request("idAlertaRojaEstadoActual"), "")
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "")
    Try

      Dim queryString As String
      Dim url As String

      queryString = Criptografia.EncriptarTripleDES("idAlertaRojaEstadoActual=" & idAlertaRojaEstadoActual & "&nroTransporte=" & nroTransporte)

      'Reemplazo caracteres
      url = Utilidades.ObtenerUrlBase() & "/public/InformeFinal.aspx?a=" & queryString

      nombreArchivoPDF = Archivo.CrearNombreUnicoArchivo("RespuestaAlertaRoja_" & nroTransporte)
      pathPDF = Utilidades.RutaLogsSitio() & "\" & carpetaLog & "\" & nombreArchivoPDF & ".pdf"

      Dim objPdf As PdfManager = New PdfManager()
      Dim objDoc As PdfDocument = objPdf.CreateDocument()
      objDoc.ImportFromUrl(url, INF_ATRIBUTOS_CONFIGURACION_PDF)
      objDoc.PageMode = pdfPageMode.pdfUseThumbs
      objDoc.PageLayout = pdfPageLayout.pdfSinglePage
      objDoc.Save(pathPDF, False)

      Sistema.GrabarLogSesion(oUsuario.Id, "Se genera archivo pdf: archivo " & pathPDF)

    Catch ex As Exception
      Sistema.GrabarLogSesion(oUsuario.Id, "No se pudo generar archivo pdf: archivo " & pathPDF)
    End Try

    Return nombreArchivoPDF

  End Function

  ''' <summary>
  ''' Envia correo a transportista
  ''' </summary>
  ''' <remarks></remarks>
  Private Function EnviarMail(ByVal nombreTransportista As String, ByVal emailTransportista As String, ByVal pathPdf As String) As Boolean
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgProceso As String
    Dim asuntoEmail As String
    Dim bodyMensaje As String
    Dim contadorErrores As Integer = 0
    Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
    Dim emailPara As String
    Dim emailsAlertasRojas As String = Configuracion.Leer("jobListadoMailAlertarRojas")
    Try

      'Email: Asunto
      '----------------------------------------------------------------------------
      asuntoEmail = "Alertas Rojas fecha: "
      asuntoEmail &= Replace(Herramientas.MyNow().ToString("dd/MM/yyyy"), "-", "/")
      asuntoEmail &= " - " & nombreTransportista

      'Email: Body
      '----------------------------------------------------------------------------
      bodyMensaje = "Estimada Línea de Transporte:{$BR}{$BR}En el presente mail se adjunta un archivo con información de los viajes con Alertas Rojas, es necesario que ud. "
      bodyMensaje &= "ingrese en los próximos 2 días hábiles al siguiente link " & Utilidades.ObtenerUrlBase() & " y entregue una explicación para cada alerta.{$BR}{$BR}"
      bodyMensaje &= "Atte.{$BR}{$BR}"
      bodyMensaje &= "Transporte Pochteca{$BR}{$BR}{$BR}"
      bodyMensaje &= "(este mail se ejecutó automáticamente por el sistema. No responder por favor.)"
      bodyMensaje = bodyMensaje.Replace("{$BR}", vbCrLf)

      'Email: Correos
      '----------------------------------------------------------------------------
      emailPara = emailTransportista
      emailPara &= IIf(String.IsNullOrEmpty(emailsAlertasRojas), "", ";" & emailsAlertasRojas)

      If Utilidades.MailModoDebug Then
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(Utilidades.MailPruebas)
        ColeccionEmailsCC = Nothing
      Else
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(emailPara)
        ColeccionEmailsCC = Mail.ConstruirCollectionMail(Utilidades.MailAdministrador)
      End If

      'Email: Enviar
      '----------------------------------------------------------------------------
      msgProceso = Mail.Enviar(Utilidades.MailHost, _
                               Utilidades.MailEmisor, _
                               Utilidades.MailPasswordEmisor, _
                               Utilidades.MailNombreEmisor, _
                               ColeccionEmailsPara, _
                               asuntoEmail, _
                               bodyMensaje, _
                               False, _
                               ColeccionEmailsCC, _
                               True, _
                               pathPdf
                               )
      'Email: Status
      '----------------------------------------------------------------------------
      'graba el mensaje enviado por mail en el disco
      If msgProceso <> String.Empty Then contadorErrores = 1
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
      Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
      bodyMensaje &= vbCrLf & vbCrLf & "Enviado a: " & emailTransportista & ";" & Utilidades.MailAdministrador
      Archivo.CrearArchivoTextoEnDisco(bodyMensaje, Utilidades.RutaLogsSitio(), "AlertasRojas", nombreArchivo)
      Sistema.GrabarLogSesion(oUsuario.Id, "Se envian email a Línea de Transporte: emailTransportista " & emailTransportista)
      Return True
    Catch ex As Exception
      Sistema.GrabarLogSesion(oUsuario.Id, "No se pudo enviar email a Línea de Transporte: emailTransportista " & emailTransportista & ". ERROR: " & ex.Message)
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Graba la notificacion del envio
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarNotificacion(ByVal rutTransportista As String)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = ""
    Dim ds As DataSet

    Try
      ds = Alerta.ObtenerDetalleAlertasRojasPorTransportista(rutTransportista, False)

      For Each row As DataRow In ds.Tables(0).Rows
        nroTransporte = row("nroTransporte")
        Alerta.GrabarEstadoAlertaRoja("-1", oUsuario.Id, nroTransporte, Alerta.ESTADO_ALERTA_ROJA_ENVIADO_TRANSPORTISTA)
        Sistema.GrabarLogSesion(oUsuario.Id, "Graba estado aleta roja: idUsuario " & oUsuario.Id & " IdMaster " & nroTransporte)
      Next
    Catch ex As Exception
      Sistema.GrabarLogSesion(oUsuario.Id, "No se pudo Grabar estado aleta roja: idUsuario " & oUsuario.Id & " IdMaster " & nroTransporte & ". ERROR:" & ex.Message)
    End Try
  End Sub
#End Region

End Class
