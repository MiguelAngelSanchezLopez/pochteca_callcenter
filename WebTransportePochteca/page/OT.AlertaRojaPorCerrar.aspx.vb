﻿Imports CapaNegocio

Public Class OT_AlertaRojaPorCerrar
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    Gestionar
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String

    nroTransporte = Utilidades.IsNull(Me.txtNroTransporte.Text, "-1")

    'obtiene listado
    ds = Alerta.ObtenerAlertasRojasPorCerrar("-1", "-1", nroTransporte)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("dv") = Nothing
    Else
      Session("dv") = ds.Tables(0).DefaultView
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim totalRegistros As Integer

    dv = CType(Session("dv"), DataView)
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = dv.Table.Rows.Count
    End If

    If totalRegistros > 0 Then

      Me.pnlMensajeUsuario.Visible = False
      Me.pnlHolderGrilla.Visible = True
      'Me.pnlBotones.Visible = True

      'carga la grilla a utilizar
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()

      Utilidades.MostrarTotalRegistrosGrilla(dv, Me.lblTotalRegistros)
    Else
      Me.pnlHolderGrilla.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = "No se encontraron registros"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)

      'oculta la grilla
      Me.gvPrincipal.Visible = False
      ' Me.pnlBotones.Visible = False

    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    '-- inicializacion de controles que NO dependen del postBack --

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    'Me.btnGestionar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Gestionar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' metodo general para la paginacion de la grilla
  ''' </summary>
  Private Sub GridViewPageIndexChanging(ByVal gridView As GridView, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    Try

      'Cambia de pagina
      gridView.PageIndex = e.NewPageIndex

      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()

    Catch ex As Exception

      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)

    End Try

  End Sub

  ''' <summary>
  ''' metodo general para ordenar segun grilla seleccionada
  ''' </summary>
  Private Sub GridViewSorting(ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    Dim oUtilidades As New Utilidades
    Dim setearOrdenInicial As Boolean = False
    Try
      'Recupera el dataset para ordenar
      Dim dv As DataView = CType(Session("dv"), DataView)

      'determina el campo por el cual se ordenara
      Dim strOrdenarPor As String = e.SortExpression

      'Si ya se habia ordenado por ese campo, ordena pero en forma descendente
      If Not dv Is Nothing Then
        If strOrdenarPor = dv.Sort Then
          strOrdenarPor = strOrdenarPor & " DESC"
        End If
        'Ordena la grilla de acuerdo a la columna seleccionada
        dv.Sort = strOrdenarPor
      End If

      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub
#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)

      ObtenerDatos()
      ActualizaInterfaz()

    End If

  End Sub

  Protected Sub gvPrincipal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound
    'If e.Row.RowType = DataControlRowType.DataRow Then
    '  'code here
    'End If
  End Sub

  Protected Sub gvPrincipal_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPrincipal.Sorting
    'llama metodo general para ordenar
    GridViewSorting(e)
  End Sub

  Protected Sub gvPrincipal_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
    'llama metodo general para paginar
    GridViewPageIndexChanging(Me.gvPrincipal, e)
  End Sub

  Protected Sub btnFiltrar_Click(sender As Object, e As EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim pageSize As String = Trim(txtRegistrosPorPagina.Text)
    If pageSize = "" Or pageSize = "0" Then
      pageSize = Utilidades.ObtenerRegistrosPorPaginaDefault()
      txtRegistrosPorPagina.Text = pageSize
    End If
    'asigna total de registros por pagina
    Me.gvPrincipal.PageSize = pageSize
    'Muestra la primera pagina del paginador
    Me.gvPrincipal.PageIndex = 0

    ObtenerDatos()
    ActualizaInterfaz()

    Sistema.GrabarLogSesion(oUsuario.Id, "Busca Alerta Rojas")

  End Sub
End Class