﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Mantenedor_PaginaDetalle

  '''<summary>
  '''Control Head1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

  '''<summary>
  '''Control form1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

  '''<summary>
  '''Control txtIdPagina.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtIdPagina As Global.System.Web.UI.HtmlControls.HtmlInputHidden

  '''<summary>
  '''Control lblTituloFormulario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTituloFormulario As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblIdRegistro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblIdRegistro As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control pnlMensajeUsuario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeUsuario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control txtTitulo.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtTitulo As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtNombreArchivo.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtNombreArchivo As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control hErrorNombreArchivo.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hErrorNombreArchivo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtCategoria.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtCategoria As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control chkMostrarEnMenu.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents chkMostrarEnMenu As Global.System.Web.UI.WebControls.CheckBox

  '''<summary>
  '''Control chkEstado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents chkEstado As Global.System.Web.UI.WebControls.CheckBox

  '''<summary>
  '''Control Div1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents Div1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control hListadoFunciones.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hListadoFunciones As Global.System.Web.UI.WebControls.Literal

  '''<summary>
  '''Control msPerfiles.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents msPerfiles As Global.WebTransportePochteca.wucMultiselectCombinados

  '''<summary>
  '''Control btnCerrar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnCerrar As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control btnEliminar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnEliminar As Global.System.Web.UI.WebControls.LinkButton

  '''<summary>
  '''Control btnCrear.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnCrear As Global.System.Web.UI.WebControls.LinkButton

  '''<summary>
  '''Control btnModificar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnModificar As Global.System.Web.UI.WebControls.LinkButton
End Class
