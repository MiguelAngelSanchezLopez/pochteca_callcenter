﻿'' Add's Internos
Imports CapaNegocio

Public Class Carrier_Indicadores
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If (oUsuario.PerfilLlave = Carrier.PERFIL_LINEA_TRANSPORTE) Then
      Me.hComboTransportista.Visible = True
    Else
      Me.hComboTransportista.Style.Add("display", "none")
      Me.ddlTransportista.SelectedValue = oUsuario.IdTransportista
    End If
  End Sub

  Protected Sub BtBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtBuscar.Click
    Dim res As Boolean = True
    Dim msg As String = String.Empty
    If (txtFechaDesde.Text <> "" And txtFechaHasta.Text <> "") Then
      Try
        If CDate(txtFechaDesde.Text) > CDate(txtFechaHasta.Text) Then
          res = False
          msg = "La fecha de inicio no puede ser mayor a la de termino"
        End If
      Catch ex As Exception
        res = False
      End Try
    End If

    If (res) Then
      Buscar()
    Else
      Utilidades.RegistrarScript(Me.Page, "Sistema.alertaMensajeError('" + msg + "');", Utilidades.eRegistrar.FINAL, "alertfecha")
    End If

  End Sub

  Public Sub Buscar()
    Try
      'Dim _us As Usuario = Session("Usuario")
      'ViewState("gridprincipal") = Carrier.ListarInforme(_us.IdTransportista, txtFechaDesde.Text, txtFechaHasta.Text, dropInforme.SelectedValue)

      Dim idUsuarioTransportista As String = Utilidades.IsNull(Me.ddlTransportista.SelectedValue, "-1")
      ViewState("gridprincipal") = Carrier.ListarInforme(idUsuarioTransportista, txtFechaDesde.Text, txtFechaHasta.Text, dropInforme.SelectedValue)
      gvPrincipal.DataSource = ViewState("gridprincipal")
      gvPrincipal.DataBind()
    Catch ex As Exception
      Dim err As String = ex.Message.ToString()
    End Try
  End Sub

  Protected Sub ImgPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    Dim filaGrilla As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
    Dim idx As Integer = filaGrilla.RowIndex
    Dim ds As New DataSet
    ds = Carrier.InformeArchivo(filaGrilla.Cells(0).Text)

    Dim contenType As String
    Dim nombre As String
    Dim contenido As Byte()

    If ds.Tables(0).Rows.Count > 0 Then
      contenType = ds.Tables(0).Rows(0)("ContentType").ToString()
      nombre = ds.Tables(0).Rows(0)("NombreArchivo").ToString()
      contenido = ds.Tables(0).Rows(0)("Contenido")

      Response.Buffer = True
      Response.Clear()
      Response.AppendHeader("content-disposition", "attachment; filename=" & nombre.Replace(" ", "_"))
      Response.ContentType = contenType
      Response.BinaryWrite(contenido)
      Response.End()
    End If

  End Sub

  Protected Sub gvPrincipal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
    gvPrincipal.PageIndex = e.NewPageIndex
    gvPrincipal.DataSource = ViewState("gridprincipal")
    gvPrincipal.DataBind()
  End Sub
End Class