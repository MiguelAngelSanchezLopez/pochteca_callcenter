﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OT.AlertaRojaDetalle.aspx.vb"
  Inherits="WebTransportePochteca.OT_AlertaRojaDetalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Detalle P&aacute;gina</title>
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
  <div class="container sist-margin-top-10">
    <div class="form-group text-center">
      <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
      <div>
        <asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
      <asp:Panel ID="pnlMensajeUsuario" runat="server">
      </asp:Panel>
    </div>
    <div>
      <asp:Panel ID="pnlMensajeAcceso" runat="server">
      </asp:Panel>
      <asp:Panel ID="pnlContenido" runat="server">
        <div class="form-group">
          <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%"
            Visible="false">
            <div id="divViaje">
                <asp:Literal ID="ltlContenido" runat="server" Text=""></asp:Literal>
            </div>
          </asp:Panel>
        </div>
      </asp:Panel>
    </div>


    <!-- Botones -->
    <div class="form-group text-center sist-clear-both">
      <br />
      <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUpMantenedor('1')">
        <span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
    </div>
  </div>
  </form>
</body>
</html>
