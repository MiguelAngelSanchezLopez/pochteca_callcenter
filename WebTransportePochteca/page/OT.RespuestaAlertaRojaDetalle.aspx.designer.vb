﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class OT_RespuestaAlertaRojaDetalle

  '''<summary>
  '''Control Head1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

  '''<summary>
  '''Control form1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

  '''<summary>
  '''Control hRechazado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hRechazado As Global.System.Web.UI.WebControls.HiddenField

  '''<summary>
  '''Control hJsonRechazado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hJsonRechazado As Global.System.Web.UI.WebControls.HiddenField

  '''<summary>
  '''Control lblTituloFormulario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTituloFormulario As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblNombreTransportista.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblNombreTransportista As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblIdRegistro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblIdRegistro As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control pnlMensajeUsuario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeUsuario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control ltlListado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ltlListado As Global.System.Web.UI.WebControls.Literal

  '''<summary>
  '''Control pnlEstadoTransportista.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlEstadoTransportista As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlEstadoTransportistaSugeridos.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlEstadoTransportistaSugeridos As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control lblFechaCreacionSugerido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblFechaCreacionSugerido As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control ltlEstadoTransportistaSugerido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ltlEstadoTransportistaSugerido As Global.System.Web.UI.WebControls.Literal

  '''<summary>
  '''Control pnlEstadoTransportistaAprobados.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlEstadoTransportistaAprobados As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control lblFechaCreacionAprobado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblFechaCreacionAprobado As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control ltlEstadoTransportistaAprobado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ltlEstadoTransportistaAprobado As Global.System.Web.UI.WebControls.Literal

  '''<summary>
  '''Control pnlEstadoTransportistaMultiselect.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlEstadoTransportistaMultiselect As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control lblDescripcionEstadoTransportistaMultiselect.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblDescripcionEstadoTransportistaMultiselect As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control msEstado.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents msEstado As Global.WebTransportePochteca.wucMultiselectCombinados

  '''<summary>
  '''Control txtMontoUnoTransportista.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtMontoUnoTransportista As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtMontoDosTransportista.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtMontoDosTransportista As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtObservacion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtObservacion As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control btnCerrar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnCerrar As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control btnGrabar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnGrabar As Global.System.Web.UI.WebControls.LinkButton

  '''<summary>
  '''Control btnFinalizarPlanAccion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnFinalizarPlanAccion As Global.System.Web.UI.WebControls.LinkButton
End Class
