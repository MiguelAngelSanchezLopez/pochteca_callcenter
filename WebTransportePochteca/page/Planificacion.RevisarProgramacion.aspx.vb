﻿Imports CapaNegocio
Imports System.Data

Public Class Planificacion_RevisarProgramacion
  Inherits System.Web.UI.Page

#Region "Enum"
  Private NOMBRE_PAGINA As String = Utilidades.ObtenerNombrePaginaActual()
  Private RECARGAR_PAGINA As String = NOMBRE_PAGINA & "_RECARGAR"

  Private Enum eFunciones
    Ver
    Grabar
    ReasignarCamion
    EliminarProgramacion
    ExportarProgramacion
    CrearNuevaProgramacion
  End Enum

  Private Enum eTabla As Integer
    T00_ListadoPlanificacion = 0
    T01_Locales = 1
  End Enum
#End Region

#Region "Metodo Controles ASP"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim cargadoDesdePopUp As String = Utilidades.IsNull(CType(Session(RECARGAR_PAGINA), String), "0")

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    If cargadoDesdePopUp = "1" Then
      ObtenerDatos()
      ActualizaInterfaz()
    End If

    MostrarMensajeUsuario()
    Session(RECARGAR_PAGINA) = "0"
  End Sub

  Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    GrabarFiltrosEnSesion()
    ObtenerDatos()
    ActualizaInterfaz()
    Sistema.GrabarLogSesion(oUsuario.Id, "Busca planificaciones")
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    textoMensaje &= GrabarDatosRegistro(status)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "modificado"
          textoMensaje = "{MODIFICADO}Los datos fueron actualizados satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudieron actualizar los datos. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudieron actualizar los datos. Intente nuevamente por favor.<br />" & textoMensaje
    End If

    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje
    Session(RECARGAR_PAGINA) = "1"
    Response.Redirect("./Planificacion.RevisarProgramacion.aspx")
  End Sub

#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim dcFiltros As Dictionary(Of String, String) = CType(Session(NOMBRE_PAGINA), Dictionary(Of String, String))
    Dim cargadoDesdePopUp As String = Utilidades.IsNull(CType(Session(RECARGAR_PAGINA), String), "0")
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario, nroTransporte, fechaPresentacion, patenteTracto, patenteTrailer, carga, horaPresentacion, minutoPresentacion, codLocal As String

    If (cargadoDesdePopUp = "1") Then
      If (Not dcFiltros Is Nothing) Then
        If (dcFiltros.ContainsKey("idUsuario")) Then If dcFiltros.Item("idUsuario") <> "-1" Then Me.ddlTransportista.SelectedValue = dcFiltros.Item("idUsuario")
        If (dcFiltros.ContainsKey("carga")) Then If dcFiltros.Item("carga") <> "-1" Then Me.ddlCarga.SelectedValue = dcFiltros.Item("carga")
        If (dcFiltros.ContainsKey("nroTransporte")) Then Me.txtNroTransporte.Text = dcFiltros.Item("nroTransporte")
        If (dcFiltros.ContainsKey("fechaPresentacion")) Then Me.txtFechaPresentacion.Text = dcFiltros.Item("fechaPresentacion")
        If (dcFiltros.ContainsKey("patenteTracto")) Then Me.txtPatenteTracto.Text = dcFiltros.Item("patenteTracto")
        If (dcFiltros.ContainsKey("patenteTrailer")) Then Me.txtPatenteTrailer.Text = dcFiltros.Item("patenteTrailer")
        If (dcFiltros.ContainsKey("local")) Then If dcFiltros.Item("local") <> "-1" Then Me.ddlLocal.SelectedValue = dcFiltros.Item("local")
        If (dcFiltros.ContainsKey("horaPresentacion")) Then Me.wucHoraInicio.SelectedValueHora = dcFiltros.Item("horaPresentacion")
        If (dcFiltros.ContainsKey("minutoPresentacion")) Then Me.wucHoraInicio.SelectedValueMinutos = dcFiltros.Item("minutoPresentacion")
      End If
    End If

    'obtiene valores de los controles
    idUsuario = Utilidades.IsNull(Me.ddlTransportista.SelectedValue, "-1")
    nroTransporte = Utilidades.IsNull(Me.txtNroTransporte.Text, "-1")
    fechaPresentacion = Utilidades.IsNull(Me.txtFechaPresentacion.Text, "-1")
    patenteTracto = Utilidades.IsNull(Me.txtPatenteTracto.Text, "-1")
    patenteTrailer = Utilidades.IsNull(Me.txtPatenteTrailer.Text, "-1")
    carga = Utilidades.IsNull(Me.ddlCarga.SelectedValue, "-1")
    codLocal = Utilidades.IsNull(Me.ddlLocal.SelectedValue, "-1")
    horaPresentacion = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueHora, "-1")
    minutoPresentacion = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueMinutos, "-1")

    'obtiene listado
    ds = PlanificacionTransportista.ObtenerListado(idUsuario, nroTransporte, fechaPresentacion, patenteTracto, patenteTrailer, carga, codLocal, horaPresentacion, minutoPresentacion)

    'guarda el resultado en session
    Me.ViewState.Add("ds", ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim tablaDatos As String

    tablaDatos = DibujarTablaDatos()
    Me.ltlTablaDatos.Text = tablaDatos
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    Me.btnNuevaProgramacion.Attributes.Add("onclick", "Planificacion.reasignarCamion({ idPlanificacion: '-1', nroTransporte: '-1', llaveRecargarPagina:'" & RECARGAR_PAGINA & "', nombrePagina:'" & NOMBRE_PAGINA & "' })")

    'verifica los permisos sobre los controles
    VerificarPermisos()

    Me.wucHoraInicio.SelectedValueHora = ""
    Me.wucHoraInicio.SelectedValueMinutos = ""
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)
    Me.btnExportar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.ExportarProgramacion.ToString)
    Me.btnNuevaProgramacion.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                                      Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.CrearNuevaProgramacion.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensaje.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' dibuja tabla con la planificacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarTablaDatos() As String
    Dim html As String = ""
    Dim template As String = ""
    Dim ds As New DataSet
    Dim dt As New DataTable
    Dim totalRegistros As Integer
    Dim sb As New StringBuilder
    Dim templateAux, nroTransporte, fechaPresentacion, carga, tipoCamion, patenteTracto, patenteTrailer, locales, nombreTransportista, rutTransportista As String
    Dim nombreConductor, rutConductor, celular, anden, idPlanificacion, successAnden, disabledAnden, opciones, toolTipAnden, popOverDescripcion, htmlBloqueadoPorFueraHorario As String
    Dim camion, patente As String
    Dim finalizada, confirmacionAnden, bloqueadoPorFueraHorario As Boolean
    Dim htmlSinRegistro As String = "<em>Sin dato</em>"

    Try
      ds = Me.ViewState.Item("ds")

      template &= "<tr>"
      template &= "  <td>"
      template &= "    <div class=""sist-cursor-pointer show-popover"" data-toggle=""popover"" data-trigger=""hover"" data-placement=""right"" title=""Informaci&oacute;n adicional planificaci&oacute;n"" data-content=""{POPOVER_DESCRIPCION}""><u>{FECHA_PRESENTACION}</u></div>"
      template &= "  </td>"
      template &= "  <td><span id=""{PREFIJO_CONTROL}_lblRutTransportista"" data-target=""nro-transporte"" data-prefijoControl=""{PREFIJO_CONTROL}"">{NOMBRE_TRANSPORTISTA}</span></td>"
      template &= "  <td>"
      template &= "    {NRO_TRANSPORTE}<br />"
      template &= "    {BLOQUEADO_POR_FUERA_HORARIO}</td>"
      template &= "  </td>"
      template &= "  <td>{LOCALES}</td>"
      template &= "  <td>{CAMION}</td>"
      template &= "  <td>{PATENTE}</td>"
      template &= "  <td>{NOMBRE_CONDUCTOR}</td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtAnden"" value=""{ANDEN}"" class=""form-control input-sm show-tooltip {SUCCESS_ANDEN}"" data-toggle=""tooltip"" data-placement=""bottom"" title=""{TOOLTIP_ANDEN}"" {DISABLED_ANDEN} /></td>"
      template &= "  <td>{OPCIONES}</td>"
      template &= "</tr>"

      dt = ds.Tables(eTabla.T00_ListadoPlanificacion)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        Me.pnlBotones.Visible = False
        html = Utilidades.MostrarMensajeUsuario("No hay planificaciones asociadas", Utilidades.eTipoMensajeAlert.Warning, True)
      Else
        sb.Append("<div class=""small"">")
        sb.Append("<table class=""table table-condensed table-bordered"">")
        sb.Append("  <thead>")
        sb.Append("    <tr>")
        sb.Append("      <th style=""width:140px"">Fecha Presentaci&oacute;n</th>")
        sb.Append("      <th>L&iacute;nea de Transporte</th>")
        sb.Append("      <th style=""width:110px"">IdMaster</th>")
        sb.Append("      <th>Tienda</th>")
        sb.Append("      <th style=""width:120px"">Cami&oacute;n</th>")
        sb.Append("      <th style=""width:110px"">Placa</th>")
        sb.Append("      <th>Operador</th>")
        sb.Append("      <th style=""width:80px"">And&eacute;n</th>")
        sb.Append("      <th style=""width:160px"">Opciones</th>")
        sb.Append("    </tr>")
        sb.Append("  </thead>")
        sb.Append("  <tbody>")

        'asume que la planificacion no esta finalizada
        finalizada = False

        For Each dr As DataRow In dt.Rows
          templateAux = template
          opciones = ""
          idPlanificacion = dr.Item("IdPlanificacion")
          rutTransportista = dr.Item("RutTransportista")
          nombreTransportista = dr.Item("NombreTransportista")
          nroTransporte = dr.Item("NroTransporte")
          fechaPresentacion = dr.Item("FechaPresentacion")
          carga = dr.Item("Carga")
          tipoCamion = dr.Item("TipoCamion")
          patenteTracto = dr.Item("PatenteTracto")
          patenteTrailer = dr.Item("PatenteTrailer")
          nombreConductor = dr.Item("NombreConductor")
          rutConductor = dr.Item("RutConductor")
          celular = dr.Item("Celular")
          anden = dr.Item("Anden")
          finalizada = dr.Item("Finalizada")
          confirmacionAnden = dr.Item("ConfirmacionAnden")
          bloqueadoPorFueraHorario = dr.Item("BloqueadoPorFueraHorario")
          successAnden = IIf(finalizada, "", IIf(confirmacionAnden, "alert-success", ""))
          disabledAnden = IIf(finalizada, "disabled=""disabled""", "")

          'obtiene los locales asociados al NroTransporte
          locales = ObtenerLocalesPorNroTransporte(ds, nroTransporte, idPlanificacion)
          popOverDescripcion = ObtenerHtmlPopOverDescripcion(dr)

          'formatea valores
          toolTipAnden = IIf(confirmacionAnden, "and&eacute;n confirmado", IIf(String.IsNullOrEmpty(anden), "", "and&eacute;n SIN CONFIRMAR"))
          htmlBloqueadoPorFueraHorario = IIf(bloqueadoPorFueraHorario, "<span class=""alert alert-danger sist-padding-label-prioridad sist-margin-cero small""><strong>Bloqueo horario</strong></span>", "")

          nombreTransportista = Server.HtmlEncode(nombreTransportista)
          nombreTransportista &= IIf(String.IsNullOrEmpty(rutTransportista), "", "<em class=""help-block small sist-margin-cero""><strong>Rut</strong>: " & Server.HtmlEncode(rutTransportista) & "</em>")

          nombreConductor = Server.HtmlEncode(nombreConductor)
          nombreConductor &= IIf(String.IsNullOrEmpty(rutConductor), "", "<em class=""help-block small sist-margin-cero""><strong>Rut</strong>: " & Server.HtmlEncode(rutConductor) & "</em>")
          nombreConductor &= IIf(String.IsNullOrEmpty(celular), "", "<em class=""help-block small sist-margin-cero""><strong>Celular</strong>: " & Server.HtmlEncode(celular) & "</em>")

          camion = "<strong>Tipo</strong>: " & IIf(String.IsNullOrEmpty(tipoCamion), htmlSinRegistro, Server.HtmlEncode(tipoCamion))
          camion &= "<br /><strong>Carga</strong>: " & IIf(String.IsNullOrEmpty(carga), htmlSinRegistro, Server.HtmlEncode(carga))

          patente = "<strong>Tracto</strong>: " & IIf(String.IsNullOrEmpty(patenteTracto), htmlSinRegistro, Server.HtmlEncode(patenteTracto))
          patente &= "<br /><strong>Remolque</strong>: " & IIf(String.IsNullOrEmpty(patenteTrailer), htmlSinRegistro, Server.HtmlEncode(patenteTrailer))

          If (Not finalizada AndAlso (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.ReasignarCamion.ToString))) Then
            opciones &= "<button type=""button"" class=""btn btn-sm btn-success show-tooltip"" onclick=""Planificacion.reasignarCamion({ idPlanificacion: '{ID_PLANIFICACION}', nroTransporte: '{NRO_TRANSPORTE}', llaveRecargarPagina:'{RECARGAR_PAGINA}', nombrePagina:'{NOMBRE_PAGINA}' })"" data-toggle=""tooltip"" data-placement=""bottom"" title=""reasignar cami&oacute;n""><span class=""glyphicon glyphicon-transfer""></span></button>&nbsp;"
          End If
          opciones &= "<button type=""button"" class=""btn btn-sm btn-default show-tooltip"" onclick=""Planificacion.verHistorialReasignacionCamion({ idPlanificacion: '{ID_PLANIFICACION}', nroTransporte: '{NRO_TRANSPORTE}' })"" data-toggle=""tooltip"" data-placement=""bottom"" title=""ver historial de reasignaci&oacute;n""><span class=""glyphicon glyphicon-list""></span></button>&nbsp;"
          opciones &= "<button type=""button"" class=""btn btn-sm btn-default show-tooltip"" onclick=""Planificacion.verInformacionPatio({ idPlanificacion: '{ID_PLANIFICACION}', nroTransporte: '{NRO_TRANSPORTE}', llaveRecargarPagina:'{RECARGAR_PAGINA}' })"" data-toggle=""tooltip"" data-placement=""bottom"" title=""ver informaci&oacute;n del patio""><span class=""glyphicon glyphicon-phone""></span></button>&nbsp;"

          If (Not finalizada AndAlso (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.EliminarProgramacion.ToString))) Then
            opciones &= "<button type=""button"" class=""btn btn-sm btn-default show-tooltip"" onclick=""Planificacion.eliminarRegistro({ idPlanificacion: '{ID_PLANIFICACION}', nroTransporte: '{NRO_TRANSPORTE}', llaveRecargarPagina:'{RECARGAR_PAGINA}' })"" data-toggle=""tooltip"" data-placement=""bottom"" title=""eliminar planificaci&oacute;n""><span class=""glyphicon glyphicon-trash""></span></button>&nbsp;"
          End If

          'reemplaza marcas
          templateAux = templateAux.Replace("{OPCIONES}", opciones) 'dejarlo siempre al principio
          templateAux = templateAux.Replace("{ID_PLANIFICACION}", idPlanificacion)
          templateAux = templateAux.Replace("{NOMBRE_TRANSPORTISTA}", nombreTransportista)
          templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
          templateAux = templateAux.Replace("{FECHA_PRESENTACION}", fechaPresentacion)
          templateAux = templateAux.Replace("{CAMION}", camion)
          templateAux = templateAux.Replace("{LOCALES}", locales)
          templateAux = templateAux.Replace("{PATENTE}", patente)
          templateAux = templateAux.Replace("{NOMBRE_CONDUCTOR}", nombreConductor)
          templateAux = templateAux.Replace("{ANDEN}", anden)
          templateAux = templateAux.Replace("{RECARGAR_PAGINA}", RECARGAR_PAGINA)
          templateAux = templateAux.Replace("{NOMBRE_PAGINA}", NOMBRE_PAGINA)
          templateAux = templateAux.Replace("{SUCCESS_ANDEN}", successAnden)
          templateAux = templateAux.Replace("{TOOLTIP_ANDEN}", toolTipAnden)
          templateAux = templateAux.Replace("{DISABLED_ANDEN}", disabledAnden)
          templateAux = templateAux.Replace("{POPOVER_DESCRIPCION}", popOverDescripcion)
          templateAux = templateAux.Replace("{BLOQUEADO_POR_FUERA_HORARIO}", htmlBloqueadoPorFueraHorario)
          templateAux = templateAux.Replace("{PREFIJO_CONTROL}", idPlanificacion & "_" & nroTransporte) 'dejarlo siempre al final
          sb.Append(templateAux)
        Next

        sb.Append("  </tbody>")
        sb.Append("</table>")
        sb.Append("</div>")
        html = sb.ToString()

        Me.pnlBotones.Visible = True
      End If
    Catch ex As Exception
      Me.pnlBotones.Visible = False
      html = Utilidades.MostrarMensajeUsuario(ex.Message, Utilidades.eTipoMensajeAlert.Danger, False)
    End Try

    Return html
  End Function

  ''' <summary>
  ''' obtiene los locales asociados al NroTransporte
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="nroTransporte"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerLocalesPorNroTransporte(ByVal ds As DataSet, ByVal nroTransporte As String, ByVal idPlanificacion As String) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim htmlSinRegistro As String = "<em class=""help-block"">No tiene tiendas asociadas</em>"
    Dim dt As DataTable
    Dim templateAux, idPlanificacionTransportista, nombreLocal, codigoLocal, ordenEntrega, json As String
    Dim sb As New StringBuilder
    Dim listadoJSON As New List(Of Dictionary(Of String, Integer))
    Dim dcIdPlanificacionTransportista As New Dictionary(Of String, Integer)

    Try
      template &= "<div>"
      template &= "  {ORDEN_ENTREGA}) {NOMBRE_LOCAL}"
      template &= "</div>"

      dt = ds.Tables(eTabla.T01_Locales)
      Dim filtroListado = From row As DataRow In dt.Rows Where
                          row.Item("NroTransporte") = nroTransporte _
                          And row.Item("IdPlanificacion") = idPlanificacion

      If (filtroListado.Count = 0) Then
        html = htmlSinRegistro
      Else
        For Each dr As DataRow In filtroListado
          dcIdPlanificacionTransportista = New Dictionary(Of String, Integer)
          templateAux = template
          idPlanificacionTransportista = dr.Item("IdPlanificacionTransportista")
          nombreLocal = dr.Item("nombreLocal")
          codigoLocal = dr.Item("codigoLocal")
          ordenEntrega = dr.Item("ordenEntrega")

          'formatea valores
          nombreLocal = nombreLocal & IIf(String.IsNullOrEmpty(codigoLocal), "", " - " & codigoLocal)

          templateAux = templateAux.Replace("{ORDEN_ENTREGA}", ordenEntrega)
          templateAux = templateAux.Replace("{NOMBRE_LOCAL}", nombreLocal)
          sb.Append(templateAux)

          dcIdPlanificacionTransportista.Add("IdPlanificacionTransportista", idPlanificacionTransportista)
          listadoJSON.Add(dcIdPlanificacionTransportista)
        Next

        json = MyJSON.ConvertObjectToJSON(listadoJSON)
        sb.Append("<input type=""hidden"" id=""{PREFIJO_CONTROL}_txtJSONIdPlanificacionTransportista"" value=""" & Server.HtmlEncode(json) & """ />")
        html = sb.ToString()
      End If
    Catch ex As Exception
      html = htmlSinRegistro
    End Try

    Return html
  End Function

  ''' <summary>
  ''' graba los filtros de busqueda en sesion
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarFiltrosEnSesion()
    Dim dcFiltros As New Dictionary(Of String, String)

    Try
      dcFiltros.Add("idUsuario", Me.ddlTransportista.SelectedValue)
      dcFiltros.Add("nroTransporte", Me.txtNroTransporte.Text)
      dcFiltros.Add("fechaPresentacion", Me.txtFechaPresentacion.Text)
      dcFiltros.Add("patenteTracto", Me.txtPatenteTracto.Text)
      dcFiltros.Add("patenteTrailer", Me.txtPatenteTrailer.Text)
      dcFiltros.Add("carga", Me.ddlCarga.SelectedValue)
      dcFiltros.Add("local", Me.ddlLocal.SelectedValue)
      dcFiltros.Add("horaPresentacion", Me.wucHoraInicio.SelectedValueHora)
      dcFiltros.Add("minutoPresentacion", Me.wucHoraInicio.SelectedValueMinutos)

      Session(NOMBRE_PAGINA) = dcFiltros
    Catch ex As Exception
      Session(NOMBRE_PAGINA) = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim json, idPlanificacionTransportista, anden As String
    Dim objJSON, row As Object
    Dim oPlanificacionTransportista As New PlanificacionTransportista

    Try
      json = Me.txtJSONRevisarProgramacion.Value
      objJSON = MyJSON.ConvertJSONToObject(json)

      For Each row In objJSON
        idPlanificacionTransportista = MyJSON.ItemObject(row, "IdPlanificacionTransportista")
        anden = MyJSON.ItemObject(row, "Anden")

        'formatea valores
        anden = anden.Trim()

        'graba valores
        oPlanificacionTransportista = New PlanificacionTransportista(idPlanificacionTransportista)

        If (String.IsNullOrEmpty(oPlanificacionTransportista.Anden) And Not String.IsNullOrEmpty(anden)) Then
          oPlanificacionTransportista.FechaAsignacionAnden = Herramientas.MyNow()
          oPlanificacionTransportista.IdUsuarioAsignacionAnden = oUsuario.Id
        End If
        oPlanificacionTransportista.Anden = Sistema.Capitalize(anden, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.Actualizar()

        If (Not String.IsNullOrEmpty(oPlanificacionTransportista.NombreConductor)) Then
          oPlanificacionTransportista.AsignarTrazaViaje()
        End If

      Next

      Sistema.GrabarLogSesion(oUsuario.Id, "Graba revisión de programación")
      status = "modificado"
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la revisi&oacute;n de la programaci&oacute;n.<br />" & ex.Message
      status = "error"
    End Try

    Return msgError
  End Function

  ''' <summary>
  ''' muestra datos de la planificacion en un popover
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerHtmlPopOverDescripcion(ByVal dr As DataRow) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim criterio, observacion, htmlBloqueadoPorFueraHorario, fechaBloqueoFueraHorario As String
    Dim bloqueadoPorFueraHorario As Boolean
    Dim htmlSinRegistro As String = "<em>Sin dato</em>"

    Try
      template &= "<div><strong>CRITERIO</strong></div>"
      template &= "<div>{CRITERIO}</div>"
      template &= "<br />"
      template &= "<div><strong>OBSERVACION</strong></div>"
      template &= "<div>{OBSERVACION}</div>"
      template &= "<div>{BLOQUEADO_POR_FUERA_HORARIO}</div>"

      'obtiene valores
      criterio = dr.Item("Criterio")
      observacion = dr.Item("Observacion")
      bloqueadoPorFueraHorario = dr.Item("BloqueadoPorFueraHorario")
      fechaBloqueoFueraHorario = dr.Item("FechaBloqueoFueraHorario")

      'formatea valores
      criterio = IIf(String.IsNullOrEmpty(criterio), htmlSinRegistro, criterio)
      observacion = IIf(String.IsNullOrEmpty(observacion), htmlSinRegistro, observacion)
      htmlBloqueadoPorFueraHorario = IIf(bloqueadoPorFueraHorario, "<br /><span class='alert alert-danger sist-padding-label-prioridad sist-margin-cero'><strong>Bloqueado por fuera horario, a las " & fechaBloqueoFueraHorario & "</strong></span>", "")

      'reemplaza marcas
      template = template.Replace("{CRITERIO}", Server.HtmlEncode(criterio))
      template = template.Replace("{OBSERVACION}", Server.HtmlEncode(observacion))
      template = template.Replace("{BLOQUEADO_POR_FUERA_HORARIO}", htmlBloqueadoPorFueraHorario)
      html = template
    Catch ex As Exception
      html = ex.Message
    End Try

    Return html
  End Function

#End Region

End Class