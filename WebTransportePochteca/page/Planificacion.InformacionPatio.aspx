﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Planificacion.InformacionPatio.aspx.vb" Inherits="WebTransportePochteca.Planificacion_InformacionPatio" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Informaci&oacute;n Patio</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/planificacion.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Informaci&oacute;n Patio"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          
          <h4>Confirmaci&oacute;n And&eacute;n</h4>
          <div id="pnlConfirmacionAnden" runat="server" class="form-group row">
            <div class="col-xs-4">
              <label class="control-label">And&eacute;n</label>
              <div><asp:Label ID="lblAnden" runat="server"></asp:Label></div>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Fecha Confirmaci&oacute;n</label>
              <div><asp:Label ID="lblFechaConfirmacionAnden" runat="server"></asp:Label></div>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Confirmado Por</label>
              <div><asp:Label ID="lblConfirmadoAndenPor" runat="server"></asp:Label></div>
            </div>
          </div>
          <asp:Literal ID="ltlMensajeConfirmacionAnden" runat="server"></asp:Literal>
          <hr />

          <h4>Confirmaci&oacute;n Sello</h4>
          <div id="pnlConfirmacionSello" runat="server" class="form-group row">
            <div class="col-xs-4">
              <label class="control-label">Sello</label>
              <div><asp:Label ID="lblSello" runat="server"></asp:Label></div>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Fecha Confirmaci&oacute;n</label>
              <div><asp:Label ID="lblFechaConfirmacionSello" runat="server"></asp:Label></div>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Confirmado Por</label>
              <div><asp:Label ID="lblConfirmadoSelloPor" runat="server"></asp:Label></div>
            </div>
          </div>
          <asp:Literal ID="ltlMensajeConfirmacionSello" runat="server"></asp:Literal>
        </asp:Panel>
        
        <br />
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Planificacion.cerrarPopUp('0')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnActualizarListado" runat="server" CssClass="btn btn-lg btn-primary"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Actualizar listado</asp:LinkButton>
        </div>

      </div>
    </div>

  </form>
</body>
</html>
