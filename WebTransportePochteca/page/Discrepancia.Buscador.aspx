﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Discrepancia.Buscador.aspx.vb" Inherits="WebTransportePochteca.Discrepancia_Buscador" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/discrepancia.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h2">Discrepancias</p>
      </div>
      <div class="col-xs-4 text-right">
        &nbsp;
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body small">
          <div class="row">
            <div class="col-xs-3">
              <label class="control-label">Resoluci&oacute;n Final</label>
              <uc1:wucCombo id="ddlResolucionFinal" runat="server" FuenteDatos="Tabla" TipoCombo="DiscrepanciaResolucionFinal" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Tienda</label>
              <input type="hidden" id="txtJSONLocales" runat="server" value="[]" />
              <select id="ddlLocal" runat="server" class="form-control chosen-select-locales"></select>
              <div><asp:CheckBox ID="chkSoloFocoTablet" runat="server" Text="&nbsp;S&oacute;lo con tablet" /></div>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Secci&oacute;n</label>
              <uc1:wucCombo id="ddlSeccion" runat="server" FuenteDatos="Tabla" TipoCombo="DiscrepanciaSeccion" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Cedis</label>
              <uc1:wucCombo id="ddlCentroDistribucion" runat="server" FuenteDatos="Tabla" TipoCombo="CentroDistribucion" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">Carga</label>
              <uc1:wucCombo id="ddlCarga" runat="server" FuenteDatos="Tabla" TipoCombo="Carga" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">IdMaster</label>
              <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control input-sm"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">DM</label>
              <asp:TextBox ID="txtDM" runat="server" MaxLength="150" CssClass="form-control input-sm"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Contable Desde</label>
              <asp:TextBox ID="txtFechaContableDesde" runat="server" MaxLength="10" CssClass="form-control date-pick input-sm"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Contable Hasta</label>
              <asp:TextBox ID="txtFechaContableHasta" runat="server" MaxLength="10" CssClass="form-control date-pick input-sm"></asp:TextBox>
              <div id="lblMensajeErrorFechaContableHasta" runat="server"></div>
              <div id="lblMensajeErrorFechaDiferenciaDias" runat="server"></div>
            </div>
            <div class="col-xs-1">
              <label class="control-label">Nro. Filas</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control input-sm" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Discrepancia.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
          <div id="lblMensajeDiferenciaDias" runat="server"></div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtDiferenciaDias" runat="server" value="0" />
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered" EnableModelValidation="True">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="IdMaster" SortExpression="NroTransporte">
                <ItemTemplate>
                  <asp:Label ID="lblNroTransporte" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DM" HeaderText="DM" SortExpression="DM" />
              <asp:BoundField DataField="NombreLocal" HeaderText="Tienda" SortExpression="NombreLocal" />
              <asp:BoundField DataField="FechaContable" HeaderText="Fecha Contable" SortExpression="FechaContable" />
              <asp:BoundField DataField="MontoDiscrepancia" HeaderText="Monto Discrepancia" SortExpression="MontoDiscrepancia" />
              <asp:BoundField DataField="Seccion" HeaderText="Secci&oacute;n" SortExpression="Seccion" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtDM" runat="server" value='<%# Bind("DM") %>' />
                  <input type="hidden" id="txtNroTransporte" runat="server" value='<%# Bind("NroTransporte") %>' />
                  <input type="hidden" id="txtCodigoLocal" runat="server" value='<%# Bind("CodigoLocal") %>' />
                  <input type="hidden" id="txtNombreLocal" runat="server" value='<%# Bind("NombreLocal") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

      <div class="form-group text-center">
        <div id="btnExportar" runat="server" class="btn btn-success" onclick="Reporte.exportar({ prefijoControl: Sistema.PREFIJO_CONTROL, invocadoDesde: Reporte.FORMULARIO_DISCREPANCIA_BUSCADOR, llaveReporte: 'DiscrepanciaBuscador' })"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar alertas</div>
        <div id="lblMensajeGeneracionReporte"></div>
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });

      jQuery("#" + Sistema.PREFIJO_CONTROL + "lblMensajeDiferenciaDias").html("<em>(los registros que se muestran tiene como diferencia hasta " + Discrepancia.DIFERENCIA_DIAS_RANGO_FECHA + " d&iacute;as, desde la fecha actual o desde un rango de fecha establecido)</em>");
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txtDiferenciaDias").val(Discrepancia.DIFERENCIA_DIAS_RANGO_FECHA);
    });
  </script>

</asp:Content>
