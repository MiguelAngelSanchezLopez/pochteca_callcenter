﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Mantenedor_CambiarClave

  '''<summary>
  '''Control Head1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

  '''<summary>
  '''Control form1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

  '''<summary>
  '''Control txtClaveEncriptada.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtClaveEncriptada As Global.System.Web.UI.HtmlControls.HtmlInputHidden

  '''<summary>
  '''Control lblTituloFormulario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTituloFormulario As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control pnlMensajeUsuario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeUsuario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control ltlMensajeCaducidadPassword.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ltlMensajeCaducidadPassword As Global.System.Web.UI.WebControls.Literal

  '''<summary>
  '''Control txtClaveActual.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtClaveActual As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtNuevaClave.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtNuevaClave As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtConfirmarNuevaClave.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtConfirmarNuevaClave As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control btnCerrarSesion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnCerrarSesion As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control btnCerrar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnCerrar As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control btnGrabar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnGrabar As Global.System.Web.UI.WebControls.LinkButton
End Class
