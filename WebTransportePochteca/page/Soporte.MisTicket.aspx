﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Soporte.MisTicket.aspx.vb" Inherits="WebTransportePochteca.Soporte_MisTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables.bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/combo.js"></script>
  <script type="text/javascript" src="../js/soporte.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Mis Ticket</p>
      </div>
      <div class="col-xs-4 text-right">
        <div id="btnNuevo" runat="server" class="btn btn-success" onclick="Sistema.accederUrl({ location: 'top', url: 'Soporte.NuevoTicket.aspx' });"><span class="glyphicon glyphicon-plus"></span>&nbsp;Enviar Ticket</div>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-default">
        <div class="panel-heading"><span class="panel-title"><strong>FILTROS DE B&Uacute;SQUEDA</strong></span></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">Nro Ticket</label>
              <asp:TextBox ID="txtIdTicket" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Texto de b&uacute;squeda</label>
              <asp:TextBox ID="txtTextoBusqueda" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
              <div class="help-block small sist-margin-cero">(escriba un texto para realizar una b&uacute;squeda generalizada)</div>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Estado</label>
              <select id="ddlEstado" runat="server" class="form-control chosen-select">
                <option value="">--Todos--</option>
              </select>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <button type="button" id="btnFiltrar" class="btn btn-primary" onclick="Soporte.validarFormularioBusqueda()"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</button>
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <div class="table-responsive" style="width: 100%">
          <div id="hTablaDatos" runat="server"></div>
        </div>
      </div>
    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();
      setTimeout(function () { Soporte.inicializarControlesMisTicket(); }, 500); 
    });
  </script>

</asp:Content>
