﻿Imports System.Web.Script.Serialization

Public Class Carrier_EmbarqueEnTienda
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim semaforo As String = Request("semaforo")
    Dim jsonString As String = Request("json")
    Dim jss As New JavaScriptSerializer
    Dim json As Object = jss.Deserialize(Of Object)(jsonString)

    If (semaforo = "ROJO") Then
      semaforo = "<div class=""text-danger""><img src=""../img/bullet_red.png"" alt="""" />" + json.item("IdEmbarque") + "</div>"
    Else
      semaforo = "<div class=""text-success""><img src=""../img/bullet_green.png"" alt="""" /> " + json.item("IdEmbarque") + "</div>"
    End If

    lblSemaforo.Text = semaforo
    lblIdMaster.Text = json.item("IdMaster")
    lblCedis.Text = json.item("NombreCompletoCedis")
    lblPlacaRemolque.Text = json.item("PlacaRemolque")
    lblPlacaTracto.Text = json.item("PlacaTracto")
    lblDeterminante.Text = json.item("Determinante")
    lblLLegada.Text = json.item("FechaLlegada")
    lblFechaCita.Text = json.item("FechaCita")
    lblTipoTienda.Text = json.item("TipoTienda")
    lblTiempoSobrestadia.Text = json.item("TiempoSobreestadia")
    lblEstadoEnTienda.Text = json.item("EstadoEnTienda")

  End Sub

End Class