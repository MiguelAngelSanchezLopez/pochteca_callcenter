﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mantenedor.DefinicionAlertaDetalle.aspx.vb" Inherits="WebTransportePochteca.Mantenedor_DefinicionAlertaDetalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Definici&oacute;n Alerta</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdAlertaDefinicion" runat="server" value="-1" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS GENERALES</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-8">
                    <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtNombre" runat="server" MaxLength="255" CssClass="form-control"></asp:TextBox>
                    <span id="hErrorNombre" runat="server"></span>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Gestionar por CEMTRA</label>
                    <div><asp:CheckBox ID="chkGestionarPorCemtra" runat="server" /></div>
                  </div>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">FORMATOS ASOCIADOS</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <asp:ListBox ID="ddlFormato" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple"></asp:ListBox>
                <div id="hErrorFormato" runat="server"></div>
              </div>
            </div>
          </div>        

        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUpMantenedor('1')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnEliminar" runat="server" CssClass="btn btn-lg btn-danger" OnClientClick="return(Alerta.validarEliminarDefinicionAlerta())"><span class="glyphicon glyphicon-trash"></span>&nbsp;Eliminar</asp:LinkButton>
          <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioDefinicionAlerta())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
          <asp:LinkButton ID="btnModificar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioDefinicionAlerta())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

      });
    </script>

  </form>
</body>
</html>
