﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Planificacion.Cargador.aspx.vb" Inherits="WebTransportePochteca.Planificacion_Cargador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/planificacion.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Cargar Planificaci&oacute;n</p>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><strong>SELECCIONAR ARCHIVO DE CARGA</strong></h3></div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-6">
              <asp:FileUpload ID="fileCarga" runat="server" CssClass="form-control"></asp:FileUpload>
            </div>
            <div id="hBotonValidar" runat="server" class="col-xs-2">
              <asp:LinkButton ID="btnValidar" runat="server" CssClass="btn btn-success" OnClientClick="return(Planificacion.validarFormularioCarga())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Validar y Cargar datos</asp:LinkButton>
            </div>
            <div id="hMensajeBotonValidar" runat="server" class="col-xs-6 sist-display-none">
              <span class="help-block"><em>... Validando y grabando planificaci&oacute;n, espere un momento</em></span>
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO CARGA -->
      <div class="form-group">
        <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
      </div>

    </asp:Panel>
  </div>

</asp:Content>
