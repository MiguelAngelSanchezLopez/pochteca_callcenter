﻿jQuery.fn.makeSearchList = function (opciones) {
  var prefijoTextoBusqueda = "";
  var tagHtmlFiltrar = "li";

  if (opciones) {
    if (opciones.elem_input) { input = jQuery("#" + opciones.elem_input); }
    if (opciones.prefijoTextoBusqueda) { prefijoTextoBusqueda = opciones.prefijoTextoBusqueda; }
    if (opciones.tag_html_filtrar) { tagHtmlFiltrar = opciones.tag_html_filtrar; }
  } else {
    input = jQuery("<strong>Buscar</strong>&nbsp;<input type=\"text\" class=\"form-control\" id=\"msl_txtBuscador\" /><br />");
    jQuery(this).before(input);
  }

  var list = jQuery(this);
  jQuery(input).keyup(function () {
    var searchText = jQuery(this).val();

    jQuery(list).find(tagHtmlFiltrar).each(function () {
      var currentLiText = jQuery(this).text().toUpperCase();
      var showCurrentLi = currentLiText.indexOf(prefijoTextoBusqueda.toUpperCase() + searchText.toUpperCase()) !== -1;
      jQuery(this).toggle(showCurrentLi);
    });

  });

};
