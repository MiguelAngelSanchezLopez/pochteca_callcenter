﻿var Reporte = {
  KEY_TIPO_REPORTE_GENERICO: "Generico", //por defecto activa los controles: FECHA DESDE, FECHA HASTA
  SETINTERVAL_VALIDAR_GENERACION: 0,
  FORMULARIO_SUPERVISOR_REPORTE_LISTADO: "Supervisor.ReporteListado.aspx",
  FORMULARIO_OT_ALERTA_ROJA_LISTADO: "OT.AlertaRojaListado.aspx",
  FORMULARIO_REVISAR_PROGRAMACION: "Planificacion.RevisarProgramacion.aspx",
  FORMULARIO_DISCREPANCIA_BUSCADOR: "Discrepancia.Buscador.aspx",
  FORMULARIO_DISCREPANCIA_DETALLE: "Discrepancia.Detalle.aspx",
  FORMULARIO_VIAJE_IMPORTACION_LISTADO: "GestionOperacion.AsignacionViajeImportacionListado.aspx",
  FORMULARIO_VIAJE_EXPORTACION_LISTADO: "GestionOperacion.AsignacionViajeExportacionListado.aspx",

  mostrarControles: function () {
    var llaveReporte;

    try {
      Reporte.ocultarControles();
      Reporte.obtenerDatos();
      llaveReporte = jQuery("#txtLlaveReporte").val();

      //obtiene la key para saber que controles mostrar
      switch (llaveReporte) {
        case Reporte.KEY_TIPO_REPORTE_GENERICO:
          Reporte.visibleControlesGenerico();
          break;
      }

    } catch (e) {
      alert("Exception Reporte.mostrarControles:\n" + e);
    }
  },

  obtenerDatos: function (opciones) {
    var queryString = "";
    var idReporte;

    try {
      idReporte = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlReporte_ddlCombo").val();
      idReporte = (idReporte == "") ? "-1" : idReporte;

      //construye queryString
      queryString += "op=ObtenerDatosJSON";
      queryString += "&idReporte=" + idReporte;

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener datos del reporte");
        } else {
          Reporte.cargarDatos(respuesta);
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener datos del reporte");
      }
      jQuery.ajax({
        url: "../webAjax/waReporte.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Reporte.obtenerDatos:\n" + e);
    }
  },

  cargarDatos: function (json) {
    var llaveReporte = "";
    var formatoDocumento = "";
    var item, icono;

    try {
      if (json != "") {
        json = jQuery.parseJSON(json);

        if (json) {
          if (json.length > 0) {
            item = json[0];
            llaveReporte = item.Llave;
            formatoDocumento = item.FormatoDocumento;
          }
        }
      }

      switch (formatoDocumento) {
        case "excel":
          icono = "<img src=\"../img/ico-excel.png\" alt=\"excel\" title=\"excel\" />";
          break;
        case "word":
          icono = "<img src=\"../img/ico-word.png\" alt=\"word\" title=\"word\" />";
          break;
        case "pdf":
          icono = "<img src=\"../img/ico-pdf.png\" alt=\"pdf\" title=\"pdf\" />";
          break;
        default:
          icono = "<img src=\"../img/ico-desconocido.png\" alt=\"desconocido\" title=\"desconocido\" />";
          break;
      }

      jQuery("#txtLlaveReporte").val(llaveReporte);
      jQuery("#txtFormatoDocumento").val(formatoDocumento);
      jQuery("#lblFormato").html(icono);

    } catch (e) {
      alert("Exception Reporte.cargarDatos:\n" + e);
    }

  },

  ocultarControles: function (opciones) {
    try {
      jQuery("div[data-visible=control-reporte]").hide();
    } catch (e) {
      alert("Exception Reporte.ocultarControles:\n" + e);
    }
  },

  validarFormulario: function () {
    try {
      llaveReporte = jQuery("#txtLlaveReporte").val();

      var jsonValidarCampos = [
        { elemento_a_validar: Sistema.PREFIJO_CONTROL + "ddlReporte_ddlCombo", requerido: true }
      ];

      //obtiene la key para saber que controles mostrar
      switch (llaveReporte) {
        case Reporte.KEY_TIPO_REPORTE_GENERICO:
          jsonValidarCampos = Reporte.validarControlesGenerico(jsonValidarCampos);
          break;
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        jQuery("#hTablaDatos").html(Sistema.mensajeCargandoDatos());
        jQuery("#btnFiltrar").hide();
        setTimeout(function () { Reporte.obtenerTablaDatos(); }, 100);
      }

    } catch (e) {
      alert("Exception Reporte.validarFormulario:\n" + e);
    }

  },

  obtenerTablaDatos: function () {
    var queryString = "";
    var jsonDatos, invocadoDesde;

    try {
      jsonDatos = Reporte.obtenerJSONDatos();

      //construye queryString
      queryString += "op=ObtenerTablaDatos";
      queryString += "&idReporte=" + jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlReporte_ddlCombo").val();
      queryString += "&jsonDatos=" + Sistema.urlEncode(jsonDatos);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener datos del reporte");
        } else {
          json = jQuery.parseJSON(respuesta);

          switch (parseInt(json.estado)) {
            case Sistema.CONST_CODIGO_SQL_ERROR:
            case Sistema.CONST_CODIGO_SQL_SIN_DATOS:
              jQuery("#" + Sistema.PREFIJO_CONTROL + "btnExportar").addClass("sist-display-none");
              jQuery("#" + Sistema.PREFIJO_CONTROL + "pnlMensajeUsuario").html(json.listado);
              jQuery("#hTablaDatos").html("");
              break;
            default:
              jQuery("#" + Sistema.PREFIJO_CONTROL + "btnExportar").removeClass("sist-display-none");
              jQuery("#" + Sistema.PREFIJO_CONTROL + "pnlMensajeUsuario").html("");
              jQuery("#hTablaDatos").html("<div class=\"small\">" + json.listado + "</div>");
              jQuery("#tblPrincipal").dataTable({ "bFilter": false });
              break;
          }

          jQuery("#btnFiltrar").show();
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener datos del reporte");
      }
      jQuery.ajax({
        url: "../webAjax/waReporte.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception Reporte.obtenerTablaDatos:\n" + e);
    }

  },

  exportar: function (opciones) {
    var queryString = "";
    var jsonDatos, invocadoDesde, idUsuario, nroTransporte, fechaPresentacion, patenteTracto, patenteTrailer, carga, codigoLocal, seccion;
    var codigoCentroDistribucion, dm, fechaContableDesde, fechaContableHasta, resolucionFinal, prefijoControl, diferenciaDias, soloFocoTablet;

    try {
      invocadoDesde = opciones.invocadoDesde;
      prefijoControl = opciones.prefijoControl;

      switch (invocadoDesde) {
        case Reporte.FORMULARIO_SUPERVISOR_REPORTE_LISTADO:
          //construye queryString
          queryString += "?idReporte=" + jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlReporte_ddlCombo").val();
          break;

        case Reporte.FORMULARIO_OT_ALERTA_ROJA_LISTADO:
          //construye queryString
          queryString += "?idReporte=-1";
          queryString += "&llaveReporte=" + opciones.llaveReporte;
          break;

        case Reporte.FORMULARIO_REVISAR_PROGRAMACION:
          idUsuario = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlTransportista_ddlCombo").val();
          nroTransporte = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtNroTransporte").val();
          fechaPresentacion = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaPresentacion").val();
          patenteTracto = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtPatenteTracto").val();
          patenteTrailer = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtPatenteTrailer").val();
          carga = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlCarga_ddlCombo").val();

          //construye queryString
          queryString += "?idUsuario=" + idUsuario;
          queryString += "&nroTransporte=" + nroTransporte;
          queryString += "&fechaPresentacion=" + fechaPresentacion;
          queryString += "&patenteTracto=" + patenteTracto;
          queryString += "&patenteTrailer=" + patenteTrailer;
          queryString += "&carga=" + carga;
          queryString += "&llaveReporte=" + opciones.llaveReporte;
          break;

        case Reporte.FORMULARIO_DISCREPANCIA_BUSCADOR:
          nroTransporte = jQuery("#" + prefijoControl + "txtNroTransporte").val();
          codigoLocal = jQuery("#" + prefijoControl + "ddlLocal").val();
          seccion = jQuery("#" + prefijoControl + "ddlSeccion_ddlCombo").val();
          codigoCentroDistribucion = jQuery("#" + prefijoControl + "ddlCentroDistribucion_ddlCombo").val();
          dm = jQuery("#" + prefijoControl + "txtDM").val();
          fechaContableDesde = jQuery("#" + prefijoControl + "txtFechaContableDesde").val();
          fechaContableHasta = jQuery("#" + prefijoControl + "txtFechaContableHasta").val();
          resolucionFinal = jQuery("#" + prefijoControl + "ddlResolucionFinal_ddlCombo").val();
          carga = jQuery("#" + prefijoControl + "ddlCarga_ddlCombo").val();
          diferenciaDias = jQuery("#" + prefijoControl + "txtDiferenciaDias").val();
          soloFocoTablet = (jQuery("#" + prefijoControl + "chkSoloFocoTablet").is(":checked")) ? "1" : "0";

          queryString += "?idReporte=-1";
          queryString += "&llaveReporte=" + opciones.llaveReporte;
          queryString += "&nroTransporte=" + nroTransporte;
          queryString += "&codigoLocal=" + codigoLocal;
          queryString += "&seccion=" + Sistema.urlEncode(seccion);
          queryString += "&codigoCentroDistribucion=" + codigoCentroDistribucion;
          queryString += "&dm=" + dm;
          queryString += "&fechaContableDesde=" + fechaContableDesde;
          queryString += "&fechaContableHasta=" + fechaContableHasta;
          queryString += "&resolucionFinal=" + Sistema.urlEncode(resolucionFinal);
          queryString += "&carga=" + carga;
          queryString += "&diferenciaDias=" + diferenciaDias;
          queryString += "&soloFocoTablet=" + soloFocoTablet;
          break;

        case Reporte.FORMULARIO_DISCREPANCIA_DETALLE:
          nroTransporte = jQuery("#" + prefijoControl + "txtNroTransporte").val();
          codigoLocal = jQuery("#" + prefijoControl + "txtCodigoLocal").val();

          queryString += "?idReporte=-1";
          queryString += "&llaveReporte=" + opciones.llaveReporte;
          queryString += "&nroTransporte=" + nroTransporte;
          queryString += "&codigoLocal=" + codigoLocal;
          break;

        case Reporte.FORMULARIO_VIAJE_IMPORTACION_LISTADO:
        case Reporte.FORMULARIO_VIAJE_EXPORTACION_LISTADO:
          queryString += "?idReporte=-1";
          queryString += "&llaveReporte=" + opciones.llaveReporte;
          break;
      };

      queryString += "&invocadoDesde=" + Sistema.urlEncode(invocadoDesde);

      jQuery("#" + Sistema.PREFIJO_CONTROL + "btnExportar").addClass("sist-display-none");
      jQuery("#lblMensajeGeneracionReporte").html(Sistema.mensajeCargandoDatos("Generando reporte, espere un momento por favor ..."));
      jQuery("#lblMensajeGeneracionReporte").show();

      setTimeout(function () {
        Reporte.validarGeneracion();
        //envia valores para generar el excel
        Sistema.accederUrl({ location: "top", url: "../page/Common.GenerarReporte.aspx" + queryString });
      }, 100);
    } catch (e) {
      alert("Exception Reporte.exportar:\n" + e);
    }

  },

  obtenerJSONDatos: function () {
    var llaveReporte, json;

    try {
      llaveReporte = jQuery("#txtLlaveReporte").val();

      //obtiene la key para saber que controles mostrar
      switch (llaveReporte) {
        case Reporte.KEY_TIPO_REPORTE_GENERICO:
          json = Reporte.obtenerJSONDatosGenerico();
          break;
      }

    } catch (e) {
      alert("Exception Reporte.obtenerJSONDatos:\n" + e);
    }

    return Sistema.convertirJSONtoString(json);
  },

  validarGeneracion: function () {
    var queryString = "";
    var idGenerarReporte;

    //construye queryString
    queryString += "op=ObtenerGeneracionInforme";

    var okFunc = function (t) {
      var respuesta = t;
      if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
        alert("Ha ocurrido un error interno y no se pudo obtener la generación del reporte");
      } else {
        //vuelve a monitear la sesion despues de X segundos
        if (respuesta == "0") {
          Reporte.SETINTERVAL_VALIDAR_GENERACION = setTimeout("Reporte.validarGeneracion()", 1000);
        } else {
          clearTimeout(Reporte.SETINTERVAL_VALIDAR_GENERACION);
          jQuery("#" + Sistema.PREFIJO_CONTROL + "btnExportar").removeClass("sist-display-none");
          jQuery("#lblMensajeGeneracionReporte").html("");
          jQuery("#lblMensajeGeneracionReporte").hide();
        }
      }
    }
    var errFunc = function (t) {
      alert("Ha ocurrido un error interno y no se pudo obtener la generación del reporte");
    }
    jQuery.ajax({
      url: "../webAjax/waReporte.aspx",
      type: "post",
      async: false,
      data: queryString,
      success: okFunc,
      error: errFunc
    });
  },


  //---------------------------------------------------------------
  // REPORTE: GENERICO
  //---------------------------------------------------------------
  visibleControlesGenerico: function () {
    try {
      //muestra controles
      jQuery("#hFormato").show();
      jQuery("#hFechaDesde").show();
      jQuery("#hFechaHasta").show();
      jQuery("#hBotonFiltrar").show();

      //limpia controles
      jQuery("#txtFechaDesde").val("");
      jQuery("#txtFechaHasta").val("");
    } catch (e) {
      alert("Exception Reporte.visibleControlesGenerico:\n" + e);
    }

  },

  validarControlesGenerico: function (jsonValidarCampos) {
    var item;

    try {
      var fechaDesde = jQuery("#txtFechaDesde").val();
      var fechaHasta = jQuery("#txtFechaHasta").val();

      item = { elemento_a_validar: "txtFechaDesde", requerido: false, tipo_validacion: "fecha" };
      jsonValidarCampos.push(item);

      item = { elemento_a_validar: "txtFechaHasta", requerido: false, tipo_validacion: "fecha" };
      jsonValidarCampos.push(item);

      item = { valor_a_validar: fechaDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: "lblMensajeErrorFechaHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaDesde, horaInicio: "00:00", fechaTermino: fechaHasta, horaTermino: "23:59"} };
      jsonValidarCampos.push(item);

    } catch (e) {
      alert("Exception Reporte.validarControlesGenerico:\n" + e);
    }

    return jsonValidarCampos;
  },

  obtenerJSONDatosGenerico: function () {
    var json = [];
    var item;

    try {
      item = {
        fechaDesde: jQuery("#txtFechaDesde").val(),
        fechaHasta: jQuery("#txtFechaHasta").val()
      };
      json.push(item);

    } catch (e) {
      alert("Exception Reporte.obtenerJSONDatosGenerico:\n" + e);
    }

    return json;
  },

  accionPdf: function (opciones) {
    var queryString = "";
    var jsonDatos, invocadoDesde;
    var transportista, email, valorChecked;
    var status = "";

    try {
      var json = [];
      var item = {};

      if (Reporte.validaControlesPDF(opciones)) {

        jQuery("input:checkbox:checked").each(function () {
          valorChecked = jQuery(this).val().split('|');

          item = {
            nombreTransportista: valorChecked[0],
            emailTransportista: valorChecked[1],
            rutTransportista: valorChecked[2]
          };

          json.push(item);

        });

        queryString += "?op=" + opciones.Accion + "&json=" + Sistema.convertirJSONtoString(json) + "&pag=AlertaRojaListado";

        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnDownload").addClass("sist-display-none");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMail").addClass("sist-display-none");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnExportar").addClass("sist-display-none");

        jQuery("#lblMensajeGeneracionReporte").html(Sistema.mensajeCargandoDatos("Generando reporte, espere un momento por favor ..."));
        jQuery("#lblMensajeGeneracionReporte").show();

        setTimeout(function () {
          Reporte.validarGeneracionPDF();

          //envia valores para generar el excel
          Sistema.accederUrl({ location: "top", url: "../page/Common.GenerarReportePDF.aspx" + queryString });

        }, 100);
      }
    } catch (e) {
      alert("Exception Reporte.accionPdf:\n" + e);
    }
  },

  validaControlesPDF: function (opciones) {
    var isValid = true;
    var numberOfChecked = jQuery('input:checkbox:checked').length;

    if (numberOfChecked > 0) {
      if (opciones.Accion == "email") {
        jQuery("input:checkbox:checked").each(function () {
          var email = jQuery(this).val().split('|')[1];

          if (email.length < 1) {
            alert("No se puede enviar PDF a Línea de Transporte sin correo");
            isValid = false;
            return false;
          }
        });
      }
    } else {
      alert("Debe seleccionar al menos una Línea de Transporte");
      isValid = false;
    }

    return isValid;
  },

  validarGeneracionPDF: function () {
    var queryString = "";
    var idGenerarReporte;

    //construye queryString
    queryString += "op=ObtenerGeneracionInforme";

    var okFunc = function (t) {
      var respuesta = t;
      if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
        alert("Ha ocurrido un error interno y no se pudo obtener la generación del reporte");
      } else {
        //vuelve a monitear la sesion despues de X segundos
        if (respuesta == "0") {
          Reporte.SETINTERVAL_VALIDAR_GENERACION = setTimeout("Reporte.validarGeneracionPDF()", 1000);
        } else {
          clearTimeout(Reporte.SETINTERVAL_VALIDAR_GENERACION);
          jQuery("#" + Sistema.PREFIJO_CONTROL + "btnDownload").removeClass("sist-display-none");
          jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMail").removeClass("sist-display-none");
          jQuery("#" + Sistema.PREFIJO_CONTROL + "btnExportar").removeClass("sist-display-none");
          jQuery("#lblMensajeGeneracionReporte").html("");
          jQuery("#lblMensajeGeneracionReporte").hide();
        }
      }
    }
    var errFunc = function (t) {
      alert("Ha ocurrido un error interno y no se pudo obtener la generación del reporte");
    }
    jQuery.ajax({
      url: "../webAjax/waReporte.aspx",
      type: "post",
      async: false,
      data: queryString,
      success: okFunc,
      error: errFunc
    });
  },

  reporteRespuestaAlertaRoja: function (opciones) {
    var queryString = "";
    var jsonDatos, invocadoDesde;
    var status = "";

    var idAlertaRojaEstadoActual = opciones.idAlertaRojaEstadoActual;
    var nroTransporte = opciones.nroTransporte;

    queryString += "?pag=RespuestaAlertaRoja";
    queryString += "&idAlertaRojaEstadoActual=" + idAlertaRojaEstadoActual;
    queryString += "&nroTransporte=" + nroTransporte;

    try {
      jQuery("#lblMensajeGeneracionReporte").html(Sistema.mensajeCargandoDatos("Generando reporte, espere un momento por favor ..."));
      jQuery("#lblMensajeGeneracionReporte").show();

      setTimeout(function () {
        //envia valores para generar el excel
        Sistema.accederUrl({ location: "top", url: "../page/Common.GenerarReportePDF.aspx" + queryString });
      }, 100);
    } catch (e) {
      alert("Exception Reporte.reporteRespuestaAlertaRoja:\n" + e);
    }
  }



};