﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login.aspx.vb" Inherits="WebTransportePochteca.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <title>Ingreso Alto Track</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="css/sistema.css" />
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
  <script type="text/javascript" src="js/sistema.js"></script>
  <script type="text/javascript" src="js/validacion.js"></script>
</head>

<body>
  <form id="form1" runat="server" autocomplete="off">
    <div class="container">
      <div class="form-group sist-padding-top-20">&nbsp;</div>
      <div class="form-group sist-padding-top-20">&nbsp;</div>

      <div class="form-group">
        <center>
          <div class="sist-width-login">
            <div class="well well-lg sist-padding-35">
              <div class="row">
                <div class="col-xs-6">
                  <img src="img/logo128.png" alt="logo" />
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                      <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control input-lg" Text=""></asp:TextBox>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                      <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control input-lg"></asp:TextBox>
                    </div>
                  </div>
                  <div>
                    <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" CssClass="btn btn-lg btn-primary btn-block" OnClientClick="return(Sistema.login())" />
                  </div>
                </div>
              </div>
            </div>
            <div class="help-block text-center">
              <asp:Label ID="lblVersionSistema" runat="server"></asp:Label>
            </div>
            <div class="form-group text-center">
              <asp:Panel ID="pnlMensajeError" runat="server" CssClass="alert alert-danger" Visible="false">
                <asp:Label ID="lblMensaje" runat="server" Text="&nbsp;"></asp:Label>
              </asp:Panel>
            </div>
          </div>
        </center>
      </div>

      <div id="footer" class="form-group text-center">
        <asp:Label ID="lblAmbienteEjecucion" runat="server" Text="" CssClass="h5"></asp:Label>
      </div>

    </div>

  </form>
</body>
</html>
