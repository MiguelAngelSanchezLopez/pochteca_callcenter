﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="OT.RespuestaAlertaRojaListado.aspx.vb" Inherits="WebTransportePochteca.OT_RespuestaAlertaRojaListado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Respuesta Alertas Rojas</p>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-3">
              <label class="control-label">IdMaster</label>
              <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Estado</label>
              <uc1:wucCombo id="ddlEstadoAlertaRoja" runat="server" FuenteDatos="Tabla" TipoCombo="EstadoRespuestaAlertaRoja" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioBuscadorRespuestaAlertaRoja())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="1" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="80px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="NroTransporte" HeaderText="IdMaster" SortExpression="NroTransporte" HeaderStyle-Width="200px" />
              <asp:BoundField DataField="NombreTransportista" HeaderText="L&iacute;nea de Transporte" SortExpression="NombreTransportista" />
              <asp:BoundField DataField="Fecha" HeaderText="Fecha respuesta" SortExpression="Fecha" HeaderStyle-Width="180px" />
              <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" HeaderStyle-Width="300px" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdAlertaRojaEstadoActual" runat="server" value='<%# Bind("IdAlertaRojaEstadoActual") %>' />
                  <input type="hidden" id="txtNroTransporte" runat="server" value='<%# Bind("NroTransporte") %>' />
                  <input type="hidden" id="txtNombreTransportista" runat="server" value='<%# Bind("NombreTransportista") %>' />
                  <input type="hidden" id="txtRutTransportista" runat="server" value='<%# Bind("RutTransportista") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>

</asp:Content>