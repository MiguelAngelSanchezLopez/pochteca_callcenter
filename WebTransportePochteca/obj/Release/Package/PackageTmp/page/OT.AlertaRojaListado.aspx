﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="OT.AlertaRojaListado.aspx.vb" Inherits="WebTransportePochteca.OT_AlertaRojaListado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
  <script type="text/javascript" src="../js/alerta.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Alertas Rojas</p>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-2">
              <label class="control-label">Fecha Desde</label>
              <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Hasta</label>
              <asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              <div id="lblMensajeErrorFechaHasta" runat="server">
              </div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-3">
              <asp:CheckBox ID="chkGestionadas" runat="server" CssClass="control-label" Text="&nbsp;Alertas Gestionadas" />
            </div>
          </div>
        </div>
      </div>
      <!-- RESULTADO -->
      <div class="form-group">
        <asp:Panel ID="pnlMensajeUsuario" runat="server">
        </asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered" EnableModelValidation="True" DataKeyNames="NombreTransportista,CorreoTransportista">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="NombreTransportista" HeaderText="L&iacute;nea de Transporte" SortExpression="NombreTransportista" HeaderStyle-Width="250px" />
              <asp:TemplateField HeaderText="Correo">
                <ItemTemplate>
                  <asp:Label ID="lblCorreoTransportista" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="FechaDesde" HeaderText="Fecha Desde" SortExpression="FechaDesde" HeaderStyle-Width="160px" />
              <asp:BoundField DataField="FechaHasta" HeaderText="Fecha Hasta" SortExpression="FechaHasta" HeaderStyle-Width="160px" />
              <asp:BoundField DataField="CantidadAlerta" HeaderText="Cantidad Alertas" SortExpression="CantidadAlerta" HeaderStyle-Width="140px" />
              <asp:TemplateField HeaderText="Seleccionar" HeaderStyle-Width="110px">
                <ItemTemplate>
                  <input id="chkSeleccion" type="checkbox" value="<%# Eval("NombreTransportista") + "|" + Eval("CorreoTransportista") + "|" + Eval("RutTransportista")%>" />
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtRutTransportista" runat="server" value='<%# Bind("RutTransportista") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>
      </div>
      <asp:Panel ID="pnlBotones" runat="server">
        <div class="form-group text-center">
          <div id="btnDownload" runat="server" class="btn btn-lg btn-success" onclick="Reporte.accionPdf({Accion:'download'})"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Descargar PDF</div>
          <div id="btnMail" runat="server" class="btn btn-lg btn-success" onclick="Reporte.accionPdf({Accion:'email'})"><span class="glyphicon glyphicon-send"></span>&nbsp;Enviar PDF</div>
          <div id="btnExportar" runat="server" class="btn btn-lg btn-success" onclick="Reporte.exportar({ invocadoDesde: Reporte.FORMULARIO_OT_ALERTA_ROJA_LISTADO, llaveReporte: 'Generico' })"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar Alertas Rojas Cerradas</div>
          <div id="lblMensajeGeneracionReporte"></div>
        </div>
      </asp:Panel>
    </asp:Panel>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });

    });
  </script>
</asp:Content>
