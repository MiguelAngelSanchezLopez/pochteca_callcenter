﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Transportista.TransportistaDetalle.aspx.vb" Inherits="WebTransportePochteca.Transportista_TransportistaDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/transportista.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Ficha Transportista</p>
      </div>     
      <br />
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    
    <asp:Panel ID="pnlContenido" Runat="server">
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>      
        <asp:Panel ID="pnlFormulario" runat="server">
          
          <!--Datos Formato -->
          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS DEL FORMATO</h3></div>
            <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                    <label class="control-label">Rut<strong class="text-danger">&nbsp;*</strong></label>                                    
                    <asp:TextBox ID="txtRutEmpresa" runat="server" Columns="20" MaxLength="20" 
                      CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>                                    
                    <asp:TextBox ID="txtNombreEmpresa" runat="server" Columns="20" MaxLength="20" 
                      CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
                <div class="col-xs-2">
                  <label class="control-label">Volumen Flota<strong class="text-danger">&nbsp;*</strong></label>                                    
                  <asp:TextBox ID="txtVolumen" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
            </div>
            </div>
          </div>

          <!--Dueño Formato -->
          <div class="panel panel-primary">
          <div class="panel-heading"><h3 class="panel-title">DUEÑO FORMATO</h3></div>
            <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                    <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>                                    
                    <asp:TextBox ID="txtNombreDueño" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">Teléfono 1<strong class="text-danger">&nbsp;*</strong></label>                                    
                    <asp:TextBox ID="txtTelefono1Dueño" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Teléfono 2<strong class="text-danger">&nbsp;*</strong></label>                                    
                  <asp:TextBox ID="txtTelefono2Dueño" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="row">
              <div class="col-xs-5">
                  <label class="control-label">Correo Electrónico<strong class="text-danger">&nbsp;*</strong></label>                                    
                  <asp:TextBox ID="txtEmailDueño" runat="server" Columns="20" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
              </div>
            </div>

            </div>
          </div>

          <!-- Gerente Operaciones -->
          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">GERENTE OPERACIÓN</h3></div>
            <div class="panel-body">

              <div class="form-group">
              <div class="row">
              <div class="col-xs-4">
                  <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>                                    
                  <asp:TextBox ID="txtNombreGerente" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
              </div>
              <div class="col-xs-4">
                  <label class="control-label">Teléfono 1<strong class="text-danger">&nbsp;*</strong></label>                                    
                  <asp:TextBox ID="txtTelefonoGerente" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
              </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
              <div class="col-xs-5">
                  <label class="control-label">Correo Electrónico<strong class="text-danger">&nbsp;*</strong></label>                                    
                  <asp:TextBox ID="txtEmailGerente" runat="server" Columns="20" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
              </div>
            </div>

            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">ESCALAMIENTOS</h3></div>
            <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-xs-6">
                  <div>
                    <label class="control-label">Teléfono 1</label>                                    
                  </div>
                  <div class="col-sm-6 sist-padding-cero">
                    <asp:TextBox ID="txtTelefono1Escalamiento" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-sm-6 sist-padding-uno">
                    <label class="control-label">Contacto 1</label>                                    
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-xs-6">
                  <div>
                    <label class="control-label">Teléfono 2</label>                                    
                  </div>
                  <div class="col-sm-6 sist-padding-cero">
                    <asp:TextBox ID="txtTelefono2Escalamiento" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-sm-6 sist-padding-uno">
                    <label class="control-label">Contacto 2</label>                                    
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-xs-6">
                  <div>
                    <label class="control-label">Teléfono 3</label>                                    
                  </div>
                  <div class="col-sm-6 sist-padding-cero">
                    <asp:TextBox ID="txtTelefono3Escalamiento" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-sm-6 sist-padding-uno">
                    <label class="control-label">Contacto 3</label>                                    
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-xs-6">
                  <div>
                    <label class="control-label">Teléfono 4</label>                                    
                  </div>
                  <div class="col-sm-6 sist-padding-cero">
                    <asp:TextBox ID="txtTelefono4Escalamiento" runat="server" Columns="20" MaxLength="9" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-sm-6 sist-padding-uno">
                    <label class="control-label">Contacto 4</label>                                    
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>

          <div class="form-group text-center">
            <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Transportista.validarFormularioDetalle())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
          </div>
      </asp:Panel>
      </div>
      </asp:Panel>
   </div>          
 
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();
    });
  </script>

</asp:Content>
