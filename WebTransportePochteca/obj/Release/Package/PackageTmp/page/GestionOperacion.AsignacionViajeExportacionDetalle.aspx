﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionOperacion.AsignacionViajeExportacionDetalle.aspx.vb" Inherits="WebTransportePochteca.GestionOperacion_AsignacionViajeExportacionDetalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Asignaci&oacute;n viaje exportaci&oacute;n</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables.bootstrap.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/gestionOperacion.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Asignar Operador"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS VIAJE EXPORTACION</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="form-group row">
                  <div class="col-xs-2">
                    <label class="control-label">Orden Servicio</label>
                    <div><asp:Label ID="lblOrdenServicio" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Fecha Presentaci&oacute;n</label>
                    <div><asp:Label ID="lblFechaPresentacion" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Cliente</label>
                    <div><asp:Label ID="lblNombreCliente" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Medida</label>
                    <div><asp:Label ID="lblDimension" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Puerto Descarga</label>
                    <div><asp:Label ID="lblPuertoDescarga" runat="server"></asp:Label></div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-2">
                    <label class="control-label">Nave</label>
                    <div><asp:Label ID="lblNave" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Nro Contenedor</label>
                    <div><asp:Label ID="lblNroContenedor" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">Direcci&oacute;n Destino</label>
                    <div><asp:Label ID="lblDireccionDestino" runat="server"></asp:Label></div>
                  </div>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="pull-left">
                <h3 class="panel-title">OPERADORES</h3>
              </div>
              <div class="pull-right">
                <asp:LinkButton ID="btnDesasignar" runat="server" CssClass="btn btn-danger btn-sm" OnClientClick="return(GestionOperacion.desasignarConductor())" Visible="false"><span class="glyphicon glyphicon-remove"></span>&nbsp;Desasignar operador</asp:LinkButton>
                <div id="btnAsignarConductorManual" runat="server" class="btn btn-default btn-sm" onclick="GestionOperacion.abrirFormularioAsignarConductorManual()"><span class="glyphicon glyphicon-user"></span>&nbsp;Asignar manualmente</div>
              </div>
              <div class="sist-clear-both"></div>
            </div>
            <div class="panel-body">
              <div class="small">
                <input type="hidden" id="txtJSONDatosConductor" runat="server" value="[]"/>
                <input type="hidden" id="txtRutConductorAsignado" runat="server" value="" />
                <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
              </div>
              <div id="hMensajeErrorTablaDatos"></div>
            </div>
          </div> 

        </asp:Panel>

        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="GestionOperacion.cerrarPopUp('1')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(GestionOperacion.validarFormularioAsignacionConductor())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".show-tooltip").tooltip();
      });
    </script>

  </form>
</body>
</html>
